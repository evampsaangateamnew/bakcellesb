package com.nsn.devicemanagement.wsdl.common._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for ApplicationScanAction complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="ApplicationScanAction">
 *   &lt;complexContent>
 *     &lt;extension base="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}Action">
 *       &lt;sequence>
 *         &lt;element name="delivered" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="deployed" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="runningState" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ApplicationScanAction", propOrder = { "delivered", "deployed", "runningState" })
public class ApplicationScanAction extends Action {
	protected boolean delivered;
	protected boolean deployed;
	protected boolean runningState;

	/**
	 * Gets the value of the delivered property.
	 * 
	 */
	public boolean isDelivered() {
		return delivered;
	}

	/**
	 * Sets the value of the delivered property.
	 * 
	 */
	public void setDelivered(boolean value) {
		this.delivered = value;
	}

	/**
	 * Gets the value of the deployed property.
	 * 
	 */
	public boolean isDeployed() {
		return deployed;
	}

	/**
	 * Sets the value of the deployed property.
	 * 
	 */
	public void setDeployed(boolean value) {
		this.deployed = value;
	}

	/**
	 * Gets the value of the runningState property.
	 * 
	 */
	public boolean isRunningState() {
		return runningState;
	}

	/**
	 * Sets the value of the runningState property.
	 * 
	 */
	public void setRunningState(boolean value) {
		this.runningState = value;
	}
}
