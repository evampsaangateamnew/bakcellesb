package com.evampsaanga.bakcell.notificationadmin;

import java.util.List;

import com.evampsaanga.bakcell.responseheaders.BaseResponse;

public class GetHistoryNotificationsResponse extends BaseResponse {
	private List<HistoryNotification> historyNotifications;

	public List<HistoryNotification> getHistoryNotifications() {
		return historyNotifications;
	}

	public void setHistoryNotifications(List<HistoryNotification> historyNotifications) {
		this.historyNotifications = historyNotifications;
	}

	@Override
	public String toString() {
		return "GetHistoryNotificationsResponse [historyNotifications=" + historyNotifications + ", toString()="
				+ super.toString() + "]";
	}
}
