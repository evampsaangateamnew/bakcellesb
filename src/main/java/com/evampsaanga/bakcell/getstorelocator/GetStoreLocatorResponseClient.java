package com.evampsaanga.bakcell.getstorelocator;

import com.evampsaanga.bakcell.responseheaders.BaseResponse;

public class GetStoreLocatorResponseClient extends BaseResponse {
	Data data = new Data();

	/**
	 * @return the data
	 */
	public Data getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(Data data) {
		this.data = data;
	}
}
