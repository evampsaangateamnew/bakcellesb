package com.huawei.crm.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for GetOfferingSpecList complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="GetOfferingSpecList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OfferingSpecInfo" type="{http://crm.huawei.com/query/}OfferingSpecInfo" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetOfferingSpecList", propOrder = { "offeringSpecInfo" })
public class GetOfferingSpecList {
	@XmlElement(name = "OfferingSpecInfo", required = true)
	protected List<OfferingSpecInfo> offeringSpecInfo;

	/**
	 * Gets the value of the offeringSpecInfo property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the offeringSpecInfo property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getOfferingSpecInfo().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link OfferingSpecInfo }
	 * 
	 * 
	 */
	public List<OfferingSpecInfo> getOfferingSpecInfo() {
		if (offeringSpecInfo == null) {
			offeringSpecInfo = new ArrayList<OfferingSpecInfo>();
		}
		return this.offeringSpecInfo;
	}
}
