package com.evampsaanga.bakcell.getappmenu;

import com.evampsaanga.bakcell.responseheaders.BaseResponse;
/**
 * App Menu Response Container For Phase 2
 * @author Aqeel Abbas
 *
 */
public class GetAppMenuResponseV2 extends BaseResponse {
	private DatumV2 data = new DatumV2();

	public DatumV2 getData() {
		return data;
	}

	public void setData(DatumV2 data) {
		this.data = data;
	}

	
}
