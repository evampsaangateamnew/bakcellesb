
package com.huawei.crm.query.order.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SkuInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SkuInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="SkuId" type="{http://crm.huawei.com/basetype/}SkuId"/>
 *         &lt;element name="SkuName" type="{http://crm.huawei.com/basetype/}SkuName"/>
 *         &lt;element name="SkuDesc" type="{http://crm.huawei.com/basetype/}SkuDesc" minOccurs="0"/>
 *         &lt;element name="ProductId" type="{http://crm.huawei.com/basetype/}ProductId"/>
 *         &lt;element name="ResTypeId" type="{http://crm.huawei.com/basetype/}ResTypeId"/>
 *         &lt;element name="ItemCode" type="{http://crm.huawei.com/basetype/}ItemCode"/>
 *         &lt;element name="IsDefault" type="{http://crm.huawei.com/basetype/}IsDefault"/>
 *         &lt;element name="IsOnlyAmount" type="{http://crm.huawei.com/basetype/}IsOnlyAmount"/>
 *         &lt;element name="Attrs" type="{http://crm.huawei.com/query/}Attrs" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SkuInfo", propOrder = {

})
public class SkuInfo {

    @XmlElement(name = "SkuId", required = true)
    protected String skuId;
    @XmlElement(name = "SkuName", required = true)
    protected String skuName;
    @XmlElement(name = "SkuDesc")
    protected String skuDesc;
    @XmlElement(name = "ProductId", required = true)
    protected String productId;
    @XmlElement(name = "ResTypeId", required = true)
    protected String resTypeId;
    @XmlElement(name = "ItemCode", required = true)
    protected String itemCode;
    @XmlElement(name = "IsDefault", required = true)
    protected String isDefault;
    @XmlElement(name = "IsOnlyAmount", required = true)
    protected String isOnlyAmount;
    @XmlElement(name = "Attrs")
    protected Attrs attrs;

    /**
     * Gets the value of the skuId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSkuId() {
        return skuId;
    }

    /**
     * Sets the value of the skuId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSkuId(String value) {
        this.skuId = value;
    }

    /**
     * Gets the value of the skuName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSkuName() {
        return skuName;
    }

    /**
     * Sets the value of the skuName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSkuName(String value) {
        this.skuName = value;
    }

    /**
     * Gets the value of the skuDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSkuDesc() {
        return skuDesc;
    }

    /**
     * Sets the value of the skuDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSkuDesc(String value) {
        this.skuDesc = value;
    }

    /**
     * Gets the value of the productId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductId() {
        return productId;
    }

    /**
     * Sets the value of the productId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductId(String value) {
        this.productId = value;
    }

    /**
     * Gets the value of the resTypeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResTypeId() {
        return resTypeId;
    }

    /**
     * Sets the value of the resTypeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResTypeId(String value) {
        this.resTypeId = value;
    }

    /**
     * Gets the value of the itemCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemCode() {
        return itemCode;
    }

    /**
     * Sets the value of the itemCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemCode(String value) {
        this.itemCode = value;
    }

    /**
     * Gets the value of the isDefault property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsDefault() {
        return isDefault;
    }

    /**
     * Sets the value of the isDefault property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsDefault(String value) {
        this.isDefault = value;
    }

    /**
     * Gets the value of the isOnlyAmount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsOnlyAmount() {
        return isOnlyAmount;
    }

    /**
     * Sets the value of the isOnlyAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsOnlyAmount(String value) {
        this.isOnlyAmount = value;
    }

    /**
     * Gets the value of the attrs property.
     * 
     * @return
     *     possible object is
     *     {@link Attrs }
     *     
     */
    public Attrs getAttrs() {
        return attrs;
    }

    /**
     * Sets the value of the attrs property.
     * 
     * @param value
     *     allowed object is
     *     {@link Attrs }
     *     
     */
    public void setAttrs(Attrs value) {
        this.attrs = value;
    }

}
