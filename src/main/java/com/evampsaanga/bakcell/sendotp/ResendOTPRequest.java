package com.evampsaanga.bakcell.sendotp;

import com.evampsaanga.bakcell.requestheaders.BaseRequest;

public class ResendOTPRequest extends BaseRequest{
	private String cause;
	private String userName;
	public String getCause() {
		return cause;
	}
	public void setCause(String cause) {
		this.cause = cause;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	

}
