package com.evampsaanga.bakcell.notificationadmin;

public class FCMNotification {
	private String deviceId;
	private String msisdn;
	private String fcmId;
	private String ringingAction;
	private String isEnable;
	private String subscriberType;
	private String tariffType;

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getFcmId() {
		return fcmId;
	}

	public void setFcmId(String fcmId) {
		this.fcmId = fcmId;
	}

	public String getRingingAction() {
		return ringingAction;
	}

	public void setRingingAction(String ringingAction) {
		this.ringingAction = ringingAction;
	}

	public String getIsEnable() {
		return isEnable;
	}

	public void setIsEnable(String isEnable) {
		this.isEnable = isEnable;
	}

	public String getSubscriberType() {
		return subscriberType;
	}

	public void setSubscriberType(String subscriberType) {
		this.subscriberType = subscriberType;
	}

	public String getTariffType() {
		return tariffType;
	}

	public void setTariffType(String tariffType) {
		this.tariffType = tariffType;
	}

	@Override
	public String toString() {
		return "FCMNotification [deviceId=" + deviceId + ", msisdn=" + msisdn + ", fcmId=" + fcmId + ", ringingAction="
				+ ringingAction + ", isEnable=" + isEnable + ", subscriberType=" + subscriberType + ", tariffType="
				+ tariffType + "]";
	}
}
