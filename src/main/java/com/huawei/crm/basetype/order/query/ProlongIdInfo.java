
package com.huawei.crm.basetype.order.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ProlongIdInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProlongIdInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DurationID" type="{http://crm.huawei.com/basetype/}DurationId" minOccurs="0"/>
 *         &lt;element name="EffectiveMode" type="{http://crm.huawei.com/basetype/}ProlongEffectiveMode" minOccurs="0"/>
 *         &lt;element name="OperationType" type="{http://crm.huawei.com/basetype/}ProlongOperationType"/>
 *         &lt;element name="ProlongActionType" type="{http://crm.huawei.com/basetype/}ProlongActionType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProlongIdInfo", propOrder = {
    "durationID",
    "effectiveMode",
    "operationType",
    "prolongActionType"
})
public class ProlongIdInfo {

    @XmlElement(name = "DurationID")
    protected String durationID;
    @XmlElement(name = "EffectiveMode")
    protected String effectiveMode;
    @XmlElement(name = "OperationType", required = true)
    protected String operationType;
    @XmlElement(name = "ProlongActionType", required = true)
    protected String prolongActionType;

    /**
     * Gets the value of the durationID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDurationID() {
        return durationID;
    }

    /**
     * Sets the value of the durationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDurationID(String value) {
        this.durationID = value;
    }

    /**
     * Gets the value of the effectiveMode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEffectiveMode() {
        return effectiveMode;
    }

    /**
     * Sets the value of the effectiveMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEffectiveMode(String value) {
        this.effectiveMode = value;
    }

    /**
     * Gets the value of the operationType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperationType() {
        return operationType;
    }

    /**
     * Sets the value of the operationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperationType(String value) {
        this.operationType = value;
    }

    /**
     * Gets the value of the prolongActionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProlongActionType() {
        return prolongActionType;
    }

    /**
     * Sets the value of the prolongActionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProlongActionType(String value) {
        this.prolongActionType = value;
    }

}
