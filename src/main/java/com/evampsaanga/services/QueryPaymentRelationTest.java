package com.evampsaanga.services;

import com.evampsaanga.querypayment.BcService;
import com.huawei.bme.cbsinterface.bccommon.querypayment.SubAccessCode;
import com.huawei.bme.cbsinterface.bcservices.querypayment.QueryPaymentRelationRequest;
import com.huawei.bme.cbsinterface.bcservices.querypayment.QueryPaymentRelationRequestMsg;
import com.huawei.bme.cbsinterface.bcservices.querypayment.QueryPaymentRelationRequest.PaymentObj;

public class QueryPaymentRelationTest {

	public static void main(String[] args) {
		
		  QueryPaymentRelationRequestMsg paymentRelationRequestMsg = new QueryPaymentRelationRequestMsg();

		  paymentRelationRequestMsg.setRequestHeader(BcService.getRequestHeader());
		  QueryPaymentRelationRequest paymentRelationRequest = new QueryPaymentRelationRequest();

		  PaymentObj paymentObj = new PaymentObj();
		  SubAccessCode subAccessCode = new SubAccessCode();
		  subAccessCode.setPrimaryIdentity(args[0]);
		  paymentObj.setSubAccessCode(subAccessCode);
		  paymentRelationRequest.setPaymentObj(paymentObj);

		  paymentRelationRequestMsg.setQueryPaymentRelationRequest(paymentRelationRequest);
		  BcService.getInstance().queryPaymentRelation(paymentRelationRequestMsg);
		
	}
	
}
