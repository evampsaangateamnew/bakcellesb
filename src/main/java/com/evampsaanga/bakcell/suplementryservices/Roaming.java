package com.evampsaanga.bakcell.suplementryservices;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Roaming {
	@JsonProperty("filters")
	private InternetFilters filters = null;
	@JsonProperty("offers")
	private Offers[] offers;

	private List<Countries> countries;

	public Offers[] getOffers() {
		return offers;
	}

	public void setOffers(Offers[] offers) {
		this.offers = offers;
	}

	public List<Countries> getCountries() {
		return countries;
	}

	public void setCountries(List<Countries> countries) {
		this.countries = countries;
	}

}