package com.evampsaanga.bakcell.plasticcard;

import com.evampsaanga.bakcell.requestheaders.BaseRequest;

public class DeleteSavedCardRequest extends BaseRequest {

	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
