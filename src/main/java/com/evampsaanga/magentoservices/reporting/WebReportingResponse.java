package com.evampsaanga.magentoservices.reporting;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.evampsaanga.bakcell.responseheaders.BaseResponse;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "WebReportingResponse")
public class WebReportingResponse extends BaseResponse {

}
