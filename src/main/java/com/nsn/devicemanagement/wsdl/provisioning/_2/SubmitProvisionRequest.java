package com.nsn.devicemanagement.wsdl.provisioning._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import com.nsn.devicemanagement.wsdl.common._2.DmMap;

/**
 * <p>
 * Java class for submitProvisionRequest complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="submitProvisionRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="msisdn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="imeiOrPhoneType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="scenarioName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="specialProperties" type="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}dmMap" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "submitProvisionRequest", propOrder = { "msisdn", "imeiOrPhoneType", "scenarioName",
		"specialProperties" })
public class SubmitProvisionRequest {
	protected String msisdn;
	protected String imeiOrPhoneType;
	protected String scenarioName;
	protected DmMap specialProperties;

	/**
	 * Gets the value of the msisdn property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMsisdn() {
		return msisdn;
	}

	/**
	 * Sets the value of the msisdn property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMsisdn(String value) {
		this.msisdn = value;
	}

	/**
	 * Gets the value of the imeiOrPhoneType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getImeiOrPhoneType() {
		return imeiOrPhoneType;
	}

	/**
	 * Sets the value of the imeiOrPhoneType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setImeiOrPhoneType(String value) {
		this.imeiOrPhoneType = value;
	}

	/**
	 * Gets the value of the scenarioName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getScenarioName() {
		return scenarioName;
	}

	/**
	 * Sets the value of the scenarioName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setScenarioName(String value) {
		this.scenarioName = value;
	}

	/**
	 * Gets the value of the specialProperties property.
	 * 
	 * @return possible object is {@link DmMap }
	 * 
	 */
	public DmMap getSpecialProperties() {
		return specialProperties;
	}

	/**
	 * Sets the value of the specialProperties property.
	 * 
	 * @param value
	 *            allowed object is {@link DmMap }
	 * 
	 */
	public void setSpecialProperties(DmMap value) {
		this.specialProperties = value;
	}
}
