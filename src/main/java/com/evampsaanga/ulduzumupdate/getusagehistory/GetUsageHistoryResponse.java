package com.evampsaanga.ulduzumupdate.getusagehistory;

import java.util.List;

public class GetUsageHistoryResponse {
	String result;
	List<UsageHistory> data;

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public List<UsageHistory> getData() {
		return data;
	}

	public void setData(List<UsageHistory> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "GetUsageHistoryResponse [result=" + result + ", data=" + data + "]";
	}

}
