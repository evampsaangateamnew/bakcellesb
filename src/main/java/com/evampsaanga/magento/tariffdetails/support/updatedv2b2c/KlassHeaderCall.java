package com.evampsaanga.magento.tariffdetails.support.updatedv2b2c;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class KlassHeaderCall {
	
	private String callValue = "";
	private String callMetrics = "";
	private String callLabel = "";
	private String callIcon = "";

	public String getCallValue() {
		return callValue;
	}

	public void setCallValue(String callValue) {
		this.callValue = callValue;
	}

	public String getCallMetrics() {
		return callMetrics;
	}

	public void setCallMetrics(String callMetrics) {
		this.callMetrics = callMetrics;
	}

	public String getCallLabel() {
		return callLabel;
	}

	public void setCallLabel(String callLabel) {
		this.callLabel = callLabel;
	}

	public String getCallIcon() {
		return callIcon;
	}

	public void setCallIcon(String callIcon) {
		this.callIcon = callIcon;
	}

	@Override
	public String toString() {
		return "ClassPojo [callValue = " + callValue + ", callMetrics = " + callMetrics + ", callLabel = " + callLabel
				+ ", callIcon = " + callIcon + "]";
	}
}