package com.evampsaanga.bakcell.sendemail;

import java.io.IOException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.amqimplementationsesb.LogsType;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.ThirdPartyNames;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.developer.utils.SOAPLoggingHandler;

@Path("/bakcell")
public class SendEmailRequestLand {

	public static final Logger logger = Logger.getLogger("bakcellLogs-V2");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SendEmailResponse Get(@Header("credentials") String credential, @Header("Content-Type") String contentType,
			@Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.SEND_EMAIL);
		logs.setThirdPartyName(ThirdPartyNames.SENDEMAIL);
		logs.setTableType(LogsType.SendEmail);
		try {
			logger.info("Request Landed on SendEmailRequestLand:" + requestBody);
			SendEmailRequest cclient = new SendEmailRequest();
			try {
				cclient = Helper.JsonToObject(requestBody, SendEmailRequest.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setIsB2B(cclient.getIsB2B());
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				SendEmailResponse resp = new SendEmailResponse();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}

			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					SendEmailResponse resp = new SendEmailResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						SendEmailResponse res = new SendEmailResponse();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						logs.setResponseCode(res.getReturnCode());
						logs.setResponseDescription(res.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return res;
					}
				} else {
					SendEmailResponse resp = new SendEmailResponse();
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					String lang = "2002";
					if (cclient.getLang().equalsIgnoreCase("3"))
						lang = "2002";
					if (cclient.getLang().equalsIgnoreCase("2"))
						lang = "2052";
					if (cclient.getLang().equalsIgnoreCase("4"))
						lang = "2060";
					return getResponse(cclient.getText(), cclient.getSubject(), cclient.getFrom(), cclient.getTo(),
							logger, cclient);
				} else {
					SendEmailResponse resp = new SendEmailResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}

			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		SendEmailResponse resp = new SendEmailResponse();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	public SendEmailResponse getResponse(String text, String subject, String from, String to, Logger logger,
			SendEmailRequest cclient) {
		SendEmailResponse resp = new SendEmailResponse();

		logger.info("NOW LANDED IN getResponseMethod");
		// start of sendEmail

		/*
		 * final String username = "Business.Services@bakcell.com"; final String
		 * password = "B2525akcell++";
		 */

		final String username = ConfigurationManager.getConfigurationFromCache("email.smtp.username");
		final String password = ConfigurationManager.getConfigurationFromCache("email.smtp.password");

		logger.info("USERNAME VALUE CONFIG " + username);
		logger.info("PASSWORD VALUE CONFIG " + password);
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("lang", cclient.getLang());
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			if (cclient.getFrom() == null || cclient.getFrom().isEmpty()) {
				if (cclient.getIsB2B() != null && !cclient.getIsB2B().isEmpty()) {
					// This is for Bakcell B2B sender name
					from = ConfigurationManager.getConfigurationFromCache("email.from.2");
				} else {
					// This is for Bakcell B2C sender name
					from = ConfigurationManager.getConfigurationFromCache("email.from.1");
				}
				logger.info("FROM VALUE CONFIG " + from);
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.host", ConfigurationManager.getConfigurationFromCache("email.mail.smtp.host"));
		props.put("mail.smtp.port", "25");
		logger.info("HOST VALUE CONFIG " + ConfigurationManager.getConfigurationFromCache("email.mail.smtp.host"));
		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
			message.setSubject(subject);

			// message.setText(text);

			// message.setContent(text, "text/html");

			Multipart mp = new MimeMultipart();
			MimeBodyPart htmlPart = new MimeBodyPart();
			htmlPart.setContent(text, "text/html; charset=UTF-8");
			mp.addBodyPart(htmlPart);
			message.setContent(mp);

			Transport.send(message);

			System.out.println("Done");
			logger.info("RESULT IN TRY EMAIL SEND SUCCESSFULLY");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}

		// end of sendEmail

		resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
		resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
		try {
			logger.info("Response FROM ESB " + Helper.ObjectToJson(resp));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resp;
	}

}
