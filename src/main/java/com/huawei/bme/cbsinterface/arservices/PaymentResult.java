package com.huawei.bme.cbsinterface.arservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.cbs.ar.wsservice.arcommon.BalanceChgInfo;

/**
 * <p>
 * Java class for PaymentResult complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PaymentSerialNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BalanceChgInfo" type="{http://cbs.huawei.com/ar/wsservice/arcommon}BalanceChgInfo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="PaymentBonus" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="FreeUnitItemList" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="FreeUnitID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="FreeUnitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="FreeUnitTypeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="MeasureUnit" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="MeasureUnitName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="BonusAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="BalanceList" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="BalanceType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="BalanceID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="BalanceTypeName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="BonusAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="OutStandingList" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Invoiceno" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="InvoiceID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="BillCycleID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DueDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="OutStandingDetail" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="OutStandingAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="InvoiceDetailID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                             &lt;element name="ChargeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentResult", propOrder = { "paymentSerialNo", "balanceChgInfo", "paymentBonus", "outStandingList" })
public class PaymentResult {
	@XmlElement(name = "PaymentSerialNo", required = true)
	protected String paymentSerialNo;
	@XmlElement(name = "BalanceChgInfo")
	protected List<BalanceChgInfo> balanceChgInfo;
	@XmlElement(name = "PaymentBonus")
	protected PaymentResult.PaymentBonus paymentBonus;
	@XmlElement(name = "OutStandingList")
	protected List<PaymentResult.OutStandingList> outStandingList;

	/**
	 * Gets the value of the paymentSerialNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaymentSerialNo() {
		return paymentSerialNo;
	}

	/**
	 * Sets the value of the paymentSerialNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPaymentSerialNo(String value) {
		this.paymentSerialNo = value;
	}

	/**
	 * Gets the value of the balanceChgInfo property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the balanceChgInfo property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getBalanceChgInfo().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link BalanceChgInfo }
	 * 
	 * 
	 */
	public List<BalanceChgInfo> getBalanceChgInfo() {
		if (balanceChgInfo == null) {
			balanceChgInfo = new ArrayList<BalanceChgInfo>();
		}
		return this.balanceChgInfo;
	}

	/**
	 * Gets the value of the paymentBonus property.
	 * 
	 * @return possible object is {@link PaymentResult.PaymentBonus }
	 * 
	 */
	public PaymentResult.PaymentBonus getPaymentBonus() {
		return paymentBonus;
	}

	/**
	 * Sets the value of the paymentBonus property.
	 * 
	 * @param value
	 *            allowed object is {@link PaymentResult.PaymentBonus }
	 * 
	 */
	public void setPaymentBonus(PaymentResult.PaymentBonus value) {
		this.paymentBonus = value;
	}

	/**
	 * Gets the value of the outStandingList property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the outStandingList property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getOutStandingList().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link PaymentResult.OutStandingList }
	 * 
	 * 
	 */
	public List<PaymentResult.OutStandingList> getOutStandingList() {
		if (outStandingList == null) {
			outStandingList = new ArrayList<PaymentResult.OutStandingList>();
		}
		return this.outStandingList;
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="Invoiceno" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *         &lt;element name="InvoiceID" type="{http://www.w3.org/2001/XMLSchema}long"/>
	 *         &lt;element name="BillCycleID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="DueDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
	 *         &lt;element name="OutStandingDetail" maxOccurs="unbounded">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="OutStandingAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
	 *                   &lt;element name="InvoiceDetailID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
	 *                   &lt;element name="ChargeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "invoiceno", "invoiceID", "billCycleID", "dueDate", "currencyID",
			"outStandingDetail" })
	public static class OutStandingList {
		@XmlElement(name = "Invoiceno", required = true)
		protected String invoiceno;
		@XmlElement(name = "InvoiceID")
		protected long invoiceID;
		@XmlElement(name = "BillCycleID")
		protected String billCycleID;
		@XmlElement(name = "DueDate")
		protected String dueDate;
		@XmlElement(name = "CurrencyID", required = true)
		protected BigInteger currencyID;
		@XmlElement(name = "OutStandingDetail", required = true)
		protected List<PaymentResult.OutStandingList.OutStandingDetail> outStandingDetail;

		/**
		 * Gets the value of the invoiceno property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getInvoiceno() {
			return invoiceno;
		}

		/**
		 * Sets the value of the invoiceno property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setInvoiceno(String value) {
			this.invoiceno = value;
		}

		/**
		 * Gets the value of the invoiceID property.
		 * 
		 */
		public long getInvoiceID() {
			return invoiceID;
		}

		/**
		 * Sets the value of the invoiceID property.
		 * 
		 */
		public void setInvoiceID(long value) {
			this.invoiceID = value;
		}

		/**
		 * Gets the value of the billCycleID property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getBillCycleID() {
			return billCycleID;
		}

		/**
		 * Sets the value of the billCycleID property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setBillCycleID(String value) {
			this.billCycleID = value;
		}

		/**
		 * Gets the value of the dueDate property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getDueDate() {
			return dueDate;
		}

		/**
		 * Sets the value of the dueDate property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setDueDate(String value) {
			this.dueDate = value;
		}

		/**
		 * Gets the value of the currencyID property.
		 * 
		 * @return possible object is {@link BigInteger }
		 * 
		 */
		public BigInteger getCurrencyID() {
			return currencyID;
		}

		/**
		 * Sets the value of the currencyID property.
		 * 
		 * @param value
		 *            allowed object is {@link BigInteger }
		 * 
		 */
		public void setCurrencyID(BigInteger value) {
			this.currencyID = value;
		}

		/**
		 * Gets the value of the outStandingDetail property.
		 * 
		 * <p>
		 * This accessor method returns a reference to the live list, not a
		 * snapshot. Therefore any modification you make to the returned list
		 * will be present inside the JAXB object. This is why there is not a
		 * <CODE>set</CODE> method for the outStandingDetail property.
		 * 
		 * <p>
		 * For example, to add a new item, do as follows:
		 * 
		 * <pre>
		 * getOutStandingDetail().add(newItem);
		 * </pre>
		 * 
		 * 
		 * <p>
		 * Objects of the following type(s) are allowed in the list
		 * {@link PaymentResult.OutStandingList.OutStandingDetail }
		 * 
		 * 
		 */
		public List<PaymentResult.OutStandingList.OutStandingDetail> getOutStandingDetail() {
			if (outStandingDetail == null) {
				outStandingDetail = new ArrayList<PaymentResult.OutStandingList.OutStandingDetail>();
			}
			return this.outStandingDetail;
		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * 
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="OutStandingAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
		 *         &lt;element name="InvoiceDetailID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
		 *         &lt;element name="ChargeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "outStandingAmount", "invoiceDetailID", "chargeCode" })
		public static class OutStandingDetail {
			@XmlElement(name = "OutStandingAmount")
			protected long outStandingAmount;
			@XmlElement(name = "InvoiceDetailID")
			protected Long invoiceDetailID;
			@XmlElement(name = "ChargeCode")
			protected String chargeCode;

			/**
			 * Gets the value of the outStandingAmount property.
			 * 
			 */
			public long getOutStandingAmount() {
				return outStandingAmount;
			}

			/**
			 * Sets the value of the outStandingAmount property.
			 * 
			 */
			public void setOutStandingAmount(long value) {
				this.outStandingAmount = value;
			}

			/**
			 * Gets the value of the invoiceDetailID property.
			 * 
			 * @return possible object is {@link Long }
			 * 
			 */
			public Long getInvoiceDetailID() {
				return invoiceDetailID;
			}

			/**
			 * Sets the value of the invoiceDetailID property.
			 * 
			 * @param value
			 *            allowed object is {@link Long }
			 * 
			 */
			public void setInvoiceDetailID(Long value) {
				this.invoiceDetailID = value;
			}

			/**
			 * Gets the value of the chargeCode property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getChargeCode() {
				return chargeCode;
			}

			/**
			 * Sets the value of the chargeCode property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setChargeCode(String value) {
				this.chargeCode = value;
			}
		}
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="FreeUnitItemList" maxOccurs="unbounded" minOccurs="0">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="FreeUnitID" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                   &lt;element name="FreeUnitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                   &lt;element name="FreeUnitTypeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *                   &lt;element name="MeasureUnit" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                   &lt;element name="MeasureUnitName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *                   &lt;element name="BonusAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
	 *                   &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                   &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="BalanceList" maxOccurs="unbounded" minOccurs="0">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="BalanceType" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                   &lt;element name="BalanceID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *                   &lt;element name="BalanceTypeName" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                   &lt;element name="BonusAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
	 *                   &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
	 *                   &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                   &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "freeUnitItemList", "balanceList" })
	public static class PaymentBonus {
		@XmlElement(name = "FreeUnitItemList")
		protected List<PaymentResult.PaymentBonus.FreeUnitItemList> freeUnitItemList;
		@XmlElement(name = "BalanceList")
		protected List<PaymentResult.PaymentBonus.BalanceList> balanceList;

		/**
		 * Gets the value of the freeUnitItemList property.
		 * 
		 * <p>
		 * This accessor method returns a reference to the live list, not a
		 * snapshot. Therefore any modification you make to the returned list
		 * will be present inside the JAXB object. This is why there is not a
		 * <CODE>set</CODE> method for the freeUnitItemList property.
		 * 
		 * <p>
		 * For example, to add a new item, do as follows:
		 * 
		 * <pre>
		 * getFreeUnitItemList().add(newItem);
		 * </pre>
		 * 
		 * 
		 * <p>
		 * Objects of the following type(s) are allowed in the list
		 * {@link PaymentResult.PaymentBonus.FreeUnitItemList }
		 * 
		 * 
		 */
		public List<PaymentResult.PaymentBonus.FreeUnitItemList> getFreeUnitItemList() {
			if (freeUnitItemList == null) {
				freeUnitItemList = new ArrayList<PaymentResult.PaymentBonus.FreeUnitItemList>();
			}
			return this.freeUnitItemList;
		}

		/**
		 * Gets the value of the balanceList property.
		 * 
		 * <p>
		 * This accessor method returns a reference to the live list, not a
		 * snapshot. Therefore any modification you make to the returned list
		 * will be present inside the JAXB object. This is why there is not a
		 * <CODE>set</CODE> method for the balanceList property.
		 * 
		 * <p>
		 * For example, to add a new item, do as follows:
		 * 
		 * <pre>
		 * getBalanceList().add(newItem);
		 * </pre>
		 * 
		 * 
		 * <p>
		 * Objects of the following type(s) are allowed in the list
		 * {@link PaymentResult.PaymentBonus.BalanceList }
		 * 
		 * 
		 */
		public List<PaymentResult.PaymentBonus.BalanceList> getBalanceList() {
			if (balanceList == null) {
				balanceList = new ArrayList<PaymentResult.PaymentBonus.BalanceList>();
			}
			return this.balanceList;
		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * 
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="BalanceType" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *         &lt;element name="BalanceID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
		 *         &lt;element name="BalanceTypeName" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *         &lt;element name="BonusAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
		 *         &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
		 *         &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *         &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "balanceType", "balanceID", "balanceTypeName", "bonusAmt", "currencyID",
				"effectiveTime", "expireTime" })
		public static class BalanceList {
			@XmlElement(name = "BalanceType", required = true)
			protected String balanceType;
			@XmlElement(name = "BalanceID")
			protected String balanceID;
			@XmlElement(name = "BalanceTypeName", required = true)
			protected String balanceTypeName;
			@XmlElement(name = "BonusAmt")
			protected long bonusAmt;
			@XmlElement(name = "CurrencyID", required = true)
			protected BigInteger currencyID;
			@XmlElement(name = "EffectiveTime", required = true)
			protected String effectiveTime;
			@XmlElement(name = "ExpireTime", required = true)
			protected String expireTime;

			/**
			 * Gets the value of the balanceType property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getBalanceType() {
				return balanceType;
			}

			/**
			 * Sets the value of the balanceType property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setBalanceType(String value) {
				this.balanceType = value;
			}

			/**
			 * Gets the value of the balanceID property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getBalanceID() {
				return balanceID;
			}

			/**
			 * Sets the value of the balanceID property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setBalanceID(String value) {
				this.balanceID = value;
			}

			/**
			 * Gets the value of the balanceTypeName property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getBalanceTypeName() {
				return balanceTypeName;
			}

			/**
			 * Sets the value of the balanceTypeName property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setBalanceTypeName(String value) {
				this.balanceTypeName = value;
			}

			/**
			 * Gets the value of the bonusAmt property.
			 * 
			 */
			public long getBonusAmt() {
				return bonusAmt;
			}

			/**
			 * Sets the value of the bonusAmt property.
			 * 
			 */
			public void setBonusAmt(long value) {
				this.bonusAmt = value;
			}

			/**
			 * Gets the value of the currencyID property.
			 * 
			 * @return possible object is {@link BigInteger }
			 * 
			 */
			public BigInteger getCurrencyID() {
				return currencyID;
			}

			/**
			 * Sets the value of the currencyID property.
			 * 
			 * @param value
			 *            allowed object is {@link BigInteger }
			 * 
			 */
			public void setCurrencyID(BigInteger value) {
				this.currencyID = value;
			}

			/**
			 * Gets the value of the effectiveTime property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getEffectiveTime() {
				return effectiveTime;
			}

			/**
			 * Sets the value of the effectiveTime property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setEffectiveTime(String value) {
				this.effectiveTime = value;
			}

			/**
			 * Gets the value of the expireTime property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getExpireTime() {
				return expireTime;
			}

			/**
			 * Sets the value of the expireTime property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setExpireTime(String value) {
				this.expireTime = value;
			}
		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * 
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="FreeUnitID" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *         &lt;element name="FreeUnitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *         &lt;element name="FreeUnitTypeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
		 *         &lt;element name="MeasureUnit" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *         &lt;element name="MeasureUnitName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
		 *         &lt;element name="BonusAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
		 *         &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *         &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "freeUnitID", "freeUnitType", "freeUnitTypeName", "measureUnit",
				"measureUnitName", "bonusAmt", "effectiveTime", "expireTime" })
		public static class FreeUnitItemList {
			@XmlElement(name = "FreeUnitID", required = true)
			protected String freeUnitID;
			@XmlElement(name = "FreeUnitType", required = true)
			protected String freeUnitType;
			@XmlElement(name = "FreeUnitTypeName")
			protected String freeUnitTypeName;
			@XmlElement(name = "MeasureUnit", required = true)
			protected String measureUnit;
			@XmlElement(name = "MeasureUnitName")
			protected String measureUnitName;
			@XmlElement(name = "BonusAmt", required = true, type = Long.class, nillable = true)
			protected Long bonusAmt;
			@XmlElement(name = "EffectiveTime", required = true)
			protected String effectiveTime;
			@XmlElement(name = "ExpireTime", required = true)
			protected String expireTime;

			/**
			 * Gets the value of the freeUnitID property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getFreeUnitID() {
				return freeUnitID;
			}

			/**
			 * Sets the value of the freeUnitID property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setFreeUnitID(String value) {
				this.freeUnitID = value;
			}

			/**
			 * Gets the value of the freeUnitType property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getFreeUnitType() {
				return freeUnitType;
			}

			/**
			 * Sets the value of the freeUnitType property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setFreeUnitType(String value) {
				this.freeUnitType = value;
			}

			/**
			 * Gets the value of the freeUnitTypeName property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getFreeUnitTypeName() {
				return freeUnitTypeName;
			}

			/**
			 * Sets the value of the freeUnitTypeName property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setFreeUnitTypeName(String value) {
				this.freeUnitTypeName = value;
			}

			/**
			 * Gets the value of the measureUnit property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getMeasureUnit() {
				return measureUnit;
			}

			/**
			 * Sets the value of the measureUnit property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setMeasureUnit(String value) {
				this.measureUnit = value;
			}

			/**
			 * Gets the value of the measureUnitName property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getMeasureUnitName() {
				return measureUnitName;
			}

			/**
			 * Sets the value of the measureUnitName property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setMeasureUnitName(String value) {
				this.measureUnitName = value;
			}

			/**
			 * Gets the value of the bonusAmt property.
			 * 
			 * @return possible object is {@link Long }
			 * 
			 */
			public Long getBonusAmt() {
				return bonusAmt;
			}

			/**
			 * Sets the value of the bonusAmt property.
			 * 
			 * @param value
			 *            allowed object is {@link Long }
			 * 
			 */
			public void setBonusAmt(Long value) {
				this.bonusAmt = value;
			}

			/**
			 * Gets the value of the effectiveTime property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getEffectiveTime() {
				return effectiveTime;
			}

			/**
			 * Sets the value of the effectiveTime property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setEffectiveTime(String value) {
				this.effectiveTime = value;
			}

			/**
			 * Gets the value of the expireTime property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getExpireTime() {
				return expireTime;
			}

			/**
			 * Sets the value of the expireTime property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setExpireTime(String value) {
				this.expireTime = value;
			}
		}
	}
}
