package com.huawei.bme.cbsinterface.arservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.cbs.ar.wsservice.arcommon.BankInfo;

/**
 * <p>
 * Java class for QueryInvoicePaymentResult complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="QueryInvoicePaymentResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InvoicePaymentInfo" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="PaymentTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="PaymentAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="ApplyAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="TransID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="TransType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ExtPayType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="PaymentMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CardInfo" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="CardPinNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="CardSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="BankInfo" type="{http://cbs.huawei.com/ar/wsservice/arcommon}BankInfo" minOccurs="0"/>
 *                   &lt;element name="PayChannelID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="AccessMode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="OperID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                   &lt;element name="DeptID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryInvoicePaymentResult", propOrder = { "invoicePaymentInfo" })
public class QueryInvoicePaymentResult {
	@XmlElement(name = "InvoicePaymentInfo")
	protected List<QueryInvoicePaymentResult.InvoicePaymentInfo> invoicePaymentInfo;

	/**
	 * Gets the value of the invoicePaymentInfo property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the invoicePaymentInfo property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getInvoicePaymentInfo().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link QueryInvoicePaymentResult.InvoicePaymentInfo }
	 * 
	 * 
	 */
	public List<QueryInvoicePaymentResult.InvoicePaymentInfo> getInvoicePaymentInfo() {
		if (invoicePaymentInfo == null) {
			invoicePaymentInfo = new ArrayList<QueryInvoicePaymentResult.InvoicePaymentInfo>();
		}
		return this.invoicePaymentInfo;
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="PaymentTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *         &lt;element name="PaymentAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
	 *         &lt;element name="ApplyAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
	 *         &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
	 *         &lt;element name="TransID" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *         &lt;element name="TransType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="ExtPayType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="PaymentMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="CardInfo" minOccurs="0">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="CardPinNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *                   &lt;element name="CardSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="BankInfo" type="{http://cbs.huawei.com/ar/wsservice/arcommon}BankInfo" minOccurs="0"/>
	 *         &lt;element name="PayChannelID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="AccessMode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *         &lt;element name="OperID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
	 *         &lt;element name="DeptID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "paymentTime", "paymentAmount", "applyAmount", "currencyID", "transID",
			"transType", "extPayType", "paymentMethod", "cardInfo", "bankInfo", "payChannelID", "accessMode", "status",
			"operID", "deptID" })
	public static class InvoicePaymentInfo {
		@XmlElement(name = "PaymentTime", required = true)
		protected String paymentTime;
		@XmlElement(name = "PaymentAmount")
		protected long paymentAmount;
		@XmlElement(name = "ApplyAmount")
		protected long applyAmount;
		@XmlElement(name = "CurrencyID", required = true)
		protected BigInteger currencyID;
		@XmlElement(name = "TransID", required = true)
		protected String transID;
		@XmlElement(name = "TransType")
		protected String transType;
		@XmlElement(name = "ExtPayType")
		protected String extPayType;
		@XmlElement(name = "PaymentMethod")
		protected String paymentMethod;
		@XmlElement(name = "CardInfo")
		protected QueryInvoicePaymentResult.InvoicePaymentInfo.CardInfo cardInfo;
		@XmlElement(name = "BankInfo")
		protected BankInfo bankInfo;
		@XmlElement(name = "PayChannelID")
		protected String payChannelID;
		@XmlElement(name = "AccessMode")
		protected String accessMode;
		@XmlElement(name = "Status", required = true)
		protected String status;
		@XmlElement(name = "OperID")
		protected Long operID;
		@XmlElement(name = "DeptID")
		protected Long deptID;

		/**
		 * Gets the value of the paymentTime property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getPaymentTime() {
			return paymentTime;
		}

		/**
		 * Sets the value of the paymentTime property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setPaymentTime(String value) {
			this.paymentTime = value;
		}

		/**
		 * Gets the value of the paymentAmount property.
		 * 
		 */
		public long getPaymentAmount() {
			return paymentAmount;
		}

		/**
		 * Sets the value of the paymentAmount property.
		 * 
		 */
		public void setPaymentAmount(long value) {
			this.paymentAmount = value;
		}

		/**
		 * Gets the value of the applyAmount property.
		 * 
		 */
		public long getApplyAmount() {
			return applyAmount;
		}

		/**
		 * Sets the value of the applyAmount property.
		 * 
		 */
		public void setApplyAmount(long value) {
			this.applyAmount = value;
		}

		/**
		 * Gets the value of the currencyID property.
		 * 
		 * @return possible object is {@link BigInteger }
		 * 
		 */
		public BigInteger getCurrencyID() {
			return currencyID;
		}

		/**
		 * Sets the value of the currencyID property.
		 * 
		 * @param value
		 *            allowed object is {@link BigInteger }
		 * 
		 */
		public void setCurrencyID(BigInteger value) {
			this.currencyID = value;
		}

		/**
		 * Gets the value of the transID property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getTransID() {
			return transID;
		}

		/**
		 * Sets the value of the transID property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setTransID(String value) {
			this.transID = value;
		}

		/**
		 * Gets the value of the transType property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getTransType() {
			return transType;
		}

		/**
		 * Sets the value of the transType property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setTransType(String value) {
			this.transType = value;
		}

		/**
		 * Gets the value of the extPayType property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getExtPayType() {
			return extPayType;
		}

		/**
		 * Sets the value of the extPayType property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setExtPayType(String value) {
			this.extPayType = value;
		}

		/**
		 * Gets the value of the paymentMethod property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getPaymentMethod() {
			return paymentMethod;
		}

		/**
		 * Sets the value of the paymentMethod property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setPaymentMethod(String value) {
			this.paymentMethod = value;
		}

		/**
		 * Gets the value of the cardInfo property.
		 * 
		 * @return possible object is
		 *         {@link QueryInvoicePaymentResult.InvoicePaymentInfo.CardInfo }
		 * 
		 */
		public QueryInvoicePaymentResult.InvoicePaymentInfo.CardInfo getCardInfo() {
			return cardInfo;
		}

		/**
		 * Sets the value of the cardInfo property.
		 * 
		 * @param value
		 *            allowed object is
		 *            {@link QueryInvoicePaymentResult.InvoicePaymentInfo.CardInfo }
		 * 
		 */
		public void setCardInfo(QueryInvoicePaymentResult.InvoicePaymentInfo.CardInfo value) {
			this.cardInfo = value;
		}

		/**
		 * Gets the value of the bankInfo property.
		 * 
		 * @return possible object is {@link BankInfo }
		 * 
		 */
		public BankInfo getBankInfo() {
			return bankInfo;
		}

		/**
		 * Sets the value of the bankInfo property.
		 * 
		 * @param value
		 *            allowed object is {@link BankInfo }
		 * 
		 */
		public void setBankInfo(BankInfo value) {
			this.bankInfo = value;
		}

		/**
		 * Gets the value of the payChannelID property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getPayChannelID() {
			return payChannelID;
		}

		/**
		 * Sets the value of the payChannelID property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setPayChannelID(String value) {
			this.payChannelID = value;
		}

		/**
		 * Gets the value of the accessMode property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getAccessMode() {
			return accessMode;
		}

		/**
		 * Sets the value of the accessMode property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setAccessMode(String value) {
			this.accessMode = value;
		}

		/**
		 * Gets the value of the status property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getStatus() {
			return status;
		}

		/**
		 * Sets the value of the status property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setStatus(String value) {
			this.status = value;
		}

		/**
		 * Gets the value of the operID property.
		 * 
		 * @return possible object is {@link Long }
		 * 
		 */
		public Long getOperID() {
			return operID;
		}

		/**
		 * Sets the value of the operID property.
		 * 
		 * @param value
		 *            allowed object is {@link Long }
		 * 
		 */
		public void setOperID(Long value) {
			this.operID = value;
		}

		/**
		 * Gets the value of the deptID property.
		 * 
		 * @return possible object is {@link Long }
		 * 
		 */
		public Long getDeptID() {
			return deptID;
		}

		/**
		 * Sets the value of the deptID property.
		 * 
		 * @param value
		 *            allowed object is {@link Long }
		 * 
		 */
		public void setDeptID(Long value) {
			this.deptID = value;
		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * 
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="CardPinNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
		 *         &lt;element name="CardSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "cardPinNumber", "cardSequence" })
		public static class CardInfo {
			@XmlElement(name = "CardPinNumber")
			protected String cardPinNumber;
			@XmlElement(name = "CardSequence")
			protected String cardSequence;

			/**
			 * Gets the value of the cardPinNumber property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getCardPinNumber() {
				return cardPinNumber;
			}

			/**
			 * Sets the value of the cardPinNumber property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setCardPinNumber(String value) {
				this.cardPinNumber = value;
			}

			/**
			 * Gets the value of the cardSequence property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getCardSequence() {
				return cardSequence;
			}

			/**
			 * Sets the value of the cardSequence property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setCardSequence(String value) {
				this.cardSequence = value;
			}
		}
	}
}
