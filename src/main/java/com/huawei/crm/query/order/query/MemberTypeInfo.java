
package com.huawei.crm.query.order.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MemberTypeInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MemberTypeInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="TypeId" type="{http://crm.huawei.com/basetype/}TypeId"/>
 *         &lt;element name="MemTypeCode" type="{http://crm.huawei.com/basetype/}MemTypeCode"/>
 *         &lt;element name="MemTypeName" type="{http://crm.huawei.com/basetype/}MemTypeName"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MemberTypeInfo", propOrder = {

})
public class MemberTypeInfo {

    @XmlElement(name = "TypeId", required = true)
    protected String typeId;
    @XmlElement(name = "MemTypeCode", required = true)
    protected String memTypeCode;
    @XmlElement(name = "MemTypeName", required = true)
    protected String memTypeName;

    /**
     * Gets the value of the typeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeId() {
        return typeId;
    }

    /**
     * Sets the value of the typeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeId(String value) {
        this.typeId = value;
    }

    /**
     * Gets the value of the memTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemTypeCode() {
        return memTypeCode;
    }

    /**
     * Sets the value of the memTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemTypeCode(String value) {
        this.memTypeCode = value;
    }

    /**
     * Gets the value of the memTypeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemTypeName() {
        return memTypeName;
    }

    /**
     * Sets the value of the memTypeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemTypeName(String value) {
        this.memTypeName = value;
    }

}
