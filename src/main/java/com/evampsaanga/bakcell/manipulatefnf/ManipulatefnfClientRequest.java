package com.evampsaanga.bakcell.manipulatefnf;

import com.evampsaanga.bakcell.requestheaders.BaseRequest;

public class ManipulatefnfClientRequest extends BaseRequest {
	private String actionType = "";
	private String primaryOfferingId = "";
	private String offeringId = "";

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	/**
	 * @return the primaryOfferingId
	 */
	public String getPrimaryOfferingId() {
		return primaryOfferingId;
	}

	/**
	 * @param primaryOfferingId
	 *            the primaryOfferingId to set
	 */
	public void setPrimaryOfferingId(String primaryOfferingId) {
		this.primaryOfferingId = primaryOfferingId;
	}

	private String fnfNumber = "";

	/**
	 * @return the fnfNumber
	 */
	public String getFnfNumber() {
		return fnfNumber;
	}

	/**
	 * @param fnfNumber
	 *            the fnfNumber to set
	 */
	public void setFnfNumber(String fnfNumber) {
		this.fnfNumber = fnfNumber;
	}

	/**
	 * @return the actionType
	 */
	public String getActionType() {
		return actionType;
	}

	/**
	 * @param actionType
	 *            the actionType to set
	 */
	public void setActionType(String actionType) {
		this.actionType = actionType;
	}
}
