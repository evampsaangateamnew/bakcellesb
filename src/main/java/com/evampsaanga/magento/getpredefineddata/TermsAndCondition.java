package com.evampsaanga.magento.getpredefineddata;

public class TermsAndCondition {
	private TermsAndCond[] klass;
	private TermsAndCond[] cin;

	/**
	 * @return the klass
	 */
	public TermsAndCond[] getKlass() {
		return klass;
	}

	/**
	 * @param klass
	 *            the klass to set
	 */
	public void setKlass(TermsAndCond[] klass) {
		this.klass = klass;
	}

	/**
	 * @return the cin
	 */
	public TermsAndCond[] getCin() {
		return cin;
	}

	/**
	 * @param cin
	 *            the cin to set
	 */
	public void setCin(TermsAndCond[] cin) {
		this.cin = cin;
	}
}
