package com.nsn.devicemanagement.wsdl.common._2;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for BackupContent.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * <p>
 * 
 * <pre>
 * &lt;simpleType name="BackupContent">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CALENDAR"/>
 *     &lt;enumeration value="CONTACTS"/>
 *     &lt;enumeration value="NOTES"/>
 *     &lt;enumeration value="FILES_AND_FOLDERS"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "BackupContent")
@XmlEnum
public enum BackupContent {
	CALENDAR, CONTACTS, NOTES, FILES_AND_FOLDERS;
	public String value() {
		return name();
	}

	public static BackupContent fromValue(String v) {
		return valueOf(v);
	}
}
