package com.evampsaanga.gethomepage;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "availableBalanceCorporateValue", "availableBalanceIndividualValue", "availableCreditLabel",
		"balanceCorporateValue", "balanceIndividualValue", "balanceLabel", "corporateLabel",
		"currentCreditCorporateValue", "currentCreditIndividualValue", "currentCreditLabel", "individualLabel",
		"outstandingIndividualDebt", "outstandingIndividualDebtLabel", "template" })
public class Postpaid {
	@JsonProperty("availableBalanceCorporateValue")
	private String availableBalanceCorporateValue = "0";
	@JsonProperty("availableBalanceIndividualValue")
	private String availableBalanceIndividualValue = "0";
	@JsonProperty("balanceCorporateValue")
	private String balanceCorporateValue = "0";
	@JsonProperty("balanceIndividualValue")
	private String balanceIndividualValue = "0";
	@JsonProperty("currentCreditCorporateValue")
	private String currentCreditCorporateValue = "0";
	@JsonProperty("currentCreditIndividualValue")
	private String currentCreditIndividualValue = "0";
	@JsonProperty("outstandingIndividualDebt")
	private String outstandingIndividualDebt = "0";
	@JsonProperty("template")
	private int template = -1;

	public Postpaid() {
		super();
	}

	public Postpaid(String availableBalanceCorporateValue, String availableBalanceIndividualValue,
			String balanceCorporateValue, String balanceIndividualValue, String currentCreditCorporateValue,
			String currentCreditIndividualValue, String outstandingIndividualDebt, int template) {
		super();
		this.availableBalanceCorporateValue = availableBalanceCorporateValue;
		this.availableBalanceIndividualValue = availableBalanceIndividualValue;
		this.balanceCorporateValue = balanceCorporateValue;
		this.balanceIndividualValue = balanceIndividualValue;
		this.currentCreditCorporateValue = currentCreditCorporateValue;
		this.currentCreditIndividualValue = currentCreditIndividualValue;
		this.outstandingIndividualDebt = outstandingIndividualDebt;
		this.template = template;
	}

	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("availableBalanceCorporateValue")
	public String getAvailableBalanceCorporateValue() {
		return availableBalanceCorporateValue;
	}

	@JsonProperty("availableBalanceCorporateValue")
	public void setAvailableBalanceCorporateValue(String availableBalanceCorporateValue) {
		this.availableBalanceCorporateValue = availableBalanceCorporateValue;
	}

	@JsonProperty("availableBalanceIndividualValue")
	public String getAvailableBalanceIndividualValue() {
		return availableBalanceIndividualValue;
	}

	@JsonProperty("availableBalanceIndividualValue")
	public void setAvailableBalanceIndividualValue(String availableBalanceIndividualValue) {
		this.availableBalanceIndividualValue = availableBalanceIndividualValue;
	}

	@JsonProperty("balanceCorporateValue")
	public String getBalanceCorporateValue() {
		return balanceCorporateValue;
	}

	@JsonProperty("balanceCorporateValue")
	public void setBalanceCorporateValue(String balanceCorporateValue) {
		this.balanceCorporateValue = balanceCorporateValue;
	}

	@JsonProperty("balanceIndividualValue")
	public String getBalanceIndividualValue() {
		return balanceIndividualValue;
	}

	@JsonProperty("balanceIndividualValue")
	public void setBalanceIndividualValue(String balanceIndividualValue) {
		this.balanceIndividualValue = balanceIndividualValue;
	}

	@JsonProperty("currentCreditCorporateValue")
	public String getCurrentCreditCorporateValue() {
		return currentCreditCorporateValue;
	}

	@JsonProperty("currentCreditCorporateValue")
	public void setCurrentCreditCorporateValue(String currentCreditCorporateValue) {
		this.currentCreditCorporateValue = currentCreditCorporateValue;
	}

	@JsonProperty("currentCreditIndividualValue")
	public String getCurrentCreditIndividualValue() {
		return currentCreditIndividualValue;
	}

	@JsonProperty("currentCreditIndividualValue")
	public void setCurrentCreditIndividualValue(String currentCreditIndividualValue) {
		this.currentCreditIndividualValue = currentCreditIndividualValue;
	}

	@JsonProperty("outstandingIndividualDebt")
	public String getOutstandingIndividualDept() {
		return outstandingIndividualDebt;
	}

	@JsonProperty("outstandingIndividualDebt")
	public void setOutstandingIndividualDept(String outstandingIndividualDept) {
		this.outstandingIndividualDebt = outstandingIndividualDept;
	}

	@JsonProperty("template")
	public int getTemplate() {
		return template;
	}

	@JsonProperty("template")
	public void setTemplate(int template) {
		this.template = template;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString() {
		return "Postpaid [availableBalanceCorporateValue=" + availableBalanceCorporateValue
				+ ", availableBalanceIndividualValue=" + availableBalanceIndividualValue + ", balanceCorporateValue="
				+ balanceCorporateValue + ", balanceIndividualValue=" + balanceIndividualValue
				+ ", currentCreditCorporateValue=" + currentCreditCorporateValue + ", currentCreditIndividualValue="
				+ currentCreditIndividualValue + ", outstandingIndividualDebt=" + outstandingIndividualDebt
				+ ", template=" + template + ", additionalProperties=" + additionalProperties + "]";
	}
}
