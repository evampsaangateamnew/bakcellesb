package com.evampsaanga.bakcell.notificationadmin;

public class ReportData {
	private String lastModified;
	private String fileName;
	private String file;

	public String getLastModified() {
		return lastModified;
	}

	public void setLastModified(String lastModified) {
		this.lastModified = lastModified;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	@Override
	public String toString() {
		return "ReportData [lastModified=" + lastModified + ", fileName=" + fileName + ", filefile=" + file + "]";
	}

}
