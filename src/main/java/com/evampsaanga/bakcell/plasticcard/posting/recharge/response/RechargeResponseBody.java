package com.evampsaanga.bakcell.plasticcard.posting.recharge.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "body")
@XmlAccessorType(XmlAccessType.FIELD)
public class RechargeResponseBody {
	public RechargeResponse paymentresponse;

	public RechargeResponse getPaymentresponse() {
		return paymentresponse;
	}

	public void setPaymentresponse(RechargeResponse paymentresponse) {
		this.paymentresponse = paymentresponse;
	}

	@Override
	public String toString() {
		return "RechargeResponseBody [paymentresponse=" + paymentresponse + "]";
	}

}
