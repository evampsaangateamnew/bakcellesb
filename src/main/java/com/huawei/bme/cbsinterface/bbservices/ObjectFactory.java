package com.huawei.bme.cbsinterface.bbservices;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the com.huawei.bme.cbsinterface.bbservices
 * package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {
	private final static QName _QueryFreeUnitResultFreeUnitItemFreeUnitItemDetailInitialOriginPlanID_QNAME = new QName(
			"http://www.huawei.com/bme/cbsinterface/bbservices", "PlanID");
	private final static QName _QueryFreeUnitResultFreeUnitItemFreeUnitItemDetailFreeUnitOriginFileID_QNAME = new QName(
			"http://www.huawei.com/bme/cbsinterface/bbservices", "FileID");
	private final static QName _QueryFreeUnitResultFreeUnitItemFreeUnitItemDetailFreeUnitOriginFreeUnitInstanceID_QNAME = new QName(
			"http://www.huawei.com/bme/cbsinterface/bbservices", "FreeUnitInstanceID");

	/**
	 * Create a new ObjectFactory that can be used to create new instances of
	 * schema derived classes for package:
	 * com.huawei.bme.cbsinterface.bbservices
	 * 
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link QueryFreeUnitResult }
	 * 
	 */
	public QueryFreeUnitResult createQueryFreeUnitResult() {
		return new QueryFreeUnitResult();
	}

	/**
	 * Create an instance of {@link QueryFreeUnitResult.ShareUsageList }
	 * 
	 */
	public QueryFreeUnitResult.ShareUsageList createQueryFreeUnitResultShareUsageList() {
		return new QueryFreeUnitResult.ShareUsageList();
	}

	/**
	 * Create an instance of {@link QueryFreeUnitResult.FreeUnitItem }
	 * 
	 */
	public QueryFreeUnitResult.FreeUnitItem createQueryFreeUnitResultFreeUnitItem() {
		return new QueryFreeUnitResult.FreeUnitItem();
	}

	/**
	 * Create an instance of
	 * {@link QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail }
	 * 
	 */
	public QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail createQueryFreeUnitResultFreeUnitItemFreeUnitItemDetail() {
		return new QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail();
	}

	/**
	 * Create an instance of
	 * {@link QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.InitialOrigin }
	 * 
	 */
	public QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.InitialOrigin createQueryFreeUnitResultFreeUnitItemFreeUnitItemDetailInitialOrigin() {
		return new QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.InitialOrigin();
	}

	/**
	 * Create an instance of
	 * {@link QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.FreeUnitOrigin }
	 * 
	 */
	public QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.FreeUnitOrigin createQueryFreeUnitResultFreeUnitItemFreeUnitItemDetailFreeUnitOrigin() {
		return new QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.FreeUnitOrigin();
	}

	/**
	 * Create an instance of {@link QueryFreeUnitRequest }
	 * 
	 */
	public QueryFreeUnitRequest createQueryFreeUnitRequest() {
		return new QueryFreeUnitRequest();
	}

	/**
	 * Create an instance of {@link QueryFreeUnitRequestMsg }
	 * 
	 */
	public QueryFreeUnitRequestMsg createQueryFreeUnitRequestMsg() {
		return new QueryFreeUnitRequestMsg();
	}

	/**
	 * Create an instance of {@link QueryFreeUnitResultMsg }
	 * 
	 */
	public QueryFreeUnitResultMsg createQueryFreeUnitResultMsg() {
		return new QueryFreeUnitResultMsg();
	}

	/**
	 * Create an instance of
	 * {@link QueryFreeUnitResult.ShareUsageList.OfferingKey }
	 * 
	 */
	public QueryFreeUnitResult.ShareUsageList.OfferingKey createQueryFreeUnitResultShareUsageListOfferingKey() {
		return new QueryFreeUnitResult.ShareUsageList.OfferingKey();
	}

	/**
	 * Create an instance of
	 * {@link QueryFreeUnitResult.FreeUnitItem.MemberUsageList }
	 * 
	 */
	public QueryFreeUnitResult.FreeUnitItem.MemberUsageList createQueryFreeUnitResultFreeUnitItemMemberUsageList() {
		return new QueryFreeUnitResult.FreeUnitItem.MemberUsageList();
	}

	/**
	 * Create an instance of
	 * {@link QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.InitialOrigin.OfferingKey }
	 * 
	 */
	public QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.InitialOrigin.OfferingKey createQueryFreeUnitResultFreeUnitItemFreeUnitItemDetailInitialOriginOfferingKey() {
		return new QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.InitialOrigin.OfferingKey();
	}

	/**
	 * Create an instance of
	 * {@link QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.FreeUnitOrigin.OfferingKey }
	 * 
	 */
	public QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.FreeUnitOrigin.OfferingKey createQueryFreeUnitResultFreeUnitItemFreeUnitItemDetailFreeUnitOriginOfferingKey() {
		return new QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.FreeUnitOrigin.OfferingKey();
	}

	/**
	 * Create an instance of {@link QueryFreeUnitRequest.QueryObj }
	 * 
	 */
	public QueryFreeUnitRequest.QueryObj createQueryFreeUnitRequestQueryObj() {
		return new QueryFreeUnitRequest.QueryObj();
	}

	/**
	 * Create an instance of {@link QueryFreeUnitRequest.OfferingKey }
	 * 
	 */
	public QueryFreeUnitRequest.OfferingKey createQueryFreeUnitRequestOfferingKey() {
		return new QueryFreeUnitRequest.OfferingKey();
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Long
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://www.huawei.com/bme/cbsinterface/bbservices", name = "PlanID", scope = QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.InitialOrigin.class)
	public JAXBElement<Long> createQueryFreeUnitResultFreeUnitItemFreeUnitItemDetailInitialOriginPlanID(Long value) {
		return new JAXBElement<Long>(_QueryFreeUnitResultFreeUnitItemFreeUnitItemDetailInitialOriginPlanID_QNAME,
				Long.class, QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.InitialOrigin.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Long
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://www.huawei.com/bme/cbsinterface/bbservices", name = "PlanID", scope = QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.FreeUnitOrigin.class)
	public JAXBElement<Long> createQueryFreeUnitResultFreeUnitItemFreeUnitItemDetailFreeUnitOriginPlanID(Long value) {
		return new JAXBElement<Long>(_QueryFreeUnitResultFreeUnitItemFreeUnitItemDetailInitialOriginPlanID_QNAME,
				Long.class, QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.FreeUnitOrigin.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Long
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://www.huawei.com/bme/cbsinterface/bbservices", name = "FileID", scope = QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.FreeUnitOrigin.class)
	public JAXBElement<Long> createQueryFreeUnitResultFreeUnitItemFreeUnitItemDetailFreeUnitOriginFileID(Long value) {
		return new JAXBElement<Long>(_QueryFreeUnitResultFreeUnitItemFreeUnitItemDetailFreeUnitOriginFileID_QNAME,
				Long.class, QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.FreeUnitOrigin.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Long
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://www.huawei.com/bme/cbsinterface/bbservices", name = "FreeUnitInstanceID", scope = QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.FreeUnitOrigin.class)
	public JAXBElement<Long> createQueryFreeUnitResultFreeUnitItemFreeUnitItemDetailFreeUnitOriginFreeUnitInstanceID(
			Long value) {
		return new JAXBElement<Long>(
				_QueryFreeUnitResultFreeUnitItemFreeUnitItemDetailFreeUnitOriginFreeUnitInstanceID_QNAME, Long.class,
				QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.FreeUnitOrigin.class, value);
	}
}
