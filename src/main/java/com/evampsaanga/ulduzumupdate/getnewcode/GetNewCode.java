package com.evampsaanga.ulduzumupdate.getnewcode;

public class GetNewCode {
	private String code;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "GetNewCode [code=" + code + "]";
	}

}
