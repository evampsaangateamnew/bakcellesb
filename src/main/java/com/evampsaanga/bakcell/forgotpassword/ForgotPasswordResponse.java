package com.evampsaanga.bakcell.forgotpassword;

import com.evampsaanga.bakcell.responseheaders.BaseResponse;

public class ForgotPasswordResponse extends BaseResponse {
	private String channel;
   private String entityId;
   
	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

}
