package com.huawei.bme.cbsinterface.bcservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for QueryCorporateInstallmentResult complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="QueryCorporateInstallmentResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InstallmentList" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ServiceNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ContractID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ContractStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="TotalAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="PaidAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="OutstandingAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryCorporateInstallmentResult", propOrder = { "installmentList" })
public class QueryCorporateInstallmentResult {
	@XmlElement(name = "InstallmentList")
	protected List<QueryCorporateInstallmentResult.InstallmentList> installmentList;

	/**
	 * Gets the value of the installmentList property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the installmentList property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getInstallmentList().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link QueryCorporateInstallmentResult.InstallmentList }
	 * 
	 * 
	 */
	public List<QueryCorporateInstallmentResult.InstallmentList> getInstallmentList() {
		if (installmentList == null) {
			installmentList = new ArrayList<QueryCorporateInstallmentResult.InstallmentList>();
		}
		return this.installmentList;
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="ServiceNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *         &lt;element name="ContractID" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *         &lt;element name="ContractStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *         &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
	 *         &lt;element name="TotalAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
	 *         &lt;element name="PaidAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
	 *         &lt;element name="OutstandingAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "serviceNumber", "contractID", "contractStatus", "currencyID", "totalAmount",
			"paidAmount", "outstandingAmount" })
	public static class InstallmentList {
		@XmlElement(name = "ServiceNumber", required = true)
		protected String serviceNumber;
		@XmlElement(name = "ContractID", required = true)
		protected String contractID;
		@XmlElement(name = "ContractStatus", required = true)
		protected String contractStatus;
		@XmlElement(name = "CurrencyID", required = true)
		protected BigInteger currencyID;
		@XmlElement(name = "TotalAmount")
		protected long totalAmount;
		@XmlElement(name = "PaidAmount")
		protected long paidAmount;
		@XmlElement(name = "OutstandingAmount")
		protected long outstandingAmount;

		/**
		 * Gets the value of the serviceNumber property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getServiceNumber() {
			return serviceNumber;
		}

		/**
		 * Sets the value of the serviceNumber property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setServiceNumber(String value) {
			this.serviceNumber = value;
		}

		/**
		 * Gets the value of the contractID property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getContractID() {
			return contractID;
		}

		/**
		 * Sets the value of the contractID property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setContractID(String value) {
			this.contractID = value;
		}

		/**
		 * Gets the value of the contractStatus property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getContractStatus() {
			return contractStatus;
		}

		/**
		 * Sets the value of the contractStatus property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setContractStatus(String value) {
			this.contractStatus = value;
		}

		/**
		 * Gets the value of the currencyID property.
		 * 
		 * @return possible object is {@link BigInteger }
		 * 
		 */
		public BigInteger getCurrencyID() {
			return currencyID;
		}

		/**
		 * Sets the value of the currencyID property.
		 * 
		 * @param value
		 *            allowed object is {@link BigInteger }
		 * 
		 */
		public void setCurrencyID(BigInteger value) {
			this.currencyID = value;
		}

		/**
		 * Gets the value of the totalAmount property.
		 * 
		 */
		public long getTotalAmount() {
			return totalAmount;
		}

		/**
		 * Sets the value of the totalAmount property.
		 * 
		 */
		public void setTotalAmount(long value) {
			this.totalAmount = value;
		}

		/**
		 * Gets the value of the paidAmount property.
		 * 
		 */
		public long getPaidAmount() {
			return paidAmount;
		}

		/**
		 * Sets the value of the paidAmount property.
		 * 
		 */
		public void setPaidAmount(long value) {
			this.paidAmount = value;
		}

		/**
		 * Gets the value of the outstandingAmount property.
		 * 
		 */
		public long getOutstandingAmount() {
			return outstandingAmount;
		}

		/**
		 * Sets the value of the outstandingAmount property.
		 * 
		 */
		public void setOutstandingAmount(long value) {
			this.outstandingAmount = value;
		}
	}
}
