package com.evampsaanga.services;

import com.evampsaanga.developer.utils.SoapHandlerService;
import com.huawei.bss.soaif._interface.subscriberservice.SubscriberInterfaces;
import com.huawei.bss.soaif._interface.subscriberservice.SubscriberServices;

public class SubscriberService {
	private static SubscriberInterfaces crmPortType = null;

	private SubscriberService() {
	}

	public static synchronized SubscriberInterfaces getInstance() {
		if (crmPortType == null) {
			crmPortType = new SubscriberServices().getSubscriberServicePort();
			SoapHandlerService.configureBinding(crmPortType);
		}
		return crmPortType;
	}
}
