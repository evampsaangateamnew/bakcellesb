package com.evampsaanga.bakcell.topup;

import com.evampsaanga.bakcell.responseheaders.BaseResponse;

public class TopupResponseClient extends BaseResponse {
	private BalanceInfo balance = new BalanceInfo();

	public BalanceInfo getBalance() {
		return balance;
	}

	public void setBalance(BalanceInfo balance) {
		this.balance = balance;
	}
}
