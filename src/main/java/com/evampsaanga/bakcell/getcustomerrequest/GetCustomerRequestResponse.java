package com.evampsaanga.bakcell.getcustomerrequest;

import com.evampsaanga.bakcell.responseheaders.BaseResponse;

public class GetCustomerRequestResponse extends BaseResponse {
	private com.evampsaanga.bakcell.getcustomerrequest.CustomerData customerData = new CustomerData();

	/**
	 * @return the customerData
	 */
	public com.evampsaanga.bakcell.getcustomerrequest.CustomerData getCustomerData() {
		return customerData;
	}

	/**
	 * @param customerData
	 *            the customerData to set
	 */
	public void setCustomerData(com.evampsaanga.bakcell.getcustomerrequest.CustomerData customerData) {
		this.customerData = customerData;
	}
}
