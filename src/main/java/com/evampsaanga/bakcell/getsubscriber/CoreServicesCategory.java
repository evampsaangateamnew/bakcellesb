package com.evampsaanga.bakcell.getsubscriber;

import java.util.List;

public class CoreServicesCategory {

	private Long id;
	private String name;
	private Integer status;
	private String storeId;
	private Integer sortOrder;
	private List<CoreServicesCategoryItem> listOfCoreServicesCategoryItem;
	
	
	
	
	@Override
	public String toString() {
		return "CoreServicesCategory [id=" + id + ", name=" + name + ", status=" + status + ", storeId=" + storeId
				+ ", sortOrder=" + sortOrder + ", listOfCoreServicesCategoryItem=" + listOfCoreServicesCategoryItem
				+ "]";
	}
	public CoreServicesCategory(Long id, String name, Integer status, String storeId, Integer sortOrder) {
		this.id = id;
		this.name = name;
		this.status = status;
		this.storeId = storeId;
		this.sortOrder = sortOrder;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getStoreId() {
		return storeId;
	}
	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}
	public Integer getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}
	public List<CoreServicesCategoryItem> getListOfCoreServicesCategoryItem() {
		return listOfCoreServicesCategoryItem;
	}
	public void setListOfCoreServicesCategoryItem(List<CoreServicesCategoryItem> listOfCoreServicesCategoryItem) {
		this.listOfCoreServicesCategoryItem = listOfCoreServicesCategoryItem;
	}
	
	
}
