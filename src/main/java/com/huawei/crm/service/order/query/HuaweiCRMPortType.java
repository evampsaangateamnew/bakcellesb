
package com.huawei.crm.service.order.query;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

import com.huawei.crm.query.order.query.CalcOneOffFeeRequest;
import com.huawei.crm.query.order.query.CalcOneOffFeeResponse;
import com.huawei.crm.query.order.query.CheckPasswordRequest;
import com.huawei.crm.query.order.query.CheckPasswordResponse;
import com.huawei.crm.query.order.query.GetAccountHistoryRequest;
import com.huawei.crm.query.order.query.GetAccountHistoryResponse;
import com.huawei.crm.query.order.query.GetAccountListRequest;
import com.huawei.crm.query.order.query.GetAccountListResponse;
import com.huawei.crm.query.order.query.GetAccountMetaDataRequest;
import com.huawei.crm.query.order.query.GetAccountMetaDataResponse;
import com.huawei.crm.query.order.query.GetAccountRequest;
import com.huawei.crm.query.order.query.GetAccountResponse;
import com.huawei.crm.query.order.query.GetBillMediumInfoRequest;
import com.huawei.crm.query.order.query.GetBillMediumInfoResponse;
import com.huawei.crm.query.order.query.GetCallScreenNumListRequest;
import com.huawei.crm.query.order.query.GetCallScreenNumListResponse;
import com.huawei.crm.query.order.query.GetCorpCustomerDataRequest;
import com.huawei.crm.query.order.query.GetCorpCustomerDataResponse;
import com.huawei.crm.query.order.query.GetCustPOfferingRequest;
import com.huawei.crm.query.order.query.GetCustPOfferingResponse;
import com.huawei.crm.query.order.query.GetCustSOfferingRequest;
import com.huawei.crm.query.order.query.GetCustSOfferingResponse;
import com.huawei.crm.query.order.query.GetCustomerDataListRequest;
import com.huawei.crm.query.order.query.GetCustomerDataListResponse;
import com.huawei.crm.query.order.query.GetCustomerHistoryRequest;
import com.huawei.crm.query.order.query.GetCustomerHistoryResponse;
import com.huawei.crm.query.order.query.GetCustomerMetaDataRequest;
import com.huawei.crm.query.order.query.GetCustomerMetaDataResponse;
import com.huawei.crm.query.order.query.GetCustomerMetaDatasRequest;
import com.huawei.crm.query.order.query.GetCustomerMetaDatasResponse;
import com.huawei.crm.query.order.query.GetCustomerRequest;
import com.huawei.crm.query.order.query.GetCustomerResponse;
import com.huawei.crm.query.order.query.GetDictTableRequest;
import com.huawei.crm.query.order.query.GetDictTableResponse;
import com.huawei.crm.query.order.query.GetFnFDataRequest;
import com.huawei.crm.query.order.query.GetFnFDataResponse;
import com.huawei.crm.query.order.query.GetGroupMemberDataRequest;
import com.huawei.crm.query.order.query.GetGroupMemberDataResponse;
import com.huawei.crm.query.order.query.GetGroupRequest;
import com.huawei.crm.query.order.query.GetGroupResponse;
import com.huawei.crm.query.order.query.GetHistoryBillFileRequest;
import com.huawei.crm.query.order.query.GetHistoryBillFileResponse;
import com.huawei.crm.query.order.query.GetHomeZoneListRequest;
import com.huawei.crm.query.order.query.GetHomeZoneListResponse;
import com.huawei.crm.query.order.query.GetNetworkSettingDataRequest;
import com.huawei.crm.query.order.query.GetNetworkSettingDataResponse;
import com.huawei.crm.query.order.query.GetOfferSalesFeeRequest;
import com.huawei.crm.query.order.query.GetOfferSalesFeeResponse;
import com.huawei.crm.query.order.query.GetOfferingSpecRequest;
import com.huawei.crm.query.order.query.GetOfferingSpecResponse;
import com.huawei.crm.query.order.query.GetSaleOfferingsRequest;
import com.huawei.crm.query.order.query.GetSaleOfferingsResponse;
import com.huawei.crm.query.order.query.GetSpecialOfferingRequest;
import com.huawei.crm.query.order.query.GetSpecialOfferingResponse;
import com.huawei.crm.query.order.query.GetSubResourceDataRequest;
import com.huawei.crm.query.order.query.GetSubResourceDataResponse;
import com.huawei.crm.query.order.query.GetSubRscInstallmentRequest;
import com.huawei.crm.query.order.query.GetSubRscInstallmentResponse;
import com.huawei.crm.query.order.query.GetSubscriberHistoryRequest;
import com.huawei.crm.query.order.query.GetSubscriberHistoryResponse;
import com.huawei.crm.query.order.query.GetSubscriberMetaDataRequest;
import com.huawei.crm.query.order.query.GetSubscriberMetaDataResponse;
import com.huawei.crm.query.order.query.GetSubscriberMetaDatasRequest;
import com.huawei.crm.query.order.query.GetSubscriberMetaDatasResponse;
import com.huawei.crm.query.order.query.GetSubscriberRequest;
import com.huawei.crm.query.order.query.GetSubscriberResponse;
import com.huawei.crm.query.order.query.GetTimeSchemaRequest;
import com.huawei.crm.query.order.query.GetTimeSchemaResponse;
import com.huawei.crm.query.order.query.QueryContactLogRequest;
import com.huawei.crm.query.order.query.QueryContactLogResponse;
import com.huawei.crm.query.order.query.QueryCustomerRequest;
import com.huawei.crm.query.order.query.QueryCustomerResponse;
import com.huawei.crm.query.order.query.ValidateOfferingRelationRequest;
import com.huawei.crm.query.order.query.ValidateOfferingRelationResponse;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "HuaweiCRMPortType", targetNamespace = "http://crm.huawei.com/service/")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    com.huawei.crm.query.order.query.ObjectFactory.class,
    com.huawei.crm.service.order.query.ObjectFactory.class,
    com.huawei.crm.basetype.order.query.ObjectFactory.class
})
public interface HuaweiCRMPortType {


    /**
     * 
     * @param getSubRscInstallmentRequestMsgReq
     * @return
     *     returns com.huawei.crm.query.GetSubRscInstallmentResponse
     */
    @WebMethod(operationName = "GetSubRscInstallment", action = "urn:#GetSubRscInstallment")
    @WebResult(name = "GetSubRscInstallmentResponse", targetNamespace = "http://crm.huawei.com/query/", partName = "GetSubRscInstallmentResponseMsgResp")
    public GetSubRscInstallmentResponse getSubRscInstallment(
        @WebParam(name = "GetSubRscInstallmentRequest", targetNamespace = "http://crm.huawei.com/query/", partName = "GetSubRscInstallmentRequestMsgReq")
        GetSubRscInstallmentRequest getSubRscInstallmentRequestMsgReq);

    /**
     * 
     * @param getCustomerDataListRequestMsgReq
     * @return
     *     returns com.huawei.crm.query.GetCustomerDataListResponse
     */
    @WebMethod(operationName = "GetCustomerDataList", action = "urn:#GetCustomerDataList")
    @WebResult(name = "GetCustomerDataListResponse", targetNamespace = "http://crm.huawei.com/query/", partName = "GetCustomerDataListResponseMsgResp")
    public GetCustomerDataListResponse getCustomerDataList(
        @WebParam(name = "GetCustomerDataListRequest", targetNamespace = "http://crm.huawei.com/query/", partName = "GetCustomerDataListRequestMsgReq")
        GetCustomerDataListRequest getCustomerDataListRequestMsgReq);

    /**
     * 
     * @param getOfferingSpecRequestMsgReq
     * @return
     *     returns com.huawei.crm.query.GetOfferingSpecResponse
     */
    @WebMethod(operationName = "GetOfferingSpec", action = "urn:#GetOfferingSpec")
    @WebResult(name = "GetOfferingSpecResponse", targetNamespace = "http://crm.huawei.com/query/", partName = "GetOfferingSpecResponseMsgResp")
    public GetOfferingSpecResponse getOfferingSpec(
        @WebParam(name = "GetOfferingSpecRequest", targetNamespace = "http://crm.huawei.com/query/", partName = "GetOfferingSpecRequestMsgReq")
        GetOfferingSpecRequest getOfferingSpecRequestMsgReq);

    /**
     * 
     * @param getDictTableRequestMsgReq
     * @return
     *     returns com.huawei.crm.query.GetDictTableResponse
     */
    @WebMethod(operationName = "GetDictTable", action = "urn:#GetDictTable")
    @WebResult(name = "GetDictTableResponse", targetNamespace = "http://crm.huawei.com/query/", partName = "GetDictTableResponseMsgResp")
    public GetDictTableResponse getDictTable(
        @WebParam(name = "GetDictTableRequest", targetNamespace = "http://crm.huawei.com/query/", partName = "GetDictTableRequestMsgReq")
        GetDictTableRequest getDictTableRequestMsgReq);

    /**
     * 
     * @param retrieveOrderRequestMsgReq
     * @return
     *     returns com.huawei.crm.service.RetrieveOrderResponse
     */
    @WebMethod(operationName = "RetrieveOrder", action = "urn:#RetrieveOrder")
    @WebResult(name = "RetrieveOrderResponse", targetNamespace = "http://crm.huawei.com/service/", partName = "RetrieveOrderResponseMsgResp")
    public RetrieveOrderResponse retrieveOrder(
        @WebParam(name = "RetrieveOrderRequest", targetNamespace = "http://crm.huawei.com/service/", partName = "RetrieveOrderRequestMsgReq")
        RetrieveOrderRequest retrieveOrderRequestMsgReq);

    /**
     * 
     * @param retrieveOrderDetailRequestMsgReq
     * @return
     *     returns com.huawei.crm.service.RetrieveOrderDetailResponse
     */
    @WebMethod(operationName = "RetrieveOrderDetail", action = "urn:#RetrieveOrderDetail")
    @WebResult(name = "RetrieveOrderDetailResponse", targetNamespace = "http://crm.huawei.com/service/", partName = "RetrieveOrderDetailResponseMsgResp")
    public RetrieveOrderDetailResponse retrieveOrderDetail(
        @WebParam(name = "RetrieveOrderDetailRequest", targetNamespace = "http://crm.huawei.com/service/", partName = "RetrieveOrderDetailRequestMsgReq")
        RetrieveOrderDetailRequest retrieveOrderDetailRequestMsgReq);

    /**
     * 
     * @param retrievePendingOrderRequestMsgReq
     * @return
     *     returns com.huawei.crm.service.RetrievePendingOrderResponse
     */
    @WebMethod(operationName = "RetrievePendingOrder", action = "urn:#RetrievePendingOrder")
    @WebResult(name = "RetrievePendingOrderResponse", targetNamespace = "http://crm.huawei.com/service/", partName = "RetrievePendingOrderResponseMsgResp")
    public RetrievePendingOrderResponse retrievePendingOrder(
        @WebParam(name = "RetrievePendingOrderRequest", targetNamespace = "http://crm.huawei.com/service/", partName = "RetrievePendingOrderRequestMsgReq")
        RetrievePendingOrderRequest retrievePendingOrderRequestMsgReq);

    /**
     * 
     * @param getTimeSchemaRequestMsgReq
     * @return
     *     returns com.huawei.crm.query.GetTimeSchemaResponse
     */
    @WebMethod(operationName = "GetTimeSchema", action = "GetTimeSchema")
    @WebResult(name = "GetTimeSchemaResponse", targetNamespace = "http://crm.huawei.com/query/", partName = "GetTimeSchemaResponseMsgResp")
    public GetTimeSchemaResponse getTimeSchema(
        @WebParam(name = "GetTimeSchemaRequest", targetNamespace = "http://crm.huawei.com/query/", partName = "GetTimeSchemaRequestMsgReq")
        GetTimeSchemaRequest getTimeSchemaRequestMsgReq);

    /**
     * 
     * @param getAccountDataRequestMsgReq
     * @return
     *     returns com.huawei.crm.query.GetAccountResponse
     */
    @WebMethod(operationName = "GetAccountData", action = "QueryAccount")
    @WebResult(name = "GetAccountResponse", targetNamespace = "http://crm.huawei.com/query/", partName = "GetAccountDataResponseMsgResp")
    public GetAccountResponse getAccountData(
        @WebParam(name = "GetAccountRequest", targetNamespace = "http://crm.huawei.com/query/", partName = "GetAccountDataRequestMsgReq")
        GetAccountRequest getAccountDataRequestMsgReq);

    /**
     * 
     * @param getCustomerDataRequestMsgReq
     * @return
     *     returns com.huawei.crm.query.GetCustomerResponse
     */
    @WebMethod(operationName = "GetCustomerData", action = "QueryCustomer")
    @WebResult(name = "GetCustomerResponse", targetNamespace = "http://crm.huawei.com/query/", partName = "GetCustomerDataResponseMsgResp")
    public GetCustomerResponse getCustomerData(
        @WebParam(name = "GetCustomerRequest", targetNamespace = "http://crm.huawei.com/query/", partName = "GetCustomerDataRequestMsgReq")
        GetCustomerRequest getCustomerDataRequestMsgReq);

    /**
     * 
     * @param getSubscriberDataRequestMsgReq
     * @return
     *     returns com.huawei.crm.query.GetSubscriberResponse
     */
    @WebMethod(operationName = "GetSubscriberData", action = "QuerySubscriber")
    @WebResult(name = "GetSubscriberResponse", targetNamespace = "http://crm.huawei.com/query/", partName = "GetSubscriberDataResponseMsgResp")
    public GetSubscriberResponse getSubscriberData(
        @WebParam(name = "GetSubscriberRequest", targetNamespace = "http://crm.huawei.com/query/", partName = "GetSubscriberDataRequestMsgReq")
        GetSubscriberRequest getSubscriberDataRequestMsgReq);

    /**
     * 
     * @param getCustomerMetaDataRequestMsgReq
     * @return
     *     returns com.huawei.crm.query.GetCustomerMetaDataResponse
     */
    @WebMethod(operationName = "GetCustomerMetaData", action = "QueryCustomerMetaData")
    @WebResult(name = "GetCustomerMetaDataResponse", targetNamespace = "http://crm.huawei.com/query/", partName = "GetCustomerMetaDataResponseMsgResp")
    public GetCustomerMetaDataResponse getCustomerMetaData(
        @WebParam(name = "GetCustomerMetaDataRequest", targetNamespace = "http://crm.huawei.com/query/", partName = "GetCustomerMetaDataRequestMsgReq")
        GetCustomerMetaDataRequest getCustomerMetaDataRequestMsgReq);

    /**
     * 
     * @param getCustomerMetaDatasRequestMsgReq
     * @return
     *     returns com.huawei.crm.query.GetCustomerMetaDatasResponse
     */
    @WebMethod(operationName = "GetCustomerMetaDatas", action = "QueryCustomerMetaDatas")
    @WebResult(name = "GetCustomerMetaDatasResponse", targetNamespace = "http://crm.huawei.com/query/", partName = "GetCustomerMetaDatasResponseMsgResp")
    public GetCustomerMetaDatasResponse getCustomerMetaDatas(
        @WebParam(name = "GetCustomerMetaDatasRequest", targetNamespace = "http://crm.huawei.com/query/", partName = "GetCustomerMetaDatasRequestMsgReq")
        GetCustomerMetaDatasRequest getCustomerMetaDatasRequestMsgReq);

    /**
     * 
     * @param getAccountMetaDataRequestMsgReq
     * @return
     *     returns com.huawei.crm.query.GetAccountMetaDataResponse
     */
    @WebMethod(operationName = "GetAccountMetaData", action = "QueryAccountMetaData")
    @WebResult(name = "GetAccountMetaDataResponse", targetNamespace = "http://crm.huawei.com/query/", partName = "GetAccountMetaDataResponseMsgResp")
    public GetAccountMetaDataResponse getAccountMetaData(
        @WebParam(name = "GetAccountMetaDataRequest", targetNamespace = "http://crm.huawei.com/query/", partName = "GetAccountMetaDataRequestMsgReq")
        GetAccountMetaDataRequest getAccountMetaDataRequestMsgReq);

    /**
     * 
     * @param getSubscriberMetaDataRequestMsgReq
     * @return
     *     returns com.huawei.crm.query.GetSubscriberMetaDataResponse
     */
    @WebMethod(operationName = "GetSubscriberMetaData", action = "QuerySubscriberMetaData")
    @WebResult(name = "GetSubscriberMetaDataResponse", targetNamespace = "http://crm.huawei.com/query/", partName = "GetSubscriberMetaDataResponseMsgResp")
    public GetSubscriberMetaDataResponse getSubscriberMetaData(
        @WebParam(name = "GetSubscriberMetaDataRequest", targetNamespace = "http://crm.huawei.com/query/", partName = "GetSubscriberMetaDataRequestMsgReq")
        GetSubscriberMetaDataRequest getSubscriberMetaDataRequestMsgReq);

    /**
     * 
     * @param getSubscriberMetaDatasRequestMsgReq
     * @return
     *     returns com.huawei.crm.query.GetSubscriberMetaDatasResponse
     */
    @WebMethod(operationName = "GetSubscriberMetaDatas", action = "QuerySubscriberMetaDatas")
    @WebResult(name = "GetSubscriberMetaDatasResponse", targetNamespace = "http://crm.huawei.com/query/", partName = "GetSubscriberMetaDatasResponseMsgResp")
    public GetSubscriberMetaDatasResponse getSubscriberMetaDatas(
        @WebParam(name = "GetSubscriberMetaDatasRequest", targetNamespace = "http://crm.huawei.com/query/", partName = "GetSubscriberMetaDatasRequestMsgReq")
        GetSubscriberMetaDatasRequest getSubscriberMetaDatasRequestMsgReq);

    /**
     * 
     * @param getCustomerHistoryRequestMsgReq
     * @return
     *     returns com.huawei.crm.query.GetCustomerHistoryResponse
     */
    @WebMethod(operationName = "GetCustomerHistory", action = "QueryCustomerHistory")
    @WebResult(name = "GetCustomerHistoryResponse", targetNamespace = "http://crm.huawei.com/query/", partName = "GetCustomerHistoryResponseMsgResp")
    public GetCustomerHistoryResponse getCustomerHistory(
        @WebParam(name = "GetCustomerHistoryRequest", targetNamespace = "http://crm.huawei.com/query/", partName = "GetCustomerHistoryRequestMsgReq")
        GetCustomerHistoryRequest getCustomerHistoryRequestMsgReq);

    /**
     * 
     * @param getAccountHistoryRequestMsgReq
     * @return
     *     returns com.huawei.crm.query.GetAccountHistoryResponse
     */
    @WebMethod(operationName = "GetAccountHistory", action = "QueryAccountHistory")
    @WebResult(name = "GetAccountHistoryResponse", targetNamespace = "http://crm.huawei.com/query/", partName = "GetAccountHistoryResponseMsgResp")
    public GetAccountHistoryResponse getAccountHistory(
        @WebParam(name = "GetAccountHistoryRequest", targetNamespace = "http://crm.huawei.com/query/", partName = "GetAccountHistoryRequestMsgReq")
        GetAccountHistoryRequest getAccountHistoryRequestMsgReq);

    /**
     * 
     * @param getSubscriberHistoryRequestMsgReq
     * @return
     *     returns com.huawei.crm.query.GetSubscriberHistoryResponse
     */
    @WebMethod(operationName = "GetSubscriberHistory", action = "QuerySubscriberHistory")
    @WebResult(name = "GetSubscriberHistoryResponse", targetNamespace = "http://crm.huawei.com/query/", partName = "GetSubscriberHistoryResponseMsgResp")
    public GetSubscriberHistoryResponse getSubscriberHistory(
        @WebParam(name = "GetSubscriberHistoryRequest", targetNamespace = "http://crm.huawei.com/query/", partName = "GetSubscriberHistoryRequestMsgReq")
        GetSubscriberHistoryRequest getSubscriberHistoryRequestMsgReq);

    /**
     * 
     * @param getAvailableCustPOfferingRequestMsgReq
     * @return
     *     returns com.huawei.crm.query.GetCustPOfferingResponse
     */
    @WebMethod(operationName = "GetAvailableCustPOffering", action = "GetAvailableCustPOffering")
    @WebResult(name = "GetCustPOfferingResponse", targetNamespace = "http://crm.huawei.com/query/", partName = "GetAvailableCustPOfferingResponseMsgResp")
    public GetCustPOfferingResponse getAvailableCustPOffering(
        @WebParam(name = "GetCustPOfferingRequest", targetNamespace = "http://crm.huawei.com/query/", partName = "GetAvailableCustPOfferingRequestMsgReq")
        GetCustPOfferingRequest getAvailableCustPOfferingRequestMsgReq);

    /**
     * 
     * @param getAvailableCustSOfferingRequestMsgReq
     * @return
     *     returns com.huawei.crm.query.GetCustSOfferingResponse
     */
    @WebMethod(operationName = "GetAvailableCustSOffering", action = "GetAvailableCustSOffering")
    @WebResult(name = "GetCustSOfferingResponse", targetNamespace = "http://crm.huawei.com/query/", partName = "GetAvailableCustSOfferingResponseMsgResp")
    public GetCustSOfferingResponse getAvailableCustSOffering(
        @WebParam(name = "GetCustSOfferingRequest", targetNamespace = "http://crm.huawei.com/query/", partName = "GetAvailableCustSOfferingRequestMsgReq")
        GetCustSOfferingRequest getAvailableCustSOfferingRequestMsgReq);

    /**
     * 
     * @param getSpecialOfferingRequestMsgReq
     * @return
     *     returns com.huawei.crm.query.GetSpecialOfferingResponse
     */
    @WebMethod(operationName = "GetSpecialOffering", action = "GetSpecialOffering")
    @WebResult(name = "GetSpecialOfferingResponse", targetNamespace = "http://crm.huawei.com/query/", partName = "GetSpecialOfferingResponseMsgResp")
    public GetSpecialOfferingResponse getSpecialOffering(
        @WebParam(name = "GetSpecialOfferingRequest", targetNamespace = "http://crm.huawei.com/query/", partName = "GetSpecialOfferingRequestMsgReq")
        GetSpecialOfferingRequest getSpecialOfferingRequestMsgReq);

    /**
     * 
     * @param checkPasswordRequestMsgReq
     * @return
     *     returns com.huawei.crm.query.CheckPasswordResponse
     */
    @WebMethod(operationName = "CheckPassword", action = "CheckPassword")
    @WebResult(name = "CheckPasswordResponse", targetNamespace = "http://crm.huawei.com/query/", partName = "CheckPasswordResponseMsgResp")
    public CheckPasswordResponse checkPassword(
        @WebParam(name = "CheckPasswordRequest", targetNamespace = "http://crm.huawei.com/query/", partName = "CheckPasswordRequestMsgReq")
        CheckPasswordRequest checkPasswordRequestMsgReq);

    /**
     * 
     * @param getCorpCustomerDataRequestMsgReq
     * @return
     *     returns com.huawei.crm.query.GetCorpCustomerDataResponse
     */
    @WebMethod(operationName = "GetCorpCustomerData", action = "GetCorpCustomerData")
    @WebResult(name = "GetCorpCustomerDataResponse", targetNamespace = "http://crm.huawei.com/query/", partName = "GetCorpCustomerDataResponseMsgResp")
    public GetCorpCustomerDataResponse getCorpCustomerData(
        @WebParam(name = "GetCorpCustomerDataRequest", targetNamespace = "http://crm.huawei.com/query/", partName = "GetCorpCustomerDataRequestMsgReq")
        GetCorpCustomerDataRequest getCorpCustomerDataRequestMsgReq);

    /**
     * 
     * @param getGroupDataRequestMsgReq
     * @return
     *     returns com.huawei.crm.query.GetGroupResponse
     */
    @WebMethod(operationName = "GetGroupData", action = "GetGroupData")
    @WebResult(name = "GetGroupResponse", targetNamespace = "http://crm.huawei.com/query/", partName = "GetGroupDataResponseMsgResp")
    public GetGroupResponse getGroupData(
        @WebParam(name = "GetGroupRequest", targetNamespace = "http://crm.huawei.com/query/", partName = "GetGroupDataRequestMsgReq")
        GetGroupRequest getGroupDataRequestMsgReq);

    /**
     * 
     * @param getGroupMemberDataRequestMsgReq
     * @return
     *     returns com.huawei.crm.query.GetGroupMemberDataResponse
     */
    @WebMethod(operationName = "GetGroupMemberData", action = "GetGroupMemberData")
    @WebResult(name = "GetGroupMemberDataResponse", targetNamespace = "http://crm.huawei.com/query/", partName = "GetGroupMemberDataResponseMsgResp")
    public GetGroupMemberDataResponse getGroupMemberData(
        @WebParam(name = "GetGroupMemberDataRequest", targetNamespace = "http://crm.huawei.com/query/", partName = "GetGroupMemberDataRequestMsgReq")
        GetGroupMemberDataRequest getGroupMemberDataRequestMsgReq);

    /**
     * 
     * @param getHistoryBillFileRequestMsgReq
     * @return
     *     returns com.huawei.crm.query.GetHistoryBillFileResponse
     */
    @WebMethod(operationName = "GetHistoryBillFile", action = "GetHistoryBillFile")
    @WebResult(name = "GetHistoryBillFileResponse", targetNamespace = "http://crm.huawei.com/query/", partName = "GetHistoryBillFileResponseMsgResp")
    public GetHistoryBillFileResponse getHistoryBillFile(
        @WebParam(name = "GetHistoryBillFileRequest", targetNamespace = "http://crm.huawei.com/query/", partName = "GetHistoryBillFileRequestMsgReq")
        GetHistoryBillFileRequest getHistoryBillFileRequestMsgReq);

    /**
     * 
     * @param calcOneOffFeeRequestMsgReq
     * @return
     *     returns com.huawei.crm.query.CalcOneOffFeeResponse
     */
    @WebMethod(operationName = "CalcOneOffFee", action = "CalcOneOffFee")
    @WebResult(name = "CalcOneOffFeeResponse", targetNamespace = "http://crm.huawei.com/query/", partName = "CalcOneOffFeeResponseMsgResp")
    public CalcOneOffFeeResponse calcOneOffFee(
        @WebParam(name = "CalcOneOffFeeRequest", targetNamespace = "http://crm.huawei.com/query/", partName = "CalcOneOffFeeRequestMsgReq")
        CalcOneOffFeeRequest calcOneOffFeeRequestMsgReq);

    /**
     * 
     * @param getFnFDataRequestMsgReq
     * @return
     *     returns com.huawei.crm.query.GetFnFDataResponse
     */
    @WebMethod(operationName = "GetFnFData", action = "GetFnFData")
    @WebResult(name = "GetFnFDataResponse", targetNamespace = "http://crm.huawei.com/query/", partName = "GetFnFDataResponseMsgResp")
    public GetFnFDataResponse getFnFData(
        @WebParam(name = "GetFnFDataRequest", targetNamespace = "http://crm.huawei.com/query/", partName = "GetFnFDataRequestMsgReq")
        GetFnFDataRequest getFnFDataRequestMsgReq);

    /**
     * 
     * @param getNetworkSettingDataRequestMsgReq
     * @return
     *     returns com.huawei.crm.query.GetNetworkSettingDataResponse
     */
    @WebMethod(operationName = "GetNetworkSettingData", action = "GetNetworkSettingData")
    @WebResult(name = "GetNetworkSettingDataResponse", targetNamespace = "http://crm.huawei.com/query/", partName = "GetNetworkSettingDataResponseMsgResp")
    public GetNetworkSettingDataResponse getNetworkSettingData(
        @WebParam(name = "GetNetworkSettingDataRequest", targetNamespace = "http://crm.huawei.com/query/", partName = "GetNetworkSettingDataRequestMsgReq")
        GetNetworkSettingDataRequest getNetworkSettingDataRequestMsgReq);

    /**
     * 
     * @param getSaleOfferingsRequestMsgReq
     * @return
     *     returns com.huawei.crm.query.GetSaleOfferingsResponse
     */
    @WebMethod(operationName = "GetSaleOfferings", action = "GetSaleOfferings")
    @WebResult(name = "GetSaleOfferingsResponse", targetNamespace = "http://crm.huawei.com/query/", partName = "GetSaleOfferingsResponseMsgResp")
    public GetSaleOfferingsResponse getSaleOfferings(
        @WebParam(name = "GetSaleOfferingsRequest", targetNamespace = "http://crm.huawei.com/query/", partName = "GetSaleOfferingsRequestMsgReq")
        GetSaleOfferingsRequest getSaleOfferingsRequestMsgReq);

    /**
     * 
     * @param getOfferSalesFeeRequestMsgReq
     * @return
     *     returns com.huawei.crm.query.GetOfferSalesFeeResponse
     */
    @WebMethod(operationName = "GetOfferSalesFee", action = "GetOfferSalesFee")
    @WebResult(name = "GetOfferSalesFeeResponse", targetNamespace = "http://crm.huawei.com/query/", partName = "GetOfferSalesFeeResponseMsgResp")
    public GetOfferSalesFeeResponse getOfferSalesFee(
        @WebParam(name = "GetOfferSalesFeeRequest", targetNamespace = "http://crm.huawei.com/query/", partName = "GetOfferSalesFeeRequestMsgReq")
        GetOfferSalesFeeRequest getOfferSalesFeeRequestMsgReq);

    /**
     * 
     * @param validateOfferingRelationRequestMsgReq
     * @return
     *     returns com.huawei.crm.query.ValidateOfferingRelationResponse
     */
    @WebMethod(operationName = "ValidateOfferingRelation", action = "ValidateOfferingRelation")
    @WebResult(name = "ValidateOfferingRelationResponse", targetNamespace = "http://crm.huawei.com/query/", partName = "ValidateOfferingRelationResponseMsgResp")
    public ValidateOfferingRelationResponse validateOfferingRelation(
        @WebParam(name = "ValidateOfferingRelationRequest", targetNamespace = "http://crm.huawei.com/query/", partName = "ValidateOfferingRelationRequestMsgReq")
        ValidateOfferingRelationRequest validateOfferingRelationRequestMsgReq);

    /**
     * 
     * @param getSubResourceDataRequestMsgReq
     * @return
     *     returns com.huawei.crm.query.GetSubResourceDataResponse
     */
    @WebMethod(operationName = "GetSubResourceData", action = "GetSubResourceData")
    @WebResult(name = "GetSubResourceDataResponse", targetNamespace = "http://crm.huawei.com/query/", partName = "GetSubResourceDataResponseMsgResp")
    public GetSubResourceDataResponse getSubResourceData(
        @WebParam(name = "GetSubResourceDataRequest", targetNamespace = "http://crm.huawei.com/query/", partName = "GetSubResourceDataRequestMsgReq")
        GetSubResourceDataRequest getSubResourceDataRequestMsgReq);

    /**
     * 
     * @param queryContactLogRequestMsgReq
     * @return
     *     returns com.huawei.crm.query.QueryContactLogResponse
     */
    @WebMethod(operationName = "QueryContactLog", action = "urn:#QueryContactLog")
    @WebResult(name = "QueryContactLogResponse", targetNamespace = "http://crm.huawei.com/query/", partName = "QueryContactLogResponseMsgResp")
    public QueryContactLogResponse queryContactLog(
        @WebParam(name = "QueryContactLogRequest", targetNamespace = "http://crm.huawei.com/query/", partName = "QueryContactLogRequestMsgReq")
        QueryContactLogRequest queryContactLogRequestMsgReq);

    /**
     * 
     * @param getCallScreenNumListRequestMsgReq
     * @return
     *     returns com.huawei.crm.query.GetCallScreenNumListResponse
     */
    @WebMethod(operationName = "GetCallScreenNumList", action = "urn:#GetCallScreenNumList")
    @WebResult(name = "GetCallScreenNumListResponse", targetNamespace = "http://crm.huawei.com/query/", partName = "GetCallScreenNumListResponseMsgResp")
    public GetCallScreenNumListResponse getCallScreenNumList(
        @WebParam(name = "GetCallScreenNumListRequest", targetNamespace = "http://crm.huawei.com/query/", partName = "GetCallScreenNumListRequestMsgReq")
        GetCallScreenNumListRequest getCallScreenNumListRequestMsgReq);

    /**
     * 
     * @param queryCustomerRequestMsgReq
     * @return
     *     returns com.huawei.crm.query.QueryCustomerResponse
     */
    @WebMethod(operationName = "QueryCustomer", action = "QueryCustomerInfo")
    @WebResult(name = "QueryCustomerResponse", targetNamespace = "http://crm.huawei.com/query/", partName = "QueryCustomerResponseMsgResp")
    public QueryCustomerResponse queryCustomer(
        @WebParam(name = "QueryCustomerRequest", targetNamespace = "http://crm.huawei.com/query/", partName = "QueryCustomerRequestMsgReq")
        QueryCustomerRequest queryCustomerRequestMsgReq);

    /**
     * 
     * @param getAccountListRequestMsgReq
     * @return
     *     returns com.huawei.crm.query.GetAccountListResponse
     */
    @WebMethod(operationName = "GetAccountList", action = "GetAccountList")
    @WebResult(name = "GetAccountListResponse", targetNamespace = "http://crm.huawei.com/query/", partName = "GetAccountListResponseMsgResp")
    public GetAccountListResponse getAccountList(
        @WebParam(name = "GetAccountListRequest", targetNamespace = "http://crm.huawei.com/query/", partName = "GetAccountListRequestMsgReq")
        GetAccountListRequest getAccountListRequestMsgReq);

    /**
     * 
     * @param getHomeZoneListRequestMsgReq
     * @return
     *     returns com.huawei.crm.query.GetHomeZoneListResponse
     */
    @WebMethod(operationName = "GetHomeZoneList", action = "GetHomeZoneList")
    @WebResult(name = "GetHomeZoneListResponse", targetNamespace = "http://crm.huawei.com/query/", partName = "GetHomeZoneListResponseMsgResp")
    public GetHomeZoneListResponse getHomeZoneList(
        @WebParam(name = "GetHomeZoneListRequest", targetNamespace = "http://crm.huawei.com/query/", partName = "GetHomeZoneListRequestMsgReq")
        GetHomeZoneListRequest getHomeZoneListRequestMsgReq);

    /**
     * 
     * @param getBillMediumInfoRequestMsgReq
     * @return
     *     returns com.huawei.crm.query.GetBillMediumInfoResponse
     */
    @WebMethod(operationName = "GetBillMediumInfo", action = "GetBillMediumInfo")
    @WebResult(name = "GetBillMediumInfoResponse", targetNamespace = "http://crm.huawei.com/query/", partName = "GetBillMediumInfoResponseMsgResp")
    public GetBillMediumInfoResponse getBillMediumInfo(
        @WebParam(name = "GetBillMediumInfoRequest", targetNamespace = "http://crm.huawei.com/query/", partName = "GetBillMediumInfoRequestMsgReq")
        GetBillMediumInfoRequest getBillMediumInfoRequestMsgReq);

}
