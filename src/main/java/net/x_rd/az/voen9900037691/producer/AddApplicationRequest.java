
package net.x_rd.az.voen9900037691.producer;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import https.wwww_e_gov.RequestApplicationTypes;
import https.wwww_e_gov.RequestAuthentication;


/**
 * <p>Java class for AddApplicationRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AddApplicationRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Authentication" type="{https://wwww.e-gov.az}RequestAuthentication" minOccurs="0"/>
 *         &lt;element name="TransactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApplicationTypes" type="{https://wwww.e-gov.az}RequestApplicationTypes" minOccurs="0"/>
 *         &lt;element name="ApplicationInformation" type="{http://voen9900037691.az.x-rd.net/producer}ArrayOfRequestApplication" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddApplicationRequest", propOrder = {
    "authentication",
    "transactionId",
    "applicationTypes",
    "applicationInformation"
})
public class AddApplicationRequest {

    @XmlElement(name = "Authentication")
    protected RequestAuthentication authentication;
    @XmlElement(name = "TransactionId")
    protected String transactionId;
    @XmlElement(name = "ApplicationTypes")
    protected RequestApplicationTypes applicationTypes;
    @XmlElement(name = "ApplicationInformation")
    protected ArrayOfRequestApplication applicationInformation;

    /**
     * Gets the value of the authentication property.
     * 
     * @return
     *     possible object is
     *     {@link RequestAuthentication }
     *     
     */
    public RequestAuthentication getAuthentication() {
        return authentication;
    }

    /**
     * Sets the value of the authentication property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestAuthentication }
     *     
     */
    public void setAuthentication(RequestAuthentication value) {
        this.authentication = value;
    }

    /**
     * Gets the value of the transactionId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Sets the value of the transactionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionId(String value) {
        this.transactionId = value;
    }

    /**
     * Gets the value of the applicationTypes property.
     * 
     * @return
     *     possible object is
     *     {@link RequestApplicationTypes }
     *     
     */
    public RequestApplicationTypes getApplicationTypes() {
        return applicationTypes;
    }

    /**
     * Sets the value of the applicationTypes property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestApplicationTypes }
     *     
     */
    public void setApplicationTypes(RequestApplicationTypes value) {
        this.applicationTypes = value;
    }

    /**
     * Gets the value of the applicationInformation property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfRequestApplication }
     *     
     */
    public ArrayOfRequestApplication getApplicationInformation() {
        return applicationInformation;
    }

    /**
     * Sets the value of the applicationInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfRequestApplication }
     *     
     */
    public void setApplicationInformation(ArrayOfRequestApplication value) {
        this.applicationInformation = value;
    }

}
