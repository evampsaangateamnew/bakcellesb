package com.evampsaanga.ulduzum;

import com.evampsaanga.bakcell.responseheaders.BaseResponse;
import com.evampsaanga.ulduzum.codegenerate.GetCodegenerateData;

public class UlduzumCodeGenerateResponse extends BaseResponse
{
	private GetCodegenerateData data;

	public GetCodegenerateData getData() 
	{
		return data;
	}

	public void setData(GetCodegenerateData data) 
	{
		this.data = data;
	}
	
	
	
	

}
