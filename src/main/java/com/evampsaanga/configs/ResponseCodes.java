package com.evampsaanga.configs;

public class ResponseCodes {
	public static final String GENERIC_ERROR_DES = "sorry, due to connectivity problems, the last command could not be completed";
	public static final String GENERIC_ERROR_CODE = "500";
	public static final String CONNECTIVITY_PROBLEM_DES = "sorry, due to connectivity problems, the last command could not be completed";
	public static final String CONNECTIVITY_PROBLEM_CODE = "50";
	public static final String MSISDN_NOT_FOUND_BK_CODE = "1211000402";
	public static final String MSISDN_NOT_FOUND_BK_DES = "MSISDN does not exist";
	public static final String MISSING_PARAMETER_CODE = "199";
	public static final String START_DATE_CANNOT_BE_IN_FUTURE_CODE = "141";
	public static final String END_DATE_CANNOT_BE_IN_FUTURE_CODE = "142";
	public static final String START_DATE_GRATER_END_DATE_CODE = "143";
	public static final String START_DATE_CANNOT_BE_IN_FUTURE_DES = "Start date cannot be in future.";
	public static final String END_DATE_CANNOT_BE_IN_FUTURE_DES = "End date cannot be in future.";
	public static final String START_DATE_GRATER_END_DATE_DES = "Start date cannot be greater then end date";

	public static final String START_DATE_INVALID_CODE = "144";
	public static final String START_DATE_INVALID_DES = "Invalid Start date format,  Should be in yyyy-MM-dd format";
	public static final String END_DATE_INVALID_CODE = "145";
	public static final String END_DATE_INVALID_DES = "Invalid End date format,  Should be in yyyy-MM-dd format";

	public static final String MISSING_PARAMETER_DES = "one of more parameter is missing or of invalid type";
	public static final String ERROR_401_CODE = "401";
	public static final String ERROR_401 = "access Not authorized";
	public static final String ERROR_MSISDN = "Not a valid msisdn number";
	public static final String ERROR_MSISDN_CODE = "-1";
	public static final String ERROR_400 = "Bad Request";
	public static final String ERROR_400_CODE = "400";
	public static final String SUCESS_DES_200 = "Sucessfull";
	public static final String SUCESS_CODE_200 = "200";
	public static final String CALL_FORWARD_TO_SAME_NUMBER_CODE = "1300";
	public static final String CALL_FORWARD_TO_SAME_NUMBER_DES = "You cannot forward to signed up number.";
	public static final String CALL_FORWARD_TO_INVALID_NUMBER_CODE = "1301";
	public static final String CALL_FORWARD_TO_INVALID_NUMBER_DES = "Number is incorrect.";
	public static final String INTERNAL_SERVER_ERROR_CODE = "500";
	public static final String INTERNAL_SERVER_ERROR_DES = "Internal Server error please report to authority";
	public static final String MAGENTO_SERVER_ERROR_CODE = "402";
	public static final String MAGENTO_SERVER_ERROR_DES = "Magento Server issue";
	public static final String CUSTOMER_TYPE_ERROR_CODE = "5001";
	public static final String CUSTOMER_TYPE__ERROR_DES = "customer type prepaid/postpaid not defined";
	public static final String CUSTOMER_STATUS_ERROR_CODE = "5002";
	public static final String CUSTOMER_STATUS_ERROR_DES = "Forgot password allowed only in Active state and B1W.";
	public static final String INVALID_AMOUNT_ERROR_CODE = "4444";
	public static final String PAYMENT_GENERIC_ERROR_CODE = "1201";

	public static final String TRANSFERER_AND_TRANSFREE_SHOUULD_BE_DIFFERENT_CODE = "1200";
	public static final String TRANSFERER_AND_TRANSFREE_SHOUULD_BE_DIFFERENT_DES = "You cannot transfer money to signed up number.";

	public static final String INVALID_AMOUNT_ERROR_DES = "Invalid Amount";
	public static final String INSUFFICIENT_BALANCE_ERROR_CODE = "4443";
	public static final String INSUFFICIENT_BALANCE_ERROR_DES = "4443";
	public static final String SMS_LIMIT_CODE_199 = "199";
	public static final String SMS_LIMIT_CODE_198 = "198";
	public static final String MONEY_TRANSFER_TO_POSTPAID_CODE = "11";
	public static final String MONEY_TRANSFER_TO_POSTPAID_DES = "Transfer to postpaid user not allowed";
	// for magento phase2
	public static final String TARIFF_DETAILS_VERSION_2_SUCCESSFUL = "271";
	// unable to send sms
	public static final String UNSUCCESS_CODE = "444";
	public static final String UNSUCCESS_DESC = "Unable to Send SMS. Check Reciever MSISDN:";
	public static final String UNSUCCESS_DESC_CS = "Balance Is Not Suffient To Activate The Requested Offer";

	public static final String UNSUCCESS_CODE_CANCEL = "445";
	public static final String UNSUCCESS_CANCEL_DESC = "No Orders Found";

	public static final String NO_FAILED_CODE = "446";
	public static final String NO_FAILED_DES = "No Failed Orders Are Found!";
	// ulduzum code generate error code
	public static final String CODE_GENERATE_ERROR_CODE = "447";
	public static final String CODE_NO_DATA_FOUND = "4004";
	public static final String ERROR_NO_DATA_FOUND = "No data found";
	public static final String ULDUZUM_DAILY_LIMIT_REACHED = "4005";
	public static final String ULDUZUM_DAILY_LIMIT_REACHED_DESC = "daily-limit-exceeded";
	// getcdrssummar (usagehistory) , getOperationShitsoty
	public static final String ERROR_CODE_SERVICE_UNAVAILABLE_AT_12AM_TO_5AM = "448";
	public static final String ERROR_MESSAGE_SERVICE_UNAVAILABLE_AT_12AM_TO_5AM = "Sory this Service is Unavailable Betweeen 12:00 A.M to 5:00 A.M";

	public static final String PASSWORD_MATCH_FAILED_CODE = "43";
	public static final String PASSWORD_MATCH_FAILED_DESCRIPTION = "Wrong Password Attempt";
	public static final String USER_NOT_FOUND_CODE = "44";
	public static final String USER_NOT_FOUND_DESCRIPTION = "User Not Found";

	public static final String USER_CEARED_FROM_CACHE_DESCRIPTION = "User cleared From Cache and below is the information of that user";
	public static final String PASSWORD_AUTHENTICATION_FAILED = "104";
	public static final String PASSWORD_AUTHENTICATION_FAILED_MSG = "Password authentication failed";
	public static final String LOGIN_ATTEMPT_FAILED = "45";
	public static final String LOGIN_ATTEMPT_FAILED_DESCRIPTION = "Password attempt failed for 3 times";
	public static final String USER_DOESNOT_EXIST_CODE = "06";
	public static final String USER_DOESNOT_EXIST_DESC = "User does not exist";
	public static final String USER_ALREADY_EXIST_CODE = "03";
	public static final String USER_ALREADY_EXIST_DESC = "User already exist";

	// ChangePayment Relation b2B bulk, response codes
	public static final String LOWER_LIMIT_LESS_THAN_MRC_OR_BALANCE_ERROR_CODE = "450";
	public static final String LOWER_LIMIT_LESS_THAN_MRC_OR_BALANCE_ERROR_DESCRIPTION = "Your limit is Less than the Corporate Balance or current MRC";

	public static final String MAX_LIMIT_GREATER_THAN_MRC_OR_COMAPNY_VALUE_ERROR_CODE = "451";
	public static final String MAX_LIMIT_GREATER_THAN_MRC_OR_COMAPNY_VALUE_ERROR_DESCRIPTION = "Your Maximum should be greater than 0 or less than the CompanyValue+current MRC";
	public static final String OM_BALANCE_LESS_THAN_ZERO = "100";
	public static final String OM_BALANCE_LESS_THAN_ZERO_DESC = "Balance is less than zero";
	public static final String OM_GROUP_PERMISSION_NOT_ALLOWED = "101";
	public static final String OM_GROUP_PERMISSION_NOT_ALLOWED_DESC = "Group permission is not allowed";
	public static final String OFFER_SUBSCRIPTION_NOT_ALLOWED_DES = "Offer subscription is not allowed in this time range.";
	public static final String OFFER_SUBSCRIPTION_NOT_ALLOWED_CODE = "1599";

}
