
package com.huawei.crm.query.order.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.order.query.ExtParameterList;
import com.huawei.crm.basetype.order.query.OfferingIdList;


/**
 * <p>Java class for GetOfferSalesFeeIn complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetOfferSalesFeeIn">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OfferingIdList" type="{http://crm.huawei.com/basetype/}OfferingIdList"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetOfferSalesFeeIn", propOrder = {
    "offeringIdList",
    "extParamList"
})
public class GetOfferSalesFeeIn {

    @XmlElement(name = "OfferingIdList", required = true)
    protected OfferingIdList offeringIdList;
    @XmlElement(name = "ExtParamList")
    protected ExtParameterList extParamList;

    /**
     * Gets the value of the offeringIdList property.
     * 
     * @return
     *     possible object is
     *     {@link OfferingIdList }
     *     
     */
    public OfferingIdList getOfferingIdList() {
        return offeringIdList;
    }

    /**
     * Sets the value of the offeringIdList property.
     * 
     * @param value
     *     allowed object is
     *     {@link OfferingIdList }
     *     
     */
    public void setOfferingIdList(OfferingIdList value) {
        this.offeringIdList = value;
    }

    /**
     * Gets the value of the extParamList property.
     * 
     * @return
     *     possible object is
     *     {@link ExtParameterList }
     *     
     */
    public ExtParameterList getExtParamList() {
        return extParamList;
    }

    /**
     * Sets the value of the extParamList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtParameterList }
     *     
     */
    public void setExtParamList(ExtParameterList value) {
        this.extParamList = value;
    }

}
