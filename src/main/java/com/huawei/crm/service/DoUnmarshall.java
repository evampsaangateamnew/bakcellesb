package com.huawei.crm.service;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

public class DoUnmarshall {
	public static void main(String[] args) {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(Item.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><item catalog-number=\"1\" description=\"Test description\">    <price xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xsi:type=\"xs:long\"></price></item>";
			// String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>
			// <NS2:USSDGWLoanRspMsg
			// xmlns:NS2=\"http://www.huawei.com/bss/soaif/interface/USSDGateWay/\">
			// <NS3:RspHeader
			// xmlns:NS3=\"http://www.huawei.com/bss/soaif/interface/common/\">
			// <NS3:ReturnCode>102050522</NS3:ReturnCode> <NS3:ReturnMsg>No
			// subscriber information is found based on value testmsdn of
			// service number.</NS3:ReturnMsg> <NS3:RspTime>2017-05-03
			// 10:35:36</NS3:RspTime> </NS3:RspHeader> </NS2:USSDGWLoanRspMsg>";
			// String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><item
			// price=\"\" description=\"Test description\"
			// catalog-number=\"10\"/>";
			InputStream inputStream = new ByteArrayInputStream(xml.getBytes());
			Item item = (Item) unmarshaller.unmarshal(inputStream);
			System.out.println(item.toString());
		} catch (JAXBException e) {
		}
	}
}