package com.evampsaanga.magento.tariffdetails.support.updatedv2b2c;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Data {
	@JsonProperty("Postpaid")
	private Postpaid Postpaid = null;
	@JsonProperty("Prepaid")
	private Prepaid Prepaid = null;
	
	@JsonProperty("Postpaid")
	public Postpaid getPostpaid() {
		return Postpaid;
	}
	@JsonProperty("Postpaid")
	public void setPostpaid(Postpaid postpaid) {
		Postpaid = postpaid;
	}
	@JsonProperty("Prepaid")
	public Prepaid getPrepaid() {
		return Prepaid;
	}
	@JsonProperty("Prepaid")
	public void setPrepaid(Prepaid prepaid) {
		Prepaid = prepaid;
	}
}
