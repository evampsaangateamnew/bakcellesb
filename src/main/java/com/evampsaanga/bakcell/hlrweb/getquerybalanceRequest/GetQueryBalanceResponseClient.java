package com.evampsaanga.bakcell.hlrweb.getquerybalanceRequest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.evampsaanga.bakcell.responseheaders.BaseResponse;
import com.evampsaanga.gethomepage.Balance;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetSubscriberResponseClient", propOrder = { "responseBody" })
@XmlRootElement(name = "GetSubscriberResponseClient")
public class GetQueryBalanceResponseClient extends BaseResponse {
	Balance balance = new Balance();

	/**
	 * @return the balance
	 */
	public Balance getBalance() {
		return balance;
	}

	/**
	 * @param balance
	 *            the balance to set
	 */
	public void setBalance(Balance balance) {
		this.balance = balance;
	}
}
