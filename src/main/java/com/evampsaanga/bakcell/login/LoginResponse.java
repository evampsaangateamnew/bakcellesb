package com.evampsaanga.bakcell.login;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import com.evampsaanga.bakcell.getcustomerrequest.CustomerData;
import com.evampsaanga.bakcell.responseheaders.BaseResponse;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "SignUpRequest")
public class LoginResponse extends BaseResponse {
	private com.evampsaanga.bakcell.getcustomerrequest.CustomerData customerData = new CustomerData();

	/**
	 * @return the customerData
	 */
	public com.evampsaanga.bakcell.getcustomerrequest.CustomerData getCustomerData() {
		return customerData;
	}

	/**
	 * @param customerData
	 *            the customerData to set
	 */
	public void setCustomerData(com.evampsaanga.bakcell.getcustomerrequest.CustomerData customerData) {
		this.customerData = customerData;
	}
}
