package com.huawei.bme.cbsinterface.bbservices;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bbcommon.CustAccessCode;
import com.huawei.bme.cbsinterface.bbcommon.SubAccessCode;
import com.huawei.bme.cbsinterface.bbcommon.SubGroupAccessCode;

/**
 * <p>
 * Java class for QueryFreeUnitRequest complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="QueryFreeUnitRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="QueryObj">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;choice>
 *                   &lt;element name="CustAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bbcommon}CustAccessCode"/>
 *                   &lt;element name="SubAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bbcommon}SubAccessCode"/>
 *                   &lt;element name="SubGroupAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bbcommon}SubGroupAccessCode"/>
 *                 &lt;/choice>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="OfferingKey" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="OfferingID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="PurchaseSeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryFreeUnitRequest", propOrder = { "queryObj", "offeringKey" })
public class QueryFreeUnitRequest {
	@XmlElement(name = "QueryObj", required = true)
	protected QueryFreeUnitRequest.QueryObj queryObj;
	@XmlElement(name = "OfferingKey")
	protected QueryFreeUnitRequest.OfferingKey offeringKey;

	/**
	 * Gets the value of the queryObj property.
	 * 
	 * @return possible object is {@link QueryFreeUnitRequest.QueryObj }
	 * 
	 */
	public QueryFreeUnitRequest.QueryObj getQueryObj() {
		return queryObj;
	}

	/**
	 * Sets the value of the queryObj property.
	 * 
	 * @param value
	 *            allowed object is {@link QueryFreeUnitRequest.QueryObj }
	 * 
	 */
	public void setQueryObj(QueryFreeUnitRequest.QueryObj value) {
		this.queryObj = value;
	}

	/**
	 * Gets the value of the offeringKey property.
	 * 
	 * @return possible object is {@link QueryFreeUnitRequest.OfferingKey }
	 * 
	 */
	public QueryFreeUnitRequest.OfferingKey getOfferingKey() {
		return offeringKey;
	}

	/**
	 * Sets the value of the offeringKey property.
	 * 
	 * @param value
	 *            allowed object is {@link QueryFreeUnitRequest.OfferingKey }
	 * 
	 */
	public void setOfferingKey(QueryFreeUnitRequest.OfferingKey value) {
		this.offeringKey = value;
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="OfferingID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
	 *         &lt;element name="PurchaseSeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "offeringID", "purchaseSeq" })
	public static class OfferingKey {
		@XmlElement(name = "OfferingID", required = true, nillable = true)
		protected BigInteger offeringID;
		@XmlElement(name = "PurchaseSeq")
		protected String purchaseSeq;

		/**
		 * Gets the value of the offeringID property.
		 * 
		 * @return possible object is {@link BigInteger }
		 * 
		 */
		public BigInteger getOfferingID() {
			return offeringID;
		}

		/**
		 * Sets the value of the offeringID property.
		 * 
		 * @param value
		 *            allowed object is {@link BigInteger }
		 * 
		 */
		public void setOfferingID(BigInteger value) {
			this.offeringID = value;
		}

		/**
		 * Gets the value of the purchaseSeq property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getPurchaseSeq() {
			return purchaseSeq;
		}

		/**
		 * Sets the value of the purchaseSeq property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setPurchaseSeq(String value) {
			this.purchaseSeq = value;
		}
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;choice>
	 *         &lt;element name="CustAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bbcommon}CustAccessCode"/>
	 *         &lt;element name="SubAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bbcommon}SubAccessCode"/>
	 *         &lt;element name="SubGroupAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bbcommon}SubGroupAccessCode"/>
	 *       &lt;/choice>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "custAccessCode", "subAccessCode", "subGroupAccessCode" })
	public static class QueryObj {
		@XmlElement(name = "CustAccessCode")
		protected CustAccessCode custAccessCode;
		@XmlElement(name = "SubAccessCode")
		protected SubAccessCode subAccessCode;
		@XmlElement(name = "SubGroupAccessCode")
		protected SubGroupAccessCode subGroupAccessCode;

		/**
		 * Gets the value of the custAccessCode property.
		 * 
		 * @return possible object is {@link CustAccessCode }
		 * 
		 */
		public CustAccessCode getCustAccessCode() {
			return custAccessCode;
		}

		/**
		 * Sets the value of the custAccessCode property.
		 * 
		 * @param value
		 *            allowed object is {@link CustAccessCode }
		 * 
		 */
		public void setCustAccessCode(CustAccessCode value) {
			this.custAccessCode = value;
		}

		/**
		 * Gets the value of the subAccessCode property.
		 * 
		 * @return possible object is {@link SubAccessCode }
		 * 
		 */
		public SubAccessCode getSubAccessCode() {
			return subAccessCode;
		}

		/**
		 * Sets the value of the subAccessCode property.
		 * 
		 * @param value
		 *            allowed object is {@link SubAccessCode }
		 * 
		 */
		public void setSubAccessCode(SubAccessCode value) {
			this.subAccessCode = value;
		}

		/**
		 * Gets the value of the subGroupAccessCode property.
		 * 
		 * @return possible object is {@link SubGroupAccessCode }
		 * 
		 */
		public SubGroupAccessCode getSubGroupAccessCode() {
			return subGroupAccessCode;
		}

		/**
		 * Sets the value of the subGroupAccessCode property.
		 * 
		 * @param value
		 *            allowed object is {@link SubGroupAccessCode }
		 * 
		 */
		public void setSubGroupAccessCode(SubGroupAccessCode value) {
			this.subGroupAccessCode = value;
		}
	}
}
