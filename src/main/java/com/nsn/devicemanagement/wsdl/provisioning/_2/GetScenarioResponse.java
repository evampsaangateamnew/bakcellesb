package com.nsn.devicemanagement.wsdl.provisioning._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.nsn.devicemanagement.wsdl.common._2.SettingProvisioningScenario;

/**
 * <p>
 * Java class for getScenarioResponse complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="getScenarioResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}SettingProvisioningScenario" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getScenarioResponse", propOrder = { "_return" })
public class GetScenarioResponse {
	@XmlElement(name = "return")
	protected SettingProvisioningScenario _return;

	/**
	 * Gets the value of the return property.
	 * 
	 * @return possible object is {@link SettingProvisioningScenario }
	 * 
	 */
	public SettingProvisioningScenario getReturn() {
		return _return;
	}

	/**
	 * Sets the value of the return property.
	 * 
	 * @param value
	 *            allowed object is {@link SettingProvisioningScenario }
	 * 
	 */
	public void setReturn(SettingProvisioningScenario value) {
		this._return = value;
	}
}
