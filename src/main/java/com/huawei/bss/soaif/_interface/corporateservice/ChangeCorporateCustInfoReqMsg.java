
package com.huawei.bss.soaif._interface.corporateservice;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.bss.soaif._interface.common.querycorporate.CorporateInfo;
import com.huawei.bss.soaif._interface.common.querycorporate.CustomerDocumentInfo;
import com.huawei.bss.soaif._interface.common.querycorporate.ManagerInfo;
import com.huawei.bss.soaif._interface.common.querycorporate.ReqHeader;
import com.huawei.bss.soaif._interface.common.querycorporate.SimpleProperty;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.huawei.com/bss/soaif/interface/common/}ReqHeader"/>
 *         &lt;element name="Customer">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="CustId" type="{http://www.huawei.com/bss/soaif/interface/common/}CustId"/>
 *                   &lt;element name="ExternalCustomerId" type="{http://www.huawei.com/bss/soaif/interface/common/}ExternalCustomerId" minOccurs="0"/>
 *                   &lt;element name="CustomerRelaId" type="{http://www.huawei.com/bss/soaif/interface/common/}CustomerRelaId" minOccurs="0"/>
 *                   &lt;element name="CustLevel" type="{http://www.huawei.com/bss/soaif/interface/common/}CustLevel" minOccurs="0"/>
 *                   &lt;element name="CorporateInfo" type="{http://www.huawei.com/bss/soaif/interface/common/}CorporateInfo"/>
 *                   &lt;element name="Address" maxOccurs="100" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}Address">
 *                           &lt;sequence>
 *                             &lt;element name="ActionType" type="{http://www.huawei.com/bss/soaif/interface/common/}ActionType"/>
 *                           &lt;/sequence>
 *                         &lt;/extension>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="Manager" maxOccurs="100" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}ManagerInfo">
 *                           &lt;sequence>
 *                             &lt;element name="ActionType" type="{http://www.huawei.com/bss/soaif/interface/common/}ActionType"/>
 *                           &lt;/sequence>
 *                         &lt;/extension>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="BRNExpiryDate" type="{http://www.huawei.com/bss/soaif/interface/common/}BRNExpiryDate" minOccurs="0"/>
 *                   &lt;element name="TaxCertificateID" type="{http://www.huawei.com/bss/soaif/interface/common/}TaxCertificateID" minOccurs="0"/>
 *                   &lt;element name="ContactNumber" type="{http://www.huawei.com/bss/soaif/interface/common/}ContactNumber" minOccurs="0"/>
 *                   &lt;element name="SendToLegal" type="{http://www.huawei.com/bss/soaif/interface/common/}SendToLegal" minOccurs="0"/>
 *                   &lt;element name="WrittenLanguage" type="{http://www.huawei.com/bss/soaif/interface/common/}Language" minOccurs="0"/>
 *                   &lt;element name="CustLanguage" type="{http://www.huawei.com/bss/soaif/interface/common/}Language" minOccurs="0"/>
 *                   &lt;element name="Remark" type="{http://www.huawei.com/bss/soaif/interface/common/}Remark" minOccurs="0"/>
 *                   &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}SimpleProperty" maxOccurs="100" minOccurs="0"/>
 *                   &lt;element name="CustomerDocumentList" type="{http://www.huawei.com/bss/soaif/interface/common/}CustomerDocumentInfo" maxOccurs="100" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ExternalOrderId" type="{http://www.huawei.com/bss/soaif/interface/common/}ExternalOrderId" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "reqHeader",
    "customer",
    "externalOrderId"
})
@XmlRootElement(name = "ChangeCorporateCustInfoReqMsg")
public class ChangeCorporateCustInfoReqMsg {

    @XmlElement(name = "ReqHeader", namespace = "http://www.huawei.com/bss/soaif/interface/common/", required = true)
    protected ReqHeader reqHeader;
    @XmlElement(name = "Customer", required = true)
    protected ChangeCorporateCustInfoReqMsg.Customer customer;
    @XmlElement(name = "ExternalOrderId")
    protected String externalOrderId;

    /**
     * Gets the value of the reqHeader property.
     * 
     * @return
     *     possible object is
     *     {@link ReqHeader }
     *     
     */
    public ReqHeader getReqHeader() {
        return reqHeader;
    }

    /**
     * Sets the value of the reqHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReqHeader }
     *     
     */
    public void setReqHeader(ReqHeader value) {
        this.reqHeader = value;
    }

    /**
     * Gets the value of the customer property.
     * 
     * @return
     *     possible object is
     *     {@link ChangeCorporateCustInfoReqMsg.Customer }
     *     
     */
    public ChangeCorporateCustInfoReqMsg.Customer getCustomer() {
        return customer;
    }

    /**
     * Sets the value of the customer property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeCorporateCustInfoReqMsg.Customer }
     *     
     */
    public void setCustomer(ChangeCorporateCustInfoReqMsg.Customer value) {
        this.customer = value;
    }

    /**
     * Gets the value of the externalOrderId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalOrderId() {
        return externalOrderId;
    }

    /**
     * Sets the value of the externalOrderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalOrderId(String value) {
        this.externalOrderId = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="CustId" type="{http://www.huawei.com/bss/soaif/interface/common/}CustId"/>
     *         &lt;element name="ExternalCustomerId" type="{http://www.huawei.com/bss/soaif/interface/common/}ExternalCustomerId" minOccurs="0"/>
     *         &lt;element name="CustomerRelaId" type="{http://www.huawei.com/bss/soaif/interface/common/}CustomerRelaId" minOccurs="0"/>
     *         &lt;element name="CustLevel" type="{http://www.huawei.com/bss/soaif/interface/common/}CustLevel" minOccurs="0"/>
     *         &lt;element name="CorporateInfo" type="{http://www.huawei.com/bss/soaif/interface/common/}CorporateInfo"/>
     *         &lt;element name="Address" maxOccurs="100" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}Address">
     *                 &lt;sequence>
     *                   &lt;element name="ActionType" type="{http://www.huawei.com/bss/soaif/interface/common/}ActionType"/>
     *                 &lt;/sequence>
     *               &lt;/extension>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="Manager" maxOccurs="100" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}ManagerInfo">
     *                 &lt;sequence>
     *                   &lt;element name="ActionType" type="{http://www.huawei.com/bss/soaif/interface/common/}ActionType"/>
     *                 &lt;/sequence>
     *               &lt;/extension>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="BRNExpiryDate" type="{http://www.huawei.com/bss/soaif/interface/common/}BRNExpiryDate" minOccurs="0"/>
     *         &lt;element name="TaxCertificateID" type="{http://www.huawei.com/bss/soaif/interface/common/}TaxCertificateID" minOccurs="0"/>
     *         &lt;element name="ContactNumber" type="{http://www.huawei.com/bss/soaif/interface/common/}ContactNumber" minOccurs="0"/>
     *         &lt;element name="SendToLegal" type="{http://www.huawei.com/bss/soaif/interface/common/}SendToLegal" minOccurs="0"/>
     *         &lt;element name="WrittenLanguage" type="{http://www.huawei.com/bss/soaif/interface/common/}Language" minOccurs="0"/>
     *         &lt;element name="CustLanguage" type="{http://www.huawei.com/bss/soaif/interface/common/}Language" minOccurs="0"/>
     *         &lt;element name="Remark" type="{http://www.huawei.com/bss/soaif/interface/common/}Remark" minOccurs="0"/>
     *         &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}SimpleProperty" maxOccurs="100" minOccurs="0"/>
     *         &lt;element name="CustomerDocumentList" type="{http://www.huawei.com/bss/soaif/interface/common/}CustomerDocumentInfo" maxOccurs="100" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "custId",
        "externalCustomerId",
        "customerRelaId",
        "custLevel",
        "corporateInfo",
        "address",
        "manager",
        "brnExpiryDate",
        "taxCertificateID",
        "contactNumber",
        "sendToLegal",
        "writtenLanguage",
        "custLanguage",
        "remark",
        "additionalProperty",
        "customerDocumentList"
    })
    public static class Customer {

        @XmlElement(name = "CustId", required = true)
        protected String custId;
        @XmlElement(name = "ExternalCustomerId")
        protected String externalCustomerId;
        @XmlElement(name = "CustomerRelaId")
        protected String customerRelaId;
        @XmlElement(name = "CustLevel")
        protected String custLevel;
        @XmlElement(name = "CorporateInfo", required = true)
        protected CorporateInfo corporateInfo;
        @XmlElement(name = "Address")
        protected List<ChangeCorporateCustInfoReqMsg.Customer.Address> address;
        @XmlElement(name = "Manager")
        protected List<ChangeCorporateCustInfoReqMsg.Customer.Manager> manager;
        @XmlElement(name = "BRNExpiryDate")
        protected String brnExpiryDate;
        @XmlElement(name = "TaxCertificateID")
        protected String taxCertificateID;
        @XmlElement(name = "ContactNumber")
        protected BigInteger contactNumber;
        @XmlElement(name = "SendToLegal")
        protected String sendToLegal;
        @XmlElement(name = "WrittenLanguage")
        protected String writtenLanguage;
        @XmlElement(name = "CustLanguage")
        protected String custLanguage;
        @XmlElement(name = "Remark")
        protected String remark;
        @XmlElement(name = "AdditionalProperty")
        protected List<SimpleProperty> additionalProperty;
        @XmlElement(name = "CustomerDocumentList")
        protected List<CustomerDocumentInfo> customerDocumentList;

        /**
         * Gets the value of the custId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCustId() {
            return custId;
        }

        /**
         * Sets the value of the custId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCustId(String value) {
            this.custId = value;
        }

        /**
         * Gets the value of the externalCustomerId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExternalCustomerId() {
            return externalCustomerId;
        }

        /**
         * Sets the value of the externalCustomerId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExternalCustomerId(String value) {
            this.externalCustomerId = value;
        }

        /**
         * Gets the value of the customerRelaId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCustomerRelaId() {
            return customerRelaId;
        }

        /**
         * Sets the value of the customerRelaId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCustomerRelaId(String value) {
            this.customerRelaId = value;
        }

        /**
         * Gets the value of the custLevel property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCustLevel() {
            return custLevel;
        }

        /**
         * Sets the value of the custLevel property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCustLevel(String value) {
            this.custLevel = value;
        }

        /**
         * Gets the value of the corporateInfo property.
         * 
         * @return
         *     possible object is
         *     {@link CorporateInfo }
         *     
         */
        public CorporateInfo getCorporateInfo() {
            return corporateInfo;
        }

        /**
         * Sets the value of the corporateInfo property.
         * 
         * @param value
         *     allowed object is
         *     {@link CorporateInfo }
         *     
         */
        public void setCorporateInfo(CorporateInfo value) {
            this.corporateInfo = value;
        }

        /**
         * Gets the value of the address property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the address property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAddress().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ChangeCorporateCustInfoReqMsg.Customer.Address }
         * 
         * 
         */
        public List<ChangeCorporateCustInfoReqMsg.Customer.Address> getAddress() {
            if (address == null) {
                address = new ArrayList<ChangeCorporateCustInfoReqMsg.Customer.Address>();
            }
            return this.address;
        }

        /**
         * Gets the value of the manager property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the manager property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getManager().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ChangeCorporateCustInfoReqMsg.Customer.Manager }
         * 
         * 
         */
        public List<ChangeCorporateCustInfoReqMsg.Customer.Manager> getManager() {
            if (manager == null) {
                manager = new ArrayList<ChangeCorporateCustInfoReqMsg.Customer.Manager>();
            }
            return this.manager;
        }

        /**
         * Gets the value of the brnExpiryDate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBRNExpiryDate() {
            return brnExpiryDate;
        }

        /**
         * Sets the value of the brnExpiryDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBRNExpiryDate(String value) {
            this.brnExpiryDate = value;
        }

        /**
         * Gets the value of the taxCertificateID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTaxCertificateID() {
            return taxCertificateID;
        }

        /**
         * Sets the value of the taxCertificateID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTaxCertificateID(String value) {
            this.taxCertificateID = value;
        }

        /**
         * Gets the value of the contactNumber property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getContactNumber() {
            return contactNumber;
        }

        /**
         * Sets the value of the contactNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setContactNumber(BigInteger value) {
            this.contactNumber = value;
        }

        /**
         * Gets the value of the sendToLegal property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSendToLegal() {
            return sendToLegal;
        }

        /**
         * Sets the value of the sendToLegal property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSendToLegal(String value) {
            this.sendToLegal = value;
        }

        /**
         * Gets the value of the writtenLanguage property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getWrittenLanguage() {
            return writtenLanguage;
        }

        /**
         * Sets the value of the writtenLanguage property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setWrittenLanguage(String value) {
            this.writtenLanguage = value;
        }

        /**
         * Gets the value of the custLanguage property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCustLanguage() {
            return custLanguage;
        }

        /**
         * Sets the value of the custLanguage property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCustLanguage(String value) {
            this.custLanguage = value;
        }

        /**
         * Gets the value of the remark property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRemark() {
            return remark;
        }

        /**
         * Sets the value of the remark property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRemark(String value) {
            this.remark = value;
        }

        /**
         * Gets the value of the additionalProperty property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the additionalProperty property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAdditionalProperty().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SimpleProperty }
         * 
         * 
         */
        public List<SimpleProperty> getAdditionalProperty() {
            if (additionalProperty == null) {
                additionalProperty = new ArrayList<SimpleProperty>();
            }
            return this.additionalProperty;
        }

        /**
         * Gets the value of the customerDocumentList property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the customerDocumentList property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCustomerDocumentList().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link CustomerDocumentInfo }
         * 
         * 
         */
        public List<CustomerDocumentInfo> getCustomerDocumentList() {
            if (customerDocumentList == null) {
                customerDocumentList = new ArrayList<CustomerDocumentInfo>();
            }
            return this.customerDocumentList;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}Address">
         *       &lt;sequence>
         *         &lt;element name="ActionType" type="{http://www.huawei.com/bss/soaif/interface/common/}ActionType"/>
         *       &lt;/sequence>
         *     &lt;/extension>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "actionType"
        })
        public static class Address
            extends com.huawei.bss.soaif._interface.common.querycorporate.Address
        {

            @XmlElement(name = "ActionType", required = true)
            protected String actionType;

            /**
             * Gets the value of the actionType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getActionType() {
                return actionType;
            }

            /**
             * Sets the value of the actionType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setActionType(String value) {
                this.actionType = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}ManagerInfo">
         *       &lt;sequence>
         *         &lt;element name="ActionType" type="{http://www.huawei.com/bss/soaif/interface/common/}ActionType"/>
         *       &lt;/sequence>
         *     &lt;/extension>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "actionType"
        })
        public static class Manager
            extends ManagerInfo
        {

            @XmlElement(name = "ActionType", required = true)
            protected String actionType;

            /**
             * Gets the value of the actionType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getActionType() {
                return actionType;
            }

            /**
             * Sets the value of the actionType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setActionType(String value) {
                this.actionType = value;
            }

        }

    }

}
