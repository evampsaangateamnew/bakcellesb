package com.evampsaanga.bakcell.getnetworksettings;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.services.CRMServices;
import com.huawei.crm.basetype.GetSubProductInfo;
import com.huawei.crm.basetype.RequestHeader;
import com.huawei.crm.query.GetNetworkSettingDataIn;
import com.huawei.crm.query.GetNetworkSettingDataRequest;
import com.huawei.crm.query.GetNetworkSettingDataResponse;

@Path("/bakcell")
public class GetNetworkSettingsLand {
	public static final Logger logger = Logger.getLogger("bakcellLogs-V2");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetNetworkSettingsResponseClient Get(@Header("credentials") String credential, @Body() String requestBody) {
		GetNetworkSettingsRequestClient cclient = null;
		GetNetworkSettingsResponseClient resp = new GetNetworkSettingsResponseClient();
		try {
			logger.info("Request Landed on CoreservicesList");
			String credentials = null;
			try {
				cclient = Helper.JsonToObject(requestBody, GetNetworkSettingsRequestClient.class);
			} catch (Exception ex1) {
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				return resp;
			}
			if (cclient != null) {
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					return resp;
				}
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400);
					resp.setReturnMsg(verification);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					GetNetworkSettingDataResponse responseFromBakcell = RequestSoap(getRequestHeader(), cclient);
					if (responseFromBakcell.getResponseHeader().getRetCode().equals("0")) {
						resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						for (GetSubProductInfo element : responseFromBakcell.getGetNetworkSettingDataBody()
								.getGetNetworkSettingDataList()) {
							try {
								resp.getList()
										.add(new NetworkService(
												ConfigurationManager.getCRMMappingValueByKey(
														ConfigurationManager.MAPPING_CRM_STATUS + element.getStatus()),
												element.getProductId(), element.getProductName()));
							} catch (Exception ex) {
								logger.error(Helper.GetException(ex));
							}
						}
					} else {
						resp.setReturnCode(responseFromBakcell.getResponseHeader().getRetCode());
						resp.setReturnMsg(responseFromBakcell.getResponseHeader().getRetMsg());
					}
					return resp;
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			return resp;
		}
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		return resp;
	}

	public GetNetworkSettingDataResponse RequestSoap(RequestHeader reqh, GetNetworkSettingsRequestClient cclient) {
		GetNetworkSettingDataRequest gNSD = new GetNetworkSettingDataRequest();
		gNSD.setRequestHeader(reqh);
		GetNetworkSettingDataIn gNSDI = new GetNetworkSettingDataIn();
		gNSDI.setServiceNumber(cclient.getmsisdn());
		gNSD.setGetNetworkSettingDataBody(gNSDI);
		try {
			return CRMServices.getInstance().getNetworkSettingData(gNSD);
		} catch (Exception ee) {
			logger.error(Helper.GetException(ee));
		}
		return new GetNetworkSettingDataResponse();
	}

	private RequestHeader getRequestHeader() {
		RequestHeader reqh = new RequestHeader();
		String accessUser = ConfigurationManager.getCRMProperties("crm.sub.AccessUser").trim();
		reqh.setChannelId(ConfigurationManager.getCRMProperties("crm.sub.ChannelId").trim());
		reqh.setTechnicalChannelId(ConfigurationManager.getCRMProperties("crm.sub.TechnicalChannelId").trim());
		reqh.setAccessUser(accessUser);
		reqh.setTenantId(ConfigurationManager.getCRMProperties("crm.sub.TenantId").trim());
		reqh.setAccessPwd(ConfigurationManager.getCRMProperties("crm.sub.AccessPwd").trim());
		reqh.setTestFlag(ConfigurationManager.getCRMProperties("crm.sub.TestFlag").trim());
		reqh.setLanguage(ConfigurationManager.getCRMProperties("crm.sub.Language").trim());
		reqh.setTransactionId(Helper.generateTransactionID());
		return reqh;
	}
}
