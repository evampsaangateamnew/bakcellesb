package com.evampsaanga.bakcell.notificationadmin.magentomodels;

public class TariffData {
	private String offerName;
	private String offeringId;
	private String desc;

	public String getOfferName() {
		return offerName;
	}

	public void setOfferName(String offerName) {
		this.offerName = offerName;
	}

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	@Override
	public String toString() {
		return "OfferData [offerName=" + offerName + ", offeringId=" + offeringId + ", desc=" + desc + "]";
	}
}
