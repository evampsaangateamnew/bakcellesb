package com.evampsaanga.bakcell.getroamingstatus;

import com.evampsaanga.bakcell.requestheaders.BaseRequest;

public class GetRoamingStatusRequest extends BaseRequest {
	private String productId;

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

}
