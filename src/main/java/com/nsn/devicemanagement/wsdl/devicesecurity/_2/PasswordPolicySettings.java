package com.nsn.devicemanagement.wsdl.devicesecurity._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for passwordPolicySettings complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="passwordPolicySettings">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="expirationInterval" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="historyBufferSize" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="maxLength" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="maxRepeatedCharacters" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="minLength" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="requireCharsAndNumbers" type="{http://nsn.com/DeviceManagement/wsdl/DeviceSecurity/2.0}requirementSetting" minOccurs="0"/>
 *         &lt;element name="requireUpperAndLower" type="{http://nsn.com/DeviceManagement/wsdl/DeviceSecurity/2.0}requirementSetting" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "passwordPolicySettings", propOrder = { "expirationInterval", "historyBufferSize", "maxLength",
		"maxRepeatedCharacters", "minLength", "requireCharsAndNumbers", "requireUpperAndLower" })
public class PasswordPolicySettings {
	protected Integer expirationInterval;
	protected Integer historyBufferSize;
	protected Integer maxLength;
	protected Integer maxRepeatedCharacters;
	protected Integer minLength;
	@XmlSchemaType(name = "string")
	protected RequirementSetting requireCharsAndNumbers;
	@XmlSchemaType(name = "string")
	protected RequirementSetting requireUpperAndLower;

	/**
	 * Gets the value of the expirationInterval property.
	 * 
	 * @return possible object is {@link Integer }
	 * 
	 */
	public Integer getExpirationInterval() {
		return expirationInterval;
	}

	/**
	 * Sets the value of the expirationInterval property.
	 * 
	 * @param value
	 *            allowed object is {@link Integer }
	 * 
	 */
	public void setExpirationInterval(Integer value) {
		this.expirationInterval = value;
	}

	/**
	 * Gets the value of the historyBufferSize property.
	 * 
	 * @return possible object is {@link Integer }
	 * 
	 */
	public Integer getHistoryBufferSize() {
		return historyBufferSize;
	}

	/**
	 * Sets the value of the historyBufferSize property.
	 * 
	 * @param value
	 *            allowed object is {@link Integer }
	 * 
	 */
	public void setHistoryBufferSize(Integer value) {
		this.historyBufferSize = value;
	}

	/**
	 * Gets the value of the maxLength property.
	 * 
	 * @return possible object is {@link Integer }
	 * 
	 */
	public Integer getMaxLength() {
		return maxLength;
	}

	/**
	 * Sets the value of the maxLength property.
	 * 
	 * @param value
	 *            allowed object is {@link Integer }
	 * 
	 */
	public void setMaxLength(Integer value) {
		this.maxLength = value;
	}

	/**
	 * Gets the value of the maxRepeatedCharacters property.
	 * 
	 * @return possible object is {@link Integer }
	 * 
	 */
	public Integer getMaxRepeatedCharacters() {
		return maxRepeatedCharacters;
	}

	/**
	 * Sets the value of the maxRepeatedCharacters property.
	 * 
	 * @param value
	 *            allowed object is {@link Integer }
	 * 
	 */
	public void setMaxRepeatedCharacters(Integer value) {
		this.maxRepeatedCharacters = value;
	}

	/**
	 * Gets the value of the minLength property.
	 * 
	 * @return possible object is {@link Integer }
	 * 
	 */
	public Integer getMinLength() {
		return minLength;
	}

	/**
	 * Sets the value of the minLength property.
	 * 
	 * @param value
	 *            allowed object is {@link Integer }
	 * 
	 */
	public void setMinLength(Integer value) {
		this.minLength = value;
	}

	/**
	 * Gets the value of the requireCharsAndNumbers property.
	 * 
	 * @return possible object is {@link RequirementSetting }
	 * 
	 */
	public RequirementSetting getRequireCharsAndNumbers() {
		return requireCharsAndNumbers;
	}

	/**
	 * Sets the value of the requireCharsAndNumbers property.
	 * 
	 * @param value
	 *            allowed object is {@link RequirementSetting }
	 * 
	 */
	public void setRequireCharsAndNumbers(RequirementSetting value) {
		this.requireCharsAndNumbers = value;
	}

	/**
	 * Gets the value of the requireUpperAndLower property.
	 * 
	 * @return possible object is {@link RequirementSetting }
	 * 
	 */
	public RequirementSetting getRequireUpperAndLower() {
		return requireUpperAndLower;
	}

	/**
	 * Sets the value of the requireUpperAndLower property.
	 * 
	 * @param value
	 *            allowed object is {@link RequirementSetting }
	 * 
	 */
	public void setRequireUpperAndLower(RequirementSetting value) {
		this.requireUpperAndLower = value;
	}
}
