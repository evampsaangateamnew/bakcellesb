package com.evampsaanga.bakcell.getloan;

import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBException;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.amqimplementationsesb.LogsType;
import com.evampsaanga.bakcell.XMLOverHTTP.LoanRequestFee;
import com.evampsaanga.bakcell.hlrweb.getquerybalanceRequest.HLRBalanceServices;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.ThirdPartyNames;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.developer.utils.SOAPLoggingHandler;
import com.evampsaanga.gethomepage.Prepaid;
import com.evampsaanga.gethomepage.QueryLoanLog;
import com.evampsaanga.services.USSDService;
import com.huawei.bme.cbsinterface.arservices.QueryLoanLogResult;
import com.huawei.bme.cbsinterface.arservices.QueryLoanLogResult.LoanLogDetail;
import com.huawei.bme.cbsinterface.arservices.QueryLoanLogResultMsg;
import com.huawei.bss.soaif._interface.common.ReqHeader;
import com.huawei.bss.soaif._interface.ussdgateway.USSDGWLoanReqMsg;
import com.huawei.bss.soaif._interface.ussdgateway.USSDGWLoanReqMsg.SubAccessCode;
import com.huawei.bss.soaif._interface.ussdgateway.USSDGWLoanRspMsg;

@Path("/bakcell/")
public class GetLoanLand {
	public static final Logger logger = Logger.getLogger("bakcellLogs-V2");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetLoanResponse Get(@Header("credentials") String credential, @Header("Content-Type") String contentType,
			@Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.LOAN_REQUEST_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.LOAN_REQUEST);
		logs.setTableType(LogsType.LoanRequest);
		try {
			logger.info("Request Landed on GetLoanLand:" + requestBody);
			GetLoanRequest cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, GetLoanRequest.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setIsB2B(cclient.getIsB2B());
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				GetLoanResponse resp = new GetLoanResponse();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(ResponseCodes.ERROR_400_CODE);
				logs.setResponseDescription(ResponseCodes.ERROR_400);
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					GetLoanResponse resp = new GetLoanResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						GetLoanResponse res = new GetLoanResponse();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						logs.setResponseCode(ResponseCodes.ERROR_400);
						logs.setResponseDescription(verification);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return res;
					}
				} else {
					GetLoanResponse resp = new GetLoanResponse();
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode((ResponseCodes.ERROR_MSISDN_CODE));
					logs.setResponseDescription(ResponseCodes.ERROR_MSISDN);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					USSDGWLoanReqMsg ussdGatewayLoanReqMsg = new USSDGWLoanReqMsg();
					ussdGatewayLoanReqMsg.setLoanAmount(
							((long) (Constants.MONEY_DIVIDEND * Double.parseDouble(cclient.getLoanAmount()))));
					ussdGatewayLoanReqMsg.setReqHeader(getRequestHeader());
					USSDGWLoanReqMsg.SubAccessCode subA = new SubAccessCode();
					subA.setPrimaryIdentity(cclient.getmsisdn());
					ussdGatewayLoanReqMsg.setSubAccessCode(subA);
					USSDGWLoanRspMsg response = USSDService.getInstance().ussdGatewayLoan(ussdGatewayLoanReqMsg);
					GetLoanResponse resp = new GetLoanResponse();
					if (response.getRspHeader().getReturnCode().equals("0000")) {
						if ((cclient.getBrandId().equalsIgnoreCase(Constants.CIN_BRAND_ID) || cclient.getBrandId().equalsIgnoreCase(Constants.KLASS_BRAND_ID)) && cclient.getSubscriberType().equalsIgnoreCase(Constants.PREPAID) )
						{
							logger.info("loan fee deducted on success");
							deductLoanProcessingFee(cclient.getmsisdn());
						}
						HLRBalanceServices service = new HLRBalanceServices();
						Prepaid balanceInfo = service.getBalance(cclient.getmsisdn(), Constants.USER_TYPE_PREPAID,null)
								.getPrepaid();
						resp.setNewCredit(Helper.getBakcellMoney(queryLoanLog(cclient.getmsisdn())));
						resp.setNewBalance(balanceInfo.getMainWallet().getAmount());
						resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
						logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.setAmount(cclient.getLoanAmount() + "");
						logs.setUserType(Constants.USER_TYPE_PREPAID);
						logs.updateLog(logs);
						return resp;
					} else {
						if ((cclient.getBrandId().equalsIgnoreCase(Constants.CIN_BRAND_ID) || cclient.getBrandId().equalsIgnoreCase(Constants.KLASS_BRAND_ID)) && cclient.getSubscriberType().equalsIgnoreCase(Constants.PREPAID) )
						{
							logger.info("loan fee deducted on failure");
							deductLoanProcessingFee(cclient.getmsisdn());
						}
						resp.setReturnCode(response.getRspHeader().getReturnCode());
						resp.setReturnMsg(response.getRspHeader().getReturnMsg());
						logs.setResponseDescription(ResponseCodes.GENERIC_ERROR_DES);
						logs.setResponseCode(response.getRspHeader().getReturnCode());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.setAmount(cclient.getLoanAmount() + "");
						logs.updateLog(logs);
						return resp;
					}
				} else {
					GetLoanResponse resp = new GetLoanResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		GetLoanResponse resp = new GetLoanResponse();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseDescription(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	public long queryLoanLog(String msisdn) {
		Long totalCredit = 0L;
		QueryLoanLog lo = new QueryLoanLog();
		QueryLoanLogResultMsg loanResponse = lo.queryLoanLog(msisdn);
		if (loanResponse.getResultHeader().getResultCode().equals("0")) {
			QueryLoanLogResult loanLog = loanResponse.getQueryLoanLogResult();
			if (loanLog != null && loanLog.getRepaymentLogDetail() != null) {
				List<LoanLogDetail> loanLogDetails = loanLog.getLoanLogDetail();
				for (int i = 0; i < loanLogDetails.size(); i++) {
					if (loanLogDetails.get(i).getLoanStatus().equalsIgnoreCase(Constants.LOAN_STATUS_OPEN)) {
						if (loanLogDetails.get(i).getPaidAMT() > 0)
							totalCredit += loanLogDetails.get(i).getRemainingAMT();
						else
							totalCredit += loanLogDetails.get(i).getRemainingAMT()
									+ loanLogDetails.get(i).getInitLoanPoundage();
					}
				}
			}
		}
		return totalCredit;
	}

	public ReqHeader getRequestHeader() {
		ReqHeader reqh = new ReqHeader();
		reqh.setAccessUser(ConfigurationManager.getConfigurationFromCache("ussd.reqh.AccessUser").trim());
		reqh.setChannelId(ConfigurationManager.getConfigurationFromCache("ussd.reqh.ChannelId").trim());
		reqh.setAccessPwd(ConfigurationManager.getConfigurationFromCache("ussd.reqh.AccessPwd").trim());
		reqh.setTransactionId(Helper.generateTransactionID());
		System.out.println("Request header sucesss");
		return reqh;
	}

	private void deductLoanProcessingFee(String msisdn) {
		LoanRequestFee loanRequestFee = new LoanRequestFee();
		try {
			logger.info(loanRequestFee.getLoanReqFeeResponse(msisdn));
		} catch (JAXBException e) {
			logger.error(Helper.GetException(e));
		}
	}
}
