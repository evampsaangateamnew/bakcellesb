package com.evampsaanga.bakcell.balance;

import com.evampsaanga.bakcell.requestheaders.BaseRequest;

public class BalanceRequest extends BaseRequest {

	private String customerType = "";  
	private String subscriberType = "";
	private String statusCode = "";

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

 
	public String getSubscriberType() {
		return subscriberType;
	}

	public void setSubscriberType(String subscriberType) {
		this.subscriberType = subscriberType;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

}
