package com.evampsaanga.ulduzumupdate.getmerchants;

import java.util.List;

public class Merchant {
	private int id;
	private String name;
	private int category_id;
	private String category_name;
	private String logo;
	private String coord_lat;
	private String coord_lng;
	private boolean isspecialdisc;
	private String discount;
	private List<Branches> branches;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the category_id
	 */
	public int getCategory_id() {
		return category_id;
	}

	/**
	 * @param category_id
	 *            the category_id to set
	 */
	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}

	/**
	 * @return the category_name
	 */
	public String getCategory_name() {
		return category_name;
	}

	/**
	 * @param category_name
	 *            the category_name to set
	 */
	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}

	/**
	 * @return the logo
	 */
	public String getLogo() {
		return logo;
	}

	/**
	 * @param logo
	 *            the logo to set
	 */
	public void setLogo(String logo) {
		this.logo = logo;
	}

	/**
	 * @return the coord_lat
	 */
	public String getCoord_lat() {
		return coord_lat;
	}

	/**
	 * @param coord_lat
	 *            the coord_lat to set
	 */
	public void setCoord_lat(String coord_lat) {
		this.coord_lat = coord_lat;
	}

	/**
	 * @return the coord_lng
	 */
	public String getCoord_lng() {
		return coord_lng;
	}

	/**
	 * @param coord_lng
	 *            the coord_lng to set
	 */
	public void setCoord_lng(String coord_lng) {
		this.coord_lng = coord_lng;
	}

	/**
	 * @return the isspecialdisc
	 */
	public boolean isIsspecialdisc() {
		return isspecialdisc;
	}

	/**
	 * @param isspecialdisc
	 *            the isspecialdisc to set
	 */
	public void setIsspecialdisc(boolean isspecialdisc) {
		this.isspecialdisc = isspecialdisc;
	}

	/**
	 * @return the discount
	 */
	public String getDiscount() {
		return discount;
	}

	/**
	 * @param discount
	 *            the discount to set
	 */
	public void setDiscount(String discount) {
		this.discount = discount;
	}

	/**
	 * @return the branches
	 */
	public List<Branches> getBranches() {
		return branches;
	}

	/**
	 * @param branches
	 *            the branches to set
	 */
	public void setBranches(List<Branches> branches) {
		this.branches = branches;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Merchant [id=" + id + ", name=" + name + ", category_id=" + category_id + ", category_name="
				+ category_name + ", logo=" + logo + ", coord_lat=" + coord_lat + ", coord_lng=" + coord_lng
				+ ", isspecialdisc=" + isspecialdisc + ", discount=" + discount + ", branches=" + branches + "]";
	}

}
