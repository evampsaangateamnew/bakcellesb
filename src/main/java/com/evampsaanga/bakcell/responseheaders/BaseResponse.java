package com.evampsaanga.bakcell.responseheaders;

public class BaseResponse {
	private String returnMsg = "";
	private String returnCode = "0";

	public String getReturnMsg() {
		return returnMsg;
	}

	public void setReturnMsg(String returnMsg) {
		this.returnMsg = returnMsg;
	}

	public String getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

	@Override
	public String toString() {
		return "BaseResponse [returnMsg=" + returnMsg + ", returnCode=" + returnCode + "]";
	}

}
