package com.evampsaanga.bakcell.termsandconditionsV2;

import com.evampsaanga.bakcell.requestheaders.BaseRequest;

public class TnCRequest extends BaseRequest{

	private String userName;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
}
