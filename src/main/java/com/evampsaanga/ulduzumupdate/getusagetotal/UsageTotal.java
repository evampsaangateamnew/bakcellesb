package com.evampsaanga.ulduzumupdate.getusagetotal;

public class UsageTotal {
	private String segment_type;
	private String last_unused_code;
	private int daily_limit_left;
	private double total_redemptions;
	private double total_amount;
	private double total_discounted;
	private double total_discount;
	private int daily_limit_total;

	public String getSegment_type() {
		return segment_type;
	}

	public void setSegment_type(String segment_type) {
		this.segment_type = segment_type;
	}

	public String getLast_unused_code() {
		return last_unused_code;
	}

	public void setLast_unused_code(String last_unused_code) {
		this.last_unused_code = last_unused_code;
	}

	public int getDaily_limit_left() {
		return daily_limit_left;
	}

	public void setDaily_limit_left(int daily_limit_left) {
		this.daily_limit_left = daily_limit_left;
	}

	public double getTotal_redemptions() {
		return total_redemptions;
	}

	public void setTotal_redemptions(double total_redemptions) {
		this.total_redemptions = total_redemptions;
	}

	public double getTotal_amount() {
		return total_amount;
	}

	public void setTotal_amount(double total_amount) {
		this.total_amount = total_amount;
	}

	public double getTotal_discounted() {
		return total_discounted;
	}

	public void setTotal_discounted(double total_discounted) {
		this.total_discounted = total_discounted;
	}

	public double getTotal_discount() {
		return total_discount;
	}

	public void setTotal_discount(double total_discount) {
		this.total_discount = total_discount;
	}

	public int getDaily_limit_total() {
		return daily_limit_total;
	}

	public void setDaily_limit_total(int daily_limit_total) {
		this.daily_limit_total = daily_limit_total;
	}

	@Override
	public String toString() {
		return "UsageTotal [segment_type=" + segment_type + ", last_unused_code=" + last_unused_code
				+ ", daily_limit_left=" + daily_limit_left + ", total_redemptions=" + total_redemptions
				+ ", total_amount=" + total_amount + ", total_discounted=" + total_discounted + ", total_discount="
				+ total_discount + ", daily_limit_total=" + daily_limit_total + "]";
	}

}
