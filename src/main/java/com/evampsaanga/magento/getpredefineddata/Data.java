package com.evampsaanga.magento.getpredefineddata;

public class Data {
	private Topup topup = new Topup();
	private Fnf fnf = new Fnf();

	/**
	 * @return the fnf
	 */
	public Fnf getFnf() {
		return fnf;
	}

	/**
	 * @param fnf
	 *            the fnf to set
	 */
	public void setFnf(Fnf fnf) {
		this.fnf = fnf;
	}

	/**
	 * @return the topup
	 */
	public Topup getTopup() {
		return topup;
	}

	/**
	 * @param topup
	 *            the topup to set
	 */
	public void setTopup(Topup topup) {
		this.topup = topup;
	}
}
