package com.huawei.crm.basetype;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for OrderItemList complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="OrderItemList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetOrderItemInfo" type="{http://crm.huawei.com/basetype/}GetOrderItemInfo" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderItemList", propOrder = { "getOrderItemInfo" })
public class OrderItemList {
	@XmlElement(name = "GetOrderItemInfo", required = true)
	protected List<GetOrderItemInfo> getOrderItemInfo;

	/**
	 * Gets the value of the getOrderItemInfo property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the getOrderItemInfo property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getGetOrderItemInfo().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link GetOrderItemInfo }
	 * 
	 * 
	 */
	public List<GetOrderItemInfo> getGetOrderItemInfo() {
		if (getOrderItemInfo == null) {
			getOrderItemInfo = new ArrayList<GetOrderItemInfo>();
		}
		return this.getOrderItemInfo;
	}
}
