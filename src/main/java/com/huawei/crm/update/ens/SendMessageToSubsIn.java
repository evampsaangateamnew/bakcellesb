package com.huawei.crm.update.ens;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for SendMessageToSubsIn complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="SendMessageToSubsIn">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ServiceNumber" type="{http://crm.huawei.com/basetype/}ServiceNumber" minOccurs="0"/>
 *         &lt;element name="TEXT" type="{http://crm.huawei.com/basetype/}Text"/>
 *         &lt;element name="SubscriberId" type="{http://crm.huawei.com/basetype/}SubscriberId" minOccurs="0"/>
 *         &lt;element name="ExternalSubscriberId" type="{http://crm.huawei.com/basetype/}ExternalSubscriberId" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SendMessageToSubsIn", propOrder = {})
public class SendMessageToSubsIn {
	@XmlElement(name = "ServiceNumber")
	protected String serviceNumber;
	@XmlElement(name = "TEXT", required = true)
	protected String text;
	@XmlElement(name = "SubscriberId")
	protected Long subscriberId;
	@XmlElement(name = "ExternalSubscriberId")
	protected String externalSubscriberId;

	/**
	 * Gets the value of the serviceNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getServiceNumber() {
		return serviceNumber;
	}

	/**
	 * Sets the value of the serviceNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setServiceNumber(String value) {
		this.serviceNumber = value;
	}

	/**
	 * Gets the value of the text property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTEXT() {
		return text;
	}

	/**
	 * Sets the value of the text property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTEXT(String value) {
		this.text = value;
	}

	/**
	 * Gets the value of the subscriberId property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getSubscriberId() {
		return subscriberId;
	}

	/**
	 * Sets the value of the subscriberId property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setSubscriberId(Long value) {
		this.subscriberId = value;
	}

	/**
	 * Gets the value of the externalSubscriberId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getExternalSubscriberId() {
		return externalSubscriberId;
	}

	/**
	 * Sets the value of the externalSubscriberId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setExternalSubscriberId(String value) {
		this.externalSubscriberId = value;
	}
}
