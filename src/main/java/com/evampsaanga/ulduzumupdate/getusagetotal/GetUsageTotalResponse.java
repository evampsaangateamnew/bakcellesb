package com.evampsaanga.ulduzumupdate.getusagetotal;

public class GetUsageTotalResponse {
	String result;
	UsageTotal data;

	/**
	 * @return the result
	 */
	public String getResult() {
		return result;
	}

	/**
	 * @param result
	 *            the result to set
	 */
	public void setResult(String result) {
		this.result = result;
	}

	public UsageTotal getData() {
		return data;
	}

	public void setData(UsageTotal data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "GetMerchantUlduzumResponse [result=" + result + ", data=" + data + "]";
	}

}
