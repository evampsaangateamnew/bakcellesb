package com.evampsaanga.bakcell.getcdrsbydate;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.amqimplementationsesb.LogsType;
import com.evampsaanga.bakcell.db.DBBakcellFactory;
import com.evampsaanga.bakcell.getcdrsoperationhistory.CDRsOperationDetails;
import com.evampsaanga.bakcell.getcdrsoperationhistory.CDRsOperationHistoryRequestResponse;
import com.evampsaanga.bakcell.getcdrssummary.GetCDRsSummaryRequestResponse;
import com.evampsaanga.bakcell.utilities.ConversionUtilities;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.ThirdPartyNames;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.developer.utils.SOAPLoggingHandler;

@Path("/bakcell/")
public class GetCDRsByDateLand 
{
	public static final Logger logger = Logger.getLogger("bakcellLogs-V2");
	
	public static HashMap<String, String> usageHistoryTranslationMapping = new HashMap<>();
	public static HashMap<String, String> cdrsColumnMapping = new HashMap<>();
	
	public static void populateUsageHistoryTranslationMapping()
	{
		populateColumnMapping();
		Connection myConnection = DBBakcellFactory.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String getAllTranslationQuery = "select * from E_CARE_ID_DESCRIPTIONS";
		try
		{
		statement = myConnection.prepareStatement(getAllTranslationQuery);
		resultSet = statement.executeQuery();
		String key = "";
		while(resultSet.next())
		{
			key = resultSet.getString("COLUMN_ID")+Constants.TRANSLATION_CDRS_KEY_SEPARATOR+resultSet.getString("TYPE_ID")+Constants.TRANSLATION_CDRS_KEY_SEPARATOR+resultSet.getString("ID")+Constants.TRANSLATION_CDRS_KEY_SEPARATOR+resultSet.getString("ID_LANG");
			usageHistoryTranslationMapping.put(key, resultSet.getString("DESCRIPTION"));
			logger.info("-----------------------------");
			logger.info("key: "+key+" : Value: "+resultSet.getString("DESCRIPTION"));
			logger.info("-----------------------------");
		}
		}catch (Exception e) {
			logger.error(Helper.GetException(e));
		}
		finally {
			try{
				if(resultSet != null)
					resultSet.close();
				if(statement != null)
					statement.close();
			}catch (Exception e) {
				logger.error(Helper.GetException(e));
			}
		}
	}
	
	public static void populateColumnMapping()
	{
		Connection myConnection = DBBakcellFactory.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String getAllTranslationQuery = "select * from E_CARE_ID_COLUMN";
		try
		{
		statement = myConnection.prepareStatement(getAllTranslationQuery);
		resultSet = statement.executeQuery();
		while(resultSet.next())
			if(resultSet.getString("DESCRIPTION")!=null)
			{
			cdrsColumnMapping.put(resultSet.getString("DESCRIPTION"),resultSet.getString("COLUMN_ID"));
			}
			else{
				cdrsColumnMapping.put("",resultSet.getString("COLUMN_ID"));
			}
		}catch (Exception e) {
			logger.error(Helper.GetException(e));
		}
		finally {
			try{
				if(resultSet != null)
					resultSet.close();
				if(statement != null)
					statement.close();
			}catch (Exception e) {
				logger.error(Helper.GetException(e));
			}
		}
	}

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetCDRsByDateRequestResponse Get(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.GET_CDRS_BY_DATE_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.GET_CDRS_BY_DATE);
		logs.setTableType(LogsType.GetCdrsByDate);
		try {
			logger.info("Request Landed on GetCDRsByDateLand:" + requestBody);
			GetCDRsByDateRequestClient cclient = new GetCDRsByDateRequestClient();
			try {
				cclient = Helper.JsonToObject(requestBody, GetCDRsByDateRequestClient.class);
				if (cclient != null) {
					if(cclient.getIsB2B()!=null && cclient.getIsB2B().equals("true"))
					{
						logs.setTransactionName(Transactions.GET_CDRS_BY_DATE_TRANSACTION_NAME_B2B);
					}
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setIsB2B(cclient.getIsB2B());
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				GetCDRsByDateRequestResponse resp = new GetCDRsByDateRequestResponse();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			try {
				if (cclient != null) {
					Date datestart = new SimpleDateFormat("yyyy-MM-dd").parse(cclient.getStartDate());
					Date dateend = new SimpleDateFormat("yyyy-MM-dd").parse(cclient.getEndDate());
					Date today = new Date();
					if (datestart.compareTo(dateend) > 0) {
						GetCDRsByDateRequestResponse resp = new GetCDRsByDateRequestResponse();
						resp.setReturnCode(ResponseCodes.START_DATE_GRATER_END_DATE_CODE);
						resp.setReturnMsg(ResponseCodes.START_DATE_GRATER_END_DATE_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
					else if(today.compareTo(datestart) < 0) {
						GetCDRsByDateRequestResponse resp = new GetCDRsByDateRequestResponse();
						resp.setReturnCode(ResponseCodes.START_DATE_CANNOT_BE_IN_FUTURE_CODE);
						resp.setReturnMsg(ResponseCodes.START_DATE_CANNOT_BE_IN_FUTURE_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
					else if(today.compareTo(dateend) < 0) {
						GetCDRsByDateRequestResponse resp = new GetCDRsByDateRequestResponse();
						resp.setReturnCode(ResponseCodes.END_DATE_CANNOT_BE_IN_FUTURE_CODE);
						resp.setReturnMsg(ResponseCodes.END_DATE_CANNOT_BE_IN_FUTURE_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				}
			} catch (Exception e) {
					logger.error(Helper.GetException(e));
				}
			if (cclient.getAccountId().equals("")) {
				GetCDRsByDateRequestResponse resp = new GetCDRsByDateRequestResponse();
				resp.setReturnCode(ResponseCodes.MISSING_PARAMETER_CODE);
				resp.setReturnMsg(ResponseCodes.MISSING_PARAMETER_DES);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					GetCDRsByDateRequestResponse resp = new GetCDRsByDateRequestResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						GetCDRsByDateRequestResponse res = new GetCDRsByDateRequestResponse();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						logs.setResponseCode(res.getReturnCode());
						logs.setResponseDescription(res.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return res;
					}
				} else {
					GetCDRsByDateRequestResponse resp = new GetCDRsByDateRequestResponse();
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					String lang = "2002";
					if(cclient.getLang().equalsIgnoreCase("3"))
						lang = "2002";
					if(cclient.getLang().equalsIgnoreCase("2"))
						lang = "2052";
					if(cclient.getLang().equalsIgnoreCase("4"))
						lang = "2060";
					 SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");

				        String start="00:00:00";
				        Date startime = simpleDateFormat.parse(start);
				        Date endtime = simpleDateFormat.parse("05:00:00");

				        //current time
				        String timeStamp = new SimpleDateFormat("HH:mm:ss").format(new Date());
				       
				        logger.info("TimeStamp :"+timeStamp);
				        Date current_time = simpleDateFormat.parse(timeStamp);

				    if (current_time.after(startime) && current_time.before(endtime)) {
				    	GetCDRsByDateRequestResponse resp = new GetCDRsByDateRequestResponse();
				    	logger.info("Time is between 00:00:00 and 05:00:00 ");
				    	resp.setReturnCode(ResponseCodes.ERROR_CODE_SERVICE_UNAVAILABLE_AT_12AM_TO_5AM);
						resp.setReturnMsg(ResponseCodes.ERROR_MESSAGE_SERVICE_UNAVAILABLE_AT_12AM_TO_5AM);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
				            
				      }
				    else
				    {
					return getResponse(cclient.getStartDate(), cclient.getEndDate(), cclient.getmsisdn(),
							cclient.getAccountId(), logs, cclient.getCustomerId(),lang);
				    }
				} else {
					GetCDRsByDateRequestResponse resp = new GetCDRsByDateRequestResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		GetCDRsByDateRequestResponse resp = new GetCDRsByDateRequestResponse();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	public CustomerID getCustomerIDFromView(String msisdn) {
		CustomerID customerId = new CustomerID();
		Connection myConnection = DBBakcellFactory.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			String sql = "select * from V_E_CARE_CUST_INFO t where PRI_IDENTITY = substr('" + msisdn + "',-9)";
			logger.info(sql);
			statement = myConnection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				customerId.setCbscustomerId(resultSet.getString("CBS_CUST_ID"));
				customerId.setCrmcustomerId(resultSet.getString("CRM_CUST_ID"));
			}
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		}
		return customerId;
	}

	public GetCDRsByDateRequestResponse getResponse(String startDate, String endDate, String msisdn, String accountId,
			Logs logs, String customerIdAPI,String lang) {
		CustomerID customerId = getCustomerIDFromView(msisdn);
		Connection myConnection = DBBakcellFactory.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			ArrayList<CDRDetails> list = new ArrayList<>();
			if (customerId != null && customerId.getCrmcustomerId() != null && customerId.getCrmcustomerId().equalsIgnoreCase(customerIdAPI)) {
				logger.info("MSISDN-" + msisdn + "-CustomerID From DB-" + customerId.getCrmcustomerId()
						+ "-CustomerID From API-" + customerIdAPI + "-ID matched.");
				startDate = startDate + " 00:00:00";
				endDate = endDate + " 23:59:59";
				String msisdnInt = Constants.AZERI_COUNTRY_CODE+msisdn;
				String sql = "select * from e_care_dev.v_e_care_cdr where e_care_dev.v_e_care_cdr.CUST_LOCAL_START_DATE >= to_date(?,'yyyy-mm-dd hh24:mi:ss') and e_care_dev.v_e_care_cdr.CUST_LOCAL_START_DATE <=" + " to_date(?,'yyyy-mm-dd hh24:mi:ss') and e_care_dev.v_e_care_cdr.PRI_IDENTITY=?";
				statement = myConnection.prepareStatement(sql);
				statement.setString(1, startDate);
				statement.setString(2, endDate);
				statement.setString(3, msisdnInt);
				logger.info(statement.toString());
				resultSet = statement.executeQuery();
				if(usageHistoryTranslationMapping.isEmpty())
					populateUsageHistoryTranslationMapping();
				while (resultSet.next()) {
					if(resultSet.getString("OWNER_CUST_ID").equalsIgnoreCase(customerId.getCbscustomerId()))
					{
					Date formattedStarDate = new SimpleDateFormat(Constants.SQL_DATE_FORMAT_required)
							.parse(resultSet.getString("CUST_LOCAL_START_DATE"));
					Date formattedEndDate = new SimpleDateFormat(Constants.SQL_DATE_FORMAT_required)
							.parse(resultSet.getString("CUST_LOCAL_END_DATE"));
					String service = "";
					String description = "";
					if(usageHistoryTranslationMapping.containsKey(cdrsColumnMapping.get("USG_SERV_TYPE_NAME")+Constants.TRANSLATION_CDRS_KEY_SEPARATOR+resultSet.getString("TYPE")+Constants.TRANSLATION_CDRS_KEY_SEPARATOR+resultSet.getString("USG_SERV_TYPE_NAME")+Constants.TRANSLATION_CDRS_KEY_SEPARATOR+lang))
						service = usageHistoryTranslationMapping.get(cdrsColumnMapping.get("USG_SERV_TYPE_NAME")+Constants.TRANSLATION_CDRS_KEY_SEPARATOR+resultSet.getString("TYPE")+Constants.TRANSLATION_CDRS_KEY_SEPARATOR+resultSet.getString("USG_SERV_TYPE_NAME")+Constants.TRANSLATION_CDRS_KEY_SEPARATOR+lang);
					else
						service = resultSet.getString("USG_SERV_TYPE_NAME");
					if(usageHistoryTranslationMapping.containsKey(cdrsColumnMapping.get("SPECIALZONEID")+Constants.TRANSLATION_CDRS_KEY_SEPARATOR+resultSet.getString("TYPE")+Constants.TRANSLATION_CDRS_KEY_SEPARATOR+resultSet.getString("SPECIALZONEID")+Constants.TRANSLATION_CDRS_KEY_SEPARATOR+lang))
						description = usageHistoryTranslationMapping.get(cdrsColumnMapping.get("SPECIALZONEID")+Constants.TRANSLATION_CDRS_KEY_SEPARATOR+resultSet.getString("TYPE")+Constants.TRANSLATION_CDRS_KEY_SEPARATOR+resultSet.getString("SPECIALZONEID")+Constants.TRANSLATION_CDRS_KEY_SEPARATOR+lang);
					else
						description = resultSet.getString("SPECIALZONEID");
					logger.info("================================================================");
					logger.info("DESCRIPTION: "+description);
					logger.info("USG_SERV_TYPE_NAME: "+resultSet.getString("USG_SERV_TYPE_NAME"));
					logger.info("TYPE: "+resultSet.getString("TYPE"));
					logger.info("CARRIER_NAME: "+resultSet.getString("CARRIER_NAME"));
					logger.info("ACTUAL_USAGE: "+resultSet.getString("ACTUAL_USAGE"));
					logger.info("MEASURE_NAME: "+resultSet.getString("MEASURE_NAME"));
					logger.info("USG_SERV_TYPE_NAME: "+resultSet.getString("USG_SERV_TYPE_NAME"));
					logger.info("SPECIALZONEID: "+resultSet.getString("SPECIALZONEID"));
					logger.info("CHARGEABLE_AMOUNT: "+ConversionUtilities.numberFormattor(resultSet.getString("CHARGEABLE_AMOUNT")));
					logger.info("================================================================");
					
					list.add(new CDRDetails(resultSet.getString("RECIPIENT_NUMBER"),
							new SimpleDateFormat(Constants.SQL_DATE_FORMAT_History).format(formattedStarDate),
							new SimpleDateFormat(Constants.SQL_DATE_FORMAT_History).format(formattedEndDate),
							service, 
							usageHistoryTranslationMapping.get(cdrsColumnMapping.get("TIME_TYPE")+Constants.TRANSLATION_CDRS_KEY_SEPARATOR+resultSet.getString("TYPE")+Constants.TRANSLATION_CDRS_KEY_SEPARATOR+resultSet.getString("TIME_TYPE")+Constants.TRANSLATION_CDRS_KEY_SEPARATOR+lang),
							resultSet.getString("CARRIER_NAME"),
							resultSet.getString("ACTUAL_USAGE") + " " + usageHistoryTranslationMapping.get(cdrsColumnMapping.get("MEASURE_NAME")+Constants.TRANSLATION_CDRS_KEY_SEPARATOR+resultSet.getString("TYPE")+Constants.TRANSLATION_CDRS_KEY_SEPARATOR+resultSet.getString("MEASURE_NAME")+Constants.TRANSLATION_CDRS_KEY_SEPARATOR+lang),
							ConversionUtilities.numberFormattor(resultSet.getString("CHARGEABLE_AMOUNT")),
							resultSet.getString("USG_SERV_TYPE_NAME"), 
							description));
				}
				}
			} else
				logger.error("MSISDN-" + msisdn + "-CustomerID From DB-" + customerId.getCrmcustomerId()
						+ "-CustomerID From API-" + customerIdAPI + "-ID not matched.");
			
			String voiceList[] = ConfigurationManager.getConfigurationFromCache("cdr.sum.VOICE").split(",");
			String smsList[] = ConfigurationManager.getConfigurationFromCache("cdr.sum.SMS").split(",");
			String dataList[] = ConfigurationManager.getConfigurationFromCache("cdr.sum.DATA").split(",");
			for(int i=0;i<list.size();i++)
			{
				for(int j=0;j<voiceList.length;j++)
					if(list.get(i).getType() != null && list.get(i).getType().equalsIgnoreCase(voiceList[j]))
						list.get(i).setType(Constants.VOICE);
			
				for(int j=0;j<smsList.length;j++)
					if(list.get(i).getType() != null && list.get(i).getType().equalsIgnoreCase(smsList[j]))
						list.get(i).setType(Constants.SMS);
			
				for(int j=0;j<dataList.length;j++)
					if(list.get(i).getType() != null && list.get(i).getType().equalsIgnoreCase(dataList[j]))
						list.get(i).setType(Constants.DATA);
				if(list.get(i).getType() == null || ( !list.get(i).getType().equalsIgnoreCase(Constants.VOICE) && !list.get(i).getType().equalsIgnoreCase(Constants.SMS) && !list.get(i).getType().equalsIgnoreCase(Constants.DATA)))
					list.get(i).setType(Constants.OTHER);
			}
			
			GetCDRsByDateRequestResponse rep11 = new GetCDRsByDateRequestResponse();
			
			Collections.sort(list, new Comparator<CDRDetails>() {
				  public int compare(CDRDetails o1, CDRDetails o2) {
					  Date d1 = null;
					  Date d2 = null;
					  if (o1.getStartDateTime() == null || o2.getStartDateTime() == null)
				        return 0;
				      try {
						d1 = new SimpleDateFormat("dd/MM/yy HH:mm:ss").parse(o1.getStartDateTime());
						d2 = new SimpleDateFormat("dd/MM/yy HH:mm:ss").parse(o2.getStartDateTime());
					} catch (ParseException e) {
						logger.error(Helper.GetException(e));
					}
				      return -1 * (d1.compareTo(d2));
				  }
				});
			
			rep11.setRecords(list);
			rep11.setReturnCode(ResponseCodes.SUCESS_CODE_200);
			rep11.setReturnMsg(ResponseCodes.SUCESS_DES_200);
			logs.setResponseCode(rep11.getReturnCode());
			logs.setResponseDescription(rep11.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return rep11;
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}
			} catch (Exception ex2) {
				logger.error(Helper.GetException(ex2));
			}
			try {
				if (statement != null) {
					statement.close();
				}
			} catch (Exception ex2) {
				logger.error(Helper.GetException(ex2));
			}
		}
		GetCDRsByDateRequestResponse resp = new GetCDRsByDateRequestResponse();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.updateLog(logs);
		return resp;
	}
}
