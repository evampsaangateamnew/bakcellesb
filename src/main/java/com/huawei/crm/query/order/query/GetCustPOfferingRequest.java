
package com.huawei.crm.query.order.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.order.query.RequestHeader;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequestHeader" type="{http://crm.huawei.com/basetype/}RequestHeader"/>
 *         &lt;element name="GetCustPOfferingBody" type="{http://crm.huawei.com/query/}GetCustPOfferingIn"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "requestHeader",
    "getCustPOfferingBody"
})
@XmlRootElement(name = "GetCustPOfferingRequest")
public class GetCustPOfferingRequest {

    @XmlElement(name = "RequestHeader", required = true)
    protected RequestHeader requestHeader;
    @XmlElement(name = "GetCustPOfferingBody", required = true)
    protected GetCustPOfferingIn getCustPOfferingBody;

    /**
     * Gets the value of the requestHeader property.
     * 
     * @return
     *     possible object is
     *     {@link RequestHeader }
     *     
     */
    public RequestHeader getRequestHeader() {
        return requestHeader;
    }

    /**
     * Sets the value of the requestHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestHeader }
     *     
     */
    public void setRequestHeader(RequestHeader value) {
        this.requestHeader = value;
    }

    /**
     * Gets the value of the getCustPOfferingBody property.
     * 
     * @return
     *     possible object is
     *     {@link GetCustPOfferingIn }
     *     
     */
    public GetCustPOfferingIn getGetCustPOfferingBody() {
        return getCustPOfferingBody;
    }

    /**
     * Sets the value of the getCustPOfferingBody property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetCustPOfferingIn }
     *     
     */
    public void setGetCustPOfferingBody(GetCustPOfferingIn value) {
        this.getCustPOfferingBody = value;
    }

}
