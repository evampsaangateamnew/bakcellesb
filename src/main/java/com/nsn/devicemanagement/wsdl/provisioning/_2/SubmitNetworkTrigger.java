package com.nsn.devicemanagement.wsdl.provisioning._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for submitNetworkTrigger complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="submitNetworkTrigger">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="msisdn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="imei" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="imsi" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "submitNetworkTrigger", propOrder = { "msisdn", "imei", "imsi" })
public class SubmitNetworkTrigger {
	protected String msisdn;
	protected String imei;
	protected String imsi;

	/**
	 * Gets the value of the msisdn property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMsisdn() {
		return msisdn;
	}

	/**
	 * Sets the value of the msisdn property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMsisdn(String value) {
		this.msisdn = value;
	}

	/**
	 * Gets the value of the imei property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getImei() {
		return imei;
	}

	/**
	 * Sets the value of the imei property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setImei(String value) {
		this.imei = value;
	}

	/**
	 * Gets the value of the imsi property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getImsi() {
		return imsi;
	}

	/**
	 * Sets the value of the imsi property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setImsi(String value) {
		this.imsi = value;
	}
}
