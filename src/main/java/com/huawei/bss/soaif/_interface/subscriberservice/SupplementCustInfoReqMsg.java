package com.huawei.bss.soaif._interface.subscriberservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bss.soaif._interface.common.Name;
import com.huawei.bss.soaif._interface.common.ObjectAccessInfo;
import com.huawei.bss.soaif._interface.common.ReqHeader;
import com.huawei.bss.soaif._interface.common.SimpleProperty;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.huawei.com/bss/soaif/interface/common/}ReqHeader"/>
 *         &lt;sequence>
 *           &lt;element name="AccessInfo">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}ObjectAccessInfo">
 *                 &lt;/extension>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="ExistingCust" minOccurs="0">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="CustId" type="{http://www.huawei.com/bss/soaif/interface/common/}CustId"/>
 *                     &lt;element name="AcctId" type="{http://www.huawei.com/bss/soaif/interface/common/}AcctId"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="Customer" minOccurs="0">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="CustId" type="{http://www.huawei.com/bss/soaif/interface/common/}CustId"/>
 *                     &lt;element name="Title" type="{http://www.huawei.com/bss/soaif/interface/common/}Title" minOccurs="0"/>
 *                     &lt;element name="Name" type="{http://www.huawei.com/bss/soaif/interface/common/}Name" minOccurs="0"/>
 *                     &lt;element name="Nationality" type="{http://www.huawei.com/bss/soaif/interface/common/}Nationality" minOccurs="0"/>
 *                     &lt;element name="Birthday" type="{http://www.huawei.com/bss/soaif/interface/common/}Date" minOccurs="0"/>
 *                     &lt;element name="Contact" maxOccurs="unbounded" minOccurs="0">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}Contact">
 *                             &lt;sequence>
 *                               &lt;element name="ActionType" type="{http://www.huawei.com/bss/soaif/interface/common/}ActionType"/>
 *                             &lt;/sequence>
 *                           &lt;/extension>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="Address" maxOccurs="unbounded" minOccurs="0">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}Address">
 *                             &lt;sequence>
 *                               &lt;element name="ActionType" type="{http://www.huawei.com/bss/soaif/interface/common/}ActionType"/>
 *                             &lt;/sequence>
 *                           &lt;/extension>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="Account" minOccurs="0">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="AcctId" type="{http://www.huawei.com/bss/soaif/interface/common/}AcctId"/>
 *                     &lt;element name="AcctName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="BillLanguage" type="{http://www.huawei.com/bss/soaif/interface/common/}Language" minOccurs="0"/>
 *                     &lt;element name="Title" type="{http://www.huawei.com/bss/soaif/interface/common/}Title" minOccurs="0"/>
 *                     &lt;element name="Name" type="{http://www.huawei.com/bss/soaif/interface/common/}Name" minOccurs="0"/>
 *                     &lt;element name="Contact" minOccurs="0">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}Contact">
 *                             &lt;sequence>
 *                               &lt;element name="ActionType" type="{http://www.huawei.com/bss/soaif/interface/common/}ActionType"/>
 *                             &lt;/sequence>
 *                           &lt;/extension>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="Address" maxOccurs="unbounded" minOccurs="0">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}Address">
 *                             &lt;sequence>
 *                               &lt;element name="ActionType" type="{http://www.huawei.com/bss/soaif/interface/common/}ActionType"/>
 *                             &lt;/sequence>
 *                           &lt;/extension>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="BillCycleType" type="{http://www.huawei.com/bss/soaif/interface/common/}BillCycleType" minOccurs="0"/>
 *                     &lt;element name="AcctPayMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="PaymentChannel" maxOccurs="unbounded" minOccurs="0">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}PaymentChannel">
 *                             &lt;sequence>
 *                               &lt;element name="ActionType" type="{http://www.huawei.com/bss/soaif/interface/common/}ActionType"/>
 *                             &lt;/sequence>
 *                           &lt;/extension>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="BillMedium" maxOccurs="unbounded" minOccurs="0">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}BillMedium">
 *                             &lt;sequence>
 *                               &lt;element name="ActionType" type="{http://www.huawei.com/bss/soaif/interface/common/}ActionType"/>
 *                             &lt;/sequence>
 *                           &lt;/extension>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="IfActivateSub" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "reqHeader", "accessInfo", "existingCust", "customer", "account", "ifActivateSub" })
@XmlRootElement(name = "SupplementCustInfoReqMsg")
public class SupplementCustInfoReqMsg {
	@XmlElement(name = "ReqHeader", namespace = "http://www.huawei.com/bss/soaif/interface/common/", required = true)
	protected ReqHeader reqHeader;
	@XmlElement(name = "AccessInfo", required = true)
	protected SupplementCustInfoReqMsg.AccessInfo accessInfo;
	@XmlElement(name = "ExistingCust")
	protected SupplementCustInfoReqMsg.ExistingCust existingCust;
	@XmlElement(name = "Customer")
	protected SupplementCustInfoReqMsg.Customer customer;
	@XmlElement(name = "Account")
	protected SupplementCustInfoReqMsg.Account account;
	@XmlElement(name = "IfActivateSub")
	protected String ifActivateSub;

	/**
	 * Gets the value of the reqHeader property.
	 * 
	 * @return possible object is {@link ReqHeader }
	 * 
	 */
	public ReqHeader getReqHeader() {
		return reqHeader;
	}

	/**
	 * Sets the value of the reqHeader property.
	 * 
	 * @param value
	 *            allowed object is {@link ReqHeader }
	 * 
	 */
	public void setReqHeader(ReqHeader value) {
		this.reqHeader = value;
	}

	/**
	 * Gets the value of the accessInfo property.
	 * 
	 * @return possible object is {@link SupplementCustInfoReqMsg.AccessInfo }
	 * 
	 */
	public SupplementCustInfoReqMsg.AccessInfo getAccessInfo() {
		return accessInfo;
	}

	/**
	 * Sets the value of the accessInfo property.
	 * 
	 * @param value
	 *            allowed object is {@link SupplementCustInfoReqMsg.AccessInfo }
	 * 
	 */
	public void setAccessInfo(SupplementCustInfoReqMsg.AccessInfo value) {
		this.accessInfo = value;
	}

	/**
	 * Gets the value of the existingCust property.
	 * 
	 * @return possible object is {@link SupplementCustInfoReqMsg.ExistingCust }
	 * 
	 */
	public SupplementCustInfoReqMsg.ExistingCust getExistingCust() {
		return existingCust;
	}

	/**
	 * Sets the value of the existingCust property.
	 * 
	 * @param value
	 *            allowed object is
	 *            {@link SupplementCustInfoReqMsg.ExistingCust }
	 * 
	 */
	public void setExistingCust(SupplementCustInfoReqMsg.ExistingCust value) {
		this.existingCust = value;
	}

	/**
	 * Gets the value of the customer property.
	 * 
	 * @return possible object is {@link SupplementCustInfoReqMsg.Customer }
	 * 
	 */
	public SupplementCustInfoReqMsg.Customer getCustomer() {
		return customer;
	}

	/**
	 * Sets the value of the customer property.
	 * 
	 * @param value
	 *            allowed object is {@link SupplementCustInfoReqMsg.Customer }
	 * 
	 */
	public void setCustomer(SupplementCustInfoReqMsg.Customer value) {
		this.customer = value;
	}

	/**
	 * Gets the value of the account property.
	 * 
	 * @return possible object is {@link SupplementCustInfoReqMsg.Account }
	 * 
	 */
	public SupplementCustInfoReqMsg.Account getAccount() {
		return account;
	}

	/**
	 * Sets the value of the account property.
	 * 
	 * @param value
	 *            allowed object is {@link SupplementCustInfoReqMsg.Account }
	 * 
	 */
	public void setAccount(SupplementCustInfoReqMsg.Account value) {
		this.account = value;
	}

	/**
	 * Gets the value of the ifActivateSub property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIfActivateSub() {
		return ifActivateSub;
	}

	/**
	 * Sets the value of the ifActivateSub property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIfActivateSub(String value) {
		this.ifActivateSub = value;
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}ObjectAccessInfo">
	 *     &lt;/extension>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "")
	public static class AccessInfo extends ObjectAccessInfo {
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="AcctId" type="{http://www.huawei.com/bss/soaif/interface/common/}AcctId"/>
	 *         &lt;element name="AcctName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="BillLanguage" type="{http://www.huawei.com/bss/soaif/interface/common/}Language" minOccurs="0"/>
	 *         &lt;element name="Title" type="{http://www.huawei.com/bss/soaif/interface/common/}Title" minOccurs="0"/>
	 *         &lt;element name="Name" type="{http://www.huawei.com/bss/soaif/interface/common/}Name" minOccurs="0"/>
	 *         &lt;element name="Contact" minOccurs="0">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}Contact">
	 *                 &lt;sequence>
	 *                   &lt;element name="ActionType" type="{http://www.huawei.com/bss/soaif/interface/common/}ActionType"/>
	 *                 &lt;/sequence>
	 *               &lt;/extension>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="Address" maxOccurs="unbounded" minOccurs="0">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}Address">
	 *                 &lt;sequence>
	 *                   &lt;element name="ActionType" type="{http://www.huawei.com/bss/soaif/interface/common/}ActionType"/>
	 *                 &lt;/sequence>
	 *               &lt;/extension>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="BillCycleType" type="{http://www.huawei.com/bss/soaif/interface/common/}BillCycleType" minOccurs="0"/>
	 *         &lt;element name="AcctPayMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="PaymentChannel" maxOccurs="unbounded" minOccurs="0">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}PaymentChannel">
	 *                 &lt;sequence>
	 *                   &lt;element name="ActionType" type="{http://www.huawei.com/bss/soaif/interface/common/}ActionType"/>
	 *                 &lt;/sequence>
	 *               &lt;/extension>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="BillMedium" maxOccurs="unbounded" minOccurs="0">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}BillMedium">
	 *                 &lt;sequence>
	 *                   &lt;element name="ActionType" type="{http://www.huawei.com/bss/soaif/interface/common/}ActionType"/>
	 *                 &lt;/sequence>
	 *               &lt;/extension>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "acctId", "acctName", "billLanguage", "title", "name", "contact", "address",
			"billCycleType", "acctPayMethod", "paymentChannel", "billMedium", "additionalProperty" })
	public static class Account {
		@XmlElement(name = "AcctId", required = true)
		protected String acctId;
		@XmlElement(name = "AcctName")
		protected String acctName;
		@XmlElement(name = "BillLanguage")
		protected String billLanguage;
		@XmlElementRef(name = "Title", namespace = "http://www.huawei.com/bss/soaif/interface/SubscriberService/", type = JAXBElement.class, required = false)
		protected JAXBElement<String> title;
		@XmlElement(name = "Name")
		protected Name name;
		@XmlElement(name = "Contact")
		protected SupplementCustInfoReqMsg.Account.Contact contact;
		@XmlElement(name = "Address")
		protected List<SupplementCustInfoReqMsg.Account.Address> address;
		@XmlElement(name = "BillCycleType")
		protected String billCycleType;
		@XmlElement(name = "AcctPayMethod")
		protected String acctPayMethod;
		@XmlElement(name = "PaymentChannel")
		protected List<SupplementCustInfoReqMsg.Account.PaymentChannel> paymentChannel;
		@XmlElement(name = "BillMedium")
		protected List<SupplementCustInfoReqMsg.Account.BillMedium> billMedium;
		@XmlElement(name = "AdditionalProperty")
		protected List<SimpleProperty> additionalProperty;

		/**
		 * Gets the value of the acctId property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getAcctId() {
			return acctId;
		}

		/**
		 * Sets the value of the acctId property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setAcctId(String value) {
			this.acctId = value;
		}

		/**
		 * Gets the value of the acctName property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getAcctName() {
			return acctName;
		}

		/**
		 * Sets the value of the acctName property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setAcctName(String value) {
			this.acctName = value;
		}

		/**
		 * Gets the value of the billLanguage property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getBillLanguage() {
			return billLanguage;
		}

		/**
		 * Sets the value of the billLanguage property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setBillLanguage(String value) {
			this.billLanguage = value;
		}

		/**
		 * Gets the value of the title property.
		 * 
		 * @return possible object is {@link JAXBElement }{@code <}{@link String
		 *         }{@code >}
		 * 
		 */
		public JAXBElement<String> getTitle() {
			return title;
		}

		/**
		 * Sets the value of the title property.
		 * 
		 * @param value
		 *            allowed object is {@link JAXBElement
		 *            }{@code <}{@link String }{@code >}
		 * 
		 */
		public void setTitle(JAXBElement<String> value) {
			this.title = value;
		}

		/**
		 * Gets the value of the name property.
		 * 
		 * @return possible object is {@link Name }
		 * 
		 */
		public Name getName() {
			return name;
		}

		/**
		 * Sets the value of the name property.
		 * 
		 * @param value
		 *            allowed object is {@link Name }
		 * 
		 */
		public void setName(Name value) {
			this.name = value;
		}

		/**
		 * Gets the value of the contact property.
		 * 
		 * @return possible object is
		 *         {@link SupplementCustInfoReqMsg.Account.Contact }
		 * 
		 */
		public SupplementCustInfoReqMsg.Account.Contact getContact() {
			return contact;
		}

		/**
		 * Sets the value of the contact property.
		 * 
		 * @param value
		 *            allowed object is
		 *            {@link SupplementCustInfoReqMsg.Account.Contact }
		 * 
		 */
		public void setContact(SupplementCustInfoReqMsg.Account.Contact value) {
			this.contact = value;
		}

		/**
		 * Gets the value of the address property.
		 * 
		 * <p>
		 * This accessor method returns a reference to the live list, not a
		 * snapshot. Therefore any modification you make to the returned list
		 * will be present inside the JAXB object. This is why there is not a
		 * <CODE>set</CODE> method for the address property.
		 * 
		 * <p>
		 * For example, to add a new item, do as follows:
		 * 
		 * <pre>
		 * getAddress().add(newItem);
		 * </pre>
		 * 
		 * 
		 * <p>
		 * Objects of the following type(s) are allowed in the list
		 * {@link SupplementCustInfoReqMsg.Account.Address }
		 * 
		 * 
		 */
		public List<SupplementCustInfoReqMsg.Account.Address> getAddress() {
			if (address == null) {
				address = new ArrayList<SupplementCustInfoReqMsg.Account.Address>();
			}
			return this.address;
		}

		/**
		 * Gets the value of the billCycleType property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getBillCycleType() {
			return billCycleType;
		}

		/**
		 * Sets the value of the billCycleType property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setBillCycleType(String value) {
			this.billCycleType = value;
		}

		/**
		 * Gets the value of the acctPayMethod property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getAcctPayMethod() {
			return acctPayMethod;
		}

		/**
		 * Sets the value of the acctPayMethod property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setAcctPayMethod(String value) {
			this.acctPayMethod = value;
		}

		/**
		 * Gets the value of the paymentChannel property.
		 * 
		 * <p>
		 * This accessor method returns a reference to the live list, not a
		 * snapshot. Therefore any modification you make to the returned list
		 * will be present inside the JAXB object. This is why there is not a
		 * <CODE>set</CODE> method for the paymentChannel property.
		 * 
		 * <p>
		 * For example, to add a new item, do as follows:
		 * 
		 * <pre>
		 * getPaymentChannel().add(newItem);
		 * </pre>
		 * 
		 * 
		 * <p>
		 * Objects of the following type(s) are allowed in the list
		 * {@link SupplementCustInfoReqMsg.Account.PaymentChannel }
		 * 
		 * 
		 */
		public List<SupplementCustInfoReqMsg.Account.PaymentChannel> getPaymentChannel() {
			if (paymentChannel == null) {
				paymentChannel = new ArrayList<SupplementCustInfoReqMsg.Account.PaymentChannel>();
			}
			return this.paymentChannel;
		}

		/**
		 * Gets the value of the billMedium property.
		 * 
		 * <p>
		 * This accessor method returns a reference to the live list, not a
		 * snapshot. Therefore any modification you make to the returned list
		 * will be present inside the JAXB object. This is why there is not a
		 * <CODE>set</CODE> method for the billMedium property.
		 * 
		 * <p>
		 * For example, to add a new item, do as follows:
		 * 
		 * <pre>
		 * getBillMedium().add(newItem);
		 * </pre>
		 * 
		 * 
		 * <p>
		 * Objects of the following type(s) are allowed in the list
		 * {@link SupplementCustInfoReqMsg.Account.BillMedium }
		 * 
		 * 
		 */
		public List<SupplementCustInfoReqMsg.Account.BillMedium> getBillMedium() {
			if (billMedium == null) {
				billMedium = new ArrayList<SupplementCustInfoReqMsg.Account.BillMedium>();
			}
			return this.billMedium;
		}

		/**
		 * Gets the value of the additionalProperty property.
		 * 
		 * <p>
		 * This accessor method returns a reference to the live list, not a
		 * snapshot. Therefore any modification you make to the returned list
		 * will be present inside the JAXB object. This is why there is not a
		 * <CODE>set</CODE> method for the additionalProperty property.
		 * 
		 * <p>
		 * For example, to add a new item, do as follows:
		 * 
		 * <pre>
		 * getAdditionalProperty().add(newItem);
		 * </pre>
		 * 
		 * 
		 * <p>
		 * Objects of the following type(s) are allowed in the list
		 * {@link SimpleProperty }
		 * 
		 * 
		 */
		public List<SimpleProperty> getAdditionalProperty() {
			if (additionalProperty == null) {
				additionalProperty = new ArrayList<SimpleProperty>();
			}
			return this.additionalProperty;
		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * 
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}Address">
		 *       &lt;sequence>
		 *         &lt;element name="ActionType" type="{http://www.huawei.com/bss/soaif/interface/common/}ActionType"/>
		 *       &lt;/sequence>
		 *     &lt;/extension>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "actionType" })
		public static class Address extends com.huawei.bss.soaif._interface.common.Address {
			@XmlElement(name = "ActionType", required = true)
			protected String actionType;

			/**
			 * Gets the value of the actionType property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getActionType() {
				return actionType;
			}

			/**
			 * Sets the value of the actionType property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setActionType(String value) {
				this.actionType = value;
			}
		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * 
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}BillMedium">
		 *       &lt;sequence>
		 *         &lt;element name="ActionType" type="{http://www.huawei.com/bss/soaif/interface/common/}ActionType"/>
		 *       &lt;/sequence>
		 *     &lt;/extension>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "actionType" })
		public static class BillMedium extends com.huawei.bss.soaif._interface.common.BillMedium {
			@XmlElement(name = "ActionType", required = true)
			protected String actionType;

			/**
			 * Gets the value of the actionType property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getActionType() {
				return actionType;
			}

			/**
			 * Sets the value of the actionType property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setActionType(String value) {
				this.actionType = value;
			}
		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * 
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}Contact">
		 *       &lt;sequence>
		 *         &lt;element name="ActionType" type="{http://www.huawei.com/bss/soaif/interface/common/}ActionType"/>
		 *       &lt;/sequence>
		 *     &lt;/extension>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "actionType" })
		public static class Contact extends com.huawei.bss.soaif._interface.common.Contact {
			@XmlElement(name = "ActionType", required = true)
			protected String actionType;

			/**
			 * Gets the value of the actionType property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getActionType() {
				return actionType;
			}

			/**
			 * Sets the value of the actionType property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setActionType(String value) {
				this.actionType = value;
			}
		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * 
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}PaymentChannel">
		 *       &lt;sequence>
		 *         &lt;element name="ActionType" type="{http://www.huawei.com/bss/soaif/interface/common/}ActionType"/>
		 *       &lt;/sequence>
		 *     &lt;/extension>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "actionType" })
		public static class PaymentChannel extends com.huawei.bss.soaif._interface.common.PaymentChannel {
			@XmlElement(name = "ActionType", required = true)
			protected String actionType;

			/**
			 * Gets the value of the actionType property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getActionType() {
				return actionType;
			}

			/**
			 * Sets the value of the actionType property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setActionType(String value) {
				this.actionType = value;
			}
		}
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="CustId" type="{http://www.huawei.com/bss/soaif/interface/common/}CustId"/>
	 *         &lt;element name="Title" type="{http://www.huawei.com/bss/soaif/interface/common/}Title" minOccurs="0"/>
	 *         &lt;element name="Name" type="{http://www.huawei.com/bss/soaif/interface/common/}Name" minOccurs="0"/>
	 *         &lt;element name="Nationality" type="{http://www.huawei.com/bss/soaif/interface/common/}Nationality" minOccurs="0"/>
	 *         &lt;element name="Birthday" type="{http://www.huawei.com/bss/soaif/interface/common/}Date" minOccurs="0"/>
	 *         &lt;element name="Contact" maxOccurs="unbounded" minOccurs="0">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}Contact">
	 *                 &lt;sequence>
	 *                   &lt;element name="ActionType" type="{http://www.huawei.com/bss/soaif/interface/common/}ActionType"/>
	 *                 &lt;/sequence>
	 *               &lt;/extension>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="Address" maxOccurs="unbounded" minOccurs="0">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}Address">
	 *                 &lt;sequence>
	 *                   &lt;element name="ActionType" type="{http://www.huawei.com/bss/soaif/interface/common/}ActionType"/>
	 *                 &lt;/sequence>
	 *               &lt;/extension>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "custId", "title", "name", "nationality", "birthday", "contact", "address",
			"additionalProperty" })
	public static class Customer {
		@XmlElement(name = "CustId", required = true)
		protected String custId;
		@XmlElementRef(name = "Title", namespace = "http://www.huawei.com/bss/soaif/interface/SubscriberService/", type = JAXBElement.class, required = false)
		protected JAXBElement<String> title;
		@XmlElement(name = "Name")
		protected Name name;
		@XmlElementRef(name = "Nationality", namespace = "http://www.huawei.com/bss/soaif/interface/SubscriberService/", type = JAXBElement.class, required = false)
		protected JAXBElement<String> nationality;
		@XmlElement(name = "Birthday")
		protected String birthday;
		@XmlElement(name = "Contact")
		protected List<SupplementCustInfoReqMsg.Customer.Contact> contact;
		@XmlElement(name = "Address")
		protected List<SupplementCustInfoReqMsg.Customer.Address> address;
		@XmlElement(name = "AdditionalProperty")
		protected List<SimpleProperty> additionalProperty;

		/**
		 * Gets the value of the custId property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getCustId() {
			return custId;
		}

		/**
		 * Sets the value of the custId property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setCustId(String value) {
			this.custId = value;
		}

		/**
		 * Gets the value of the title property.
		 * 
		 * @return possible object is {@link JAXBElement }{@code <}{@link String
		 *         }{@code >}
		 * 
		 */
		public JAXBElement<String> getTitle() {
			return title;
		}

		/**
		 * Sets the value of the title property.
		 * 
		 * @param value
		 *            allowed object is {@link JAXBElement
		 *            }{@code <}{@link String }{@code >}
		 * 
		 */
		public void setTitle(JAXBElement<String> value) {
			this.title = value;
		}

		/**
		 * Gets the value of the name property.
		 * 
		 * @return possible object is {@link Name }
		 * 
		 */
		public Name getName() {
			return name;
		}

		/**
		 * Sets the value of the name property.
		 * 
		 * @param value
		 *            allowed object is {@link Name }
		 * 
		 */
		public void setName(Name value) {
			this.name = value;
		}

		/**
		 * Gets the value of the nationality property.
		 * 
		 * @return possible object is {@link JAXBElement }{@code <}{@link String
		 *         }{@code >}
		 * 
		 */
		public JAXBElement<String> getNationality() {
			return nationality;
		}

		/**
		 * Sets the value of the nationality property.
		 * 
		 * @param value
		 *            allowed object is {@link JAXBElement
		 *            }{@code <}{@link String }{@code >}
		 * 
		 */
		public void setNationality(JAXBElement<String> value) {
			this.nationality = value;
		}

		/**
		 * Gets the value of the birthday property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getBirthday() {
			return birthday;
		}

		/**
		 * Sets the value of the birthday property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setBirthday(String value) {
			this.birthday = value;
		}

		/**
		 * Gets the value of the contact property.
		 * 
		 * <p>
		 * This accessor method returns a reference to the live list, not a
		 * snapshot. Therefore any modification you make to the returned list
		 * will be present inside the JAXB object. This is why there is not a
		 * <CODE>set</CODE> method for the contact property.
		 * 
		 * <p>
		 * For example, to add a new item, do as follows:
		 * 
		 * <pre>
		 * getContact().add(newItem);
		 * </pre>
		 * 
		 * 
		 * <p>
		 * Objects of the following type(s) are allowed in the list
		 * {@link SupplementCustInfoReqMsg.Customer.Contact }
		 * 
		 * 
		 */
		public List<SupplementCustInfoReqMsg.Customer.Contact> getContact() {
			if (contact == null) {
				contact = new ArrayList<SupplementCustInfoReqMsg.Customer.Contact>();
			}
			return this.contact;
		}

		/**
		 * Gets the value of the address property.
		 * 
		 * <p>
		 * This accessor method returns a reference to the live list, not a
		 * snapshot. Therefore any modification you make to the returned list
		 * will be present inside the JAXB object. This is why there is not a
		 * <CODE>set</CODE> method for the address property.
		 * 
		 * <p>
		 * For example, to add a new item, do as follows:
		 * 
		 * <pre>
		 * getAddress().add(newItem);
		 * </pre>
		 * 
		 * 
		 * <p>
		 * Objects of the following type(s) are allowed in the list
		 * {@link SupplementCustInfoReqMsg.Customer.Address }
		 * 
		 * 
		 */
		public List<SupplementCustInfoReqMsg.Customer.Address> getAddress() {
			if (address == null) {
				address = new ArrayList<SupplementCustInfoReqMsg.Customer.Address>();
			}
			return this.address;
		}

		/**
		 * Gets the value of the additionalProperty property.
		 * 
		 * <p>
		 * This accessor method returns a reference to the live list, not a
		 * snapshot. Therefore any modification you make to the returned list
		 * will be present inside the JAXB object. This is why there is not a
		 * <CODE>set</CODE> method for the additionalProperty property.
		 * 
		 * <p>
		 * For example, to add a new item, do as follows:
		 * 
		 * <pre>
		 * getAdditionalProperty().add(newItem);
		 * </pre>
		 * 
		 * 
		 * <p>
		 * Objects of the following type(s) are allowed in the list
		 * {@link SimpleProperty }
		 * 
		 * 
		 */
		public List<SimpleProperty> getAdditionalProperty() {
			if (additionalProperty == null) {
				additionalProperty = new ArrayList<SimpleProperty>();
			}
			return this.additionalProperty;
		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * 
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}Address">
		 *       &lt;sequence>
		 *         &lt;element name="ActionType" type="{http://www.huawei.com/bss/soaif/interface/common/}ActionType"/>
		 *       &lt;/sequence>
		 *     &lt;/extension>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "actionType" })
		public static class Address extends com.huawei.bss.soaif._interface.common.Address {
			@XmlElement(name = "ActionType", required = true)
			protected String actionType;

			/**
			 * Gets the value of the actionType property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getActionType() {
				return actionType;
			}

			/**
			 * Sets the value of the actionType property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setActionType(String value) {
				this.actionType = value;
			}
		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * 
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}Contact">
		 *       &lt;sequence>
		 *         &lt;element name="ActionType" type="{http://www.huawei.com/bss/soaif/interface/common/}ActionType"/>
		 *       &lt;/sequence>
		 *     &lt;/extension>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "actionType" })
		public static class Contact extends com.huawei.bss.soaif._interface.common.Contact {
			@XmlElement(name = "ActionType", required = true)
			protected String actionType;

			/**
			 * Gets the value of the actionType property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getActionType() {
				return actionType;
			}

			/**
			 * Sets the value of the actionType property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setActionType(String value) {
				this.actionType = value;
			}
		}
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="CustId" type="{http://www.huawei.com/bss/soaif/interface/common/}CustId"/>
	 *         &lt;element name="AcctId" type="{http://www.huawei.com/bss/soaif/interface/common/}AcctId"/>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "custId", "acctId" })
	public static class ExistingCust {
		@XmlElement(name = "CustId", required = true)
		protected String custId;
		@XmlElement(name = "AcctId", required = true)
		protected String acctId;

		/**
		 * Gets the value of the custId property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getCustId() {
			return custId;
		}

		/**
		 * Sets the value of the custId property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setCustId(String value) {
			this.custId = value;
		}

		/**
		 * Gets the value of the acctId property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getAcctId() {
			return acctId;
		}

		/**
		 * Sets the value of the acctId property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setAcctId(String value) {
			this.acctId = value;
		}
	}
}
