
package com.huawei.bme.cbsinterface.bcservices.querypayment;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.huawei.bme.cbsinterface.bcservices package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.huawei.bme.cbsinterface.bcservices
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link QueryOfferingRentCycleRequest }
     * 
     */
    public QueryOfferingRentCycleRequest createQueryOfferingRentCycleRequest() {
        return new QueryOfferingRentCycleRequest();
    }

    /**
     * Create an instance of {@link QueryOfferingRentCycleRequest.OfferingInst }
     * 
     */
    public QueryOfferingRentCycleRequest.OfferingInst createQueryOfferingRentCycleRequestOfferingInst() {
        return new QueryOfferingRentCycleRequest.OfferingInst();
    }

    /**
     * Create an instance of {@link QueryOfferingRentCycleRequest.OfferingInst.OfferingOwner }
     * 
     */
    public QueryOfferingRentCycleRequest.OfferingInst.OfferingOwner createQueryOfferingRentCycleRequestOfferingInstOfferingOwner() {
        return new QueryOfferingRentCycleRequest.OfferingInst.OfferingOwner();
    }

    /**
     * Create an instance of {@link QueryConsumptionLimitResult }
     * 
     */
    public QueryConsumptionLimitResult createQueryConsumptionLimitResult() {
        return new QueryConsumptionLimitResult();
    }

    /**
     * Create an instance of {@link QueryPaymentRelationRequest }
     * 
     */
    public QueryPaymentRelationRequest createQueryPaymentRelationRequest() {
        return new QueryPaymentRelationRequest();
    }

    /**
     * Create an instance of {@link QueryPaymentRelationResult }
     * 
     */
    public QueryPaymentRelationResult createQueryPaymentRelationResult() {
        return new QueryPaymentRelationResult();
    }

    /**
     * Create an instance of {@link QueryPaymentRelationResult.PaymentRelationList }
     * 
     */
    public QueryPaymentRelationResult.PaymentRelationList createQueryPaymentRelationResultPaymentRelationList() {
        return new QueryPaymentRelationResult.PaymentRelationList();
    }

    /**
     * Create an instance of {@link QueryPaymentRelationResult.PaymentRelationList.PaymentLimit }
     * 
     */
    public QueryPaymentRelationResult.PaymentRelationList.PaymentLimit createQueryPaymentRelationResultPaymentRelationListPaymentLimit() {
        return new QueryPaymentRelationResult.PaymentRelationList.PaymentLimit();
    }

    /**
     * Create an instance of {@link QueryInstallmentResult }
     * 
     */
    public QueryInstallmentResult createQueryInstallmentResult() {
        return new QueryInstallmentResult();
    }

    /**
     * Create an instance of {@link QuerySubLifeCycleResult }
     * 
     */
    public QuerySubLifeCycleResult createQuerySubLifeCycleResult() {
        return new QuerySubLifeCycleResult();
    }

    /**
     * Create an instance of {@link QueryOfferingRentCycleResult }
     * 
     */
    public QueryOfferingRentCycleResult createQueryOfferingRentCycleResult() {
        return new QueryOfferingRentCycleResult();
    }

    /**
     * Create an instance of {@link QueryOfferingRentCycleResult.OfferingRentCycle }
     * 
     */
    public QueryOfferingRentCycleResult.OfferingRentCycle createQueryOfferingRentCycleResultOfferingRentCycle() {
        return new QueryOfferingRentCycleResult.OfferingRentCycle();
    }

    /**
     * Create an instance of {@link QueryOfferingRentCycleResult.OfferingRentCycle.OfferingOwner }
     * 
     */
    public QueryOfferingRentCycleResult.OfferingRentCycle.OfferingOwner createQueryOfferingRentCycleResultOfferingRentCycleOfferingOwner() {
        return new QueryOfferingRentCycleResult.OfferingRentCycle.OfferingOwner();
    }

    /**
     * Create an instance of {@link QueryCorporateInstallmentResult }
     * 
     */
    public QueryCorporateInstallmentResult createQueryCorporateInstallmentResult() {
        return new QueryCorporateInstallmentResult();
    }

    /**
     * Create an instance of {@link QueryInstallmentRequest }
     * 
     */
    public QueryInstallmentRequest createQueryInstallmentRequest() {
        return new QueryInstallmentRequest();
    }

    /**
     * Create an instance of {@link QueryConsumptionLimitRequest }
     * 
     */
    public QueryConsumptionLimitRequest createQueryConsumptionLimitRequest() {
        return new QueryConsumptionLimitRequest();
    }

    /**
     * Create an instance of {@link QueryCorporateInstallmentRequest }
     * 
     */
    public QueryCorporateInstallmentRequest createQueryCorporateInstallmentRequest() {
        return new QueryCorporateInstallmentRequest();
    }

    /**
     * Create an instance of {@link QueryCorporateInstallmentRequest.GroupMemberList }
     * 
     */
    public QueryCorporateInstallmentRequest.GroupMemberList createQueryCorporateInstallmentRequestGroupMemberList() {
        return new QueryCorporateInstallmentRequest.GroupMemberList();
    }

    /**
     * Create an instance of {@link QueryCorporateInstallmentRequestMsg }
     * 
     */
    public QueryCorporateInstallmentRequestMsg createQueryCorporateInstallmentRequestMsg() {
        return new QueryCorporateInstallmentRequestMsg();
    }

    /**
     * Create an instance of {@link QueryConsumptionLimitRequestMsg }
     * 
     */
    public QueryConsumptionLimitRequestMsg createQueryConsumptionLimitRequestMsg() {
        return new QueryConsumptionLimitRequestMsg();
    }

    /**
     * Create an instance of {@link QueryInstallmentRequestMsg }
     * 
     */
    public QueryInstallmentRequestMsg createQueryInstallmentRequestMsg() {
        return new QueryInstallmentRequestMsg();
    }

    /**
     * Create an instance of {@link QueryCorporateInstallmentResultMsg }
     * 
     */
    public QueryCorporateInstallmentResultMsg createQueryCorporateInstallmentResultMsg() {
        return new QueryCorporateInstallmentResultMsg();
    }

    /**
     * Create an instance of {@link QueryOfferingRentCycleResultMsg }
     * 
     */
    public QueryOfferingRentCycleResultMsg createQueryOfferingRentCycleResultMsg() {
        return new QueryOfferingRentCycleResultMsg();
    }

    /**
     * Create an instance of {@link QuerySubLifeCycleResultMsg }
     * 
     */
    public QuerySubLifeCycleResultMsg createQuerySubLifeCycleResultMsg() {
        return new QuerySubLifeCycleResultMsg();
    }

    /**
     * Create an instance of {@link QueryInstallmentResultMsg }
     * 
     */
    public QueryInstallmentResultMsg createQueryInstallmentResultMsg() {
        return new QueryInstallmentResultMsg();
    }

    /**
     * Create an instance of {@link QueryPaymentRelationResultMsg }
     * 
     */
    public QueryPaymentRelationResultMsg createQueryPaymentRelationResultMsg() {
        return new QueryPaymentRelationResultMsg();
    }

    /**
     * Create an instance of {@link QueryPaymentRelationRequestMsg }
     * 
     */
    public QueryPaymentRelationRequestMsg createQueryPaymentRelationRequestMsg() {
        return new QueryPaymentRelationRequestMsg();
    }

    /**
     * Create an instance of {@link QueryConsumptionLimitResultMsg }
     * 
     */
    public QueryConsumptionLimitResultMsg createQueryConsumptionLimitResultMsg() {
        return new QueryConsumptionLimitResultMsg();
    }

    /**
     * Create an instance of {@link QueryOfferingRentCycleRequestMsg }
     * 
     */
    public QueryOfferingRentCycleRequestMsg createQueryOfferingRentCycleRequestMsg() {
        return new QueryOfferingRentCycleRequestMsg();
    }

    /**
     * Create an instance of {@link QuerySubLifeCycleRequestMsg }
     * 
     */
    public QuerySubLifeCycleRequestMsg createQuerySubLifeCycleRequestMsg() {
        return new QuerySubLifeCycleRequestMsg();
    }

    /**
     * Create an instance of {@link QuerySubLifeCycleRequest }
     * 
     */
    public QuerySubLifeCycleRequest createQuerySubLifeCycleRequest() {
        return new QuerySubLifeCycleRequest();
    }

    /**
     * Create an instance of {@link QueryOfferingRentCycleRequest.OfferingInst.OfferingOwner.AcctAccessCode }
     * 
     */
    public QueryOfferingRentCycleRequest.OfferingInst.OfferingOwner.AcctAccessCode createQueryOfferingRentCycleRequestOfferingInstOfferingOwnerAcctAccessCode() {
        return new QueryOfferingRentCycleRequest.OfferingInst.OfferingOwner.AcctAccessCode();
    }

    /**
     * Create an instance of {@link QueryConsumptionLimitResult.LimitUsageList }
     * 
     */
    public QueryConsumptionLimitResult.LimitUsageList createQueryConsumptionLimitResultLimitUsageList() {
        return new QueryConsumptionLimitResult.LimitUsageList();
    }

    /**
     * Create an instance of {@link QueryPaymentRelationRequest.PayAccount }
     * 
     */
    public QueryPaymentRelationRequest.PayAccount createQueryPaymentRelationRequestPayAccount() {
        return new QueryPaymentRelationRequest.PayAccount();
    }

    /**
     * Create an instance of {@link QueryPaymentRelationRequest.PaymentObj }
     * 
     */
    public QueryPaymentRelationRequest.PaymentObj createQueryPaymentRelationRequestPaymentObj() {
        return new QueryPaymentRelationRequest.PaymentObj();
    }

    /**
     * Create an instance of {@link QueryPaymentRelationResult.PaymentRelationList.PayRelation }
     * 
     */
    public QueryPaymentRelationResult.PaymentRelationList.PayRelation createQueryPaymentRelationResultPaymentRelationListPayRelation() {
        return new QueryPaymentRelationResult.PaymentRelationList.PayRelation();
    }

    /**
     * Create an instance of {@link QueryPaymentRelationResult.PaymentRelationList.PaymentLimit.PaymentLimitInfo }
     * 
     */
    public QueryPaymentRelationResult.PaymentRelationList.PaymentLimit.PaymentLimitInfo createQueryPaymentRelationResultPaymentRelationListPaymentLimitPaymentLimitInfo() {
        return new QueryPaymentRelationResult.PaymentRelationList.PaymentLimit.PaymentLimitInfo();
    }

    /**
     * Create an instance of {@link QueryInstallmentResult.InatallmentDetail }
     * 
     */
    public QueryInstallmentResult.InatallmentDetail createQueryInstallmentResultInatallmentDetail() {
        return new QueryInstallmentResult.InatallmentDetail();
    }

    /**
     * Create an instance of {@link QuerySubLifeCycleResult.LifeCycleStatus }
     * 
     */
    public QuerySubLifeCycleResult.LifeCycleStatus createQuerySubLifeCycleResultLifeCycleStatus() {
        return new QuerySubLifeCycleResult.LifeCycleStatus();
    }

    /**
     * Create an instance of {@link QueryOfferingRentCycleResult.OfferingRentCycle.OfferingOwner.AcctAccessCode }
     * 
     */
    public QueryOfferingRentCycleResult.OfferingRentCycle.OfferingOwner.AcctAccessCode createQueryOfferingRentCycleResultOfferingRentCycleOfferingOwnerAcctAccessCode() {
        return new QueryOfferingRentCycleResult.OfferingRentCycle.OfferingOwner.AcctAccessCode();
    }

    /**
     * Create an instance of {@link QueryCorporateInstallmentResult.InstallmentList }
     * 
     */
    public QueryCorporateInstallmentResult.InstallmentList createQueryCorporateInstallmentResultInstallmentList() {
        return new QueryCorporateInstallmentResult.InstallmentList();
    }

    /**
     * Create an instance of {@link QueryInstallmentRequest.QueryObj }
     * 
     */
    public QueryInstallmentRequest.QueryObj createQueryInstallmentRequestQueryObj() {
        return new QueryInstallmentRequest.QueryObj();
    }

    /**
     * Create an instance of {@link QueryConsumptionLimitRequest.QueryObj }
     * 
     */
    public QueryConsumptionLimitRequest.QueryObj createQueryConsumptionLimitRequestQueryObj() {
        return new QueryConsumptionLimitRequest.QueryObj();
    }

    /**
     * Create an instance of {@link QueryCorporateInstallmentRequest.GroupMemberList.SubAccessCode }
     * 
     */
    public QueryCorporateInstallmentRequest.GroupMemberList.SubAccessCode createQueryCorporateInstallmentRequestGroupMemberListSubAccessCode() {
        return new QueryCorporateInstallmentRequest.GroupMemberList.SubAccessCode();
    }

    /**
     * Create an instance of {@link QueryCorporateInstallmentRequest.GroupMemberList.FirstPaymentList }
     * 
     */
    public QueryCorporateInstallmentRequest.GroupMemberList.FirstPaymentList createQueryCorporateInstallmentRequestGroupMemberListFirstPaymentList() {
        return new QueryCorporateInstallmentRequest.GroupMemberList.FirstPaymentList();
    }

}
