package com.huawei.crm.service.ens;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;
import com.huawei.crm.update.ens.SendMessageToSubscriberRequest;
import com.huawei.crm.update.ens.SendMessageToSubscriberResponse;
import com.huawei.crm.update.ens.SendNotificationRequest;
import com.huawei.crm.update.ens.SendNotificationResponse;
import com.huawei.crm.update.ens.SendSMSMessageRequest;
import com.huawei.crm.update.ens.SendSMSMessageResponse;
import com.huawei.crm.update.ens.TriggerNotificationRequest;
import com.huawei.crm.update.ens.TriggerNotificationResponse;

/**
 * This class was generated by the JAX-WS RI. JAX-WS RI 2.2.4-b01 Generated
 * source version: 2.2
 * 
 */
@WebService(name = "HuaweiCRMPortType", targetNamespace = "http://crm.huawei.com/service/")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({ com.huawei.crm.update.ens.ObjectFactory.class, com.huawei.crm.basetype.ens.ObjectFactory.class,
		com.huawei.crm.service.ens.ObjectFactory.class })
public interface HuaweiCRMPortType {
	/**
	 * 
	 * @param syncscenariosRequestMsgReq
	 * @return returns com.huawei.crm.service.enscrmquery.SyncscenariosResponse
	 */
	@WebMethod(operationName = "Syncscenarios", action = "Syncscenarios")
	@WebResult(name = "SyncscenariosResponse", targetNamespace = "http://crm.huawei.com/service/", partName = "SyncscenariosResponseMsgResp")
	public SyncscenariosResponse syncscenarios(
			@WebParam(name = "SyncscenariosRequest", targetNamespace = "http://crm.huawei.com/service/", partName = "SyncscenariosRequestMsgReq") SyncscenariosRequest syncscenariosRequestMsgReq);

	/**
	 * 
	 * @param submitOrderRequestMsgReq
	 * @return returns com.huawei.crm.service.enscrmquery.SubmitOrderResponse
	 */
	@WebMethod(operationName = "SubmitOrder", action = "SubmitOrder")
	@WebResult(name = "SubmitOrderResponse", targetNamespace = "http://crm.huawei.com/service/", partName = "SubmitOrderResponseMsgResp")
	public SubmitOrderResponse submitOrder(
			@WebParam(name = "SubmitOrderRequest", targetNamespace = "http://crm.huawei.com/service/", partName = "SubmitOrderRequestMsgReq") SubmitOrderRequest submitOrderRequestMsgReq);

	/**
	 * 
	 * @param resumeOrderRequestMsgReq
	 * @return returns com.huawei.crm.service.enscrmquery.ResumeOrderResponse
	 */
	@WebMethod(operationName = "ResumeOrder", action = "ResumeOrder")
	@WebResult(name = "ResumeOrderResponse", targetNamespace = "http://crm.huawei.com/service/", partName = "ResumeOrderResponseMsgResp")
	public ResumeOrderResponse resumeOrder(
			@WebParam(name = "ResumeOrderRequest", targetNamespace = "http://crm.huawei.com/service/", partName = "ResumeOrderRequestMsgReq") ResumeOrderRequest resumeOrderRequestMsgReq);

	/**
	 * 
	 * @param continueOrderRequestMsgReq
	 * @return returns com.huawei.crm.service.enscrmquery.ContinueOrderResponse
	 */
	@WebMethod(operationName = "ContinueOrder", action = "ContinueOrder")
	@WebResult(name = "ContinueOrderResponse", targetNamespace = "http://crm.huawei.com/service/", partName = "ContinueOrderResponseMsgResp")
	public ContinueOrderResponse continueOrder(
			@WebParam(name = "ContinueOrderRequest", targetNamespace = "http://crm.huawei.com/service/", partName = "ContinueOrderRequestMsgReq") ContinueOrderRequest continueOrderRequestMsgReq);

	/**
	 * 
	 * @param notifyOrderRequestMsgReq
	 * @return returns com.huawei.crm.service.enscrmquery.NotifyOrderResponse
	 */
	@WebMethod(operationName = "NotifyOrder", action = "NotifyOrder")
	@WebResult(name = "NotifyOrderResponse", targetNamespace = "http://crm.huawei.com/service/", partName = "NotifyOrderResponseMsgResp")
	public NotifyOrderResponse notifyOrder(
			@WebParam(name = "NotifyOrderRequest", targetNamespace = "http://crm.huawei.com/service/", partName = "NotifyOrderRequestMsgReq") NotifyOrderRequest notifyOrderRequestMsgReq);

	/**
	 * 
	 * @param checkOutOrderRequestMsgReq
	 * @return returns com.huawei.crm.service.enscrmquery.CheckoutOrderResponse
	 */
	@WebMethod(operationName = "CheckOutOrder", action = "CheckOutOrder")
	@WebResult(name = "CheckoutOrderResponse", targetNamespace = "http://crm.huawei.com/service/", partName = "CheckOutOrderResponseMsgResp")
	public CheckoutOrderResponse checkOutOrder(
			@WebParam(name = "CheckoutOrderRequest", targetNamespace = "http://crm.huawei.com/service/", partName = "CheckOutOrderRequestMsgReq") CheckoutOrderRequest checkOutOrderRequestMsgReq);

	/**
	 * 
	 * @param sendMessageToSubscriberRequestMsgReq
	 * @return returns com.huawei.crm.update.SendMessageToSubscriberResponse
	 */
	@WebMethod(operationName = "SendMessageToSubscriber", action = "SendMessageToSubscriber")
	@WebResult(name = "SendMessageToSubscriberResponse", targetNamespace = "http://crm.huawei.com/update/", partName = "SendMessageToSubscriberResponseMsgResp")
	public SendMessageToSubscriberResponse sendMessageToSubscriber(
			@WebParam(name = "SendMessageToSubscriberRequest", targetNamespace = "http://crm.huawei.com/update/", partName = "SendMessageToSubscriberRequestMsgReq") SendMessageToSubscriberRequest sendMessageToSubscriberRequestMsgReq);

	/**
	 * 
	 * @param sendSMSMessageRequestMsgReq
	 * @return returns com.huawei.crm.update.SendSMSMessageResponse
	 */
	@WebMethod(operationName = "SendSMSMessage", action = "SendSMSMessage")
	@WebResult(name = "SendSMSMessageResponse", targetNamespace = "http://crm.huawei.com/update/", partName = "SendSMSMessageResponseMsgResp")
	public SendSMSMessageResponse sendSMSMessage(
			@WebParam(name = "SendSMSMessageRequest", targetNamespace = "http://crm.huawei.com/update/", partName = "SendSMSMessageRequestMsgReq") SendSMSMessageRequest sendSMSMessageRequestMsgReq);

	/**
	 * 
	 * @param triggerNotificationRequest
	 * @return returns com.huawei.crm.update.TriggerNotificationResponse
	 */
	@WebMethod(operationName = "TriggerNotification", action = "TriggerNotification")
	@WebResult(name = "TriggerNotificationResponse", targetNamespace = "http://crm.huawei.com/update/", partName = "TriggerNotificationResponse")
	public TriggerNotificationResponse triggerNotification(
			@WebParam(name = "TriggerNotificationRequest", targetNamespace = "http://crm.huawei.com/update/", partName = "TriggerNotificationRequest") TriggerNotificationRequest triggerNotificationRequest);

	/**
	 * 
	 * @param sendNotificationRequest
	 * @return returns com.huawei.crm.update.SendNotificationResponse
	 */
	@WebMethod(operationName = "SendNotification", action = "SendNotification")
	@WebResult(name = "SendNotificationResponse", targetNamespace = "http://crm.huawei.com/update/", partName = "SendNotificationResponse")
	public SendNotificationResponse sendNotification(
			@WebParam(name = "SendNotificationRequest", targetNamespace = "http://crm.huawei.com/update/", partName = "SendNotificationRequest") SendNotificationRequest sendNotificationRequest);
}
