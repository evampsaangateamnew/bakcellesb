package com.evampsaanga.services;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.evampsaanga.developer.utils.SoapHandlerService;
import com.huawei.crm.basetype.order.query.RequestHeader;
import com.huawei.crm.service.order.query.HuaweiCRMPortType;
import com.huawei.crm.service.order.query.OrderQuery;

public class QueryOrderService {

	private static HuaweiCRMPortType huaweiCRMPortType = null;

	private QueryOrderService() {
	}

	public static synchronized HuaweiCRMPortType getInstance() {
		if (huaweiCRMPortType == null) {
			huaweiCRMPortType = new OrderQuery().getHuaweiCRMPort();
			SoapHandlerService.configureBinding(huaweiCRMPortType);
		}
		return huaweiCRMPortType;
	}
	
	public static RequestHeader getQueryOderHeader()
	{
		RequestHeader reqHeader = new RequestHeader();
		reqHeader.setAccessPwd("r8q0a5WwGNboj9I35XzNcQ==");
		reqHeader.setChannelId("3");
		reqHeader.setAccessUser("ecare");
		reqHeader.setTransactionId(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
		reqHeader.setTenantId("101");
		reqHeader.setTechnicalChannelId("53");
		reqHeader.setVersion("1");
		
		return reqHeader;
	}
}
