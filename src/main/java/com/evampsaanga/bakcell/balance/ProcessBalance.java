package com.evampsaanga.bakcell.balance;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.amqimplementationsesb.LogsType;
import com.evampsaanga.bakcell.hlrweb.getquerybalanceRequest.HLRBalanceServices;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.developer.utils.SOAPLoggingHandler;
import com.evampsaanga.gethomepage.GetHomePageDataLand;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.huawei.bss.soaif._interface.hlrwebservice.HLRWebService;
import com.huawei.bss.soaif._interface.hlrwebservice.QueryBalanceIn;
import com.huawei.bss.soaif._interface.hlrwebservice.QueryBalanceRequest;
import com.huawei.bss.soaif._interface.hlrwebservice.QueryBalanceResponse;

@Path("/bakcell/")
public class ProcessBalance {
	public static final Logger logger = Logger.getLogger("bakcellLogs-V2");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BalanceResponse getBalanceDetails(@Header("credentials") String credential, @Body() String requestBody) {
		Logs logs = new Logs();
		try {
			logs.setTableType(LogsType.HomePage);
			logs.setTransactionName(LogsType.AppMenu.name());
			logger.info("Get Balance request Landed:" + requestBody);
			BalanceRequest cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, BalanceRequest.class);
				if (cclient != null) {
					if (cclient.getIsB2B() != null && cclient.getIsB2B().equals("true"))
						logs.setTransactionName(Transactions.GET_HOME_PAGE_B2B);

					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setUserType(cclient.getCustomerType());
					logs.setIsB2B(cclient.getIsB2B());
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				BalanceResponse resp = new BalanceResponse();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
					BalanceResponse resp = new BalanceResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials == null) {
					BalanceResponse resp = new BalanceResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						BalanceResponse res = new BalanceResponse();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						logs.setResponseCode(res.getReturnCode());
						logs.setResponseDescription(res.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return res;
					}
				} else {
					BalanceResponse resp = new BalanceResponse();
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					double minAmountTobePaid = 0.0;
					try {

						boolean dateCheck = false;

						HLRBalanceServices service = new HLRBalanceServices();
						QueryBalanceRequest qBR = new QueryBalanceRequest();
						qBR.setReqHeader(new GetHomePageDataLand().getReqHeaderForQueryBalanceHLR());
						QueryBalanceIn qBI = new QueryBalanceIn();
						qBI.setPrimaryIdentity(cclient.getmsisdn());
						qBR.setQueryBalanceBody(qBI);
						QueryBalanceResponse balanceResponse = HLRWebService.getInstance().queryBalance(qBR);

						BalanceResponse res = new BalanceResponse();
						res.setBalance(
								service.getBalance(cclient.getmsisdn(), cclient.getCustomerType(), balanceResponse));
						logger.info("****************************************************");
						logger.info("AMOUN_TO_BE_PAID: check for B01 B02 Postpaid");
						logger.info("STATUS_CODE: " + cclient.getStatusCode());
						logger.info("check if B03 " + cclient.getStatusCode().equalsIgnoreCase("B03"));
						logger.info("check if B04 " + cclient.getStatusCode().equalsIgnoreCase("B04"));
						logger.info("check if Postpaid " + cclient.getSubscriberType().equalsIgnoreCase("postpaid"));
						logger.info("STATUS_CODE: " + cclient.getStatusCode());
						if ((cclient.getStatusCode().equalsIgnoreCase("B03")
								|| cclient.getStatusCode().equalsIgnoreCase("B04"))
								&& cclient.getSubscriberType().equalsIgnoreCase("postpaid")) {

							if (balanceResponse != null
									&& (balanceResponse.getRspHeader().getReturnCode().equals("0"))) {
								logger.info("check if balance response is null and return code is 0");

								if (balanceResponse.getQueryBalanceBody().get(0).getOutStandingList() != null
										&& balanceResponse.getQueryBalanceBody().get(0).getOutStandingList()
												.size() > 0) {
									logger.info(
											"check if outstanding balance is not null and outstanding list is greater than 0");
									// LifeCycle case
									if (balanceResponse.getQueryBalanceBody().get(0).getAccountCredit().get(0)
											.getCreditLimitType().equalsIgnoreCase("C_TOTAL_CREDIT_LIMIT")) {
										logger.info("check if credit limit type is C_TOTAL_CREDIT_LIMIT");
										logger.info("AMOUN_TO_BE_PAID:LifeCycle case");
										for (int i = 0; i < balanceResponse.getQueryBalanceBody().get(0)
												.getOutStandingList().size(); i++) {
											// return true if date is not passed
											// false if date is passed
											if (Helper.checkDate(balanceResponse.getQueryBalanceBody().get(0)
													.getOutStandingList().get(i).getDueDate())) {
												dateCheck = true;

											}
											if (!dateCheck) {
												// Date Passed
												minAmountTobePaid = Double.parseDouble(
														res.getBalance().getPostpaid().getOutstandingIndividualDept()
																+ 0.01);
												logger.info("AMOUN_TO_BE_PAID: minAmounttobe Paid" + minAmountTobePaid);
											} else {
												// date not passed
												minAmountTobePaid = Double.parseDouble(res.getBalance().getPostpaid()
														.getAvailableBalanceIndividualValue()) + 0.01;
												logger.info("AMOUN_TO_BE_PAID: minAmounttobe Paid" + minAmountTobePaid);
											}

										}

									} else {
										logger.info("AMOUN_TO_BE_PAID: Bill Cycle");
										double outstanding = 0;
										// Bill Cycle
										// return true if date is not passed
										// false if date is passed
										for (int i = 0; i < balanceResponse.getQueryBalanceBody().get(0)
												.getOutStandingList().size(); i++) {
											// return true if date is not passed
											// false if date is passed
											if (Helper.checkDate(balanceResponse.getQueryBalanceBody().get(0)
													.getOutStandingList().get(i).getDueDate())) {
												dateCheck = true;
												logger.info("AMOUN_TO_BE_PAID: data check " + dateCheck);

											}
											if (!dateCheck) {
												outstanding += Double.parseDouble(balanceResponse.getQueryBalanceBody()
														.get(0).getOutStandingList().get(i).getOutStandingDetail()
														.getOutStandingAmount());
												logger.info("AMOUN_TO_BE_PAID: outstanding " + outstanding);
												// Date Passed
												// amount to be paid
												// minAmountTobePaid =
												// Double.parseDouble(res.getData().getBalance().getPostpaid().getOutstandingIndividualDept()+0.01);
											} else {
												// date not passed
												minAmountTobePaid = Double.parseDouble(
														res.getBalance().getPostpaid().getOutstandingIndividualDept())
														+ 0.01;
												logger.info("AMOUN_TO_BE_PAID: minAmounttobe Paid" + minAmountTobePaid);
												// minAmountTobePaid =
												// Double.parseDouble(res.getData().getBalance().getPostpaid().getAvailableBalanceIndividualValue())+0.01;
											}

										}
										if (!dateCheck)
											minAmountTobePaid = outstanding + 0.01;

										// else
										// minAmountTobePaid=Double.parseDouble(balanceResponse.getQueryBalanceBody().get(0).getOutStandingList().get(i).getOutStandingDetail().getOutStandingAmount()+0.01);
									}
								} else {
									// to be converted to string
									minAmountTobePaid = Math.abs(Double.parseDouble(
											res.getBalance().getPostpaid().getAvailableBalanceIndividualValue()))
											+ 0.01;
									logger.info("AMOUN_TO_BE_PAID: minAmounttobe Paid" + minAmountTobePaid);

								}
							}
						}

						logger.info("AMOUNT_TO_BE_PAID: minAmounttobe Paid FINAL" + minAmountTobePaid);
						logger.info("****************************************************");

						ObjectMapper mapper = new ObjectMapper();
						logger.info("BALANCE Response is: " + mapper.writeValueAsString(res.getBalance()));

					} catch (Exception ex) {
						logger.error(Helper.GetException(ex));
					}
				} else {
					BalanceResponse resp = new BalanceResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		BalanceResponse resp = new BalanceResponse();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);

		try {
			logger.debug("Response Returned Get Balance : " + new ObjectMapper().writeValueAsString(resp));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resp;
	}
}
