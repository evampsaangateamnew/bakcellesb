package com.evampsaanga.bakcell.notificationadmin;

import java.util.List;
import com.evampsaanga.bakcell.responseheaders.BaseResponse;

public class GetNotificationsResponse extends BaseResponse {
	private List<NotificationData> notifications;

	public List<NotificationData> getNotifications() {
		return notifications;
	}

	public void setNotifications(List<NotificationData> notifications) {
		this.notifications = notifications;
	}

	@Override
	public String toString() {
		return "GetNotificationsResponse [notifications=" + notifications + ", toString()=" + super.toString() + "]";
	}

}
