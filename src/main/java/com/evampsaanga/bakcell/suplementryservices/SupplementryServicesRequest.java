package com.evampsaanga.bakcell.suplementryservices;

import com.evampsaanga.bakcell.requestheaders.BaseRequest;

public class SupplementryServicesRequest extends BaseRequest {
	private String offeringName = "";
	private String brandName = "";

	/**
	 * @return the brandName
	 */
	public String getBrandName() {
		return brandName;
	}

	/**
	 * @param brandName
	 *            the brandName to set
	 */
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getOfferingName() {
		return offeringName;
	}

	public void setOfferingName(String offeringName) {
		this.offeringName = offeringName;
	}
}
