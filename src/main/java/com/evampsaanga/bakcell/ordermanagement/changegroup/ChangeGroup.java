package com.evampsaanga.bakcell.ordermanagement.changegroup;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.transaction.annotation.Transactional;

import com.evampsaanga.bakcell.createorder.clientsample.CreateOrderService;
import com.evampsaanga.bakcell.db.DBFactory;
import com.evampsaanga.bakcell.getUsersV2.UsersGroupData;
import com.evampsaanga.bakcell.hlrweb.getquerybalanceRequest.HLRBalanceServices;
import com.evampsaanga.bakcell.ordermanagementV2.OrderManagementLand;
import com.evampsaanga.cache.UserGroupDataCache;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.gethomepage.Balance;
import com.evampsaanga.magento.tariffdetailsv2.Datum;
import com.evampsaanga.magento.tariffdetailsv2.TariffDetailsMagentoResponse;
import com.evampsaanga.services.CRMServices;
import com.evampsaanga.services.QueryOrderService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.huawei.bme.cbsinterface.bcservices.PayRelationComparator;
import com.huawei.bme.cbsinterface.bcservices.querypayment.QueryPaymentRelationResultMsg;
import com.huawei.bme.cbsinterface.bcservices.querypayment.QueryPaymentRelationResult.PaymentRelationList.PayRelation;
import com.huawei.bss.soaif._interface.common.createorder.CreditLimitInfo;
import com.huawei.bss.soaif._interface.common.createorder.GroupSubscriberInfo;
import com.huawei.bss.soaif._interface.common.createorder.ManageCreditLimit;
import com.huawei.bss.soaif._interface.common.createorder.MemberSubscriberInfo;
import com.huawei.bss.soaif._interface.common.createorder.PaymentPlanInfo;
import com.huawei.bss.soaif._interface.common.createorder.PaymentRelation.PaymentLimit;
import com.huawei.bss.soaif._interface.common.createorder.PaymentRelation;
import com.huawei.bss.soaif._interface.orderservice.createorder.CreateOrderReqMsg;
import com.huawei.bss.soaif._interface.orderservice.createorder.CreateOrderReqMsg.Order;
import com.huawei.bss.soaif._interface.orderservice.createorder.CreateOrderRspMsg;
import com.huawei.crm.basetype.RequestHeader;
import com.huawei.crm.query.GetCustomerIn;
import com.huawei.crm.query.GetCustomerRequest;
import com.huawei.crm.query.GetCustomerResponse;
import com.huawei.crm.query.order.query.GetGroupDataIn;
import com.huawei.crm.query.order.query.GetGroupRequest;

public class ChangeGroup 
{
	private List<RecieverMsisdn> activeMsisdn;
	private StringBuffer stringBuffer;
	private ChangeGroupAttributes changeGroupAttributes;
	private Connection connection;
	
	
	

	public ChangeGroup(JSONObject jsonObject, StringBuffer stringBuffer, Connection conn) throws IOException, Exception 
	{
		// TODO Auto-generated constructor stub
//		this.stringBuffer = stringBuffer;//Helper.JsonToObject(jsonObject.toString(), OrderManagementRequest.class);
		this.changeGroupAttributes = Helper.JsonToObject(jsonObject.toString(), ChangeGroupAttributes.class);
		this.connection = conn;
		this.stringBuffer = stringBuffer;
		this.activeMsisdn = new ArrayList<>();
		
		
	}

	
	
	public StringBuffer getStringBuffer() {
		return stringBuffer;
	}



	public void setStringBuffer(StringBuffer stringBuffer) 
	{
		this.stringBuffer = stringBuffer;
	}



	public ChangeGroupAttributes getChangeGroupAttributes() {
		return changeGroupAttributes;
	}



	public void setChangeGroupAttributes(ChangeGroupAttributes changeGroupAttributes) {
		this.changeGroupAttributes = changeGroupAttributes;
	}



	
	
	public List<RecieverMsisdn> getActiveMsisdn() {
		return activeMsisdn;
	}



	public void setActiveMsisdn(List<RecieverMsisdn> activeMsisdn) {
		this.activeMsisdn = activeMsisdn;
	}



	
	//check msisdn active
	public void checkMsisdnActive() throws JsonProcessingException
	{
		OrderManagementLand.loggerV2.info("checking MSISDN ");
		for(int i=0;i<changeGroupAttributes.getRecieverMsisdn().size();i++)
		{
			GetCustomerRequest getCustomerRequest = new GetCustomerRequest();
			getCustomerRequest.setRequestHeader(getRequestHeader());
			GetCustomerIn getCustomerIn = new GetCustomerIn();
			getCustomerIn.setServiceNumber(changeGroupAttributes.getRecieverMsisdn().get(i).getMsisdn());
			
			getCustomerRequest.setGetCustomerBody(getCustomerIn);
			
			GetCustomerResponse getCustomerResponse = CRMServices.getInstance().getCustomerData(getCustomerRequest);
			if (getCustomerResponse != null) 
			{
				if (getCustomerResponse.getResponseHeader().getRetCode().equals("0")) 
				{
					if(getCustomerResponse.getGetCustomerBody().getStatus().equals("A02"))
					{
						this.activeMsisdn.add(changeGroupAttributes.getRecieverMsisdn().get(i));
						//log message
						logMessage(stringBuffer, "msisdn",changeGroupAttributes.getRecieverMsisdn().get(i).getMsisdn());
					}
				}
			}
			
		}
		
	}
	//check balance Individual to be positive
	public void checkBalance()
	{
		
	}
	//removing from group
	public CreateOrderRspMsg removeFromGroup(long groupId,String msisdn)
	{
		OrderManagementLand.loggerV2.info("***********************");
        OrderManagementLand.loggerV2.info("Create Web Service Client...");
        CreateOrderReqMsg createOrderReqMsg = new CreateOrderReqMsg();
        createOrderReqMsg.setReqHeader(CreateOrderService.getCreateOderHeader());
        Order order = new Order();
        order.setOrderType("CG012");
		createOrderReqMsg.setOrder(order);
//		long groupIDlong = (long)groupId;
		GroupSubscriberInfo groupSubscriberInfo = new GroupSubscriberInfo();
		groupSubscriberInfo.setGroupId(groupId);//(1010003262919L);
		
		MemberSubscriberInfo memberSubscriberInfo = new MemberSubscriberInfo();
		memberSubscriberInfo.setMemberServiceNumber(msisdn);//("555956002");
		
		createOrderReqMsg.setGroupSubscriber(groupSubscriberInfo);
		createOrderReqMsg.setGroupMember(memberSubscriberInfo);
		
		CreateOrderRspMsg createOrderRspMsg = CreateOrderService.getInstance().createOrder(createOrderReqMsg);

		
		
        OrderManagementLand.loggerV2.info("***********************");
        OrderManagementLand.loggerV2.info("Call Over!");
        return createOrderRspMsg;
	}
	//adding into group
	public CreateOrderRspMsg addIntoGroup(long groupID,String msisdn,String orderId) throws IOException, Exception
	{
		boolean processingOrder = true; 
		int attempt=0;
		while(processingOrder && attempt<3)
		{
			logMessage(stringBuffer, "Sleeping for 30 seconds");
			Thread.sleep(30000);
			processingOrder = processingOrder(orderId);
			attempt++;
		}
		if(!processingOrder)
		{
			OrderManagementLand.loggerV2.info("***********************");
	        OrderManagementLand.loggerV2.info("Create Web Service Client...");
	        CreateOrderReqMsg createOrderReqMsg = new CreateOrderReqMsg();
	        createOrderReqMsg.setReqHeader(CreateOrderService.getCreateOderHeader());
	        Order order = new Order();
	        order.setOrderType("CG011");
			createOrderReqMsg.setOrder(order );
			
			GroupSubscriberInfo groupSubscriberInfo = new GroupSubscriberInfo();
			groupSubscriberInfo.setGroupId(groupID);//(1010005949215L);
			
			MemberSubscriberInfo memberSubscriberInfo = new MemberSubscriberInfo();
			memberSubscriberInfo.setMemberServiceNumber(msisdn);//("555956002");
			
			createOrderReqMsg.setGroupSubscriber(groupSubscriberInfo);
			createOrderReqMsg.setGroupMember(memberSubscriberInfo);
			
			CreateOrderRspMsg createOrderRspMsg = CreateOrderService.getInstance().createOrder(createOrderReqMsg);
//			logMessage(stringBuffer, "Sleeping for 60 seconds for Adding Change Payment Relation");
//			Thread.sleep(60000);
	        OrderManagementLand.loggerV2.info("***********************");
	        OrderManagementLand.loggerV2.info("Call Over!");
	        return createOrderRspMsg;
		}
		else
		{
			//update order detail that order is taking too long for processing
			
			return null;
			
		}
	}
	public StringBuffer processOrder(String orderId) throws IOException, Exception
	{
		//check allowed for 
		
		
		
		//all active msisdns are saved in List of activeMsisdns
		checkMsisdnActive();
		//start logging
		logRequestTitle(stringBuffer,Constants.CHANGEL_GROUP_TYPE);
		
		//check if destinationGroup is FullPay then we need to call magento API for MRC
		TariffDetailsMagentoResponse tariffDetailsMagentoResponse = ConfigurationManager.getConfigurationFromTariffCache(Constants.TARIFF_DETAIL_ENGLISH);
		if(changeGroupAttributes.getGroupTypeTo().equals("PartPay") || changeGroupAttributes.getGroupTypeTo().equals("FullPay"))
		{
			logMessage(stringBuffer, "*********** Tariff Details ************ ");
			logMessage(stringBuffer, "Fetching tariff details");
			addTariffIds();
//			logMessage(stringBuffer, Helper.ObjectToJson(tariffDetailsMagentoResponse).toString());
		}
//		OrderManagementLand.loggerV2.info("ActiveMSISDN size is "+this.activeMsisdn.size());
		//iterate loop for change group of msisdns	
		for(int i=0;i<this.activeMsisdn.size();i++)
		{

			logMessage(stringBuffer, "*********** Executing MSISDN "+this.activeMsisdn.get(i).getMsisdn()+" ************ ");			
			logMessage(stringBuffer, "groupType: "+this.activeMsisdn.get(i).getGroupType());
			logMessage(stringBuffer, "GroupTypeTo: "+changeGroupAttributes.getGroupTypeTo());
			
			
			//----------------------------------------------- if(Pay by Sub to Part Pay or Full Pay) ----------------------------------------
			String tariffPermission = this.activeMsisdn.get(i).getGroupType()+"-"+changeGroupAttributes.getGroupTypeTo();
			logMessage(stringBuffer, "Permission: "+tariffPermission);
			logMessage(stringBuffer, "Permission available: "+changeGroupAttributes.getTariffPermissions());
			logMessage(stringBuffer, "permission: "+changeGroupAttributes.getTariffPermissions().contains(tariffPermission));
			if(changeGroupAttributes.getTariffPermissions().contains(tariffPermission))
			{

						
			
				//if(Pay by Sub to Part Pay or Full Pay)
				
				if((this.activeMsisdn.get(i).getGroupType().equalsIgnoreCase("PaybySubs")) && (changeGroupAttributes.getGroupTypeTo().equalsIgnoreCase("Part Pay") || changeGroupAttributes.getGroupTypeTo().equalsIgnoreCase("PartPay") || changeGroupAttributes.getGroupTypeTo().equalsIgnoreCase("FullPay") ) )

				{
					
					QueryPaymentRelationResultMsg queryPaymentRelationResultMsg = OrderManagementLand.queryPaymentRelation(this.activeMsisdn.get(i).getMsisdn(), stringBuffer);
					if(queryPaymentRelationResultMsg.getResultHeader().getResultCode().equals("0"))
					{
						//log message
						logMessage(stringBuffer, "PayBySub/PartPay/PartPay to FullPay");
						////check balance individual for +ive
						HLRBalanceServices service = new HLRBalanceServices();
						
						logMessage(stringBuffer, "*********** HLR Balance ************ ");
						logMessage(stringBuffer, "Calling Balance and check if individual Balance is +ive "+activeMsisdn.get(i).getMsisdn());
						
						
						Balance balance = service.getBalance(activeMsisdn.get(i).getMsisdn(), "postpaid",null);
						logMessage(stringBuffer, "Balance Response: "+Helper.ObjectToJson(balance));
						//check if individual balance is +ive
						if(Double.parseDouble(balance.getPostpaid().getBalanceIndividualValue())>0)
						{
							////remove from groupID
							logMessage(stringBuffer, "*********** Removing from group ************ ");
							logMessage(stringBuffer, "GroupID",changeGroupAttributes.getRecieverMsisdn().get(i).getGroupIdFrom());
							logMessage(stringBuffer, "MSISDN",activeMsisdn.get(i).getMsisdn());
							
							////stack overflow 
							Double doubleGroupIdFrom = Double.parseDouble(activeMsisdn.get(i).getGroupIdFrom());
							Long groupIdFrom = doubleGroupIdFrom.longValue();
							
							CreateOrderRspMsg createOrderRspMsgRemoveGroup = removeFromGroup(groupIdFrom, activeMsisdn.get(i).getMsisdn());
							if(createOrderRspMsgRemoveGroup.getRspHeader().getReturnCode().equalsIgnoreCase("0000"))
							{
								//success
								logMessage(stringBuffer, "Removing Group returns 0000 success");
												
								////add into groupID
								
								logMessage(stringBuffer, "*********** Adding to group *********** ");
								logMessage(stringBuffer, "GroupID",changeGroupAttributes.getGroupIdTo());
								logMessage(stringBuffer, "MSISDN",activeMsisdn.get(i).getMsisdn());
								
								////stack overflow 
								Double doubleGroupIdTo = Double.parseDouble(changeGroupAttributes.getGroupIdTo());
								Long groupIdTo = doubleGroupIdTo.longValue();
								CreateOrderRspMsg createOrderRspMsgAddGroup = addIntoGroup(groupIdTo, activeMsisdn.get(i).getMsisdn(),createOrderRspMsgRemoveGroup.getOrderId());
								
								
								
								
								
								
								if(createOrderRspMsgAddGroup.getRspHeader().getReturnCode().equalsIgnoreCase("0000"))
								{
									//success
									logMessage(stringBuffer, "Adding Group returns code 0000 success");
									//Add Payment Relation value will be MRC of current tariff
									logMessage(stringBuffer, "Adding Payment relation value to current MRC");
									
									
									com.huawei.crm.query.GetSubscriberResponse response = new com.evampsaanga.bakcell.getsubscriber.CRMSubscriberService().GetSubscriberRequest(activeMsisdn.get(i).getMsisdn());
									OrderManagementLand.loggerV2.info("Offering ID: "+response.getGetSubscriberBody().getPrimaryOffering().getOfferingId().getOfferingId());
//									OrderManagementLand.loggerV2.info("Tariff Response: "+Helper.ObjectToJson(tariffDetailsMagentoResponse));
									
									//magento is returning un sorted response
									if(response.getResponseHeader().getRetCode().equals("0"))
										{
										int index = compare(response.getGetSubscriberBody().getPrimaryOffering().getOfferingId().getOfferingId(), tariffDetailsMagentoResponse.getData());
										OrderManagementLand.loggerV2.info("Payment Relation");
										if(index!=-1)
										{
											logMessage(stringBuffer, "*********** Payment Relation ************ ");
											logMessage(stringBuffer, "MRC: "+tariffDetailsMagentoResponse.getData().get(index).getHeader().getMrcValue());
											
											
											
											logMessage(stringBuffer, "*********** PIC MSISDN ************ "+changeGroupAttributes.getmsisdn());
											logMessage(stringBuffer, "*********** MSISDN ************ "+this.activeMsisdn.get(i).getMsisdn());
											
											ArrayList<UsersGroupData> usersGroupData = new ArrayList<UsersGroupData>();
											JSONObject getGroupRequest = new JSONObject();
											getGroupRequest.put("lang", "3");
											getGroupRequest.put("iP", "9.9.9.9");
											getGroupRequest.put("channel", "web");
											getGroupRequest.put("msisdn", changeGroupAttributes.getmsisdn());
											getGroupRequest.put("isB2B", "true");
											getGroupRequest.put("customerID", changeGroupAttributes.getCustomerId());
											UserGroupDataCache userCache = new UserGroupDataCache();
											usersGroupData = (ArrayList<UsersGroupData>) userCache.checkUsersInCache(getGroupRequest.toString());
											
											
											String customerCrmAccountId=null;
											for(int k=0;k<usersGroupData.size();k++)
											{
												if(usersGroupData.get(k).getMsisdn().equals(this.activeMsisdn.get(i).getMsisdn()))
													customerCrmAccountId = usersGroupData.get(k).getCorp_crm_acct_id();
											}
											
											logMessage(stringBuffer, "*********** Account ID CRM ************ "+customerCrmAccountId);
											
											boolean flag = true;
											GetGroupRequest getGroupDataRequestMsgReq = new GetGroupRequest();
											getGroupDataRequestMsgReq.setRequestHeader(QueryOrderService.getQueryOderHeader());
											GetGroupDataIn getGroupDataIn = new GetGroupDataIn();
											getGroupDataIn.setServiceNumber(activeMsisdn.get(i).getMsisdn());
											getGroupDataRequestMsgReq.setGetGroupBody(getGroupDataIn);
//											while(flag)
//											{
//											com.huawei.crm.query.order.query.GetGroupResponse getGroupResponse = QueryOrderService.getInstance().getGroupData(getGroupDataRequestMsgReq);
//											if(getGroupResponse != null && getGroupResponse.getResponseHeader().getRetCode().equalsIgnoreCase("0"))
//											{
//												if(getGroupResponse.getGetGroupBody() != null && getGroupResponse.getGetGroupBody().getGetGroupDataList() != null)
//												{
//													if(changeGroupAttributes.getGroupTypeTo().equalsIgnoreCase("FullPay"))
//														for(int k=0;k<getGroupResponse.getGetGroupBody().getGetGroupDataList().size();k++)
//															if(getGroupResponse.getGetGroupBody().getGetGroupDataList().get(k).getGroupName().contains("Fullpay"))
//																flag = false;
//														if(changeGroupAttributes.getGroupTypeTo().equalsIgnoreCase("PartPay"))
//															for(int k=0;k<getGroupResponse.getGetGroupBody().getGetGroupDataList().size();k++)
//																if(getGroupResponse.getGetGroupBody().getGetGroupDataList().get(k).getGroupName().contains("Partpay"))
//																	flag = false;
//														if(changeGroupAttributes.getGroupTypeTo().equalsIgnoreCase("PayBySubs"))
////															for(int k=0;k<getGroupResponse.getGetGroupBody().getGetGroupDataList().size();k++)
////																if(getGroupResponse.getGetGroupBody().getGetGroupDataList().get(k).getGroupName().contains("PayBySubs"))
//																	flag = false;
//														
//															
//												}
//												if(flag)
//												{
//													logMessage(stringBuffer, "Putting to sleep after checking group info");
//													Thread.sleep(15000);
//												}
//											}
//											}
											Thread.sleep(20000);
											CreateOrderRspMsg createOrderRspMsg  = addPaymentRelationOfCurrentTarrif(tariffDetailsMagentoResponse.getData().get(index).getHeader().getMrcValue(),activeMsisdn.get(i).getMsisdn(),customerCrmAccountId,queryPaymentRelationResultMsg,response.getGetSubscriberBody().getSubscriberId(),changeGroupAttributes.getGroupTypeTo(),this.activeMsisdn.get(i).getCorpCrmCustName());
											
											if(createOrderRspMsg.getRspHeader().getReturnCode().equalsIgnoreCase("0000"))
											{
												logMessage(stringBuffer, "Changing Payment Relation returns success");
												//Set initial credit limit zero
												logMessage(stringBuffer, "Changing limit to zero");
												
												logMessage(stringBuffer, "*********** Setting initial limit to zero ************ ");
												 CreateOrderRspMsg creditLimitResponse=setInitialCreditLimit(0,queryPaymentRelationResultMsg.getQueryPaymentRelationResult().getPaymentRelationList().getPayRelation().get(0).getAcctKey(),this.activeMsisdn.get(i).getMsisdn());
												
												 if(creditLimitResponse!=null && creditLimitResponse.getRspHeader().getReturnCode().equals("0000"))
													{
														//success
														updateOrderDetails(orderId, activeMsisdn.get(i).getMsisdn(), "C", ResponseCodes.SUCESS_DES_200, ResponseCodes.SUCESS_DES_200+":"+activeMsisdn.get(i).getMsisdn(), connection, this.stringBuffer);
//														updateOrderStatus(orderId, "C", connection, stringBuffer);
													}
													else
													{
														//failed
			
														logMessage(stringBuffer, "Credit limit failed");
														updateOrderDetails(orderId, activeMsisdn.get(i).getMsisdn(), "F", creditLimitResponse.getRspHeader().getReturnCode(), creditLimitResponse.getRspHeader().getReturnMsg()+":"+activeMsisdn.get(i).getMsisdn(), connection, this.stringBuffer);
//														updateOrderStatus(orderId, "F", connection, stringBuffer);
													}
												 
			//									OrderManagementLand.loggerV2.info(stringBuffer.toString());
												
												
											}
											else
											{
												logMessage(stringBuffer, "Changing Payment Relation failed");
												updateOrderDetails(orderId, activeMsisdn.get(i).getMsisdn(), "F", createOrderRspMsg.getRspHeader().getReturnCode(), createOrderRspMsg.getRspHeader().getReturnMsg()+":"+activeMsisdn.get(i).getMsisdn(), connection, this.stringBuffer);
//												updateOrderStatus(orderId, "F", connection, stringBuffer);
											}
										}
										else
										{
											logMessage(stringBuffer, "msisdn not found in magento response");
											//update order detail table as F
											updateOrderDetails(orderId, activeMsisdn.get(i).getMsisdn(), "F", "Unable to get MRC of Number", ResponseCodes.UNSUCCESS_DESC+":"+activeMsisdn.get(i).getMsisdn(), connection, this.stringBuffer);
//											updateOrderStatus(orderId, "F", connection, stringBuffer);
										}
									}
									else
									{
										logMessage(stringBuffer, "Subscriber is Failed");
										//update order detail table as F
										updateOrderDetails(orderId, activeMsisdn.get(i).getMsisdn(), "F", response.getResponseHeader().getRetCode(), response.getResponseHeader().getRetMsg()+":"+activeMsisdn.get(i).getMsisdn(), connection, this.stringBuffer);
//										updateOrderStatus(orderId, "F", connection, stringBuffer);
									}
									
								}
								else
								{
									//failed
									logMessage(stringBuffer, "Adding Group Failed");
									updateOrderDetails(orderId, activeMsisdn.get(i).getMsisdn(), "F", createOrderRspMsgAddGroup.getRspHeader().getReturnCode(), createOrderRspMsgAddGroup.getRspHeader().getReturnMsg()+":"+activeMsisdn.get(i).getMsisdn(), connection, this.stringBuffer);
//									updateOrderStatus(orderId, "F", connection, stringBuffer);
								}
								
							}
							else
							{
								//failed
								logMessage(stringBuffer, "Removing Group Failed");
								updateOrderDetails(orderId, activeMsisdn.get(i).getMsisdn(), "F", createOrderRspMsgRemoveGroup.getRspHeader().getReturnCode(), createOrderRspMsgRemoveGroup.getRspHeader().getReturnMsg()+":"+activeMsisdn.get(i).getMsisdn(), connection, this.stringBuffer);
//								updateOrderStatus(orderId, "F", connection, stringBuffer);
							}
								
						}
						else
						{
							//update order details to F
							logMessage(stringBuffer, "Available credit limit is less than zero ");
							updateOrderDetails(orderId, activeMsisdn.get(i).getMsisdn(), "F", ResponseCodes.OM_BALANCE_LESS_THAN_ZERO, ResponseCodes.OM_BALANCE_LESS_THAN_ZERO_DESC+":"+activeMsisdn.get(i).getMsisdn(), connection, this.stringBuffer);
//							updateOrderStatus(orderId, "F", connection, stringBuffer);
						}
					}
					else
					{
						//update order details to F
						logMessage(stringBuffer, "Query Payment relation failed ");
						updateOrderDetails(orderId, activeMsisdn.get(i).getMsisdn(), "F", queryPaymentRelationResultMsg.getResultHeader().getResultCode(), queryPaymentRelationResultMsg.getResultHeader().getResultDesc()+":"+activeMsisdn.get(i).getMsisdn(), connection, this.stringBuffer);
//						updateOrderStatus(orderId, "F", connection, stringBuffer);
					}
					
					
				}
				//else if(Part Pay to Full Pay (Same for vice versa))
				
				else if(((activeMsisdn.get(i).getGroupType().equalsIgnoreCase("PartPay")) && changeGroupAttributes.getGroupTypeTo().equalsIgnoreCase("FullPay")) || (activeMsisdn.get(i).getGroupType().equalsIgnoreCase("FullPay") && changeGroupAttributes.getGroupTypeTo().equalsIgnoreCase("PartPay")))
				{
					
					QueryPaymentRelationResultMsg queryPaymentRelationResultMsg = OrderManagementLand.queryPaymentRelation(this.activeMsisdn.get(i).getMsisdn(), stringBuffer);
					if(queryPaymentRelationResultMsg.getResultHeader().getResultCode().equals("0"))
					{
					
	
						logMessage(stringBuffer, "PartPay to FullPay");
						
						logMessage(stringBuffer, "*********** Removing from group ************ ");
						logMessage(stringBuffer, "GroupID",changeGroupAttributes.getRecieverMsisdn().get(i).getGroupIdFrom());
						logMessage(stringBuffer, "MSISDN",activeMsisdn.get(i).getMsisdn());
						
						////remove from groupID
						CreateOrderRspMsg createOrderRspMsgRemoveGroup = removeFromGroup(Long.parseLong(activeMsisdn.get(i).getGroupIdFrom()), activeMsisdn.get(i).getMsisdn());
						com.huawei.crm.query.GetSubscriberResponse response = new com.evampsaanga.bakcell.getsubscriber.CRMSubscriberService().GetSubscriberRequest(activeMsisdn.get(i).getMsisdn());
						if(createOrderRspMsgRemoveGroup.getRspHeader().getReturnCode().equals("0000"))
						{
							logMessage(stringBuffer, "*********** Removing from group returns 0000 success *********** ");
							
							logMessage(stringBuffer, "*********** Adding to group *********** ");
							logMessage(stringBuffer, "GroupID",changeGroupAttributes.getGroupIdTo());
							logMessage(stringBuffer, "MSISDN",activeMsisdn.get(i).getMsisdn());
							
							////add into groupID
							CreateOrderRspMsg createOrderRspMsgAddMessage = addIntoGroup(Long.parseLong(changeGroupAttributes.getGroupIdTo()), activeMsisdn.get(i).getMsisdn(),createOrderRspMsgRemoveGroup.getOrderId());
							if(createOrderRspMsgAddMessage.getRspHeader().getReturnCode().equalsIgnoreCase("0000"))
							{
//								processOrderForSleep(i);
								boolean flag = true;
								GetGroupRequest getGroupDataRequestMsgReq = new GetGroupRequest();
								getGroupDataRequestMsgReq.setRequestHeader(QueryOrderService.getQueryOderHeader());
								GetGroupDataIn getGroupDataIn = new GetGroupDataIn();
								getGroupDataIn.setServiceNumber(activeMsisdn.get(i).getMsisdn());
								getGroupDataRequestMsgReq.setGetGroupBody(getGroupDataIn);
//								while(flag)
//								{
//								com.huawei.crm.query.order.query.GetGroupResponse getGroupResponse = QueryOrderService.getInstance().getGroupData(getGroupDataRequestMsgReq);
//								if(getGroupResponse != null && getGroupResponse.getResponseHeader().getRetCode().equalsIgnoreCase("0"))
//								{
//									if(getGroupResponse.getGetGroupBody() != null && getGroupResponse.getGetGroupBody().getGetGroupDataList() != null)
//									{
//										if(changeGroupAttributes.getGroupTypeTo().equalsIgnoreCase("FullPay"))
//											for(int k=0;k<getGroupResponse.getGetGroupBody().getGetGroupDataList().size();k++)
//												if(getGroupResponse.getGetGroupBody().getGetGroupDataList().get(k).getGroupName().contains("Fullpay"))
//													flag = false;
//											if(changeGroupAttributes.getGroupTypeTo().equalsIgnoreCase("PartPay"))
//												for(int k=0;k<getGroupResponse.getGetGroupBody().getGetGroupDataList().size();k++)
//													if(getGroupResponse.getGetGroupBody().getGetGroupDataList().get(k).getGroupName().contains("Partpay"))
//														flag = false;
//											if(changeGroupAttributes.getGroupTypeTo().equalsIgnoreCase("PayBySubs"))
////												for(int k=0;k<getGroupResponse.getGetGroupBody().getGetGroupDataList().size();k++)
////													if(getGroupResponse.getGetGroupBody().getGetGroupDataList().get(k).getGroupName().contains("PayBySubs"))
//														flag = false;
//											
//												
//									}
//									if(flag)
//									{
//										logMessage(stringBuffer, "Putting to sleep after checking group info");
//										Thread.sleep(15000);
//									}
//								}
								Thread.sleep(20000);
//								}
								//success
								logMessage(stringBuffer, "Adding Group returns code 0000 success");
								//Add Payment Relation value will be MRC of current tariff
								logMessage(stringBuffer, "*********** Payment Relation ************ ");
								int index = compare(response.getGetSubscriberBody().getPrimaryOffering().getOfferingId().getOfferingId(), tariffDetailsMagentoResponse.getData());
								logMessage(stringBuffer, "MRC: "+tariffDetailsMagentoResponse.getData().get(i).getHeader().getMrcValue());
								
								
								
								logMessage(stringBuffer, "*********** PIC MSISDN ************ "+changeGroupAttributes.getmsisdn());
								logMessage(stringBuffer, "*********** MSISDN ************ "+this.activeMsisdn.get(i).getMsisdn());
								
								ArrayList<UsersGroupData> usersGroupData = new ArrayList<UsersGroupData>();
								JSONObject getGroupRequest = new JSONObject();
								getGroupRequest.put("lang", "3");
								getGroupRequest.put("iP", "9.9.9.9");
								getGroupRequest.put("channel", "web");
								getGroupRequest.put("msisdn", changeGroupAttributes.getmsisdn());
								getGroupRequest.put("isB2B", "true");
								getGroupRequest.put("customerID", changeGroupAttributes.getCustomerId());
								UserGroupDataCache userCache = new UserGroupDataCache();
								usersGroupData = (ArrayList<UsersGroupData>) userCache.checkUsersInCache(getGroupRequest.toString());
								
								
								String customerCrmAccountId=null;
								for(int k=0;k<usersGroupData.size();k++)
								{
									if(usersGroupData.get(k).getMsisdn().equals(this.activeMsisdn.get(i).getMsisdn()))
										customerCrmAccountId = usersGroupData.get(k).getCorp_crm_acct_id();
								}
								
								
								logMessage(stringBuffer, "*********** Account ID CRM ************ "+customerCrmAccountId);
								
								CreateOrderRspMsg createOrderRspMsg = addPaymentRelationOfPPayToFullPay(tariffDetailsMagentoResponse.getData().get(index).getHeader().getMrcValue(),activeMsisdn.get(i).getMsisdn(),customerCrmAccountId,queryPaymentRelationResultMsg,response.getGetSubscriberBody().getSubscriberId(),changeGroupAttributes.getGroupTypeTo(),this.activeMsisdn.get(i).getCorpCrmCustName());
								
								if(createOrderRspMsg.getRspHeader().getReturnCode().equals("0000"))
								{
									logMessage(stringBuffer, "Change Payment relation returns code 0000 success");
									updateOrderDetails(orderId, activeMsisdn.get(i).getMsisdn(), "C", ResponseCodes.SUCESS_DES_200, ResponseCodes.SUCESS_DES_200+":"+activeMsisdn.get(i).getMsisdn(), connection, this.stringBuffer);
//									updateOrderStatus(orderId, "C", connection, stringBuffer);
								}
								else
								{
									
									logMessage(stringBuffer, "Change Payment Failed");
									updateOrderDetails(orderId, activeMsisdn.get(i).getMsisdn(), "F", createOrderRspMsg.getRspHeader().getReturnCode(), createOrderRspMsg.getRspHeader().getReturnMsg()+":"+activeMsisdn.get(i).getMsisdn(), connection, this.stringBuffer);
//									updateOrderStatus(orderId, "F", connection, stringBuffer);
								}
							}
							else
							{
								logMessage(stringBuffer, "Add group Failed");
								//failed
								updateOrderDetails(orderId, activeMsisdn.get(i).getMsisdn(), "F", createOrderRspMsgAddMessage.getRspHeader().getReturnCode(), createOrderRspMsgAddMessage.getRspHeader().getReturnMsg()+":"+activeMsisdn.get(i).getMsisdn(), connection, this.stringBuffer);
//								updateOrderStatus(orderId, "F", connection, stringBuffer);
							}
							
						}
						else
						{
							logMessage(stringBuffer, "Remove group Failed");
							updateOrderDetails(orderId, activeMsisdn.get(i).getMsisdn(), "F", createOrderRspMsgRemoveGroup.getRspHeader().getReturnCode(), createOrderRspMsgRemoveGroup.getRspHeader().getReturnMsg()+":"+activeMsisdn.get(i).getMsisdn(), connection, this.stringBuffer);
//							updateOrderStatus(orderId, "F", connection, stringBuffer);
						}
					}
					else
					{
						//update order details to F
						logMessage(stringBuffer, "Query Payment relation failed");
						updateOrderDetails(orderId, activeMsisdn.get(i).getMsisdn(), "F", queryPaymentRelationResultMsg.getResultHeader().getResultCode(), queryPaymentRelationResultMsg.getResultHeader().getResultDesc()+":"+activeMsisdn.get(i).getMsisdn(), connection, this.stringBuffer);
//						updateOrderStatus(orderId, "F", connection, stringBuffer);
					}
					
					
					
				}
				//else if(Part Pay / Full Pay to Pay By Subs)
				else if((activeMsisdn.get(i).getGroupType().equalsIgnoreCase("PartPay") || activeMsisdn.get(i).getGroupType().equalsIgnoreCase("FullPay")) && (changeGroupAttributes.getGroupTypeTo().equalsIgnoreCase("PayBySubs")))
				{
					
					logMessage(stringBuffer, "PartPay/FullPay to PayBySub");
					
					logMessage(stringBuffer, "*********** Removing from group ************ ");
					logMessage(stringBuffer, "GroupID",changeGroupAttributes.getRecieverMsisdn().get(i).getGroupIdFrom());
					logMessage(stringBuffer, "MSISDN",activeMsisdn.get(i).getMsisdn());
					
					
					////Remove from group
					CreateOrderRspMsg createOrderRspMsgRemoveGroup =  removeFromGroup(Long.parseLong(activeMsisdn.get(i).getGroupIdFrom()), activeMsisdn.get(i).getMsisdn());
					if(createOrderRspMsgRemoveGroup.getRspHeader().getReturnCode().equals("0000"))
					{
						//success
						logMessage(stringBuffer, "Removing Group returns code 0000 success");
						logMessage(stringBuffer, "*********** Adding to group *********** ");
						logMessage(stringBuffer, "GroupID",changeGroupAttributes.getGroupIdTo());
						logMessage(stringBuffer, "MSISDN",activeMsisdn.get(i).getMsisdn());
						
						////Add to pay by subs group
						CreateOrderRspMsg createOrderRspMsgAddMessage = addIntoGroup(Long.parseLong(changeGroupAttributes.getGroupIdTo()), activeMsisdn.get(i).getMsisdn(),createOrderRspMsgRemoveGroup.getOrderId());
						if(createOrderRspMsgAddMessage.getRspHeader().getReturnCode().equals("0000"))
						{
							//success
							logMessage(stringBuffer, "Adding Group returns code 0000 success");
							updateOrderDetails(orderId, activeMsisdn.get(i).getMsisdn(), "C", ResponseCodes.SUCESS_DES_200, ResponseCodes.SUCESS_DES_200+":"+activeMsisdn.get(i).getMsisdn(), connection, this.stringBuffer);
//							updateOrderStatus(orderId, "C", connection, stringBuffer);
						}
						else
						{
							//failed
							logMessage(stringBuffer, "Adding Group Failed");
							updateOrderDetails(orderId, activeMsisdn.get(i).getMsisdn(), "F", createOrderRspMsgAddMessage.getRspHeader().getReturnCode(), createOrderRspMsgAddMessage.getRspHeader().getReturnMsg()+":"+activeMsisdn.get(i).getMsisdn(), connection, this.stringBuffer);
//							updateOrderStatus(orderId, "F", connection, stringBuffer);
						}
					}
					else
					{
						//failed
						logMessage(stringBuffer, "Removing Group Failed");
						updateOrderDetails(orderId, activeMsisdn.get(i).getMsisdn(), "F", createOrderRspMsgRemoveGroup.getRspHeader().getReturnCode(), createOrderRspMsgRemoveGroup.getRspHeader().getReturnMsg()+":"+activeMsisdn.get(i).getMsisdn(), connection, this.stringBuffer);
//						updateOrderStatus(orderId, "F", connection, stringBuffer);
					}
					
					
					
				}
				else
				{
					logMessage(stringBuffer, "Order is failed");
					updateOrderDetails(orderId, activeMsisdn.get(i).getMsisdn(), "F", "Exceptional Case", ResponseCodes.UNSUCCESS_DESC+":"+activeMsisdn.get(i).getMsisdn(), connection, this.stringBuffer);
//					updateOrderStatus(orderId, "F", connection, stringBuffer);
				}
			}
			else
			{
				logMessage(stringBuffer, "Group permission not allowed");
				updateOrderDetails(orderId, activeMsisdn.get(i).getMsisdn(), "F", ResponseCodes.OM_GROUP_PERMISSION_NOT_ALLOWED, ResponseCodes.OM_GROUP_PERMISSION_NOT_ALLOWED_DESC+":"+activeMsisdn.get(i).getMsisdn(), connection, this.stringBuffer);
//				updateOrderStatus(orderId, "F", connection, stringBuffer);
			}
		}
		updateOrderStatus(orderId, "C", connection, stringBuffer);
		OrderManagementLand.loggerV2.info(stringBuffer.toString());
		logRequestTitle(stringBuffer,"End of "+Constants.CHANGEL_GROUP_TYPE);
		return stringBuffer;
	}



	private void processOrderForSleep(int index) throws InterruptedException {
		// TODO Auto-generated method stub
		boolean flag = true;
		GetGroupRequest getGroupDataRequestMsgReq = new GetGroupRequest();
		getGroupDataRequestMsgReq.setRequestHeader(QueryOrderService.getQueryOderHeader());
		GetGroupDataIn getGroupDataIn = new GetGroupDataIn();
		getGroupDataIn.setServiceNumber(activeMsisdn.get(index).getMsisdn());
		getGroupDataRequestMsgReq.setGetGroupBody(getGroupDataIn);
		while(flag)
		{
			com.huawei.crm.query.order.query.GetGroupResponse getGroupResponse = QueryOrderService.getInstance().getGroupData(getGroupDataRequestMsgReq);
			if(getGroupResponse != null && getGroupResponse.getResponseHeader().getRetCode().equalsIgnoreCase("0"))
			{
				if(getGroupResponse.getGetGroupBody() != null && getGroupResponse.getGetGroupBody().getGetGroupDataList() != null)
				{
					if(changeGroupAttributes.getGroupTypeTo().equalsIgnoreCase("FullPay"))
					for(int k=0;k<getGroupResponse.getGetGroupBody().getGetGroupDataList().size();k++)
						if(getGroupResponse.getGetGroupBody().getGetGroupDataList().get(k).getGroupName().contains("Fullpay"))
							flag = false;
					if(changeGroupAttributes.getGroupTypeTo().equalsIgnoreCase("PartPay"))
						for(int k=0;k<getGroupResponse.getGetGroupBody().getGetGroupDataList().size();k++)
							if(getGroupResponse.getGetGroupBody().getGetGroupDataList().get(k).getGroupName().contains("Partpay"))
								flag = false;
					if(changeGroupAttributes.getGroupTypeTo().equalsIgnoreCase("PayBySubs"))
//						for(int k=0;k<getGroupResponse.getGetGroupBody().getGetGroupDataList().size();k++)
//							if(getGroupResponse.getGetGroupBody().getGetGroupDataList().get(k).getGroupName().contains("PayBySubs"))
								flag = false;
					
						
				}
				if(flag)
				{
					logMessage(stringBuffer, "Putting to sleep after checking group info");
					Thread.sleep(15000);
				}
			}
		}
	}



	//to be implemented which function is returning OrderDetails
	private boolean processingOrder(String orderID) throws IOException, Exception 
	{
//		RetrieveOrderRequest retrieveOrderRequestMsgReq = new RetrieveOrderRequest();
//		retrieveOrderRequestMsgReq.setRequestHeader(QueryOrderService.getQueryOderHeader());
//		RetrieveOrderRequestBody retrieveOrderRequestBody = new RetrieveOrderRequestBody();
//		retrieveOrderRequestBody.setOrderId(orderID);
//		retrieveOrderRequestMsgReq.setRetrieveOrderRequestBody(retrieveOrderRequestBody );
//		RetrieveOrderResponse retrieveOrderResponse = QueryOrderService.getInstance().retrieveOrder(retrieveOrderRequestMsgReq);
//		logMessage(stringBuffer,"Retreve Order Detail request: "+Helper.ObjectToJson(retrieveOrderRequestBody));
//		logMessage(stringBuffer,"Retreve Order Detail Response: "+Helper.ObjectToJson(retrieveOrderResponse));
//		if(retrieveOrderResponse.getResponseHeader().getRetCode().equals("0"))
//			return false;
//		else
//			return true;
		return false;
	}



	private void logRequestTitle(StringBuffer stringBuffer2,String message) {
		// TODO Auto-generated method stub
		stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
		stringBuffer.append(System.getProperty("line.separator"));
		stringBuffer.append(Helper.getOMlogTimeStamp()+" " + message);
		stringBuffer.append(System.getProperty("line.separator"));
		stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
		stringBuffer.append(System.getProperty("line.separator"));
		
		
		
		OrderManagementLand.loggerV2.info(Helper.getOMlogTimeStamp() + "************************************************");
		OrderManagementLand.loggerV2.info(Helper.getOMlogTimeStamp()+" " + message);
		OrderManagementLand.loggerV2.info(Helper.getOMlogTimeStamp() + "************************************************");
	}
	
	
	private void logMessage(StringBuffer stringBuffer2,String message) {
		// TODO Auto-generated method stub
		stringBuffer.append(Helper.getOMlogTimeStamp() + message);
		stringBuffer.append(System.getProperty("line.separator"));
		
		OrderManagementLand.loggerV2.info(Helper.getOMlogTimeStamp() + message);
		
	}
	private void logMessage(StringBuffer stringBuffer2,String key,String message) {
		// TODO Auto-generated method stub
		stringBuffer.append(Helper.getOMlogTimeStamp() + message);
		stringBuffer.append(System.getProperty("line.separator"));
		
		OrderManagementLand.loggerV2.info(Helper.getOMlogTimeStamp() + message);
		
		
		
		
	}



	public static CreateOrderRspMsg setInitialCreditLimit(long i, String accountID, String msisdn) {
		
		CreateOrderRspMsg createOrderRspMsg = null;
		try {
			GetCustomerResponse getCustomerResponse = Helper.getCustomerResponse(msisdn);
			if(getCustomerResponse.getResponseHeader().getRetCode().equals("0"))
			{
			    CreateOrderReqMsg createOrderReqMsg = new CreateOrderReqMsg();
			    createOrderReqMsg.setReqHeader(CreateOrderService.getCreateOderHeader());
			    Order order = new Order();
			    order.setOrderType("CO089");
				createOrderReqMsg.setOrder(order);
				
				ManageCreditLimit manageCreditLimit = new ManageCreditLimit();
				manageCreditLimit.setAccountId(accountID);//
				
				CreditLimitInfo creditLimitInfo = new CreditLimitInfo();
				creditLimitInfo.setCreditLimitType("All");
				creditLimitInfo.setLimitClass("I");
				creditLimitInfo.setLimitValue(i);
				creditLimitInfo.setActionType("2");
				creditLimitInfo.setCreditLimitType("All");
				
				creditLimitInfo.setCreditLimitSeq(Long.parseLong(Helper.getSequenceNumber(msisdn)));
			
				manageCreditLimit.setCreditLimitInfoList(creditLimitInfo);

				createOrderReqMsg.setManageCreditLimit(manageCreditLimit);
				createOrderRspMsg = CreateOrderService.getInstance().createOrder(createOrderReqMsg);
				
				
			    OrderManagementLand.loggerV2.info("***********************");
			    OrderManagementLand.loggerV2.info("Call Over!");
			    return createOrderRspMsg;
			}
			else
			{
				return null;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public static CreateOrderRspMsg changePaymentRealtion(String groupType, String msisdn, double mrcToBePassedChangePayment, String acccountKey) throws IOException, Exception
	{
		int myInt = (int)mrcToBePassedChangePayment;
		String valuelimitDivident = Integer.toString(myInt);
//		loggerV2.info("Final Value toPAssed in changePAyment :" + mrcToBePassedChangePayment);
		
		Long limitValue = ((long)Double.parseDouble(valuelimitDivident)*Constants.MONEY_DIVIDEND);
//		loggerV2.info("******************* Change Payment Relation **********************");
		 CreateOrderReqMsg createOrderReqMsg = new CreateOrderReqMsg();
		 createOrderReqMsg
		   .setReqHeader(CreateOrderService.getCreateOderHeader());
		 Order order = new Order();
		 order.setOrderType("CO076");
		 createOrderReqMsg.setOrder(order);
		 PaymentPlanInfo paymentPlanInfo = new PaymentPlanInfo();
		 paymentPlanInfo.setAccountID(1010003024611L);
		 paymentPlanInfo.setServiceNumber(msisdn);

		 PaymentRelation paymentRelation = new PaymentRelation();
		 paymentRelation.setActionType("2");
//		 paymentRelation.setPaymentRelationId(acccountKey);
		 paymentRelation.getServiceType().add("-1");
		 PaymentLimit paymentLimit = new PaymentLimit();
		 paymentLimit.setLimitMeasureUnit("101");
		 if(groupType.equalsIgnoreCase("PartPay"))
			 paymentLimit.setLimitPattern("1");
		 else
			 paymentLimit.setLimitPattern("3");
		 paymentLimit.setLimitUnit("1");
		 paymentLimit.setLimitValue(limitValue.toString());
		 paymentRelation.setPaymentLimit(paymentLimit);

		 paymentPlanInfo.getPaymentRelationList().add(paymentRelation);
		 createOrderReqMsg.setPaymentPlanList(paymentPlanInfo);
//		 loggerV2.info("Change Payment Relation Request: "+Helper.ObjectToJson(createOrderReqMsg));
		 CreateOrderRspMsg res = CreateOrderService.getInstance()
		   .createOrder(createOrderReqMsg);
//		 loggerV2.info("Change Payment Relation Response: "+Helper.ObjectToJson(res));
//		 loggerV2.info("**************** END of Change Payment Relation *****************");
		 return res;
	 
	 
	 
	}

	private CreateOrderRspMsg addPaymentRelationOfCurrentTarrif(String mrcValue,String msisdn,String accountId,QueryPaymentRelationResultMsg queryPaymentRelationResultMsg, Long subscriberId, String groupTypeTo,String crmCustomerCorporateKey) 
	{
		
		
		
		
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		
		
		// TODO Auto-generated method stub
		OrderManagementLand.loggerV2.info("***********************");
        OrderManagementLand.loggerV2.info("Create Web Service Client...");
		
		PayRelation payRelationObject = Collections.max(
				queryPaymentRelationResultMsg.getQueryPaymentRelationResult()
						.getPaymentRelationList().getPayRelation(),
				new PayRelationComparator());
		
		
        CreateOrderReqMsg createOrderReqMsg = new CreateOrderReqMsg();
        createOrderReqMsg.setReqHeader(CreateOrderService.getCreateOderHeader());
        Order order = new Order();
        order.setOrderType("CO076");
		createOrderReqMsg.setOrder(order);
		PaymentPlanInfo paymentPlanInfo = new PaymentPlanInfo();
		
		
//		long accountIdL =  (long) Double.parseDouble((accountId));
		paymentPlanInfo.setAccountID(Long.parseLong(accountId));//optional
		paymentPlanInfo.setServiceNumber(msisdn);
//		paymentPlanInfo.setSubscriberId(subscriberId);
		PaymentRelation paymentRelation = new PaymentRelation();
		paymentRelation.setActionType("1");
		paymentRelation.getServiceType().add("-1");
//		paymentRelation.setPaymentRelationId(payRelationObject.getPayRelationKey());
		
		
		PaymentLimit paymentLimit = new PaymentLimit();
		
		paymentLimit.setLimitMeasureUnit("101");
		if(groupTypeTo.equalsIgnoreCase("PartPay"))
			 paymentLimit.setLimitPattern("1");
		 else
			 paymentLimit.setLimitPattern("3");
		paymentLimit.setLimitUnit("1");
		
		
		//**** check if mrc is empty
		Long limitValue = ((long)Double.parseDouble(mrcValue)*Constants.MONEY_DIVIDEND);
		paymentLimit.setLimitValue(limitValue.toString());//money_devident //mrc value to be passed in this param
		paymentRelation.setPaymentLimit(paymentLimit);
		
		paymentPlanInfo.getPaymentRelationList().add(paymentRelation);
		createOrderReqMsg.setPaymentPlanList(paymentPlanInfo);
		
		CreateOrderRspMsg createOrderRspMsg = CreateOrderService.getInstance().createOrder(createOrderReqMsg);
		return createOrderRspMsg;
	}
	
	
	private CreateOrderRspMsg addPaymentRelationOfPPayToFullPay(String mrcValue,String msisdn,String accountId,QueryPaymentRelationResultMsg queryPaymentRelationResultMsg, Long subscriberId, String groupTypeTo,String crmCustomerCorporateKey) 
	{
		
		
		
		
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		
		
		// TODO Auto-generated method stub
		OrderManagementLand.loggerV2.info("***********************");
        OrderManagementLand.loggerV2.info("Create Web Service Client...");
		
		PayRelation payRelationObject = Collections.max(
				queryPaymentRelationResultMsg.getQueryPaymentRelationResult()
						.getPaymentRelationList().getPayRelation(),
				new PayRelationComparator());
		
		
        CreateOrderReqMsg createOrderReqMsg = new CreateOrderReqMsg();
        createOrderReqMsg.setReqHeader(CreateOrderService.getCreateOderHeader());
        Order order = new Order();
        order.setOrderType("CO076");
		createOrderReqMsg.setOrder(order);
		PaymentPlanInfo paymentPlanInfo = new PaymentPlanInfo();
		
		
//		long accountIdL =  (long) Double.parseDouble((accountId));
		paymentPlanInfo.setAccountID(Long.parseLong(accountId));//optional
		paymentPlanInfo.setServiceNumber(msisdn);
//		paymentPlanInfo.setSubscriberId(subscriberId);
		PaymentRelation paymentRelation = new PaymentRelation();
		paymentRelation.setActionType("1");
		paymentRelation.getServiceType().add("-1");
//		paymentRelation.setPaymentRelationId(payRelationObject.getPayRelationKey());
		
		
		PaymentLimit paymentLimit = new PaymentLimit();
		
		paymentLimit.setLimitMeasureUnit("101");
		if(groupTypeTo.equalsIgnoreCase("PartPay"))
			 paymentLimit.setLimitPattern("1");
		 else
			 paymentLimit.setLimitPattern("3");
		paymentLimit.setLimitUnit("1");
		
		
		//**** check if mrc is empty
		Long limitValue = (queryPaymentRelationResultMsg.getQueryPaymentRelationResult().getPaymentRelationList().getPaymentLimit().get(0).getPaymentLimitInfo().getLimit().getLimitValue());
//		Long limitValue = ((long)Double.parseDouble(mrcValue)*Constants.MONEY_DIVIDEND);
		paymentLimit.setLimitValue(limitValue.toString());//money_devident //mrc value to be passed in this param
		paymentRelation.setPaymentLimit(paymentLimit);
		
		paymentPlanInfo.getPaymentRelationList().add(paymentRelation);
		createOrderReqMsg.setPaymentPlanList(paymentPlanInfo);
		
		CreateOrderRspMsg createOrderRspMsg = CreateOrderService.getInstance().createOrder(createOrderReqMsg);
		return createOrderRspMsg;
	}



	private void addTariffIds() throws JSONException 
	{
		// TODO Auto-generated method stub
		ArrayList<String> listOfTarrifIds = new ArrayList<>();
		
		for(int i =0;i<this.activeMsisdn.size();i++)
		{
			listOfTarrifIds.add(this.activeMsisdn.get(i).getTariffIds());
		}		
		
//		JSONObject requestTariffDetails = new JSONObject();
//		requestTariffDetails.put("offeringId", listOfTarrifIds);
//		requestTariffDetails.put("lang", changeGroupAttributes.getLang());
////		OrderManagementLand.loggerV2.info("*************** Call To Magento for Bulk tariffs **********");
////		OrderManagementLand.loggerV2.info("REQUEST: "+requestTariffDetails.toString());
//		logMessage(stringBuffer, "Request to Magento: "+requestTariffDetails.toString());
//		com.evampsaanga.magento.tariffdetailsv2.TariffDetailsMagentoResponse data = new com.evampsaanga.magento.tariffdetailsv2.TariffDetailsMagentoResponse();
//		data = ConfigurationManager.getConfigurationFromTariffCache(Constants.TARIFF_DETAIL_ENGLISH);
//		try {
//			String response = RestClient.SendCallToMagento(
//					ConfigurationManager.getConfigurationFromCache("magento.app.tariffV2"), requestTariffDetails.toString());
//			OrderManagementLand.loggerV2.info("RESPONSE: "+response);
//			OrderManagementLand.loggerV2.info("*************** End of Magento **********");
//			data = Helper.JsonToObject(response,
//					com.evampsaanga.magento.tariffdetailsv2.TariffDetailsMagentoResponse.class);

			
//			loggerV2.info("TariffResponse FROM magento >>:" + response);
			/*
			 * List<com.evampsaanga.magento.tariffdetailsv2.Datum>
			 * matchingObjects = dt.stream(). filter(dt ->
			 * dt.equals("testemail")). collect(Collectors.toList());
			 */

//		} catch (IOException e) {
//			// TODO Auto-generated catch block
////			loggerV2.error(Helper.GetException(e));
//			e.printStackTrace();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
////			loggerV2.error(Helper.GetException(e));
//			e.printStackTrace();
//		}

//		return data;
	}
	public RequestHeader getRequestHeader() {
		RequestHeader reqh = new RequestHeader();
		String accessUser = ConfigurationManager.getConfigurationFromCache("crm.sub.AccessUser").trim();
		reqh.setChannelId(ConfigurationManager.getConfigurationFromCache("crm.sub.ChannelId").trim());
		reqh.setTechnicalChannelId(ConfigurationManager.getConfigurationFromCache("crm.sub.TechnicalChannelId").trim());
		reqh.setAccessUser(accessUser);
		reqh.setTenantId(ConfigurationManager.getConfigurationFromCache("crm.sub.TenantId").trim());
		reqh.setAccessPwd(ConfigurationManager.getConfigurationFromCache("crm.sub.AccessPwd").trim());
		reqh.setTestFlag(ConfigurationManager.getConfigurationFromCache("crm.sub.TestFlag").trim());
		reqh.setLanguage(ConfigurationManager.getConfigurationFromCache("crm.sub.Language").trim());
		reqh.setTransactionId(Helper.generateTransactionID());
		
		return reqh;
	}

	@Transactional(rollbackFor = Exception.class)
	private boolean updateOrderDetails(String order_id, String msisdn, String status, String responseCode,
			String responseDesc, Connection conn, StringBuffer stringBuffer)
			throws SQLException {

		if(conn.isClosed())
			conn = DBFactory.getDbConnectionFromPool();
		OrderManagementLand.loggerV2.info("=======now IN updateOrderDetails Method=======");
		
		  stringBuffer.append(Helper.getOMlogTimeStamp() +"************************************************");
		  stringBuffer.append(System.getProperty("line.separator"));
		  stringBuffer.append(Helper.getOMlogTimeStamp() +"*** Update Order Details  ");
		  stringBuffer.append(System.getProperty("line.separator"));
		  stringBuffer.append(Helper.getOMlogTimeStamp() +"************************************************");
		  stringBuffer.append(System.getProperty("line.separator"));
		 
		String query = "";
		if (msisdn.equals("")) {
			query = "update order_details set status = ?, responsecode=?, responsedescription=? where order_id_fk= ? and status='F'";
		} else
			query = "update order_details set status = ?, responsecode=?, responsedescription=? where order_id_fk= ? and msisdn=?";
		// String
		PreparedStatement preparedStmt = conn.prepareStatement(query);
		preparedStmt = conn.prepareStatement(query);
		preparedStmt.setString(1, status);
		preparedStmt.setString(2, responseCode);
		preparedStmt.setString(3, responseDesc);
		preparedStmt.setString(4, order_id);
		if (!msisdn.equals(""))
			preparedStmt.setString(5, msisdn);
		
		  stringBuffer.append(Helper.getOMlogTimeStamp() +"Execute Query : " + preparedStmt.toString());
		  stringBuffer.append(System.getProperty("line.separator"));
		 
		OrderManagementLand.loggerV2.info("QUERY for update Order_purchase" + preparedStmt.toString());
		preparedStmt.execute();
		int count = preparedStmt.getUpdateCount();
		OrderManagementLand.loggerV2.info("Count In updateOrderDetails " + count);
		if (count > 0) {
			conn.close();
			stringBuffer.append(Helper.getOMlogTimeStamp() + "Record updated : ");
			stringBuffer.append(System.getProperty("line.separator"));
			return true;
		} else {
			conn.close();
			 stringBuffer.append(Helper.getOMlogTimeStamp() + "Record updation failed : ");
			 stringBuffer.append(System.getProperty("line.separator"));
			return false;
		}
	}
	
	
	public static List < Datum > filterAndGetEmployees(List < Datum > employees,
		    Predicate < Datum > predicate)
	{
		    return employees.stream().filter(predicate).collect(Collectors. < Datum > toList());
	}
	
//	public static Predicate < Datum > isSalaryMoreThan(String offeringID) {
//        return p -> p.getHeader().getOfferingId().equals(offeringID);
//    }
		
	public static void main(String[] args) 
	{
		try 
		{
			String requestPacket =" {\"lang\":\"3\",\"offeringId\":[\"1655630758\",\"720469688\",\"770094093\",\"1151813812\"]}";
			String response = "{ \"resultCode\": \"271\", \"msg\": \"Success: Result from database\", \"data\": [ { \"tariffType\": \"Klass\", \"header\": { \"type\": \"Prepaid\", \"sortOrder\": \"200\", \"id\": \"60\", \"offeringName\": \"Klass_XL_2017\", \"offeringId\": \"1151813812\", \"tagIcon\": \"\", \"tag\": \"\", \"currency\": \"manat\", \"name\": \"Klass XL\", \"mrcLabel\": \"Monthly\", \"mrcValue\": \"10\", \"Call\": { \"callIcon\": \"countrywideCall\", \"callLabel\": \"Countrywide calls\", \"callValue\": \"250\", \"callMetrics\": \"min\" }, \"Internet\": { \"internetIcon\": \"whatsappUsing\", \"internetLabel\": \"WhatsApp\", \"internetValue\": \"\", \"internetMetrics\": \"FREE\" }, \"SMS\": { \"smsIcon\": \"countrywideInternet\", \"smsLabel\": \"Countrywide Internet\", \"smsValue\": \"1000\", \"smsMetrics\": \"MB\" }, \"countryWide\": { \"countryWideIcon\": \"\", \"countryWideLabel\": \"\", \"countryWideValue\": \"\", \"countryWideMetrics\": \"\" }, \"Whatsapp\": { \"whatsappIcon\": \"\", \"whatsappLabel\": \"\", \"whatsappValue\": \"\", \"whatsappMetrics\": \"\" }, \"bonusSix\": { \"icon\": \"\", \"label\": \"\", \"value\": \"\", \"metrics\": \"\" }, \"bonusSeven\": { \"icon\": \"\", \"label\": \"\", \"value\": \"\", \"metrics\": \"\" }, \"bonusEight\": { \"icon\": \"\", \"label\": \"\", \"value\": \"\", \"metrics\": \"\" } }, \"packagePrices\": { \"packagePricesSectionLabel\": \"Paid\", \"Call\": { \"priceTemplate\": \"Single value\", \"callIcon\": \"countrywideCall\", \"callLabel\": \"Call\", \"callValueA\": \"1 hour\", \"callValueB\": \"1 minute\", \"onnetLabel\": \"On-net\", \"onnetValueA\": \"\", \"onnetValueB\": \"0.06\", \"offnetLabel\": \"Off-net\", \"offnetValueA\": \"\", \"offnetValueB\": \"0.06\", \"shortDescription\": \"\" }, \"SMS\": { \"smsIcon\": \"tsDSMS\", \"smsLabel\": \"SMS\", \"smsValue\": \"1 SMS\", \"smsCountryWideLabel\": \"Countrywide\", \"smsCountryWideValue\": \"0.03\", \"smsInternationalLabel\": \"International\", \"smsInternationalValue\": \"0.10\" }, \"Internet\": { \"internetIcon\": \"tsDInternet\", \"internetLabel\": \"Internet\", \"internetValue\": \"1 MB\", \"internetDownloadAndUploadLabel\": \"Countrywide\", \"internetDownloadAndUploadValue\": \"0.89\" } }, \"payg\": { \"paygSectionLabel\": \"Unpaid\", \"Call\": { \"priceTemplate\": \"Single value\", \"callIcon\": \"tsDCall\", \"callLabel\": \"Call\", \"callValueA\": \"1 dəqiqə\", \"callValueB\": \"1 dəqiqə\", \"onnetLabel\": \"On-net\", \"onnetValueA\": \"\", \"onnetValueB\": \"0.08\", \"offnetLabel\": \"Off-net\", \"offnetValueA\": \"\", \"offnetValueB\": \"0.08\" }, \"SMS\": { \"smsIcon\": \"tsDSMS\", \"smsLabel\": \"SMS\", \"smsValue\": \"1 SMS\", \"smsCountryWideLabel\": \"Countrywide\", \"smsCountryWideValue\": \"0.06\", \"smsInternationalLabel\": \"International\", \"smsInternationalValue\": \"0.10\" }, \"Internet\": { \"internetIcon\": \"tsDInternet\", \"internetLabel\": \"Internet\", \"internetValue\": \"1 MB\", \"internetDownloadAndUploadLabel\": \"Countrywide\", \"internetDownloadAndUploadValue\": \"0.35\" } }, \"details\": { \"detailSectionLabel\": \"Details\", \"Call\": { \"callIcon\": \"tsDMMS\", \"callLabel\": \"MMS\", \"callValue\": \"1 MMS\", \"callOnnetOutOfRegionLabel\": \"Countrywide\", \"callOnnetOutOfRegionValue\": \"0.05\", \"callOffnetOutOfRegionLabel\": \"\", \"callOffnetOutOfRegionValue\": \"\", \"titleThree\": \"\", \"valueThree\": \"\", \"titleFour\": \"\", \"valueFour\": \"\", \"callDescription\": \"\" }, \"fragmentB\": { \"callIcon\": \"tsDCall\", \"callLabel\": \"Call\", \"callValue\": \"1 minute\", \"callOnnetOutOfRegionLabel\": \"International (peak)\", \"callOnnetOutOfRegionValue\": \"0.32\", \"callOffnetOutOfRegionLabel\": \"International (off-peak)\", \"callOffnetOutOfRegionValue\": \"0.24\", \"titleThree\": \"\", \"valueThree\": \"\", \"titleFour\": \"\", \"valueFour\": \"\", \"callDescription\": \"International (peak) - during all official working days.<\\\\n>International (off-peak) - Every day within 00:00-08:00, during weekend (Saturday and Sunday) and all official nonworking days.\" }, \"fragmentC\": { \"callIcon\": \"\", \"callLabel\": \"\", \"callValue\": \"\", \"callOnnetOutOfRegionLabel\": \"\", \"callOnnetOutOfRegionValue\": \"\", \"callOffnetOutOfRegionLabel\": \"\", \"callOffnetOutOfRegionValue\": \"\", \"titleThree\": \"\", \"valueThree\": \"\", \"titleFour\": \"\", \"valueFour\": \"\", \"callDescription\": \"\" }, \"fragmentD\": { \"callIcon\": \"\", \"callLabel\": \"\", \"callValue\": \"\", \"callOnnetOutOfRegionLabel\": \"\", \"callOnnetOutOfRegionValue\": \"\", \"callOffnetOutOfRegionLabel\": \"\", \"callOffnetOutOfRegionValue\": \"\", \"titleThree\": \"\", \"valueThree\": \"\", \"titleFour\": \"\", \"valueFour\": \"\", \"callDescription\": \"\" }, \"destination\": { \"destinationIcon\": \"tsDRounding\", \"destinationLabel\": \"Charging interval\", \"destinationValue\": \"\", \"destinationOnnetLabel\": \"Countrywide calls (Paid)\", \"destinationOnnetValue\": \"10 sec\", \"destinationOffnetLabel\": \"Countrywide calls (Unpaid)\", \"destinationOffnetValue\": \"60 sec\", \"destinationInternationalLabel\": \"International calls\", \"destinationInternationalValue\": \"60 sec\", \"destinationInternetLabel\": \"Countrywide internet\", \"destinationInternetValue\": \"100 KB\", \"shortDescription\": \"\" }, \"advantages\": { \"advantagesLabel\": \"Clarification\", \"description\": \"After allocated free traffic consumed during the validity of the package 'High speed' service will be activated and 100 MB will be added on your balance just for\\r\\n89 qepiks. This amount will be charged from the funds on your balance. If your balance lower than 89 qepiks the connection speed of Internet will drop down to 64kbps, but not terminated. To terminate automatic deduction of 89 qepiks from your balance dial *445*0#YES. In order to continue to benefit from high-speed\\r\\nInternet you can activate any Sür@ Internet packages.<\\\\n>Free WhatsApp includes text messaging. When you send picture, audio, video and other files by WhatsApp you will be charged from your Internet balance.<\\\\n>Minutes provided within the package can’t be used for short numbers.\" }, \"titleSubtitle\": { \"titleLabel\": \"\", \"subtitle1Label\": \"\", \"subtitle1Value\": \"\", \"subtitle2Label\": \"\", \"subtitle2Value\": \"\", \"subtitle3Label\": \"\", \"subtitle3Value\": \"\", \"subtitle4Label\": \"\", \"subtitle4Value\": \"\", \"shortDescription\": \"\" }, \"date\": { \"dateDescription\": \"\", \"fromDateLabel\": \"\", \"fromDateValue\": \"\", \"toDateLabel\": \"\", \"toDateValue\": \"\" }, \"time\": { \"timeDescription\": \"\", \"fromTimeLabel\": \"\", \"fromTimeValue\": \"\", \"toTimeLabel\": \"\", \"toTimeValue\": \"\" }, \"roaming\": { \"country\": [], \"descriptionPosition\": \"0\", \"description\": \"\" }, \"freeResource\": { \"freeResourceLabel\": \"\", \"freeResourceValue\": \"\", \"onnetFreeResourceLabel\": \"\", \"onnetFreeResourceValue\": \"\", \"descriptionFreeResource\": \"\" } } }, { \"tariffType\": \"Cin\", \"header\": { \"type\": \"Prepaid\", \"sortOrder\": \"1000\", \"id\": \"3\", \"offeringName\": \"SevimliGin\", \"offeringId\": \"770094093\", \"tagIcon\": \"\", \"tag\": \"\", \"currency\": \"manat\", \"name\": \"SevimliCIN\", \"mrcLabel\": \"\", \"mrcValue\": \"\", \"shortDescription\": \"Beneficial communication with beloved ones.\", \"Call\": { \"priceTemplate\": \"Single value\", \"callIcon\": \"countrywideCall\", \"callLabel\": \"Call\", \"callValueA\": \"\", \"callValueB\": \"1 minute\", \"onnetLabel\": \"On-net\", \"onnetValueA\": \"\", \"onnetValueB\": \"0.06\", \"offnetLabel\": \"Off-net\", \"offnetValueA\": \"\", \"offnetValueB\": \"0.12\", \"shortDescription\": \"\" }, \"SMS\": { \"smsIcon\": \"countrywideSMS\", \"smsLabel\": \"SMS\", \"smsValue\": \"1 SMS\", \"smsCountryWideLabel\": \"Countrywide\", \"smsCountryWideValue\": \"0.05\", \"smsInternationalLabel\": \"International\", \"smsInternationalValue\": \"0.10\" }, \"Internet\": { \"internetIcon\": \"countrywideInternet\", \"internetLabel\": \"Internet\", \"internetValue\": \"1 MB\", \"internetDownloadAndUploadLabel\": \"Countrywide\", \"internetDownloadAndUploadValue\": \"0.29\" } }, \"details\": { \"detailSectionLabel\": \"Details\", \"Call\": { \"callIcon\": \"tsDCall\", \"callLabel\": \"Call\", \"callValue\": \"1 minute\", \"callOnnetOutOfRegionLabel\": \"To Lovely numbers\", \"callOnnetOutOfRegionValue\": \"0.03\", \"callOffnetOutOfRegionLabel\": \"International (peak)\", \"callOffnetOutOfRegionValue\": \"0.32\", \"titleThree\": \"International (off-peak)\", \"valueThree\": \"0.24\", \"titleFour\": \"\", \"valueFour\": \"\", \"callDescription\": \"International (peak) - during all official working days;\\r\\n\\r\\nInternational (off-peak) - Every day within 00:00-08:00, during weekend (Saturday and Sunday) and all official nonworking days.\" }, \"fragmentB\": { \"callIcon\": \"tsDMMS\", \"callLabel\": \"MMS\", \"callValue\": \"1 MMS\", \"callOnnetOutOfRegionLabel\": \"Countrywide\", \"callOnnetOutOfRegionValue\": \"0.08\", \"callOffnetOutOfRegionLabel\": \"\", \"callOffnetOutOfRegionValue\": \"\", \"titleThree\": \"\", \"valueThree\": \"\", \"titleFour\": \"\", \"valueFour\": \"\", \"callDescription\": \"\" }, \"fragmentC\": { \"callIcon\": \"\", \"callLabel\": \"\", \"callValue\": \"\", \"callOnnetOutOfRegionLabel\": \"\", \"callOnnetOutOfRegionValue\": \"\", \"callOffnetOutOfRegionLabel\": \"\", \"callOffnetOutOfRegionValue\": \"\", \"titleThree\": \"\", \"valueThree\": \"\", \"titleFour\": \"\", \"valueFour\": \"\", \"callDescription\": \"\" }, \"fragmentD\": { \"callIcon\": \"\", \"callLabel\": \"\", \"callValue\": \"\", \"callOnnetOutOfRegionLabel\": \"\", \"callOnnetOutOfRegionValue\": \"\", \"callOffnetOutOfRegionLabel\": \"\", \"callOffnetOutOfRegionValue\": \"\", \"titleThree\": \"\", \"valueThree\": \"\", \"titleFour\": \"\", \"valueFour\": \"\", \"callDescription\": \"\" }, \"destination\": { \"destinationIcon\": \"tsDRounding\", \"destinationLabel\": \"Charging interval\", \"destinationValue\": \"\", \"destinationOnnetLabel\": \"Countrywide calls\", \"destinationOnnetValue\": \"30 sec\", \"destinationOffnetLabel\": \"To Lovely numbers\", \"destinationOffnetValue\": \"60 sec\", \"destinationInternationalLabel\": \"25 bonus on-net minutes\", \"destinationInternationalValue\": \"60 sec\", \"destinationInternetLabel\": \"International calls\", \"destinationInternetValue\": \"60 sec\", \"shortDescription\": \"\" }, \"advantages\": { \"advantagesLabel\": \"\", \"description\": \"\" }, \"titleSubtitle\": { \"titleLabel\": \"\", \"subtitle1Label\": \"\", \"subtitle1Value\": \"\", \"subtitle2Label\": \"\", \"subtitle2Value\": \"\", \"subtitle3Label\": \"\", \"subtitle3Value\": \"\", \"subtitle4Label\": \"\", \"subtitle4Value\": \"\", \"shortDescription\": \"\" }, \"date\": { \"dateDescription\": \"\", \"fromDateLabel\": \"\", \"fromDateValue\": \"\", \"toDateLabel\": \"\", \"toDateValue\": \"\" }, \"time\": { \"timeDescription\": \"\", \"fromTimeLabel\": \"\", \"fromTimeValue\": \"\", \"toTimeLabel\": \"\", \"toTimeValue\": \"\" }, \"roaming\": { \"country\": [], \"descriptionPosition\": \"0\", \"description\": \"\" }, \"freeResource\": { \"freeResourceLabel\": \"\", \"freeResourceValue\": \"\", \"onnetFreeResourceLabel\": \"\", \"onnetFreeResourceValue\": \"\", \"descriptionFreeResource\": \"\" } }, \"description\": { \"descriptionSectionLabel\": \"Description\", \"advantagesLabel\": \"Advantages\", \"advantagesValue\": \"Calls to 5 “Lovely numbers” numbers – 3 qepik per minute.\", \"clarificationLabel\": \"\", \"clarificationValue\": \"Every new subscriber will get 25 bonus minutes for calls to Bakcell numbers every month during a year. To get monthly bonus at least one-time 2 AZN/month top-up is required. Monthly bonuses will be valid until the end of each calendar month. \" } }, { \"tariffType\": \"BusinessCorporate\", \"header\": { \"sortOrder\": \"1015\", \"id\": \"101\", \"offeringName\": \"Business_9\", \"offeringId\": \"720469688\", \"tagIcon\": \"\", \"tag\": \"\", \"currency\": \"manat\", \"name\": \"Business 9\", \"mrcLabel\": \"Monthly\", \"mrcValue\": \"9\", \"Call\": { \"callIcon\": \"countrywideCall\", \"callLabel\": \"Countrywide calls\", \"callValue\": \"450\", \"callMetrics\": \"min\" }, \"SMS\": { \"smsIcon\": \"countrywideInternet\", \"smsLabel\": \"Countrywide Internet\", \"smsValue\": \"1000\", \"smsMetrics\": \"MB\" }, \"Internet\": { \"internetIcon\": \"\", \"internetLabel\": \"\", \"internetValue\": \"\", \"internetMetrics\": \"\" }, \"countryWide\": { \"countryWideIcon\": \"\", \"countryWideLabel\": \"\", \"countryWideValue\": \"\", \"countryWideMetrics\": \"\" }, \"Whatsapp\": { \"whatsappIcon\": \"\", \"whatsappLabel\": \"\", \"whatsappValue\": \"\", \"whatsappMetrics\": \"\" }, \"bonusSix\": { \"icon\": \"\", \"label\": \"\", \"value\": \"\", \"metrics\": \"\" }, \"bonusSeven\": { \"icon\": \"\", \"label\": \"\", \"value\": \"\", \"metrics\": \"\" }, \"bonusEight\": { \"icon\": \"\", \"label\": \"\", \"value\": \"\", \"metrics\": \"\" } }, \"prices\": { \"pricesSectionLabel\": \"Prices\", \"Call\": { \"priceTemplate\": \"Single value\", \"callIcon\": \"tsDCall\", \"callLabel\": \"Call\", \"callValueA\": \"\", \"callValueB\": \"1 minute\", \"onnetLabel\": \"On-net\", \"onnetValueA\": \"\", \"onnetValueB\": \"0.08\", \"offnetLabel\": \"Off-net\", \"offnetValueA\": \"\", \"offnetValueB\": \"0.08\", \"shortDescription\": \"\" }, \"SMS\": { \"smsIcon\": \"tsDSMS\", \"smsLabel\": \"SMS\", \"smsValue\": \"1 SMS\", \"smsCountryWideLabel\": \"Countrywide\", \"smsCountryWideValue\": \"0.03\", \"smsInternationalLabel\": \"International\", \"smsInternationalValue\": \"0.06\" }, \"Internet\": { \"internetIcon\": \"tsDInternet\", \"internetLabel\": \"Internet\", \"internetValue\": \"1 MB\", \"internetDownloadAndUploadLabel\": \"Countrywide\", \"internetDownloadAndUploadValue\": \"0.03\" } }, \"details\": { \"detailSectionLabel\": \"Details\", \"Call\": { \"callIcon\": \"tsDMMS\", \"callLabel\": \"MMS\", \"callValue\": \"1 MMS\", \"callOnnetOutOfRegionLabel\": \"Countrywide\", \"callOnnetOutOfRegionValue\": \"0.08\", \"callOffnetOutOfRegionLabel\": \"\", \"callOffnetOutOfRegionValue\": \"\", \"titleThree\": \"\", \"valueThree\": \"\", \"titleFour\": \"\", \"valueFour\": \"\", \"callDescription\": \"\" }, \"fragmentB\": { \"callIcon\": \"tsDCall\", \"callLabel\": \"Call\", \"callValue\": \"1 minute\", \"callOnnetOutOfRegionLabel\": \"International\", \"callOnnetOutOfRegionValue\": \"0.32\", \"callOffnetOutOfRegionLabel\": \"\", \"callOffnetOutOfRegionValue\": \"\", \"titleThree\": \"\", \"valueThree\": \"\", \"titleFour\": \"\", \"valueFour\": \"\", \"callDescription\": \"\" }, \"fragmentC\": { \"callIcon\": \"\", \"callLabel\": \"\", \"callValue\": \"\", \"callOnnetOutOfRegionLabel\": \"\", \"callOnnetOutOfRegionValue\": \"\", \"callOffnetOutOfRegionLabel\": \"\", \"callOffnetOutOfRegionValue\": \"\", \"titleThree\": \"\", \"valueThree\": \"\", \"titleFour\": \"\", \"valueFour\": \"\", \"callDescription\": \"\" }, \"fragmentD\": { \"callIcon\": \"\", \"callLabel\": \"\", \"callValue\": \"\", \"callOnnetOutOfRegionLabel\": \"\", \"callOnnetOutOfRegionValue\": \"\", \"callOffnetOutOfRegionLabel\": \"\", \"callOffnetOutOfRegionValue\": \"\", \"titleThree\": \"\", \"valueThree\": \"\", \"titleFour\": \"\", \"valueFour\": \"\", \"callDescription\": \"\" }, \"destination\": { \"destinationIcon\": \"tsDRounding\", \"destinationLabel\": \"Charging interval\", \"destinationValue\": \"\", \"destinationOnnetLabel\": \"Countrywide calls\", \"destinationOnnetValue\": \"60 sec\", \"destinationOffnetLabel\": \"International calls\", \"destinationOffnetValue\": \"60 sec\", \"destinationInternationalLabel\": \"Countrywide Internet\", \"destinationInternationalValue\": \"40 KB\", \"destinationInternetLabel\": \"\", \"destinationInternetValue\": \"\", \"shortDescription\": \"\" }, \"advantages\": { \"advantagesLabel\": \"\", \"description\": \"\" }, \"titleSubtitle\": { \"titleLabel\": \"\", \"subtitle1Label\": \"\", \"subtitle1Value\": \"\", \"subtitle2Label\": \"\", \"subtitle2Value\": \"\", \"subtitle3Label\": \"\", \"subtitle3Value\": \"\", \"subtitle4Label\": \"\", \"subtitle4Value\": \"\", \"shortDescription\": \"\" }, \"date\": { \"dateDescription\": \"\", \"fromDateLabel\": \"\", \"fromDateValue\": \"\", \"toDateLabel\": \"\", \"toDateValue\": \"\" }, \"time\": { \"timeDescription\": \"\", \"fromTimeLabel\": \"\", \"fromTimeValue\": \"\", \"toTimeLabel\": \"\", \"toTimeValue\": \"\" }, \"roaming\": { \"country\": [], \"descriptionPosition\": \"0\", \"description\": \"\" }, \"freeResource\": { \"freeResourceLabel\": \"\", \"freeResourceValue\": \"\", \"onnetFreeResourceLabel\": \"\", \"onnetFreeResourceValue\": \"\", \"descriptionFreeResource\": \"\" } }, \"description\": { \"descriptionSectionLabel\": \"Description\", \"advantagesLabel\": \"\", \"advantagesValue\": \"\", \"clarificationLabel\": \"\", \"clarificationValue\": \"\" } }, { \"tariffType\": \"KlassPostpaid\", \"header\": { \"type\": \"Postpaid\", \"sortOrder\": \"1137\", \"id\": \"81\", \"offeringName\": \"Klass_3XL_Postpaid_2017\", \"offeringId\": \"1655630758\", \"tagIcon\": \"\", \"tag\": \"\", \"currency\": \"manat\", \"name\": \"Klass Postpaid 3XL\", \"mrcLabel\": \"Monthly\", \"mrcValue\": \"50\", \"Call\": { \"callIcon\": \"countrywideCall\", \"callLabel\": \"Countrywide calls\", \"callValue\": \"3000\", \"callMetrics\": \"min\" }, \"SMS\": { \"smsIcon\": \"countrywideSMS\", \"smsLabel\": \"Countrywide SMS\", \"smsValue\": \"3000\", \"smsMetrics\": \"SMS\" }, \"Internet\": { \"internetIcon\": \"countrywideInternet\", \"internetLabel\": \"Countrywide Internet\", \"internetValue\": \"10000\", \"internetMetrics\": \"MB\" }, \"countryWide\": { \"countryWideIcon\": \"whatsappUsing\", \"countryWideLabel\": \"WhatsApp\", \"countryWideValue\": \"\", \"countryWideMetrics\": \"FREE\" }, \"Whatsapp\": { \"whatsappIcon\": \"internationalCalls\", \"whatsappLabel\": \"International calls\", \"whatsappValue\": \"100\", \"whatsappMetrics\": \"min\" }, \"bonusSix\": { \"icon\": \"roamingCalls\", \"label\": \"Roaming calls\", \"value\": \"100\", \"metrics\": \"min\" }, \"bonusSeven\": { \"icon\": \"roamingInternet\", \"label\": \"Roaming Internet \", \"value\": \"1000\", \"metrics\": \"MB\" }, \"bonusEight\": { \"icon\": \"\", \"label\": \"\", \"value\": \"\", \"metrics\": \"\" } }, \"packagePrices\": { \"packagePricesSectionLabel\": \"When bonuses within the tariff are consumed\", \"Call\": { \"priceTemplate\": \"Single value\", \"callIcon\": \"countrywideCall\", \"callLabel\": \"Call\", \"callValueA\": \"\", \"callValueB\": \"1 minute\", \"onnetLabel\": \"On-net\", \"onnetValueA\": \"\", \"onnetValueB\": \"0.04\", \"offnetLabel\": \"Off-net\", \"offnetValueA\": \"\", \"offnetValueB\": \"0.04\", \"shortDescription\": \"\" }, \"SMS\": { \"smsIcon\": \"countrywideSMS\", \"smsLabel\": \"SMS\", \"smsValue\": \"1 SMS\", \"smsCountryWideLabel\": \"Countrywide\", \"smsCountryWideValue\": \"0.03\", \"smsInternationalLabel\": \"International\", \"smsInternationalValue\": \"0.10\" }, \"Internet\": { \"internetIcon\": \"countrywideInternet\", \"internetLabel\": \"Internet\", \"internetValue\": \"1 MB\", \"internetDownloadAndUploadLabel\": \"Countrywide\", \"internetDownloadAndUploadValue\": \"0\" } }, \"details\": { \"detailSectionLabel\": \"Details\", \"Call\": { \"callIcon\": \"tsDMMS\", \"callLabel\": \"MMS\", \"callValue\": \"1 MMS\", \"callOnnetOutOfRegionLabel\": \"Countrywide\", \"callOnnetOutOfRegionValue\": \"0.05\", \"callOffnetOutOfRegionLabel\": \"\", \"callOffnetOutOfRegionValue\": \"\", \"titleThree\": \"\", \"valueThree\": \"\", \"titleFour\": \"\", \"valueFour\": \"\", \"callDescription\": \"\" }, \"fragmentB\": { \"callIcon\": \"tsDCall\", \"callLabel\": \"Call\", \"callValue\": \"1 minute\", \"callOnnetOutOfRegionLabel\": \"International\", \"callOnnetOutOfRegionValue\": \"0.32\", \"callOffnetOutOfRegionLabel\": \"\", \"callOffnetOutOfRegionValue\": \"\", \"titleThree\": \"\", \"valueThree\": \"\", \"titleFour\": \"\", \"valueFour\": \"\", \"callDescription\": \"\" }, \"fragmentC\": { \"callIcon\": \"\", \"callLabel\": \"\", \"callValue\": \"\", \"callOnnetOutOfRegionLabel\": \"\", \"callOnnetOutOfRegionValue\": \"\", \"callOffnetOutOfRegionLabel\": \"\", \"callOffnetOutOfRegionValue\": \"\", \"titleThree\": \"\", \"valueThree\": \"\", \"titleFour\": \"\", \"valueFour\": \"\", \"callDescription\": \"\" }, \"fragmentD\": { \"callIcon\": \"\", \"callLabel\": \"\", \"callValue\": \"\", \"callOnnetOutOfRegionLabel\": \"\", \"callOnnetOutOfRegionValue\": \"\", \"callOffnetOutOfRegionLabel\": \"\", \"callOffnetOutOfRegionValue\": \"\", \"titleThree\": \"\", \"valueThree\": \"\", \"titleFour\": \"\", \"valueFour\": \"\", \"callDescription\": \"\" }, \"destination\": { \"destinationIcon\": \"tsDRounding\", \"destinationLabel\": \"Charging interval\", \"destinationValue\": \"\", \"destinationOnnetLabel\": \"Countrywide calls\", \"destinationOnnetValue\": \"10 sec\", \"destinationOffnetLabel\": \"International calls\", \"destinationOffnetValue\": \"30 sec\", \"destinationInternationalLabel\": \"Countrywide internet\", \"destinationInternationalValue\": \"40 KB\", \"destinationInternetLabel\": \"\", \"destinationInternetValue\": \"\", \"shortDescription\": \"\" }, \"advantages\": { \"advantagesLabel\": \"Clarification\", \"description\": \"<\\\\n>Minutes provided within the package can’t be used for short numbers.\" }, \"titleSubtitle\": { \"titleLabel\": \"\", \"subtitle1Label\": \"\", \"subtitle1Value\": \"\", \"subtitle2Label\": \"\", \"subtitle2Value\": \"\", \"subtitle3Label\": \"\", \"subtitle3Value\": \"\", \"subtitle4Label\": \"\", \"subtitle4Value\": \"\", \"shortDescription\": \"\" }, \"date\": { \"dateDescription\": \"\", \"fromDateLabel\": \"\", \"fromDateValue\": \"\", \"toDateLabel\": \"\", \"toDateValue\": \"\" }, \"time\": { \"timeDescription\": \"\", \"fromTimeLabel\": \"\", \"fromTimeValue\": \"\", \"toTimeLabel\": \"\", \"toTimeValue\": \"\" }, \"roaming\": { \"country\": [], \"descriptionPosition\": \"0\", \"description\": \"\" }, \"freeResource\": { \"freeResourceLabel\": \"\", \"freeResourceValue\": \"\", \"onnetFreeResourceLabel\": \"\", \"onnetFreeResourceValue\": \"\", \"descriptionFreeResource\": \"\" } } } ], \"execTime\": 1.1239929199219 }";
			com.evampsaanga.magento.tariffdetailsv2.TariffDetailsMagentoResponse data = Helper.JsonToObject(response,
				com.evampsaanga.magento.tariffdetailsv2.TariffDetailsMagentoResponse.class);
		
			JSONObject jsonObject = new JSONObject(response);
			System.out.println("response: "+response);
			System.out.println(jsonObject.getJSONArray("data"));
			System.out.println("Array list :"+jsonObject.getJSONArray("data"));
			String offeringID="1655630758";
			
			
//			compare(offeringID,data.getData());
			
//			List<Datum> salaryMoreThan300 = filterAndGetEmployees(data.getData(),
//					  isSalaryMoreThan("1655630758"));
//			System.out.println("Test: "+salaryMoreThan300.toString());
//			salaryMoreThan300.forEach(e -> System.out.println(e.getHeader().getOfferingId()));
			
			
			
			List<Datum> menu = data.getData();
//			List<Datum> vegitarianDishes = menu.stream()
//					                                    .filter(d -> d.getHeader().getOfferingId().equals("720469688"))
//					                                    .filter(d ->  {
//					                                        if(d.getHeader().getOfferingId().equals("1655630758") || d.getHeader().getOfferingId().equals("720469688") || d.getHeader().getOfferingId().equals("770094093"))
//					                                        	return true;
//					                                        else
//					                                        	return false;
//					                                    })
//					                                    .filter(d -> d.getHeader().getOfferingId().equals("1151813812"))
//					                                    .collect(Collectors.toList());
			
			
			
//			Datum datum = data.getData().stream().filter(d ->  "1655630758".equals(d.getHeader().getOfferingId())).findAny().orElse(null);
//			
//			System.out.println("datum "+datum.getHeader().getOfferingId());
//			System.out.println("**********************************");
////			vegitarianDishes.forEach(System.out::println);
//			System.out.println("**********************************");
			
			
			
			
			
			
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		
		
//		String mrcValue="12.45";
//		Long limitValue = ((long)Double.parseDouble(mrcValue)*Constants.MONEY_DIVIDEND);
//		System.out.println(limitValue);
		
//		String lon = "1010003262919";
//		System.out.println(Long.parseLong(lon));
//		String request ="{\"orderKey\":\"CG008\",\"groupTypeTo\":\"PartPay\",\"groupIdTo\":\"1010003262919\",\"channel\":\"android\",\"lang\":\"3\",\"msisdn\":\"pics_ens1\",\"iP\":\"10.220.245.129\",\"isB2B\":\"true\",\"recieverMsisdn\":[{\"msisdn\":\"555956005\",\"groupType\":\"PaybySubs\",\"groupIdFrom\":\"1010005949215\",\"accountID\":\"1010002894359\"},{\"msisdn\":\"555956003\",\"groupType\":\"PaybySubs\",\"groupIdFrom\":\"1010005949215\",\"accountID\":\"1010002894359\"}]}";
//		JSONObject jsonObject = null;
//		try {
//			jsonObject = new JSONObject(request);
//		} catch (JSONException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		ChangeGroupAttributes changeGroupAttributes = new ChangeGroupAttributes(); 
//		try {
//			ChangeGroup changeGroup = new ChangeGroup(jsonObject, new StringBuffer(), null);
//			changeGroup.processOrder();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		
		
	}
	
	
	



	private static int compare(String offeringID, List<Datum> list) 
	{
		// TODO Auto-generated method stub
		OrderManagementLand.loggerV2.info(list.toString());
		
		for(int i=0;i<list.size();i++)
		{
			if(list.get(i).getHeader().getOfferingId().equals(offeringID))
				return i;
			
				
		}
		return -1;
	}
	
	
	public static boolean updateOrderStatus(String order_id, String status, Connection conn,
			StringBuffer stringBuffer) throws SQLException 
	{
		String query = "update purchase_order set status = ? where order_id= ?";
		if(conn.isClosed())
			conn = DBFactory.getDbConnectionFromPool();
		PreparedStatement preparedStmt = conn.prepareStatement(query);
		preparedStmt = conn.prepareStatement(query);
		preparedStmt.setString(1, status);
		preparedStmt.setString(2, order_id);

//		loggerV2.info("QUERY for update Order_purchase" + preparedStmt.toString());
		preparedStmt.execute();
		int count = preparedStmt.getUpdateCount();
		// check is added for order management,in case of process order logging
		// will not
		// work
		if (stringBuffer != null) {

			stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
			stringBuffer.append(System.getProperty("line.separator"));
			stringBuffer.append(Helper.getOMlogTimeStamp() + "*** Update Purchase Order Details  ");
			stringBuffer.append(System.getProperty("line.separator"));
			stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
			stringBuffer.append(System.getProperty("line.separator"));
			stringBuffer.append(Helper.getOMlogTimeStamp() + "Executing Query : " + preparedStmt.toString());
			stringBuffer.append(System.getProperty("line.separator"));

			if (count > 0)
			{
				stringBuffer.append(Helper.getOMlogTimeStamp() + "Record updated : ");
				stringBuffer.append(System.getProperty("line.separator"));
			}
			else
			{
				stringBuffer.append(Helper.getOMlogTimeStamp() + "Record updated : ");
				stringBuffer.append(System.getProperty("line.separator"));
			}
		}
		if (count > 0)
			return true;
		else
			return false;
	}
	

}
