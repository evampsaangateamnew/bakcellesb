
package com.huawei.crm.query.order.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.order.query.ExtParameterList;
import com.huawei.crm.basetype.order.query.OfferingInfo;
import com.huawei.crm.basetype.order.query.OfferingList;


/**
 * <p>Java class for SubscriberFeeFactorInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubscriberFeeFactorInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="SubscriberId" type="{http://crm.huawei.com/basetype/}SubscriberId" minOccurs="0"/>
 *         &lt;element name="ServiceNumber" type="{http://crm.huawei.com/basetype/}ServiceNumber" minOccurs="0"/>
 *         &lt;element name="ICCID" type="{http://crm.huawei.com/basetype/}ICCID" minOccurs="0"/>
 *         &lt;element name="PrimaryOfferingInfo" type="{http://crm.huawei.com/basetype/}OfferingInfo" minOccurs="0"/>
 *         &lt;element name="SupplementaryOfferingList" type="{http://crm.huawei.com/basetype/}OfferingList" minOccurs="0"/>
 *         &lt;element name="Promotion" type="{http://crm.huawei.com/query/}PromotionList" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubscriberFeeFactorInfo", propOrder = {

})
public class SubscriberFeeFactorInfo {

    @XmlElement(name = "SubscriberId")
    protected Long subscriberId;
    @XmlElement(name = "ServiceNumber")
    protected String serviceNumber;
    @XmlElement(name = "ICCID")
    protected String iccid;
    @XmlElement(name = "PrimaryOfferingInfo")
    protected OfferingInfo primaryOfferingInfo;
    @XmlElement(name = "SupplementaryOfferingList")
    protected OfferingList supplementaryOfferingList;
    @XmlElement(name = "Promotion")
    protected PromotionList promotion;
    @XmlElement(name = "ExtParamList")
    protected ExtParameterList extParamList;

    /**
     * Gets the value of the subscriberId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSubscriberId() {
        return subscriberId;
    }

    /**
     * Sets the value of the subscriberId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSubscriberId(Long value) {
        this.subscriberId = value;
    }

    /**
     * Gets the value of the serviceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceNumber() {
        return serviceNumber;
    }

    /**
     * Sets the value of the serviceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceNumber(String value) {
        this.serviceNumber = value;
    }

    /**
     * Gets the value of the iccid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getICCID() {
        return iccid;
    }

    /**
     * Sets the value of the iccid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setICCID(String value) {
        this.iccid = value;
    }

    /**
     * Gets the value of the primaryOfferingInfo property.
     * 
     * @return
     *     possible object is
     *     {@link OfferingInfo }
     *     
     */
    public OfferingInfo getPrimaryOfferingInfo() {
        return primaryOfferingInfo;
    }

    /**
     * Sets the value of the primaryOfferingInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link OfferingInfo }
     *     
     */
    public void setPrimaryOfferingInfo(OfferingInfo value) {
        this.primaryOfferingInfo = value;
    }

    /**
     * Gets the value of the supplementaryOfferingList property.
     * 
     * @return
     *     possible object is
     *     {@link OfferingList }
     *     
     */
    public OfferingList getSupplementaryOfferingList() {
        return supplementaryOfferingList;
    }

    /**
     * Sets the value of the supplementaryOfferingList property.
     * 
     * @param value
     *     allowed object is
     *     {@link OfferingList }
     *     
     */
    public void setSupplementaryOfferingList(OfferingList value) {
        this.supplementaryOfferingList = value;
    }

    /**
     * Gets the value of the promotion property.
     * 
     * @return
     *     possible object is
     *     {@link PromotionList }
     *     
     */
    public PromotionList getPromotion() {
        return promotion;
    }

    /**
     * Sets the value of the promotion property.
     * 
     * @param value
     *     allowed object is
     *     {@link PromotionList }
     *     
     */
    public void setPromotion(PromotionList value) {
        this.promotion = value;
    }

    /**
     * Gets the value of the extParamList property.
     * 
     * @return
     *     possible object is
     *     {@link ExtParameterList }
     *     
     */
    public ExtParameterList getExtParamList() {
        return extParamList;
    }

    /**
     * Sets the value of the extParamList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtParameterList }
     *     
     */
    public void setExtParamList(ExtParameterList value) {
        this.extParamList = value;
    }

}
