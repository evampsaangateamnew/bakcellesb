package com.evampsaanga.bakcell.manipulatefnf;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.amqimplementationsesb.LogsType;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.ThirdPartyNames;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.developer.utils.SOAPLoggingHandler;
import com.evampsaanga.services.OrderHandleService;
import com.huawei.crm.basetype.ens.OfferingExtParameterInfo;
import com.huawei.crm.basetype.ens.OfferingExtParameterList;
import com.huawei.crm.basetype.ens.OfferingInfo;
import com.huawei.crm.basetype.ens.OfferingKey;
import com.huawei.crm.basetype.ens.OrderInfo;
import com.huawei.crm.basetype.ens.OrderItemInfo;
import com.huawei.crm.basetype.ens.OrderItemValue;
import com.huawei.crm.basetype.ens.OrderItems;
import com.huawei.crm.basetype.ens.ProductInfo;
import com.huawei.crm.basetype.ens.RequestHeader;
import com.huawei.crm.basetype.ens.SubscriberInfo;
import com.huawei.crm.service.ens.SubmitOrderRequest;
import com.huawei.crm.service.ens.SubmitRequestBody;

@Path("/bakcell")
public class ManipulatefnfRequestLand {
	public static final Logger logger = Logger.getLogger("bakcellLogs-V2");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ManipulatefnfClientResponse Get(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		ManipulatefnfClientResponse resp = new ManipulatefnfClientResponse();
		Logs logs = new Logs();
		try {
			logs.setTransactionName(Transactions.MANIPULATE_FNF_TRANSACTION_NAME);
			logs.setThirdPartyName(ThirdPartyNames.MANIPULATE_FNF);
			logs.setTableType(LogsType.ManipulateFnF);
			logger.info("Request Landed on Manipulate fnf:" + requestBody);
			ManipulatefnfClientRequest cclient = new ManipulatefnfClientRequest();
			try {
				cclient = Helper.JsonToObject(requestBody, ManipulatefnfClientRequest.class);
				cclient.setPrimaryOfferingId(getFNFSupplementary(cclient.getOfferingId()));
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setLang(cclient.getLang());
					logs.setIsB2B(cclient.getIsB2B());
					}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					if (!cclient.getFnfNumber().equals("")) {
						cclient.setFnfNumber(Constants.AZERI_COUNTRY_CODE + cclient.getFnfNumber());
					}
					if (!cclient.getActionType().equals("") && !cclient.getFnfNumber().equals("")) {
						com.huawei.crm.service.ens.SubmitOrderResponse response = updatemanipulateFNF(
								cclient.getActionType(), cclient.getFnfNumber(), cclient.getmsisdn());
						if (response.getResponseHeader().getRetCode().equals("0")) {
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						} else {
							resp.setReturnCode(response.getResponseHeader().getRetCode());
							resp.setReturnMsg(response.getResponseHeader().getRetMsg());
						}
					}
					if (cclient.getActionType().equals("") && !cclient.getFnfNumber().equals("")) {
						com.huawei.crm.service.ens.SubmitOrderResponse response = addmanipulateFNF(
								cclient.getFnfNumber(), cclient.getmsisdn(), cclient.getPrimaryOfferingId());
						if (response.getResponseHeader().getRetCode().equals("0")) {
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						} else {
							resp.setReturnCode(response.getResponseHeader().getRetCode());
							resp.setReturnMsg(response.getResponseHeader().getRetMsg());
						}
					}
					if (!cclient.getActionType().equals("")) {
						String fnfnumber = cclient.getActionType().substring(0, 3);
						if (!fnfnumber.equals(Constants.AZERI_COUNTRY_CODE)) {
							cclient.setActionType(Constants.AZERI_COUNTRY_CODE + cclient.getActionType());
						}
					}
					if (!cclient.getActionType().isEmpty() && cclient.getFnfNumber().isEmpty()) {
						com.huawei.crm.service.ens.SubmitOrderResponse response = deletemanipulateFNF(
								cclient.getActionType(), cclient.getmsisdn(), cclient.getPrimaryOfferingId());
						if (response.getResponseHeader().getRetCode().equals("0")) {
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						} else {
							resp.setReturnCode(response.getResponseHeader().getRetCode());
							resp.setReturnMsg(response.getResponseHeader().getRetMsg());
						}
					}
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	public com.huawei.crm.service.ens.SubmitOrderResponse addmanipulateFNF(String fnfnumber, String msisdn,
			String supOfferingId) {
		SubmitOrderRequest submitOrderRequestMsgReq = new SubmitOrderRequest();
		submitOrderRequestMsgReq.setRequestHeader(OrderHandleService.getReqHeaderForManipulateFnF());
		OfferingInfo offeringInfo = new OfferingInfo();
		offeringInfo.setActionType(Constants.FNF_ACTION_TYPE);
		SubmitRequestBody submitRequestBody = new SubmitRequestBody();
		OrderInfo oInfo = new OrderInfo();
		oInfo.setOrderType(Constants.FNF_ORDER_ITEM);
		OrderItemInfo oitemInfo = new OrderItemInfo();
		oitemInfo.setOrderItemType(Constants.FNF_ORDER_ITEM_TYPE);
		submitRequestBody.setOrder(oInfo);
		OrderItems OrderItems = new OrderItems();
		OrderItemValue OrderItemValue = new OrderItemValue();
		OrderItemValue.setOrderItemInfo(oitemInfo);
		SubscriberInfo subscriber = new SubscriberInfo();
		subscriber.setServiceNumber(msisdn);
		OfferingKey offeringId = new OfferingKey();
		offeringId.setOfferingId(supOfferingId);
		offeringInfo.setOfferingId(offeringId);
		ProductInfo productInfo = new ProductInfo();
		productInfo.setProductId(Constants.FNF_PODUCT_ID);
		productInfo.setSelectFlag(Constants.FNF_SELECT_FLAG);
		OfferingExtParameterInfo offeringExtPaInfo = new OfferingExtParameterInfo();
		offeringExtPaInfo.setParamName(Constants.FNF_INFO);
		offeringExtPaInfo.setParamValue(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
		OfferingExtParameterList offeringExtParamList = new OfferingExtParameterList();
		OfferingExtParameterInfo param1 = new OfferingExtParameterInfo();
		param1.setParamName(Constants.FNF_SERIAL_NUMBER);
		param1.setParamValue(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
		offeringExtParamList.getParameterInfo().add(param1);
		OfferingExtParameterInfo param2 = new OfferingExtParameterInfo();
		param2.setParamName(Constants.FNF_NUMBER);
		param2.setParamValue(fnfnumber);
		offeringExtParamList.getParameterInfo().add(param2);
		offeringExtPaInfo.setExtParamList(offeringExtParamList);
		productInfo.getExtParamList().getParameterInfo().add(offeringExtPaInfo);
		offeringInfo.getProductList().getProductInfo().add(productInfo);
		subscriber.getSupplementaryOfferingList().getOfferingInfo().add(offeringInfo);
		OrderItemValue.setSubscriber(subscriber);
		OrderItems.getOrderItem().add(OrderItemValue);
		submitRequestBody.setOrderItems(OrderItems);
		submitOrderRequestMsgReq.setSubmitRequestBody(submitRequestBody);
		com.huawei.crm.service.ens.SubmitOrderResponse response = OrderHandleService.getInstance()
				.submitOrder(submitOrderRequestMsgReq);
		return response;
	}

	public com.huawei.crm.service.ens.SubmitOrderResponse deletemanipulateFNF(String action, String msisdn,
			String supOfferingId) {
		SubmitOrderRequest submitOrderRequestMsgReq = new SubmitOrderRequest();
		submitOrderRequestMsgReq.setRequestHeader(OrderHandleService.getReqHeaderForManipulateFnF());
		OfferingInfo offeringInfo = new OfferingInfo();
		offeringInfo.setActionType(Constants.FNF_ACTION_TYPE);
		SubmitRequestBody submitRequestBody = new SubmitRequestBody();
		OrderInfo oInfo = new OrderInfo();
		oInfo.setOrderType(Constants.FNF_ORDER_ITEM);
		OrderItemInfo oitemInfo = new OrderItemInfo();
		oitemInfo.setOrderItemType(Constants.FNF_ORDER_ITEM_TYPE);
		submitRequestBody.setOrder(oInfo);
		OrderItems OrderItems = new OrderItems();
		OrderItemValue OrderItemValue = new OrderItemValue();
		OrderItemValue.setOrderItemInfo(oitemInfo);
		SubscriberInfo subscriber = new SubscriberInfo();
		subscriber.setServiceNumber(msisdn);
		OfferingKey offeringId = new OfferingKey();
		offeringId.setOfferingId(supOfferingId);
		offeringInfo.setOfferingId(offeringId);
		ProductInfo productInfo = new ProductInfo();
		productInfo.setProductId(Constants.FNF_PODUCT_ID);
		productInfo.setSelectFlag(Constants.FNF_SELECT_FLAG);
		OfferingExtParameterInfo offeringExtPaInfo = new OfferingExtParameterInfo();
		offeringExtPaInfo.setParamName(Constants.FNF_INFO);
		offeringExtPaInfo.setParamValue(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
		OfferingExtParameterList offeringExtParamList = new OfferingExtParameterList();
		OfferingExtParameterInfo param1 = new OfferingExtParameterInfo();
		param1.setParamName(Constants.FNF_SERIAL_NUMBER);
		param1.setParamValue(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
		offeringExtParamList.getParameterInfo().add(param1);
		OfferingExtParameterInfo param2 = new OfferingExtParameterInfo();
		param2.setParamName(Constants.FNF_NUMBER);
		param2.setParamOldValue(action);
		offeringExtParamList.getParameterInfo().add(param2);
		offeringExtPaInfo.setExtParamList(offeringExtParamList);
		productInfo.getExtParamList().getParameterInfo().add(offeringExtPaInfo);
		offeringInfo.getProductList().getProductInfo().add(productInfo);
		subscriber.getSupplementaryOfferingList().getOfferingInfo().add(offeringInfo);
		OrderItemValue.setSubscriber(subscriber);
		OrderItems.getOrderItem().add(OrderItemValue);
		submitRequestBody.setOrderItems(OrderItems);
		submitOrderRequestMsgReq.setSubmitRequestBody(submitRequestBody);
		com.huawei.crm.service.ens.SubmitOrderResponse response = OrderHandleService.getInstance()
				.submitOrder(submitOrderRequestMsgReq);
		return response;
	}

	/**
	 * Method updates already added FNF (not used in this version)
	 * 
	 * @param action
	 * @param fnfnumber
	 * @param msidn
	 * @return
	 */
	public com.huawei.crm.service.ens.SubmitOrderResponse updatemanipulateFNF(String action, String fnfnumber,
			String msidn) {
		RequestHeader reqH = new RequestHeader();
		reqH.setVersion("1");
		reqH.setChannelId("3");
		reqH.setTechnicalChannelId("53");
		reqH.setAccessUser("ecare");
		reqH.setAccessPwd("r8q0a5WwGNboj9I35XzNcQ==");
		reqH.setTransactionId(Helper.generateTransactionID());
		SubmitOrderRequest submitOrderRequestMsgReq = new SubmitOrderRequest();
		submitOrderRequestMsgReq.setRequestHeader(reqH);
		OfferingInfo offeringInfo = new OfferingInfo();
		offeringInfo.setActionType("2");
		SubmitRequestBody submitRequestBody = new SubmitRequestBody();
		OrderInfo oInfo = new OrderInfo();
		oInfo.setOrderType("CO075");
		OrderItemInfo oitemInfo = new OrderItemInfo();
		oitemInfo.setOrderItemType("CO075");
		submitRequestBody.setOrder(oInfo);
		OrderItems OrderItems = new OrderItems();
		OrderItemValue OrderItemValue = new OrderItemValue();
		OrderItemValue.setOrderItemInfo(oitemInfo);
		SubscriberInfo subscriber = new SubscriberInfo();
		subscriber.setServiceNumber(msidn);
		OfferingKey offeringId = new OfferingKey();
		offeringId.setOfferingId("1073278679");
		offeringInfo.setOfferingId(offeringId);
		ProductInfo productInfo = new ProductInfo();
		productInfo.setProductId("1023");
		productInfo.setSelectFlag("1");
		OfferingExtParameterInfo offeringExtPaInfo = new OfferingExtParameterInfo();
		offeringExtPaInfo.setParamName("C_FNINFO");
		offeringExtPaInfo.setParamValue("1525498661");
		OfferingExtParameterList offeringExtParamList = new OfferingExtParameterList();
		OfferingExtParameterInfo param1 = new OfferingExtParameterInfo();
		param1.setParamName("C_FN_SERIAL_NO");
		// param1.setParamValue(new
		// SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
		param1.setParamValue("1525498661");
		offeringExtParamList.getParameterInfo().add(param1);
		OfferingExtParameterInfo param2 = new OfferingExtParameterInfo();
		param2.setParamName("C_FN_NUMBER");
		param2.setParamValue(fnfnumber);
		param2.setParamOldValue(action);
		offeringExtParamList.getParameterInfo().add(param2);
		offeringExtPaInfo.setExtParamList(offeringExtParamList);
		productInfo.getExtParamList().getParameterInfo().add(offeringExtPaInfo);
		offeringInfo.getProductList().getProductInfo().add(productInfo);
		subscriber.getSupplementaryOfferingList().getOfferingInfo().add(offeringInfo);
		OrderItemValue.setSubscriber(subscriber);
		OrderItems.getOrderItem().add(OrderItemValue);
		submitRequestBody.setOrderItems(OrderItems);
		submitOrderRequestMsgReq.setSubmitRequestBody(submitRequestBody);
		com.huawei.crm.service.ens.SubmitOrderResponse response = OrderHandleService.getInstance()
				.submitOrder(submitOrderRequestMsgReq);
		return response;
	}
	
	public String getFNFSupplementary(String key) {
	String value = ConfigurationManager.getConfigurationFromCache("fnf.offering.id.limit."+key);
	if(value == null)
		value = "5";
	if(value != null && value.contains(","))
		return value.split(",")[1];
	return value;
}
	
}
