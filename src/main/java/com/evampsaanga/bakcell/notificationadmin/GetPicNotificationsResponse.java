package com.evampsaanga.bakcell.notificationadmin;

import java.util.List;

import com.evampsaanga.bakcell.responseheaders.BaseResponse;

public class GetPicNotificationsResponse extends BaseResponse {
	private List<PicNotificationData> picNotifications;

	@Override
	public String toString() {
		return "GetPicNotificationsResponse [picNotifications=" + picNotifications + ", toString()=" + super.toString()
				+ "]";
	}

	public List<PicNotificationData> getPicNotifications() {
		return picNotifications;
	}

	public void setPicNotifications(List<PicNotificationData> picNotifications) {
		this.picNotifications = picNotifications;
	}
}
