package com.evampsaanga.magentoswervices.mysubscriptionofferingids;

import com.evampsaanga.bakcell.requestheaders.BaseRequest;

public class MySubscriptionsRequestClient extends BaseRequest {
	private String offeringIds = "";

	public String getOfferingIds() {
		return offeringIds;
	}

	public void setOfferingIds(String offeringIds) {
		this.offeringIds = offeringIds;
	}
}
