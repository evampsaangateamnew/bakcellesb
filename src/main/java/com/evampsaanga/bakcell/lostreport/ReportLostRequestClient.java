package com.evampsaanga.bakcell.lostreport;

import com.evampsaanga.bakcell.requestheaders.BaseRequest;

public class ReportLostRequestClient extends BaseRequest {
	String reasonCode = "";
	String operationType = "";

	public String getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	@Override
	public String toString() {
		return "ReportLostRequestClient [reasonCode=" + reasonCode + ", operationType=" + operationType + "]";
	}
	
	
}
