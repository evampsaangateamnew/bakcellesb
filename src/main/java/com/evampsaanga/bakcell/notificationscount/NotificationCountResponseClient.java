package com.evampsaanga.bakcell.notificationscount;

import com.evampsaanga.bakcell.responseheaders.BaseResponse;

public class NotificationCountResponseClient extends BaseResponse {
	Data data = new Data();

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}
}
