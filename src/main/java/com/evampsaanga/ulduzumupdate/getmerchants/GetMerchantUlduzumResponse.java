package com.evampsaanga.ulduzumupdate.getmerchants;

import java.util.List;

public class GetMerchantUlduzumResponse {
	String result;
	List<Merchant> data;

	/**
	 * @return the result
	 */
	public String getResult() {
		return result;
	}

	/**
	 * @param result
	 *            the result to set
	 */
	public void setResult(String result) {
		this.result = result;
	}

	/**
	 * @return the data
	 */
	public List<Merchant> getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(List<Merchant> data) {
		this.data = data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "GetMerchantUlduzumResponse [result=" + result + ", data=" + data + "]";
	}

}
