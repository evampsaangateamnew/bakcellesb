package com.evampsaanga.bakcell.notificationadmin;

import java.util.List;

public class GetReportsAppserverResponse {
	private String callStatus;
	private String resultCode;
	private String resultDesc;
	private List<ReportData> reportData;

	public List<ReportData> getReportData() {
		return reportData;
	}

	public String getCallStatus() {
		return callStatus;
	}

	public void setCallStatus(String callStatus) {
		this.callStatus = callStatus;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getResultDesc() {
		return resultDesc;
	}

	public void setResultDesc(String resultDesc) {
		this.resultDesc = resultDesc;
	}

	public void setReportData(List<ReportData> reportData) {
		this.reportData = reportData;
	}

	@Override
	public String toString() {
		return "GetReportsAppserverResponse [callStatus=" + callStatus + ", resultCode=" + resultCode + ", resultDesc="
				+ resultDesc + ", reportData=" + reportData + "]";
	}
}
