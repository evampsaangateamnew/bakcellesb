package com.evampsaanga.ulduzumupdate.getmerchants;

import java.util.List;

import com.evampsaanga.bakcell.responseheaders.BaseResponse;

public class GetMerchantsResponseClient extends BaseResponse {

	List<Merchant> data;

	/**
	 * @return the data
	 */
	public List<Merchant> getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(List<Merchant> data) {
		this.data = data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "GetMerchantsResponseclient [data=" + data + "]";
	}

}
