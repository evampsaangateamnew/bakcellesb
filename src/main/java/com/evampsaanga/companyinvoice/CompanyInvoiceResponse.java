
package com.evampsaanga.companyinvoice;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "returnCode",
    "returnMsg",
    "companySummary",
    "companyDetail"
})
public class CompanyInvoiceResponse {

    @JsonProperty("returnCode")
    private String returnCode;
    @JsonProperty("returnMsg")
    private String returnMsg;
    @JsonProperty("companySummary")
    private CompanySummary companySummary;
    @JsonProperty("companyDetail")
    private CompanyDetail companyDetail;

    @JsonProperty("returnCode")
    public String getReturnCode() {
        return returnCode;
    }

    @JsonProperty("returnCode")
    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    @JsonProperty("returnMsg")
    public String getReturnMsg() {
        return returnMsg;
    }

    @JsonProperty("returnMsg")
    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    @JsonProperty("companySummary")
    public CompanySummary getCompanySummary() {
        return companySummary;
    }

    @JsonProperty("companySummary")
    public void setCompanySummary(CompanySummary companySummary) {
        this.companySummary = companySummary;
    }

    @JsonProperty("companyDetail")
    public CompanyDetail getCompanyDetail() {
        return companyDetail;
    }

    @JsonProperty("companyDetail")
    public void setCompanyDetail(CompanyDetail companyDetail) {
        this.companyDetail = companyDetail;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("returnCode", returnCode).append("returnMsg", returnMsg).append("companySummary", companySummary).append("companyDetail", companyDetail).toString();
    }

}
