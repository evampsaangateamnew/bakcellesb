package com.evampsaanga.bakcell.mnpgetsubscriber;

import com.evampsaanga.bakcell.requestheaders.BaseRequest;

public class MNPGetSubscriberRequestClient extends BaseRequest {
	private String passportNumber = "";

	/**
	 * @return the passportNumber
	 */
	public String getPassportNumber() {
		return passportNumber;
	}

	/**
	 * @param passportNumber
	 *            the passportNumber to set
	 */
	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}
}
