package com.huawei.bme.cbsinterface.arservices;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.cbs.ar.wsservice.arcommon.AcctAccessCode;
import com.huawei.cbs.ar.wsservice.arcommon.SimpleProperty;

/**
 * <p>
 * Java class for TransferBalanceRequest complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="TransferBalanceRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TransferType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TransferorAcct" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://cbs.huawei.com/ar/wsservice/arcommon}AcctAccessCode">
 *                 &lt;sequence>
 *                   &lt;element name="PayType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="TransfereeAcct" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://cbs.huawei.com/ar/wsservice/arcommon}AcctAccessCode">
 *                 &lt;sequence>
 *                   &lt;element name="PayType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="SrcBalanceType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DestBalanceType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransferAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="Remark" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AdditionalProperty" type="{http://cbs.huawei.com/ar/wsservice/arcommon}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransferBalanceRequest", propOrder = { "transferType", "transferorAcct", "transfereeAcct",
		"srcBalanceType", "destBalanceType", "transferAmount", "remark", "additionalProperty" })
public class TransferBalanceRequest {
	@XmlElement(name = "TransferType", required = true)
	protected String transferType;
	@XmlElement(name = "TransferorAcct")
	protected TransferBalanceRequest.TransferorAcct transferorAcct;
	@XmlElement(name = "TransfereeAcct")
	protected TransferBalanceRequest.TransfereeAcct transfereeAcct;
	@XmlElement(name = "SrcBalanceType")
	protected String srcBalanceType;
	@XmlElement(name = "DestBalanceType")
	protected String destBalanceType;
	@XmlElement(name = "TransferAmount")
	protected String transferAmount;
	@XmlElement(name = "Remark")
	protected String remark;
	@XmlElement(name = "AdditionalProperty")
	protected List<SimpleProperty> additionalProperty;

	/**
	 * Gets the value of the transferType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTransferType() {
		return transferType;
	}

	/**
	 * Sets the value of the transferType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTransferType(String value) {
		this.transferType = value;
	}

	/**
	 * Gets the value of the transferorAcct property.
	 * 
	 * @return possible object is {@link TransferBalanceRequest.TransferorAcct }
	 * 
	 */
	public TransferBalanceRequest.TransferorAcct getTransferorAcct() {
		return transferorAcct;
	}

	/**
	 * Sets the value of the transferorAcct property.
	 * 
	 * @param value
	 *            allowed object is
	 *            {@link TransferBalanceRequest.TransferorAcct }
	 * 
	 */
	public void setTransferorAcct(TransferBalanceRequest.TransferorAcct value) {
		this.transferorAcct = value;
	}

	/**
	 * Gets the value of the transfereeAcct property.
	 * 
	 * @return possible object is {@link TransferBalanceRequest.TransfereeAcct }
	 * 
	 */
	public TransferBalanceRequest.TransfereeAcct getTransfereeAcct() {
		return transfereeAcct;
	}

	/**
	 * Sets the value of the transfereeAcct property.
	 * 
	 * @param value
	 *            allowed object is
	 *            {@link TransferBalanceRequest.TransfereeAcct }
	 * 
	 */
	public void setTransfereeAcct(TransferBalanceRequest.TransfereeAcct value) {
		this.transfereeAcct = value;
	}

	/**
	 * Gets the value of the srcBalanceType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSrcBalanceType() {
		return srcBalanceType;
	}

	/**
	 * Sets the value of the srcBalanceType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSrcBalanceType(String value) {
		this.srcBalanceType = value;
	}

	/**
	 * Gets the value of the destBalanceType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDestBalanceType() {
		return destBalanceType;
	}

	/**
	 * Sets the value of the destBalanceType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDestBalanceType(String value) {
		this.destBalanceType = value;
	}

	/**
	 * Gets the value of the transferAmount property.
	 * 
	 */
	public String getTransferAmount() {
		return transferAmount;
	}

	/**
	 * Sets the value of the transferAmount property.
	 * 
	 */
	public void setTransferAmount(String value) {
		this.transferAmount = value;
	}

	/**
	 * Gets the value of the remark property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * Sets the value of the remark property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark(String value) {
		this.remark = value;
	}

	/**
	 * Gets the value of the additionalProperty property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the additionalProperty property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getAdditionalProperty().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link SimpleProperty }
	 * 
	 * 
	 */
	public List<SimpleProperty> getAdditionalProperty() {
		if (additionalProperty == null) {
			additionalProperty = new ArrayList<SimpleProperty>();
		}
		return this.additionalProperty;
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;extension base="{http://cbs.huawei.com/ar/wsservice/arcommon}AcctAccessCode">
	 *       &lt;sequence>
	 *         &lt;element name="PayType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *       &lt;/sequence>
	 *     &lt;/extension>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "payType" })
	public static class TransfereeAcct extends AcctAccessCode {
		@XmlElement(name = "PayType")
		protected String payType;

		/**
		 * Gets the value of the payType property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getPayType() {
			return payType;
		}

		/**
		 * Sets the value of the payType property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setPayType(String value) {
			this.payType = value;
		}
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;extension base="{http://cbs.huawei.com/ar/wsservice/arcommon}AcctAccessCode">
	 *       &lt;sequence>
	 *         &lt;element name="PayType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *       &lt;/sequence>
	 *     &lt;/extension>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "payType" })
	public static class TransferorAcct extends AcctAccessCode {
		@XmlElement(name = "PayType")
		protected String payType;

		/**
		 * Gets the value of the payType property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getPayType() {
			return payType;
		}

		/**
		 * Sets the value of the payType property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setPayType(String value) {
			this.payType = value;
		}
	}
}
