
package com.evampsaanga.msisdninvoice;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;

import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "returnCode",
    "returnMsg",
    "msisdnSummary",
    "msisdnDetail",
    "totalValue"
    
})
public class MSISDNInvoiceResponse {

	
	@JsonProperty("totalValue")
    private String totalValue;
    @JsonProperty("returnCode")
    private String returnCode;
    @JsonProperty("returnMsg")
    private String returnMsg;
    
    
    
    
    @JsonProperty("totalValue")
    public String getTotalValue() {
		return totalValue;
	}
    @JsonProperty("totalValue")
	public void setTotalValue(String totalValue) {
		this.totalValue = totalValue;
	}

	@JsonProperty("msisdnSummary")
    private ArrayList<MSISDNSummary> msisdnSummary;
    @JsonProperty("msisdnDetail")
    private MSISDNDetail msisdnDetail;

    @JsonProperty("returnCode")
    public String getReturnCode() {
        return returnCode;
    }

    @JsonProperty("returnCode")
    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    @JsonProperty("returnMsg")
    public String getReturnMsg() {
        return returnMsg;
    }

    @JsonProperty("returnMsg")
    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    @JsonProperty("msisdnSummary")
    public ArrayList<MSISDNSummary> getMsisdnSummary() {
		return msisdnSummary;
	}
    @JsonProperty("msisdnSummary")
	public void setMsisdnSummary(ArrayList<MSISDNSummary> msisdnSummary) {
		this.msisdnSummary = msisdnSummary;
	}

    @JsonProperty("msisdnDetail")
    public MSISDNDetail getMsisdnDetail() {
        return msisdnDetail;
    }

   
	@JsonProperty("msisdnDetail")
    public void setMsisdnDetail(MSISDNDetail msisdnDetail) {
        this.msisdnDetail = msisdnDetail;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("returnCode", returnCode).append("returnMsg", returnMsg).append("msisdnSummary", msisdnSummary).append("msisdnDetail", msisdnDetail).toString();
    }

}
