
package com.huawei.bss.soaif._interface.orderservice.createorder;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.huawei.bss.soaif._interface.orderservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CreateOrderReqMsgAccountNewAccountTitle_QNAME = new QName("http://www.huawei.com/bss/soaif/interface/OrderService/", "Title");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.huawei.bss.soaif._interface.orderservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateOrderReqMsg }
     * 
     */
    public CreateOrderReqMsg createCreateOrderReqMsg() {
        return new CreateOrderReqMsg();
    }

    /**
     * Create an instance of {@link CreateOrderReqMsg.Account }
     * 
     */
    public CreateOrderReqMsg.Account createCreateOrderReqMsgAccount() {
        return new CreateOrderReqMsg.Account();
    }

    /**
     * Create an instance of {@link CreateOrderReqMsg.Customer }
     * 
     */
    public CreateOrderReqMsg.Customer createCreateOrderReqMsgCustomer() {
        return new CreateOrderReqMsg.Customer();
    }

    /**
     * Create an instance of {@link CreateOrderRspMsg }
     * 
     */
    public CreateOrderRspMsg createCreateOrderRspMsg() {
        return new CreateOrderRspMsg();
    }

    /**
     * Create an instance of {@link CreateOrderReqMsg.Order }
     * 
     */
    public CreateOrderReqMsg.Order createCreateOrderReqMsgOrder() {
        return new CreateOrderReqMsg.Order();
    }

    /**
     * Create an instance of {@link CreateOrderReqMsg.Subscriber }
     * 
     */
    public CreateOrderReqMsg.Subscriber createCreateOrderReqMsgSubscriber() {
        return new CreateOrderReqMsg.Subscriber();
    }

    /**
     * Create an instance of {@link CreateOrderReqMsg.PrimaryOffering }
     * 
     */
    public CreateOrderReqMsg.PrimaryOffering createCreateOrderReqMsgPrimaryOffering() {
        return new CreateOrderReqMsg.PrimaryOffering();
    }

    /**
     * Create an instance of {@link CreateOrderReqMsg.SupplementaryOffering }
     * 
     */
    public CreateOrderReqMsg.SupplementaryOffering createCreateOrderReqMsgSupplementaryOffering() {
        return new CreateOrderReqMsg.SupplementaryOffering();
    }

    /**
     * Create an instance of {@link CreateOrderReqMsg.Account.NewAccount }
     * 
     */
    public CreateOrderReqMsg.Account.NewAccount createCreateOrderReqMsgAccountNewAccount() {
        return new CreateOrderReqMsg.Account.NewAccount();
    }

    /**
     * Create an instance of {@link CreateOrderReqMsg.Customer.NewCustomer }
     * 
     */
    public CreateOrderReqMsg.Customer.NewCustomer createCreateOrderReqMsgCustomerNewCustomer() {
        return new CreateOrderReqMsg.Customer.NewCustomer();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.huawei.com/bss/soaif/interface/OrderService/", name = "Title", scope = CreateOrderReqMsg.Account.NewAccount.class)
    public JAXBElement<String> createCreateOrderReqMsgAccountNewAccountTitle(String value) {
        return new JAXBElement<String>(_CreateOrderReqMsgAccountNewAccountTitle_QNAME, String.class, CreateOrderReqMsg.Account.NewAccount.class, value);
    }

}
