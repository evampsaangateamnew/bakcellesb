package com.nsn.devicemanagement.wsdl.provisioning._2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import com.nsn.devicemanagement.wsdl.common._2.DmMap;
import com.nsn.devicemanagement.wsdl.common._2.PersonalParams;

/**
 * <p>
 * Java class for submitProvisionReq complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="submitProvisionReq">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="msisdn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="imeiOrPhoneType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="groupName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="personParams" type="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}personalParams" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="specialProperties" type="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}dmMap" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "submitProvisionReq", propOrder = { "msisdn", "imeiOrPhoneType", "groupName", "personParams",
		"specialProperties" })
public class SubmitProvisionReq {
	protected String msisdn;
	protected String imeiOrPhoneType;
	protected String groupName;
	protected List<PersonalParams> personParams;
	protected DmMap specialProperties;

	/**
	 * Gets the value of the msisdn property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMsisdn() {
		return msisdn;
	}

	/**
	 * Sets the value of the msisdn property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMsisdn(String value) {
		this.msisdn = value;
	}

	/**
	 * Gets the value of the imeiOrPhoneType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getImeiOrPhoneType() {
		return imeiOrPhoneType;
	}

	/**
	 * Sets the value of the imeiOrPhoneType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setImeiOrPhoneType(String value) {
		this.imeiOrPhoneType = value;
	}

	/**
	 * Gets the value of the groupName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * Sets the value of the groupName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setGroupName(String value) {
		this.groupName = value;
	}

	/**
	 * Gets the value of the personParams property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the personParams property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getPersonParams().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link PersonalParams }
	 * 
	 * 
	 */
	public List<PersonalParams> getPersonParams() {
		if (personParams == null) {
			personParams = new ArrayList<PersonalParams>();
		}
		return this.personParams;
	}

	/**
	 * Gets the value of the specialProperties property.
	 * 
	 * @return possible object is {@link DmMap }
	 * 
	 */
	public DmMap getSpecialProperties() {
		return specialProperties;
	}

	/**
	 * Sets the value of the specialProperties property.
	 * 
	 * @param value
	 *            allowed object is {@link DmMap }
	 * 
	 */
	public void setSpecialProperties(DmMap value) {
		this.specialProperties = value;
	}
}
