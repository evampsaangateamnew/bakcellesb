package com.evampsaanga.bakcell.getnetworkingssettings;

import com.evampsaanga.bakcell.responseheaders.BaseResponse;

public class GetNetworkSettingsResponse extends BaseResponse {
	com.huawei.crm.query.GetNetworkSettingDataList getNetworkSettingDataBody=null;

	public com.huawei.crm.query.GetNetworkSettingDataList getGetNetworkSettingDataBody() {
		return getNetworkSettingDataBody;
	}

	public void setGetNetworkSettingDataBody(com.huawei.crm.query.GetNetworkSettingDataList getNetworkSettingDataBody) {
		this.getNetworkSettingDataBody = getNetworkSettingDataBody;
	}
	

}
