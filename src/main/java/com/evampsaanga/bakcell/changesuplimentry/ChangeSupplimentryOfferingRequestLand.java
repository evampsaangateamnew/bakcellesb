package com.evampsaanga.bakcell.changesuplimentry;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.amqimplementationsesb.LogsType;
import com.evampsaanga.bakcell.hlrweb.getquerybalanceRequest.HLRBalanceServices;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.ThirdPartyNames;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.developer.utils.SOAPLoggingHandler;
import com.evampsaanga.gethomepage.Balance;
import com.evampsaanga.services.USSDService;
import com.huawei.bss.soaif._interface.common.ReqHeader;
import com.huawei.bss.soaif._interface.ussdgateway.USSDGWChngSOReqMsg;
import com.huawei.bss.soaif._interface.ussdgateway.USSDGWChngSORspMsg;
import com.huawei.crm.service.ens.SubmitOrderResponse;

@Path("/bakcell")
public class ChangeSupplimentryOfferingRequestLand {
	public static final Logger logger = Logger.getLogger("bakcellLogs-V2");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response Get(@Header("credentials") String credential, @Header("Content-Type") String contentType,
			@Body() String requestBody) {
		Response resp = new Response();
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.CHANGE_SUPPLIMENTRY_OFFERING_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.CHANGE_SUPPLIMENTRY_OFFERING);
		logs.setTableType(LogsType.ChangeSupplimentryOffering);
		try {
			logger.info("Request Landed on ChangeSupplimentryOfferingRequestLand Land:" + requestBody);
			Request cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, Request.class);

				if (cclient != null) {
					if(cclient.getIsB2B()!=null && cclient.getIsB2B().equals("true"))
						logs.setTransactionName(Transactions.CHANGE_SUPPLIMENTRY_OFFERING_TRANSACTION_NAME_B2B);
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setLang(cclient.getLang());
					logs.setActivatedOfferId(cclient.getOfferingId());
					logs.setIsB2B(cclient.getIsB2B());
				}

			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					int actionType = -1;
					try {
						actionType = Integer.parseInt(cclient.getActionType());
						if (actionType != 1 && actionType != 3) {
							resp.setReturnCode(ResponseCodes.MISSING_PARAMETER_CODE);
							resp.setReturnMsg(ResponseCodes.MISSING_PARAMETER_DES);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception ex) {
						resp.setReturnCode(ResponseCodes.MISSING_PARAMETER_CODE);
						resp.setReturnMsg(ResponseCodes.MISSING_PARAMETER_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
					if (actionType == 1) 
					{
						
                        if (cclient.getOfferingId().equalsIgnoreCase("1151806699"))
                        {
                        	
                        	
	                    	Calendar currentDayOneHourStart = Calendar.getInstance();
	                    	currentDayOneHourStart.set(Calendar.HOUR_OF_DAY, 0);
	                    	currentDayOneHourStart.set(Calendar.MINUTE, 0);
	                    	currentDayOneHourStart.set(Calendar.SECOND, 0);
	                        logger.info("<<< Current Time >>>:" + new Date());
	                    	
	                        Calendar currentDayOneHourEnd = Calendar.getInstance();
	                        currentDayOneHourEnd.set(Calendar.HOUR_OF_DAY, 1);
	                        currentDayOneHourEnd.set(Calendar.MINUTE, 0);
	                        currentDayOneHourEnd.set(Calendar.SECOND, 0);
	                        logger.info("<<< Current Time >>>:" + new Date());
                        	
	                          Calendar startCal = Calendar.getInstance();
	                          startCal.set(Calendar.HOUR_OF_DAY, 19);
	                          startCal.set(Calendar.MINUTE, 0);
	                          startCal.set(Calendar.SECOND, 0);
	                          logger.info("<<< Current Time >>>:" + new Date());
	                           
	                          logger.info("<<< Start Time >>>:" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss a").format(startCal.getTime()));
	                          logger.info("<<< Start Time >>>:" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss a").format(startCal.getTime()));
	                           
	                          Calendar endCal = Calendar.getInstance();
//	                          endCal.add(Calendar.DATE, 1);
	                          endCal.set(Calendar.HOUR_OF_DAY, 0);
	                          endCal.set(Calendar.MINUTE, 0);
	                          endCal.set(Calendar.SECOND, 0);
	                           
	                          logger.info("<<< End Time >>>:" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss a").format(endCal.getTime()));
	                          
	                          
	                          Calendar currentDate = Calendar.getInstance();
	                          
	                          
	                          logger.info("<<< Current Time >>>:" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss a").format(currentDate.getTime()));
                          logger.info("First Check "+(currentDate.after(currentDayOneHourStart) && (currentDate.before(currentDayOneHourEnd) )));
                          
                          logger.info("second check"+((currentDate.after(startCal)) && (currentDate.before(endCal))));
                          logger.info("second check detail "+(currentDate.after(startCal)));
                          logger.info("second check detail "+(currentDate.before(endCal)));
                          logger.info("complete"+((currentDate.after(currentDayOneHourStart) && (currentDate.before(currentDayOneHourEnd) ))||((currentDate.after(startCal)) && (currentDate.before(endCal)))));
                          
                          
                          if ((currentDate.after(currentDayOneHourStart) && (currentDate.before(currentDayOneHourEnd) ))||((currentDate.after(startCal)) )) //&& (currentDate.before(endCal))
                          {
                            resp.setReturnCode(ResponseCodes.OFFER_SUBSCRIPTION_NOT_ALLOWED_CODE);
                            resp.setReturnMsg(ResponseCodes.OFFER_SUBSCRIPTION_NOT_ALLOWED_DES);
                            logs.setResponseCode(resp.getReturnCode());
                            logs.setResponseDescription(resp.getReturnMsg());
                            logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
                            logs.updateLog(logs);
                            return resp;
                          }
                          SubmitOrderResponse submitOrderResponse =  ChangeSupplimentryOfferingSignupRequestLand.changeSupplementaryOffers(String.valueOf(actionType), "1151806699,951809759", cclient.getmsisdn());
                          if (submitOrderResponse.getResponseHeader().getRetCode().equals("0")) {
      						
//      						Thread.sleep(25000);
//      						USSDGWChngSORspMsg responeForOnceActivating = ussdChangeSuplimentryOfferings(actionType, cclient.getmsisdn(),
//      								"951809759");
      						
      							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
      							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
      							logs.setResponseCode(resp.getReturnCode());
      							logs.setResponseDescription(resp.getReturnMsg());
      							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
      							logs.updateLog(logs);
      							return resp;
      						
      						

      						/*
      						 * if(actionType==1){ logs.setTransactionName(""); }else
      						 * if(actionType==3){ logs.setTransactionName(""); }
      						 */
      					
      					} else {
      						resp.setReturnCode(submitOrderResponse.getResponseHeader().getRetCode());
      						resp.setReturnMsg(submitOrderResponse.getResponseHeader().getRetMsg());
      						logs.setResponseCode(resp.getReturnCode());
      						logs.setResponseDescription(resp.getReturnMsg());
      						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
      						logs.updateLog(logs);
      						return resp;
      					}
                        }
                    }
					
					USSDGWChngSORspMsg respone = ussdChangeSuplimentryOfferings(actionType, cclient.getmsisdn(),cclient.getOfferingId());
					if (respone.getRspHeader().getReturnCode().equals("0000")) {
  						
//						Thread.sleep(25000);
//						USSDGWChngSORspMsg responeForOnceActivating = ussdChangeSuplimentryOfferings(actionType, cclient.getmsisdn(),
//								"951809759");
						
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						
						

						/*
						 * if(actionType==1){ logs.setTransactionName(""); }else
						 * if(actionType==3){ logs.setTransactionName(""); }
						 */
					
					} else {
						resp.setReturnCode(respone.getRspHeader().getReturnCode());
						resp.setReturnMsg(respone.getRspHeader().getReturnMsg());
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
					
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	// funtion for Phase 2
	public Response changeSupplementaryV2(RequestLandBulk request) {
		Response resp = new Response();
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.CHANGE_SUPPLIMENTRY_OFFERING_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.CHANGE_SUPPLIMENTRY_OFFERING);
		logs.setTableType(LogsType.ChangeSupplimentryOffering);
		try {
			logger.info("MSISDN:" + request.getmsisdn()
					+ " Request Landed on ChangeSupplimentryOfferingRequestLand BUlk:" + request.toString());

			try {
				if (request != null && !request.getmsisdn().equals("")) {
					logs.setIp(request.getiP());
					logs.setChannel(request.getChannel());
					logs.setMsisdn(request.getmsisdn());
					logs.setLang(request.getLang());
					logs.setActivatedOfferId(request.getOfferingId());
					logs.setIsB2B(request.getIsB2B());
				}

			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (request != null) {
				logs.setIp(request.getiP());
				logs.setChannel(request.getChannel());
				logs.setMsisdn(request.getmsisdn());

				if (!request.getmsisdn().equals("")) {
					int actionType = -1;
					try {
						actionType = Integer.parseInt(request.getActionType());
						if (actionType != 1 && actionType != 3) {
							resp.setReturnCode(ResponseCodes.MISSING_PARAMETER_CODE);
							resp.setReturnMsg(ResponseCodes.MISSING_PARAMETER_DES);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception ex) {
						resp.setReturnCode(ResponseCodes.MISSING_PARAMETER_CODE);
						resp.setReturnMsg(ResponseCodes.MISSING_PARAMETER_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
					try {
						HLRBalanceServices service = new HLRBalanceServices();
						if (request.getGroupType().equalsIgnoreCase("PaybySubs")) {
							USSDGWChngSORspMsg respone = ussdChangeSuplimentryOfferings(actionType, request.getmsisdn(),
									request.getOfferingId());
							if (respone.getRspHeader().getReturnCode().equals("0000")) {
								resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
								resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							} else {
								resp.setReturnCode(respone.getRspHeader().getReturnCode());
								resp.setReturnMsg(respone.getRspHeader().getReturnMsg());
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							}
						} else if(request.getGroupType().equalsIgnoreCase("PartPay")) {
							Balance responseFromBakcell = service.getBalance(request.getmsisdn(), "postpaid",null);
							Helper.logInfoMessageV2(request.getmsisdn() + " - Balance is" +  responseFromBakcell.getPostpaid().getAvailableBalanceIndividualValue());
							Helper.logInfoMessageV2(request.getmsisdn() + " - Balance in double is " +Double.parseDouble(responseFromBakcell.getPostpaid().getAvailableBalanceIndividualValue()));
							Helper.logInfoMessageV2(request.getmsisdn() + " - Balance is" +  responseFromBakcell.getPostpaid().getAvailableBalanceCorporateValue());
							Helper.logInfoMessageV2(request.getmsisdn() + " - Balance in double is " +Double.parseDouble(responseFromBakcell.getPostpaid().getAvailableBalanceCorporateValue()));
							Helper.logInfoMessageV2(request.getmsisdn() + " - Activation Price from request is: " + request.getActPrice());
							if (responseFromBakcell.getReturnCode().equals("0")) {
								if (Double.parseDouble(responseFromBakcell.getPostpaid()
										.getAvailableBalanceIndividualValue()) + Double.parseDouble(responseFromBakcell.getPostpaid()
										.getAvailableBalanceCorporateValue()) >= Double
												.parseDouble(request.getActPrice())) {
									USSDGWChngSORspMsg respone = ussdChangeSuplimentryOfferings(actionType,
											request.getmsisdn(), request.getOfferingId());
									if (respone.getRspHeader().getReturnCode().equals("0000")) {
										resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
										resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
										logs.setResponseCode(resp.getReturnCode());
										logs.setResponseDescription(resp.getReturnMsg());
										logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
										logs.updateLog(logs);
										return resp;
									} else {
										resp.setReturnCode(respone.getRspHeader().getReturnCode());
										resp.setReturnMsg(respone.getRspHeader().getReturnMsg());
										logs.setResponseCode(resp.getReturnCode());
										logs.setResponseDescription(resp.getReturnMsg());
										logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
										logs.updateLog(logs);
										return resp;
									}
								} else {
									resp.setReturnCode(ResponseCodes.UNSUCCESS_CODE);
									resp.setReturnMsg(ResponseCodes.UNSUCCESS_DESC_CS);
									logs.setResponseCode(resp.getReturnCode());
									logs.setResponseDescription(resp.getReturnMsg());
									logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
									logs.updateLog(logs);
									return resp;
								}

							}
						}
						else {
							Balance responseFromBakcell = service.getBalance(request.getmsisdn(), "postpaid",null);
							Helper.logInfoMessageV2(request.getmsisdn() + " - Balance is" +  responseFromBakcell.getPostpaid().getAvailableBalanceIndividualValue());
							Helper.logInfoMessageV2(request.getmsisdn() + " - Balance in double is " +Double.parseDouble(responseFromBakcell.getPostpaid().getAvailableBalanceIndividualValue()));
							Helper.logInfoMessageV2(request.getmsisdn() + " - Activation Price from request is: " + request.getActPrice());
							if (responseFromBakcell.getReturnCode().equals("0")) {
								if (Double.parseDouble(responseFromBakcell.getPostpaid()
										.getAvailableBalanceIndividualValue()) + Double.parseDouble(responseFromBakcell.getPostpaid()
										.getAvailableBalanceCorporateValue()) >= Double
												.parseDouble(request.getActPrice())) {
									USSDGWChngSORspMsg respone = ussdChangeSuplimentryOfferings(actionType,request.getmsisdn(), request.getOfferingId());
									if (respone.getRspHeader().getReturnCode().equals("0000")) {
										resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
										resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
										logs.setResponseCode(resp.getReturnCode());
										logs.setResponseDescription(resp.getReturnMsg());
										logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
										logs.updateLog(logs);
										return resp;
									} else {
										resp.setReturnCode(respone.getRspHeader().getReturnCode());
										resp.setReturnMsg(respone.getRspHeader().getReturnMsg());
										logs.setResponseCode(resp.getReturnCode());
										logs.setResponseDescription(resp.getReturnMsg());
										logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
										logs.updateLog(logs);
										return resp;
									}
								} else {
									resp.setReturnCode(ResponseCodes.UNSUCCESS_CODE);
									resp.setReturnMsg(ResponseCodes.UNSUCCESS_DESC_CS);
									logs.setResponseCode(resp.getReturnCode());
									logs.setResponseDescription(resp.getReturnMsg());
									logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
									logs.updateLog(logs);
									return resp;
								}

							}
						}
					} catch (Exception e) {
						logger.error(Helper.GetException(e));
						resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
						resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}

				} else {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}

		return resp;
	}

	public USSDGWChngSORspMsg ussdChangeSuplimentryOfferings(int action, String msisdn, String offeringUnqiqueId) {
		USSDGWChngSOReqMsg req = new USSDGWChngSOReqMsg();
		req.setReqHeader(getRequestHeader());
		req.setServiceNumber(msisdn);
		req.setIsConfirmed(Constants.USSD_IS_CONFIRMED);
		USSDGWChngSOReqMsg.SupplementaryOfferingList list = new USSDGWChngSOReqMsg.SupplementaryOfferingList();
		list.setActionType(Integer.toString(action));
		USSDGWChngSOReqMsg.SupplementaryOfferingList.OfferingId offeringId = new USSDGWChngSOReqMsg.SupplementaryOfferingList.OfferingId();
		offeringId.setOfferingId(offeringUnqiqueId);
		list.setOfferingId(offeringId);
		req.getSupplementaryOfferingList().add(list);
		USSDGWChngSORspMsg respone = USSDService.getInstance().ussdGatewayChangeSupplementaryOffering(req);
		return respone;
	}

	public ReqHeader getRequestHeader() {
		ReqHeader reqh = new ReqHeader();
		try {
			reqh.setAccessUser(ConfigurationManager.getConfigurationFromCache("ussd.reqh.AccessUser").trim());
			reqh.setChannelId(ConfigurationManager.getConfigurationFromCache("ussd.reqh.ChannelId").trim());
			reqh.setAccessPwd(ConfigurationManager.getConfigurationFromCache("ussd.reqh.AccessPwd").trim());
			reqh.setTransactionId(Helper.generateTransactionID());
			return reqh;
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		}
		return reqh;
	}
	public static void main(String[] args) 
	{
		String request="{\"msisdn\":\"555956005\",\"iP\":\"127.0.0.1\",\"channel\":\"web\",\"lang\":\"3\",\"offeringId\":\"1151806699\",\"actionType\":\"1\"}";
		new ChangeSupplimentryOfferingRequestLand().Get("RQ/Rjcf8Gn2+7srAiXNO0ZGuRnJORSir", "application/json", request);
		
	}
}
