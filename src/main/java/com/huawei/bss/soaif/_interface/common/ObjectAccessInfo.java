package com.huawei.bss.soaif._interface.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for ObjectAccessInfo complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="ObjectAccessInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ObjectIdType" type="{http://www.huawei.com/bss/soaif/interface/common/}ObjectIdType"/>
 *         &lt;element name="ObjectId" type="{http://www.huawei.com/bss/soaif/interface/common/}ObjectId"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjectAccessInfo", propOrder = { "objectIdType", "objectId" })
@XmlSeeAlso({ com.huawei.bss.soaif._interface.subscriberservice.ChangeSubMSISDNReqMsg.AccessInfo.class,
		com.huawei.bss.soaif._interface.subscriberservice.DeactivateSubReqMsg.AccessInfo.class,
		com.huawei.bss.soaif._interface.subscriberservice.CancelLostReqMsg.AccessInfo.class,
		com.huawei.bss.soaif._interface.subscriberservice.ChangeSubOwnerReqMsg.AccessInfo.class,
		com.huawei.bss.soaif._interface.subscriberservice.ChangeRechargeBlacklistReqMsg.AccessInfo.class,
		com.huawei.bss.soaif._interface.subscriberservice.ChangeSubSIMReqMsg.AccessInfo.class,
		com.huawei.bss.soaif._interface.subscriberservice.ReportLostReqMsg.AccessInfo.class,
		com.huawei.bss.soaif._interface.subscriberservice.AssignAcctToSubReqMsg.AccessInfo.class,
		com.huawei.bss.soaif._interface.subscriberservice.QueryEntityIdsRspMsg.EntityId.class,
		com.huawei.bss.soaif._interface.subscriberservice.PreDeactivateSubReqMsg.AccessInfo.class,
		com.huawei.bss.soaif._interface.subscriberservice.SupplementCustInfoReqMsg.AccessInfo.class,
		com.huawei.bss.soaif._interface.subscriberservice.ChangePrepaidToPostpaidReqMsg.AccessInfo.class })
public class ObjectAccessInfo {
	@XmlElement(name = "ObjectIdType", required = true)
	protected String objectIdType;
	@XmlElement(name = "ObjectId", required = true)
	protected String objectId;

	/**
	 * Gets the value of the objectIdType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getObjectIdType() {
		return objectIdType;
	}

	/**
	 * Sets the value of the objectIdType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setObjectIdType(String value) {
		this.objectIdType = value;
	}

	/**
	 * Gets the value of the objectId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getObjectId() {
		return objectId;
	}

	/**
	 * Sets the value of the objectId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setObjectId(String value) {
		this.objectId = value;
	}
}
