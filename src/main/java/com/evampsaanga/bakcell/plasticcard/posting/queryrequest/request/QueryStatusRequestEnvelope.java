package com.evampsaanga.bakcell.plasticcard.posting.queryrequest.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType; 
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "envelope")
@XmlAccessorType(XmlAccessType.FIELD)
public class QueryStatusRequestEnvelope {

	public int reqtype;
	public String username;
	public String password;
	public QueryStatusRequestBody body;

	public int getReqtype() {
		return reqtype;
	}

	public void setReqtype(int reqtype) {
		this.reqtype = reqtype;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public QueryStatusRequestBody getBody() {
		return body;
	}

	public void setBody(QueryStatusRequestBody body) {
		this.body = body;
	}

	@Override
	public String toString() {
		return "QueryStatusRequestEnvelope [reqtype=" + reqtype + ", username=" + username + ", password=" + password + ", body=" + body
				+ "]";
	}

}
