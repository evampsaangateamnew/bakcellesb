package com.evampsaanga.msisdninvoice;



import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.codehaus.stax2.validation.Validatable;
import org.springframework.jca.cci.connection.CciLocalTransactionManager;

import com.cloudcontrolled.api.response.Response;

import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.amqimplementationsesb.LogsType;
import com.evampsaanga.bakcell.db.DBBakcellFactory;
import com.evampsaanga.bakcell.getUsersV2.UsersGroupData;
import com.evampsaanga.bakcell.getappfaq.GetAppFaqRequest;
import com.evampsaanga.bakcell.getappfaq.GetAppFaqResponse;
import com.evampsaanga.bakcell.getappfaq.MagentoResponse;
import com.evampsaanga.bakcell.getappfaq.MagentoResponseV2;
import com.evampsaanga.cache.UserGroupDataCache;
import com.evampsaanga.companyinvoice.FetchInvoiceAndBillMonth;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.ThirdPartyNames;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.developer.utils.SOAPLoggingHandler;
import com.evampsaanga.services.QueryOrderService;
import com.huawei.crm.query.order.query.GetGroupDataIn;
import com.huawei.crm.query.order.query.GetGroupRequest;
import com.saanga.magento.apiclient.RestClient;


@Path("/bakcell")
public class MSISDNInvoice {
	

	public static final Logger logger = Logger.getLogger("bakcellLogs-V2");
	public static final Logger loggerV2 = Logger.getLogger("bakcellLogs-V2");
    public static String modeulName="COMPANY_INVOICE";
	@POST
	@Path("/getsummary")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public MSISDNInvoiceResponse getSummary(@Body String requestBody, @Header("credentials") String credential) {
		
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.COMPANY_INVOICE);
		logs.setThirdPartyName(ThirdPartyNames.ODS);
		logs.setTableType(LogsType.CompanyInvoice);
		MSISDNInvoiceRequest cclient = null;
		MSISDNInvoiceResponse resp = new MSISDNInvoiceResponse();
		try {
			cclient = Helper.JsonToObject(requestBody, MSISDNInvoiceRequest.class);
			logMessage("REQUEST: "+Helper.ObjectToJson(cclient));
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
				logs.setIsB2B(cclient.getIsB2B());
			}
		} catch (Exception ex) {
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_401);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}

			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try 
				{
					//Below both funtion fetch account ID if msisdn is PayBySubs
					
					//////////////////////////////////HardCored value for testing //////////////////////
					
//					cclient.setBillMonth("-4");
					///////////////////////////////////////////////////////////////////////////
					FetchInvoiceAndBillMonth fetchInvoiceAndBillMonth = new FetchInvoiceAndBillMonth();
					fetchInvoiceAndBillMonth.setInvoceidAndBillMonth(cclient.getAcctCode(), cclient.getBillMonth(), cclient.getLang(),cclient.getmsisdn(),cclient.getSelectedMsisdn(),cclient.getCustomerId());
					if(fetchInvoiceAndBillMonth.getBillMonth()!=null && fetchInvoiceAndBillMonth.getInvoiceId()!=null)
					{
//						String totalValue = getTotalValueFromODS(cclient.getBillMonth(),fetchInvoiceAndBillMonth.getInvoiceId(),cclient.getSelectedMsisdn());
//						if(totalValue!=null)
							resp  = getSummaryFromODS(cclient.getAcctCode(),cclient.getBillMonth(),fetchInvoiceAndBillMonth.getInvoiceId(),"",cclient.getSelectedMsisdn());
//						else
//						{
//							resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
//							resp.setReturnMsg("Unable to fetch Total amount");
//						}
					}
					else
					{
						resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
						resp.setReturnMsg("Invoice ID is missing");
						
					}
					logMessage("RESPONSE: "+Helper.ObjectToJson(resp));
					
//					MSISDNSummary msisdnSummary = new MSISDNSummary();
//					msisdnSummary.setLimit("23");
//					msisdnSummary.setMsisdn("555956001");
//					msisdnSummary.setTotal("455");
//					resp.setTotalValue("5000");
//					resp.setMsisdnSummary(msisdnSummary);
//					resp.setReturnCode("200");
//					resp.setReturnMsg("success");
//					resp = Helper.JsonToObject(response, MSISDNInvoiceResponse.class);
					
				
				} 
				catch (Exception ex) 
				{
					loggerV2.info(Helper.GetException(ex));
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
//		logs.updateLog(logs);
		
		return resp;
	}

	private String getAccountIDFromCache(String msisdn,String selectedMsisdn,String customerId,String accountID) throws IOException, Exception 
	{
		// TODO Auto-generated method stub
		String result=null;
		ArrayList<UsersGroupData> usersGroupData = new ArrayList<UsersGroupData>();
		JSONObject getGroupRequest = new JSONObject();
		getGroupRequest.put("lang", "3");
		getGroupRequest.put("iP", "9.9.9.9");
		getGroupRequest.put("channel", "web");
		getGroupRequest.put("msisdn", msisdn);
		getGroupRequest.put("isB2B", "true");
		getGroupRequest.put("customerID", customerId);
		UserGroupDataCache userCache = new UserGroupDataCache();
		usersGroupData = (ArrayList<UsersGroupData>) userCache.checkUsersInCache(getGroupRequest.toString());
		
		
		String customerCrmAccountId=null;
		for(int k=0;k<usersGroupData.size();k++)
		{
			logMessage("Group Name : "+usersGroupData.get(k).getGroup_name());
			logMessage("MSISDN : "+usersGroupData.get(k).getMsisdn());
			if(usersGroupData.get(k).getMsisdn().equals(selectedMsisdn) && (usersGroupData.get(k).getGroup_name().equals("PaybySubs")))
			{
				String value=null;
				String query = "select CRM_ACCT_ID from V_E_CARE_BILL_CHARGE_OBJ t where PRI_IDENTITY='"+selectedMsisdn+"'";
				try {
					logMessage("Query: " + query);
					ResultSet rs = DBBakcellFactory.getConnection().prepareStatement(query).executeQuery();
					while (rs.next()) {
						value = rs.getString("CRM_ACCT_ID");
					}
				}
				catch (SQLException e) {
					logger.info("ERROR: "+ Helper.GetException(e));
					
					return null;
				}
				
					return value;
				
//				customerCrmAccountId = usersGroupData.get(k).getCorp_crm_acct_id();
			}
			else
				result = accountID;
		}
		return result;
		
	}

	private String getGroupFromGroupInfoAPI(String selectedMsisdn, String accountId) {
		// TODO Auto-generated method stub
		String result=null;
		GetGroupRequest getGroupDataRequestMsgReq = new GetGroupRequest();
		getGroupDataRequestMsgReq.setRequestHeader(QueryOrderService.getQueryOderHeader());
		GetGroupDataIn getGroupDataIn = new GetGroupDataIn();
		getGroupDataIn.setServiceNumber(selectedMsisdn);
		getGroupDataRequestMsgReq.setGetGroupBody(getGroupDataIn);
		com.huawei.crm.query.order.query.GetGroupResponse getGroupResponse = QueryOrderService.getInstance().getGroupData(getGroupDataRequestMsgReq);
		if(getGroupResponse != null && getGroupResponse.getResponseHeader().getRetCode().equalsIgnoreCase("0"))
		{
			if(getGroupResponse.getGetGroupBody() != null && getGroupResponse.getGetGroupBody().getGetGroupDataList() != null)
			{
				for(int k=0;k<getGroupResponse.getGetGroupBody().getGetGroupDataList().size();k++)
					if(!(getGroupResponse.getGetGroupBody().getGetGroupDataList().get(k).getGroupName().contains("Fullpay") || getGroupResponse.getGetGroupBody().getGetGroupDataList().get(k).getGroupName().contains("Partpay")))
					{
						logMessage("Group Name : "+getGroupResponse.getGetGroupBody().getGetGroupDataList().get(k).getGroupName());
						result =  String.valueOf(getGroupResponse.getGetGroupBody().getGetGroupDataList().get(k).getAccountId());
					}
					else
						result =accountId;
			}
			else
				result =accountId;
		}
		else
			result =accountId;
		return result;
		
	}

	private String getTotalValueFromODS(String billMonth, String invoiceId, String selectedMsisdn) throws ParseException {
		// TODO Auto-generated method stub
		String value=null;
		String query = "select sum(TOTAL) TOTAL from V_E_CARE_BILL_CHARGE_OBJ t where INVOICE_ID = '"+invoiceId+"' and BILL_MONTH = to_date('"+Helper.getDateOfLastMonth(billMonth)+"','yyyymmdd') and PRI_IDENTITY='"+selectedMsisdn+"'";
		try {
			logMessage("Query: " + query);
			ResultSet rs = DBBakcellFactory.getConnection().prepareStatement(query).executeQuery();
			ArrayList<MSISDNSummary> msisdnSummaries = new ArrayList<MSISDNSummary>();
			while (rs.next()) {
				value = rs.getString("TOTAL");
			}
		}
		catch (SQLException e) {
			logger.info("ERROR: "+ Helper.GetException(e));
			
			return null;
		}
		
			return value;
			
	}

	private MSISDNInvoiceResponse getSummaryFromODS(String acctCode, String billMonth, String invoiceId, String totalValue,String selectedMsisdn) throws ParseException {
		MSISDNInvoiceResponse companyInvoiceResponse = new MSISDNInvoiceResponse();
		
		String query = "select PRI_IDENTITY ,PRORATE_LIMIT ,sum(TOTAL) TOTAL from V_E_CARE_BILL_CHARGE_OBJ t where INVOICE_ID = '"+invoiceId+"' and BILL_MONTH = to_date('"+Helper.getDateOfLastMonth(billMonth)+"','yyyymmdd') and PRI_IDENTITY='"+selectedMsisdn+"' group by PRI_IDENTITY ,PRORATE_LIMIT";
		try {
			logMessage("Query: " + query);
			ResultSet rs = DBBakcellFactory.getConnection().prepareStatement(query).executeQuery();
			ArrayList<MSISDNSummary> msisdnSummaries = new ArrayList<MSISDNSummary>();
			while (rs.next()) {
				MSISDNSummary msisdnSummary = new MSISDNSummary();
				msisdnSummary.setMsisdn((rs.getString("PRI_IDENTITY")));
				msisdnSummary.setLimit(String.valueOf(Helper.roundUptoDecimal(Double.parseDouble(rs.getString("PRORATE_LIMIT")), 2)));
				msisdnSummary.setTotal(String.valueOf(Helper.roundUptoDecimal(Double.parseDouble(rs.getString("TOTAL")), 2)));
				msisdnSummaries.add(msisdnSummary);
			}
			companyInvoiceResponse.setTotalValue(totalValue);
			companyInvoiceResponse.setMsisdnSummary(msisdnSummaries);
		} catch (SQLException e) {
			logger.info("ERROR: ", e);
			
			companyInvoiceResponse.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
			companyInvoiceResponse.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
		}
		logMessage("");
		companyInvoiceResponse.setReturnCode(ResponseCodes.SUCESS_CODE_200);
		companyInvoiceResponse.setReturnMsg(ResponseCodes.SUCESS_DES_200);
		return companyInvoiceResponse;
	}


	@POST
	@Path("/getdetails")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public MSISDNInvoiceResponse getDetails(@Body String requestBody, @Header("credentials") String credential) {
		logMessage("***************************************************************");
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.COMPANY_INVOICE);
		logs.setThirdPartyName(ThirdPartyNames.ODS);
		logs.setTableType(LogsType.CompanyInvoice);
		MSISDNInvoiceRequest cclient = null;
		MSISDNInvoiceResponse resp = new MSISDNInvoiceResponse();
		try {
			cclient = Helper.JsonToObject(requestBody, MSISDNInvoiceRequest.class);
			logMessage("REQUEST: "+Helper.ObjectToJson(cclient));
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
				logs.setIsB2B(cclient.getIsB2B());
			}
		} catch (Exception ex) {
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_401);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials == null) 
			{
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}

			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) 
			{
				try {
					
					if(cclient.getAcctCode()!=null && !cclient.getAcctCode().isEmpty())
					{
						//////////////////////////////////HardCored value for testing //////////////////////
						
//						cclient.setBillMonth("-4");
						///////////////////////////////////////////////////////////////////////////
						FetchInvoiceAndBillMonth fetchInvoiceAndBillMonth = new FetchInvoiceAndBillMonth();
						fetchInvoiceAndBillMonth.setInvoceidAndBillMonth(cclient.getAcctCode(), cclient.getBillMonth(), cclient.getLang(),cclient.getmsisdn(),cclient.getSelectedMsisdn(),cclient.getCustomerId());
						if(fetchInvoiceAndBillMonth.getBillMonth()!=null && fetchInvoiceAndBillMonth.getInvoiceId()!=null)	
							resp = getDetailsFromODS(fetchInvoiceAndBillMonth.getInvoiceId(),cclient.getBillMonth(),cclient.getLang(),cclient.getSelectedMsisdn());
						else
						{
							resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
							resp.setReturnMsg("INVOICE_ID is missing");
						}
							
					}
					else
					{
						resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
						resp.setReturnMsg("Bill Month is missing");
					}
//					resp.setReturnCode("200");
//					resp.setReturnMsg("success");
//					MSISDNDetail msisdnDetail = new MSISDNDetail();
//					ArrayList<BillSubItemCode> billSubItemCodes = new ArrayList<BillSubItemCode>();
//					BillSubItemCode billSubItemCode = new BillSubItemCode();
//					Details details = new Details();
//					details.setDetailDesc("15");
//					details.setDetailInvoice("23");
//					details.setDetailTotal("43");
//					details.setDetailUnit("MB");
//					details.setDetailUsage("34");
//					details.setSubitemTitle("Internet");
//					
//					
//					
//					billSubItemCode.setDetails(details);
//					
//					billSubItemCodes.add(billSubItemCode);
//					msisdnDetail.setBillSubItemCodes(billSubItemCodes);
//					resp.setMsisdnDetail(msisdnDetail);
					logMessage("RESPONSE: "+Helper.ObjectToJson(resp));
					
				
				} catch (Exception ex) {
					loggerV2.info(Helper.GetException(ex));
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
//		logs.updateLog(logs);
		
		logMessage("***************************************************************");
		return resp;
	}
	private MSISDNInvoiceResponse getDetailsFromODS(String invoiceId, String billMonth,String lang,String msisdn) throws ParseException {

		MSISDNInvoiceResponse companyInvoiceResponse = new MSISDNInvoiceResponse();
		MSISDNDetail companyDetail = new MSISDNDetail();
		
		ArrayList<BillSubItemCode> billSubItemCodes = new ArrayList<BillSubItemCode>();
		String query = "select  * from V_E_CARE_BILL_CHARGE_OBJ t where 1=1 and INVOICE_ID = '"+invoiceId+"' and BILL_MONTH = to_date('"+Helper.getDateOfLastMonth(billMonth)+"','yyyymmdd') and PRI_IDENTITY = '"+msisdn+"'";
//		String query = "select  * from V_E_CARE_BILL_CHARGE_OBJ t where 1=1 and INVOICE_ID = '"+invoiceId+"' and BILL_MONTH = to_date('"+Helper.getDateOfLastMonth(billMonth)+"','yyyymmdd') and PRI_IDENTITY = '552265064'";
		try {
			logMessage("Query: " + query);
			ResultSet rs = DBBakcellFactory.getConnection().prepareStatement(query).executeQuery();
			while (rs.next()) 
			{
				BillSubItemCode billSubItemCode = new BillSubItemCode();
				Details details = new Details();
				logMessage("INVOICE "+rs.getFloat("INVOICE"));
				logMessage("TAX "+rs.getFloat("TAX"));
//				details.setDetailInvoice(String.valueOf(Helper.roundUptoDecimal(Double.parseDouble(rs.getString("INVOICE")), 2)));
				details.setDetailInvoice(String.valueOf(Helper.roundUptoDecimal((rs.getFloat("INVOICE")+rs.getFloat("TAX")),2)));
				logMessage(details.getDetailInvoice());
				details.setDetailDesc(String.valueOf(Helper.roundUptoDecimal(Double.parseDouble(rs.getString("DISC")), 2)));
				logMessage(details.getDetailDesc());
				details.setDetailTotal(String.valueOf(Helper.roundUptoDecimal(Double.parseDouble(rs.getString("TOTAL")), 2)));
				logMessage(details.getDetailTotal());
				details.setDetailUsage(rs.getString("USAGE"));
				logMessage(rs.getString("USAGE"));
				details.setSubitemTitle(rs.getString("BILL_SUBITEM_CODE"),lang);
				logMessage(rs.getString("USAGE"));
				details.setDetailUnit(rs.getString("BILL_SUBITEM_CODE"),lang);
				logMessage(rs.getString("USAGE"));
				billSubItemCode.setDetails(details);
				billSubItemCodes.add(billSubItemCode);
				
				
			}
			companyDetail.setBillSubItemCodes(billSubItemCodes);
			companyInvoiceResponse.setMsisdnDetail(companyDetail);
		} catch (SQLException e) {
			logger.info("ERROR: ", e);
			companyInvoiceResponse.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
			companyInvoiceResponse.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
		}
		
		
		companyInvoiceResponse.setReturnCode(ResponseCodes.SUCESS_CODE_200);
		companyInvoiceResponse.setReturnMsg(ResponseCodes.SUCESS_DES_200);
		return companyInvoiceResponse;
	
	}

	private void logMessage(String message)
	{
		logger.info(modeulName+": "+message);
	}
	public static void main(String[] args) {
		Double value = 148.690322;
		System.out.println(Helper.roundUptoDecimal(value, 2));
		
	}


}
