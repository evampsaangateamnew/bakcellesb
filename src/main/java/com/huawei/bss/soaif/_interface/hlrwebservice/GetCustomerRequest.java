package com.huawei.bss.soaif._interface.hlrwebservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bss.soaif._interface.common.ReqHeader;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.huawei.com/bss/soaif/interface/common/}ReqHeader"/>
 *         &lt;element name="getCustomerBody" type="{http://www.huawei.com/bss/soaif/interface/HLRWEBService/}getCustomerIn"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "reqHeader", "getCustomerBody" })
@XmlRootElement(name = "getCustomerRequest")
public class GetCustomerRequest {
	@XmlElement(name = "ReqHeader", namespace = "http://www.huawei.com/bss/soaif/interface/common/", required = true)
	protected ReqHeader reqHeader;
	@XmlElement(required = true)
	protected GetCustomerIn getCustomerBody;

	/**
	 * Gets the value of the reqHeader property.
	 * 
	 * @return possible object is {@link ReqHeader }
	 * 
	 */
	public ReqHeader getReqHeader() {
		return reqHeader;
	}

	/**
	 * Sets the value of the reqHeader property.
	 * 
	 * @param value
	 *            allowed object is {@link ReqHeader }
	 * 
	 */
	public void setReqHeader(ReqHeader value) {
		this.reqHeader = value;
	}

	/**
	 * Gets the value of the getCustomerBody property.
	 * 
	 * @return possible object is {@link GetCustomerIn }
	 * 
	 */
	public GetCustomerIn getGetCustomerBody() {
		return getCustomerBody;
	}

	/**
	 * Sets the value of the getCustomerBody property.
	 * 
	 * @param value
	 *            allowed object is {@link GetCustomerIn }
	 * 
	 */
	public void setGetCustomerBody(GetCustomerIn value) {
		this.getCustomerBody = value;
	}
}
