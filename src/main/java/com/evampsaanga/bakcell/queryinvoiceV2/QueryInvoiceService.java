package com.evampsaanga.bakcell.queryinvoiceV2;

import java.io.IOException;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.amqimplementationsesb.LogsType;
import com.evampsaanga.authorization.AuthorizationAndAuthentication;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.ThirdPartyNames;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.services.CBSARService;
import com.huawei.bme.cbsinterface.arservices.QueryInvoiceRequest;
import com.huawei.bme.cbsinterface.arservices.QueryInvoiceRequestMsg;
import com.huawei.bme.cbsinterface.arservices.QueryInvoiceResult.InvoiceInfo;
import com.huawei.bme.cbsinterface.arservices.QueryInvoiceResultMsg;
import com.huawei.bme.cbsinterface.arservices.QueryInvoiceRequest.TimePeriod;

/**
 * 
 * @author Aqeel Abbas
 * Queries the individual invoices under the pic
 *
 */
@Path("/bakcell")
public class QueryInvoiceService {
	public static final Logger logger = Logger.getLogger("bakcellLogs-V2");
/**
 * 
 * @param requestBody
 * @param credential
 * @return invoice Response
 * @throws IOException
 * @throws Exception
 */
	@POST
	@Path("/queryinvoice")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public QueryInvoiceResponse queryInvoice(@Body String requestBody, @Header("credentials") String credential)
			throws IOException, Exception {
		// Logging incoming request to log file

		// Below is declaration block for variables to be used
		// Logs object to store values which are to be inserted in database for
		// reporting
		QueryInvoiceRequestData queryInvoiceRequest = new QueryInvoiceRequestData();
		QueryInvoiceResponse queryInvoiceResponse = new QueryInvoiceResponse();
		List<QueryInvoiceResponseData> responseDataList = new ArrayList<>();
		QueryInvoiceResponseData responseData = new QueryInvoiceResponseData();
		Logs logs = new Logs();
		// Generic information about logs is being set to logs object
		logs.setRequestDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.setTransactionName(Transactions.INVOICE);
		logs.setThirdPartyName(ThirdPartyNames.LOGIN);
		logs.setTableType(LogsType.CustomerData);
		// Authenticating the request using credentials string received in
		// header
		boolean authenticationresult = AuthorizationAndAuthentication.authenticateAndAuthorizeCredentials(credential);

		// if request is not authenticated adding unauthorized response
		// codes to response and logs object
		if (!authenticationresult)
			prepareErrorResponse(queryInvoiceResponse, logs, ResponseCodes.ERROR_401_CODE, ResponseCodes.ERROR_401);

		try {
			queryInvoiceRequest = Helper.JsonToObject(requestBody, QueryInvoiceRequestData.class);
			Helper.logInfoMessageV2(queryInvoiceRequest.getmsisdn()
					+ " - Request lanaded on queryinvoice with data as :" + requestBody);
			logs.setIsB2B(queryInvoiceRequest.getIsB2B());
			// logger.info("@@<<<<<<<<<<<<<<<<<<<<< query invoice start
			// >>>>>>>>>>>>>>>>>>>>>@@");
		} catch (Exception ex) {
			// block catches the exception from mapper and sets the 400 bad
			// request response code
			logger.error(Helper.GetException(ex));
			prepareErrorResponse(queryInvoiceResponse, logs, ResponseCodes.ERROR_400_CODE, ResponseCodes.ERROR_400);
		}
		if (queryInvoiceRequest != null && authenticationresult) {
			QueryInvoiceRequestMsg queryInvoiceRequestMsg = new QueryInvoiceRequestMsg();
			QueryInvoiceRequest invoicerequest = new QueryInvoiceRequest();
			com.huawei.bme.cbsinterface.arservices.QueryInvoiceRequest.AcctAccessCode actAccessCode = new com.huawei.bme.cbsinterface.arservices.QueryInvoiceRequest.AcctAccessCode();
			actAccessCode.setAccountCode(queryInvoiceRequest.getCustomerID());
			invoicerequest.setAcctAccessCode(actAccessCode);
			TimePeriod TimePeriod = new TimePeriod();
			TimePeriod.setEndTime(queryInvoiceRequest.getEndTime());
			TimePeriod.setStartTime(queryInvoiceRequest.getStartTime());
			invoicerequest.setTimePeriod(TimePeriod);
			queryInvoiceRequestMsg.setQueryInvoiceRequest(invoicerequest);
			queryInvoiceRequestMsg.setRequestHeader(CBSARService.getRequestPaymentRequestHeader());
			QueryInvoiceResultMsg resultmsg = CBSARService.getInstance().queryInvoice(queryInvoiceRequestMsg);
			Helper.logInfoMessageV2(
					queryInvoiceRequest.getmsisdn() + "- Response from CBSAR: " + Helper.ObjectToJson(resultmsg));
			if (resultmsg.getQueryInvoiceResult() != null) {
				for (InvoiceInfo invInfo : resultmsg.getQueryInvoiceResult().getInvoiceInfo()) {
					String dueDate = invInfo.getDueDate();
					DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
					Date date = (Date) formatter.parse(dueDate);
					SimpleDateFormat newFormat = new SimpleDateFormat("dd/MM/yyyy");
					String dueDateNDisp = newFormat.format(date);

					responseData.setDueDate(dueDateNDisp);
					String invoiceDate = invInfo.getInvoiceDate();
					date = (Date) formatter.parse(invoiceDate);
					String invoiceDateNDisp = newFormat.format(date);
					NumberFormat df = new DecimalFormat("#0.00");
					df.setRoundingMode(RoundingMode.FLOOR);
					Helper.logInfoMessageV2(queryInvoiceRequest.getmsisdn() + "- Invoice amount without formating " + invInfo.getInvoiceAmount());
					Long invAmount = new Long( invInfo.getInvoiceAmount());
					responseData.setInvoiceAmount(df.format((invAmount.doubleValue() /Constants.MONEY_DIVIDEND)));

					responseData.setInvoiceDate(invoiceDateNDisp);
					if (invInfo.getStatus().equals("C")) {
						Helper.logInfoMessageV2(queryInvoiceRequest.getmsisdn() + " - Invoice Status: " + invInfo.getStatus() );
						date = (Date) formatter.parse(invInfo.getSettleDate());
						responseData.setSettleDate(newFormat.format(date));
						newFormat = new SimpleDateFormat("dd/MM/yyyy");
						responseData.setSettleDateDisp(newFormat.format(date));
					} else
						invInfo.setSettleDate(null);
					responseData.setStatus(invInfo.getStatus());

					newFormat = new SimpleDateFormat("dd/MM/yyyy");
					date = (Date) formatter.parse(dueDate);
					responseData.setDueDateDisp(newFormat.format(date));

					date = (Date) formatter.parse(invoiceDate);
					responseData.setInvoiceDateDisp(newFormat.format(date));

					responseDataList.add(responseData);
				}
				queryInvoiceResponse.setQueryInvoiceResponseData(responseDataList);
				queryInvoiceResponse.setReturnCode("200");
				queryInvoiceResponse.setReturnMsg("Sucessfull");
				logs.setResponseCode(queryInvoiceResponse.getReturnCode());
				logs.setResponseDescription(queryInvoiceResponse.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
			} else {
				prepareErrorResponse(queryInvoiceResponse, logs, ResponseCodes.ERROR_401_CODE, ResponseCodes.ERROR_401);
			}
		}
		return queryInvoiceResponse;
	}

	/**
	 * 
	 * @param queryInvoiceResponse
	 * @param logs
	 * @param returnCode
	 * @param returnMessage
	 */
	public void prepareErrorResponse(QueryInvoiceResponse queryInvoiceResponse, Logs logs, String returnCode,
			String returnMessage) {
		queryInvoiceResponse.setReturnCode(returnCode);
		queryInvoiceResponse.setReturnMsg(returnMessage);
		logs.setResponseCode(queryInvoiceResponse.getReturnCode());
		logs.setResponseDescription(queryInvoiceResponse.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);

	}
	public static void main(String[] args) {
		NumberFormat df = new DecimalFormat("#0.##");
		df.setRoundingMode(RoundingMode.FLOOR);
		Long b = new Long(24561540442L);
		String a = df.format(b.doubleValue() /Constants.MONEY_DIVIDEND);
		System.out.print(a);
	}

}
