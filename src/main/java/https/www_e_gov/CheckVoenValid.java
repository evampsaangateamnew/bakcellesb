
package https.www_e_gov;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import net.x_rd.az.voen9900037691.producer.CheckVoenValidRequest;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CheckVoenValidRequest" type="{http://voen9900037691.az.x-rd.net/producer}CheckVoenValidRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "checkVoenValidRequest"
})
@XmlRootElement(name = "CheckVoenValid")
public class CheckVoenValid {

    @XmlElement(name = "CheckVoenValidRequest")
    protected CheckVoenValidRequest checkVoenValidRequest;

    /**
     * Gets the value of the checkVoenValidRequest property.
     * 
     * @return
     *     possible object is
     *     {@link CheckVoenValidRequest }
     *     
     */
    public CheckVoenValidRequest getCheckVoenValidRequest() {
        return checkVoenValidRequest;
    }

    /**
     * Sets the value of the checkVoenValidRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link CheckVoenValidRequest }
     *     
     */
    public void setCheckVoenValidRequest(CheckVoenValidRequest value) {
        this.checkVoenValidRequest = value;
    }

}
