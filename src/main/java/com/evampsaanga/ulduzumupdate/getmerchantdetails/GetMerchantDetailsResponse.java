package com.evampsaanga.ulduzumupdate.getmerchantdetails;

public class GetMerchantDetailsResponse {
	String result;
	Merchant data;

	/**
	 * @return the result
	 */
	public String getResult() {
		return result;
	}

	/**
	 * @param result
	 *            the result to set
	 */
	public void setResult(String result) {
		this.result = result;
	}

	/**
	 * @return the data
	 */
	public Merchant getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(Merchant data) {
		this.data = data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "GetMerchantUlduzumResponse [result=" + result + ", data=" + data + "]";
	}

}
