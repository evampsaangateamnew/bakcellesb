package com.evampsaanga.bakcell.ussdgwquerybalance;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "USSDGWQueryBalanceRequestClient", propOrder = { "requestBody" })
@XmlRootElement(name = "USSDGWQueryBalanceRequestClient")
public class USSDGWQueryBalanceRequestClient extends com.evampsaanga.bakcell.requestheaders.BaseRequest {
	@XmlElement(name = "requestBody", required = true)
	private String requestBody = "";

	public USSDGWQueryBalanceRequestClient() {
	}

	public String getSubscriberBody() {
		return requestBody;
	}

	public void setSubscriberBody(String body) {
		this.requestBody = body;
	}
}
