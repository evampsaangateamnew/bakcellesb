package com.evampsaanga.refreshcache;

import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.amqimplementationsesb.LogsType;
import com.evampsaanga.amqimplementationsesb.QueueMessageListener;
import com.evampsaanga.bakcell.getcdrsbydate.GetCDRsByDateLand;
import com.evampsaanga.bakcell.getsubscriber.CRMSubscriberService;
import com.evampsaanga.cache.UserGroupDataCache;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.ThirdPartyNames;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;

@Path("/cacheManagement")
public class RefreshCacheRequestLand {

    public static final Logger logger = Logger.getLogger("bakcellLogs-V2");
    
    @POST
	@Path("/refreshconfigcache")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public RefreshCacheResponse RefreshConfigCache(@Header("credentials") String credential, @Body() String requestBody) {
    	logger.info("Refreh ESB Cache Request" + requestBody);
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.REFRESH_ESB_CACHE_NAME);
		logs.setThirdPartyName(ThirdPartyNames.INTERNAL_CACHE);
		logs.setTableType(LogsType.Login);
		RefreshCacheRequest cclient = null;
		RefreshCacheResponse resp = new RefreshCacheResponse();
		try {
			cclient = Helper.JsonToObject(requestBody, RefreshCacheRequest.class);
			logs.setIp(cclient.getiP());
			logs.setChannel(cclient.getChannel());
			logs.setMsisdn(cclient.getmsisdn());
			logs.setLang(cclient.getLang());
			logs.setIsB2B(cclient.getIsB2B());
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		
		{
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				if(cclient.getIsB2B().equalsIgnoreCase("B2CEsb"))
				{
					QueueMessageListener.hashmapTransactionMappings.clear();
					CRMSubscriberService.coreServicesCategoryCacheHashMap.clear();
					ConfigurationManager.propertiesMessageTemplates.clear();
					HashMap<String, String> propertiesCacheHashMap= ConfigurationManager.propertiesCacheHashMap;
					propertiesCacheHashMap.clear();
				
				}
				else if(cclient.getIsB2B().equalsIgnoreCase("B2BPic"))
				{
					CRMSubscriberService.coreServicesCategoryCacheHashMap.clear();	
					UserGroupDataCache.users.clear();
					UserGroupDataCache.usersCount.clear();
				}
				else if((cclient.getIsB2B().equalsIgnoreCase("B2CCdrs")))
				{
					
					GetCDRsByDateLand.usageHistoryTranslationMapping.clear();
					GetCDRsByDateLand.cdrsColumnMapping.clear();
				}
				resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
				resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.updateLog(logs);
				return resp;
				
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.updateLog(logs);
				return resp;
			}
		}
	}
    
}
