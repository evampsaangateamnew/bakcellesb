package com.evampsaanga.ulduzumupdate.getusagetotal;

import com.evampsaanga.bakcell.responseheaders.BaseResponse;

public class GetUsageTotalResponseClient extends BaseResponse {

	UsageTotal data;

	public UsageTotal getData() {
		return data;
	}

	public void setData(UsageTotal data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "GetMerchantsResponseClient [data=" + data + "]";
	}

}
