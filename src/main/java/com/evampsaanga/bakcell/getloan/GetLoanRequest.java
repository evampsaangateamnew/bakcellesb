package com.evampsaanga.bakcell.getloan;

import com.evampsaanga.bakcell.requestheaders.BaseRequest;

public class GetLoanRequest extends BaseRequest {
	private String loanAmount = "";
	private String brandId = "";
	private String subscriberType="";
	
	

	public String getSubscriberType() {
		return subscriberType;
	}

	public void setSubscriberType(String subscriberType) {
		this.subscriberType = subscriberType;
	}

	/**
	 * @return the loanAmount
	 */
	public String getLoanAmount() {
		return loanAmount;
	}

	/**
	 * @param loanAmount
	 *            the loanAmount to set
	 */
	public void setLoanAmount(String loanAmount) {
		this.loanAmount = loanAmount;
	}

	public String getBrandId() {
		return brandId;
	}

	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}
}