package com.evampsaanga.magento.getpredefineddata;

public class SelectAmount {
	private SelectAmountSub[] klass;
	private SelectAmountSub[] cin;

	/**
	 * @return the klass
	 */
	public SelectAmountSub[] getKlass() {
		return klass;
	}

	/**
	 * @param klass
	 *            the klass to set
	 */
	public void setKlass(SelectAmountSub[] klass) {
		this.klass = klass;
	}

	/**
	 * @return the cin
	 */
	public SelectAmountSub[] getCin() {
		return cin;
	}

	/**
	 * @param cin
	 *            the cin to set
	 */
	public void setCin(SelectAmountSub[] cin) {
		this.cin = cin;
	}
}
