
package com.huawei.crm.service.order.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.order.query.ExtParameterList;


/**
 * <p>Java class for OrderLineInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderLineInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="CustomerId" type="{http://crm.huawei.com/basetype/}CustomerId"/>
 *         &lt;element name="ItemType" type="{http://crm.huawei.com/basetype/}EffectMode"/>
 *         &lt;element name="ItemId" type="{http://crm.huawei.com/basetype/}ItemId"/>
 *         &lt;element name="ItemName" type="{http://crm.huawei.com/basetype/}OfferingName"/>
 *         &lt;element name="IdentityId" type="{http://crm.huawei.com/basetype/}ResourceCode" minOccurs="0"/>
 *         &lt;element name="SkuId" type="{http://crm.huawei.com/basetype/}SkuId"/>
 *         &lt;element name="Quantity" type="{http://crm.huawei.com/basetype/}Quantity"/>
 *         &lt;element name="ItemInstanceId" type="{http://crm.huawei.com/basetype/}OfferingId" minOccurs="0"/>
 *         &lt;element name="ShippingFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderLineInfo", propOrder = {

})
public class OrderLineInfo {

    @XmlElement(name = "CustomerId")
    protected long customerId;
    @XmlElement(name = "ItemType", required = true)
    protected String itemType;
    @XmlElement(name = "ItemId", required = true)
    protected String itemId;
    @XmlElement(name = "ItemName", required = true)
    protected String itemName;
    @XmlElement(name = "IdentityId")
    protected String identityId;
    @XmlElement(name = "SkuId", required = true)
    protected String skuId;
    @XmlElement(name = "Quantity", required = true)
    protected String quantity;
    @XmlElement(name = "ItemInstanceId")
    protected String itemInstanceId;
    @XmlElement(name = "ShippingFlag", required = true)
    protected String shippingFlag;
    @XmlElement(name = "ExtParamList")
    protected ExtParameterList extParamList;

    /**
     * Gets the value of the customerId property.
     * 
     */
    public long getCustomerId() {
        return customerId;
    }

    /**
     * Sets the value of the customerId property.
     * 
     */
    public void setCustomerId(long value) {
        this.customerId = value;
    }

    /**
     * Gets the value of the itemType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemType() {
        return itemType;
    }

    /**
     * Sets the value of the itemType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemType(String value) {
        this.itemType = value;
    }

    /**
     * Gets the value of the itemId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemId() {
        return itemId;
    }

    /**
     * Sets the value of the itemId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemId(String value) {
        this.itemId = value;
    }

    /**
     * Gets the value of the itemName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemName() {
        return itemName;
    }

    /**
     * Sets the value of the itemName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemName(String value) {
        this.itemName = value;
    }

    /**
     * Gets the value of the identityId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentityId() {
        return identityId;
    }

    /**
     * Sets the value of the identityId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentityId(String value) {
        this.identityId = value;
    }

    /**
     * Gets the value of the skuId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSkuId() {
        return skuId;
    }

    /**
     * Sets the value of the skuId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSkuId(String value) {
        this.skuId = value;
    }

    /**
     * Gets the value of the quantity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuantity() {
        return quantity;
    }

    /**
     * Sets the value of the quantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuantity(String value) {
        this.quantity = value;
    }

    /**
     * Gets the value of the itemInstanceId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemInstanceId() {
        return itemInstanceId;
    }

    /**
     * Sets the value of the itemInstanceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemInstanceId(String value) {
        this.itemInstanceId = value;
    }

    /**
     * Gets the value of the shippingFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShippingFlag() {
        return shippingFlag;
    }

    /**
     * Sets the value of the shippingFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShippingFlag(String value) {
        this.shippingFlag = value;
    }

    /**
     * Gets the value of the extParamList property.
     * 
     * @return
     *     possible object is
     *     {@link ExtParameterList }
     *     
     */
    public ExtParameterList getExtParamList() {
        return extParamList;
    }

    /**
     * Sets the value of the extParamList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtParameterList }
     *     
     */
    public void setExtParamList(ExtParameterList value) {
        this.extParamList = value;
    }

}
