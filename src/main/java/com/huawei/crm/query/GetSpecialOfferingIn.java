package com.huawei.crm.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.crm.basetype.OfferingKey;

/**
 * <p>
 * Java class for GetSpecialOfferingIn complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="GetSpecialOfferingIn">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ExternalSubscriberId" type="{http://crm.huawei.com/basetype/}ExternalSubscriberId" minOccurs="0"/>
 *         &lt;element name="SubscriberId" type="{http://crm.huawei.com/basetype/}SubscriberId" minOccurs="0"/>
 *         &lt;element name="ServiceNumber" type="{http://crm.huawei.com/basetype/}ServiceNumber" minOccurs="0"/>
 *         &lt;element name="OfferingId" type="{http://crm.huawei.com/basetype/}OfferingKey" minOccurs="0"/>
 *         &lt;element name="ServiceCategory" type="{http://crm.huawei.com/basetype/}ServiceCategory" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetSpecialOfferingIn", propOrder = {})
public class GetSpecialOfferingIn {
	@XmlElement(name = "ExternalSubscriberId")
	protected String externalSubscriberId;
	@XmlElement(name = "SubscriberId")
	protected Long subscriberId;
	@XmlElement(name = "ServiceNumber")
	protected String serviceNumber;
	@XmlElement(name = "OfferingId")
	protected OfferingKey offeringId;
	@XmlElement(name = "ServiceCategory")
	protected String serviceCategory;

	/**
	 * Gets the value of the externalSubscriberId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getExternalSubscriberId() {
		return externalSubscriberId;
	}

	/**
	 * Sets the value of the externalSubscriberId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setExternalSubscriberId(String value) {
		this.externalSubscriberId = value;
	}

	/**
	 * Gets the value of the subscriberId property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getSubscriberId() {
		return subscriberId;
	}

	/**
	 * Sets the value of the subscriberId property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setSubscriberId(Long value) {
		this.subscriberId = value;
	}

	/**
	 * Gets the value of the serviceNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getServiceNumber() {
		return serviceNumber;
	}

	/**
	 * Sets the value of the serviceNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setServiceNumber(String value) {
		this.serviceNumber = value;
	}

	/**
	 * Gets the value of the offeringId property.
	 * 
	 * @return possible object is {@link OfferingKey }
	 * 
	 */
	public OfferingKey getOfferingId() {
		return offeringId;
	}

	/**
	 * Sets the value of the offeringId property.
	 * 
	 * @param value
	 *            allowed object is {@link OfferingKey }
	 * 
	 */
	public void setOfferingId(OfferingKey value) {
		this.offeringId = value;
	}

	/**
	 * Gets the value of the serviceCategory property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getServiceCategory() {
		return serviceCategory;
	}

	/**
	 * Sets the value of the serviceCategory property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setServiceCategory(String value) {
		this.serviceCategory = value;
	}
}
