package com.evampsaanga.ulduzumupdate.getusagehistory;

import java.util.List;

import com.evampsaanga.bakcell.responseheaders.BaseResponse;

public class GetUsageHistoryResponseClient extends BaseResponse {

	List<UsageHistory> data;

	public List<UsageHistory> getData() {
		return data;
	}

	public void setData(List<UsageHistory> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "GetUsageHistoryResponseClient [data=" + data + "]";
	}

}
