
package com.huawei.bme.cbsinterface.bcservices.querypayment;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.bme.cbsinterface.cbscommon.querypayment.RequestHeader;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequestHeader" type="{http://www.huawei.com/bme/cbsinterface/cbscommon}RequestHeader"/>
 *         &lt;element name="QueryPaymentRelationRequest" type="{http://www.huawei.com/bme/cbsinterface/bcservices}QueryPaymentRelationRequest"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "requestHeader",
    "queryPaymentRelationRequest"
})
@XmlRootElement(name = "QueryPaymentRelationRequestMsg")
public class QueryPaymentRelationRequestMsg {

    @XmlElement(name = "RequestHeader", namespace = "", required = true)
    protected RequestHeader requestHeader;
    @XmlElement(name = "QueryPaymentRelationRequest", namespace = "", required = true)
    protected QueryPaymentRelationRequest queryPaymentRelationRequest;

    /**
     * Gets the value of the requestHeader property.
     * 
     * @return
     *     possible object is
     *     {@link RequestHeader }
     *     
     */
    public RequestHeader getRequestHeader() {
        return requestHeader;
    }

    /**
     * Sets the value of the requestHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestHeader }
     *     
     */
    public void setRequestHeader(RequestHeader value) {
        this.requestHeader = value;
    }

    /**
     * Gets the value of the queryPaymentRelationRequest property.
     * 
     * @return
     *     possible object is
     *     {@link QueryPaymentRelationRequest }
     *     
     */
    public QueryPaymentRelationRequest getQueryPaymentRelationRequest() {
        return queryPaymentRelationRequest;
    }

    /**
     * Sets the value of the queryPaymentRelationRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryPaymentRelationRequest }
     *     
     */
    public void setQueryPaymentRelationRequest(QueryPaymentRelationRequest value) {
        this.queryPaymentRelationRequest = value;
    }

}
