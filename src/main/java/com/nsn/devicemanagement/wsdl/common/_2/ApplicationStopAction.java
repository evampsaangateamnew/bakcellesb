package com.nsn.devicemanagement.wsdl.common._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import com.nsn.devicemanagement.wsdl.applicationmanagement._2.Application;

/**
 * <p>
 * Java class for ApplicationStopAction complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="ApplicationStopAction">
 *   &lt;complexContent>
 *     &lt;extension base="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}Action">
 *       &lt;sequence>
 *         &lt;element name="application" type="{http://nsn.com/DeviceManagement/wsdl/ApplicationManagement/2.0}application" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ApplicationStopAction", propOrder = { "application" })
public class ApplicationStopAction extends Action {
	protected Application application;

	/**
	 * Gets the value of the application property.
	 * 
	 * @return possible object is {@link Application }
	 * 
	 */
	public Application getApplication() {
		return application;
	}

	/**
	 * Sets the value of the application property.
	 * 
	 * @param value
	 *            allowed object is {@link Application }
	 * 
	 */
	public void setApplication(Application value) {
		this.application = value;
	}
}
