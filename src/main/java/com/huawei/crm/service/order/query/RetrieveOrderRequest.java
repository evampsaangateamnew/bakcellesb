
package com.huawei.crm.service.order.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.order.query.RequestHeader;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequestHeader" type="{http://crm.huawei.com/basetype/}RequestHeader"/>
 *         &lt;element name="RetrieveOrderRequestBody" type="{http://crm.huawei.com/service/}RetrieveOrderRequestBody"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "requestHeader",
    "retrieveOrderRequestBody"
})
@XmlRootElement(name = "RetrieveOrderRequest")
public class RetrieveOrderRequest {

    @XmlElement(name = "RequestHeader", required = true)
    protected RequestHeader requestHeader;
    @XmlElement(name = "RetrieveOrderRequestBody", required = true)
    protected RetrieveOrderRequestBody retrieveOrderRequestBody;

    /**
     * Gets the value of the requestHeader property.
     * 
     * @return
     *     possible object is
     *     {@link RequestHeader }
     *     
     */
    public RequestHeader getRequestHeader() {
        return requestHeader;
    }

    /**
     * Sets the value of the requestHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestHeader }
     *     
     */
    public void setRequestHeader(RequestHeader value) {
        this.requestHeader = value;
    }

    /**
     * Gets the value of the retrieveOrderRequestBody property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveOrderRequestBody }
     *     
     */
    public RetrieveOrderRequestBody getRetrieveOrderRequestBody() {
        return retrieveOrderRequestBody;
    }

    /**
     * Sets the value of the retrieveOrderRequestBody property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveOrderRequestBody }
     *     
     */
    public void setRetrieveOrderRequestBody(RetrieveOrderRequestBody value) {
        this.retrieveOrderRequestBody = value;
    }

}
