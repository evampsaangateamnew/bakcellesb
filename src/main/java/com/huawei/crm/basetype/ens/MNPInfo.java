package com.huawei.crm.basetype.ens;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for MNPInfo complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="MNPInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="PortId" type="{http://crm.huawei.com/basetype/}PortId"/>
 *         &lt;element name="PortType" type="{http://crm.huawei.com/basetype/}PortType"/>
 *         &lt;element name="PortServiceNumber" type="{http://crm.huawei.com/basetype/}PortServiceNumber"/>
 *         &lt;element name="RecipientSp" type="{http://crm.huawei.com/basetype/}RecipientSp"/>
 *         &lt;element name="DonorSp" type="{http://crm.huawei.com/basetype/}DonorSp"/>
 *         &lt;element name="DonorNo" type="{http://crm.huawei.com/basetype/}DonorNo" minOccurs="0"/>
 *         &lt;element name="PortDate" type="{http://crm.huawei.com/basetype/}PortDate"/>
 *         &lt;element name="RecipientNo" type="{http://crm.huawei.com/basetype/}RecipientNo" minOccurs="0"/>
 *         &lt;element name="ReMsisdn" type="{http://crm.huawei.com/basetype/}ReMsisdn" minOccurs="0"/>
 *         &lt;element name="CallNumberReservationPassWord" type="{http://crm.huawei.com/basetype/}CallNumberReservationPassWord" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MNPInfo", propOrder = {})
public class MNPInfo {
	@XmlElement(name = "PortId", required = true)
	protected String portId;
	@XmlElement(name = "PortType", required = true)
	protected String portType;
	@XmlElement(name = "PortServiceNumber", required = true)
	protected String portServiceNumber;
	@XmlElement(name = "RecipientSp", required = true)
	protected String recipientSp;
	@XmlElement(name = "DonorSp", required = true)
	protected String donorSp;
	@XmlElement(name = "DonorNo")
	protected String donorNo;
	@XmlElement(name = "PortDate", required = true)
	protected String portDate;
	@XmlElement(name = "RecipientNo")
	protected String recipientNo;
	@XmlElement(name = "ReMsisdn")
	protected String reMsisdn;
	@XmlElement(name = "CallNumberReservationPassWord")
	protected String callNumberReservationPassWord;
	@XmlElement(name = "ExtParamList")
	protected ExtParameterList extParamList;

	/**
	 * Gets the value of the portId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPortId() {
		return portId;
	}

	/**
	 * Sets the value of the portId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPortId(String value) {
		this.portId = value;
	}

	/**
	 * Gets the value of the portType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPortType() {
		return portType;
	}

	/**
	 * Sets the value of the portType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPortType(String value) {
		this.portType = value;
	}

	/**
	 * Gets the value of the portServiceNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPortServiceNumber() {
		return portServiceNumber;
	}

	/**
	 * Sets the value of the portServiceNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPortServiceNumber(String value) {
		this.portServiceNumber = value;
	}

	/**
	 * Gets the value of the recipientSp property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRecipientSp() {
		return recipientSp;
	}

	/**
	 * Sets the value of the recipientSp property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRecipientSp(String value) {
		this.recipientSp = value;
	}

	/**
	 * Gets the value of the donorSp property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDonorSp() {
		return donorSp;
	}

	/**
	 * Sets the value of the donorSp property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDonorSp(String value) {
		this.donorSp = value;
	}

	/**
	 * Gets the value of the donorNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDonorNo() {
		return donorNo;
	}

	/**
	 * Sets the value of the donorNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDonorNo(String value) {
		this.donorNo = value;
	}

	/**
	 * Gets the value of the portDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPortDate() {
		return portDate;
	}

	/**
	 * Sets the value of the portDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPortDate(String value) {
		this.portDate = value;
	}

	/**
	 * Gets the value of the recipientNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRecipientNo() {
		return recipientNo;
	}

	/**
	 * Sets the value of the recipientNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRecipientNo(String value) {
		this.recipientNo = value;
	}

	/**
	 * Gets the value of the reMsisdn property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getReMsisdn() {
		return reMsisdn;
	}

	/**
	 * Sets the value of the reMsisdn property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setReMsisdn(String value) {
		this.reMsisdn = value;
	}

	/**
	 * Gets the value of the callNumberReservationPassWord property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCallNumberReservationPassWord() {
		return callNumberReservationPassWord;
	}

	/**
	 * Sets the value of the callNumberReservationPassWord property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCallNumberReservationPassWord(String value) {
		this.callNumberReservationPassWord = value;
	}

	/**
	 * Gets the value of the extParamList property.
	 * 
	 * @return possible object is {@link ExtParameterList }
	 * 
	 */
	public ExtParameterList getExtParamList() {
		return extParamList;
	}

	/**
	 * Sets the value of the extParamList property.
	 * 
	 * @param value
	 *            allowed object is {@link ExtParameterList }
	 * 
	 */
	public void setExtParamList(ExtParameterList value) {
		this.extParamList = value;
	}
}
