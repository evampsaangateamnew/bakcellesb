package com.evampsaanga.configs;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.bakcell.db.DBFactory;
import com.evampsaanga.bakcell.getDashboardData.DashboardDataRequest;
import com.evampsaanga.bakcell.otp.MessageTemplateResponse;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.magento.tariffdetailsv2.TariffDetailsMagentoResponse;
import com.saanga.magento.apiclient.RestClient;

public class ConfigurationManager {
	
	public static final Logger logger = Logger.getLogger("bakcellLogs-V2");
	
	public static HashMap<String, String> propertiesCacheHashMap = new HashMap<>();
	public static HashMap<String,com.evampsaanga.magento.tariffdetailsv2.TariffDetailsMagentoResponse> propertiesCacheTariffMap = new HashMap<>();
	public static HashMap<String, MessageTemplateResponse> propertiesMessageTemplates = new HashMap<>();
	private static Properties propsDB = null;
	private static Properties propsMapping = null;
	private static Properties propsNGBSSAPI = null;
	private static Properties propsMappingISO = null;
	public static final String MAPPING_BALANCE_LOWER_LIMIT = "balance.lower.limit";
	public static final String MAPPING_CRM_CUSTOMER_TYPE = "crm.customertype.";
	public static final String MAPPING_CRM_TITLE = "crm.title.";
	public static final String MAPPING_CRM_GENDER = "crm.gender.";
	public static final String MAPPING_LOYALTY = "loyalty.";
	public static final String MAPPING_HLR_LANGUAGE = "hlr.language.";
	public static final String MAPPING_CRM_LANGUAGE = "crm.language.";
	public static final String MAPPING_HLR_SUBSCRIBER_TYPE = "hlr.subscriberType.";
	public static final String MAPPING_HLR_STATUS = "hlr.status.";
	public static final String MAPPING_CRM_STATUS = "crm.status.";
	public static final String MAPPING_HLR_BRANDID = "hlr.brandId.";
	public static final String MAPPING_HLR_GROUPID = "hlr.groupId.";
	public static final String MAPPING_INSTALLMENTSTATUS = "handset.status.";
	public static final String MAPPING_LOANLOG_STATUS = "cbs.ar.loanlog.";
	public static final String MAPPING_BALANCE_MAIN = "hlrweb.balance.";
	public static final String MAPPING_GROUPID = "groupId.";
	public static final String MAPPING_GROUPIDTRANS = "groupIdtrans.";
	public static final String ULDUZUM_LANGUAGE_MAPPING = "ulduzum.subscriber.language.";
	public static final String MSISDN_CODE_START="994";
	
	public static String getMappingValueByKey(String key) {
		if (propsMapping == null)
				loadProperties();
		return propsMapping.getProperty(key, "");
	}
	
	/**
	 * Method accepts key as input and returns value against that key from
	 * loaded properties
	 * 
	 * @param key
	 * @return
	 */
	public static String getNGBSSAPIProperties(String key) {
		if (propsNGBSSAPI == null)
			loadNGBSSAPIProperties();
		String value = "";
		value = propsNGBSSAPI.getProperty(key, "");
		return value;
	}
	
	public static void loadPropertiesISO() {
		FileInputStream fileInput = null;
		Reader reader = null;
		try {
			File file = new File(Config.MAPPING);
			fileInput = new FileInputStream(file);
			reader = new InputStreamReader(fileInput, "ISO-8859-1");
			propsMappingISO = new Properties();
			propsMappingISO.load(reader);
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		}
		finally {
			try{
				if(fileInput != null)
					fileInput.close();
				if(reader != null)
					reader.close();
			}catch (Exception e) {
				logger.error(Helper.GetException(e));
			}
		}
	}
	
	public static String getMappingValueByKeyISO(String key) {
		if (propsMappingISO == null)
				loadPropertiesISO();
		return propsMappingISO.getProperty(key, "");
	}
	
	public static String getLoyaltyMapping(String key) {
		if (propsMapping == null)
			loadProperties();
		return propsMapping.getProperty(key,"Low");
	}
	
	/**
	 * Method loads properties for CRM
	 */
	public static void loadNGBSSAPIProperties() {
		FileInputStream fileInput = null;
		Reader reader = null;
		try {
			File file = new File(Config.NGBSS_API_PROPERTIES);
			fileInput = new FileInputStream(file);
			reader = new InputStreamReader(fileInput, "UTF-8");
			propsNGBSSAPI = new Properties();
			propsNGBSSAPI.load(reader);
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		}
		finally {
			try{
				if(fileInput != null)
					fileInput.close();
				if(reader != null)
					reader.close();
			}catch (Exception e) {
				logger.error(Helper.GetException(e));
			}
		}
	}
	
	public static void loadProperties() {
		FileInputStream fileInput = null;
		Reader reader = null;
		try {
			File file = new File(Config.MAPPING);
			fileInput = new FileInputStream(file);
			reader = new InputStreamReader(fileInput, "UTF-8");
			propsMapping = new Properties();
			propsMapping.load(reader);
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		}
		finally {
			try{
				if(fileInput != null)
					fileInput.close();
				if(reader != null)
					reader.close();
			}catch (Exception e) {
				logger.error(Helper.GetException(e));
			}
		}
	}
	
	public static void reloadPropertiesCacheTariff()
	{
		if(propertiesCacheTariffMap!=null && propertiesCacheTariffMap.size() == 0)
		{
			try 
			{
				logger.info("**************Loading Tariff Details**************");
				com.evampsaanga.magento.tariffdetailsv2.TariffDetailsMagentoResponse dataEnglish = new com.evampsaanga.magento.tariffdetailsv2.TariffDetailsMagentoResponse();
				com.evampsaanga.magento.tariffdetailsv2.TariffDetailsMagentoResponse dataAzeri = new com.evampsaanga.magento.tariffdetailsv2.TariffDetailsMagentoResponse();
				com.evampsaanga.magento.tariffdetailsv2.TariffDetailsMagentoResponse dataRussian = new com.evampsaanga.magento.tariffdetailsv2.TariffDetailsMagentoResponse();
				
				JSONObject requestdestinationTariff = new JSONObject();
				requestdestinationTariff.put("offeringId", "");
				requestdestinationTariff.put("lang", "3");
				logger.info("Request Magento: "+requestdestinationTariff.toString());
				//call third party client and save cache
				 String responseEnglish = RestClient.SendCallToMagento(
			     ConfigurationManager.getConfigurationFromCache("magento.app.tariffV2"), requestdestinationTariff.toString());
				 logger.info("Response Magento: "+responseEnglish);
				 
				 
				 
				 requestdestinationTariff.put("lang", "2");
				 logger.info("Request Magento: "+requestdestinationTariff.toString());
				 String responseAzeri= RestClient.SendCallToMagento(
					     ConfigurationManager.getConfigurationFromCache("magento.app.tariffV2"), requestdestinationTariff.toString());
				 logger.info("Response Magento: "+responseAzeri);
				 requestdestinationTariff.put("lang", "1");
				 logger.info("Request Magento: "+requestdestinationTariff.toString());
				 String responseRussian = RestClient.SendCallToMagento(
					     ConfigurationManager.getConfigurationFromCache("magento.app.tariffV2"), requestdestinationTariff.toString());
				 logger.info("Response Magento: "+responseRussian);
				 dataEnglish = Helper.JsonToObject(responseEnglish,com.evampsaanga.magento.tariffdetailsv2.TariffDetailsMagentoResponse.class);
				 dataAzeri = Helper.JsonToObject(responseAzeri,com.evampsaanga.magento.tariffdetailsv2.TariffDetailsMagentoResponse.class);
				 dataRussian = Helper.JsonToObject(responseRussian,com.evampsaanga.magento.tariffdetailsv2.TariffDetailsMagentoResponse.class);
				 
	
				 propertiesCacheTariffMap.put(Constants.TARIFF_DETAIL_ENGLISH, dataEnglish);
				 propertiesCacheTariffMap.put(Constants.TARIFF_DETAIL_RUSSIAN, dataRussian);
				 propertiesCacheTariffMap.put(Constants.TARIFF_DETAIL_AZERI, dataAzeri);
	
	
				 logger.debug("TariffResponse FROM magento >>:" + responseEnglish);
				 logger.debug("TariffResponse FROM magento >>:" + responseRussian);
				 logger.debug("TariffResponse FROM magento >>:" + responseAzeri);
		 
			}
			catch (JSONException e) 
			{
				// TODO Auto-generated catch block
				logger.info(Helper.GetException(e));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.info(Helper.GetException(e));
				
			}

		 
		}
	}
	
	
	
	public static void reloadPropertiesCache()
	{
		if(propertiesCacheHashMap!=null && propertiesCacheHashMap.size() == 0)
		{
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try (Connection connection = DBFactory.getMagentoDBConnection()){
		String getAllConfiguration = "select * from configuration_items";
		preparedStatement = connection.prepareStatement(getAllConfiguration);
		resultSet = preparedStatement.executeQuery();
		while(resultSet.next())
			propertiesCacheHashMap.put(resultSet.getString("key"), resultSet.getString("value"));
		}catch (Exception e) {
			logger.error(Helper.GetException(e));
		}
		finally {
			try{
				if(resultSet != null)
					resultSet.close();
				if(preparedStatement != null)
					preparedStatement.close();
			}catch (Exception e) {
				logger.error(Helper.GetException(e));
			}
		}
		}
	}
	
	public static void reloadPropertiesMessageTemplates()
	{
		if(propertiesMessageTemplates!=null && propertiesMessageTemplates.size() == 0)
		{
			try {
				String response =  RestClient.SendCallToMagento(
						ConfigurationManager.getConfigurationFromCache("magento.app.signupflow.getMessages"),
						"");
				MessageTemplateResponse messageTemplateResponse = Helper.JsonToObject(response, MessageTemplateResponse.class);
				propertiesMessageTemplates.put(Constants.GET_MESSAGE_TEMPLATE_KEY, messageTemplateResponse);
				
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.info(Helper.GetException(e));
			}
			
		
		}
	}
	
	public static MessageTemplateResponse getMessageTemplateFromCache(String key)
	{
		logger.info("Cache Get Message template for key "+key);
		logger.info("Cache Check in HashMap "+propertiesMessageTemplates.containsKey(key));
		if(propertiesMessageTemplates.containsKey(key))
		{
			
			logger.info("Cache "+propertiesMessageTemplates.get(key));
			return propertiesMessageTemplates.get(key);
		}
		else
		{
			logger.info("Cache Reloading properties");
			reloadPropertiesMessageTemplates();
			logger.info("Cache Check in HashMap after reload"+propertiesMessageTemplates.containsKey(key));
			if(propertiesMessageTemplates.containsKey(key))
				return propertiesMessageTemplates.get(key);
			else
				return null;
		}
	}
	
	public static TariffDetailsMagentoResponse getConfigurationFromTariffCache(String key)
	{
		logger.info("Cache Get configuration for key "+key);
		logger.info("Cache Check in HashMap "+propertiesCacheTariffMap.containsKey(key));
		if(propertiesCacheTariffMap.containsKey(key))
		{
			logger.debug("Cache "+propertiesCacheTariffMap.get(key));
			return propertiesCacheTariffMap.get(key);
		}
		else
		{
			logger.info("Cache Reloading properties");
			reloadPropertiesCacheTariff();
			logger.info("Cache Check in HashMap after reload"+propertiesCacheTariffMap.containsKey(key));
			if(propertiesCacheTariffMap.containsKey(key))
				return propertiesCacheTariffMap.get(key);
			else
				return null;
		}
	}
	
	public static String getConfigurationFromCache(String key)
	{
		logger.info("Cache Get configuration for key "+key);
		logger.info("Cache Check in HashMap "+propertiesCacheHashMap.containsKey(key));
		if(propertiesCacheHashMap.containsKey(key))
		{
			logger.info("Cache "+propertiesCacheHashMap.get(key));
			return propertiesCacheHashMap.get(key);
		}
		else
		{
			logger.info("Cache Reloading properties");
			reloadPropertiesCache();
			logger.info("Cache Check in HashMap after reload"+propertiesCacheHashMap.containsKey(key));
			if(propertiesCacheHashMap.containsKey(key))
				return propertiesCacheHashMap.get(key);
			else
				return "";
		}
	}

	public static boolean getContainsValueByTariffKey(String key) {
		if(propertiesCacheTariffMap.containsKey(key))
			return true;
		else
			reloadPropertiesCacheTariff();
		return propertiesCacheTariffMap.containsKey(key);
	}
	
	
	public static boolean getContainsValueByKey(String key) {
		if(propertiesCacheHashMap.containsKey(key))
			return true;
		else
			reloadPropertiesCache();
		return propertiesCacheHashMap.containsKey(key);
	}
	
	
	
	public static boolean getMessageTemplateValueByKey(String key) {
		if(propertiesMessageTemplates.containsKey(key))
			return true;
		else
			reloadPropertiesMessageTemplates();
		return propertiesMessageTemplates.containsKey(key);
	}

	/**
	 * Method loads properties for Local DB
	 */
	public static void loadDBProperties() {
		FileInputStream fileInput = null;
		Reader reader = null;
		try {
			File file = new File(Config.DATABASE_FILE);
			fileInput = new FileInputStream(file);
			reader = new InputStreamReader(fileInput, "UTF-8");
			propsDB = new Properties();
			propsDB.load(reader);
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		}
		finally {
			try{
				if(fileInput != null)
					fileInput.close();
				if(reader != null)
					reader.close();
			}catch (Exception e) {
				logger.error(Helper.GetException(e));
			}
		}
	}

	/**
	 * Method accepts key as input and returns value against that key from
	 * loaded properties
	 * 
	 * @param key
	 * @return
	 */
	public static String getDBProperties(String key) {
		if (propsDB == null)
			loadDBProperties();
		String value = "";
		value = propsDB.getProperty(key, "");
		return value;
	}
}
