package com.evampsaanga.bakcell.balance;

import com.evampsaanga.bakcell.responseheaders.BaseResponse;
import com.evampsaanga.gethomepage.Balance;

public class BalanceResponse extends BaseResponse{
	 Balance balance = new Balance();

	public Balance getBalance() {
		return balance;
	}

	public void setBalance(Balance balance) {
		this.balance = balance;
	}

	@Override
	public String toString() {
		return "BalanceResponse [balance=" + balance + ", toString()=" + super.toString() + "]";
	}
	 
}
