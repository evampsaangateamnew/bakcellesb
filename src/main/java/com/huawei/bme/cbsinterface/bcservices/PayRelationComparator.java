package com.huawei.bme.cbsinterface.bcservices;

import java.util.Comparator;

import com.huawei.bme.cbsinterface.bcservices.querypayment.QueryPaymentRelationResult.PaymentRelationList;
import com.huawei.bme.cbsinterface.bcservices.querypayment.QueryPaymentRelationResult.PaymentRelationList.PayRelation;




public class PayRelationComparator implements Comparator<PayRelation> {

	@Override
	public int compare(PayRelation payRelation, PayRelation payRelationCompareTo) {
		return payRelation.getPriority().compareTo(payRelationCompareTo.getPriority());
		    
	}

	
}
