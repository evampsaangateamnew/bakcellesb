package com.evampsaanga.bakcell.notificationadmin;

import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.amqimplementationsesb.LogsType;
import com.evampsaanga.bakcell.notificationadmin.magentomodels.MagentoResponse;
import com.evampsaanga.bakcell.notificationadmin.magentomodels.OfferData;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.ThirdPartyNames;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.developer.utils.SOAPLoggingHandler;
import com.saanga.magento.apiclient.RestClient;

@Path("/bakcell")
public class NotificationAdminLand {

	public static final Logger logger = Logger.getLogger("bakcellLogs-V2");

	@POST
	@Path("/savenotification")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SaveNotificationResponse saveNotification(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.SAVE_NOTIFICATION_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.ADMIN_NOTIFICATION);
		logs.setTableType(LogsType.SaveNotification);
		try {
			logger.info("Request Landed on " + Transactions.SAVE_NOTIFICATION_TRANSACTION_NAME + requestBody);
			SaveNotificationRequest cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, SaveNotificationRequest.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setIsB2B(cclient.getIsB2B());
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				SaveNotificationResponse resp = new SaveNotificationResponse();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(ResponseCodes.ERROR_400_CODE);
				logs.setResponseDescription(ResponseCodes.ERROR_400);
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					SaveNotificationResponse resp = new SaveNotificationResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						SaveNotificationResponse res = new SaveNotificationResponse();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						logs.setResponseCode(ResponseCodes.ERROR_400);
						logs.setResponseDescription(verification);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return res;
					}
				} else {
					SaveNotificationResponse resp = new SaveNotificationResponse();
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode((ResponseCodes.ERROR_MSISDN_CODE));
					logs.setResponseDescription(ResponseCodes.ERROR_MSISDN);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					logger.info(cclient.getmsisdn() + "- Request Call to Save Notification");
					boolean status = false;
					SaveNotificationResponse resp = new SaveNotificationResponse();
					if (cclient.getConfirm().equalsIgnoreCase("true")) {
						status = NotificationDAO.saveNotification(cclient);
						if (status) {
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
							logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
						} else {
							resp.setReturnCode(ResponseCodes.GENERIC_ERROR_CODE);
							resp.setReturnMsg(ResponseCodes.GENERIC_ERROR_DES);
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
						}
					} else {
						JSONObject requestData = new JSONObject();
						requestData.put("lang", "3");
						String response = RestClient.SendCallToMagentoNar(
								ConfigurationManager.getConfigurationFromCache("magento.app.productdata"),
								requestData.toString());
						MagentoResponse responseData = Helper.JsonToObject(response, MagentoResponse.class);
						if (responseData.getResultCode().equalsIgnoreCase("590")) {
							String msgAZ = cclient.getMessageAz().toLowerCase();
							String msgRU = cclient.getMessageRu().toLowerCase();
							String msgEN = cclient.getMessageEn().toLowerCase();
							for (OfferData offerdata : responseData.getData().getOffer()) {
								if (msgAZ.contains(offerdata.getOfferName().toLowerCase())
										|| msgRU.contains(offerdata.getOfferName().toLowerCase())
										|| msgEN.contains(offerdata.getOfferName().toLowerCase())) {
									resp.setReturnCode("20");
									resp.setReturnMsg("Kindly confirm");
									return resp;
								}
							}
							status = NotificationDAO.saveNotification(cclient);
							if (status) {
								resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
								resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
								logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
								logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
							} else {
								resp.setReturnCode(ResponseCodes.GENERIC_ERROR_CODE);
								resp.setReturnMsg(ResponseCodes.GENERIC_ERROR_DES);
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
							}
						}

					}

					logger.info(cclient.getmsisdn() + "-Response returned from Save Notification-" + resp);
					return resp;

				} else {
					SaveNotificationResponse resp = new SaveNotificationResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		SaveNotificationResponse resp = new SaveNotificationResponse();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseDescription(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	@POST
	@Path("/getnotification")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetNotificationsResponse getNotification(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.GET_NOTIFICATIONS_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.ADMIN_NOTIFICATION);
		logs.setTableType(LogsType.GetNotifications);
		try {
			logger.info("Request Landed on " + Transactions.GET_NOTIFICATIONS_TRANSACTION_NAME + requestBody);
			GetNotificationsRequest cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, GetNotificationsRequest.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setIsB2B(cclient.getIsB2B());
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				GetNotificationsResponse resp = new GetNotificationsResponse();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(ResponseCodes.ERROR_400_CODE);
				logs.setResponseDescription(ResponseCodes.ERROR_400);
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					GetNotificationsResponse resp = new GetNotificationsResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						GetNotificationsResponse res = new GetNotificationsResponse();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						logs.setResponseCode(ResponseCodes.ERROR_400);
						logs.setResponseDescription(verification);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return res;
					}
				} else {
					GetNotificationsResponse resp = new GetNotificationsResponse();
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode((ResponseCodes.ERROR_MSISDN_CODE));
					logs.setResponseDescription(ResponseCodes.ERROR_MSISDN);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					logger.info(cclient.getmsisdn() + "- Request Call to Get Notification");

					List<NotificationData> notificationData = NotificationDAO.getNotificationsFromDB();
					GetNotificationsResponse resp = new GetNotificationsResponse();
					resp.setNotifications(notificationData);
					resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
					resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);

					logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
					logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());

					logs.updateLog(logs);
					logger.info(cclient.getmsisdn() + "-Response returned from Get Notification-" + resp);
					return resp;

				} else {
					GetNotificationsResponse resp = new GetNotificationsResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		GetNotificationsResponse resp = new GetNotificationsResponse();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseDescription(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	@POST
	@Path("/deletenotification")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DeleteNotificationResponse deleteNotification(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.DELETE_NOTIFICATION_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.ADMIN_NOTIFICATION);
		logs.setTableType(LogsType.DeleteNotification);
		try {
			logger.info("Request Landed on " + Transactions.DELETE_NOTIFICATION_TRANSACTION_NAME + requestBody);
			DeleteNotificationRequest cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, DeleteNotificationRequest.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setIsB2B(cclient.getIsB2B());
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				DeleteNotificationResponse resp = new DeleteNotificationResponse();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(ResponseCodes.ERROR_400_CODE);
				logs.setResponseDescription(ResponseCodes.ERROR_400);
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					DeleteNotificationResponse resp = new DeleteNotificationResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						DeleteNotificationResponse res = new DeleteNotificationResponse();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						logs.setResponseCode(ResponseCodes.ERROR_400);
						logs.setResponseDescription(verification);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return res;
					}
				} else {
					DeleteNotificationResponse resp = new DeleteNotificationResponse();
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode((ResponseCodes.ERROR_MSISDN_CODE));
					logs.setResponseDescription(ResponseCodes.ERROR_MSISDN);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					logger.info(cclient.getmsisdn() + "- Request Call to Get Reports");

					boolean status = NotificationDAO.deleteNotification(cclient);
					DeleteNotificationResponse resp = new DeleteNotificationResponse();
					if (status) {
						resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
						logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());

						logs.updateLog(logs);
					} else {
						resp.setReturnCode(ResponseCodes.GENERIC_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.GENERIC_ERROR_DES);
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());

						logs.updateLog(logs);
					}
					logger.info(cclient.getmsisdn() + "-Response returned from Get Reports-" + resp);
					return resp;

				} else {
					DeleteNotificationResponse resp = new DeleteNotificationResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		DeleteNotificationResponse resp = new DeleteNotificationResponse();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseDescription(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	@POST
	@Path("/savepicnotification")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SavePicNotificationResponse savePicNotification(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.SAVE_PIC_NOTIFICATION_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.ADMIN_NOTIFICATION);
		logs.setTableType(LogsType.SavePicNotification);
		try {
			logger.info("Request Landed on " + Transactions.SAVE_PIC_NOTIFICATION_TRANSACTION_NAME + requestBody);
			SavePicNotificationRequest cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, SavePicNotificationRequest.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setIsB2B(cclient.getIsB2B());
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				SavePicNotificationResponse resp = new SavePicNotificationResponse();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(ResponseCodes.ERROR_400_CODE);
				logs.setResponseDescription(ResponseCodes.ERROR_400);
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					SavePicNotificationResponse resp = new SavePicNotificationResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						SavePicNotificationResponse res = new SavePicNotificationResponse();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						logs.setResponseCode(ResponseCodes.ERROR_400);
						logs.setResponseDescription(verification);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return res;
					}
				} else {
					SavePicNotificationResponse resp = new SavePicNotificationResponse();
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode((ResponseCodes.ERROR_MSISDN_CODE));
					logs.setResponseDescription(ResponseCodes.ERROR_MSISDN);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					logger.info(cclient.getmsisdn() + "- Request Call to Save Pic Notification");

					boolean status = false;
					SavePicNotificationResponse resp = new SavePicNotificationResponse();
					if (cclient.getConfirm().equalsIgnoreCase("true")) {
						boolean saved = NotificationDAO.saveFile(cclient);
						if (saved) {
							status = NotificationDAO.savePicNotifications(cclient);
							if (status) {
								resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
								resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
								logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
								logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
							} else {
								resp.setReturnCode(ResponseCodes.GENERIC_ERROR_CODE);
								resp.setReturnMsg(ResponseCodes.GENERIC_ERROR_DES);
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
							}
						} else {
							resp.setReturnCode(ResponseCodes.GENERIC_ERROR_CODE);
							resp.setReturnMsg("file upload fail");
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
						}
					} else {
						JSONObject requestData = new JSONObject();
						requestData.put("lang", "3");
						String response = RestClient.SendCallToMagentoNar(
								ConfigurationManager.getConfigurationFromCache("magento.app.productdata"),
								requestData.toString());
						MagentoResponse responseData = Helper.JsonToObject(response, MagentoResponse.class);
						if (responseData.getResultCode().equalsIgnoreCase("590")) {
							String msgAZ = cclient.getMessageAz().toLowerCase();
							String msgRU = cclient.getMessageRu().toLowerCase();
							String msgEN = cclient.getMessageEn().toLowerCase();
							for (OfferData offerdata : responseData.getData().getOffer()) {
								if (msgAZ.contains(offerdata.getOfferName().toLowerCase())
										|| msgRU.contains(offerdata.getOfferName().toLowerCase())
										|| msgEN.contains(offerdata.getOfferName().toLowerCase())) {
									resp.setReturnCode("20");
									resp.setReturnMsg("Kindly confirm");
									return resp;
								}
							}
							boolean saved = NotificationDAO.saveFile(cclient);
							if (saved) {
								status = NotificationDAO.savePicNotifications(cclient);
								if (status) {
									resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
									resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
									logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
									logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
									logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
									logs.updateLog(logs);
								} else {
									resp.setReturnCode(ResponseCodes.GENERIC_ERROR_CODE);
									resp.setReturnMsg(ResponseCodes.GENERIC_ERROR_DES);
									logs.setResponseDescription(resp.getReturnMsg());
									logs.setResponseCode(resp.getReturnCode());
									logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
									logs.updateLog(logs);
								}
							} else {
								resp.setReturnCode(ResponseCodes.GENERIC_ERROR_CODE);
								resp.setReturnMsg("file upload fail");
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
							}
						}

					}

					logger.info(cclient.getmsisdn() + "-Response returned from Save Pic Notification-" + resp);
					return resp;

				} else {
					SavePicNotificationResponse resp = new SavePicNotificationResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		SavePicNotificationResponse resp = new SavePicNotificationResponse();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseDescription(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	@POST
	@Path("/getpicnotifications")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetPicNotificationsResponse getPicNotifications(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.GET_PIC_NOTIFICATIONS_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.ADMIN_NOTIFICATION);
		logs.setTableType(LogsType.GetPicNotifications);
		try {
			logger.info("Request Landed on " + Transactions.GET_PIC_NOTIFICATIONS_TRANSACTION_NAME + requestBody);
			GetPicNotificationsRequest cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, GetPicNotificationsRequest.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setIsB2B(cclient.getIsB2B());
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				GetPicNotificationsResponse resp = new GetPicNotificationsResponse();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(ResponseCodes.ERROR_400_CODE);
				logs.setResponseDescription(ResponseCodes.ERROR_400);
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					GetPicNotificationsResponse resp = new GetPicNotificationsResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						GetPicNotificationsResponse res = new GetPicNotificationsResponse();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						logs.setResponseCode(ResponseCodes.ERROR_400);
						logs.setResponseDescription(verification);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return res;
					}
				} else {
					GetPicNotificationsResponse resp = new GetPicNotificationsResponse();
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode((ResponseCodes.ERROR_MSISDN_CODE));
					logs.setResponseDescription(ResponseCodes.ERROR_MSISDN);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					logger.info(cclient.getmsisdn() + "- Request Call to Get PIC Notification");

					List<PicNotificationData> picNotifications = NotificationDAO.getPicNotificationsFromDB();

					GetPicNotificationsResponse resp = new GetPicNotificationsResponse();
					resp.setPicNotifications(picNotifications);
					resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
					resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
					logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
					logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());

					logs.updateLog(logs);

					logger.info(cclient.getmsisdn() + "-Response returned from Get PIC Notification-" + resp);
					return resp;

				} else {
					GetPicNotificationsResponse resp = new GetPicNotificationsResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		GetPicNotificationsResponse resp = new GetPicNotificationsResponse();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseDescription(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	@POST
	@Path("/deletepicnotification")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DeletePicNotificationResponse deletePicNotification(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.DELETE_PIC_NOTIFICATION_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.ADMIN_NOTIFICATION);
		logs.setTableType(LogsType.DeletePicNotification);
		try {
			logger.info("Request Landed on " + Transactions.DELETE_PIC_NOTIFICATION_TRANSACTION_NAME + requestBody);
			DeletePicNotificationRequest cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, DeletePicNotificationRequest.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setIsB2B(cclient.getIsB2B());
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				DeletePicNotificationResponse resp = new DeletePicNotificationResponse();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(ResponseCodes.ERROR_400_CODE);
				logs.setResponseDescription(ResponseCodes.ERROR_400);
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					DeletePicNotificationResponse resp = new DeletePicNotificationResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						DeletePicNotificationResponse res = new DeletePicNotificationResponse();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						logs.setResponseCode(ResponseCodes.ERROR_400);
						logs.setResponseDescription(verification);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return res;
					}
				} else {
					DeletePicNotificationResponse resp = new DeletePicNotificationResponse();
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode((ResponseCodes.ERROR_MSISDN_CODE));
					logs.setResponseDescription(ResponseCodes.ERROR_MSISDN);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					logger.info(cclient.getmsisdn() + "- Request Call to Get Reports");

					boolean status = NotificationDAO.deletePicNotification(cclient);
					DeletePicNotificationResponse resp = new DeletePicNotificationResponse();
					if (status) {
						resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
						logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());

						logs.updateLog(logs);
					} else {
						resp.setReturnCode(ResponseCodes.GENERIC_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.GENERIC_ERROR_DES);
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());

						logs.updateLog(logs);
					}
					logger.info(cclient.getmsisdn() + "-Response returned from Get Reports-" + resp);
					return resp;

				} else {
					DeletePicNotificationResponse resp = new DeletePicNotificationResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		DeletePicNotificationResponse resp = new DeletePicNotificationResponse();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseDescription(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	@POST
	@Path("/getreports")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetReportsResponse getReports(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.GET_REPORTS_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.ADMIN_NOTIFICATION);
		logs.setTableType(LogsType.GetReports);
		try {
			logger.info("Request Landed on " + Transactions.GET_REPORTS_TRANSACTION_NAME + requestBody);
			GetReportsRequest cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, GetReportsRequest.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setIsB2B(cclient.getIsB2B());
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				GetReportsResponse resp = new GetReportsResponse();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(ResponseCodes.ERROR_400_CODE);
				logs.setResponseDescription(ResponseCodes.ERROR_400);
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					GetReportsResponse resp = new GetReportsResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						GetReportsResponse res = new GetReportsResponse();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						logs.setResponseCode(ResponseCodes.ERROR_400);
						logs.setResponseDescription(verification);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return res;
					}
				} else {
					GetReportsResponse resp = new GetReportsResponse();
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode((ResponseCodes.ERROR_MSISDN_CODE));
					logs.setResponseDescription(ResponseCodes.ERROR_MSISDN);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					logger.info(cclient.getmsisdn() + "- Request Call to Get Reports");

					List<ReportData> reportDataList = NotificationDAO.getReports();

//					ReportData reportData = new ReportData();
//					reportData.setLastModified("15-12-2018 00:39:57");
//					reportData.setFileName("Notification-38.csv");
//					reportData.setFilePath("http://10.220.48.195:8080/bakcell-admin-portal/view_files.faces#");
//
//					List<ReportData> reportDataList = new ArrayList<ReportData>();
//					reportDataList.add(reportData);
//					reportDataList.add(reportData);
//					reportDataList.add(reportData);

					GetReportsResponse resp = new GetReportsResponse();

					if (reportDataList != null) {
						resp.setReportData(reportDataList);
						resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
						logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());

						logs.updateLog(logs);
					} else {
						resp.setReturnCode(ResponseCodes.GENERIC_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.GENERIC_ERROR_DES);
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());

						logs.updateLog(logs);
					}

					logger.info(cclient.getmsisdn() + "-Response returned from Get Reports-" + resp);
					return resp;

				} else {
					GetReportsResponse resp = new GetReportsResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		GetReportsResponse resp = new GetReportsResponse();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseDescription(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	@POST
	@Path("/getfcmnotifications")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetFCMNotificationsResponse getFCMNotifications(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.GET_FCM_NOTIFICATIONS_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.ADMIN_NOTIFICATION);
		logs.setTableType(LogsType.GetFCMNotifications);
		try {
			logger.info("Request Landed on " + Transactions.GET_FCM_NOTIFICATIONS_TRANSACTION_NAME + requestBody);
			GetFCMNotificationsRequest cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, GetFCMNotificationsRequest.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setIsB2B(cclient.getIsB2B());
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				GetFCMNotificationsResponse resp = new GetFCMNotificationsResponse();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(ResponseCodes.ERROR_400_CODE);
				logs.setResponseDescription(ResponseCodes.ERROR_400);
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					GetFCMNotificationsResponse resp = new GetFCMNotificationsResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						GetFCMNotificationsResponse res = new GetFCMNotificationsResponse();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						logs.setResponseCode(ResponseCodes.ERROR_400);
						logs.setResponseDescription(verification);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return res;
					}
				} else {
					GetFCMNotificationsResponse resp = new GetFCMNotificationsResponse();
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode((ResponseCodes.ERROR_MSISDN_CODE));
					logs.setResponseDescription(ResponseCodes.ERROR_MSISDN);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					logger.info(cclient.getmsisdn() + "- Request Call to Get FCM Notification");
					List<FCMNotification> fcmNotifications = NotificationDAO.getFCMNotification(cclient);

					GetFCMNotificationsResponse resp = new GetFCMNotificationsResponse();
					resp.setFcmNotifications(fcmNotifications);
					resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
					resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
					logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
					logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());

					logs.updateLog(logs);

					logger.info(cclient.getmsisdn() + "-Response returned from Get FCM Notification-" + resp);
					return resp;

				} else {
					GetFCMNotificationsResponse resp = new GetFCMNotificationsResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		GetFCMNotificationsResponse resp = new GetFCMNotificationsResponse();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseDescription(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	@POST
	@Path("/gethistorynotifications")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetHistoryNotificationsResponse getHistoryNotifications(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.GET_HISTORY_NOTIFICATIONS_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.ADMIN_NOTIFICATION);
		logs.setTableType(LogsType.GetHistoryNotifications);
		try {
			logger.info("Request Landed on " + Transactions.GET_HISTORY_NOTIFICATIONS_TRANSACTION_NAME + requestBody);
			GetHistoryNotificationsRequest cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, GetHistoryNotificationsRequest.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setIsB2B(cclient.getIsB2B());
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				GetHistoryNotificationsResponse resp = new GetHistoryNotificationsResponse();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(ResponseCodes.ERROR_400_CODE);
				logs.setResponseDescription(ResponseCodes.ERROR_400);
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					GetHistoryNotificationsResponse resp = new GetHistoryNotificationsResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						GetHistoryNotificationsResponse res = new GetHistoryNotificationsResponse();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						logs.setResponseCode(ResponseCodes.ERROR_400);
						logs.setResponseDescription(verification);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return res;
					}
				} else {
					GetHistoryNotificationsResponse resp = new GetHistoryNotificationsResponse();
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode((ResponseCodes.ERROR_MSISDN_CODE));
					logs.setResponseDescription(ResponseCodes.ERROR_MSISDN);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					logger.info(cclient.getmsisdn() + "- Request Call to Get History Notification");

					List<HistoryNotification> historyNotifications = NotificationDAO
							.getHistoryByMsisdn(cclient.getUserMsisdn());

					GetHistoryNotificationsResponse resp = new GetHistoryNotificationsResponse();
					resp.setHistoryNotifications(historyNotifications);
					resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
					resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
					logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
					logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());

					logs.updateLog(logs);

					logger.info(cclient.getmsisdn() + "-Response returned from Get History Notification-" + resp);
					return resp;

				} else {
					GetHistoryNotificationsResponse resp = new GetHistoryNotificationsResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		GetHistoryNotificationsResponse resp = new GetHistoryNotificationsResponse();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseDescription(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	@POST
	@Path("/adminlogin")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public AdminLoginResponse adminLogin(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.ADMIN_LOGIN_NOTIFICATION_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.ADMIN_NOTIFICATION);
		logs.setTableType(LogsType.AdminLogin);
		try {
			logger.info("Request Landed on " + Transactions.ADMIN_LOGIN_NOTIFICATION_TRANSACTION_NAME + requestBody);
			AdminLoginRequest cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, AdminLoginRequest.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setIsB2B(cclient.getIsB2B());
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				AdminLoginResponse resp = new AdminLoginResponse();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(ResponseCodes.ERROR_400_CODE);
				logs.setResponseDescription(ResponseCodes.ERROR_400);
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					AdminLoginResponse resp = new AdminLoginResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						AdminLoginResponse res = new AdminLoginResponse();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						logs.setResponseCode(ResponseCodes.ERROR_400);
						logs.setResponseDescription(verification);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return res;
					}
				} else {
					AdminLoginResponse resp = new AdminLoginResponse();
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode((ResponseCodes.ERROR_MSISDN_CODE));
					logs.setResponseDescription(ResponseCodes.ERROR_MSISDN);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					logger.info(cclient.getmsisdn() + "- Request Call to Save Pic Notification");

					boolean status = NotificationDAO.authenticateUser(cclient);

					AdminLoginResponse resp = new AdminLoginResponse();
					if (status) {
						resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
						logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());

						logs.updateLog(logs);
					} else {
						resp.setReturnCode(ResponseCodes.GENERIC_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.GENERIC_ERROR_DES);
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());

						logs.updateLog(logs);
					}
					logger.info(cclient.getmsisdn() + "-Response returned from Save Pic Notification-" + resp);
					return resp;

				} else {
					AdminLoginResponse resp = new AdminLoginResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		AdminLoginResponse resp = new AdminLoginResponse();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseDescription(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}
}
