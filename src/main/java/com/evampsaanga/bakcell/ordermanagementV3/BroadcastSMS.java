package com.evampsaanga.bakcell.ordermanagementV3;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.bakcell.db.DBFactory;
import com.evampsaanga.bakcell.ordermanagementV3.OrderManagementLand;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;

public class BroadcastSMS extends OrderBase{
	public static final Logger loggerV2 = Logger.getLogger("bakcellLogs-order");
	
	public OrderManagementResponse insertOrder(JSONObject jsonObject, String credential, Logs logs){
		String credentials = null;
		OrderManagementResponse resp = new OrderManagementResponse();
		OrderManagementLand land = new OrderManagementLand();
//		java.sql.Connection conn = DBFactory.getDbConnectionFromPool();
		try(java.sql.Connection conn = DBFactory.getDbConnectionFromPool();) {
			credentials = Decrypter.getInstance().decrypt(credential);
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				resp.setResponseMsg("Credentials are not correct");
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			} else if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				jsonObject.put("type", "BroadcastSMS");
				try {
					// inserting data into purchase order table
					if (jsonObject.getString("msisdn").toString().equals("")) {
						resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
						resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
					String uuid = land.insertPurchaseOrder(jsonObject, conn);
					// inserting data into order attributes
					try {
						land.addOrderDetails(jsonObject, uuid, conn, jsonObject.get("type").toString());

						try {
							land.addOrderttributes(jsonObject, uuid, conn);
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							resp.setResponseMsg("Your Request is under Process Now");
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						} catch (Exception e) {
							loggerV2.error(Helper.GetException(e));
							resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
							resp.setReturnMsg(e.getMessage());
							resp.setResponseMsg(e.getMessage());
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception e) {
						loggerV2.error(Helper.GetException(e));
						resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
						resp.setReturnMsg(e.getMessage());
						resp.setResponseMsg(e.getMessage());
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} catch (Exception e) {
					loggerV2.error(Helper.GetException(e));
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(e.getMessage());
					resp.setResponseMsg(e.getMessage());
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception e) {
			loggerV2.error(Helper.GetException(e));
			resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
			resp.setReturnMsg(e.getMessage());
			resp.setResponseMsg(e.getMessage());
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		return resp;
	}

}
