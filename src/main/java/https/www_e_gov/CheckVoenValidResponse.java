
package https.www_e_gov;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import https.wwww_e_gov.ResponseInformationByVoen;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CheckVoenValidResult" type="{https://wwww.e-gov.az}ResponseInformationByVoen" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "checkVoenValidResult"
})
@XmlRootElement(name = "CheckVoenValidResponse")
public class CheckVoenValidResponse {

    @XmlElement(name = "CheckVoenValidResult")
    protected ResponseInformationByVoen checkVoenValidResult;

    /**
     * Gets the value of the checkVoenValidResult property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseInformationByVoen }
     *     
     */
    public ResponseInformationByVoen getCheckVoenValidResult() {
        return checkVoenValidResult;
    }

    /**
     * Sets the value of the checkVoenValidResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseInformationByVoen }
     *     
     */
    public void setCheckVoenValidResult(ResponseInformationByVoen value) {
        this.checkVoenValidResult = value;
    }

}
