package com.evampsaanga.bakcell.limitpaymentrelation;

import java.io.IOException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.amqimplementationsesb.LogsType;
import com.evampsaanga.bakcell.hlrweb.getquerybalanceRequest.HLRBalanceServices;
import com.evampsaanga.bakcell.ordermanagementV2.OrderManagementLand;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.ThirdPartyNames;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.developer.utils.SOAPLoggingHandler;
import com.evampsaanga.gethomepage.Balance;
import com.evampsaanga.magento.tariffdetailsv2.TariffDetailsMagentoResponse;

@Path("/bakcell/")
public class LimitPaymentRelationRequestLand {
	public static final Logger logger = Logger.getLogger("bakcellLogs-V2");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LimitPaymentRelationResponse Get(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.CHANGE_TARRIF_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.CHANGE_TARRIF);
		logs.setTableType(LogsType.ChangeTarrif);

		LimitPaymentRelationRequestClient cclient = null;
		LimitPaymentRelationResponse resp = new LimitPaymentRelationResponse();
		try {
			logger.info("Request Landed on LimitPaymentRelationRequestLand:" + requestBody);

			try {
				cclient = Helper.JsonToObject(requestBody, LimitPaymentRelationRequestClient.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setLang(cclient.getLang());
					logs.setIsB2B(cclient.getIsB2B());
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				logger.info(cclient.getmsisdn() + "-Checking Company Standard Value");
				if (!Helper.isValidIntegerAndFloat(cclient.getCompanyStandardValue())) {
					logger.info(cclient.getmsisdn() + "-Company Standard Value is Invalid");
					resp.setReturnCode("100");
					resp.setReturnMsg("Company Standard Value is incorrect");
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				logger.info(cclient.getmsisdn() + "-Company Standard Value is valid");
				if (credentials == null) {

					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {

					com.huawei.crm.query.GetSubscriberResponse response = new com.evampsaanga.bakcell.getsubscriber.CRMSubscriberService()
							.GetSubscriberRequest(cclient.getMsisdnuser());

					JSONObject request = new JSONObject();
					request.put("offeringId",
							response.getGetSubscriberBody().getPrimaryOffering().getOfferingId().getOfferingId());
					request.put("lang", cclient.getLang());

					logger.info(cclient.getmsisdn() + "-msisdn " + cclient.getmsisdn() + request.toString());

					TariffDetailsMagentoResponse dataTariffDetailsbulk = OrderManagementLand
							.tariffDetailsV2BulkResponse(request.toString());
					// SubmitOrderResponse response =
					// changetarrifresponse(cclient.getmsisdn(),
					// cclient.getOfferingId());

					String destinationtariffMrcValue = "";

					String maximumLimit = "";

					if (dataTariffDetailsbulk.getResultCode()
							.equals(ResponseCodes.TARIFF_DETAILS_VERSION_2_SUCCESSFUL)) {

						int indexForOffering = Helper.compare(
								response.getGetSubscriberBody().getPrimaryOffering().getOfferingId().getOfferingId(),
								dataTariffDetailsbulk.getData());
						if (indexForOffering != -1) {
							destinationtariffMrcValue = dataTariffDetailsbulk.getData().get(indexForOffering)
									.getHeader().getMrcValue();
						}

						if (cclient.getGroupType().equalsIgnoreCase("PayBySubs")
								|| cclient.getGroupType().equalsIgnoreCase("PayBySub")) {
							logger.info(cclient.getmsisdn() + "-IF PayBySubs: " + cclient.getGroupType());
							logger.info(
									cclient.getmsisdn() + "-MrcInt: " + conversionStrtoInt(destinationtariffMrcValue));
							logger.info(cclient.getmsisdn() + "-CompanyValueInt: "
									+ conversionStrtoInt(cclient.getCompanyStandardValue()));

							HLRBalanceServices service = new HLRBalanceServices();
							Balance balance = service.getBalance(cclient.getMsisdnuser(), "postpaid", null);

							logger.info(cclient.getmsisdn() + "-IF PayBySubs BalanceResponseCode: "
									+ balance.getReturnCode());
							if (balance.getReturnCode().equals("0")) {
								logger.info(cclient.getmsisdn() + "-IF PayBySubs BalanceCorporate String: "
										+ balance.getPostpaid().getCurrentCreditIndividualValue());
								logger.info(
										cclient.getmsisdn() + "-IF PayBySubs MRC String:" + destinationtariffMrcValue);
								logger.info(cclient.getmsisdn() + "-PayBySubs companyStandard String:"
										+ cclient.getCompanyStandardValue());
								double individualBalance = Math.ceil(
										Double.parseDouble(balance.getPostpaid().getCurrentCreditIndividualValue()));
								double mrcValue = Math.ceil(Double.parseDouble(destinationtariffMrcValue));
								logger.info(cclient.getmsisdn() + "-IF PayBySubs Balance Double: " + individualBalance);
								logger.info(cclient.getmsisdn() + "-IF PayBySubs MRc Double: " + mrcValue);

								if (individualBalance < 0) {
									individualBalance = (-1) * individualBalance;
									logger.info(cclient.getmsisdn() + "-Balane after making positive :"
											+ individualBalance);
								}

								// START OF NEW LOGIC 13 JULY 2020
								logger.info(cclient.getmsisdn() + "-Start of New Logic");
								logger.info(
										cclient.getmsisdn() + "-Current MRC VALUE is :" + destinationtariffMrcValue);

								int maxLimit = conversionStrtoInt(destinationtariffMrcValue) + 30;
								logger.info(cclient.getmsisdn() + "-Current MRC + 30 VALUE is :" + maxLimit);

								// maximumLimit =
								// Double.toString(individualBalance +
								// Double.parseDouble(cclient.getCompanyStandardValue()));
								maximumLimit = maxLimit + "";
								int minAmount = (int) Math.round(individualBalance);

								logger.info(cclient.getmsisdn() + "-INITIAL LIMIT IS :" + minAmount);
								logger.info(cclient.getmsisdn() + "-MAX LIMIT IS :" + maximumLimit);

								resp.setInitialLimit(String.valueOf(minAmount));
								resp.setMaxLimit(String.valueOf(conversionStrtoInt(maximumLimit)));
								resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
								resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							}

							else {
								resp.setReturnCode(balance.getReturnCode());
								resp.setReturnMsg(balance.getReturnMsg());
								logs.setResponseCode(balance.getReturnCode());
								logs.setResponseDescription(balance.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());

							}
						} else if (cclient.getGroupType().equalsIgnoreCase("PartPay")
								|| cclient.getGroupType().equalsIgnoreCase("FullPay")) {

							logger.info(cclient.getmsisdn() + "-IF PartPay or FullPay: " + cclient.getGroupType());
							HLRBalanceServices service = new HLRBalanceServices();
							Balance balance = service.getBalance(cclient.getMsisdnuser(), "postpaid", null);

							logger.info(cclient.getmsisdn() + "-IF PartPay or FullPay BalanceResponseCode: "
									+ balance.getReturnCode());
							if (balance.getReturnCode().equals("0")) {
								logger.info(cclient.getmsisdn()
										+ "-IF PartPay or FullPay getCurrentCreditCorporateValue String: "
										+ balance.getPostpaid().getCurrentCreditCorporateValue());
								logger.info(cclient.getmsisdn() + "-IF PartPay or FullPay MRC String:"
										+ destinationtariffMrcValue);
								logger.info(cclient.getmsisdn() + "-IF PartPay or FullPay companyStandard String:"
										+ cclient.getCompanyStandardValue());
								double corporateBalance = Math.ceil(
										Double.parseDouble(balance.getPostpaid().getCurrentCreditCorporateValue()));
								double mrcValue = Math.ceil(Double.parseDouble(destinationtariffMrcValue));
								logger.info(cclient.getmsisdn() + "-IF PartPay or FullPay Balance Double: "
										+ corporateBalance);
								logger.info(cclient.getmsisdn() + "-IF PartPay or FullPay MRc Double: " + mrcValue);

								if (corporateBalance < 0.0) {
									corporateBalance = (-1) * corporateBalance;
									logger.info(
											cclient.getmsisdn() + "-Balane after making positive :" + corporateBalance);
								}
								if (corporateBalance > mrcValue) {
									logger.info(cclient.getmsisdn() + "-inittialLimit Balance: " + corporateBalance);
									resp.setInitialLimit(
											String.valueOf(conversionStrtoInt(Double.toString(corporateBalance))));
									maximumLimit = String.valueOf(
											Double.parseDouble(balance.getPostpaid().getCurrentCreditCorporateValue())
													+ Double.parseDouble(cclient.getCompanyStandardValue()));
								} else {
									logger.info(cclient.getmsisdn() + "-initial Limit MRC: " + mrcValue);
									resp.setInitialLimit(String.valueOf(conversionStrtoInt(destinationtariffMrcValue)));
									maximumLimit = String.valueOf(
											Double.parseDouble(balance.getPostpaid().getCurrentCreditCorporateValue())
													+ Double.parseDouble(cclient.getCompanyStandardValue()));
								}
								resp.setMaxLimit(String.valueOf(conversionStrtoInt(maximumLimit)));
								// resp.setMaxLimit(maximumLimit);
								resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
								resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							}

							else {
								resp.setReturnCode(balance.getReturnCode());
								resp.setReturnMsg(balance.getReturnMsg());
								logs.setResponseCode(balance.getReturnCode());
								logs.setResponseDescription(balance.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());

							}

						} else {
							logger.info(cclient.getmsisdn() + "-Wrong Group Type");
						}

					} else {
						resp.setReturnCode(dataTariffDetailsbulk.getResultCode());
						resp.setReturnMsg(dataTariffDetailsbulk.getMsg());
						logs.setResponseCode(dataTariffDetailsbulk.getResultCode());
						logs.setResponseDescription(dataTariffDetailsbulk.getMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					}
					logs.updateLog(logs);

				} else {

					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);

				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
		}

		try {
			logger.info(cclient.getmsisdn() + "-ResponseBeforeReturn: " + Helper.ObjectToJson(resp));
		} catch (IOException e) {
			logger.error(Helper.GetException(e));
			resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(Helper.GetException(e));
			resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
		}
		return resp;
	}

	public static int conversionStrtoInt(String valueinput)

	{
		if (valueinput == null || valueinput.equalsIgnoreCase("0") || valueinput.isEmpty()) {
			return 0;
		}
		int finalValue = 0;
		if (valueinput.contains(".")) {
			double value = Double.parseDouble(valueinput);
			finalValue = (int) value;

		} else {
			finalValue = Integer.parseInt(valueinput);

		}

		return finalValue;
	}

}
