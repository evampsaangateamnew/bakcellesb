package com.evampsaanga.magento.tariffdetailsold;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Data {
	@JsonProperty("Postpaid")
	private Postpaid Postpaid = null;
	@JsonProperty("Prepaid")
	private Prepaid Prepaid = null;
}
