package com.evampsaanga.bakcell.notificationadmin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.activemq.transport.udp.DatagramEndpoint;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.appserver.refreshappservercache.AppServerResponse;
import com.evampsaanga.bakcell.db.DBFactory;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.developer.utils.Helper;
import com.saanga.magento.apiclient.RestClient;

public class NotificationDAO {
	public static final Logger logger = Logger.getLogger("bakcellLogs-V2");

	public static boolean saveNotification(SaveNotificationRequest cclient) {
		PreparedStatement preparedStmt = null;
		ResultSet resultSet = null;
		try {
			Connection conn = DBFactory.getAppConnection();
			String query;
			if (cclient.getId() == null) {
				query = "INSERT INTO Notifications (title, messageAzeri, messageEnglish, messageRussian, datetime, icon, actionType, actionID, btnTxt, subscriberType, tariffType)"
						+ " VALUES (?,?,?,?,?,?,?,?,?,?,?)";
			} else {
				query = "UPDATE Notifications set title=?, messageAzeri=?, messageEnglish=?, messageRussian=?, datetime=?, icon=?, actionType=?, actionID=?, btnTxt=?, subscriberType=?, tariffType=? where id="
						+ cclient.getId();
			}

			// create the mysql insert prepared statement
			preparedStmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			preparedStmt.setString(1, cclient.getTitle());
			preparedStmt.setString(2, cclient.getMessageAz());
			preparedStmt.setString(3, cclient.getMessageEn());
			preparedStmt.setString(4, cclient.getMessageRu());
			preparedStmt.setString(5, cclient.getDateTime());
			preparedStmt.setString(6, cclient.getIcon());
			preparedStmt.setString(7, cclient.getActionType());
			preparedStmt.setString(8, cclient.getActionId());
			preparedStmt.setString(9, cclient.getButtonText());
			preparedStmt.setString(10, cclient.getSubscriberTypes());
			preparedStmt.setString(11, cclient.getTariffType());
			logger.info(cclient.getmsisdn() + "-Notifications Data insertion query-" + query);
			preparedStmt.executeUpdate();

			resultSet = preparedStmt.getGeneratedKeys();
			if (resultSet.next()) {
				logger.info(cclient.getmsisdn() + "-generate update : " + resultSet.getInt(1));
			}
			return true;
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			try {
				if (resultSet != null)
					resultSet.close();
				if (preparedStmt != null)
					preparedStmt.close();

			} catch (SQLException e) {
				logger.error(Helper.GetException(e));
			}

		}
		return false;
	}

	public static List<NotificationData> getNotificationsFromDB() {
		logger.info("Getting Notification data from database");
		List<NotificationData> notifications = new ArrayList<>();
		String query = "SELECT * FROM Notifications";
		ResultSet resultSet = null;
		PreparedStatement stmt = null;
		try {
			java.sql.Connection conn = DBFactory.getAppConnection();
			stmt = conn.prepareStatement(query);
			logger.info("Get Notifications from DB query-" + query);
			resultSet = stmt.executeQuery(query);

			while (resultSet.next()) {
				NotificationData notification = new NotificationData();
				notification.setId(resultSet.getString("id"));
				notification.setTitle(resultSet.getString("title"));
				notification.setMessageAz(resultSet.getString("messageAzeri"));
				notification.setMessageEn(resultSet.getString("messageEnglish"));
				notification.setMessageRu(resultSet.getString("messageRussian"));
				notification.setIcon(resultSet.getString("icon"));
				notification.setDateTime(resultSet.getString("datetime"));
				notification.setActionType(resultSet.getString("actionType"));
				notification.setActionId(resultSet.getString("actionID"));
				notification.setButtonText(resultSet.getString("btnTxt"));
				notification.setSubscriberTypes(resultSet.getString("subscriberType"));
				notification.setNotificationStatus(resultSet.getString("notificationStatus"));
				notification.setTariffType(resultSet.getString("tariffType"));

				notifications.add(notification);
			}
			logger.info("Return data from Notifications DB");
			return notifications;

		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			try {
				if (resultSet != null)
					resultSet.close();
				if (stmt != null)
					stmt.close();
			} catch (SQLException e) {
				logger.error(Helper.GetException(e));
			}

		}
		return null;
	}

	public static boolean savePicNotifications(SavePicNotificationRequest cclient) {
		PreparedStatement preparedStmt = null;
		ResultSet resultSet = null;
		try {
			Connection conn = DBFactory.getAppConnection();
			String query;
			if (cclient.getId() == null) {
				query = "INSERT INTO PICNotifications (title_en, title_az, title_ru, messageAzeri, messageEnglish, messageRussian, datetime, icon, actionType, actionID, btnTxt, filename, lang, notificationStatus) "
						+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			} else {
				query = "UPDATE PICNotifications set title_en=?, title_az=?, title_ru=?, messageAzeri=?, messageEnglish=?, messageRussian=?, datetime=?, icon=?, actionType=?, actionID=?, btnTxt=?, filename=?, lang=?, notificationStatus=? where id="
						+ cclient.getId();
			}

			// create the mysql insert prepared statement
			preparedStmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			preparedStmt.setString(1, cclient.getTitleEn());
			preparedStmt.setString(2, cclient.getTitleEn());
			preparedStmt.setString(3, cclient.getTitleEn());
			preparedStmt.setString(4, cclient.getMessageAz());
			preparedStmt.setString(5, cclient.getMessageEn());
			preparedStmt.setString(6, cclient.getMessageRu());
			preparedStmt.setString(7, cclient.getDateTime());
			preparedStmt.setString(8, cclient.getIcon());
			preparedStmt.setString(9, cclient.getActionType());
			preparedStmt.setString(10, cclient.getActionId());
			preparedStmt.setString(11, cclient.getButtonText());
			preparedStmt.setString(12, cclient.getFileName());
			preparedStmt.setString(13, cclient.getLanguage());
			preparedStmt.setString(14, "n"); // n->new, p->processing, c->completed
			logger.info(cclient.getmsisdn() + "-Pic Notifications Data insertion query-" + query);
			preparedStmt.executeUpdate();

			resultSet = preparedStmt.getGeneratedKeys();
			if (resultSet.next()) {
				logger.info(cclient.getmsisdn() + "-generate update : " + resultSet.getInt(1));
			}
			return true;
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			try {
				if (resultSet != null)
					resultSet.close();
				if (preparedStmt != null)
					preparedStmt.close();

			} catch (SQLException e) {
				logger.error(Helper.GetException(e));
			}

		}
		return false;
	}

	public static List<PicNotificationData> getPicNotificationsFromDB() {
		logger.info("Getting Pic Notification data from database");
		List<PicNotificationData> picNotifications = new ArrayList<>();
		String query = "SELECT * FROM PICNotifications";
		ResultSet resultSet = null;
		PreparedStatement stmt = null;
		try {
			java.sql.Connection conn = DBFactory.getAppConnection();
			stmt = conn.prepareStatement(query);
			logger.info("Get Pic Notifications from DB query-" + query);
			resultSet = stmt.executeQuery(query);

			while (resultSet.next()) {
				PicNotificationData picNotification = new PicNotificationData();
				picNotification.setId(resultSet.getString("id"));
				picNotification.setTitleAz(resultSet.getString("title_en"));
				picNotification.setTitleEn(resultSet.getString("title_az"));
				picNotification.setTitleRu(resultSet.getString("title_ru"));
				picNotification.setMessageAz(resultSet.getString("messageAzeri"));
				picNotification.setMessageEn(resultSet.getString("messageEnglish"));
				picNotification.setMessageRu(resultSet.getString("messageRussian"));
				picNotification.setIcon(resultSet.getString("icon"));
				picNotification.setDateTime(resultSet.getString("datetime"));
				picNotification.setActionType(resultSet.getString("actionType"));
				picNotification.setActionId(resultSet.getString("actionID"));
				picNotification.setButtonText(resultSet.getString("btnTxt"));
				picNotification.setLanguage(resultSet.getString("lang"));
				picNotification.setNotificationStatus(resultSet.getString("notificationStatus"));
				picNotification.setFileName(resultSet.getString("filename"));

				picNotifications.add(picNotification);
			}
			logger.info("Return data from Notifications DB");
			return picNotifications;

		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			try {
				if (resultSet != null)
					resultSet.close();
				if (stmt != null)
					stmt.close();
			} catch (SQLException e) {
				logger.error(Helper.GetException(e));
			}

		}
		return null;
	}

	public static List<HistoryNotification> getHistoryByMsisdn(String msisdn) {
		logger.info("Getting History By msisdn data from database");
		List<HistoryNotification> historyNotifications = new ArrayList<>();
		String query = "SELECT * FROM History where msisdn=" + msisdn;
		ResultSet resultSet = null;
		PreparedStatement stmt = null;
		try {
			java.sql.Connection conn = DBFactory.getAppConnection();
			stmt = conn.prepareStatement(query);
			logger.info("Get History By msisdn from DB query-" + query);
			resultSet = stmt.executeQuery(query);

			while (resultSet.next()) {
				HistoryNotification historyNotification = new HistoryNotification();
				historyNotification.setId(resultSet.getString("id"));
				historyNotification.setMsisdn(resultSet.getString("msisdn"));
				historyNotification.setDateTime(resultSet.getString("datetime"));
				historyNotification.setMessageAz(resultSet.getString("messageAzeri"));
				historyNotification.setMessageRu(resultSet.getString("messageRussian"));
				historyNotification.setMessageEn(resultSet.getString("messageEnglish"));
				historyNotification.setIcon(resultSet.getString("icon"));
				historyNotification.setActionType(resultSet.getString("actionType"));
				historyNotification.setActionID(resultSet.getString("actionID"));
				historyNotification.setBtnTxt(resultSet.getString("btnTxt"));
				historyNotification.setReadStatus(resultSet.getString("read_status"));
				historyNotification.setNotificationId(resultSet.getString("notificationid"));

				historyNotifications.add(historyNotification);
			}
			logger.info("Return data from History DB");
			return historyNotifications;

		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			try {
				if (resultSet != null)
					resultSet.close();
				if (stmt != null)
					stmt.close();
			} catch (SQLException e) {
				logger.error(Helper.GetException(e));
			}

		}
		return null;
	}

	public static boolean deleteNotification(DeleteNotificationRequest cclient) {

		PreparedStatement preparedStmt = null;
		ResultSet resultSet = null;
		try {
			Connection conn = DBFactory.getAppConnection();
			String query = "DELETE FROM Notifications WHERE id=" + cclient.getId();
			// create the mysql insert prepared statement
			preparedStmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			logger.info(cclient.getmsisdn() + "-Delete Notification query-" + query);
			preparedStmt.executeUpdate();

			resultSet = preparedStmt.getGeneratedKeys();
			if (resultSet.next()) {
				logger.info(cclient.getmsisdn() + "-generate update : " + resultSet.getInt(1));
			}
			return true;
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			try {
				if (resultSet != null)
					resultSet.close();
				if (preparedStmt != null)
					preparedStmt.close();

			} catch (SQLException e) {
				logger.error(Helper.GetException(e));
			}

		}
		return false;
	}

	public static boolean deletePicNotification(DeletePicNotificationRequest cclient) {
		PreparedStatement preparedStmt = null;
		ResultSet resultSet = null;
		try {
			Connection conn = DBFactory.getAppConnection();
			String query = "DELETE FROM PICNotifications WHERE id=" + cclient.getId();
			// create the mysql insert prepared statement
			preparedStmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			logger.info(cclient.getmsisdn() + "-Delete Notification query-" + query);
			preparedStmt.executeUpdate();

			resultSet = preparedStmt.getGeneratedKeys();
			if (resultSet.next()) {
				logger.info(cclient.getmsisdn() + "-generate update : " + resultSet.getInt(1));
			}
			return true;
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			try {
				if (resultSet != null)
					resultSet.close();
				if (preparedStmt != null)
					preparedStmt.close();

			} catch (SQLException e) {
				logger.error(Helper.GetException(e));
			}

		}
		return false;
	}

	public static List<FCMNotification> getFCMNotification(GetFCMNotificationsRequest cclient) {
		logger.info("Getting FCM Notifications By msisdn data from database");
		List<FCMNotification> fcmNotifications = new ArrayList<>();
		String query = "SELECT * FROM FCM_IDs where msisdn=" + cclient.getUserMsisdn();
		ResultSet resultSet = null;
		PreparedStatement stmt = null;
		try {
			java.sql.Connection conn = DBFactory.getAppConnection();
			stmt = conn.prepareStatement(query);
			logger.info("Get FCM Notifications By msisdn from DB query-" + query);
			resultSet = stmt.executeQuery(query);

			while (resultSet.next()) {
				FCMNotification fcmNotification = new FCMNotification();
				fcmNotification.setDeviceId(resultSet.getString("device_id"));
				fcmNotification.setMsisdn(resultSet.getString("msisdn"));
				fcmNotification.setFcmId(resultSet.getString("fcm_id"));
				fcmNotification.setRingingAction(resultSet.getString("ringing_action"));
				fcmNotification.setIsEnable(resultSet.getString("is_enable"));
				fcmNotification.setSubscriberType(resultSet.getString("subscriberType"));
				fcmNotification.setTariffType(resultSet.getString("tariffType"));

				fcmNotifications.add(fcmNotification);
			}
			logger.info("Return data from get FCM Notification");
			return fcmNotifications;

		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			try {
				if (resultSet != null)
					resultSet.close();
				if (stmt != null)
					stmt.close();
			} catch (SQLException e) {
				logger.error(Helper.GetException(e));
			}

		}
		return null;
	}

	public static boolean authenticateUser(AdminLoginRequest cclient) {
		logger.info("Authenticate User from database");
		String query = "SELECT * FROM Users where username='" + cclient.getmsisdn() + "' and password= '"
				+ cclient.getPassword() + "'";
		ResultSet resultSet = null;
		PreparedStatement stmt = null;
		try {
			java.sql.Connection conn = DBFactory.getAppConnection();
			stmt = conn.prepareStatement(query);
			logger.info("Authenticate User from database-" + query);
			resultSet = stmt.executeQuery(query);

			while (resultSet.next()) {
				return true;
			}
			logger.info("Return from Authenticate User");

		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			try {
				if (resultSet != null)
					resultSet.close();
				if (stmt != null)
					stmt.close();
			} catch (SQLException e) {
				logger.error(Helper.GetException(e));
			}

		}
		return false;
	}

	public static List<ReportData> getReports() throws Exception {
		logger.info("Request call to app server for get reports");
		JSONObject jsonObject = new JSONObject();
		String response = RestClient.SendCallToAppserver(
				ConfigurationManager.getConfigurationFromCache("appserver.app.getreports"), jsonObject.toString());
		logger.info("Response Returned from App server");
		GetReportsAppserverResponse data = Helper.JsonToObject(response, GetReportsAppserverResponse.class);

		if (data.getResultCode().equals("00")) {
			return data.getReportData();
		} else {
			return null;
		}
	}

	public static boolean saveFile(SavePicNotificationRequest cclient) throws Exception {
		logger.info("Request call to app server for get reports");
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("fileName", cclient.getFileName());
		jsonObject.put("file", cclient.getFile());

		String response = RestClient.SendCallToAppserver(
				ConfigurationManager.getConfigurationFromCache("appserver.app.savepicfile"), jsonObject.toString());
		logger.info("Response Returned from App server" + response);
		SaveFileAppserverResponse appServerResponse = Helper.JsonToObject(response, SaveFileAppserverResponse.class);

		if (appServerResponse.getResultCode().equals("00")) {
			return true;
		} else {
			return false;
		}
	}

}
