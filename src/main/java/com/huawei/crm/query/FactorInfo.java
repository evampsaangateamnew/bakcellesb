package com.huawei.crm.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for FactorInfo complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="FactorInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="CustomerFactor" type="{http://crm.huawei.com/query/}CustomerFeeFactorInfo" minOccurs="0"/>
 *         &lt;element name="SubscriberFactor" type="{http://crm.huawei.com/query/}SubscriberFeeFactorInfo" minOccurs="0"/>
 *         &lt;element name="AccountFactor" type="{http://crm.huawei.com/query/}AccountFeeFactorInfo" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FactorInfo", propOrder = {})
public class FactorInfo {
	@XmlElement(name = "CustomerFactor")
	protected CustomerFeeFactorInfo customerFactor;
	@XmlElement(name = "SubscriberFactor")
	protected SubscriberFeeFactorInfo subscriberFactor;
	@XmlElement(name = "AccountFactor")
	protected AccountFeeFactorInfo accountFactor;

	/**
	 * Gets the value of the customerFactor property.
	 * 
	 * @return possible object is {@link CustomerFeeFactorInfo }
	 * 
	 */
	public CustomerFeeFactorInfo getCustomerFactor() {
		return customerFactor;
	}

	/**
	 * Sets the value of the customerFactor property.
	 * 
	 * @param value
	 *            allowed object is {@link CustomerFeeFactorInfo }
	 * 
	 */
	public void setCustomerFactor(CustomerFeeFactorInfo value) {
		this.customerFactor = value;
	}

	/**
	 * Gets the value of the subscriberFactor property.
	 * 
	 * @return possible object is {@link SubscriberFeeFactorInfo }
	 * 
	 */
	public SubscriberFeeFactorInfo getSubscriberFactor() {
		return subscriberFactor;
	}

	/**
	 * Sets the value of the subscriberFactor property.
	 * 
	 * @param value
	 *            allowed object is {@link SubscriberFeeFactorInfo }
	 * 
	 */
	public void setSubscriberFactor(SubscriberFeeFactorInfo value) {
		this.subscriberFactor = value;
	}

	/**
	 * Gets the value of the accountFactor property.
	 * 
	 * @return possible object is {@link AccountFeeFactorInfo }
	 * 
	 */
	public AccountFeeFactorInfo getAccountFactor() {
		return accountFactor;
	}

	/**
	 * Sets the value of the accountFactor property.
	 * 
	 * @param value
	 *            allowed object is {@link AccountFeeFactorInfo }
	 * 
	 */
	public void setAccountFactor(AccountFeeFactorInfo value) {
		this.accountFactor = value;
	}
}
