
package com.huawei.crm.query.order.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SubGoodsResourceInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubGoodsResourceInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResourceSeq" type="{http://crm.huawei.com/basetype/}SubResSeq"/>
 *         &lt;element name="ResourceType" type="{http://crm.huawei.com/basetype/}ResourceType"/>
 *         &lt;element name="ResourceModel" type="{http://crm.huawei.com/basetype/}ResourceModel"/>
 *         &lt;element name="ResourceCode" type="{http://crm.huawei.com/basetype/}ResourceCode" minOccurs="0"/>
 *         &lt;element name="Quantity" type="{http://crm.huawei.com/basetype/}Quantity" minOccurs="0"/>
 *         &lt;element name="SalePrice" type="{http://crm.huawei.com/basetype/}Amount" minOccurs="0"/>
 *         &lt;element name="SaleDate" type="{http://crm.huawei.com/basetype/}Time" minOccurs="0"/>
 *         &lt;element name="OfferID" type="{http://crm.huawei.com/basetype/}OfferingId" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://crm.huawei.com/basetype/}SubResStatus"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubGoodsResourceInfo", propOrder = {
    "resourceSeq",
    "resourceType",
    "resourceModel",
    "resourceCode",
    "quantity",
    "salePrice",
    "saleDate",
    "offerID",
    "status"
})
public class SubGoodsResourceInfo {

    @XmlElement(name = "ResourceSeq", required = true)
    protected String resourceSeq;
    @XmlElement(name = "ResourceType", required = true)
    protected String resourceType;
    @XmlElement(name = "ResourceModel", required = true)
    protected String resourceModel;
    @XmlElement(name = "ResourceCode")
    protected String resourceCode;
    @XmlElement(name = "Quantity")
    protected String quantity;
    @XmlElement(name = "SalePrice")
    protected Long salePrice;
    @XmlElement(name = "SaleDate")
    protected String saleDate;
    @XmlElement(name = "OfferID")
    protected String offerID;
    @XmlElement(name = "Status", required = true)
    protected String status;

    /**
     * Gets the value of the resourceSeq property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResourceSeq() {
        return resourceSeq;
    }

    /**
     * Sets the value of the resourceSeq property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResourceSeq(String value) {
        this.resourceSeq = value;
    }

    /**
     * Gets the value of the resourceType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResourceType() {
        return resourceType;
    }

    /**
     * Sets the value of the resourceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResourceType(String value) {
        this.resourceType = value;
    }

    /**
     * Gets the value of the resourceModel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResourceModel() {
        return resourceModel;
    }

    /**
     * Sets the value of the resourceModel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResourceModel(String value) {
        this.resourceModel = value;
    }

    /**
     * Gets the value of the resourceCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResourceCode() {
        return resourceCode;
    }

    /**
     * Sets the value of the resourceCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResourceCode(String value) {
        this.resourceCode = value;
    }

    /**
     * Gets the value of the quantity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuantity() {
        return quantity;
    }

    /**
     * Sets the value of the quantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuantity(String value) {
        this.quantity = value;
    }

    /**
     * Gets the value of the salePrice property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSalePrice() {
        return salePrice;
    }

    /**
     * Sets the value of the salePrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSalePrice(Long value) {
        this.salePrice = value;
    }

    /**
     * Gets the value of the saleDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSaleDate() {
        return saleDate;
    }

    /**
     * Sets the value of the saleDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSaleDate(String value) {
        this.saleDate = value;
    }

    /**
     * Gets the value of the offerID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfferID() {
        return offerID;
    }

    /**
     * Sets the value of the offerID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfferID(String value) {
        this.offerID = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

}
