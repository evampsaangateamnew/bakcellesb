
package https.wwww_e_gov;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ResponseVoenBySun complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResponseVoenBySun">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SunList" type="{https://wwww.e-gov.az}ArrayOfResponseVoenBySunItems" minOccurs="0"/>
 *         &lt;element name="Status" type="{https://wwww.e-gov.az}ResponseStatus" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseVoenBySun", propOrder = {
    "sunList",
    "status"
})
public class ResponseVoenBySun {

    @XmlElement(name = "SunList")
    protected ArrayOfResponseVoenBySunItems sunList;
    @XmlElement(name = "Status")
    protected ResponseStatus status;

    /**
     * Gets the value of the sunList property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfResponseVoenBySunItems }
     *     
     */
    public ArrayOfResponseVoenBySunItems getSunList() {
        return sunList;
    }

    /**
     * Sets the value of the sunList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfResponseVoenBySunItems }
     *     
     */
    public void setSunList(ArrayOfResponseVoenBySunItems value) {
        this.sunList = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseStatus }
     *     
     */
    public ResponseStatus getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseStatus }
     *     
     */
    public void setStatus(ResponseStatus value) {
        this.status = value;
    }

}
