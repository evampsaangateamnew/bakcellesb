package com.huawei.bss.soaif._interface.ussdgateway;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for BalanceDetail complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="BalanceDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BalanceInstanceID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="InitialAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="EffectiveTime" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime" minOccurs="0"/>
 *         &lt;element name="ExpireTime" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BalanceDetail", propOrder = { "balanceInstanceID", "amount", "initialAmount", "effectiveTime",
		"expireTime" })
public class BalanceDetail {
	@XmlElement(name = "BalanceInstanceID")
	protected Long balanceInstanceID;
	@XmlElement(name = "Amount")
	protected Long amount;
	@XmlElement(name = "InitialAmount")
	protected Long initialAmount;
	@XmlElement(name = "EffectiveTime")
	protected String effectiveTime;
	@XmlElement(name = "ExpireTime")
	protected String expireTime;

	/**
	 * Gets the value of the balanceInstanceID property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getBalanceInstanceID() {
		return balanceInstanceID;
	}

	/**
	 * Sets the value of the balanceInstanceID property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setBalanceInstanceID(Long value) {
		this.balanceInstanceID = value;
	}

	/**
	 * Gets the value of the amount property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getAmount() {
		return amount;
	}

	/**
	 * Sets the value of the amount property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setAmount(Long value) {
		this.amount = value;
	}

	/**
	 * Gets the value of the initialAmount property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getInitialAmount() {
		return initialAmount;
	}

	/**
	 * Sets the value of the initialAmount property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setInitialAmount(Long value) {
		this.initialAmount = value;
	}

	/**
	 * Gets the value of the effectiveTime property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEffectiveTime() {
		return effectiveTime;
	}

	/**
	 * Sets the value of the effectiveTime property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEffectiveTime(String value) {
		this.effectiveTime = value;
	}

	/**
	 * Gets the value of the expireTime property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getExpireTime() {
		return expireTime;
	}

	/**
	 * Sets the value of the expireTime property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setExpireTime(String value) {
		this.expireTime = value;
	}
}
