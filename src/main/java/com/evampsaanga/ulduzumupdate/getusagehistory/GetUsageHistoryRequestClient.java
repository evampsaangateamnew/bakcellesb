package com.evampsaanga.ulduzumupdate.getusagehistory;

import com.evampsaanga.bakcell.requestheaders.BaseRequest;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetUsageHistoryRequestClient extends BaseRequest {
	private String startDate;
	private String endDate;

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	@Override
	public String toString() {
		return "GetUsageHistoryRequestClient [startDate=" + startDate + ", endDate=" + endDate + "]";
	}

}
