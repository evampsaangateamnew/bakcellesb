package com.evampsaanga.bakcell.plasticcard;

import com.evampsaanga.bakcell.responseheaders.BaseResponse;

public class DeleteFastTopupResponse extends BaseResponse{

	@Override
	public String toString() {
		return "DeleteFastTopupResponse [toString()=" + super.toString() + "]";
	}
}
