package com.evampsaanga.magento.tariffdetails.support.updatedv2b2c;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Prepaid extends TarrifDataV2{
	@JsonProperty("Cin")
	private Cin[] Cin;
	
	@JsonProperty("Klass")
	private Klass[] Klass;
	
	@JsonProperty("Cin_new")
	private Klass[] Cin_new;

	@JsonProperty("Cin")
	public Cin[] getCin() {
		return Cin;
	}
	@JsonProperty("Cin")
	public void setCin(Cin[] cin) {
		Cin = cin;
	}
	@JsonProperty("Klass")
	public Klass[] getKlass() {
		return Klass;
	}
	@JsonProperty("Klass")
	public void setKlass(Klass[] klass) {
		Klass = klass;
	}
	@JsonProperty("Cin_new")
	public Klass[] getCin_new() {
		return Cin_new;
	}
	@JsonProperty("Cin_new")
	public void setCin_new(Klass[] cin_new) {
		Cin_new = cin_new;
	}
	
	
}