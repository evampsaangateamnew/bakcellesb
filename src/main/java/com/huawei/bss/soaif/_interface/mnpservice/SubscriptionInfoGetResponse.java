package com.huawei.bss.soaif._interface.mnpservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bss.soaif._interface.common.RspHeader;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.huawei.com/bss/soaif/interface/common/}RspHeader"/>
 *         &lt;element name="Subscription_Info_GetBody" type="{http://www.huawei.com/bss/soaif/interface/MNPService/}Subscription_Info_GetOut" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "rspHeader", "subscriptionInfoGetBody" })
@XmlRootElement(name = "Subscription_Info_GetResponse")
public class SubscriptionInfoGetResponse {
	@XmlElement(name = "RspHeader", namespace = "http://www.huawei.com/bss/soaif/interface/common/", required = true)
	protected RspHeader rspHeader;
	@XmlElement(name = "Subscription_Info_GetBody")
	protected SubscriptionInfoGetOut subscriptionInfoGetBody;

	/**
	 * Gets the value of the rspHeader property.
	 * 
	 * @return possible object is {@link RspHeader }
	 * 
	 */
	public RspHeader getRspHeader() {
		return rspHeader;
	}

	/**
	 * Sets the value of the rspHeader property.
	 * 
	 * @param value
	 *            allowed object is {@link RspHeader }
	 * 
	 */
	public void setRspHeader(RspHeader value) {
		this.rspHeader = value;
	}

	/**
	 * Gets the value of the subscriptionInfoGetBody property.
	 * 
	 * @return possible object is {@link SubscriptionInfoGetOut }
	 * 
	 */
	public SubscriptionInfoGetOut getSubscriptionInfoGetBody() {
		return subscriptionInfoGetBody;
	}

	/**
	 * Sets the value of the subscriptionInfoGetBody property.
	 * 
	 * @param value
	 *            allowed object is {@link SubscriptionInfoGetOut }
	 * 
	 */
	public void setSubscriptionInfoGetBody(SubscriptionInfoGetOut value) {
		this.subscriptionInfoGetBody = value;
	}
}
