package com.evampsaanga.bakcell.queryBalancePICV2;

import com.evampsaanga.bakcell.requestheaders.BaseRequest;

public class QueryBalancePicRequest extends BaseRequest{
	private String customerID;

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

}
