package com.huawei.crm.basetype.ens;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for CallScreenRangeTime complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="CallScreenRangeTime">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CallScreenStartTime" type="{http://crm.huawei.com/basetype/}CallScreenStartTime" minOccurs="0"/>
 *         &lt;element name="CallScreenEndTime" type="{http://crm.huawei.com/basetype/}CallScreenEndTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallScreenRangeTime", propOrder = { "callScreenStartTime", "callScreenEndTime" })
public class CallScreenRangeTime {
	@XmlElement(name = "CallScreenStartTime")
	protected String callScreenStartTime;
	@XmlElement(name = "CallScreenEndTime")
	protected String callScreenEndTime;

	/**
	 * Gets the value of the callScreenStartTime property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCallScreenStartTime() {
		return callScreenStartTime;
	}

	/**
	 * Sets the value of the callScreenStartTime property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCallScreenStartTime(String value) {
		this.callScreenStartTime = value;
	}

	/**
	 * Gets the value of the callScreenEndTime property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCallScreenEndTime() {
		return callScreenEndTime;
	}

	/**
	 * Sets the value of the callScreenEndTime property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCallScreenEndTime(String value) {
		this.callScreenEndTime = value;
	}
}
