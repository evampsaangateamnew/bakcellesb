package com.evampsaanga.bakcell.notificationadmin;

import com.evampsaanga.bakcell.requestheaders.BaseRequest;
import com.fasterxml.jackson.annotation.JsonInclude;

public class SaveNotificationRequest extends BaseRequest {
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String id;
	private String title;
	private String messageAz;
	private String messageEn;
	private String messageRu;
	private String icon;
	private String dateTime;
	private String actionType;
	private String actionId;
	private String buttonText;
	private String subscriberTypes;
	private String tariffType;
	private String confirm;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMessageAz() {
		return messageAz;
	}

	public void setMessageAz(String messageAz) {
		this.messageAz = messageAz;
	}

	public String getMessageEn() {
		return messageEn;
	}

	public void setMessageEn(String messageEn) {
		this.messageEn = messageEn;
	}

	public String getMessageRu() {
		return messageRu;
	}

	public void setMessageRu(String messageRu) {
		this.messageRu = messageRu;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getActionId() {
		return actionId;
	}

	public void setActionId(String actionId) {
		this.actionId = actionId;
	}

	public String getButtonText() {
		return buttonText;
	}

	public void setButtonText(String buttonText) {
		this.buttonText = buttonText;
	}

	public String getSubscriberTypes() {
		return subscriberTypes;
	}

	public void setSubscriberTypes(String subscriberTypes) {
		this.subscriberTypes = subscriberTypes;
	}

	public String getTariffType() {
		return tariffType;
	}

	public void setTariffType(String tariffType) {
		this.tariffType = tariffType;
	}

	public String getConfirm() {
		return confirm;
	}

	public void setConfirm(String confirm) {
		this.confirm = confirm;
	}

	@Override
	public String toString() {
		return "SaveNotificationRequest [id=" + id + ", title=" + title + ", messageAz=" + messageAz + ", messageEn="
				+ messageEn + ", messageRu=" + messageRu + ", icon=" + icon + ", dateTime=" + dateTime + ", actionType="
				+ actionType + ", actionId=" + actionId + ", buttonText=" + buttonText + ", subscriberTypes="
				+ subscriberTypes + ", tariffType=" + tariffType + ", confirm=" + confirm + "]";
	}

}
