package com.evampsaanga.mhmclient;


import com.evampsaanga.developer.utils.SoapHandlerService;

import https.www_e_gov.OperatorService;
import https.www_e_gov.OperatorServiceSoap;

public class MHMService 
{
	private static OperatorServiceSoap operatorService = null;

	private MHMService() 
	{
	}

	public static synchronized OperatorServiceSoap getInstance() 
	{
		if (operatorService == null) {
//			crmPortType = new OrderQuery().getHuaweiCRMPort();
			operatorService = new OperatorService().getOperatorServiceSoap();
			SoapHandlerService.configureBinding(operatorService);
		}
		return operatorService;
	}
	

	
	
}
