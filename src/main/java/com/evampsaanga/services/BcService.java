package com.evampsaanga.services;

import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.developer.utils.SoapHandlerService;
import com.huawei.bme.cbsinterface.bcservices.BcServices;
import com.huawei.bme.cbsinterface.bcservices.BcServices_Service;
import com.huawei.bme.cbsinterface.cbscommon.OperatorInfo;
import com.huawei.bme.cbsinterface.cbscommon.OwnershipInfo;
import com.huawei.bme.cbsinterface.cbscommon.SecurityInfo;

public class BcService {
	private static BcServices portType = null;

	private BcService() {
	}

	public static synchronized BcServices getInstance() {
		if (portType == null) {
			portType = new BcServices_Service().getBcServicesPort();
			SoapHandlerService.configureBinding(portType);
		}
		return portType;
	}

	public static com.huawei.bme.cbsinterface.cbscommon.RequestHeader getRequestHeader() {
		com.huawei.bme.cbsinterface.cbscommon.RequestHeader requestHeader = new com.huawei.bme.cbsinterface.cbscommon.RequestHeader();
		requestHeader.setVersion(Constants.QSL_VERSION);
		requestHeader.setBusinessCode(Constants.QSL_BUSINESS_CODE);
		OwnershipInfo ownershipinf = new OwnershipInfo();
		ownershipinf.setBEID(Constants.QSL_BEID);
		ownershipinf.setBRID(Constants.QSL_BRID);
		requestHeader.setOwnershipInfo(ownershipinf);
		SecurityInfo value = new SecurityInfo();
		value.setLoginSystemCode(ConfigurationManager.getConfigurationFromCache("cbsbb.username"));
		value.setPassword(ConfigurationManager.getConfigurationFromCache("cbsbb.password"));
		requestHeader.setAccessSecurity(value);
		OperatorInfo opInfo = new OperatorInfo();
		opInfo.setOperatorID(Constants.QSL_OPERATOR_ID);
		requestHeader.setOperatorInfo(opInfo);
		requestHeader.setMessageSeq(Helper.generateTransactionID());
		return requestHeader;
	}
}
