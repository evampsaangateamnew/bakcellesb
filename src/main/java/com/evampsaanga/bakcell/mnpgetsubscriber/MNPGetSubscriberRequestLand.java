package com.evampsaanga.bakcell.mnpgetsubscriber;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.amqimplementationsesb.LogsType;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.ThirdPartyNames;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.developer.utils.SOAPLoggingHandler;
import com.evampsaanga.developer.utils.SoapHandlerService;
import com.huawei.bss.soaif._interface.common.ReqHeader;
import com.huawei.bss.soaif._interface.mnpservice.MNPPortType;
import com.huawei.bss.soaif._interface.mnpservice.MNPServices;
import com.huawei.bss.soaif._interface.mnpservice.SubscriptionInfoGetIn;
import com.huawei.bss.soaif._interface.mnpservice.SubscriptionInfoGetRequest;
import com.huawei.bss.soaif._interface.mnpservice.SubscriptionInfoGetResponse;

@Path("/bakcell")
public class MNPGetSubscriberRequestLand {
	public static final Logger logger = Logger.getLogger("bakcellLogs-V2");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public MNPGetSubscriberResponseClient Get(@Header("credentials") String credential, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.MNP_GET_SUBSCRIBER);
		logs.setThirdPartyName(ThirdPartyNames.MNP_GET_SUBSCRIBER);
		logs.setTableType(LogsType.AppFaq);

		MNPGetSubscriberRequestClient cclient = null;
		MNPGetSubscriberResponseClient resp = new MNPGetSubscriberResponseClient();
		try {
			cclient = Helper.JsonToObject(requestBody, MNPGetSubscriberRequestClient.class);
			logger.info("Request Landed on MNPGetSubscriberRequestLand:" + requestBody);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setIsB2B(cclient.getIsB2B());
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		try {
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						resp.setReturnCode(ResponseCodes.ERROR_400);
						resp.setReturnMsg(verification);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					if (cclient.getPassportNumber().equals("")) {
						resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
						resp.setReturnMsg(ResponseCodes.ERROR_400 + " " + "passport id empty");
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
					SubscriptionInfoGetRequest subscriptionInfoGetRequestMsgReq = new SubscriptionInfoGetRequest();
					subscriptionInfoGetRequestMsgReq.setReqHeader(getRequestHeaderForMNP());
					SubscriptionInfoGetIn mnpSubReqMsg = new SubscriptionInfoGetIn();
					mnpSubReqMsg.setSubscriberNumber(cclient.getmsisdn());
					subscriptionInfoGetRequestMsgReq.setSubscriptionInfoGetBody(mnpSubReqMsg);
					if (cclient.getPassportNumber().equals("0000000")) {
						resp.setReturnCode(ResponseCodes.MISSING_PARAMETER_CODE);
						resp.setReturnMsg(ResponseCodes.MISSING_PARAMETER_DES + " " + "In valid passport number");
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
					MNPServices services = new MNPServices();
					MNPPortType port = services.getMNPServicePort();
					SoapHandlerService.configureBinding(port);
					SubscriptionInfoGetResponse res = port.subscriptionInfoGet(subscriptionInfoGetRequestMsgReq);
					if (res.getRspHeader().getReturnCode().equals("0")) {
						if (res.getSubscriptionInfoGetBody().getPINNumber().equalsIgnoreCase(cclient.getPassportNumber())) {
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						} else {
							resp.setReturnCode(ResponseCodes.MISSING_PARAMETER_CODE);
							resp.setReturnMsg("Please Provide valid passport number");
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} else {
						resp.setReturnCode(res.getRspHeader().getReturnCode());
						resp.setReturnMsg(res.getRspHeader().getReturnMsg());
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
			resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
		}
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	public ReqHeader getRequestHeaderForMNP() {
		ReqHeader reqheaderForMNP = new ReqHeader();
		reqheaderForMNP.setAccessUser(ConfigurationManager.getConfigurationFromCache("mnp.sub.AccessUser"));
		reqheaderForMNP.setAccessPwd(ConfigurationManager.getConfigurationFromCache("mnp.sub.AccessPwd"));
		reqheaderForMNP.setChannelId(ConfigurationManager.getConfigurationFromCache("mnp.sub.channelId"));
		reqheaderForMNP.setTransactionId(Helper.generateTransactionID());
		return reqheaderForMNP;
	}
}
