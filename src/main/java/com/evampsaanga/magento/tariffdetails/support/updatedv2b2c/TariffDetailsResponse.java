package com.evampsaanga.magento.tariffdetails.support.updatedv2b2c;

import com.evampsaanga.bakcell.responseheaders.BaseResponse;

public class TariffDetailsResponse extends BaseResponse {
	private com.evampsaanga.magento.tariffdetails.support.updatedv2b2c.Data data = new Data();

	public com.evampsaanga.magento.tariffdetails.support.updatedv2b2c.Data getData() {
		return data;
	}

	public void setData(com.evampsaanga.magento.tariffdetails.support.updatedv2b2c.Data data) {
		this.data = data;
	}
}
