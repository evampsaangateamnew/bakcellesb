package com.evampsaanga.bakcell.queryBalancePICV2;

import com.evampsaanga.bakcell.responseheaders.BaseResponse;

public class QueryBalancePicResponse extends BaseResponse{
	QueryBalancePicResponseData queryBalancePicResponseData;

	public QueryBalancePicResponseData getQueryBalancePicResponseData() {
		return queryBalancePicResponseData;
	}

	public void setQueryBalancePicResponseData(QueryBalancePicResponseData queryBalancePicResponseData) {
		this.queryBalancePicResponseData = queryBalancePicResponseData;
	}
	
}
