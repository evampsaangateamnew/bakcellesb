
package com.huawei.bss.soaif._interface.corporateservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.bss.soaif._interface.common.querycorporate.CorporateCustomer;
import com.huawei.bss.soaif._interface.common.querycorporate.RspHeader;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.huawei.com/bss/soaif/interface/common/}RspHeader"/>
 *         &lt;element name="Customer" type="{http://www.huawei.com/bss/soaif/interface/common/}CorporateCustomer" maxOccurs="100" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "rspHeader",
    "customer"
})
@XmlRootElement(name = "QueryCorporateCustRspMsg")
public class QueryCorporateCustRspMsg {

    @XmlElement(name = "RspHeader", namespace = "http://www.huawei.com/bss/soaif/interface/common/", required = true)
    protected RspHeader rspHeader;
    @XmlElement(name = "Customer")
    protected List<CorporateCustomer> customer;

    /**
     * Gets the value of the rspHeader property.
     * 
     * @return
     *     possible object is
     *     {@link RspHeader }
     *     
     */
    public RspHeader getRspHeader() {
        return rspHeader;
    }

    /**
     * Sets the value of the rspHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link RspHeader }
     *     
     */
    public void setRspHeader(RspHeader value) {
        this.rspHeader = value;
    }

    /**
     * Gets the value of the customer property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the customer property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCustomer().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CorporateCustomer }
     * 
     * 
     */
    public List<CorporateCustomer> getCustomer() {
        if (customer == null) {
            customer = new ArrayList<CorporateCustomer>();
        }
        return this.customer;
    }

}
