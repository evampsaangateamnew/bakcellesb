package com.evampsaanga.magentoservices;

import com.evampsaanga.bakcell.requestheaders.BaseRequest;

public class ContactUsRequest extends BaseRequest {
	String storeId = "";

	/**
	 * @return the storeId
	 */
	public String getStoreId() {
		return storeId;
	}

	/**
	 * @param storeId
	 *            the storeId to set
	 */
	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}
}
