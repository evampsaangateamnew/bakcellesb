package com.nsn.devicemanagement.wsdl.common._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for SettingProvisioning complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="SettingProvisioning">
 *   &lt;complexContent>
 *     &lt;extension base="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}ProvisioningAction">
 *       &lt;sequence>
 *         &lt;element name="subscriptionAware" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SettingProvisioning", propOrder = { "subscriptionAware" })
public class SettingProvisioning extends ProvisioningAction {
	protected boolean subscriptionAware;

	/**
	 * Gets the value of the subscriptionAware property.
	 * 
	 */
	public boolean isSubscriptionAware() {
		return subscriptionAware;
	}

	/**
	 * Sets the value of the subscriptionAware property.
	 * 
	 */
	public void setSubscriptionAware(boolean value) {
		this.subscriptionAware = value;
	}
}
