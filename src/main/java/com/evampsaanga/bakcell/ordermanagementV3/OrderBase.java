package com.evampsaanga.bakcell.ordermanagementV3;

import org.apache.log4j.Logger;

public class OrderBase {
	public static final Logger loggerV2 = Logger.getLogger("bakcellLogs-order");
	
	
	private String orderType;
	private String orderKey;
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public String getOrderKey() {
		return orderKey;
	}
	public void setOrderKey(String orderKey) {
		this.orderKey = orderKey;
	}
}
