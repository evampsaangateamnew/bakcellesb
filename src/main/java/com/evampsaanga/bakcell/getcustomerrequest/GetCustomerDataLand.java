package com.evampsaanga.bakcell.getcustomerrequest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.ws.WebServiceException;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.amqimplementationsesb.LogsType;
import com.evampsaanga.bakcell.hlrweb.getquerybalanceRequest.HLRBalanceServices;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.ThirdPartyNames;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.developer.utils.SOAPLoggingHandler;
import com.evampsaanga.services.CRMServices;
import com.evampsaanga.services.QueryOrderService;
import com.huawei.bss.soaif._interface.common.querycorporate.ServiceNum;
import com.huawei.bss.soaif._interface.corporateservice.QueryCorporateCustReqMsg;
import com.huawei.bss.soaif._interface.corporateservice.QueryCorporateCustRspMsg;
import com.huawei.bss.soaif._interface.hlrwebservice.AcctList;
import com.huawei.bss.soaif._interface.hlrwebservice.AcctList.CorPayRelaList;
import com.huawei.bss.soaif._interface.hlrwebservice.QueryBalanceResponse;
import com.huawei.crm.basetype.RequestHeader;
import com.huawei.crm.query.GetCustomerIn;
import com.huawei.crm.query.GetCustomerRequest;
import com.huawei.crm.query.GetCustomerResponse;
import com.huawei.crm.query.order.query.GetGroupDataIn;
import com.huawei.crm.query.order.query.GetGroupRequest;
import com.huawei.crm.query.order.query.GetGroupResponse;

@Path("/bakcell/")
public class GetCustomerDataLand {
	public static final Logger logger = Logger.getLogger("bakcellLogs-V2");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetCustomerRequestResponse Get(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		Logs logs = new Logs();
		GetCustomerRequestResponse resp = new GetCustomerRequestResponse();
	
		
		try {
			logs.setRequestDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.setTransactionName(Transactions.CUSTOMER_DATA_TRANSACTION_NAME);
			logs.setThirdPartyName(ThirdPartyNames.CUSTOMER_DATA);
			logs.setTableType(LogsType.CustomerData);
			logger.info("Request Landed on getCustomerData:" + requestBody);
			GetCustomerRequestClient cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, GetCustomerRequestClient.class);
				if (cclient != null) 
				{
					if(cclient.getIsB2B()!=null && cclient.getIsB2B().equals("true"))
					{
						logs.setTransactionName(Transactions.CUSTOMER_DATA_TRANSACTION_NAME_B2B);
					}
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setIsB2B(cclient.getIsB2B());
					logger.info("TransactionName: "+logs.getTransactionName());
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					logger.info("Unable to decrypt credentials");
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					logger.info("<<<<<<<< <<<<< credentials = null >>>>> >>>>>>>>");
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						logger.info("<<<<<<<< <<<<< verification = null >>>>> >>>>>>>>");
						GetCustomerRequestResponse res = new GetCustomerRequestResponse();
						res.setReturnCode(ResponseCodes.ERROR_400_CODE);
						res.setReturnMsg(verification);
						logs.setResponseCode(res.getReturnCode());
						logs.setResponseDescription(res.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return res;
					}
				} else {
					logger.info("<<<<<<<< <<<<< ERROR_MSISDN_CODE >>>>> >>>>>>>>");
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					resp = RequestSoap(cclient, logs);
					resp.getCustomerData().setPopupTitle(ConfigurationManager
							.getConfigurationFromCache("predefined.popuptitle." + cclient.getLang()));
					resp.getCustomerData().setPopupContent(ConfigurationManager
							.getConfigurationFromCache("predefined.popupcontent." + cclient.getLang()));
					resp.getCustomerData().setFirstPopup(
							ConfigurationManager.getConfigurationFromCache("predefined.firstpopup"));
					resp.getCustomerData().setLateOnPopup(
							ConfigurationManager.getConfigurationFromCache("predefined.lateronpopup"));
					
					String tariffIdsNumber=ConfigurationManager.getConfigurationFromCache("hidenumbers.forcoreservices");
					ArrayList<String> items = 
							new  ArrayList<String>(Arrays.asList(tariffIdsNumber.split(",")));
					resp.getCustomerData().setHideNumberTariffIds(items);
					logger.info("<<<<<<<< <<<<< Resp >>>>> >>>>>>>>" + resp.toString());
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				} else {
					logger.info("<<<<<<<< <<<<< ERROR_401_CODE >>>>> >>>>>>>>");
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error("Error", ex);
			logger.error(Helper.GetException(ex));
		}
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.updateLog(logs);
		return resp;
	}

	public RequestHeader getRequestHeader() {
		RequestHeader reqh = new RequestHeader();
		String accessUser = ConfigurationManager.getConfigurationFromCache("crm.sub.AccessUser").trim();
		reqh.setChannelId(ConfigurationManager.getConfigurationFromCache("crm.sub.ChannelId").trim());
		reqh.setTechnicalChannelId(ConfigurationManager.getConfigurationFromCache("crm.sub.TechnicalChannelId").trim());
		reqh.setAccessUser(accessUser);
		reqh.setTenantId(ConfigurationManager.getConfigurationFromCache("crm.sub.TenantId").trim());
		reqh.setAccessPwd(ConfigurationManager.getConfigurationFromCache("crm.sub.AccessPwd").trim());
		reqh.setTestFlag(ConfigurationManager.getConfigurationFromCache("crm.sub.TestFlag").trim());
		reqh.setLanguage(ConfigurationManager.getConfigurationFromCache("crm.sub.Language").trim());
		reqh.setTransactionId(Helper.generateTransactionID());
		logger.info("REQUEST HEADER IS >>>>>>>>>>>>>>>>>>> :" + reqh);
		return reqh;
	}

	public GetCustomerRequestResponse RequestSoap(GetCustomerRequestClient cclient, Logs logs) {
		GetCustomerRequestResponse rep11 = new GetCustomerRequestResponse();
		GetCustomerRequest cDR = new GetCustomerRequest();
		cDR.setRequestHeader(getRequestHeader());
		GetCustomerIn gCI = new GetCustomerIn();
//		gCI.setCustomerId(1010000134480l);
		gCI.setServiceNumber(cclient.getmsisdn());
		
		cDR.setGetCustomerBody(gCI);		
		try {
			logger.info("<<<<<<<<<<<<<<<< GetCustomerRequest >>>>>>>>>>>>>>>>" + cDR.toString());
			GetCustomerResponse sR = CRMServices.getInstance().getCustomerData(cDR);
			logger.info("<<<<<<<<<<<<<<<< GetCustomerResponse >>>>>>>>>>>>>>>>" + sR.toString());
			if (sR != null) {
				if (sR.getResponseHeader().getRetCode().equals("0")) {
					com.huawei.crm.query.GetSubscriberResponse respns = new com.evampsaanga.bakcell.getsubscriber.CRMSubscriberService()
							.GetSubscriberRequest(cclient.getmsisdn());
					logger.info("<<<<<<<<<<<<<<<< GetSubscriberResponse >>>>>>>>>>>>>>>>" + respns.toString());
					if (respns.getResponseHeader().getRetCode().equals("0")) {
						CustomerData customer = new CustomerData();
						customer = customer.setResponseBody(sR, respns);
						
						rep11.setCustomerData(customer);
						rep11.getCustomerData().getSubscriberType();
						
						//START Here we are deciding whether we have to show mr for cin , cnenw
						String offferingIDsgunboyCinNew=ConfigurationManager.getConfigurationFromCache("mrc.offering.offeringIds.gunboy.cinew");
						List<String> cinNewGunBoyOfferingIds = new ArrayList<String>(Arrays.asList(offferingIDsgunboyCinNew.split(",")));
						if(cinNewGunBoyOfferingIds.contains(rep11.getCustomerData().getOfferingId()))
						{
						 rep11.getCustomerData().setCinTariff("show");
						}
						//END Here we are deciding whether we have to show mr for cin , cnenw
						if (!rep11.getCustomerData().getSubscriberType().equals(ConfigurationManager.getConfigurationFromCache(ConfigurationManager.MAPPING_HLR_SUBSCRIBER_TYPE + "0"))) {
							HLRBalanceServices bService = new HLRBalanceServices();
						
							
							
							Boolean isCorporate=false;
							String groupType="";
							QueryBalanceResponse queryBalanceResponse=bService.getQueryBalanceResponse(cclient.getmsisdn());
							logger.info("BalanceResponseCode: "+ queryBalanceResponse.getRspHeader().getReturnCode());
							
							if (queryBalanceResponse.getRspHeader().getReturnCode().equals("0")) {
								try {
									logger.info("queryBalanceBody: "+ Helper.ObjectToJson(queryBalanceResponse.getQueryBalanceBody()));
									for (AcctList iterable_element : queryBalanceResponse.getQueryBalanceBody()) {
										if (!iterable_element.getCorPayRelaList().isEmpty()) {
											isCorporate=true;
										}
										
										if(isCorporate){
											for(CorPayRelaList corPayRelaList :iterable_element.getCorPayRelaList()){
												
												
												if(corPayRelaList!=null && corPayRelaList.getPayRelaType().equals("F")){
													groupType="FullPay";
												}else if (corPayRelaList!=null && corPayRelaList.getPayRelaType().equals("P")){
													groupType="PartPay";
												}else{
													groupType="PaybySubs";
												}
											}
											rep11.getCustomerData().setCustomerType(ConfigurationManager.getConfigurationFromCache(ConfigurationManager.MAPPING_CRM_CUSTOMER_TYPE + "1"));
											rep11.getCustomerData().setGroupType(groupType);
										}
									}
								} catch (Exception ex) {
									logger.error(Helper.GetException(ex));
								}
							}
							
							logger.info("isCorporate: "+isCorporate );
							logger.info("groupType: "+groupType );
							logger.info("subscriberType : "+ customer.getSubscriberType());
							
							if (!isCorporate && customer.getSubscriberType().equalsIgnoreCase("postpaid")) {
								//if Api resp is OK,  groupType=PaybySubs
								logger.info("---- Customer is Postpaid and corporate");
								
								QueryCorporateCustReqMsg queryCorporateCustReqMsg = new QueryCorporateCustReqMsg();
									queryCorporateCustReqMsg.setReqHeader(CorporateServiceFactory.getRequestHeader());
							        ServiceNum serviceNum = new ServiceNum();
							        serviceNum.setServiceNum(cclient.getmsisdn());
									queryCorporateCustReqMsg.setMemberServiceNumber(serviceNum );
									QueryCorporateCustRspMsg corportateResp=CorporateServiceFactory.getInstance().queryCorporateCust(queryCorporateCustReqMsg );
									logger.info("CorporateBakcell Response Code & Msg--: "+corportateResp.getRspHeader().getReturnCode()+" : "+ corportateResp.getRspHeader().getReturnMsg());
									if(corportateResp.getRspHeader().getReturnCode().equalsIgnoreCase("0000") && corportateResp.getCustomer() != null && !corportateResp.getCustomer().isEmpty())
									{
										GetGroupRequest getGroupDataRequestMsgReq = new GetGroupRequest();
										getGroupDataRequestMsgReq.setRequestHeader(QueryOrderService.getQueryOderHeader());
										GetGroupDataIn getGroupDataIn = new GetGroupDataIn();
										//getGroupDataIn.setCustomerId(getCustomerResponse.getGetCustomerBody().getCustomerId());
										getGroupDataIn.setServiceNumber(cclient.getmsisdn());
										getGroupDataRequestMsgReq.setGetGroupBody(getGroupDataIn);
										
										
										GetGroupResponse respQueryOrder=QueryOrderService.getInstance().getGroupData(getGroupDataRequestMsgReq );QueryOrderService.getInstance().getGroupData(getGroupDataRequestMsgReq);
										logger.info("ResultCode CrmQueryOrder: "+respQueryOrder.getResponseHeader().getRetCode());
										logger.info("ResultMessage CrmQueryOrder: "+respQueryOrder.getResponseHeader().getRetMsg());
										if(respQueryOrder.getResponseHeader().getRetCode().equalsIgnoreCase("0"))
										{
										
											for(int i=0; i<respQueryOrder.getGetGroupBody().getGetGroupDataList().size();i++)
											{
											  if(respQueryOrder.getGetGroupBody().getGetGroupDataList().get(i).getGroupName().toLowerCase().contains("paybysubs"))
											  {
												  rep11.getCustomerData().setCustomerType(ConfigurationManager.getConfigurationFromCache(ConfigurationManager.MAPPING_CRM_CUSTOMER_TYPE + "1"));
												  rep11.getCustomerData().setGroupType("PaybySubs");
												  break;
											  }
											}
										}
										if(rep11.getCustomerData().getGroupType() == null)
										{
											rep11.getCustomerData().setCustomerType(ConfigurationManager.getConfigurationFromCache(ConfigurationManager.MAPPING_CRM_CUSTOMER_TYPE + "0"));
											rep11.getCustomerData().setGroupType("");
										}
									}
									else
									{
										rep11.getCustomerData().setCustomerType(ConfigurationManager.getConfigurationFromCache(ConfigurationManager.MAPPING_CRM_CUSTOMER_TYPE + "0"));
										rep11.getCustomerData().setGroupType(groupType);
									}
								
							}
							
						}
						
						logs.setUserType(rep11.getCustomerData().getSubscriberType());
						logs.setTariffId(rep11.getCustomerData().getOfferingId());
						rep11.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						rep11.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						return rep11;
					} else {
						rep11.setReturnCode(respns.getResponseHeader().getRetCode());
						rep11.setReturnMsg(respns.getResponseHeader().getRetMsg());
						return rep11;
					}
				} else if (sR.getResponseHeader().getRetCode().equals(ResponseCodes.MSISDN_NOT_FOUND_BK_CODE)) {
					rep11.setReturnCode(ResponseCodes.MSISDN_NOT_FOUND_BK_CODE);
					rep11.setReturnMsg(ResponseCodes.MSISDN_NOT_FOUND_BK_DES);
				} else {
					rep11.setReturnCode(sR.getResponseHeader().getRetCode());
					rep11.setReturnMsg(sR.getResponseHeader().getRetMsg());
				}
			}
		} catch (Exception ee) {
			if (ee instanceof WebServiceException) {
				rep11 = new GetCustomerRequestResponse();
				rep11.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
				rep11.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			} else {
				rep11 = new GetCustomerRequestResponse();
				rep11.setReturnCode(ResponseCodes.SUCESS_CODE_200);
				rep11.setReturnMsg(ResponseCodes.SUCESS_DES_200);
			}
			logger.error(Helper.GetException(ee));
		}
		return rep11;
	}
	public static void main(String[] args) {
		String name="PIC_E-Care(Paybysubs)_Test";
		String name1 = name.toLowerCase();
		System.out.println(name1);
		if(name1.contains("paybysubs"))
		{
			System.out.println("YES Contains");
		}
		else
		{
			System.out.println("NO");
		}
		
	}
}
