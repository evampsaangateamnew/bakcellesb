package com.evampsaanga.bakcell.hlrweb.getquerybalanceRequest;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.gethomepage.Balance;
import com.evampsaanga.gethomepage.BounusWallet;
import com.evampsaanga.gethomepage.CountryWideWallet;
import com.evampsaanga.gethomepage.MainWallet;
import com.evampsaanga.gethomepage.Postpaid;
import com.evampsaanga.gethomepage.Prepaid;
import com.huawei.bss.soaif._interface.common.ReqHeader;
import com.huawei.bss.soaif._interface.hlrwebservice.AccountCredit;
import com.huawei.bss.soaif._interface.hlrwebservice.AcctBalance;
import com.huawei.bss.soaif._interface.hlrwebservice.AcctList;
import com.huawei.bss.soaif._interface.hlrwebservice.AcctList.CorPayRelaList;
import com.huawei.bss.soaif._interface.hlrwebservice.AcctList.CorpReserAmountList;
import com.huawei.bss.soaif._interface.hlrwebservice.AcctList.IndvReserAmountList;
import com.huawei.bss.soaif._interface.hlrwebservice.CreditAmountInfo;
import com.huawei.bss.soaif._interface.hlrwebservice.HLRWebService;
import com.huawei.bss.soaif._interface.hlrwebservice.OutStandingList;
import com.huawei.bss.soaif._interface.hlrwebservice.QueryBalanceIn;
import com.huawei.bss.soaif._interface.hlrwebservice.QueryBalanceRequest;
import com.huawei.bss.soaif._interface.hlrwebservice.QueryBalanceResponse;

public class HLRBalanceServices {
	public static final Logger logger = Logger.getLogger("bakcellLogs-V2");

	private static Balance processBalanceResponse(String msisdn, String userType, List<AcctList> balanceResponse) {
		Balance finalBalanceResponse = new Balance();
		Postpaid postPaidBalanceData = new Postpaid();
		if (userType.equals(Constants.POSTPAID)) {
			for (AcctList acctList : balanceResponse) {
				long individualInitialCreditLimit = 0L; // remaining
				boolean individualInitialCreditLimitFlag = false;
				long initialCreditLimit = 0L;
				long temproryCreditLimit = 0L;
				long advancePayment = 0L;
				long individualReserveAmount = 0L;
				long previousMonthIndividualUnbilledAmount = 0L;
				long currentMonthIndividualUnbilledAmount = 0L;
				long outStandingIndividualAmount = 0L;// document 5 outstanding
														// Individual// Debt 1
				boolean outStandingIndividualAmountFlag = false;
				long minumumAmountToBePaid = 0;
				long availableIndividualCreditAmount = 0;// document 7
															// Pointnumber 7
				boolean availableIndividualCreditAmountFlag = false;
				long corporateUnBilledAmount = 0;// Unbilled Corporate balance
				boolean corporateUnBilledAmountFlag = false; // point number 2
				long corporatePaymentRelation = 0;// document 10 point number 3
				boolean corporatePaymentRelationFlag = false; // corporate
																// current
																// credit
				long availableCorporateCreditAmount = 0;// document 13 point
														// number 4
				boolean availableCorporateCreditAmountFlag = false;
				long currentBalance = 0;// document 9 Point number 5
				boolean currentBalanceFlag = false;
				try {
					for (CorPayRelaList corporatePaymentRelationObject : acctList.getCorPayRelaList()) {
						logger.info("Corporate Payment Relation running in loop");
						if (corporatePaymentRelationObject.getPayRelaType().equals("P")) {
							logger.info("Corporate Payment Relation present with P");
							postPaidBalanceData.setTemplate(3);
							try {
								corporatePaymentRelation = corporatePaymentRelation
										+ Long.parseLong(corporatePaymentRelationObject.getCorPayRelaAmount());
								corporatePaymentRelationFlag = true;
							} catch (Exception ex) {
								logger.error(Helper.GetException(ex));
							}
						} else if (corporatePaymentRelationObject.getPayRelaType().equals("F")) {
							logger.info("Corporate Payment Relation present with F");
							postPaidBalanceData.setTemplate(2);
							try {
								corporatePaymentRelation = corporatePaymentRelation
										+ Long.parseLong(corporatePaymentRelationObject.getCorPayRelaAmount());
								corporatePaymentRelationFlag = true;
							} catch (Exception ex) {
								logger.error(Helper.GetException(ex));
							}
						}
					}
				} catch (Exception ex) {
					logger.error(Helper.GetException(ex));
				}
				if (acctList.getCorPayRelaList().isEmpty()) {
					corporatePaymentRelationFlag = false;
					logger.info("corporatePaymentRelation List empty");
				}
				try {
					corporateUnBilledAmount = Long.parseLong(acctList.getCorpUnbilledAmount());
					if (acctList.getCorpUnbilledAmount().equals("0")) {
						corporateUnBilledAmountFlag = false;
					} else {
						corporateUnBilledAmountFlag = true;
					}
				} catch (Exception ex) {
					logger.error(Helper.GetException(ex));
				}
				for (CorpReserAmountList corportateReservedAmount : acctList.getCorpReserAmountList()) {
					try {
						corporateUnBilledAmount = corporateUnBilledAmount
								+ Long.parseLong(corportateReservedAmount.getCorpReserAmount());
						if (corportateReservedAmount.getCorpReserAmount().equals("0")) {
							corporateUnBilledAmountFlag = false;
						} else {
							corporateUnBilledAmountFlag = true;
						}
					} catch (Exception ex) {
						logger.error(Helper.GetException(ex));
					}
				}
				for (AccountCredit acountCredit : acctList.getAccountCredit()) {
					for (CreditAmountInfo creditAmountInfo : acountCredit.getCreditAmountInfo()) {
						if (creditAmountInfo.getLimitClass().equals("I")) {
							try {
								if (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(creditAmountInfo.getExpireTime())
										.before(new Date())) {
									continue;
								}
							} catch (Exception ex) {
								logger.error(Helper.GetException(ex));
							}
							try {
								initialCreditLimit = initialCreditLimit + Long.parseLong(creditAmountInfo.getAmount());
							} catch (Exception ex) {
								logger.error(Helper.GetException(ex));
							}
						} else if (creditAmountInfo.getLimitClass().equals("T")) {
							try {
								temproryCreditLimit = temproryCreditLimit
										+ Long.parseLong(creditAmountInfo.getAmount());
							} catch (Exception ex) {
								logger.error(Helper.GetException(ex));
							}
						}
					}
					try {
						availableIndividualCreditAmount = availableIndividualCreditAmount
								+ Long.parseLong(acountCredit.getTotalRemainAmount());
						availableIndividualCreditAmountFlag = true;
					} catch (Exception ex) {
						logger.error(Helper.GetException(ex));
					}
				}
				try {
					previousMonthIndividualUnbilledAmount = previousMonthIndividualUnbilledAmount
							+ Long.parseLong(acctList.getPreMonIndvUnbilledAmount());
				} catch (Exception ex) {
					logger.error(Helper.GetException(ex));
				}
				try {
					currentMonthIndividualUnbilledAmount = currentMonthIndividualUnbilledAmount
							+ Long.parseLong(acctList.getCurMonIndvUnbilledAmount());
				} catch (Exception ex) {
					logger.error(Helper.GetException(ex));
				}
				for (IndvReserAmountList indRAmount : acctList.getIndvReserAmountList()) {
					try {
						individualReserveAmount = individualReserveAmount
								+ Long.parseLong(indRAmount.getIndvReserAmount());
					} catch (Exception ex) {
						logger.error(Helper.GetException(ex));
					}
				}
				for (OutStandingList OutindRAmount : acctList.getOutStandingList()) {
					try {
						outStandingIndividualAmount = outStandingIndividualAmount
								+ Long.parseLong(OutindRAmount.getOutStandingDetail().getOutStandingAmount());
						outStandingIndividualAmountFlag = true;
					} catch (Exception ex) {
						logger.error(Helper.GetException(ex));
					}
					Date dueDate = null;
					if (OutindRAmount.getDueDate().substring(8, OutindRAmount.getDueDate().length()).equals("000000")) {
						try {
							dueDate = new SimpleDateFormat("yyyyMMdd")
									.parse(OutindRAmount.getDueDate().substring(0, 8));
						} catch (Exception ex) {
							logger.error(Helper.GetException(ex));
						}
					}
					if (dueDate != null) {
						int a = new Date().compareTo(dueDate);
						if (a >= 0) {
							try {
								minumumAmountToBePaid = minumumAmountToBePaid
										+ Long.parseLong(OutindRAmount.getOutStandingDetail().getOutStandingAmount());
							} catch (Exception ex) {
								logger.error(Helper.GetException(ex));
							}
						}
					}
				}
				List<AcctBalance> balanceResults = acctList.getBalanceResult();
				for (AcctBalance acctBalance : balanceResults) {
					if (acctBalance.getBalanceType().equals("C_MAIN_BILLING_ACCOUNT")) {
						try {
							advancePayment = Long.parseLong(acctBalance.getTotalAmount()) + advancePayment;
						} catch (Exception ex) {
							logger.error(Helper.GetException(ex));
						}
					}
				}
				try {
					availableCorporateCreditAmount = corporatePaymentRelation - corporateUnBilledAmount;
					if (corporatePaymentRelationFlag) {
						availableCorporateCreditAmountFlag = true;
					}
				} catch (Exception ex) {
					logger.error("Exception availableCorporateCreditAmount" + Helper.GetException(ex));
				}
				if (advancePayment < previousMonthIndividualUnbilledAmount) {
					currentBalance = -1 * currentMonthIndividualUnbilledAmount;
					currentBalanceFlag = true;
				} else {
					currentBalance = advancePayment - previousMonthIndividualUnbilledAmount
							- currentMonthIndividualUnbilledAmount;
					currentBalanceFlag = true;
				}
				individualInitialCreditLimit = initialCreditLimit;
				individualInitialCreditLimitFlag = true;
				postPaidBalanceData = new Postpaid(availableCorporateCreditAmount + "",
						availableIndividualCreditAmount + "", corporateUnBilledAmount + "", currentBalance + "",
						corporatePaymentRelation + "", individualInitialCreditLimit + "",
						outStandingIndividualAmount + "", postPaidBalanceData.getTemplate());
				if (!corporatePaymentRelationFlag)
					postPaidBalanceData.setTemplate(1);
				logger.info("Template:" + postPaidBalanceData.getTemplate()
						+ " \nFlags=availableCorporateCreditAmountFlag:" + availableCorporateCreditAmountFlag
						+ ",availableCorporateCreditAmountFlag:" + corporatePaymentRelationFlag
						+ ",corporateUnBilledAmountFlag:" + corporateUnBilledAmountFlag
						+ ",outStandingIndividualAmountFlag:" + outStandingIndividualAmountFlag + ",currentBalanceFlag:"
						+ currentBalanceFlag + ",individualInitialCreditLimitFlag:" + individualInitialCreditLimitFlag
						+ ",availableIndividualCreditAmountFlag:" + availableIndividualCreditAmountFlag);
				break;
			}
			NumberFormat formatter = new DecimalFormat("#0.00");
			formatter.setRoundingMode(RoundingMode.FLOOR);
			postPaidBalanceData.setAvailableBalanceCorporateValue(Helper.getBakcellMoneyCeilingMode(
					Long.parseLong(postPaidBalanceData.getAvailableBalanceCorporateValue())));
			postPaidBalanceData.setAvailableBalanceIndividualValue(Helper.getBakcellMoneyCeilingMode(
					Long.parseLong(postPaidBalanceData.getAvailableBalanceIndividualValue())));
			postPaidBalanceData.setBalanceCorporateValue(
					Helper.getBakcellMoneyCeilingMode(Long.parseLong(postPaidBalanceData.getBalanceCorporateValue())));
			postPaidBalanceData.setBalanceIndividualValue(
					Helper.getBakcellMoneyCeilingMode(Long.parseLong(postPaidBalanceData.getBalanceIndividualValue())));
			postPaidBalanceData.setCurrentCreditCorporateValue(Helper
					.getBakcellMoneyCeilingMode(Long.parseLong(postPaidBalanceData.getCurrentCreditCorporateValue())));
			postPaidBalanceData.setCurrentCreditIndividualValue(Helper
					.getBakcellMoneyCeilingMode(Long.parseLong(postPaidBalanceData.getCurrentCreditIndividualValue())));
			if (Double.parseDouble(postPaidBalanceData.getOutstandingIndividualDept()) > 0.00) {
				postPaidBalanceData.setOutstandingIndividualDept("-" + Helper.getBakcellMoneyCeilingMode(
						Long.parseLong(postPaidBalanceData.getOutstandingIndividualDept())));
			} else {
				postPaidBalanceData.setOutstandingIndividualDept(Helper.getBakcellMoneyCeilingMode(
						Long.parseLong(postPaidBalanceData.getOutstandingIndividualDept())));
			}
			if (Double.parseDouble(postPaidBalanceData.getBalanceCorporateValue()) > 0.00)
				postPaidBalanceData.setBalanceCorporateValue("-" + postPaidBalanceData.getBalanceCorporateValue());
			finalBalanceResponse.setPostpaid(postPaidBalanceData);
		} else if (userType.equals(Constants.PREPAID)) {
			Prepaid prepaidBalanceData = new Prepaid();
			String lowerLimit = ConfigurationManager
					.getConfigurationFromCache(ConfigurationManager.MAPPING_BALANCE_LOWER_LIMIT);
			for (AcctList acctList : balanceResponse) {
				List<AcctBalance> balanceResults = acctList.getBalanceResult();
				for (AcctBalance acctBalance : balanceResults) {
					try {
						logger.info("AccountType----------------  " + acctBalance.getBalanceType());
						if (!ConfigurationManager.getContainsValueByKey(
								ConfigurationManager.MAPPING_BALANCE_MAIN + acctBalance.getBalanceType())) {
							continue;
						}
						String array[] = ConfigurationManager
								.getConfigurationFromCache(
										ConfigurationManager.MAPPING_BALANCE_MAIN + acctBalance.getBalanceType())
								.split(",");
						String name = array[0];
						NumberFormat df = new DecimalFormat("#0.00");
						df.setRoundingMode(RoundingMode.FLOOR);
						if (name.equals(Constants.BONUS_WALLET))
							prepaidBalanceData.setBounusWallet(new BounusWallet(name,
									ConfigurationManager.getConfigurationFromCache(acctBalance.getCurrencyID()),
									"" + df.format((Double.parseDouble(acctBalance.getTotalAmount())
											/ Constants.MONEY_DIVIDEND)),
									acctBalance.getBalanceDetail().getEffectiveTime(),
									acctBalance.getBalanceDetail().getExpireTime(), lowerLimit));
						else if (name.equals(Constants.COUNTRY_WIDE))
							prepaidBalanceData.setCountryWideWallet(new CountryWideWallet(name,
									ConfigurationManager.getConfigurationFromCache(acctBalance.getCurrencyID()),
									"" + df.format((Double.parseDouble(acctBalance.getTotalAmount())
											/ Constants.MONEY_DIVIDEND)),
									acctBalance.getBalanceDetail().getEffectiveTime(),
									acctBalance.getBalanceDetail().getExpireTime(), lowerLimit));
						else if (name.equals(Constants.MAIN_WALLET))
							prepaidBalanceData.setMainWallet(new MainWallet(name,
									ConfigurationManager.getConfigurationFromCache(acctBalance.getCurrencyID()),
									Helper.getBakcellMoney((Long.parseLong(acctBalance.getTotalAmount()))),
									acctBalance.getBalanceDetail().getEffectiveTime(),
									acctBalance.getBalanceDetail().getExpireTime(), lowerLimit));
					} catch (Exception ex) {
						logger.error(Helper.GetException(ex));
					}
				}
			}
			finalBalanceResponse.setPrepaid(prepaidBalanceData);
		}
		return finalBalanceResponse;
	}

	public boolean isCorporate(String msisdn) {
		QueryBalanceRequest qBR = new QueryBalanceRequest();
		qBR.setReqHeader(getReqHeaderForQueryBalanceHLR());
		QueryBalanceIn qBI = new QueryBalanceIn();
		qBI.setPrimaryIdentity(msisdn);
		qBR.setQueryBalanceBody(qBI);
		QueryBalanceResponse res = HLRWebService.getInstance().queryBalance(qBR);
		if (res.getRspHeader().getReturnCode().equals("0")) {
			try {
				for (AcctList iterable_element : res.getQueryBalanceBody()) {
					if (!iterable_element.getCorPayRelaList().isEmpty()) {
						return true;
					}
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
			}
		}
		return false;
	}
	
	
	public QueryBalanceResponse getQueryBalanceResponse(String msisdn) {
		QueryBalanceRequest qBR = new QueryBalanceRequest();
		qBR.setReqHeader(getReqHeaderForQueryBalanceHLR());
		QueryBalanceIn qBI = new QueryBalanceIn();
		qBI.setPrimaryIdentity(msisdn);
		qBR.setQueryBalanceBody(qBI);
		QueryBalanceResponse res = HLRWebService.getInstance().queryBalance(qBR);
		/*if (res.getRspHeader().getReturnCode().equals("0")) {
			try {
				for (AcctList iterable_element : res.getQueryBalanceBody()) {
					if (!iterable_element.getCorPayRelaList().isEmpty()) {
						return true;
					}
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
			}
		}
		return false;*/
		return res;
		
		
	}
	

	public Balance getBalance(String msisdn, String userType,QueryBalanceResponse balanceResponse) {
		logger.info("MSISDN IS:" + msisdn + " User type is: " + userType);
		if(balanceResponse==null)
		{
			QueryBalanceRequest qBR = new QueryBalanceRequest();
			qBR.setReqHeader(getReqHeaderForQueryBalanceHLR());
			QueryBalanceIn qBI = new QueryBalanceIn();
			qBI.setPrimaryIdentity(msisdn);
			qBR.setQueryBalanceBody(qBI);
			QueryBalanceResponse res = HLRWebService.getInstance().queryBalance(qBR);
			balanceResponse = res;
		}
		QueryBalanceResponse res = balanceResponse;
		Balance finalBalance = new Balance();
		if (res.getRspHeader().getReturnCode().equals("0")) {
			finalBalance = processBalanceResponse(msisdn, userType, res.getQueryBalanceBody());
		}
		finalBalance.setReturnCode(res.getRspHeader().getReturnCode());
		finalBalance.setReturnMsg(res.getRspHeader().getReturnMsg());
		return finalBalance;
	}

	private ReqHeader getReqHeaderForQueryBalanceHLR() {
		ReqHeader reqHForQBalance = new ReqHeader();
		reqHForQBalance.setChannelId(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.ChannelId").trim());
		reqHForQBalance.setAccessUser(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.AccessUser").trim());
		reqHForQBalance.setAccessPwd(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.AccessPwd").trim());
		reqHForQBalance
				.setMIDWAREChannelID(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.MIDWAREChannelID").trim());
		reqHForQBalance
				.setMIDWAREAccessPwd(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.MIDWAREAccessPwd").trim());
		reqHForQBalance
				.setMIDWAREAccessUser(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.MIDWAREAccessUser").trim());
		reqHForQBalance.setTransactionId(Helper.generateTransactionID());
		return reqHForQBalance;
	}

}
