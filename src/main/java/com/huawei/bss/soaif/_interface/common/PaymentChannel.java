package com.huawei.bss.soaif._interface.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for PaymentChannel complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentChannel">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PaymentId" type="{http://www.huawei.com/bss/soaif/interface/common/}PaymentId" minOccurs="0"/>
 *         &lt;element name="AcctType">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="A"/>
 *               &lt;enumeration value="C"/>
 *               &lt;enumeration value="D"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AcctNum">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="60"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AcctName" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="120"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BankCode">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="32"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BankBranchCode" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="32"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CreditCardType" type="{http://www.huawei.com/bss/soaif/interface/common/}CreditCardType" minOccurs="0"/>
 *         &lt;element name="CVVNum" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="32"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ExpireDate" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentChannel", propOrder = { "paymentId", "acctType", "acctNum", "acctName", "bankCode",
		"bankBranchCode", "creditCardType", "cvvNum", "expireDate" })
@XmlSeeAlso({ com.huawei.bss.soaif._interface.subscriberservice.SupplementCustInfoReqMsg.Account.PaymentChannel.class })
public class PaymentChannel {
	@XmlElement(name = "PaymentId")
	protected String paymentId;
	@XmlElement(name = "AcctType", required = true)
	protected String acctType;
	@XmlElement(name = "AcctNum", required = true)
	protected String acctNum;
	@XmlElement(name = "AcctName")
	protected String acctName;
	@XmlElement(name = "BankCode", required = true)
	protected String bankCode;
	@XmlElement(name = "BankBranchCode")
	protected String bankBranchCode;
	@XmlElement(name = "CreditCardType")
	protected String creditCardType;
	@XmlElement(name = "CVVNum")
	protected String cvvNum;
	@XmlElement(name = "ExpireDate")
	protected String expireDate;

	/**
	 * Gets the value of the paymentId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaymentId() {
		return paymentId;
	}

	/**
	 * Sets the value of the paymentId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPaymentId(String value) {
		this.paymentId = value;
	}

	/**
	 * Gets the value of the acctType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctType() {
		return acctType;
	}

	/**
	 * Sets the value of the acctType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctType(String value) {
		this.acctType = value;
	}

	/**
	 * Gets the value of the acctNum property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctNum() {
		return acctNum;
	}

	/**
	 * Sets the value of the acctNum property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctNum(String value) {
		this.acctNum = value;
	}

	/**
	 * Gets the value of the acctName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctName() {
		return acctName;
	}

	/**
	 * Sets the value of the acctName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctName(String value) {
		this.acctName = value;
	}

	/**
	 * Gets the value of the bankCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBankCode() {
		return bankCode;
	}

	/**
	 * Sets the value of the bankCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBankCode(String value) {
		this.bankCode = value;
	}

	/**
	 * Gets the value of the bankBranchCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBankBranchCode() {
		return bankBranchCode;
	}

	/**
	 * Sets the value of the bankBranchCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBankBranchCode(String value) {
		this.bankBranchCode = value;
	}

	/**
	 * Gets the value of the creditCardType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCreditCardType() {
		return creditCardType;
	}

	/**
	 * Sets the value of the creditCardType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCreditCardType(String value) {
		this.creditCardType = value;
	}

	/**
	 * Gets the value of the cvvNum property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCVVNum() {
		return cvvNum;
	}

	/**
	 * Sets the value of the cvvNum property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCVVNum(String value) {
		this.cvvNum = value;
	}

	/**
	 * Gets the value of the expireDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getExpireDate() {
		return expireDate;
	}

	/**
	 * Sets the value of the expireDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setExpireDate(String value) {
		this.expireDate = value;
	}
}
