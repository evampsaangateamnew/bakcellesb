
package net.x_rd.az.voen9900037691.producer;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import https.wwww_e_gov.RequestApplication;


/**
 * <p>Java class for ArrayOfRequestApplication complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfRequestApplication">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequestApplication" type="{https://wwww.e-gov.az}RequestApplication" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfRequestApplication", propOrder = {
    "requestApplication"
})
public class ArrayOfRequestApplication {

    @XmlElement(name = "RequestApplication", nillable = true)
    protected List<RequestApplication> requestApplication;

    /**
     * Gets the value of the requestApplication property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the requestApplication property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRequestApplication().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RequestApplication }
     * 
     * 
     */
    public List<RequestApplication> getRequestApplication() {
        if (requestApplication == null) {
            requestApplication = new ArrayList<RequestApplication>();
        }
        return this.requestApplication;
    }

}
