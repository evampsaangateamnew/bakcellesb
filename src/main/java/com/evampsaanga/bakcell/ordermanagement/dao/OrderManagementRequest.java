
package com.evampsaanga.bakcell.ordermanagement.dao;

import java.util.List;

import com.evampsaanga.bakcell.requestheaders.BaseRequest;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "actPrice",
    "msgLang",
    "orderKey",
    "recieverMsisdn",
    "destinationTariff",
    "tariffPermissions",
    "textmsg",
    "senderName",
    "amount",
    "offeringId",
    "number",
    "actionType",
    "companyValue",
    "totalLimit",
    "orderId",
    "groupTypeTo",
    "groupIdTo",
    "accountId",
    "balance",
    "lowerLimit",
    "maxLimit",
    "totalUsersbyGroup",
    "customerId",
"transactionId",
"iccid",
"contactNumber"
    
})
public class OrderManagementRequest extends BaseRequest {

    @JsonProperty("actPrice")
    private String actPrice;
    @JsonProperty("msgLang")
    private String msgLang;
    @JsonProperty("orderKey")
    private String orderKey;
    @JsonProperty("recieverMsisdn")
    private List<RecieverMsisdn> recieverMsisdn = null;
    @JsonProperty("destinationTariff")
    private String destinationTariff;
    @JsonProperty("tariffPermissions")
    private String tariffPermissions;
    @JsonProperty("textmsg")
    private String textmsg;
    @JsonProperty("senderName")
    private String senderName;
    @JsonProperty("amount")
    private String amount;
    @JsonProperty("offeringId")
    private String offeringId;
    @JsonProperty("number")
    private String number;
    @JsonProperty("actionType")
    private String actionType;
    @JsonProperty("companyValue")
    private String companyValue;
    @JsonProperty("totalLimit")
    private String totalLimit;
    @JsonProperty("orderId")
    private String orderId;
    @JsonProperty("groupTypeTo")
    private String groupTypeTo;
    @JsonProperty("groupIdTo")
    private String groupIdTo;
    @JsonProperty("accountId")
    private String accountId;
    @JsonProperty("balance")
    private String balance;
    @JsonProperty("totalUsersbyGroup")
    private String totalUsersbyGroup;
    @JsonProperty("customerId")
    private String customerId;
    @JsonProperty("transactionId")
    private String transactionId;
    @JsonProperty("iccid")
    private String iccid;
	@JsonProperty("contactNumber")
	private String contactNumber;
    
    
    
    
    public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getTotalUsersbyGroup() {
		return totalUsersbyGroup;
	}

	public void setTotalUsersbyGroup(String totalUsersbyGroup) {
		this.totalUsersbyGroup = totalUsersbyGroup;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getLowerLimit() {
		return lowerLimit;
	}

	public void setLowerLimit(String lowerLimit) {
		this.lowerLimit = lowerLimit;
	}

	public String getMaxLimit() {
		return maxLimit;
	}

	public void setMaxLimit(String maxLimit) {
		this.maxLimit = maxLimit;
	}

	@JsonProperty("lowerLimit")
    private String lowerLimit;
    @JsonProperty("maxLimit")
    private String maxLimit;
    
    @JsonProperty("actPrice")
    public String getActPrice() {
        return actPrice;
    }

    @JsonProperty("actPrice")
    public void setActPrice(String actPrice) {
        this.actPrice = actPrice;
    }

    @JsonProperty("msgLang")
    public String getMsgLang() {
        return msgLang;
    }

    @JsonProperty("msgLang")
    public void setMsgLang(String msgLang) {
        this.msgLang = msgLang;
    }

    @JsonProperty("orderKey")
    public String getOrderKey() {
        return orderKey;
    }

    @JsonProperty("orderKey")
    public void setOrderKey(String orderKey) {
        this.orderKey = orderKey;
    }

    @JsonProperty("recieverMsisdn")
    public List<RecieverMsisdn> getRecieverMsisdn() {
        return recieverMsisdn;
    }

    @JsonProperty("recieverMsisdn")
    public void setRecieverMsisdn(List<RecieverMsisdn> recieverMsisdn) {
        this.recieverMsisdn = recieverMsisdn;
    }

    @JsonProperty("destinationTariff")
    public String getDestinationTariff() {
        return destinationTariff;
    }

    @JsonProperty("destinationTariff")
    public void setDestinationTariff(String destinationTariff) {
        this.destinationTariff = destinationTariff;
    }

    @JsonProperty("tariffPermissions")
    public String getTariffPermissions() {
        return tariffPermissions;
    }

    @JsonProperty("tariffPermissions")
    public void setTariffPermissions(String tariffPermissions) {
        this.tariffPermissions = tariffPermissions;
    }

    @JsonProperty("textmsg")
    public String getTextmsg() {
        return textmsg;
    }

    @JsonProperty("textmsg")
    public void setTextmsg(String textmsg) {
        this.textmsg = textmsg;
    }

    @JsonProperty("senderName")
    public String getSenderName() {
        return senderName;
    }

    @JsonProperty("senderName")
    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    @JsonProperty("amount")
    public String getAmount() {
        return amount;
    }

    @JsonProperty("amount")
    public void setAmount(String amount) {
        this.amount = amount;
    }

    @JsonProperty("offeringId")
    public String getOfferingId() {
        return offeringId;
    }

    @JsonProperty("offeringId")
    public void setOfferingId(String offeringId) {
        this.offeringId = offeringId;
    }

    @JsonProperty("number")
    public String getNumber() {
        return number;
    }

    @JsonProperty("number")
    public void setNumber(String number) {
        this.number = number;
    }

    @JsonProperty("actionType")
    public String getActionType() {
        return actionType;
    }

    @JsonProperty("actionType")
    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    @JsonProperty("companyValue")
    public String getCompanyValue() {
        return companyValue;
    }

    @JsonProperty("companyValue")
    public void setCompanyValue(String companyValue) {
        this.companyValue = companyValue;
    }

    @JsonProperty("totalLimit")
    public String getTotalLimit() {
        return totalLimit;
    }

    @JsonProperty("totalLimit")
    public void setTotalLimit(String totalLimit) {
        this.totalLimit = totalLimit;
    }

    @JsonProperty("orderId")
    public String getOrderId() {
        return orderId;
    }

    @JsonProperty("orderId")
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @JsonProperty("groupTypeTo")
    public String getGroupTypeTo() {
        return groupTypeTo;
    }

    @JsonProperty("groupTypeTo")
    public void setGroupTypeTo(String groupTypeTo) {
        this.groupTypeTo = groupTypeTo;
    }

    @JsonProperty("groupIdTo")
    public String getGroupIdTo() {
        return groupIdTo;
    }

    @JsonProperty("accountId")
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }
    @JsonProperty("accountId")
    public String getAccountId() {
        return accountId;
    }

    @JsonProperty("groupIdTo")
    public void setGroupIdTo(String groupIdTo) {
        this.groupIdTo = groupIdTo;
    }

   

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getIccid() {
		return iccid;
	}

	public void setIccid(String iccid) {
		this.iccid = iccid;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	@Override
	public String toString() {
		return "OrderManagementRequest [actPrice=" + actPrice + ", msgLang=" + msgLang + ", orderKey=" + orderKey
				+ ", recieverMsisdn=" + recieverMsisdn + ", destinationTariff=" + destinationTariff
				+ ", tariffPermissions=" + tariffPermissions + ", textmsg=" + textmsg + ", senderName=" + senderName
				+ ", amount=" + amount + ", offeringId=" + offeringId + ", number=" + number + ", actionType="
				+ actionType + ", companyValue=" + companyValue + ", totalLimit=" + totalLimit + ", orderId=" + orderId
				+ ", groupTypeTo=" + groupTypeTo + ", groupIdTo=" + groupIdTo + ", accountId=" + accountId
				+ ", balance=" + balance + ", totalUsersbyGroup=" + totalUsersbyGroup + ", customerId=" + customerId
				+ ", transactionId=" + transactionId + ", iccid=" + iccid + ", contactNumber=" + contactNumber
				+ ", lowerLimit=" + lowerLimit + ", maxLimit=" + maxLimit + "]";
	}
	

}
