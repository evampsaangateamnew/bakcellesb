package com.nsn.devicemanagement.wsdl.common._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.nsn.devicemanagement.wsdl.provisioning._2.Priority;

/**
 * <p>
 * Java class for MmsStdFeatures complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="MmsStdFeatures">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="deliveryReportExpected" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="expiryPeriod" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="messageClass" type="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}MessageClass" minOccurs="0"/>
 *         &lt;element name="priority" type="{http://nsn.com/DeviceManagement/wsdl/Provisioning/2.0}priority" minOccurs="0"/>
 *         &lt;element name="timeStamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MmsStdFeatures", propOrder = { "deliveryReportExpected", "expiryPeriod", "messageClass", "priority",
		"timeStamp" })
public class MmsStdFeatures {
	protected Boolean deliveryReportExpected;
	protected Long expiryPeriod;
	@XmlSchemaType(name = "string")
	protected MessageClass messageClass;
	@XmlSchemaType(name = "string")
	protected Priority priority;
	@XmlSchemaType(name = "dateTime")
	protected XMLGregorianCalendar timeStamp;

	/**
	 * Gets the value of the deliveryReportExpected property.
	 * 
	 * @return possible object is {@link Boolean }
	 * 
	 */
	public Boolean isDeliveryReportExpected() {
		return deliveryReportExpected;
	}

	/**
	 * Sets the value of the deliveryReportExpected property.
	 * 
	 * @param value
	 *            allowed object is {@link Boolean }
	 * 
	 */
	public void setDeliveryReportExpected(Boolean value) {
		this.deliveryReportExpected = value;
	}

	/**
	 * Gets the value of the expiryPeriod property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getExpiryPeriod() {
		return expiryPeriod;
	}

	/**
	 * Sets the value of the expiryPeriod property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setExpiryPeriod(Long value) {
		this.expiryPeriod = value;
	}

	/**
	 * Gets the value of the messageClass property.
	 * 
	 * @return possible object is {@link MessageClass }
	 * 
	 */
	public MessageClass getMessageClass() {
		return messageClass;
	}

	/**
	 * Sets the value of the messageClass property.
	 * 
	 * @param value
	 *            allowed object is {@link MessageClass }
	 * 
	 */
	public void setMessageClass(MessageClass value) {
		this.messageClass = value;
	}

	/**
	 * Gets the value of the priority property.
	 * 
	 * @return possible object is {@link Priority }
	 * 
	 */
	public Priority getPriority() {
		return priority;
	}

	/**
	 * Sets the value of the priority property.
	 * 
	 * @param value
	 *            allowed object is {@link Priority }
	 * 
	 */
	public void setPriority(Priority value) {
		this.priority = value;
	}

	/**
	 * Gets the value of the timeStamp property.
	 * 
	 * @return possible object is {@link XMLGregorianCalendar }
	 * 
	 */
	public XMLGregorianCalendar getTimeStamp() {
		return timeStamp;
	}

	/**
	 * Sets the value of the timeStamp property.
	 * 
	 * @param value
	 *            allowed object is {@link XMLGregorianCalendar }
	 * 
	 */
	public void setTimeStamp(XMLGregorianCalendar value) {
		this.timeStamp = value;
	}
}
