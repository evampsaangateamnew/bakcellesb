package com.huawei.crm.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.crm.basetype.AddressList;
import com.huawei.crm.basetype.BillMediumList;
import com.huawei.crm.basetype.ExtParameterList;
import com.huawei.crm.basetype.GetSubOfferingList;
import com.huawei.crm.basetype.PaymentList;

/**
 * <p>
 * Java class for GetAccountOut complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="GetAccountOut">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ExternalCustomerId" type="{http://crm.huawei.com/basetype/}ExternalCustomerId" minOccurs="0"/>
 *         &lt;element name="CustomerId" type="{http://crm.huawei.com/basetype/}CustomerId" minOccurs="0"/>
 *         &lt;element name="AccountId" type="{http://crm.huawei.com/basetype/}AccountId" minOccurs="0"/>
 *         &lt;element name="PaymentType" type="{http://crm.huawei.com/basetype/}PaymentType" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://crm.huawei.com/basetype/}AccountStatus" minOccurs="0"/>
 *         &lt;element name="Title" type="{http://crm.huawei.com/basetype/}Title" minOccurs="0"/>
 *         &lt;element name="FirstName" type="{http://crm.huawei.com/basetype/}FirstName" minOccurs="0"/>
 *         &lt;element name="MiddleName" type="{http://crm.huawei.com/basetype/}MiddleName" minOccurs="0"/>
 *         &lt;element name="LastName" type="{http://crm.huawei.com/basetype/}LastName" minOccurs="0"/>
 *         &lt;element name="Balance" type="{http://crm.huawei.com/basetype/}InitialBalance" minOccurs="0"/>
 *         &lt;element name="InitialCredit" type="{http://crm.huawei.com/basetype/}InitialBalance" minOccurs="0"/>
 *         &lt;element name="BillcycleType" type="{http://crm.huawei.com/basetype/}BillcycleType" minOccurs="0"/>
 *         &lt;element name="EffectiveDate" type="{http://crm.huawei.com/basetype/}EffectiveDate" minOccurs="0"/>
 *         &lt;element name="BillMediumList" type="{http://crm.huawei.com/basetype/}BillMediumList" minOccurs="0"/>
 *         &lt;element name="PaymentList" type="{http://crm.huawei.com/basetype/}PaymentList" minOccurs="0"/>
 *         &lt;element name="AddressList" type="{http://crm.huawei.com/basetype/}AddressList" minOccurs="0"/>
 *         &lt;element name="SupplementaryOfferingList" type="{http://crm.huawei.com/basetype/}GetSubOfferingList" minOccurs="0"/>
 *         &lt;element name="AccountCode" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="32"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="Language" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="64"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PaymentMode" type="{http://crm.huawei.com/basetype/}PaymentModeType" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetAccountOut", propOrder = {})
public class GetAccountOut {
	@XmlElement(name = "ExternalCustomerId")
	protected String externalCustomerId;
	@XmlElement(name = "CustomerId")
	protected Long customerId;
	@XmlElement(name = "AccountId")
	protected Long accountId;
	@XmlElement(name = "PaymentType")
	protected String paymentType;
	@XmlElement(name = "Status")
	protected String status;
	@XmlElement(name = "Title")
	protected String title;
	@XmlElement(name = "FirstName")
	protected String firstName;
	@XmlElement(name = "MiddleName")
	protected String middleName;
	@XmlElement(name = "LastName")
	protected String lastName;
	@XmlElement(name = "Balance")
	protected Long balance;
	@XmlElement(name = "InitialCredit")
	protected Long initialCredit;
	@XmlElement(name = "BillcycleType")
	protected String billcycleType;
	@XmlElement(name = "EffectiveDate")
	protected String effectiveDate;
	@XmlElement(name = "BillMediumList")
	protected BillMediumList billMediumList;
	@XmlElement(name = "PaymentList")
	protected PaymentList paymentList;
	@XmlElement(name = "AddressList")
	protected AddressList addressList;
	@XmlElement(name = "SupplementaryOfferingList")
	protected GetSubOfferingList supplementaryOfferingList;
	@XmlElement(name = "AccountCode")
	protected String accountCode;
	@XmlElement(name = "Language")
	protected String language;
	@XmlElement(name = "PaymentMode")
	protected String paymentMode;
	@XmlElement(name = "ExtParamList")
	protected ExtParameterList extParamList;

	/**
	 * Gets the value of the externalCustomerId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getExternalCustomerId() {
		return externalCustomerId;
	}

	/**
	 * Sets the value of the externalCustomerId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setExternalCustomerId(String value) {
		this.externalCustomerId = value;
	}

	/**
	 * Gets the value of the customerId property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getCustomerId() {
		return customerId;
	}

	/**
	 * Sets the value of the customerId property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setCustomerId(Long value) {
		this.customerId = value;
	}

	/**
	 * Gets the value of the accountId property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getAccountId() {
		return accountId;
	}

	/**
	 * Sets the value of the accountId property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setAccountId(Long value) {
		this.accountId = value;
	}

	/**
	 * Gets the value of the paymentType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaymentType() {
		return paymentType;
	}

	/**
	 * Sets the value of the paymentType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPaymentType(String value) {
		this.paymentType = value;
	}

	/**
	 * Gets the value of the status property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the value of the status property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStatus(String value) {
		this.status = value;
	}

	/**
	 * Gets the value of the title property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the value of the title property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTitle(String value) {
		this.title = value;
	}

	/**
	 * Gets the value of the firstName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the value of the firstName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFirstName(String value) {
		this.firstName = value;
	}

	/**
	 * Gets the value of the middleName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * Sets the value of the middleName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMiddleName(String value) {
		this.middleName = value;
	}

	/**
	 * Gets the value of the lastName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the value of the lastName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLastName(String value) {
		this.lastName = value;
	}

	/**
	 * Gets the value of the balance property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getBalance() {
		return balance;
	}

	/**
	 * Sets the value of the balance property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setBalance(Long value) {
		this.balance = value;
	}

	/**
	 * Gets the value of the initialCredit property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getInitialCredit() {
		return initialCredit;
	}

	/**
	 * Sets the value of the initialCredit property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setInitialCredit(Long value) {
		this.initialCredit = value;
	}

	/**
	 * Gets the value of the billcycleType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBillcycleType() {
		return billcycleType;
	}

	/**
	 * Sets the value of the billcycleType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBillcycleType(String value) {
		this.billcycleType = value;
	}

	/**
	 * Gets the value of the effectiveDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEffectiveDate() {
		return effectiveDate;
	}

	/**
	 * Sets the value of the effectiveDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEffectiveDate(String value) {
		this.effectiveDate = value;
	}

	/**
	 * Gets the value of the billMediumList property.
	 * 
	 * @return possible object is {@link BillMediumList }
	 * 
	 */
	public BillMediumList getBillMediumList() {
		return billMediumList;
	}

	/**
	 * Sets the value of the billMediumList property.
	 * 
	 * @param value
	 *            allowed object is {@link BillMediumList }
	 * 
	 */
	public void setBillMediumList(BillMediumList value) {
		this.billMediumList = value;
	}

	/**
	 * Gets the value of the paymentList property.
	 * 
	 * @return possible object is {@link PaymentList }
	 * 
	 */
	public PaymentList getPaymentList() {
		return paymentList;
	}

	/**
	 * Sets the value of the paymentList property.
	 * 
	 * @param value
	 *            allowed object is {@link PaymentList }
	 * 
	 */
	public void setPaymentList(PaymentList value) {
		this.paymentList = value;
	}

	/**
	 * Gets the value of the addressList property.
	 * 
	 * @return possible object is {@link AddressList }
	 * 
	 */
	public AddressList getAddressList() {
		return addressList;
	}

	/**
	 * Sets the value of the addressList property.
	 * 
	 * @param value
	 *            allowed object is {@link AddressList }
	 * 
	 */
	public void setAddressList(AddressList value) {
		this.addressList = value;
	}

	/**
	 * Gets the value of the supplementaryOfferingList property.
	 * 
	 * @return possible object is {@link GetSubOfferingList }
	 * 
	 */
	public GetSubOfferingList getSupplementaryOfferingList() {
		return supplementaryOfferingList;
	}

	/**
	 * Sets the value of the supplementaryOfferingList property.
	 * 
	 * @param value
	 *            allowed object is {@link GetSubOfferingList }
	 * 
	 */
	public void setSupplementaryOfferingList(GetSubOfferingList value) {
		this.supplementaryOfferingList = value;
	}

	/**
	 * Gets the value of the accountCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAccountCode() {
		return accountCode;
	}

	/**
	 * Sets the value of the accountCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAccountCode(String value) {
		this.accountCode = value;
	}

	/**
	 * Gets the value of the language property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * Sets the value of the language property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLanguage(String value) {
		this.language = value;
	}

	/**
	 * Gets the value of the paymentMode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaymentMode() {
		return paymentMode;
	}

	/**
	 * Sets the value of the paymentMode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPaymentMode(String value) {
		this.paymentMode = value;
	}

	/**
	 * Gets the value of the extParamList property.
	 * 
	 * @return possible object is {@link ExtParameterList }
	 * 
	 */
	public ExtParameterList getExtParamList() {
		return extParamList;
	}

	/**
	 * Sets the value of the extParamList property.
	 * 
	 * @param value
	 *            allowed object is {@link ExtParameterList }
	 * 
	 */
	public void setExtParamList(ExtParameterList value) {
		this.extParamList = value;
	}
}
