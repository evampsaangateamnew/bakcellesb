package com.evampsaanga.bakcell.roamingcountriesdata;

import com.evampsaanga.bakcell.responseheaders.BaseResponse;

public class GetRoamingCountriesResponse extends BaseResponse {
	private com.evampsaanga.bakcell.roamingcountriesdata.Data data=new com.evampsaanga.bakcell.roamingcountriesdata.Data();

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

}
