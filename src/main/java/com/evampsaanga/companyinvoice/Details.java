
package com.evampsaanga.companyinvoice;

import com.evampsaanga.configs.ConfigurationManager;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "detailInvoice",
    "detailDesc",
    "detailTotal",
    "detailUsage",
    "subitemTitle",
    "detailUnit",
    "detailTotalTax"
})
public class Details {

	@JsonProperty("detailUnit")
	private String detailUnit;
    @JsonProperty("detailInvoice")
    private String detailInvoice;
    @JsonProperty("detailDesc")
    private String detailDesc;
    @JsonProperty("detailTotal")
    private String detailTotal;
    @JsonProperty("detailUsage")
    private String detailUsage;
    @JsonProperty("subitemTitle")
    private String subitemTitle;
    @JsonProperty("detailTotalTax")
    private String detailTotalTax;

    
    
    
    @JsonProperty("detailTotalTax")
    public String getDetailTotalTax() {
		return detailTotalTax;
	}
    @JsonProperty("detailTotalTax")
	public void setDetailTotalTax(String detailTotalTax) {
		this.detailTotalTax = detailTotalTax;
	}

	@JsonProperty("detailUnit")
    public String getDetailUnit() {
        return detailUnit;
    }

    @JsonProperty("detailUnit")
    public void setDetailUnit(String detailUnit) {
        this.detailUnit = detailUnit;
    }
    @JsonProperty("detailInvoice")
    public String getDetailInvoice() {
        return detailInvoice;
    }

    @JsonProperty("detailInvoice")
    public void setDetailInvoice(String detailInvoice) {
        this.detailInvoice = detailInvoice;
    }

    @JsonProperty("detailDesc")
    public String getDetailDesc() {
        return detailDesc;
    }

    @JsonProperty("detailDesc")
    public void setDetailDesc(String detailDesc) {
        this.detailDesc = detailDesc;
    }

    @JsonProperty("detailTotal")
    public String getDetailTotal() {
        return detailTotal;
    }

    @JsonProperty("detailTotal")
    public void setDetailTotal(String detailTotal) {
        this.detailTotal = detailTotal;
    }

    @JsonProperty("detailUsage")
    public String getDetailUsage() {
        return detailUsage;
    }

    @JsonProperty("detailUsage")
    public void setDetailUsage(String detailUsage) {
        this.detailUsage = detailUsage;
    }

    @JsonProperty("subitemTitle")
    public String getSubitemTitle() {
        return subitemTitle;
    }

    @JsonProperty("subitemTitle")
    public void setSubitemTitle(String subitemTitle) {
        this.subitemTitle = subitemTitle;
    }

    @Override
    public String toString() 
    {
        return new ToStringBuilder(this).append("detailInvoice", detailInvoice).append("detailDesc", detailDesc).append("detailTotal", detailTotal).append("detailUsage", detailUsage).append("subitemTitle", subitemTitle).append("detailUnit", detailUnit).toString();
    }

	public void setSubitemTitle(String nameFromDB, String lang) 
	{
		// TODO Auto-generated method stub
		String title = ConfigurationManager.getConfigurationFromCache("details.companyDetail."+nameFromDB+"."+lang);
		this.setSubitemTitle(title);
		
	}

	public void setDetailUnit(String nameFromDB, String lang) {
		// TODO Auto-generated method stub
		String title = ConfigurationManager.getConfigurationFromCache("details.unit."+nameFromDB+"."+lang);
		this.setDetailUnit(title);
	}

}
