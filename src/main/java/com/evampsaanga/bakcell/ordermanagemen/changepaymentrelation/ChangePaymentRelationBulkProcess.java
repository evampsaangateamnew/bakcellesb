package com.evampsaanga.bakcell.ordermanagemen.changepaymentrelation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import com.evampsaanga.bakcell.db.DBFactory;
import com.evampsaanga.bakcell.hlrweb.getquerybalanceRequest.HLRBalanceServices;
import com.evampsaanga.bakcell.ordermanagement.changegroup.ChangeGroup;
import com.evampsaanga.bakcell.ordermanagementV2.OrderManagementLand;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.gethomepage.Balance;
import com.evampsaanga.magento.tariffdetailsv2.TariffDetailsMagentoResponse;
import com.huawei.bme.cbsinterface.bcservices.PayRelationComparator;
import com.huawei.bme.cbsinterface.bcservices.querypayment.QueryPaymentRelationResult.PaymentRelationList.PayRelation;
import com.huawei.bme.cbsinterface.bcservices.querypayment.QueryPaymentRelationResultMsg;
import com.huawei.bss.soaif._interface.orderservice.createorder.CreateOrderRspMsg;


public class ChangePaymentRelationBulkProcess {
	
	public void processOrder(JSONObject jsonObject,JSONArray jArray,StringBuffer stringBuffer,String orderId,java.sql.Connection conn,Logger loggerV2)
	{
	
		loggerV2.info(":Landed in ChangePaymentRelationBulkProcess -->processOrder " );
		try{
		ArrayList<String> tariffIds = new ArrayList<String>();

		for (int i = 0; i < jArray.length(); i++) 
		{
			tariffIds.add(jArray.getJSONObject(i).get("tariff_ids").toString());
			stringBuffer.append(Helper.getOMlogTimeStamp()+" Adding Tariff IDs "+jArray.getJSONObject(i).get("tariff_ids").toString());
			stringBuffer.append(System.getProperty("line.separator"));
		}
		loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :TariffIDsBulkFromDB for ChangePaymentRelation" + tariffIds);

		loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :TariffIdArrayOfString   : " + tariffIds);
		TariffDetailsMagentoResponse dataTariffDetailsbulk = ConfigurationManager.getConfigurationFromTariffCache(Constants.TARIFF_DETAIL_ENGLISH);
        // IF condition to check tariffDetailsBulk   APi response from magento
		if (dataTariffDetailsbulk.getResultCode().equals(ResponseCodes.TARIFF_DETAILS_VERSION_2_SUCCESSFUL)) 
		{
		    
			for (int a = 0; a < jArray.length(); a++) 
			{  // START of For Loop  of recievers MSsidn
				
			try
			{
			jsonObject.put("msisdn", jArray.getJSONObject(a).get("msisdn").toString());
			jsonObject.put("groupType", jArray.getJSONObject(a).get("groupType").toString());
			loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :msisdn is: " + jsonObject.getString("msisdn"));
			loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :GroupType is: " + jArray.getJSONObject(a).get("groupType").toString());
			loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :TariffID is: " + jArray.getJSONObject(a).get("tariff_ids").toString());
//			loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :accountId is: " + jsonObject.getString("accoundId").toString());

			// PAYMENT HERE
//	       for(int i=0 ;i<dataTariffDetailsbulk.getData().size();i++)
//	       { //Start of loop for tariff Response, We will get tariffID from TarifffResponse, and then cmpare it with Tariff_Id from Database
	    	
	    	   // Start of IF comparing tarifID from magento response with TariffId from Database
//	    		if (jArray.getJSONObject(a).get("tariff_ids").toString().equalsIgnoreCase(
//						dataTariffDetailsbulk.getData().get(i).getHeader().getOfferingId())) 
			com.huawei.crm.query.GetSubscriberResponse response = new com.evampsaanga.bakcell.getsubscriber.CRMSubscriberService().GetSubscriberRequest(jArray.getJSONObject(a).get("msisdn").toString());
			if (response.getResponseHeader().getRetCode().equals("0")) 
			{
				loggerV2.info("*** MSISDN CURRENT TariffID FROM SUBSCRIBER ***" + response.getGetSubscriberBody().getPrimaryOffering().getOfferingId().getOfferingId());
				int indexForOffering = Helper.compare(response.getGetSubscriberBody().getPrimaryOffering().getOfferingId().getOfferingId(), dataTariffDetailsbulk.getData());
				if(indexForOffering!=-1)
				{
	    		
	    			
	    			String mrcValueofBulkTarifff = dataTariffDetailsbulk.getData().get(indexForOffering).getHeader()
							.getMrcValue();
					stringBuffer.append(
							Helper.getOMlogTimeStamp() + "************************************************");
					stringBuffer.append(System.getProperty("line.separator"));
					stringBuffer.append(Helper.getOMlogTimeStamp() + "***Query Balance API call");
					stringBuffer.append(System.getProperty("line.separator"));
					stringBuffer.append(
							Helper.getOMlogTimeStamp() + "************************************************");
					stringBuffer.append(System.getProperty("line.separator"));
					HLRBalanceServices service = new HLRBalanceServices();
					Balance balance = service.getBalance(
							jArray.getJSONObject(a).getString("msisdn").toString(), "postpaid",
							null);
					loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :Balanceresponsecode " + balance.getReturnCode());
					loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :BalanceresponseDescription " + balance.getReturnMsg());
					loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :MRc " +mrcValueofBulkTarifff);
					loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :companyValue " +jsonObject.getString("companyValue"));
					if(jsonObject.has("companyValue") && !jsonObject.getString("companyValue").equals(""))
					{
						if (balance.getReturnCode().equals("0")) 
						{
						loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :CorporateBalance " + balance.getPostpaid().getCurrentCreditCorporateValue());
							//STARt of IF balance successfull
			            double currentAndCompanyValue=Double.parseDouble(mrcValueofBulkTarifff)+Double.parseDouble(jsonObject.getString("companyValue"));
			          
			            loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :CurrentAndCompanyyValue: " + balance.getPostpaid().getCurrentCreditCorporateValue());
	//		            loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :MAxLimitinRequest: " + jsonObject.getString("maxLimit"));
			        	double maxiMumLimit = Double.parseDouble(jsonObject.getString("amount"));
	/*//		        	loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :MAxLimitdobleValue: " + jsonObject.getString("maxLimit"));
	*/		        	loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :GroupTypeBforeCheck: "+jArray.getJSONObject(a).getString("groupType").toString());
			            if(jArray.getJSONObject(a).getString("groupType").toString().equalsIgnoreCase("PaybySubs") || jArray.getJSONObject(a).getString("groupType").toString().equalsIgnoreCase("PaybySub"))
			             {
	           
			            	loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" : GroupType:"+ jArray.getJSONObject(a).get("groupType").toString());
			            	/*if(jsonObject.getString("channel").toString().equals("web"))
			            	{
			            		maxiMumLimit= Double.parseDouble(jsonObject.getString("maxLimit"));
			            	}*/
			            	if(maxiMumLimit>=0.0 && maxiMumLimit<=currentAndCompanyValue)
			            	 {
			            		//successful for max limit will be current + value for company standard 
			            		 loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :----Successfull maxLimit >0 and maxLimit<=comppanyValue+mrc---");
			            		 loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :-----STARt OF CHANGE INITIAL CREDIT LIMIT-----------");  
			                    
			            		 loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :-----STARt OF QUERY PAYMENT RELATION-----------");  
			                     QueryPaymentRelationResultMsg responseQPR = OrderManagementLand.queryPaymentRelation(jArray.getJSONObject(a).getString("msisdn").toString(),stringBuffer);
	
			        			loggerV2.info(
			        					"msisdn "+jsonObject.getString("msisdn")+" :RESPONSE QUERY PAYMET CODE :" + responseQPR.getResultHeader().getResultCode());
			        			loggerV2.info(
			        					"msisdn "+jsonObject.getString("msisdn")+" :RESPONSE QUERY PAYMET MSG :" + responseQPR.getResultHeader().getResultDesc());
			        			loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :RESPONSE QUERY PAYMET MSG :" + Helper.ObjectToJson(
			        					responseQPR.getQueryPaymentRelationResult().getPaymentRelationList()));
	
			        			stringBuffer.append(Helper.getOMlogTimeStamp() + "End of Query Payment Relation");
			        			stringBuffer.append(System.getProperty("line.separator"));
			        			loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :-----END OF QUERY PAYMENT RELATION-----------");
			             if(responseQPR.getResultHeader().getResultCode().equalsIgnoreCase("0"))
			             {  //Start of If  queryPAyment Successful
			        			// *********************Get
			        			// highest priority PayRelation
			        			// ********************//
			        			PayRelation payRelationObject = Collections.max(responseQPR.getQueryPaymentRelationResult()
			        					.getPaymentRelationList().getPayRelation(), new PayRelationComparator());
	
			        			loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" : QUERY PAYMET Priority :" + payRelationObject.getPriority().toString());
			        			loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :QUERY PAYMET AccountKey :" + payRelationObject.getAcctKey());
			        			/// **************************************************************************//
	
			        			// END of QueryPAymentRelation
			        			
			        			
			        				//Start Now calling setIntitialCreditLimit
			        				Thread.sleep(20000);
			        				 CreateOrderRspMsg CreditLimitResponse=ChangeGroup.setInitialCreditLimit(((long)maxiMumLimit*Constants.MONEY_DIVIDEND),payRelationObject.getAcctKey(),jArray.getJSONObject(a).get("msisdn").toString());
										
									 if(CreditLimitResponse!=null && CreditLimitResponse.getRspHeader().getReturnCode().equals("0000"))
									 {
									//success
										 OrderManagementLand.updateOrderDetails(orderId, jsonObject.getString("msisdn"), "C",ResponseCodes.SUCESS_CODE_200, ResponseCodes.SUCESS_DES_200, conn,stringBuffer);
									 }
									else
									{
										//failed
		
										OrderManagementLand.updateOrderDetails(orderId, jsonObject.getString("msisdn"), "F",
												CreditLimitResponse.getRspHeader().getReturnCode(),
												CreditLimitResponse.getRspHeader().getReturnMsg() + jsonObject.getString("msisdn"), conn,
			        							stringBuffer);
									}
			             }  //END OF if QueryPAyment Successful
			        		else   //if queryPAyment API from bakcell Failed
			        			{
			        			OrderManagementLand.updateOrderDetails(orderId, jsonObject.getString("msisdn"), "F",
			        					responseQPR.getResultHeader().getResultCode(),
			        					responseQPR.getResultHeader().getResultDesc() + jsonObject.getString("msisdn"), conn,
										stringBuffer);
							
			        			}
	            	 }
	            	 else
	            	 {
	            		 //failed as max limit will be current + value for company standard 
	            		 OrderManagementLand.updateOrderDetails(orderId, jsonObject.getString("msisdn"), "F",
	  							ResponseCodes.MAX_LIMIT_GREATER_THAN_MRC_OR_COMAPNY_VALUE_ERROR_CODE,
	  							ResponseCodes.MAX_LIMIT_GREATER_THAN_MRC_OR_COMAPNY_VALUE_ERROR_DESCRIPTION + jsonObject.getString("msisdn"), conn,
	  							stringBuffer);
	            	 }
	            		 
	             }
	            
	             if(jArray.getJSONObject(a).getString("groupType").toString().equalsIgnoreCase("PartPay") || jArray.getJSONObject(a).getString("groupType").toString().equalsIgnoreCase("FullPay"))
	             {
	            	 loggerV2.info(
	     					"msisdn "+jsonObject.getString("msisdn")+" : GroupType:"+ jArray.getJSONObject(a).get("groupType").toString());
	            	 loggerV2.info(
	      					"msisdn "+jsonObject.getString("msisdn")+" : MRcValueeFromMagento:"+ mrcValueofBulkTarifff);
	            	 
	            	 double lowerLimitToBeUses=0.0;
	            	// getCurrentCreditCorporateValue
	            	 double avaialbleCorporateValue=Double.parseDouble(balance.getPostpaid().getCurrentCreditCorporateValue());
	            	 if(avaialbleCorporateValue<0.0)
	            	 {
	            		 loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :Balance Before MakingPositive: "+avaialbleCorporateValue);
	            		 avaialbleCorporateValue=(-1.0)*(avaialbleCorporateValue);
	            		 loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :Balance After MakingPositive: "+avaialbleCorporateValue);
	            	 }
	            	 if(Double.parseDouble(mrcValueofBulkTarifff) >avaialbleCorporateValue)
	            	 {
	            		 lowerLimitToBeUses=Double.parseDouble(mrcValueofBulkTarifff);
	            		 loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :mrcValueofBulkTarifff) >avaialbleCorporateValue Value for lowerLimitToBeUses :"+lowerLimitToBeUses);
	            	 }
	            	 else
	            	 {
	            		 lowerLimitToBeUses=avaialbleCorporateValue;
	            		 loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :mrcValueofBulkTarifff) <  avaialbleCorporateValue and Value"+lowerLimitToBeUses);
	            	 }
	            	 
	            	 loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :LowerLimitToBeUsedAfterCalculation"+lowerLimitToBeUses);
	            	
	            	 loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :LoweLimitInRequestFrom Tst"+jsonObject.getString("lowerLimit"));
	            	 loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :LoweLimitInRequestFrom"+Double.parseDouble(jsonObject.getString("lowerLimit").toString()));
	            	 Double currentUnbilledAmount;
	            	 
	            	 if(Double.parseDouble(balance.getPostpaid().getBalanceCorporateValue())<0)
	            		 currentUnbilledAmount = Double.parseDouble(balance.getPostpaid().getBalanceCorporateValue()) * -1;
	            	 else
	            		 currentUnbilledAmount = Double.parseDouble(balance.getPostpaid().getBalanceCorporateValue());
	            	 
	            	 loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :Balance Corporate value"+currentUnbilledAmount);
	            	 
	            	 
	            	 ///if selected amount is less than (max(MRC or unbilled amount)) we should give error
	            	 double value;
	            	 if(Double.parseDouble(mrcValueofBulkTarifff) >currentUnbilledAmount)
	            	 {
	            		 value=Double.parseDouble(mrcValueofBulkTarifff);
	            		 loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :MRC :"+value);
	            	 }
	            	 else
	            	 {
	            		 value=currentUnbilledAmount;
	            		 loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :Unbilled Amount Value"+currentUnbilledAmount);
	            	 }
	            	             	 
	            	 if((Double.parseDouble(jsonObject.getString("amount").toString())<value)) //(Double.parseDouble(jsonObject.getString("amount").toString())<=lowerLimitToBeUses )
	            	 {
	            		 //FailedCase   And needs to use unique custom responseCode and responseDescription
	            		 OrderManagementLand.updateOrderDetails(orderId, jsonObject.getString("msisdn"), "F",
	 							ResponseCodes.LOWER_LIMIT_LESS_THAN_MRC_OR_BALANCE_ERROR_CODE,
	 							ResponseCodes.LOWER_LIMIT_LESS_THAN_MRC_OR_BALANCE_ERROR_DESCRIPTION + jsonObject.getString("msisdn"), conn,
	 							stringBuffer);
	            	 }
	            	 else
	            	 {
	            		 	//successful for lower Limit
	            		 loggerV2.info("maxMumLimit "+maxiMumLimit);
	            		 loggerV2.info("currentAndCompanyValue "+currentAndCompanyValue);
	//            			if(maxiMumLimit<=currentAndCompanyValue)
	//                   	 {
	                   		 //successful for max limit will be current + value for company standard 
	            				 loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :-----START OF QUERY PAYMENT RELATION-----------");  
	            	             QueryPaymentRelationResultMsg responseQPR = OrderManagementLand.queryPaymentRelation(jArray.getJSONObject(a).getString("msisdn").toString(),stringBuffer);
	
	            				loggerV2.info(
	            						"msisdn "+jsonObject.getString("msisdn")+" :RESPONSE QUERY PAYMET CODE :" + responseQPR.getResultHeader().getResultCode());
	            				loggerV2.info(
	            						"msisdn "+jsonObject.getString("msisdn")+" :RESPONSE QUERY PAYMET MSG :" + responseQPR.getResultHeader().getResultDesc());
	            				loggerV2.info("RESPONSE QUERY PAYMET MSG :" + Helper.ObjectToJson(
	            						responseQPR.getQueryPaymentRelationResult().getPaymentRelationList()));
	
	            				stringBuffer.append(Helper.getOMlogTimeStamp() + "End of Query Payment Relation");
	            				stringBuffer.append(System.getProperty("line.separator"));
	            				loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :-----END OF QUERY PAYMENT RELATION-----------");
	            			    if(responseQPR.getResultHeader().getResultCode().equalsIgnoreCase("0"))
	            	             {  //Start of If  queryPAyment Successful
	            				// *********************Get
	            				// highest priority PayRelation
	            				// ********************//
	            				PayRelation payRelationObject = Collections.max(responseQPR.getQueryPaymentRelationResult()
	            						.getPaymentRelationList().getPayRelation(), new PayRelationComparator());
	            				
	            				loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" : QUERY PAYMET Priority :" + payRelationObject.getPriority().toString());
	            				loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :QUERY PAYMET AccountKey :" + payRelationObject.getAcctKey());
	            				/// **************************************************************************//
	
	            				// END of QueryPAymentRelation
	
	            				// JUST FOR TEST : Start of
	            				// change PaymentRelation
	            				loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :-------START OF CHANGE PAYMENT RELATION-----");
	
	            				String valuelimit = jsonObject.getString("amount");
	            				loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :Final Value toPAssed in changePAyment :" + valuelimit);
	
	            				//Call ChangePaymentRelation  bakcell
	            				Thread.sleep(20000);
	            				CreateOrderRspMsg resCreateOrderChangePAyment=OrderManagementLand.changePaymentRealtion(jArray.getJSONObject(a).getString("groupType").toString(),jArray.getJSONObject(a).getString("msisdn").toString(), Double.parseDouble(valuelimit),payRelationObject.getPaymentLimitKey(), payRelationObject.getAcctKey());
	
	            				loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :ChangePAyment ResultCode: " + resCreateOrderChangePAyment.getRspHeader().getReturnCode());
	            				loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :ChangePAyment ResultMsg: " + resCreateOrderChangePAyment.getRspHeader().getReturnMsg());
	            				loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :ChangePAyment orderId: " + resCreateOrderChangePAyment.getOrderId());
	            				loggerV2.info("msisdn "+jsonObject.getString("msisdn")+" :--------END OF CHANGE PAYMENT RELATION-------");
	
	            				//Start of IF changePAymentRelation successful check
	            				if (resCreateOrderChangePAyment.getRspHeader().getReturnCode().equals("0")
	            						|| resCreateOrderChangePAyment.getRspHeader().getReturnCode().equals("0000")) {
	            					// updating order details
	            				
	            						OrderManagementLand.updateOrderDetails(orderId, jsonObject.getString("msisdn"), "C",
	            								ResponseCodes.SUCESS_CODE_200, ResponseCodes.SUCESS_DES_200, conn,
	            								stringBuffer);
	            					
	            				
	            				} //END of IF changePAymentRelation successful check
	            				else 
	            				{ 
	            					//START OF ELSE of  ( IF changePAymentRelation failed from bakcell)
	            					
	            					OrderManagementLand.updateOrderDetails(orderId,
	            							jsonObject.getString("msisdn"),
	            							"F",
	            							resCreateOrderChangePAyment.getRspHeader().getReturnCode(),
	            							resCreateOrderChangePAyment.getRspHeader().getReturnMsg() + jsonObject.getString("msisdn"), conn,
	            							stringBuffer);
	            					
	            				}//START OF ELSE of   IF changePAymentRelation failed from bakcell
	                   	}  //END OF if QueryPAyment Successful
	                		else   //if queryPAyment API from bakcell Failed
	                			{
	                			OrderManagementLand.updateOrderDetails(orderId, jsonObject.getString("msisdn"), "F",
	                					responseQPR.getResultHeader().getResultCode(),
	                					responseQPR.getResultHeader().getResultDesc() + jsonObject.getString("msisdn"), conn,
	        							stringBuffer);
	        				
	                			}
	//                   	 }
	//                   	 
	//                   	 else
	//                   	 {
	//                   		 //failed as max limit will be current + value for company standard 
	//                   		 OrderManagementLand.updateOrderDetails(orderId, jsonObject.getString("msisdn"), "F",
	//       							ResponseCodes.MAX_LIMIT_GREATER_THAN_MRC_OR_COMAPNY_VALUE_ERROR_CODE,
	//       							ResponseCodes.MAX_LIMIT_GREATER_THAN_MRC_OR_COMAPNY_VALUE_ERROR_DESCRIPTION + jsonObject.getString("msisdn"), conn,
	//       							stringBuffer);
	//                   	 }
	            	 }
	            
	            	 
	             }
	            
	
		    		} // END of   IF balance successfull 
				else
	    		{ 
					//ELSe  of   (IF balance Failed)
	    			
	          		 OrderManagementLand.updateOrderDetails(orderId, jsonObject.getString("msisdn"), "F",
								balance.getReturnCode(),
								balance.getReturnMsg()+ jsonObject.getString("msisdn"), conn,
								stringBuffer);
	    			
	    		}
			}
					else
					{
						OrderManagementLand.updateOrderDetails(orderId, jsonObject.getString("msisdn"), "F",
								"100",								
								"Company Value is Empty"+ jsonObject.getString("msisdn"), conn,
								stringBuffer);
					}
	    		
	    	}// END of IF comparing tarifID from magento response with TariffId from Database
				else
				{
					OrderManagementLand.updateOrderDetails(orderId, jsonObject.getString("msisdn"), "F",
							"100",
							"MRC not found"+ jsonObject.getString("msisdn"), conn,
							stringBuffer);
	    		} //END of FOR loop for tarifffResponse from MAgento
			}
			else
			{
				OrderManagementLand.updateOrderDetails(orderId, jsonObject.getString("msisdn"), "F",
						response.getResponseHeader().getRetCode(),
						response.getResponseHeader().getRetMsg()+ jsonObject.getString("msisdn"), conn,
						stringBuffer);
			}
			}catch (Exception e1) {
				// TODO: handle exception
				loggerV2.info(Helper.GetException(e1));
		  		 OrderManagementLand.updateOrderDetails(orderId, jsonObject.getString("msisdn"), "F",
							ResponseCodes.CONNECTIVITY_PROBLEM_CODE,
							e1.getMessage(), conn,
							stringBuffer);
			}
		} // END of For Loop  of recievers MSsidn
	}  //END of  IF condition to check tariffDetailsBulk   APi response from magento
	else  //ELSe  of  IF condition to check tariffDetailsBulk   APi response from magento
	{
		OrderManagementLand.updateOrderStatus(orderId, "F", conn, stringBuffer);
		
	}
		updateOrderStatus(orderId, "C", stringBuffer);
		
	}catch (Exception e) {
		// TODO: handle exception
		loggerV2.info(Helper.GetException(e));
		try {
			OrderManagementLand.updateOrderStatus(orderId, "F", conn, stringBuffer);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			loggerV2.info(Helper.GetException(e1));
		}
	}
	
	}

	public static boolean updateOrderStatus(String order_id, String status,
			StringBuffer stringBuffer) throws SQLException 
	{
		Connection conn = null;
		String query = "update purchase_order set status = ? where order_id= ?";
		if(conn == null || conn.isClosed())
			conn = DBFactory.getDbConnectionFromPool();
		PreparedStatement preparedStmt = conn.prepareStatement(query);
		preparedStmt = conn.prepareStatement(query);
		preparedStmt.setString(1, status);
		preparedStmt.setString(2, order_id);

//		loggerV2.info("QUERY for update Order_purchase" + preparedStmt.toString());
		preparedStmt.execute();
		int count = preparedStmt.getUpdateCount();
		// check is added for order management,in case of process order logging
		// will not
		// work
		if (stringBuffer != null) {

			stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
			stringBuffer.append(System.getProperty("line.separator"));
			stringBuffer.append(Helper.getOMlogTimeStamp() + "*** Update Purchase Order Details  ");
			stringBuffer.append(System.getProperty("line.separator"));
			stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
			stringBuffer.append(System.getProperty("line.separator"));
			stringBuffer.append(Helper.getOMlogTimeStamp() + "Executing Query : " + preparedStmt.toString());
			stringBuffer.append(System.getProperty("line.separator"));

			if (count > 0)
			{
				stringBuffer.append(Helper.getOMlogTimeStamp() + "Record updated : ");
				stringBuffer.append(System.getProperty("line.separator"));
			}
			else
			{
				stringBuffer.append(Helper.getOMlogTimeStamp() + "Record updated : ");
				stringBuffer.append(System.getProperty("line.separator"));
			}
		}
		if (count > 0)
			return true;
		else
			return false;
	}
	
	
	public static void main(String[] args) {
		double a=-2.42;
		double b=(-1.0)*a;
		System.out.println(b);
	}

		
}

