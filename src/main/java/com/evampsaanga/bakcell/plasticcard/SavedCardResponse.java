package com.evampsaanga.bakcell.plasticcard;

import java.util.List;

import com.evampsaanga.bakcell.responseheaders.BaseResponse;

public class SavedCardResponse extends BaseResponse {
	private List<SingleCardDetails> singleCardDetails;
	private String lastAmount;

	public List<SingleCardDetails> getSingleCardDetails() {
		return singleCardDetails;
	}

	public void setSingleCardDetails(List<SingleCardDetails> singleCardDetails) {
		this.singleCardDetails = singleCardDetails;
	}

	@Override
	public String toString() {
		return "SavedCardResponseData [singleCardDetails=" + singleCardDetails + "]";
	}

	public String getLastAmount() {
		return lastAmount;
	}

	public void setLastAmount(String lastAmount) {
		this.lastAmount = lastAmount;
	}

}