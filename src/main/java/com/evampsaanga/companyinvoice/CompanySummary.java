
package com.evampsaanga.companyinvoice;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "chargeOpen",
    "newCredit",
    "newChanrge",
    "chargeClose",
    "totalDiscount",
    "totalTax"
})
public class CompanySummary {

    @JsonProperty("chargeOpen")
    private String chargeOpen;
    @JsonProperty("newCredit")
    private String newCredit;
    @JsonProperty("newCharge")
    private String newCharge;
    @JsonProperty("chargeClose")
    private String chargeClose;
    @JsonProperty("totalDiscount")
    private String totalDiscount;
    @JsonProperty("totalTax")
    private String totalTax;

    
    @JsonProperty("totalTax")
    public String getTotalTax() {
		return totalTax;
	}
    @JsonProperty("totalTax")
	public void setTotalTax(String totalTax) {
		this.totalTax = totalTax;
	}

	@JsonProperty("chargeOpen")
    public String getChargeOpen() {
        return chargeOpen;
    }

    @JsonProperty("chargeOpen")
    public void setChargeOpen(String chargeOpen) {
        this.chargeOpen = chargeOpen;
    }

    @JsonProperty("newCredit")
    public String getNewCredit() {
        return newCredit;
    }

    @JsonProperty("newCredit")
    public void setNewCredit(String newCredit) {
        this.newCredit = newCredit;
    }

    @JsonProperty("newCharge")
    public String getNewCharge() {
        return newCharge;
    }

    @JsonProperty("newCharge")
    public void setNewCharge(String newChanrge) {
        this.newCharge = newChanrge;
    }

    @JsonProperty("chargeClose")
    public String getChargeClose() {
        return chargeClose;
    }

    @JsonProperty("chargeClose")
    public void setChargeClose(String chargeClose) {
        this.chargeClose = chargeClose;
    }

    @JsonProperty("totalDiscount")
    public String getTotalDiscount() {
        return totalDiscount;
    }

    @JsonProperty("totalDiscount")
    public void setTotalDiscount(String totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("chargeOpen", chargeOpen).append("newCredit", newCredit).append("newChanrge", newCharge).append("chargeClose", chargeClose).append("totalDiscount", totalDiscount).toString();
    }

}
