package com.evampsaanga.bakcell.suplementryservices;

import com.evampsaanga.bakcell.responseheaders.BaseResponse;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.ALWAYS)
public class SupplementryServicesResponse extends BaseResponse {
	@JsonInclude(JsonInclude.Include.ALWAYS)
	com.evampsaanga.bakcell.suplementryservices.Data data = new Data();

	public com.evampsaanga.bakcell.suplementryservices.Data getData() {
		return data;
	}

	public void setData(com.evampsaanga.bakcell.suplementryservices.Data data) {
		this.data = data;
	}
}
