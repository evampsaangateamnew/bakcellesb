package com.evampsaanga.bakcell.actionhistory;

import java.util.ArrayList;

import com.evampsaanga.bakcell.responseheaders.BaseResponse;

/**
 * Response Container For Action History
 * @author Aqeel Abbas
 *
 */
public class ActionHistoryResponse extends BaseResponse{
	private ArrayList<ActionHistoryResponseData> orderList;

	public ArrayList<ActionHistoryResponseData> getOrderList() {
		return orderList;
	}

	public void setOrderList(ArrayList<ActionHistoryResponseData> orderList) {
		this.orderList = orderList;
	}

}
