
package com.huawei.crm.service.order.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.order.query.ResponseHeader;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResponseHeader" type="{http://crm.huawei.com/basetype/}ResponseHeader"/>
 *         &lt;element name="RetrieveOrderResponseBody" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="TotalRowNum" type="{http://crm.huawei.com/basetype/}TotalRowNum"/>
 *                   &lt;element name="RetrieveOrderList" type="{http://crm.huawei.com/service/}RetrieveOrderList"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "responseHeader",
    "retrieveOrderResponseBody"
})
@XmlRootElement(name = "RetrieveOrderResponse")
public class RetrieveOrderResponse {

    @XmlElement(name = "ResponseHeader", required = true)
    protected ResponseHeader responseHeader;
    @XmlElement(name = "RetrieveOrderResponseBody")
    protected RetrieveOrderResponse.RetrieveOrderResponseBody retrieveOrderResponseBody;

    /**
     * Gets the value of the responseHeader property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseHeader }
     *     
     */
    public ResponseHeader getResponseHeader() {
        return responseHeader;
    }

    /**
     * Sets the value of the responseHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseHeader }
     *     
     */
    public void setResponseHeader(ResponseHeader value) {
        this.responseHeader = value;
    }

    /**
     * Gets the value of the retrieveOrderResponseBody property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveOrderResponse.RetrieveOrderResponseBody }
     *     
     */
    public RetrieveOrderResponse.RetrieveOrderResponseBody getRetrieveOrderResponseBody() {
        return retrieveOrderResponseBody;
    }

    /**
     * Sets the value of the retrieveOrderResponseBody property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveOrderResponse.RetrieveOrderResponseBody }
     *     
     */
    public void setRetrieveOrderResponseBody(RetrieveOrderResponse.RetrieveOrderResponseBody value) {
        this.retrieveOrderResponseBody = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="TotalRowNum" type="{http://crm.huawei.com/basetype/}TotalRowNum"/>
     *         &lt;element name="RetrieveOrderList" type="{http://crm.huawei.com/service/}RetrieveOrderList"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "totalRowNum",
        "retrieveOrderList"
    })
    public static class RetrieveOrderResponseBody {

        @XmlElement(name = "TotalRowNum")
        protected long totalRowNum;
        @XmlElement(name = "RetrieveOrderList", required = true)
        protected RetrieveOrderList retrieveOrderList;

        /**
         * Gets the value of the totalRowNum property.
         * 
         */
        public long getTotalRowNum() {
            return totalRowNum;
        }

        /**
         * Sets the value of the totalRowNum property.
         * 
         */
        public void setTotalRowNum(long value) {
            this.totalRowNum = value;
        }

        /**
         * Gets the value of the retrieveOrderList property.
         * 
         * @return
         *     possible object is
         *     {@link RetrieveOrderList }
         *     
         */
        public RetrieveOrderList getRetrieveOrderList() {
            return retrieveOrderList;
        }

        /**
         * Sets the value of the retrieveOrderList property.
         * 
         * @param value
         *     allowed object is
         *     {@link RetrieveOrderList }
         *     
         */
        public void setRetrieveOrderList(RetrieveOrderList value) {
            this.retrieveOrderList = value;
        }

    }

}
