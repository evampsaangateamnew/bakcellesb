package com.evampsaanga.developer.utils;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.appserver.refreshappservercache.BuildCacheRequestLand;
import com.evampsaanga.appserver.refreshappservercache.CustomerModelCache;
import com.evampsaanga.bakcell.createorder.clientsample.CreateOrderService;
import com.evampsaanga.bakcell.db.DBBakcellFactory;
import com.huawei.bss.soaif._interface.common.createorder.PaymentPlanInfo;
import com.huawei.bss.soaif._interface.common.createorder.PaymentRelation;
import com.huawei.bss.soaif._interface.common.createorder.PaymentRelation.PaymentLimit;
import com.huawei.bss.soaif._interface.orderservice.createorder.CreateOrderReqMsg;
import com.huawei.bss.soaif._interface.orderservice.createorder.CreateOrderRspMsg;
import com.huawei.bss.soaif._interface.orderservice.createorder.CreateOrderReqMsg.Order;
import com.huawei.crm.query.GetCustomerIn;
import com.huawei.crm.query.GetCustomerRequest;
import com.huawei.crm.query.GetCustomerResponse;
import com.evampsaanga.bakcell.db.DBFactory;
import com.evampsaanga.bakcell.getUsersV2.UsersGroupData;
import com.evampsaanga.bakcell.otp.MessageTemplateResponse;
import com.evampsaanga.bakcell.responseheaders.BaseResponse;
import com.evampsaanga.cache.UserGroupDataCache;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.magento.tariffdetailsv2.Datum;
import com.evampsaanga.services.CRMServices;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Helper {
	public static final Logger logger = Logger.getLogger("bakcellLogs-V2");
	public static final Logger loggerV2 = Logger.getLogger("bakcellLogs-V2");

	public static String getBakcellMoney(long amount) {
		Double amountD = (double) amount;
		NumberFormat formatter = new DecimalFormat("#0.00");
		formatter.setRoundingMode(RoundingMode.FLOOR);
		return "" + formatter.format(Double.parseDouble((amountD / Constants.MONEY_DIVIDEND) + ""));
	}

	public static String getBakcellMoneyCeilingMode(long amount) {
		NumberFormat formatter = new DecimalFormat("#0.000");
		formatter.setRoundingMode(RoundingMode.FLOOR);
		NumberFormat formatter1 = new DecimalFormat("#0.00");
		double divide = ((double) amount) / Constants.MONEY_DIVIDEND;
		divide = divide + 0.00001;
		String val = formatter.format(divide);
		double finalVal = Double.parseDouble(val);
		return formatter1.format(finalVal);
	}

	public static String dateFormattorFullDate(String date) {
		try {
			return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
					.format(new SimpleDateFormat("yyyyMMddHHmmss").parse(date));
		} catch (ParseException e) {
			logger.error(Helper.GetException(e));
		}
		return "";
	}

	public static String dateFormattorOnlyDate(String date) {
		try {
			return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new SimpleDateFormat("yyyyMMdd").parse(date));
		} catch (ParseException e) {
			logger.error(Helper.GetException(e));
		}
		return "";
	}

	public static String retrieveToken(String transactionName, String msisdn) {

		Random random = new Random();
		return "[" + transactionName + "]" + "[" + msisdn + "]" + "[" + getCurrentTimeStamp() + "-"
				+ String.format("%04d", random.nextInt(10000)) + "]";
	}

	public static String getCurrentTimeStamp() {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date());
	}

	public static String getBakcellMoneyMRC(long amount) {
		if (amount == 0)
			return "FREE";
		Double amountD = (double) amount;
		NumberFormat formatter = new DecimalFormat("#0.00");
		formatter.setRoundingMode(RoundingMode.CEILING);
		return "" + formatter.format(Double.parseDouble((amountD / Constants.MONEY_DIVIDEND) + ""));
	}

	public static boolean isValidFormat(String format, String value) {
		Date date = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			date = sdf.parse(value);
			if (!value.equals(sdf.format(date))) {
				date = null;
			}
		} catch (ParseException ex) {
		}
		return date != null;
	}

	public static <T> T JsonToObject(String json, Class<T> type) throws Exception, IOException {
		logger.debug(json);
		return new ObjectMapper().readValue(json, type);
	}

	public static <T> T JsonToObjectFile(File json, Class<T> type) throws Exception, IOException {
		logger.debug(json);
		return new ObjectMapper().readValue(json, type);
	}

	public static <T> String ObjectToJson(T type) throws Exception, IOException {
		return new ObjectMapper().writeValueAsString(type);
	}

	public static String GenerateDateTimeToMsAccuracy() {
		return new SimpleDateFormat(Constants.SQL_DATE_FORMAT).format(new Date());
	}

	public static String generateTransactionID() {
		String tID = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()).toString()
				+ RandomUtils.nextFloat() * 1000;
		return tID.substring(0, tID.indexOf('.'));
	}

	@SuppressWarnings("unused")
	private static String getValidationRegExByKey(String key) {
		return ConfigurationManager.getConfigurationFromCache(key);
	}

	public static String getNgbssMessageFromResponseCode(String code, String msg, String lang) {

		loggerV2.info("Landed in getNgbssMessageFromResponseCode Method ");
		String message = "";

		loggerV2.info("Checking key ::: " + "ngbss.response.code." + code + "." + lang);
		message = ConfigurationManager.getConfigurationFromCache("ngbss.response.code." + code + "." + lang);

		if (message == null || message.isEmpty()) {
			return ConfigurationManager.getConfigurationFromCache("actionhistory.generic.failure." + lang);
		}

		// else
		// {
		// return message;
		// }
		//
		// message= "ngbss.response.code." +code+".";
		return message;
	}

	@SuppressWarnings("unused")
	private static String getValidationErrorByKey(String key) {
		return ConfigurationManager.getConfigurationFromCache(key + ".error");
	}

	public static <T> String validateRequest(T type) {
		// Field[] fields = type.getClass().getSuperclass().getDeclaredFields();
		// for (int i = 0; i < fields.length; i++) {
		// fields[i].setAccessible(true);
		// try {
		// if
		// (!fields[i].get(type).toString().matches((getValidationRegExByKey(fields[i].getName()))))
		// {
		// return getValidationErrorByKey(fields[i].getName());
		// }
		// } catch (Exception ex) {
		// logger.error(Helper.GetException(ex));
		// }
		// }
		return "";
	}

	public static void logInfoMessage(String... input) {
		String logMessage = "";
		for (int i = 0; i < input.length; i++)
			logMessage = input[i] + "-";
		logger.info(logMessage);
	}

	public static void logInfoMessageV2(String... input) {
		String logMessage = "";
		for (int i = 0; i < input.length; i++)
			logMessage = input[i] + "-";
		loggerV2.info(logMessage);
	}

	public static void logDebugMessage(String... input) {
		String logMessage = "";
		for (int i = 0; i < input.length; i++)
			logMessage = input[i] + "-";
		logger.debug(logMessage);
	}

	public static String GetException(Exception ex) {
		return ExceptionUtils.getFullStackTrace(ex);
	}

	public static String GetException(Throwable ex) {
		return ExceptionUtils.getFullStackTrace(ex);
	}

	public static String getOMlogTimeStamp() {
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy_HH:mm:ss:SSSS");
		String timestamp = format.format(new Date());
		return timestamp + " :";
	}

	public static String round(int n) {
		// Smaller multiple
		int a = (n / 10) * 10;

		// Larger multiple
		int b = a + 10;

		// Return of closest of two
		return String.valueOf(b);
		// return (n - a > b - n)? b : a;
	}

	public static boolean checkDate(String date) throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		Date dateNew = simpleDateFormat.parse(date);
		Date d1 = new Date();
		return d1.before(dateNew);

	}

	public static String currentMachineIPAddress() {

		String IP = "";
		try {
			IP = InetAddress.getLocalHost().getHostAddress();
			loggerV2.info("---------CURRENT IP------- " + IP);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return IP;
	}

	public static String returnEmptyStringOnNull(String textCheck) {
		// TODO Auto-generated method stub
		if (textCheck != null)
			return textCheck;
		else
			return "";
	}

	public static String checkSHApass(String input, String msisdn) {

		try {
			loggerV2.info("LOGIN:" + msisdn + " " + input);
			String prefix = "xxxxxxxx";
			String postfix = ":xxxxxxxx:1";
			input = prefix + input;
			// Static getInstance method is called with hashing SHA
			MessageDigest md = MessageDigest.getInstance("SHA-256");

			// digest() method called
			// to calculate message digest of an input
			// and return array of byte
			byte[] messageDigest = md.digest(input.getBytes());

			// Convert byte array into signum representation
			BigInteger no = new BigInteger(1, messageDigest);

			// Convert message digest into hex value
			String hashtext = no.toString(16);

			while (hashtext.length() < 64) {
				hashtext = "0" + hashtext;
			}
			loggerV2.info("LOGIN:" + msisdn + " SHA256 converted " + hashtext + postfix);
			return hashtext + postfix;
		}

		// For specifying wrong message digest algorithms
		catch (NoSuchAlgorithmException e) {
			System.out.println("Exception thrown" + " for incorrect algorithm: " + e);
			loggerV2.info(Helper.GetException(e));

			return null;
		}
	}

	// public static String checkSHApass(String password,String msisdn)
	// {
	//
	//
	//
	// loggerV2.info("LOGIN:"+msisdn+"checking hash in db");
	// String data = "";
	// PreparedStatement preparedStatement = null;
	// ResultSet resultSet = null;
	// try{
	// String getAllConfiguration = "select CONCAT(SHA2('xxxxxxxx"+password+"',
	// 256), ':xxxxxxxx:1') pass";
	// preparedStatement =
	// DBFactory.getDbConnection().prepareStatement(getAllConfiguration);
	// loggerV2.info("LOGIN:"+msisdn+"query "+preparedStatement.toString());
	// resultSet = preparedStatement.executeQuery();
	// loggerV2.info("Converting SHA 256 :"+preparedStatement.toString());
	// while(resultSet.next())
	// {
	// loggerV2.info("LOGIN:"+msisdn+" "+resultSet.getString("pass"));
	//// loggerV2.info("Email FROM view : :"+resultSet.getString("email"));
	// data = resultSet.getString("pass");
	//
	// }
	//// propertiesCacheHashMap.put(resultSet.getString("key"),
	// resultSet.getString("value"));
	// }catch (Exception e) {
	// logger.error(Helper.GetException(e));
	//
	// }
	// finally {
	// try{
	// if(resultSet != null)
	// resultSet.close();
	// if(preparedStatement != null)
	// preparedStatement.close();
	// return data;
	// }catch (Exception e) {
	// logger.error(Helper.GetException(e));
	// }
	// }
	// return data;
	// }
 
	public static boolean checkMsisdnExistInDB(String msisdn) {
		/**
		 * First query to check if number exists. It takes less time and timeout
		 * issue can be avoided.
		 */
		
		loggerV2.info(msisdn+"-Check MSISDN in database");

		String query = "select count(*) as count from customer_entity_varchar WHERE `customer_entity_varchar`.`attribute_id` = '212' and value="
				+ msisdn + "";
		int userCount = 0;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try (Connection connection_1 = DBFactory.getMagentoDBConnection()) {
			preparedStatement = connection_1.prepareStatement(query);
			resultSet = preparedStatement.executeQuery();
			loggerV2.info(msisdn+"-Query check MSISDN-:" + preparedStatement.toString());

			if (resultSet.next()) {
				userCount = Integer.parseInt(resultSet.getString("count"));
				loggerV2.info(msisdn+"User Count:"+userCount);
			}
		} catch (Exception e) {
			
			logger.error(Helper.GetException(e));
			return false;
		}
		if (userCount > 0) {
			
			loggerV2.info(msisdn+"User found in database, checking customer information:"+userCount);
			
			query = "select * from ( SELECT entity_id, password_hash, email, sum(msisdn) msisdn, sum(customerid) customerid FROM ( SELECT customer_entity.entity_id, customer_entity.password_hash, customer_entity.email, ( CASE WHEN ( `customer_entity_varchar`.`attribute_id` = '212' ) THEN `customer_entity_varchar`.`value` END ) AS `msisdn`, ( CASE WHEN ( `customer_entity_varchar`.`attribute_id` = '582' ) THEN `customer_entity_varchar`.`value` END ) AS `customerid` FROM customer_entity, customer_entity_varchar WHERE customer_entity.entity_id = customer_entity_varchar.entity_id AND customer_entity_varchar.attribute_id IN (212, 582) AND customer_entity.group_id = 1 ) AS customer GROUP BY entity_id, password_hash,email ) as a WHERE msisdn="
					+ msisdn + ""; 
			preparedStatement = null;
			resultSet = null;
			try (Connection connection = DBFactory.getMagentoDBConnection()) {
				preparedStatement = connection.prepareStatement(query);
				resultSet = preparedStatement.executeQuery();
				loggerV2.info(msisdn+"-Query to Get User Data:" + preparedStatement.toString());
				if (resultSet.next()) {
					CustomerModelCache customerModelCache = new CustomerModelCache();
					customerModelCache.setEmail(resultSet.getString("email"));
					customerModelCache.setEntity_id(resultSet.getString("entity_id"));
					customerModelCache.setMsisdn(resultSet.getString("msisdn"));
					customerModelCache.setPassword_hash(resultSet.getString("password_hash"));
					customerModelCache.setCustomerId(resultSet.getString("customerid"));
					customerModelCache.setIsFromDB("true");
					BuildCacheRequestLand.customerCache.put(resultSet.getString("msisdn"), customerModelCache);
					return true;

				} else {
					return false;
				}

			} catch (Exception e) {
				logger.error(Helper.GetException(e));
				return false;

			}

		} else {
			return false;
		}

	}

	public static boolean checkAndUpdateCache(String msisdn) {
		try {
			if (BuildCacheRequestLand.customerCache == null)
				BuildCacheRequestLand.initHazelcast();
			if (BuildCacheRequestLand.customerCache.containsKey(msisdn)) {
				return true;
			} else {
				// return checkMsisdnExistInDB(msisdn);
				return false;

			}
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(Helper.GetException(e));
			return false;
		}
	}

	public static String getMessage(String section, String lang, String code) {
		MessageTemplateResponse messageTemplateResponse = ConfigurationManager
				.getMessageTemplateFromCache(Constants.GET_MESSAGE_TEMPLATE_KEY);
		String message = "";
		if (lang.equalsIgnoreCase("2")) {
			if (section.equalsIgnoreCase("signup"))
				message = messageTemplateResponse.getData().getSmsSignUpRu();
			else if (section.equalsIgnoreCase("forgot"))
				message = messageTemplateResponse.getData().getSmsForgotPasswordRu();
			else if (section.equalsIgnoreCase("usagehistory"))
				message = messageTemplateResponse.getData().getSmsUsageRu();
			else if (section.equalsIgnoreCase("moneytransfer"))
				message = messageTemplateResponse.getData().getSmsMoneyTransferRU();
		} else if (lang.equalsIgnoreCase("4")) {
			if (section.equalsIgnoreCase("signup"))
				message = messageTemplateResponse.getData().getSmsSignUpAz();
			else if (section.equalsIgnoreCase("forgot"))
				message = messageTemplateResponse.getData().getSmsForgotPasswordAz();
			else if (section.equalsIgnoreCase("usagehistory"))
				message = messageTemplateResponse.getData().getSmsUsageAz();
			else if (section.equalsIgnoreCase("moneytransfer"))
				message = messageTemplateResponse.getData().getSmsMoneyTransferAZ();
		} else {
			if (section.equalsIgnoreCase("signup"))
				message = messageTemplateResponse.getData().getSmsSignUpEn();
			else if (section.equalsIgnoreCase("forgot"))
				message = messageTemplateResponse.getData().getSmsForgotPasswordEn();
			else if (section.equalsIgnoreCase("usagehistory"))
				message = messageTemplateResponse.getData().getSmsUsageEn();
			else if (section.equalsIgnoreCase("moneytransfer"))
				message = messageTemplateResponse.getData().getSmsMoneyTransferEn();
		}
		if (message != null && message.length() > 0)
			message = message.replace("@code@", code);
		return message;
	}

	public static String getDateOfLastMonth(String billMonth) throws ParseException {
		Calendar aCalendar = Calendar.getInstance();
		// add -1 month to current month
		aCalendar.add(Calendar.MONTH, Integer.parseInt(billMonth));
		// set DATE to 1, so first date of previous month
		aCalendar.set(Calendar.DATE, 1);
		Date firstDateOfPreviousMonth = aCalendar.getTime();

		SimpleDateFormat f = new SimpleDateFormat("yyyyMMdd");
		return f.format(firstDateOfPreviousMonth).toString();
	}

	public static double roundUptoDecimal(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();

		long factor = (long) Math.pow(10, places);
		value = value * factor;
		long tmp = Math.round(value);
		return (double) tmp / factor;
	}

	public static String getSequenceNumber(String msisdn) {

		String sequenceNumber = "0";
		String query = "select CREDIT_LIMIT_SEQ from V_E_CARE_CORP_INFO where PRI_IDENTITY = " + msisdn;
		try {
			Helper.logInfoMessageV2("Query to count users: " + query);
			ResultSet rs = DBBakcellFactory.getConnection().prepareStatement(query).executeQuery();
			if (rs.next())
				sequenceNumber = rs.getString(1);

		} catch (SQLException e) {
			logger.info("ERROR: ", e);
		}
		Helper.logInfoMessageV2("Sequence Number is: " + sequenceNumber);
		return sequenceNumber;
	}

	public static GetCustomerResponse getCustomerResponse(String msisdn) throws IOException, Exception {
		loggerV2.info("*************************GET_CUSTOMER**************************");
		loggerV2.info("");
		loggerV2.info(msisdn + ":GET_CUSTOMER");
		GetCustomerRequest getCustomerRequest = new GetCustomerRequest();
		getCustomerRequest.setRequestHeader(CRMServices.getReqHeaderForGETNetworkSetting());
		GetCustomerIn getCustomerIn = new GetCustomerIn();
		getCustomerIn.setServiceNumber(msisdn);

		getCustomerRequest.setGetCustomerBody(getCustomerIn);
		loggerV2.info(msisdn + ":" + Helper.ObjectToJson(getCustomerRequest));
		GetCustomerResponse getCustomerResponse = CRMServices.getInstance().getCustomerData(getCustomerRequest);
		loggerV2.info(msisdn + ":" + Helper.ObjectToJson(getCustomerResponse));
		loggerV2.info("*******************************END*****************************");
		return getCustomerResponse;
	}

	public static boolean isValidIntegerAndFloat(String s) {
		// The given argument to compile() method
		// is regular expression. With the help of
		// regular expression we can validate mobile
		// number.
		// 1) Begins with 0 or 91
		// 2) Then contains 7 or 8 or 9.
		// 3) Then contains 9 digits
		Pattern p = Pattern.compile("^(?=.)([+-]?([0-9]*)(\\.([0-9]+))?)$");

		// Pattern class contains matcher() method
		// to find matching between given number
		// and regular expression
		Matcher m = p.matcher(s);
		return (m.find() && m.group().equals(s));
	}

	public static int compare(String offeringID, List<Datum> list) {
		// TODO Auto-generated method stub
		System.out.println(list.toString());

		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getHeader().getOfferingId().equals(offeringID))
				return i;

		}
		return -1;
	}

	public static void changePaymentRelation(String groupName, String msisdn, double mrcToBePassedChangePayment,
			String paymentLimitKey) {
		// TODO Auto-generated method stub
		loggerV2.info("-------START OF CHANGE PAYMENT RELATION-----");

		int myInt = (int) mrcToBePassedChangePayment;
		String valuelimit = Integer.toString(myInt);
		loggerV2.info("Final Value toPAssed in changePAyment :" + valuelimit);

		Long limitValue = ((long) Double.parseDouble(valuelimit) * Constants.MONEY_DIVIDEND);
		CreateOrderReqMsg createOrderReqMsg = new CreateOrderReqMsg();
		createOrderReqMsg.setReqHeader(CreateOrderService.getCreateOderHeader());
		Order order = new Order();
		order.setOrderType("CO076");
		createOrderReqMsg.setOrder(order);
		PaymentPlanInfo paymentPlanInfo = new PaymentPlanInfo();
		paymentPlanInfo.setAccountID(1010003024611L);
		paymentPlanInfo.setServiceNumber(msisdn);

		PaymentRelation paymentRelation = new PaymentRelation();
		paymentRelation.setActionType("2");
		paymentRelation.setPaymentRelationId(paymentLimitKey);
		paymentRelation.getServiceType().add("-1");
		PaymentLimit paymentLimit = new PaymentLimit();
		paymentLimit.setLimitMeasureUnit("101");
		if (groupName.equals("PartPay")) {
			paymentLimit.setLimitPattern("1");
		} else {
			paymentLimit.setLimitPattern("3");
		}

		paymentLimit.setLimitUnit("1");
		paymentLimit.setLimitValue(limitValue.toString());
		paymentRelation.setPaymentLimit(paymentLimit);

		paymentPlanInfo.getPaymentRelationList().add(paymentRelation);
		createOrderReqMsg.setPaymentPlanList(paymentPlanInfo);

		CreateOrderRspMsg res = CreateOrderService.getInstance().createOrder(createOrderReqMsg);
		System.out.println("***********************");

		loggerV2.info("ChangePAyment ResultCode: " + res.getRspHeader().getReturnCode());
		loggerV2.info("ChangePAyment ResultMsg: " + res.getRspHeader().getReturnMsg());
		loggerV2.info("ChangePAyment orderId: " + res.getOrderId());
		loggerV2.info("--------END OF CHANGE PAYMENT RELATION-------");

	}

	public static String getCustomerCRMAccountId(String customerId, String corporateMsisdn, String msisdn)
			throws IOException, Exception {
		ArrayList<UsersGroupData> usersGroupData = new ArrayList<UsersGroupData>();
		JSONObject getGroupRequest = new JSONObject();
		getGroupRequest.put("lang", "3");
		getGroupRequest.put("iP", "9.9.9.9");
		getGroupRequest.put("channel", "web");
		getGroupRequest.put("msisdn", corporateMsisdn);
		getGroupRequest.put("isB2B", "true");
		getGroupRequest.put("customerID", customerId);
		UserGroupDataCache userCache = new UserGroupDataCache();
		usersGroupData = (ArrayList<UsersGroupData>) userCache.checkUsersInCache(getGroupRequest.toString());

		String customerCrmAccountId = null;
		for (int k = 0; k < usersGroupData.size(); k++) {
			if (usersGroupData.get(k).getMsisdn().equals(msisdn))
				;
			customerCrmAccountId = usersGroupData.get(k).getCorp_crm_acct_id();
		}
		return customerCrmAccountId;
	}

	public static String getLang(String lang) {
		String language = "";
		if (lang != null && lang.equals(Constants.LANGUAGE_AZERI_MAPPING)) {
			language = Constants.LANGUAGE_AZERI_MAPPING_DESC;
		} else if (lang != null && lang.equals(Constants.LANGUAGE_ENGLISH_MAPPING)) {
			language = Constants.LANGUAGE_ENGLISH_MAPPING_DESC;
		} else if (lang != null && lang.equals(Constants.LANGUAGE_RUSSIAN_MAPPING)) {
			language = Constants.LANGUAGE_RUSSIAN_MAPPING_DESC;
		} else {
			language = Constants.LANGUAGE_ENGLISH_MAPPING_DESC;
		}
		return language;
	}

	public static void main(String[] args) {

		 
//		System.out.println(Helper.checkMsisdnExistInDB("559409267"));
//		Double amountD = (double) 966666;
//		NumberFormat formatter = new DecimalFormat("#0.00");
//		formatter.setRoundingMode(RoundingMode.UP);
//		System.out.println((amountD / Constants.MONEY_DIVIDEND));
//		System.out.println(formatter.format(Math.round((amountD / Constants.MONEY_DIVIDEND))));
 	}

	public static boolean ifAttributeExists(String getmsisdn) {
		// TODO Auto-generated method stub
		return false;
	}
	public static String getCurrentDate(String formatter) {
		// Create formatter
		DateTimeFormatter FOMATTER = DateTimeFormatter.ofPattern(formatter);

		// Local date time instance
		LocalDateTime localDateTime = LocalDateTime.now();

		// Get formatted String
		return FOMATTER.format(localDateTime);
	}
}
