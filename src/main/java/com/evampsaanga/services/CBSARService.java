package com.evampsaanga.services;

import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.developer.utils.SoapHandlerService;
import com.huawei.bme.cbsinterface.arservices.ArServices;
import com.huawei.bme.cbsinterface.arservices.ArServices_Service;
import com.huawei.bme.cbsinterface.cbscommon.OperatorInfo;
import com.huawei.bme.cbsinterface.cbscommon.OwnershipInfo;
import com.huawei.bme.cbsinterface.cbscommon.RequestHeader;
import com.huawei.bme.cbsinterface.cbscommon.SecurityInfo;
import com.huawei.bme.cbsinterface.cbscommon.RequestHeader.TimeFormat;

public class CBSARService {
	private static ArServices CBSARPortType = null;

	private CBSARService() {
	}

	public static synchronized ArServices getInstance() {
		if (CBSARPortType == null) {
			CBSARPortType = new ArServices_Service().getArServicesPort();
			SoapHandlerService.configureBinding(CBSARPortType);
		}
		return CBSARPortType;
	}

	public static RequestHeader getRequestPaymentRequestHeader() {
		RequestHeader reqH = new RequestHeader();
		reqH.setVersion(Constants.TOPUP_POSTPAID_VERSION);
		reqH.setBusinessCode(Constants.TOPUP_POSTPAID_BUSINESS_CODE);
		reqH.setMessageSeq(Helper.generateTransactionID());
		OwnershipInfo ownerShipInfo = new OwnershipInfo();
		ownerShipInfo.setBEID(Constants.TOPUP_POSTPAID_BEID);
		ownerShipInfo.setBRID(Constants.TOPUP_POSTPAID_BRID);
		reqH.setOwnershipInfo(ownerShipInfo);
		SecurityInfo accesSecurityInfo = new SecurityInfo();
		accesSecurityInfo.setLoginSystemCode(ConfigurationManager.getConfigurationFromCache("cbs.username"));
		accesSecurityInfo.setPassword(ConfigurationManager.getConfigurationFromCache("cbs.password"));
		reqH.setAccessSecurity(accesSecurityInfo);
		OperatorInfo operatorInfo = new OperatorInfo();
		operatorInfo.setOperatorID(Constants.TOPUP_POSTPAID_OPERATORID);
		operatorInfo.setChannelID(Constants.TOPUP_POSTPAID_CHANNELID);
		reqH.setOperatorInfo(operatorInfo);
		reqH.setAccessMode(Constants.TOPUP_POSTPAID_ACCESS_MODE);
		reqH.setMsgLanguageCode(Constants.TOPUP_POSTPAID_LANGUAGE_CODE);
		TimeFormat timeFormat = new TimeFormat();
		timeFormat.setTimeType(Constants.TOPUP_POSTPAID_TIME_TYPE);
		timeFormat.setTimeZoneID(Constants.TOPUP_POSTPAID_TIME_ZONEID);
		reqH.setTimeFormat(timeFormat);
		return reqH;
	}
}
