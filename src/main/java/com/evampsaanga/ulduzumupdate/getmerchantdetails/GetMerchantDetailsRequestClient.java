package com.evampsaanga.ulduzumupdate.getmerchantdetails;

import com.evampsaanga.bakcell.requestheaders.BaseRequest;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetMerchantDetailsRequestClient extends BaseRequest {
	private String merchantid;

	/**
	 * @return the merchantid
	 */
	public String getMerchantid() {
		return merchantid;
	}

	/**
	 * @param merchantid
	 *            the merchantid to set
	 */
	public void setMerchantid(String merchantid) {
		this.merchantid = merchantid;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "GetMerchantDetailsRequestClient [merchantid=" + merchantid + ", getIsB2B()=" + getIsB2B()
				+ ", getLang()=" + getLang() + ", getmsisdn()=" + getmsisdn() + ", getChannel()=" + getChannel()
				+ ", getiP()=" + getiP() + ", toString()=" + super.toString() + "]";
	}

}
