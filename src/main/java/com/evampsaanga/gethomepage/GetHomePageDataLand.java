package com.evampsaanga.gethomepage;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.ws.WebServiceException;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.amqimplementationsesb.LogsType;
import com.evampsaanga.bakcell.hlrweb.getquerybalanceRequest.HLRBalanceServices;
import com.evampsaanga.bakcell.ussdgwqueryfreeresource.FreeResourcesUSSDservices;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.developer.utils.SOAPLoggingHandler;
import com.evampsaanga.services.BcService;
import com.evampsaanga.services.SubscriberService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.huawei.bme.cbsinterface.arservices.QueryLoanLogResult;
import com.huawei.bme.cbsinterface.arservices.QueryLoanLogResult.LoanLogDetail;
import com.huawei.bme.cbsinterface.arservices.QueryLoanLogResultMsg;
import com.huawei.bme.cbsinterface.bccommon.OfferingKey;
import com.huawei.bme.cbsinterface.bccommon.SubAccessCode;
import com.huawei.bme.cbsinterface.bcservices.QueryOfferingRentCycleRequest;
import com.huawei.bme.cbsinterface.bcservices.QueryOfferingRentCycleRequest.OfferingInst;
import com.huawei.bme.cbsinterface.bcservices.QueryOfferingRentCycleRequest.OfferingInst.OfferingOwner;
import com.huawei.bme.cbsinterface.bcservices.QueryOfferingRentCycleRequestMsg;
import com.huawei.bme.cbsinterface.bcservices.QueryOfferingRentCycleResult.OfferingRentCycle;
import com.huawei.bme.cbsinterface.bcservices.QueryOfferingRentCycleResultMsg;
import com.huawei.bme.cbsinterface.bcservices.QuerySubLifeCycleRequest;
import com.huawei.bme.cbsinterface.bcservices.QuerySubLifeCycleRequestMsg;
import com.huawei.bme.cbsinterface.bcservices.QuerySubLifeCycleResultMsg;
import com.huawei.bss.soaif._interface.common.Installment;
import com.huawei.bss.soaif._interface.common.ReqHeader;
import com.huawei.bss.soaif._interface.common.Resource;
import com.huawei.bss.soaif._interface.hlrwebservice.HLRWebService;
import com.huawei.bss.soaif._interface.hlrwebservice.QueryBalanceIn;
import com.huawei.bss.soaif._interface.hlrwebservice.QueryBalanceRequest;
import com.huawei.bss.soaif._interface.hlrwebservice.QueryBalanceResponse;
import com.huawei.bss.soaif._interface.subscriberservice.QueryHandsetInstallmentReqMsg;
import com.huawei.bss.soaif._interface.subscriberservice.QueryHandsetInstallmentRspMsg;
import com.huawei.crm.basetype.RequestHeader;

@Path("/bakcell/")
public class GetHomePageDataLand {
	public static final Logger logger = Logger.getLogger("bakcellLogs-V2");

	public GetHomePageResponse queryLoanLog(String msisdn, GetHomePageResponse res) {
		QueryLoanLog lo = new QueryLoanLog();
		QueryLoanLogResultMsg loanResponse = lo.queryLoanLog(msisdn);
		if (loanResponse.getResultHeader().getResultCode().equals("0")) {
			Long totalCredit = 0L;
			QueryLoanLogResult loanLog = loanResponse.getQueryLoanLogResult();
			if (loanLog != null && loanLog.getRepaymentLogDetail() != null) {
				List<LoanLogDetail> loanLogDetails = loanLog.getLoanLogDetail();
				for (int i = 0; i < loanLogDetails.size(); i++) {
					if (loanLogDetails.get(i).getLoanStatus().equalsIgnoreCase(Constants.LOAN_STATUS_OPEN)) {
						totalCredit += loanLogDetails.get(i).getRepaymentAMT() - loanLogDetails.get(i).getPaidAMT();
					}
				}
			}
			Credit credit = new Credit();
			credit.setCreditTitleValue(Helper.getBakcellMoney(totalCredit));
			res.getData().setCredit(credit);
		}
		return res;
	}

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetHomePageResponse Get(@Header("credentials") String credential, @Body() String requestBody) {
		Logs logs = new Logs();
		try {
			logs.setTableType(LogsType.HomePage);
			logs.setTransactionName(LogsType.AppMenu.name());
			logger.info("GetHomePageDataLand request Landed:" + requestBody);
			GetHomePageRequestClient cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, GetHomePageRequestClient.class);
				if (cclient != null) {
					if (cclient.getIsB2B() != null && cclient.getIsB2B().equals("true"))
						logs.setTransactionName(Transactions.GET_HOME_PAGE_B2B);

					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setUserType(cclient.getCustomerType());
					logs.setIsB2B(cclient.getIsB2B());
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				GetHomePageResponse resp = new GetHomePageResponse();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
					GetHomePageResponse resp = new GetHomePageResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials == null) {
					GetHomePageResponse resp = new GetHomePageResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						GetHomePageResponse res = new GetHomePageResponse();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						logs.setResponseCode(res.getReturnCode());
						logs.setResponseDescription(res.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return res;
					}
				} else {
					GetHomePageResponse resp = new GetHomePageResponse();
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					GetHomePageResponse res = new GetHomePageResponse();
					res.getData().setMrc(getMRC(cclient));
					try {
						FreeResourcesUSSDservices resources = new FreeResourcesUSSDservices();
						res.getData().setFreeResources(
								resources.getFreeResources(cclient.getmsisdn(), cclient.getTariffType()));
					} catch (Exception ex) {
						logger.error(Helper.GetException(ex));
					}
					try {
						res = queryLoanLog(cclient.getmsisdn(), res);
					} catch (Exception ex) {
						logger.error(Helper.GetException(ex));
					}
					double minAmountTobePaid = 0.0;
					try {

						boolean dateCheck = false;

						HLRBalanceServices service = new HLRBalanceServices();
						QueryBalanceRequest qBR = new QueryBalanceRequest();
						qBR.setReqHeader(getReqHeaderForQueryBalanceHLR());
						QueryBalanceIn qBI = new QueryBalanceIn();
						qBI.setPrimaryIdentity(cclient.getmsisdn());
						qBR.setQueryBalanceBody(qBI);
						QueryBalanceResponse balanceResponse = HLRWebService.getInstance().queryBalance(qBR);
						res.getData().setBalance(
								service.getBalance(cclient.getmsisdn(), cclient.getCustomerType(), balanceResponse));
						logger.info("****************************************************");
						logger.info("AMOUN_TO_BE_PAID: check for B01 B02 Postpaid");
						logger.info("STATUS_CODE: " + cclient.getStatusCode());
						logger.info("check if B03 " + cclient.getStatusCode().equalsIgnoreCase("B03"));
						logger.info("check if B04 " + cclient.getStatusCode().equalsIgnoreCase("B04"));
						logger.info("check if Postpaid " + cclient.getSubscriberType().equalsIgnoreCase("postpaid"));
						logger.info("STATUS_CODE: " + cclient.getStatusCode());
						if ((cclient.getStatusCode().equalsIgnoreCase("B03")
								|| cclient.getStatusCode().equalsIgnoreCase("B04"))
								&& cclient.getSubscriberType().equalsIgnoreCase("postpaid")) {

							if (balanceResponse != null
									&& (balanceResponse.getRspHeader().getReturnCode().equals("0"))) {
								logger.info("check if balance response is null and return code is 0");

								if (balanceResponse.getQueryBalanceBody().get(0).getOutStandingList() != null
										&& balanceResponse.getQueryBalanceBody().get(0).getOutStandingList()
												.size() > 0) {
									logger.info(
											"check if outstanding balance is not null and outstanding list is greater than 0");
									// LifeCycle case
									if (balanceResponse.getQueryBalanceBody().get(0).getAccountCredit().get(0)
											.getCreditLimitType().equalsIgnoreCase("C_TOTAL_CREDIT_LIMIT")) {
										logger.info("check if credit limit type is C_TOTAL_CREDIT_LIMIT");
										logger.info("AMOUN_TO_BE_PAID:LifeCycle case");
										for (int i = 0; i < balanceResponse.getQueryBalanceBody().get(0)
												.getOutStandingList().size(); i++) {
											// return true if date is not passed
											// false if date is passed
											if (Helper.checkDate(balanceResponse.getQueryBalanceBody().get(0)
													.getOutStandingList().get(i).getDueDate())) {
												dateCheck = true;

											}
											if (!dateCheck) {
												// Date Passed
												minAmountTobePaid = Double.parseDouble(res.getData().getBalance()
														.getPostpaid().getOutstandingIndividualDept() + 0.01);
												logger.info("AMOUN_TO_BE_PAID: minAmounttobe Paid" + minAmountTobePaid);
											} else {
												// date not passed
												minAmountTobePaid = Double.parseDouble(res.getData().getBalance()
														.getPostpaid().getAvailableBalanceIndividualValue()) + 0.01;
												logger.info("AMOUN_TO_BE_PAID: minAmounttobe Paid" + minAmountTobePaid);
											}

										}

									} else {
										logger.info("AMOUN_TO_BE_PAID: Bill Cycle");
										double outstanding = 0;
										// Bill Cycle
										// return true if date is not passed
										// false if date is passed
										for (int i = 0; i < balanceResponse.getQueryBalanceBody().get(0)
												.getOutStandingList().size(); i++) {
											// return true if date is not passed
											// false if date is passed
											if (Helper.checkDate(balanceResponse.getQueryBalanceBody().get(0)
													.getOutStandingList().get(i).getDueDate())) {
												dateCheck = true;
												logger.info("AMOUN_TO_BE_PAID: data check " + dateCheck);

											}
											if (!dateCheck) {
												outstanding += Double.parseDouble(balanceResponse.getQueryBalanceBody()
														.get(0).getOutStandingList().get(i).getOutStandingDetail()
														.getOutStandingAmount());
												logger.info("AMOUN_TO_BE_PAID: outstanding " + outstanding);
												// Date Passed
												// amount to be paid
												// minAmountTobePaid =
												// Double.parseDouble(res.getData().getBalance().getPostpaid().getOutstandingIndividualDept()+0.01);
											} else {
												// date not passed
												minAmountTobePaid = Double.parseDouble(res.getData().getBalance()
														.getPostpaid().getOutstandingIndividualDept()) + 0.01;
												logger.info("AMOUN_TO_BE_PAID: minAmounttobe Paid" + minAmountTobePaid);
												// minAmountTobePaid =
												// Double.parseDouble(res.getData().getBalance().getPostpaid().getAvailableBalanceIndividualValue())+0.01;
											}

										}
										if (!dateCheck)
											minAmountTobePaid = outstanding + 0.01;

										// else
										// minAmountTobePaid=Double.parseDouble(balanceResponse.getQueryBalanceBody().get(0).getOutStandingList().get(i).getOutStandingDetail().getOutStandingAmount()+0.01);
									}
								} else {
									// to be converted to string
									minAmountTobePaid = Math.abs(Double.parseDouble(res.getData().getBalance()
											.getPostpaid().getAvailableBalanceIndividualValue())) + 0.01;
									logger.info("AMOUN_TO_BE_PAID: minAmounttobe Paid" + minAmountTobePaid);

								}
							}
						}

						logger.info("AMOUNT_TO_BE_PAID: minAmounttobe Paid FINAL" + minAmountTobePaid);
						logger.info("****************************************************");
						// res.getData().getBalance().setMinAmountTeBePaid(minAmountTobePaid+"");
						ObjectMapper mapper = new ObjectMapper();
						logger.info("BALANCE IS: " + mapper.writeValueAsString(res.getData().getBalance()));
						// logger.info("MinAmount IS resp : " +
						// res.getData().getBalance().getMinAmountTeBePaid());
					} catch (Exception ex) {
						logger.error(Helper.GetException(ex));
					}
					try {
						QuerySubLifeCycleResultMsg response = getsublife(cclient.getmsisdn());
						if (response.getResultHeader().getResultCode().equalsIgnoreCase("0")) {
							String index = response.getQuerySubLifeCycleResult().getCurrentStatusIndex();
							for (int lifecycle = 0; lifecycle < response.getQuerySubLifeCycleResult()
									.getLifeCycleStatus().size(); lifecycle++) {
								if (response.getQuerySubLifeCycleResult().getLifeCycleStatus().get(lifecycle)
										.getStatusIndex().equalsIgnoreCase(index)) {
									res.getData().getCredit()
											.setCreditDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
													.format(new SimpleDateFormat("yyyyMMddHHmmss").parse(
															response.getQuerySubLifeCycleResult().getLifeCycleStatus()
																	.get(lifecycle).getStatusExpireTime())));
									Calendar calendar = Calendar.getInstance();
									calendar.add(Calendar.DATE, -10);
									res.getData().getCredit().setCreditInitialDate(
											new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calendar.getTime()));
									if (index.equalsIgnoreCase("2"))
										res.getData().getCredit()
												.setCreditDateLabel(Constants.QUERY_SUBSCRIBER_LIFE_CYCLE_STATUS_B1W);
									else if (index.equalsIgnoreCase("3"))
										res.getData().getCredit()
												.setCreditDateLabel(Constants.QUERY_SUBSCRIBER_LIFE_CYCLE_STATUS_B2W);
									else if (index.equalsIgnoreCase("4"))
										res.getData().getCredit()
												.setCreditDateLabel(Constants.QUERY_SUBSCRIBER_LIFE_CYCLE_STATUS_EXP);
									
									String remainingCreditDays="0";
									String creditDays=getDays(res.getData().getCredit()
											.getCreditInitialDate(),res.getData().getCredit().getCreditDate());
									String progressDays=getDays(res.getData().getCredit()
											.getCreditInitialDate(),
											new SimpleDateFormat(Constants.SQL_DATE_FORMAT_required).format(new Date()));
									remainingCreditDays=(Integer.parseInt(creditDays)-Integer.parseInt(progressDays))+"";									res.getData().getCredit().setCreditDays(creditDays);
									res.getData().getCredit().setProgressDays(progressDays);
//									remainingDays//
									res.getData().getCredit().setRemainingCreditDays(remainingCreditDays);
								}
							}
						}
					} catch (Exception e) {
						logger.error(Helper.GetException(e));
					}
					QueryHandsetInstallmentRspMsg handsetInstallment = null;
					try {
						handsetInstallment = getHandsetInstallments(cclient.getmsisdn());
						if (handsetInstallment != null)
							if (handsetInstallment.getRspHeader().getReturnCode().equals("0000")) {
								for (Resource rs : handsetInstallment.getResource()) {
									Installment inst = getLastInstallment(rs.getInstallment());
									if (!inst.getInstallmentStatus().equalsIgnoreCase("F")) {
										com.evampsaanga.gethomepage.Installment instObject = new com.evampsaanga.gethomepage.Installment();
										if (rs.getResourceType().equals("40"))
											instObject.setName(rs.getResourceModel());
										instObject.setAmountValue(
												(inst.getTotalAmount() / Constants.MONEY_DIVIDEND) + "");
										instObject.setInstallmentFeeLimit(ConfigurationManager
												.getConfigurationFromCache("handsetinstallment.limitfee").trim() + "");
										instObject.setCurrency(Constants.CURRENCY_AZN);
										instObject.setRemainingAmountTotalValue(
												inst.getTotalAmount() / Constants.MONEY_DIVIDEND + "");
										instObject.setRemainingAmountCurrentValue(
												inst.getRemainingAmount() / Constants.MONEY_DIVIDEND + "");
										instObject.setRemainingCurrentPeriod(inst.getRemainingPeriods() + "");
										instObject.setRemainingTotalPeriod(inst.getTotalPeriods() + "");
										try {
											instObject.setRemainingPeriodBeginDateValue(
													new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
															.format(new SimpleDateFormat("yyyyMMddHHmmss")
																	.parse(inst.getBeginDate())));
										} catch (Exception ex) {
											logger.error(Helper.GetException(ex));
										}
										try {
											instObject.setRemainingPeriodEndDateValue(
													new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
															.format(new SimpleDateFormat("yyyyMMddHHmmss")
																	.parse(inst.getEndDate())));
										} catch (Exception ex) {
											logger.error(Helper.GetException(ex));
										}
										try {
											instObject.setPurchaseDateValue(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
													.format(new SimpleDateFormat("yyyyMMddHHmmss")
															.parse(rs.getPurchaseDate())));
										} catch (Exception e) {
											logger.error(Helper.GetException(e));
										}
										instObject.setInstallmentStatus(ConfigurationManager.getConfigurationFromCache(
												ConfigurationManager.MAPPING_INSTALLMENTSTATUS
														+ inst.getInstallmentStatus()));
										res.getData().getInstallments().getInstallments().add(instObject);
									}
								}
							}
					} catch (Exception ex) {
						logger.error(Helper.GetException(ex));
					}
					res.setReturnCode(ResponseCodes.SUCESS_CODE_200);
					res.setReturnMsg(ResponseCodes.SUCESS_DES_200);
					logs.setResponseCode(res.getReturnCode());
					logs.setResponseDescription(res.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return res;
				} else {
					GetHomePageResponse resp = new GetHomePageResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		GetHomePageResponse resp = new GetHomePageResponse();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);

		try {
			logger.debug("Response Returned Home Page: " + new ObjectMapper().writeValueAsString(resp));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resp;
	}

	private String getDays(String creditDate, String creditInitialDate) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDate firstDate = LocalDate.parse(creditDate, formatter);
        LocalDate secondDate = LocalDate.parse(creditInitialDate, formatter);
        long days = ChronoUnit.DAYS.between(firstDate, secondDate);
        logger.info("Days between: " + days);
        return days+"";
	}

	public Installment getLastInstallment(List<Installment> installment) throws ParseException {
		Installment lastInstallment = new Installment();
		Date date = new SimpleDateFormat("yyyyMMddHHmmss").parse("19930101000000");
		for (Installment instObject : installment) {
			try {
				Date latestD = new SimpleDateFormat("yyyyMMddHHmmss").parse(instObject.getBeginDate());
				if (latestD.after(date)) {
					date = latestD;
					lastInstallment = instObject;
				}
			} catch (ParseException ex) {
				logger.error(Helper.GetException(ex));
			}
		}
		return lastInstallment;
	}

	public QueryHandsetInstallmentRspMsg getHandsetInstallments(String msisdn) {
		com.huawei.bss.soaif._interface.common.ReqHeader reqHHLR = new com.huawei.bss.soaif._interface.common.ReqHeader();
		reqHHLR.setChannelId(getRequestHeaderForCRM().getChannelId());
		reqHHLR.setAccessUser(getRequestHeaderForCRM().getAccessUser());
		reqHHLR.setAccessPwd(getRequestHeaderForCRM().getAccessPwd());
		reqHHLR.setTransactionId(Helper.generateTransactionID());
		reqHHLR.setTenantId(getRequestHeaderForCRM().getTenantId());
		reqHHLR.setOperatorId(getRequestHeaderForCRM().getAccessUser());
		QueryHandsetInstallmentReqMsg reqMsg = new QueryHandsetInstallmentReqMsg();
		reqMsg.setServiceNumber(msisdn);
		reqMsg.setReqHeader(reqHHLR);
		return SubscriberService.getInstance().queryHandsetInstallment(reqMsg);
	}

	public GetHomePageResponse getQueryBalance(GetHomePageRequestClient cclient, GetHomePageResponse responseMain) {
		try {
			HLRBalanceServices service = new HLRBalanceServices();
			Balance balance = service.getBalance(cclient.getmsisdn(), cclient.getCustomerType(), null);
			if (balance.getReturnCode().equals("0")) {
				responseMain.getData().setBalance(balance);
			}
		} catch (Exception ee) {
			logger.error(Helper.GetException(ee));
			if (ee instanceof WebServiceException) {
			}
		}
		return responseMain;
	}

	public ReqHeader getReqHeaderForQueryBalanceHLR() {
		ReqHeader reqHForQBalance = new ReqHeader();
		reqHForQBalance.setChannelId(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.ChannelId").trim());
		reqHForQBalance.setAccessUser(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.AccessUser").trim());
		reqHForQBalance.setAccessPwd(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.AccessPwd").trim());
		reqHForQBalance.setMIDWAREChannelID(
				ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.MIDWAREChannelID").trim());
		reqHForQBalance.setMIDWAREAccessPwd(
				ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.MIDWAREAccessPwd").trim());
		reqHForQBalance.setMIDWAREAccessUser(
				ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.MIDWAREAccessUser").trim());
		reqHForQBalance.setTransactionId(Helper.generateTransactionID());
		return reqHForQBalance;
	}

	public RequestHeader getRequestHeaderForCRM() {
		RequestHeader reqhForCRM = new RequestHeader();
		reqhForCRM.setChannelId(ConfigurationManager.getConfigurationFromCache("crm.sub.ChannelId").trim());
		reqhForCRM.setTechnicalChannelId(
				ConfigurationManager.getConfigurationFromCache("crm.sub.TechnicalChannelId").trim());
		reqhForCRM.setAccessUser(ConfigurationManager.getConfigurationFromCache("crm.sub.AccessUser").trim());
		reqhForCRM.setTenantId(ConfigurationManager.getConfigurationFromCache("crm.sub.TenantId").trim());
		reqhForCRM.setAccessPwd(ConfigurationManager.getConfigurationFromCache("crm.sub.AccessPwd").trim());
		reqhForCRM.setTestFlag(ConfigurationManager.getConfigurationFromCache("crm.sub.TestFlag").trim());
		reqhForCRM.setLanguage(ConfigurationManager.getConfigurationFromCache("crm.sub.Language").trim());
		reqhForCRM.setTransactionId(Helper.generateTransactionID());
		return reqhForCRM;
	}

	public QuerySubLifeCycleResultMsg getsublife(String msisdn) {
		QuerySubLifeCycleRequestMsg requestMsg = new QuerySubLifeCycleRequestMsg();
		requestMsg.setRequestHeader(BcService.getRequestHeader());
		QuerySubLifeCycleRequest lifeC = new QuerySubLifeCycleRequest();
		SubAccessCode subACode = new SubAccessCode();
		subACode.setPrimaryIdentity(msisdn);
		lifeC.setSubAccessCode(subACode);
		requestMsg.setQuerySubLifeCycleRequest(lifeC);
		QuerySubLifeCycleResultMsg result = BcService.getInstance().querySubLifeCycle(requestMsg);
		return result;
	}

	private Mrc getMRC(GetHomePageRequestClient cClient) {
		Mrc mrc = new Mrc();
		// check if user is GUNBOUY or KLASS
		logger.info("Get MRC for " + cClient.getmsisdn());
		String offferingIDsgunboyCinNew = ConfigurationManager
				.getConfigurationFromCache("mrc.offering.offeringIds.gunboy.cinew");
		logger.info("cinNew_GunBoyOfferingIDs String: " + offferingIDsgunboyCinNew);
		List<String> cinNewGunBoyOfferingIds = new ArrayList<String>(
				Arrays.asList(offferingIDsgunboyCinNew.split(",")));
		logger.info("cinNew_GunBoyOfferingIDs List: " + cinNewGunBoyOfferingIds);

		if (cClient.getBrandId().equalsIgnoreCase(Constants.KLASS_BRAND_ID)
				|| (cClient.getBrandId().equalsIgnoreCase(Constants.CIN_BRAND_ID)
						&& cinNewGunBoyOfferingIds.contains(cClient.getOfferingId()))

		) {
			logger.info("In IF check KLAss cinNew_GunBoyOfferingIDs" + cinNewGunBoyOfferingIds);
			String offeringId = cClient.getOfferingId();
			// changing offering id in request object as per MRC mapping
			cClient.setOfferingId(
					ConfigurationManager.getConfigurationFromCache("mrc.offering." + cClient.getOfferingId()));
			mrc.setMrcTitleValue(ConfigurationManager.getConfigurationFromCache("mrc.price." + offeringId));
			mrc.setMrcType(getMRCType(offeringId));
			logger.info("Offering Key Mapping: " + cClient.getOfferingId());
			try {
				com.huawei.crm.query.GetSubscriberResponse respns = new com.evampsaanga.bakcell.getsubscriber.CRMSubscriberService()
						.GetSubscriberRequest(cClient.getmsisdn());
				if (respns.getResponseHeader().getRetCode().equals("0")) {
					for (int i = 0; i < respns.getGetSubscriberBody().getSupplementaryOfferingList()
							.getGetSubOfferingInfo().size(); i++) {
						logger.info("First check: " + respns.getGetSubscriberBody().getSupplementaryOfferingList()
								.getGetSubOfferingInfo().get(i).getOfferingId().getOfferingId()
								.equalsIgnoreCase(cClient.getOfferingId()));
						logger.info("Second check: "
								+ respns.getGetSubscriberBody().getSupplementaryOfferingList().getGetSubOfferingInfo()
										.get(i).getStatus().equalsIgnoreCase(Constants.CORE_SERVICE_ACTIVE_STATUS));
						if (respns.getGetSubscriberBody().getSupplementaryOfferingList().getGetSubOfferingInfo().get(i)
								.getOfferingId().getOfferingId().equalsIgnoreCase(cClient.getOfferingId())
								&& respns.getGetSubscriberBody().getSupplementaryOfferingList().getGetSubOfferingInfo()
										.get(i).getStatus().equalsIgnoreCase(Constants.CORE_SERVICE_ACTIVE_STATUS)) {
							mrc = new Mrc();
							QueryOfferingRentCycleResultMsg mrcResponse = getMRCValueForPostPaid(cClient);
							if (mrcResponse != null && mrcResponse.getResultHeader().getResultCode().equals("0")) {
								if (mrcResponse.getQueryOfferingRentCycleResult() != null && mrcResponse
										.getQueryOfferingRentCycleResult().getOfferingRentCycle() != null) {
									List<OfferingRentCycle> cycles = mrcResponse.getQueryOfferingRentCycleResult()
											.getOfferingRentCycle();
									for (Iterator<OfferingRentCycle> iterator = cycles.iterator(); iterator
											.hasNext();) {
										OfferingRentCycle offeringRentCycle = iterator.next();
										if (offeringRentCycle.getOpenDay() != null) {
											mrc.setMrcCurrency(ConfigurationManager
													.getConfigurationFromCache(offeringRentCycle.getCurrencyID() + ""));
											mrc.setMrcInitialDate(
													new SimpleDateFormat(Constants.SQL_DATE_FORMAT_required).format(
															new SimpleDateFormat(Constants.SQL_DATE_FORMAT_TRIMMED)
																	.parse(offeringRentCycle.getOpenDay())));
											mrc.setMrcDate(new SimpleDateFormat(Constants.SQL_DATE_FORMAT_required)
													.format(new SimpleDateFormat(Constants.SQL_DATE_FORMAT_TRIMMED)
															.parse(offeringRentCycle.getEndDay())));
											mrc.setMrcTitleValue(com.evampsaanga.developer.utils.Helper
													.getBakcellMoneyMRC(offeringRentCycle.getRentAmount()));

											if (mrc.getMrcTitleValue().equalsIgnoreCase("FREE")) {
												mrc.setMrcCurrency("");
											}
											mrc.setMrcType(getMRCType(offeringId));
											mrc.setMrcLimit(Constants.MRC_LIMIT);
											Date mrcDate = new SimpleDateFormat(Constants.SQL_DATE_FORMAT_TRIMMED)
													.parse(offeringRentCycle.getEndDay());
											if (mrcDate.after(new Date()))
												mrc.setMrcStatus(Constants.MRC_STATUS_PAID);
											else
												mrc.setMrcStatus(Constants.MRC_STATUS_UNPAID);
										}
									}
								}
							}
						}
					}
				}
				return mrc;
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
			}
		}
		// //If user is prepaid and neither KLASS nor GUNBOUY returning null for
		// MRC
		// return mrc;
		// }
		else if (cClient.getCustomerType().equalsIgnoreCase("postpaid")) {
			try {
				logger.info(cClient.getmsisdn() + "-Postpaid MRC ID FROM CACHE CHECK :"
						+ ConfigurationManager.getConfigurationFromCache("mrc.offering." + cClient.getOfferingId()));
				if (ConfigurationManager.getConfigurationFromCache("mrc.offering." + cClient.getOfferingId()) != null
						&& !ConfigurationManager.getConfigurationFromCache("mrc.offering." + cClient.getOfferingId())
								.equals("")
						&& !ConfigurationManager.getConfigurationFromCache("mrc.offering." + cClient.getOfferingId())
								.isEmpty()) {
					logger.info(
							cClient.getmsisdn() + "-Postpaid MRC Offering ID From Request :" + cClient.getOfferingId());
					cClient.setOfferingId(
							ConfigurationManager.getConfigurationFromCache("mrc.offering." + cClient.getOfferingId()));
					logger.info(
							cClient.getmsisdn() + "-Postpaid MRC OfferingID From Cache :" + cClient.getOfferingId());
				}
				QueryOfferingRentCycleResultMsg mrcResponse = getMRCValueForPostPaid(cClient);
				if (mrcResponse != null && mrcResponse.getResultHeader().getResultCode().equals("0")) {
					if (mrcResponse.getQueryOfferingRentCycleResult() != null
							&& mrcResponse.getQueryOfferingRentCycleResult().getOfferingRentCycle() != null) {
						List<OfferingRentCycle> cycles = mrcResponse.getQueryOfferingRentCycleResult()
								.getOfferingRentCycle();
						for (Iterator<OfferingRentCycle> iterator = cycles.iterator(); iterator.hasNext();) {
							OfferingRentCycle offeringRentCycle = iterator.next();
							mrc.setMrcCurrency(ConfigurationManager
									.getConfigurationFromCache(offeringRentCycle.getCurrencyID() + ""));
							mrc.setMrcInitialDate(new SimpleDateFormat(Constants.SQL_DATE_FORMAT_required)
									.format(new SimpleDateFormat(Constants.SQL_DATE_FORMAT_TRIMMED)
											.parse(offeringRentCycle.getOpenDay())));
							mrc.setMrcDate(new SimpleDateFormat(Constants.SQL_DATE_FORMAT_required)
									.format(new SimpleDateFormat(Constants.SQL_DATE_FORMAT_TRIMMED)
											.parse(offeringRentCycle.getEndDay())));
							mrc.setMrcTitleValue(com.evampsaanga.developer.utils.Helper
									.getBakcellMoneyMRC(offeringRentCycle.getRentAmount()));
							if (mrc.getMrcTitleValue().equalsIgnoreCase("FREE"))
								mrc.setMrcCurrency("");
							mrc.setMrcType(Constants.MRC_TYPE_MONTHLY);
							mrc.setMrcLimit(Constants.MRC_LIMIT);
						}
					}
				}

				return mrc;
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
			}
		} else {
			mrc = new Mrc();
			mrc.setMrcStatus("");
			mrc.setMrcType("");
		}

		try {
			logger.debug("MRC Response: " + new ObjectMapper().writeValueAsString(mrc));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mrc;
	}

	public QueryOfferingRentCycleResultMsg getMRCValueForPostPaid(GetHomePageRequestClient cclient) {
		QueryOfferingRentCycleRequestMsg qORCR = new QueryOfferingRentCycleRequestMsg();
		qORCR.setRequestHeader(BcService.getRequestHeader());
		QueryOfferingRentCycleRequest qORC = new QueryOfferingRentCycleRequest();
		OfferingInst of = new OfferingInst();
		OfferingKey offeringKey = new OfferingKey();
		offeringKey.setOfferingID(new BigInteger(cclient.getOfferingId()));
		of.setOfferingKey(offeringKey);
		OfferingOwner oO = new OfferingOwner();
		SubAccessCode subACode = new SubAccessCode();
		subACode.setPrimaryIdentity(cclient.getmsisdn());
		oO.setSubAccessCode(subACode);
		of.setOfferingOwner(oO);
		qORC.getOfferingInst().add(of);
		qORCR.setQueryOfferingRentCycleRequest(qORC);
		return BcService.getInstance().queryOfferingRentCycle(qORCR);
	}

	public String getMRCType(String key) {
		String value = "";
		try {
			value = ConfigurationManager.getConfigurationFromCache("mrc." + key);
			if (value.equalsIgnoreCase("daily"))
				value = Constants.MRC_TYPE_DAILY;
			else if (value.equalsIgnoreCase("weekly"))
				value = Constants.MRC_TYPE_WEEKLY;
			else
				value = Constants.MRC_TYPE_MONTHLY;
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
			value = Constants.MRC_TYPE_MONTHLY;
		}
		return value;
	}

}
