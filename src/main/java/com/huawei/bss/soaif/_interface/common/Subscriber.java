package com.huawei.bss.soaif._interface.common;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for Subscriber complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="Subscriber">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubId" type="{http://www.huawei.com/bss/soaif/interface/common/}SubId" minOccurs="0"/>
 *         &lt;element name="PartnerId" type="{http://www.huawei.com/bss/soaif/interface/common/}PartnerId" minOccurs="0"/>
 *         &lt;element name="BrandId" type="{http://www.huawei.com/bss/soaif/interface/common/}BrandId" minOccurs="0"/>
 *         &lt;element name="SubType" type="{http://www.huawei.com/bss/soaif/interface/common/}SubType" minOccurs="0"/>
 *         &lt;element name="ServiceNum" type="{http://www.huawei.com/bss/soaif/interface/common/}ServiceNum" minOccurs="0"/>
 *         &lt;element name="ICCID" type="{http://www.huawei.com/bss/soaif/interface/common/}ICCID" minOccurs="0"/>
 *         &lt;element name="IMSI" type="{http://www.huawei.com/bss/soaif/interface/common/}IMSI" minOccurs="0"/>
 *         &lt;element name="IMEI" type="{http://www.huawei.com/bss/soaif/interface/common/}IMEI" minOccurs="0"/>
 *         &lt;element name="Language" type="{http://www.huawei.com/bss/soaif/interface/common/}Language" minOccurs="0"/>
 *         &lt;element name="WrittenLanguage" type="{http://www.huawei.com/bss/soaif/interface/common/}Language" minOccurs="0"/>
 *         &lt;element name="Password" type="{http://www.huawei.com/bss/soaif/interface/common/}Password" minOccurs="0"/>
 *         &lt;element name="NetworkType" type="{http://www.huawei.com/bss/soaif/interface/common/}NetworkType" minOccurs="0"/>
 *         &lt;element name="SubStatus" type="{http://www.huawei.com/bss/soaif/interface/common/}SubStatus" minOccurs="0"/>
 *         &lt;element name="EffectiveDate" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime" minOccurs="0"/>
 *         &lt;element name="ExpireDate" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime" minOccurs="0"/>
 *         &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}SimpleProperty" maxOccurs="100" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Subscriber", propOrder = { "subId", "partnerId", "brandId", "subType", "serviceNum", "iccid", "imsi",
		"imei", "language", "writtenLanguage", "password", "networkType", "subStatus", "effectiveDate", "expireDate",
		"additionalProperty" })
public class Subscriber {
	@XmlElement(name = "SubId")
	protected String subId;
	@XmlElement(name = "PartnerId")
	protected String partnerId;
	@XmlElement(name = "BrandId")
	protected String brandId;
	@XmlElement(name = "SubType")
	protected String subType;
	@XmlElement(name = "ServiceNum")
	protected ServiceNum serviceNum;
	@XmlElement(name = "ICCID")
	protected String iccid;
	@XmlElement(name = "IMSI")
	protected String imsi;
	@XmlElement(name = "IMEI")
	protected String imei;
	@XmlElement(name = "Language")
	protected String language;
	@XmlElement(name = "WrittenLanguage")
	protected String writtenLanguage;
	@XmlElement(name = "Password")
	protected String password;
	@XmlElement(name = "NetworkType")
	protected String networkType;
	@XmlElement(name = "SubStatus")
	protected String subStatus;
	@XmlElement(name = "EffectiveDate")
	protected String effectiveDate;
	@XmlElement(name = "ExpireDate")
	protected String expireDate;
	@XmlElement(name = "AdditionalProperty")
	protected List<SimpleProperty> additionalProperty;

	/**
	 * Gets the value of the subId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSubId() {
		return subId;
	}

	/**
	 * Sets the value of the subId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSubId(String value) {
		this.subId = value;
	}

	/**
	 * Gets the value of the partnerId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPartnerId() {
		return partnerId;
	}

	/**
	 * Sets the value of the partnerId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPartnerId(String value) {
		this.partnerId = value;
	}

	/**
	 * Gets the value of the brandId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBrandId() {
		return brandId;
	}

	/**
	 * Sets the value of the brandId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBrandId(String value) {
		this.brandId = value;
	}

	/**
	 * Gets the value of the subType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSubType() {
		return subType;
	}

	/**
	 * Sets the value of the subType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSubType(String value) {
		this.subType = value;
	}

	/**
	 * Gets the value of the serviceNum property.
	 * 
	 * @return possible object is {@link ServiceNum }
	 * 
	 */
	public ServiceNum getServiceNum() {
		return serviceNum;
	}

	/**
	 * Sets the value of the serviceNum property.
	 * 
	 * @param value
	 *            allowed object is {@link ServiceNum }
	 * 
	 */
	public void setServiceNum(ServiceNum value) {
		this.serviceNum = value;
	}

	/**
	 * Gets the value of the iccid property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getICCID() {
		return iccid;
	}

	/**
	 * Sets the value of the iccid property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setICCID(String value) {
		this.iccid = value;
	}

	/**
	 * Gets the value of the imsi property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIMSI() {
		return imsi;
	}

	/**
	 * Sets the value of the imsi property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIMSI(String value) {
		this.imsi = value;
	}

	/**
	 * Gets the value of the imei property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIMEI() {
		return imei;
	}

	/**
	 * Sets the value of the imei property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIMEI(String value) {
		this.imei = value;
	}

	/**
	 * Gets the value of the language property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * Sets the value of the language property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLanguage(String value) {
		this.language = value;
	}

	/**
	 * Gets the value of the writtenLanguage property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getWrittenLanguage() {
		return writtenLanguage;
	}

	/**
	 * Sets the value of the writtenLanguage property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setWrittenLanguage(String value) {
		this.writtenLanguage = value;
	}

	/**
	 * Gets the value of the password property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the value of the password property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPassword(String value) {
		this.password = value;
	}

	/**
	 * Gets the value of the networkType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getNetworkType() {
		return networkType;
	}

	/**
	 * Sets the value of the networkType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setNetworkType(String value) {
		this.networkType = value;
	}

	/**
	 * Gets the value of the subStatus property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSubStatus() {
		return subStatus;
	}

	/**
	 * Sets the value of the subStatus property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSubStatus(String value) {
		this.subStatus = value;
	}

	/**
	 * Gets the value of the effectiveDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEffectiveDate() {
		return effectiveDate;
	}

	/**
	 * Sets the value of the effectiveDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEffectiveDate(String value) {
		this.effectiveDate = value;
	}

	/**
	 * Gets the value of the expireDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getExpireDate() {
		return expireDate;
	}

	/**
	 * Sets the value of the expireDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setExpireDate(String value) {
		this.expireDate = value;
	}

	/**
	 * Gets the value of the additionalProperty property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the additionalProperty property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getAdditionalProperty().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link SimpleProperty }
	 * 
	 * 
	 */
	public List<SimpleProperty> getAdditionalProperty() {
		if (additionalProperty == null) {
			additionalProperty = new ArrayList<SimpleProperty>();
		}
		return this.additionalProperty;
	}
}
