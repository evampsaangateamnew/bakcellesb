package com.evampsaanga.bakcell.plasticcard;

import com.evampsaanga.bakcell.requestheaders.BaseRequest;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class PlasticCardInitiatePaymentRequest extends BaseRequest {

	int cardType;
	String saved;
	String topupNumber;
	String amount;
	@JsonIgnore
	String firstName;
	@JsonIgnore
	String lastName;

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public int getCardType() {
		return cardType;
	}

	public void setCardType(int cardType) {
		this.cardType = cardType;
	}

	public String getSaved() {
		return saved;
	}

	public void setSaved(String isSaved) {
		this.saved = isSaved;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getTopupNumber() {
		return topupNumber;
	}

	public void setTopupNumber(String topupNumber) {
		this.topupNumber = topupNumber;
	}

	@Override
	public String toString() {
		return "PlasticCardInitiatePaymentRequest [cardType=" + cardType + ", isSaved=" + saved + ", topupNumber="
				+ topupNumber + ", firstName=" + firstName + ", lastName=" + lastName + ", toString()="
				+ super.toString() + "]";
	}

}
