package com.evampsaanga.ulduzumupdate.getmerchantdetails;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Branches {
	private String name;
	private String coord_lat;
	private String coord_lng;
	private String address;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the coord_lat
	 */
	public String getCoord_lat() {
		return coord_lat;
	}

	/**
	 * @param coord_lat
	 *            the coord_lat to set
	 */
	public void setCoord_lat(String coord_lat) {
		this.coord_lat = coord_lat;
	}

	/**
	 * @return the coord_lng
	 */
	public String getCoord_lng() {
		return coord_lng;
	}

	/**
	 * @param coord_lng
	 *            the coord_lng to set
	 */
	public void setCoord_lng(String coord_lng) {
		this.coord_lng = coord_lng;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address
	 *            the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Branches [name=" + name + ", coord_lat=" + coord_lat + ", coord_lng=" + coord_lng + ", address="
				+ address + "]";
	}

}
