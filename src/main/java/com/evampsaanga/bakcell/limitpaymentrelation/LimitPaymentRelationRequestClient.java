package com.evampsaanga.bakcell.limitpaymentrelation;

import com.evampsaanga.bakcell.requestheaders.BaseRequest;

public class LimitPaymentRelationRequestClient extends BaseRequest {
	private String offeringId="";
	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	private String groupType="";
	private String companyStandardValue="";
	private String msisdnuser="";
	
   public String getMsisdnuser() {
		return msisdnuser;
	}

	public void setMsisdnuser(String msisdnuser) {
		this.msisdnuser = msisdnuser;
	}

public String getCompanyStandardValue() {
		return companyStandardValue;
	}

	public void setCompanyStandardValue(String companyStandardValue) {
		this.companyStandardValue = companyStandardValue;
	}

	public String getGroupType() {
		return groupType;
	}

	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}



}
