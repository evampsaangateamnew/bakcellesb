package com.huawei.crm.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for ContactReasonCodeList complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="ContactReasonCodeList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ReasonCode" type="{http://crm.huawei.com/basetype/}ContactReasonCode"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContactReasonCodeList", propOrder = { "reasonCode" })
public class ContactReasonCodeList {
	@XmlElement(name = "ReasonCode", required = true)
	protected String reasonCode;

	/**
	 * Gets the value of the reasonCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getReasonCode() {
		return reasonCode;
	}

	/**
	 * Sets the value of the reasonCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setReasonCode(String value) {
		this.reasonCode = value;
	}
}
