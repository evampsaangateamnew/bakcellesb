package com.huawei.cbs.ar.wsservice.arcommon;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the com.huawei.cbs.ar.wsservice.arcommon
 * package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {
	/**
	 * Create a new ObjectFactory that can be used to create new instances of
	 * schema derived classes for package: com.huawei.cbs.ar.wsservice.arcommon
	 * 
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link AcctBalance }
	 * 
	 */
	public AcctBalance createAcctBalance() {
		return new AcctBalance();
	}

	/**
	 * Create an instance of {@link AcctAccessCode }
	 * 
	 */
	public AcctAccessCode createAcctAccessCode() {
		return new AcctAccessCode();
	}

	/**
	 * Create an instance of {@link FreeUnitChgInfo }
	 * 
	 */
	public FreeUnitChgInfo createFreeUnitChgInfo() {
		return new FreeUnitChgInfo();
	}

	/**
	 * Create an instance of {@link PaymentDetailValue }
	 * 
	 */
	public PaymentDetailValue createPaymentDetailValue() {
		return new PaymentDetailValue();
	}

	/**
	 * Create an instance of {@link Tax }
	 * 
	 */
	public Tax createTax() {
		return new Tax();
	}

	/**
	 * Create an instance of {@link OfferingInfo }
	 * 
	 */
	public OfferingInfo createOfferingInfo() {
		return new OfferingInfo();
	}

	/**
	 * Create an instance of {@link BalanceChgInfo }
	 * 
	 */
	public BalanceChgInfo createBalanceChgInfo() {
		return new BalanceChgInfo();
	}

	/**
	 * Create an instance of {@link FeeDetailValue }
	 * 
	 */
	public FeeDetailValue createFeeDetailValue() {
		return new FeeDetailValue();
	}

	/**
	 * Create an instance of {@link BalanceAdjustment }
	 * 
	 */
	public BalanceAdjustment createBalanceAdjustment() {
		return new BalanceAdjustment();
	}

	/**
	 * Create an instance of {@link CustAccessCode }
	 * 
	 */
	public CustAccessCode createCustAccessCode() {
		return new CustAccessCode();
	}

	/**
	 * Create an instance of {@link SubAccessCode }
	 * 
	 */
	public SubAccessCode createSubAccessCode() {
		return new SubAccessCode();
	}

	/**
	 * Create an instance of {@link SimpleProperty }
	 * 
	 */
	public SimpleProperty createSimpleProperty() {
		return new SimpleProperty();
	}

	/**
	 * Create an instance of {@link SubGroupAccessCode }
	 * 
	 */
	public SubGroupAccessCode createSubGroupAccessCode() {
		return new SubGroupAccessCode();
	}

	/**
	 * Create an instance of {@link BankInfo }
	 * 
	 */
	public BankInfo createBankInfo() {
		return new BankInfo();
	}

	/**
	 * Create an instance of {@link LoanChgInfo }
	 * 
	 */
	public LoanChgInfo createLoanChgInfo() {
		return new LoanChgInfo();
	}

	/**
	 * Create an instance of {@link AcctBalance.BalanceDetail }
	 * 
	 */
	public AcctBalance.BalanceDetail createAcctBalanceBalanceDetail() {
		return new AcctBalance.BalanceDetail();
	}
}
