package com.nsn.devicemanagement.wsdl.common._2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for ProvisioningAction complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="ProvisioningAction">
 *   &lt;complexContent>
 *     &lt;extension base="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}Action">
 *       &lt;sequence>
 *         &lt;element name="serviceSettings" type="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}setting" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="apnList" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="confirmation" type="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}message" minOccurs="0"/>
 *         &lt;element name="displayableGroupName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="invitation" type="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}invitationMessage" minOccurs="0"/>
 *         &lt;element name="notification" type="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}message" minOccurs="0"/>
 *         &lt;element name="preferDMProvisioningMethod" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProvisioningAction", propOrder = { "serviceSettings", "apnList", "confirmation",
		"displayableGroupName", "invitation", "notification", "preferDMProvisioningMethod" })
@XmlSeeAlso({ APNProvisioningAction.class, AccessPointNamesAction.class, SettingProvisioning.class })
public abstract class ProvisioningAction extends Action {
	protected List<Setting> serviceSettings;
	protected List<String> apnList;
	protected Message confirmation;
	protected String displayableGroupName;
	protected InvitationMessage invitation;
	protected Message notification;
	protected boolean preferDMProvisioningMethod;

	/**
	 * Gets the value of the serviceSettings property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the serviceSettings property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getServiceSettings().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list {@link Setting }
	 * 
	 * 
	 */
	public List<Setting> getServiceSettings() {
		if (serviceSettings == null) {
			serviceSettings = new ArrayList<Setting>();
		}
		return this.serviceSettings;
	}

	/**
	 * Gets the value of the apnList property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the apnList property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getApnList().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list {@link String }
	 * 
	 * 
	 */
	public List<String> getApnList() {
		if (apnList == null) {
			apnList = new ArrayList<String>();
		}
		return this.apnList;
	}

	/**
	 * Gets the value of the confirmation property.
	 * 
	 * @return possible object is {@link Message }
	 * 
	 */
	public Message getConfirmation() {
		return confirmation;
	}

	/**
	 * Sets the value of the confirmation property.
	 * 
	 * @param value
	 *            allowed object is {@link Message }
	 * 
	 */
	public void setConfirmation(Message value) {
		this.confirmation = value;
	}

	/**
	 * Gets the value of the displayableGroupName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDisplayableGroupName() {
		return displayableGroupName;
	}

	/**
	 * Sets the value of the displayableGroupName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDisplayableGroupName(String value) {
		this.displayableGroupName = value;
	}

	/**
	 * Gets the value of the invitation property.
	 * 
	 * @return possible object is {@link InvitationMessage }
	 * 
	 */
	public InvitationMessage getInvitation() {
		return invitation;
	}

	/**
	 * Sets the value of the invitation property.
	 * 
	 * @param value
	 *            allowed object is {@link InvitationMessage }
	 * 
	 */
	public void setInvitation(InvitationMessage value) {
		this.invitation = value;
	}

	/**
	 * Gets the value of the notification property.
	 * 
	 * @return possible object is {@link Message }
	 * 
	 */
	public Message getNotification() {
		return notification;
	}

	/**
	 * Sets the value of the notification property.
	 * 
	 * @param value
	 *            allowed object is {@link Message }
	 * 
	 */
	public void setNotification(Message value) {
		this.notification = value;
	}

	/**
	 * Gets the value of the preferDMProvisioningMethod property.
	 * 
	 */
	public boolean isPreferDMProvisioningMethod() {
		return preferDMProvisioningMethod;
	}

	/**
	 * Sets the value of the preferDMProvisioningMethod property.
	 * 
	 */
	public void setPreferDMProvisioningMethod(boolean value) {
		this.preferDMProvisioningMethod = value;
	}
}
