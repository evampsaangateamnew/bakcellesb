package com.evampsaanga.bakcell.plasticcard.posting.queryrequest.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "envelope")
@XmlAccessorType(XmlAccessType.FIELD)
public class QueryStatusResponseEnvelope {

	public int reqtype;
	public QueryStatusResponseBody body;

	public int getReqtype() {
		return reqtype;
	}

	public void setReqtype(int reqtype) {
		this.reqtype = reqtype;
	}

	public QueryStatusResponseBody getBody() {
		return body;
	}

	public void setBody(QueryStatusResponseBody body) {
		this.body = body;
	}

	@Override
	public String toString() {
		return "QueryStatusResponseEnvelope [reqtype=" + reqtype + ", body=" + body + "]";
	}
}
