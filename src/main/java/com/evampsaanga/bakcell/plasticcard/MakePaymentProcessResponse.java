package com.evampsaanga.bakcell.plasticcard;

import com.evampsaanga.bakcell.responseheaders.BaseResponse;

public class MakePaymentProcessResponse extends BaseResponse{

	@Override
	public String toString() {
		return "MakePaymentProcessResponse [toString()=" + super.toString() + "]";
	}

}
