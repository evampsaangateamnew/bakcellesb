package com.huawei.bme.cbsinterface.arservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.cbs.ar.wsservice.arcommon.BalanceChgInfo;
import com.huawei.cbs.ar.wsservice.arcommon.BankInfo;
import com.huawei.cbs.ar.wsservice.arcommon.SimpleProperty;

/**
 * <p>
 * Java class for QueryRechargeLogResult complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="QueryRechargeLogResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RechargeInfo" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="TradeTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="SubKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="PrimaryIdentity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="TransID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="ExtTransID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="RechargeAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="OriAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                   &lt;element name="OriCurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                   &lt;element name="CurrencyRate" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
 *                   &lt;element name="RechargeTax" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                   &lt;element name="RechargePenalty" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                   &lt;element name="RechargeType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ExtRechargeType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CardInfo" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="CardPinNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="CardSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="BankInfo" type="{http://cbs.huawei.com/ar/wsservice/arcommon}BankInfo" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;element name="RechargeChannelID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="RechargeReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="OperID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                   &lt;element name="DeptID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                   &lt;element name="ResultCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ReversalFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ReversalReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ReversalOpID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                   &lt;element name="ReversalDeptID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                   &lt;element name="ReversalTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="LifeCycleChgInfo" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="OldLifeCycleStatus" maxOccurs="unbounded">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="StatusName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="StatusExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="StatusIndex" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="NewLifeCycleStatus" maxOccurs="unbounded">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="StatusName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="StatusExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="StatusIndex" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="ChgValidity" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="BalanceChgInfo" type="{http://cbs.huawei.com/ar/wsservice/arcommon}BalanceChgInfo" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;element name="RechargeBonus" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="FreeUnitItemList" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="FreeUnitID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="FreeUnitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="FreeUnitTypeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="MeasureUnit" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="MeasureUnitName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="BonusAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                                       &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="BalanceList" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="BalanceType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="BalanceID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="BalanceTypeName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="BonusAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                                       &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                                       &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="AdditionalProperty" type="{http://cbs.huawei.com/ar/wsservice/arcommon}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="TotalRowNum" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="BeginRowNum" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="FetchRowNum" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryRechargeLogResult", propOrder = { "rechargeInfo", "totalRowNum", "beginRowNum", "fetchRowNum" })
public class QueryRechargeLogResult {
	@XmlElement(name = "RechargeInfo")
	protected List<QueryRechargeLogResult.RechargeInfo> rechargeInfo;
	@XmlElement(name = "TotalRowNum")
	protected long totalRowNum;
	@XmlElement(name = "BeginRowNum")
	protected long beginRowNum;
	@XmlElement(name = "FetchRowNum")
	protected long fetchRowNum;

	/**
	 * Gets the value of the rechargeInfo property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the rechargeInfo property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getRechargeInfo().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link QueryRechargeLogResult.RechargeInfo }
	 * 
	 * 
	 */
	public List<QueryRechargeLogResult.RechargeInfo> getRechargeInfo() {
		if (rechargeInfo == null) {
			rechargeInfo = new ArrayList<QueryRechargeLogResult.RechargeInfo>();
		}
		return this.rechargeInfo;
	}

	/**
	 * Gets the value of the totalRowNum property.
	 * 
	 */
	public long getTotalRowNum() {
		return totalRowNum;
	}

	/**
	 * Sets the value of the totalRowNum property.
	 * 
	 */
	public void setTotalRowNum(long value) {
		this.totalRowNum = value;
	}

	/**
	 * Gets the value of the beginRowNum property.
	 * 
	 */
	public long getBeginRowNum() {
		return beginRowNum;
	}

	/**
	 * Sets the value of the beginRowNum property.
	 * 
	 */
	public void setBeginRowNum(long value) {
		this.beginRowNum = value;
	}

	/**
	 * Gets the value of the fetchRowNum property.
	 * 
	 */
	public long getFetchRowNum() {
		return fetchRowNum;
	}

	/**
	 * Sets the value of the fetchRowNum property.
	 * 
	 */
	public void setFetchRowNum(long value) {
		this.fetchRowNum = value;
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="TradeTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *         &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *         &lt;element name="SubKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="PrimaryIdentity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="TransID" type="{http://www.w3.org/2001/XMLSchema}long"/>
	 *         &lt;element name="ExtTransID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="RechargeAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
	 *         &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
	 *         &lt;element name="OriAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
	 *         &lt;element name="OriCurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
	 *         &lt;element name="CurrencyRate" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
	 *         &lt;element name="RechargeTax" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
	 *         &lt;element name="RechargePenalty" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
	 *         &lt;element name="RechargeType" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *         &lt;element name="ExtRechargeType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="CardInfo" minOccurs="0">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="CardPinNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *                   &lt;element name="CardSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="BankInfo" type="{http://cbs.huawei.com/ar/wsservice/arcommon}BankInfo" maxOccurs="unbounded" minOccurs="0"/>
	 *         &lt;element name="RechargeChannelID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="RechargeReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="OperID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
	 *         &lt;element name="DeptID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
	 *         &lt;element name="ResultCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *         &lt;element name="ReversalFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *         &lt;element name="ReversalReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="ReversalOpID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
	 *         &lt;element name="ReversalDeptID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
	 *         &lt;element name="ReversalTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="LifeCycleChgInfo" minOccurs="0">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="OldLifeCycleStatus" maxOccurs="unbounded">
	 *                     &lt;complexType>
	 *                       &lt;complexContent>
	 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                           &lt;sequence>
	 *                             &lt;element name="StatusName" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                             &lt;element name="StatusExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                             &lt;element name="StatusIndex" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                           &lt;/sequence>
	 *                         &lt;/restriction>
	 *                       &lt;/complexContent>
	 *                     &lt;/complexType>
	 *                   &lt;/element>
	 *                   &lt;element name="NewLifeCycleStatus" maxOccurs="unbounded">
	 *                     &lt;complexType>
	 *                       &lt;complexContent>
	 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                           &lt;sequence>
	 *                             &lt;element name="StatusName" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                             &lt;element name="StatusExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                             &lt;element name="StatusIndex" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                           &lt;/sequence>
	 *                         &lt;/restriction>
	 *                       &lt;/complexContent>
	 *                     &lt;/complexType>
	 *                   &lt;/element>
	 *                   &lt;element name="ChgValidity" type="{http://www.w3.org/2001/XMLSchema}integer"/>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="BalanceChgInfo" type="{http://cbs.huawei.com/ar/wsservice/arcommon}BalanceChgInfo" maxOccurs="unbounded" minOccurs="0"/>
	 *         &lt;element name="RechargeBonus" minOccurs="0">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="FreeUnitItemList" maxOccurs="unbounded" minOccurs="0">
	 *                     &lt;complexType>
	 *                       &lt;complexContent>
	 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                           &lt;sequence>
	 *                             &lt;element name="FreeUnitID" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                             &lt;element name="FreeUnitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                             &lt;element name="FreeUnitTypeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *                             &lt;element name="MeasureUnit" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                             &lt;element name="MeasureUnitName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *                             &lt;element name="BonusAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
	 *                             &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                             &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                           &lt;/sequence>
	 *                         &lt;/restriction>
	 *                       &lt;/complexContent>
	 *                     &lt;/complexType>
	 *                   &lt;/element>
	 *                   &lt;element name="BalanceList" maxOccurs="unbounded" minOccurs="0">
	 *                     &lt;complexType>
	 *                       &lt;complexContent>
	 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                           &lt;sequence>
	 *                             &lt;element name="BalanceType" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                             &lt;element name="BalanceID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *                             &lt;element name="BalanceTypeName" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                             &lt;element name="BonusAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
	 *                             &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
	 *                             &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                             &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                           &lt;/sequence>
	 *                         &lt;/restriction>
	 *                       &lt;/complexContent>
	 *                     &lt;/complexType>
	 *                   &lt;/element>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="AdditionalProperty" type="{http://cbs.huawei.com/ar/wsservice/arcommon}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "tradeTime", "acctKey", "subKey", "primaryIdentity", "transID", "extTransID",
			"rechargeAmount", "currencyID", "oriAmount", "oriCurrencyID", "currencyRate", "rechargeTax",
			"rechargePenalty", "rechargeType", "extRechargeType", "cardInfo", "bankInfo", "rechargeChannelID",
			"rechargeReason", "operID", "deptID", "resultCode", "reversalFlag", "reversalReason", "reversalOpID",
			"reversalDeptID", "reversalTime", "lifeCycleChgInfo", "balanceChgInfo", "rechargeBonus",
			"additionalProperty" })
	public static class RechargeInfo {
		@XmlElement(name = "TradeTime", required = true)
		protected String tradeTime;
		@XmlElement(name = "AcctKey", required = true)
		protected String acctKey;
		@XmlElement(name = "SubKey")
		protected String subKey;
		@XmlElement(name = "PrimaryIdentity")
		protected String primaryIdentity;
		@XmlElement(name = "TransID")
		protected long transID;
		@XmlElement(name = "ExtTransID")
		protected String extTransID;
		@XmlElement(name = "RechargeAmount")
		protected long rechargeAmount;
		@XmlElement(name = "CurrencyID", required = true)
		protected BigInteger currencyID;
		@XmlElement(name = "OriAmount")
		protected Long oriAmount;
		@XmlElement(name = "OriCurrencyID")
		protected BigInteger oriCurrencyID;
		@XmlElement(name = "CurrencyRate")
		protected Float currencyRate;
		@XmlElement(name = "RechargeTax")
		protected Long rechargeTax;
		@XmlElement(name = "RechargePenalty")
		protected Long rechargePenalty;
		@XmlElement(name = "RechargeType", required = true)
		protected String rechargeType;
		@XmlElement(name = "ExtRechargeType")
		protected String extRechargeType;
		@XmlElement(name = "CardInfo")
		protected QueryRechargeLogResult.RechargeInfo.CardInfo cardInfo;
		@XmlElement(name = "BankInfo")
		protected List<BankInfo> bankInfo;
		@XmlElement(name = "RechargeChannelID")
		protected String rechargeChannelID;
		@XmlElement(name = "RechargeReason")
		protected String rechargeReason;
		@XmlElement(name = "OperID")
		protected Long operID;
		@XmlElement(name = "DeptID")
		protected Long deptID;
		@XmlElement(name = "ResultCode", required = true)
		protected String resultCode;
		@XmlElement(name = "ReversalFlag", required = true)
		protected String reversalFlag;
		@XmlElement(name = "ReversalReason")
		protected String reversalReason;
		@XmlElement(name = "ReversalOpID")
		protected Long reversalOpID;
		@XmlElement(name = "ReversalDeptID")
		protected Long reversalDeptID;
		@XmlElement(name = "ReversalTime")
		protected String reversalTime;
		@XmlElement(name = "LifeCycleChgInfo")
		protected QueryRechargeLogResult.RechargeInfo.LifeCycleChgInfo lifeCycleChgInfo;
		@XmlElement(name = "BalanceChgInfo")
		protected List<BalanceChgInfo> balanceChgInfo;
		@XmlElement(name = "RechargeBonus")
		protected QueryRechargeLogResult.RechargeInfo.RechargeBonus rechargeBonus;
		@XmlElement(name = "AdditionalProperty")
		protected List<SimpleProperty> additionalProperty;

		/**
		 * Gets the value of the tradeTime property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getTradeTime() {
			return tradeTime;
		}

		/**
		 * Sets the value of the tradeTime property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setTradeTime(String value) {
			this.tradeTime = value;
		}

		/**
		 * Gets the value of the acctKey property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getAcctKey() {
			return acctKey;
		}

		/**
		 * Sets the value of the acctKey property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setAcctKey(String value) {
			this.acctKey = value;
		}

		/**
		 * Gets the value of the subKey property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getSubKey() {
			return subKey;
		}

		/**
		 * Sets the value of the subKey property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setSubKey(String value) {
			this.subKey = value;
		}

		/**
		 * Gets the value of the primaryIdentity property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getPrimaryIdentity() {
			return primaryIdentity;
		}

		/**
		 * Sets the value of the primaryIdentity property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setPrimaryIdentity(String value) {
			this.primaryIdentity = value;
		}

		/**
		 * Gets the value of the transID property.
		 * 
		 */
		public long getTransID() {
			return transID;
		}

		/**
		 * Sets the value of the transID property.
		 * 
		 */
		public void setTransID(long value) {
			this.transID = value;
		}

		/**
		 * Gets the value of the extTransID property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getExtTransID() {
			return extTransID;
		}

		/**
		 * Sets the value of the extTransID property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setExtTransID(String value) {
			this.extTransID = value;
		}

		/**
		 * Gets the value of the rechargeAmount property.
		 * 
		 */
		public long getRechargeAmount() {
			return rechargeAmount;
		}

		/**
		 * Sets the value of the rechargeAmount property.
		 * 
		 */
		public void setRechargeAmount(long value) {
			this.rechargeAmount = value;
		}

		/**
		 * Gets the value of the currencyID property.
		 * 
		 * @return possible object is {@link BigInteger }
		 * 
		 */
		public BigInteger getCurrencyID() {
			return currencyID;
		}

		/**
		 * Sets the value of the currencyID property.
		 * 
		 * @param value
		 *            allowed object is {@link BigInteger }
		 * 
		 */
		public void setCurrencyID(BigInteger value) {
			this.currencyID = value;
		}

		/**
		 * Gets the value of the oriAmount property.
		 * 
		 * @return possible object is {@link Long }
		 * 
		 */
		public Long getOriAmount() {
			return oriAmount;
		}

		/**
		 * Sets the value of the oriAmount property.
		 * 
		 * @param value
		 *            allowed object is {@link Long }
		 * 
		 */
		public void setOriAmount(Long value) {
			this.oriAmount = value;
		}

		/**
		 * Gets the value of the oriCurrencyID property.
		 * 
		 * @return possible object is {@link BigInteger }
		 * 
		 */
		public BigInteger getOriCurrencyID() {
			return oriCurrencyID;
		}

		/**
		 * Sets the value of the oriCurrencyID property.
		 * 
		 * @param value
		 *            allowed object is {@link BigInteger }
		 * 
		 */
		public void setOriCurrencyID(BigInteger value) {
			this.oriCurrencyID = value;
		}

		/**
		 * Gets the value of the currencyRate property.
		 * 
		 * @return possible object is {@link Float }
		 * 
		 */
		public Float getCurrencyRate() {
			return currencyRate;
		}

		/**
		 * Sets the value of the currencyRate property.
		 * 
		 * @param value
		 *            allowed object is {@link Float }
		 * 
		 */
		public void setCurrencyRate(Float value) {
			this.currencyRate = value;
		}

		/**
		 * Gets the value of the rechargeTax property.
		 * 
		 * @return possible object is {@link Long }
		 * 
		 */
		public Long getRechargeTax() {
			return rechargeTax;
		}

		/**
		 * Sets the value of the rechargeTax property.
		 * 
		 * @param value
		 *            allowed object is {@link Long }
		 * 
		 */
		public void setRechargeTax(Long value) {
			this.rechargeTax = value;
		}

		/**
		 * Gets the value of the rechargePenalty property.
		 * 
		 * @return possible object is {@link Long }
		 * 
		 */
		public Long getRechargePenalty() {
			return rechargePenalty;
		}

		/**
		 * Sets the value of the rechargePenalty property.
		 * 
		 * @param value
		 *            allowed object is {@link Long }
		 * 
		 */
		public void setRechargePenalty(Long value) {
			this.rechargePenalty = value;
		}

		/**
		 * Gets the value of the rechargeType property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getRechargeType() {
			return rechargeType;
		}

		/**
		 * Sets the value of the rechargeType property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setRechargeType(String value) {
			this.rechargeType = value;
		}

		/**
		 * Gets the value of the extRechargeType property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getExtRechargeType() {
			return extRechargeType;
		}

		/**
		 * Sets the value of the extRechargeType property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setExtRechargeType(String value) {
			this.extRechargeType = value;
		}

		/**
		 * Gets the value of the cardInfo property.
		 * 
		 * @return possible object is
		 *         {@link QueryRechargeLogResult.RechargeInfo.CardInfo }
		 * 
		 */
		public QueryRechargeLogResult.RechargeInfo.CardInfo getCardInfo() {
			return cardInfo;
		}

		/**
		 * Sets the value of the cardInfo property.
		 * 
		 * @param value
		 *            allowed object is
		 *            {@link QueryRechargeLogResult.RechargeInfo.CardInfo }
		 * 
		 */
		public void setCardInfo(QueryRechargeLogResult.RechargeInfo.CardInfo value) {
			this.cardInfo = value;
		}

		/**
		 * Gets the value of the bankInfo property.
		 * 
		 * <p>
		 * This accessor method returns a reference to the live list, not a
		 * snapshot. Therefore any modification you make to the returned list
		 * will be present inside the JAXB object. This is why there is not a
		 * <CODE>set</CODE> method for the bankInfo property.
		 * 
		 * <p>
		 * For example, to add a new item, do as follows:
		 * 
		 * <pre>
		 * getBankInfo().add(newItem);
		 * </pre>
		 * 
		 * 
		 * <p>
		 * Objects of the following type(s) are allowed in the list
		 * {@link BankInfo }
		 * 
		 * 
		 */
		public List<BankInfo> getBankInfo() {
			if (bankInfo == null) {
				bankInfo = new ArrayList<BankInfo>();
			}
			return this.bankInfo;
		}

		/**
		 * Gets the value of the rechargeChannelID property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getRechargeChannelID() {
			return rechargeChannelID;
		}

		/**
		 * Sets the value of the rechargeChannelID property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setRechargeChannelID(String value) {
			this.rechargeChannelID = value;
		}

		/**
		 * Gets the value of the rechargeReason property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getRechargeReason() {
			return rechargeReason;
		}

		/**
		 * Sets the value of the rechargeReason property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setRechargeReason(String value) {
			this.rechargeReason = value;
		}

		/**
		 * Gets the value of the operID property.
		 * 
		 * @return possible object is {@link Long }
		 * 
		 */
		public Long getOperID() {
			return operID;
		}

		/**
		 * Sets the value of the operID property.
		 * 
		 * @param value
		 *            allowed object is {@link Long }
		 * 
		 */
		public void setOperID(Long value) {
			this.operID = value;
		}

		/**
		 * Gets the value of the deptID property.
		 * 
		 * @return possible object is {@link Long }
		 * 
		 */
		public Long getDeptID() {
			return deptID;
		}

		/**
		 * Sets the value of the deptID property.
		 * 
		 * @param value
		 *            allowed object is {@link Long }
		 * 
		 */
		public void setDeptID(Long value) {
			this.deptID = value;
		}

		/**
		 * Gets the value of the resultCode property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getResultCode() {
			return resultCode;
		}

		/**
		 * Sets the value of the resultCode property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setResultCode(String value) {
			this.resultCode = value;
		}

		/**
		 * Gets the value of the reversalFlag property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getReversalFlag() {
			return reversalFlag;
		}

		/**
		 * Sets the value of the reversalFlag property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setReversalFlag(String value) {
			this.reversalFlag = value;
		}

		/**
		 * Gets the value of the reversalReason property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getReversalReason() {
			return reversalReason;
		}

		/**
		 * Sets the value of the reversalReason property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setReversalReason(String value) {
			this.reversalReason = value;
		}

		/**
		 * Gets the value of the reversalOpID property.
		 * 
		 * @return possible object is {@link Long }
		 * 
		 */
		public Long getReversalOpID() {
			return reversalOpID;
		}

		/**
		 * Sets the value of the reversalOpID property.
		 * 
		 * @param value
		 *            allowed object is {@link Long }
		 * 
		 */
		public void setReversalOpID(Long value) {
			this.reversalOpID = value;
		}

		/**
		 * Gets the value of the reversalDeptID property.
		 * 
		 * @return possible object is {@link Long }
		 * 
		 */
		public Long getReversalDeptID() {
			return reversalDeptID;
		}

		/**
		 * Sets the value of the reversalDeptID property.
		 * 
		 * @param value
		 *            allowed object is {@link Long }
		 * 
		 */
		public void setReversalDeptID(Long value) {
			this.reversalDeptID = value;
		}

		/**
		 * Gets the value of the reversalTime property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getReversalTime() {
			return reversalTime;
		}

		/**
		 * Sets the value of the reversalTime property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setReversalTime(String value) {
			this.reversalTime = value;
		}

		/**
		 * Gets the value of the lifeCycleChgInfo property.
		 * 
		 * @return possible object is
		 *         {@link QueryRechargeLogResult.RechargeInfo.LifeCycleChgInfo }
		 * 
		 */
		public QueryRechargeLogResult.RechargeInfo.LifeCycleChgInfo getLifeCycleChgInfo() {
			return lifeCycleChgInfo;
		}

		/**
		 * Sets the value of the lifeCycleChgInfo property.
		 * 
		 * @param value
		 *            allowed object is
		 *            {@link QueryRechargeLogResult.RechargeInfo.LifeCycleChgInfo }
		 * 
		 */
		public void setLifeCycleChgInfo(QueryRechargeLogResult.RechargeInfo.LifeCycleChgInfo value) {
			this.lifeCycleChgInfo = value;
		}

		/**
		 * Gets the value of the balanceChgInfo property.
		 * 
		 * <p>
		 * This accessor method returns a reference to the live list, not a
		 * snapshot. Therefore any modification you make to the returned list
		 * will be present inside the JAXB object. This is why there is not a
		 * <CODE>set</CODE> method for the balanceChgInfo property.
		 * 
		 * <p>
		 * For example, to add a new item, do as follows:
		 * 
		 * <pre>
		 * getBalanceChgInfo().add(newItem);
		 * </pre>
		 * 
		 * 
		 * <p>
		 * Objects of the following type(s) are allowed in the list
		 * {@link BalanceChgInfo }
		 * 
		 * 
		 */
		public List<BalanceChgInfo> getBalanceChgInfo() {
			if (balanceChgInfo == null) {
				balanceChgInfo = new ArrayList<BalanceChgInfo>();
			}
			return this.balanceChgInfo;
		}

		/**
		 * Gets the value of the rechargeBonus property.
		 * 
		 * @return possible object is
		 *         {@link QueryRechargeLogResult.RechargeInfo.RechargeBonus }
		 * 
		 */
		public QueryRechargeLogResult.RechargeInfo.RechargeBonus getRechargeBonus() {
			return rechargeBonus;
		}

		/**
		 * Sets the value of the rechargeBonus property.
		 * 
		 * @param value
		 *            allowed object is
		 *            {@link QueryRechargeLogResult.RechargeInfo.RechargeBonus }
		 * 
		 */
		public void setRechargeBonus(QueryRechargeLogResult.RechargeInfo.RechargeBonus value) {
			this.rechargeBonus = value;
		}

		/**
		 * Gets the value of the additionalProperty property.
		 * 
		 * <p>
		 * This accessor method returns a reference to the live list, not a
		 * snapshot. Therefore any modification you make to the returned list
		 * will be present inside the JAXB object. This is why there is not a
		 * <CODE>set</CODE> method for the additionalProperty property.
		 * 
		 * <p>
		 * For example, to add a new item, do as follows:
		 * 
		 * <pre>
		 * getAdditionalProperty().add(newItem);
		 * </pre>
		 * 
		 * 
		 * <p>
		 * Objects of the following type(s) are allowed in the list
		 * {@link SimpleProperty }
		 * 
		 * 
		 */
		public List<SimpleProperty> getAdditionalProperty() {
			if (additionalProperty == null) {
				additionalProperty = new ArrayList<SimpleProperty>();
			}
			return this.additionalProperty;
		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * 
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="CardPinNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
		 *         &lt;element name="CardSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "cardPinNumber", "cardSequence" })
		public static class CardInfo {
			@XmlElement(name = "CardPinNumber")
			protected String cardPinNumber;
			@XmlElement(name = "CardSequence")
			protected String cardSequence;

			/**
			 * Gets the value of the cardPinNumber property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getCardPinNumber() {
				return cardPinNumber;
			}

			/**
			 * Sets the value of the cardPinNumber property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setCardPinNumber(String value) {
				this.cardPinNumber = value;
			}

			/**
			 * Gets the value of the cardSequence property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getCardSequence() {
				return cardSequence;
			}

			/**
			 * Sets the value of the cardSequence property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setCardSequence(String value) {
				this.cardSequence = value;
			}
		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * 
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="OldLifeCycleStatus" maxOccurs="unbounded">
		 *           &lt;complexType>
		 *             &lt;complexContent>
		 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                 &lt;sequence>
		 *                   &lt;element name="StatusName" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *                   &lt;element name="StatusExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *                   &lt;element name="StatusIndex" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *                 &lt;/sequence>
		 *               &lt;/restriction>
		 *             &lt;/complexContent>
		 *           &lt;/complexType>
		 *         &lt;/element>
		 *         &lt;element name="NewLifeCycleStatus" maxOccurs="unbounded">
		 *           &lt;complexType>
		 *             &lt;complexContent>
		 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                 &lt;sequence>
		 *                   &lt;element name="StatusName" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *                   &lt;element name="StatusExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *                   &lt;element name="StatusIndex" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *                 &lt;/sequence>
		 *               &lt;/restriction>
		 *             &lt;/complexContent>
		 *           &lt;/complexType>
		 *         &lt;/element>
		 *         &lt;element name="ChgValidity" type="{http://www.w3.org/2001/XMLSchema}integer"/>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "oldLifeCycleStatus", "newLifeCycleStatus", "chgValidity" })
		public static class LifeCycleChgInfo {
			@XmlElement(name = "OldLifeCycleStatus", required = true)
			protected List<QueryRechargeLogResult.RechargeInfo.LifeCycleChgInfo.OldLifeCycleStatus> oldLifeCycleStatus;
			@XmlElement(name = "NewLifeCycleStatus", required = true)
			protected List<QueryRechargeLogResult.RechargeInfo.LifeCycleChgInfo.NewLifeCycleStatus> newLifeCycleStatus;
			@XmlElement(name = "ChgValidity", required = true)
			protected BigInteger chgValidity;

			/**
			 * Gets the value of the oldLifeCycleStatus property.
			 * 
			 * <p>
			 * This accessor method returns a reference to the live list, not a
			 * snapshot. Therefore any modification you make to the returned
			 * list will be present inside the JAXB object. This is why there is
			 * not a <CODE>set</CODE> method for the oldLifeCycleStatus
			 * property.
			 * 
			 * <p>
			 * For example, to add a new item, do as follows:
			 * 
			 * <pre>
			 * getOldLifeCycleStatus().add(newItem);
			 * </pre>
			 * 
			 * 
			 * <p>
			 * Objects of the following type(s) are allowed in the list
			 * {@link QueryRechargeLogResult.RechargeInfo.LifeCycleChgInfo.OldLifeCycleStatus }
			 * 
			 * 
			 */
			public List<QueryRechargeLogResult.RechargeInfo.LifeCycleChgInfo.OldLifeCycleStatus> getOldLifeCycleStatus() {
				if (oldLifeCycleStatus == null) {
					oldLifeCycleStatus = new ArrayList<QueryRechargeLogResult.RechargeInfo.LifeCycleChgInfo.OldLifeCycleStatus>();
				}
				return this.oldLifeCycleStatus;
			}

			/**
			 * Gets the value of the newLifeCycleStatus property.
			 * 
			 * <p>
			 * This accessor method returns a reference to the live list, not a
			 * snapshot. Therefore any modification you make to the returned
			 * list will be present inside the JAXB object. This is why there is
			 * not a <CODE>set</CODE> method for the newLifeCycleStatus
			 * property.
			 * 
			 * <p>
			 * For example, to add a new item, do as follows:
			 * 
			 * <pre>
			 * getNewLifeCycleStatus().add(newItem);
			 * </pre>
			 * 
			 * 
			 * <p>
			 * Objects of the following type(s) are allowed in the list
			 * {@link QueryRechargeLogResult.RechargeInfo.LifeCycleChgInfo.NewLifeCycleStatus }
			 * 
			 * 
			 */
			public List<QueryRechargeLogResult.RechargeInfo.LifeCycleChgInfo.NewLifeCycleStatus> getNewLifeCycleStatus() {
				if (newLifeCycleStatus == null) {
					newLifeCycleStatus = new ArrayList<QueryRechargeLogResult.RechargeInfo.LifeCycleChgInfo.NewLifeCycleStatus>();
				}
				return this.newLifeCycleStatus;
			}

			/**
			 * Gets the value of the chgValidity property.
			 * 
			 * @return possible object is {@link BigInteger }
			 * 
			 */
			public BigInteger getChgValidity() {
				return chgValidity;
			}

			/**
			 * Sets the value of the chgValidity property.
			 * 
			 * @param value
			 *            allowed object is {@link BigInteger }
			 * 
			 */
			public void setChgValidity(BigInteger value) {
				this.chgValidity = value;
			}

			/**
			 * <p>
			 * Java class for anonymous complex type.
			 * 
			 * <p>
			 * The following schema fragment specifies the expected content
			 * contained within this class.
			 * 
			 * <pre>
			 * &lt;complexType>
			 *   &lt;complexContent>
			 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *       &lt;sequence>
			 *         &lt;element name="StatusName" type="{http://www.w3.org/2001/XMLSchema}string"/>
			 *         &lt;element name="StatusExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
			 *         &lt;element name="StatusIndex" type="{http://www.w3.org/2001/XMLSchema}string"/>
			 *       &lt;/sequence>
			 *     &lt;/restriction>
			 *   &lt;/complexContent>
			 * &lt;/complexType>
			 * </pre>
			 * 
			 * 
			 */
			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "statusName", "statusExpireTime", "statusIndex" })
			public static class NewLifeCycleStatus {
				@XmlElement(name = "StatusName", required = true)
				protected String statusName;
				@XmlElement(name = "StatusExpireTime", required = true)
				protected String statusExpireTime;
				@XmlElement(name = "StatusIndex", required = true)
				protected String statusIndex;

				/**
				 * Gets the value of the statusName property.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getStatusName() {
					return statusName;
				}

				/**
				 * Sets the value of the statusName property.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 * 
				 */
				public void setStatusName(String value) {
					this.statusName = value;
				}

				/**
				 * Gets the value of the statusExpireTime property.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getStatusExpireTime() {
					return statusExpireTime;
				}

				/**
				 * Sets the value of the statusExpireTime property.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 * 
				 */
				public void setStatusExpireTime(String value) {
					this.statusExpireTime = value;
				}

				/**
				 * Gets the value of the statusIndex property.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getStatusIndex() {
					return statusIndex;
				}

				/**
				 * Sets the value of the statusIndex property.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 * 
				 */
				public void setStatusIndex(String value) {
					this.statusIndex = value;
				}
			}

			/**
			 * <p>
			 * Java class for anonymous complex type.
			 * 
			 * <p>
			 * The following schema fragment specifies the expected content
			 * contained within this class.
			 * 
			 * <pre>
			 * &lt;complexType>
			 *   &lt;complexContent>
			 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *       &lt;sequence>
			 *         &lt;element name="StatusName" type="{http://www.w3.org/2001/XMLSchema}string"/>
			 *         &lt;element name="StatusExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
			 *         &lt;element name="StatusIndex" type="{http://www.w3.org/2001/XMLSchema}string"/>
			 *       &lt;/sequence>
			 *     &lt;/restriction>
			 *   &lt;/complexContent>
			 * &lt;/complexType>
			 * </pre>
			 * 
			 * 
			 */
			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "statusName", "statusExpireTime", "statusIndex" })
			public static class OldLifeCycleStatus {
				@XmlElement(name = "StatusName", required = true)
				protected String statusName;
				@XmlElement(name = "StatusExpireTime", required = true)
				protected String statusExpireTime;
				@XmlElement(name = "StatusIndex", required = true)
				protected String statusIndex;

				/**
				 * Gets the value of the statusName property.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getStatusName() {
					return statusName;
				}

				/**
				 * Sets the value of the statusName property.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 * 
				 */
				public void setStatusName(String value) {
					this.statusName = value;
				}

				/**
				 * Gets the value of the statusExpireTime property.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getStatusExpireTime() {
					return statusExpireTime;
				}

				/**
				 * Sets the value of the statusExpireTime property.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 * 
				 */
				public void setStatusExpireTime(String value) {
					this.statusExpireTime = value;
				}

				/**
				 * Gets the value of the statusIndex property.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getStatusIndex() {
					return statusIndex;
				}

				/**
				 * Sets the value of the statusIndex property.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 * 
				 */
				public void setStatusIndex(String value) {
					this.statusIndex = value;
				}
			}
		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * 
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="FreeUnitItemList" maxOccurs="unbounded" minOccurs="0">
		 *           &lt;complexType>
		 *             &lt;complexContent>
		 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                 &lt;sequence>
		 *                   &lt;element name="FreeUnitID" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *                   &lt;element name="FreeUnitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *                   &lt;element name="FreeUnitTypeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
		 *                   &lt;element name="MeasureUnit" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *                   &lt;element name="MeasureUnitName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
		 *                   &lt;element name="BonusAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
		 *                   &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *                   &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *                 &lt;/sequence>
		 *               &lt;/restriction>
		 *             &lt;/complexContent>
		 *           &lt;/complexType>
		 *         &lt;/element>
		 *         &lt;element name="BalanceList" maxOccurs="unbounded" minOccurs="0">
		 *           &lt;complexType>
		 *             &lt;complexContent>
		 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                 &lt;sequence>
		 *                   &lt;element name="BalanceType" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *                   &lt;element name="BalanceID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
		 *                   &lt;element name="BalanceTypeName" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *                   &lt;element name="BonusAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
		 *                   &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
		 *                   &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *                   &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *                 &lt;/sequence>
		 *               &lt;/restriction>
		 *             &lt;/complexContent>
		 *           &lt;/complexType>
		 *         &lt;/element>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "freeUnitItemList", "balanceList" })
		public static class RechargeBonus {
			@XmlElement(name = "FreeUnitItemList")
			protected List<QueryRechargeLogResult.RechargeInfo.RechargeBonus.FreeUnitItemList> freeUnitItemList;
			@XmlElement(name = "BalanceList")
			protected List<QueryRechargeLogResult.RechargeInfo.RechargeBonus.BalanceList> balanceList;

			/**
			 * Gets the value of the freeUnitItemList property.
			 * 
			 * <p>
			 * This accessor method returns a reference to the live list, not a
			 * snapshot. Therefore any modification you make to the returned
			 * list will be present inside the JAXB object. This is why there is
			 * not a <CODE>set</CODE> method for the freeUnitItemList property.
			 * 
			 * <p>
			 * For example, to add a new item, do as follows:
			 * 
			 * <pre>
			 * getFreeUnitItemList().add(newItem);
			 * </pre>
			 * 
			 * 
			 * <p>
			 * Objects of the following type(s) are allowed in the list
			 * {@link QueryRechargeLogResult.RechargeInfo.RechargeBonus.FreeUnitItemList }
			 * 
			 * 
			 */
			public List<QueryRechargeLogResult.RechargeInfo.RechargeBonus.FreeUnitItemList> getFreeUnitItemList() {
				if (freeUnitItemList == null) {
					freeUnitItemList = new ArrayList<QueryRechargeLogResult.RechargeInfo.RechargeBonus.FreeUnitItemList>();
				}
				return this.freeUnitItemList;
			}

			/**
			 * Gets the value of the balanceList property.
			 * 
			 * <p>
			 * This accessor method returns a reference to the live list, not a
			 * snapshot. Therefore any modification you make to the returned
			 * list will be present inside the JAXB object. This is why there is
			 * not a <CODE>set</CODE> method for the balanceList property.
			 * 
			 * <p>
			 * For example, to add a new item, do as follows:
			 * 
			 * <pre>
			 * getBalanceList().add(newItem);
			 * </pre>
			 * 
			 * 
			 * <p>
			 * Objects of the following type(s) are allowed in the list
			 * {@link QueryRechargeLogResult.RechargeInfo.RechargeBonus.BalanceList }
			 * 
			 * 
			 */
			public List<QueryRechargeLogResult.RechargeInfo.RechargeBonus.BalanceList> getBalanceList() {
				if (balanceList == null) {
					balanceList = new ArrayList<QueryRechargeLogResult.RechargeInfo.RechargeBonus.BalanceList>();
				}
				return this.balanceList;
			}

			/**
			 * <p>
			 * Java class for anonymous complex type.
			 * 
			 * <p>
			 * The following schema fragment specifies the expected content
			 * contained within this class.
			 * 
			 * <pre>
			 * &lt;complexType>
			 *   &lt;complexContent>
			 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *       &lt;sequence>
			 *         &lt;element name="BalanceType" type="{http://www.w3.org/2001/XMLSchema}string"/>
			 *         &lt;element name="BalanceID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
			 *         &lt;element name="BalanceTypeName" type="{http://www.w3.org/2001/XMLSchema}string"/>
			 *         &lt;element name="BonusAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
			 *         &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
			 *         &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
			 *         &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
			 *       &lt;/sequence>
			 *     &lt;/restriction>
			 *   &lt;/complexContent>
			 * &lt;/complexType>
			 * </pre>
			 * 
			 * 
			 */
			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "balanceType", "balanceID", "balanceTypeName", "bonusAmt", "currencyID",
					"effectiveTime", "expireTime" })
			public static class BalanceList {
				@XmlElement(name = "BalanceType", required = true)
				protected String balanceType;
				@XmlElement(name = "BalanceID")
				protected String balanceID;
				@XmlElement(name = "BalanceTypeName", required = true)
				protected String balanceTypeName;
				@XmlElement(name = "BonusAmt")
				protected long bonusAmt;
				@XmlElement(name = "CurrencyID", required = true)
				protected BigInteger currencyID;
				@XmlElement(name = "EffectiveTime", required = true)
				protected String effectiveTime;
				@XmlElement(name = "ExpireTime", required = true)
				protected String expireTime;

				/**
				 * Gets the value of the balanceType property.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getBalanceType() {
					return balanceType;
				}

				/**
				 * Sets the value of the balanceType property.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 * 
				 */
				public void setBalanceType(String value) {
					this.balanceType = value;
				}

				/**
				 * Gets the value of the balanceID property.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getBalanceID() {
					return balanceID;
				}

				/**
				 * Sets the value of the balanceID property.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 * 
				 */
				public void setBalanceID(String value) {
					this.balanceID = value;
				}

				/**
				 * Gets the value of the balanceTypeName property.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getBalanceTypeName() {
					return balanceTypeName;
				}

				/**
				 * Sets the value of the balanceTypeName property.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 * 
				 */
				public void setBalanceTypeName(String value) {
					this.balanceTypeName = value;
				}

				/**
				 * Gets the value of the bonusAmt property.
				 * 
				 */
				public long getBonusAmt() {
					return bonusAmt;
				}

				/**
				 * Sets the value of the bonusAmt property.
				 * 
				 */
				public void setBonusAmt(long value) {
					this.bonusAmt = value;
				}

				/**
				 * Gets the value of the currencyID property.
				 * 
				 * @return possible object is {@link BigInteger }
				 * 
				 */
				public BigInteger getCurrencyID() {
					return currencyID;
				}

				/**
				 * Sets the value of the currencyID property.
				 * 
				 * @param value
				 *            allowed object is {@link BigInteger }
				 * 
				 */
				public void setCurrencyID(BigInteger value) {
					this.currencyID = value;
				}

				/**
				 * Gets the value of the effectiveTime property.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getEffectiveTime() {
					return effectiveTime;
				}

				/**
				 * Sets the value of the effectiveTime property.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 * 
				 */
				public void setEffectiveTime(String value) {
					this.effectiveTime = value;
				}

				/**
				 * Gets the value of the expireTime property.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getExpireTime() {
					return expireTime;
				}

				/**
				 * Sets the value of the expireTime property.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 * 
				 */
				public void setExpireTime(String value) {
					this.expireTime = value;
				}
			}

			/**
			 * <p>
			 * Java class for anonymous complex type.
			 * 
			 * <p>
			 * The following schema fragment specifies the expected content
			 * contained within this class.
			 * 
			 * <pre>
			 * &lt;complexType>
			 *   &lt;complexContent>
			 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *       &lt;sequence>
			 *         &lt;element name="FreeUnitID" type="{http://www.w3.org/2001/XMLSchema}string"/>
			 *         &lt;element name="FreeUnitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
			 *         &lt;element name="FreeUnitTypeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
			 *         &lt;element name="MeasureUnit" type="{http://www.w3.org/2001/XMLSchema}string"/>
			 *         &lt;element name="MeasureUnitName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
			 *         &lt;element name="BonusAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
			 *         &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
			 *         &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
			 *       &lt;/sequence>
			 *     &lt;/restriction>
			 *   &lt;/complexContent>
			 * &lt;/complexType>
			 * </pre>
			 * 
			 * 
			 */
			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "freeUnitID", "freeUnitType", "freeUnitTypeName", "measureUnit",
					"measureUnitName", "bonusAmt", "effectiveTime", "expireTime" })
			public static class FreeUnitItemList {
				@XmlElement(name = "FreeUnitID", required = true)
				protected String freeUnitID;
				@XmlElement(name = "FreeUnitType", required = true)
				protected String freeUnitType;
				@XmlElement(name = "FreeUnitTypeName")
				protected String freeUnitTypeName;
				@XmlElement(name = "MeasureUnit", required = true)
				protected String measureUnit;
				@XmlElement(name = "MeasureUnitName")
				protected String measureUnitName;
				@XmlElement(name = "BonusAmt", required = true, type = Long.class, nillable = true)
				protected Long bonusAmt;
				@XmlElement(name = "EffectiveTime", required = true)
				protected String effectiveTime;
				@XmlElement(name = "ExpireTime", required = true)
				protected String expireTime;

				/**
				 * Gets the value of the freeUnitID property.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getFreeUnitID() {
					return freeUnitID;
				}

				/**
				 * Sets the value of the freeUnitID property.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 * 
				 */
				public void setFreeUnitID(String value) {
					this.freeUnitID = value;
				}

				/**
				 * Gets the value of the freeUnitType property.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getFreeUnitType() {
					return freeUnitType;
				}

				/**
				 * Sets the value of the freeUnitType property.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 * 
				 */
				public void setFreeUnitType(String value) {
					this.freeUnitType = value;
				}

				/**
				 * Gets the value of the freeUnitTypeName property.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getFreeUnitTypeName() {
					return freeUnitTypeName;
				}

				/**
				 * Sets the value of the freeUnitTypeName property.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 * 
				 */
				public void setFreeUnitTypeName(String value) {
					this.freeUnitTypeName = value;
				}

				/**
				 * Gets the value of the measureUnit property.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getMeasureUnit() {
					return measureUnit;
				}

				/**
				 * Sets the value of the measureUnit property.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 * 
				 */
				public void setMeasureUnit(String value) {
					this.measureUnit = value;
				}

				/**
				 * Gets the value of the measureUnitName property.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getMeasureUnitName() {
					return measureUnitName;
				}

				/**
				 * Sets the value of the measureUnitName property.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 * 
				 */
				public void setMeasureUnitName(String value) {
					this.measureUnitName = value;
				}

				/**
				 * Gets the value of the bonusAmt property.
				 * 
				 * @return possible object is {@link Long }
				 * 
				 */
				public Long getBonusAmt() {
					return bonusAmt;
				}

				/**
				 * Sets the value of the bonusAmt property.
				 * 
				 * @param value
				 *            allowed object is {@link Long }
				 * 
				 */
				public void setBonusAmt(Long value) {
					this.bonusAmt = value;
				}

				/**
				 * Gets the value of the effectiveTime property.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getEffectiveTime() {
					return effectiveTime;
				}

				/**
				 * Sets the value of the effectiveTime property.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 * 
				 */
				public void setEffectiveTime(String value) {
					this.effectiveTime = value;
				}

				/**
				 * Gets the value of the expireTime property.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getExpireTime() {
					return expireTime;
				}

				/**
				 * Sets the value of the expireTime property.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 * 
				 */
				public void setExpireTime(String value) {
					this.expireTime = value;
				}
			}
		}
	}
}
