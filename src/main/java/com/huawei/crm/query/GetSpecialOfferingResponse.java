package com.huawei.crm.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.crm.basetype.ResponseHeader;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResponseHeader" type="{http://crm.huawei.com/basetype/}ResponseHeader"/>
 *         &lt;element name="GeSpecialOfferingBody" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="SubscriberId" type="{http://crm.huawei.com/basetype/}SubscriberId"/>
 *                   &lt;element name="SubStatus" type="{http://crm.huawei.com/basetype/}SubscriberStatus"/>
 *                   &lt;element name="PaymentType" type="{http://crm.huawei.com/basetype/}PaymentType"/>
 *                   &lt;element name="SpecialOfferingList" type="{http://crm.huawei.com/query/}GetSpecialOfferingList"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "responseHeader", "geSpecialOfferingBody" })
@XmlRootElement(name = "GetSpecialOfferingResponse")
public class GetSpecialOfferingResponse {
	@XmlElement(name = "ResponseHeader", required = true)
	protected ResponseHeader responseHeader;
	@XmlElement(name = "GeSpecialOfferingBody")
	protected GetSpecialOfferingResponse.GeSpecialOfferingBody geSpecialOfferingBody;

	/**
	 * Gets the value of the responseHeader property.
	 * 
	 * @return possible object is {@link ResponseHeader }
	 * 
	 */
	public ResponseHeader getResponseHeader() {
		return responseHeader;
	}

	/**
	 * Sets the value of the responseHeader property.
	 * 
	 * @param value
	 *            allowed object is {@link ResponseHeader }
	 * 
	 */
	public void setResponseHeader(ResponseHeader value) {
		this.responseHeader = value;
	}

	/**
	 * Gets the value of the geSpecialOfferingBody property.
	 * 
	 * @return possible object is
	 *         {@link GetSpecialOfferingResponse.GeSpecialOfferingBody }
	 * 
	 */
	public GetSpecialOfferingResponse.GeSpecialOfferingBody getGeSpecialOfferingBody() {
		return geSpecialOfferingBody;
	}

	/**
	 * Sets the value of the geSpecialOfferingBody property.
	 * 
	 * @param value
	 *            allowed object is
	 *            {@link GetSpecialOfferingResponse.GeSpecialOfferingBody }
	 * 
	 */
	public void setGeSpecialOfferingBody(GetSpecialOfferingResponse.GeSpecialOfferingBody value) {
		this.geSpecialOfferingBody = value;
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;all>
	 *         &lt;element name="SubscriberId" type="{http://crm.huawei.com/basetype/}SubscriberId"/>
	 *         &lt;element name="SubStatus" type="{http://crm.huawei.com/basetype/}SubscriberStatus"/>
	 *         &lt;element name="PaymentType" type="{http://crm.huawei.com/basetype/}PaymentType"/>
	 *         &lt;element name="SpecialOfferingList" type="{http://crm.huawei.com/query/}GetSpecialOfferingList"/>
	 *       &lt;/all>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = {})
	public static class GeSpecialOfferingBody {
		@XmlElement(name = "SubscriberId")
		protected long subscriberId;
		@XmlElement(name = "SubStatus", required = true)
		protected String subStatus;
		@XmlElement(name = "PaymentType", required = true)
		protected String paymentType;
		@XmlElement(name = "SpecialOfferingList", required = true)
		protected GetSpecialOfferingList specialOfferingList;

		/**
		 * Gets the value of the subscriberId property.
		 * 
		 */
		public long getSubscriberId() {
			return subscriberId;
		}

		/**
		 * Sets the value of the subscriberId property.
		 * 
		 */
		public void setSubscriberId(long value) {
			this.subscriberId = value;
		}

		/**
		 * Gets the value of the subStatus property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getSubStatus() {
			return subStatus;
		}

		/**
		 * Sets the value of the subStatus property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setSubStatus(String value) {
			this.subStatus = value;
		}

		/**
		 * Gets the value of the paymentType property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getPaymentType() {
			return paymentType;
		}

		/**
		 * Sets the value of the paymentType property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setPaymentType(String value) {
			this.paymentType = value;
		}

		/**
		 * Gets the value of the specialOfferingList property.
		 * 
		 * @return possible object is {@link GetSpecialOfferingList }
		 * 
		 */
		public GetSpecialOfferingList getSpecialOfferingList() {
			return specialOfferingList;
		}

		/**
		 * Sets the value of the specialOfferingList property.
		 * 
		 * @param value
		 *            allowed object is {@link GetSpecialOfferingList }
		 * 
		 */
		public void setSpecialOfferingList(GetSpecialOfferingList value) {
			this.specialOfferingList = value;
		}
	}
}
