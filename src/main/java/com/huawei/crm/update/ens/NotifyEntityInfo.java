package com.huawei.crm.update.ens;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for NotifyEntityInfo complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="NotifyEntityInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="EntityTypeId" type="{http://crm.huawei.com/update/}NotifyEntityTypeId"/>
 *         &lt;element name="EntityInstCode">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="50"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="SubEntityInfo" type="{http://crm.huawei.com/update/}NotifyEntityInfo" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NotifyEntityInfo", propOrder = {})
public class NotifyEntityInfo {
	@XmlElement(name = "EntityTypeId", required = true)
	protected String entityTypeId;
	@XmlElement(name = "EntityInstCode", required = true)
	protected String entityInstCode;
	@XmlElement(name = "SubEntityInfo")
	protected NotifyEntityInfo subEntityInfo;

	/**
	 * Gets the value of the entityTypeId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEntityTypeId() {
		return entityTypeId;
	}

	/**
	 * Sets the value of the entityTypeId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEntityTypeId(String value) {
		this.entityTypeId = value;
	}

	/**
	 * Gets the value of the entityInstCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEntityInstCode() {
		return entityInstCode;
	}

	/**
	 * Sets the value of the entityInstCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEntityInstCode(String value) {
		this.entityInstCode = value;
	}

	/**
	 * Gets the value of the subEntityInfo property.
	 * 
	 * @return possible object is {@link NotifyEntityInfo }
	 * 
	 */
	public NotifyEntityInfo getSubEntityInfo() {
		return subEntityInfo;
	}

	/**
	 * Sets the value of the subEntityInfo property.
	 * 
	 * @param value
	 *            allowed object is {@link NotifyEntityInfo }
	 * 
	 */
	public void setSubEntityInfo(NotifyEntityInfo value) {
		this.subEntityInfo = value;
	}
}
