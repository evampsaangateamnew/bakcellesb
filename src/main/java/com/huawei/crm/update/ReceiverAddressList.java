package com.huawei.crm.update;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for ReceiverAddressList complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="ReceiverAddressList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ReceiverEntity" type="{http://crm.huawei.com/update/}ReceiverEntityInfo" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReceiverAddressList", propOrder = { "receiverEntity" })
public class ReceiverAddressList {
	@XmlElement(name = "ReceiverEntity", required = true)
	protected List<ReceiverEntityInfo> receiverEntity;

	/**
	 * Gets the value of the receiverEntity property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the receiverEntity property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getReceiverEntity().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link ReceiverEntityInfo }
	 * 
	 * 
	 */
	public List<ReceiverEntityInfo> getReceiverEntity() {
		if (receiverEntity == null) {
			receiverEntity = new ArrayList<ReceiverEntityInfo>();
		}
		return this.receiverEntity;
	}
}
