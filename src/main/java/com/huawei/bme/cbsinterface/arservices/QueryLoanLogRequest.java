package com.huawei.bme.cbsinterface.arservices;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.cbs.ar.wsservice.arcommon.SimpleProperty;
import com.huawei.cbs.ar.wsservice.arcommon.SubAccessCode;

/**
 * <p>
 * Java class for QueryLoanLogRequest complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="QueryLoanLogRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubAccessCode" type="{http://cbs.huawei.com/ar/wsservice/arcommon}SubAccessCode"/>
 *         &lt;element name="AdditionalProperty" type="{http://cbs.huawei.com/ar/wsservice/arcommon}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryLoanLogRequest", propOrder = { "subAccessCode", "TotalRowNum", "BeginRowNum", "FetchRowNum",
		"additionalProperty" })
public class QueryLoanLogRequest {
	@XmlElement(name = "TotalRowNum", namespace = "http://www.huawei.com/bme/cbsinterface/arservices", required = true)
	protected String TotalRowNum;
	@XmlElement(name = "BeginRowNum", namespace = "http://www.huawei.com/bme/cbsinterface/arservices", required = true)
	protected String BeginRowNum;
	@XmlElement(name = "FetchRowNum", namespace = "http://www.huawei.com/bme/cbsinterface/arservices", required = true)
	protected String FetchRowNum;

	public String getTotalRowNum() {
		return TotalRowNum;
	}

	public void setTotalRowNum(String totalRowNum) {
		TotalRowNum = totalRowNum;
	}

	public String getBeginRowNum() {
		return BeginRowNum;
	}

	public void setBeginRowNum(String beginRowNum) {
		BeginRowNum = beginRowNum;
	}

	public String getFetchRowNum() {
		return FetchRowNum;
	}

	public void setFetchRowNum(String fetchRowNum) {
		FetchRowNum = fetchRowNum;
	}

	@XmlElement(name = "SubAccessCode", required = true)
	protected SubAccessCode subAccessCode;
	@XmlElement(name = "AdditionalProperty")
	protected List<SimpleProperty> additionalProperty;

	/**
	 * Gets the value of the subAccessCode property.
	 * 
	 * @return possible object is {@link SubAccessCode }
	 * 
	 */
	public SubAccessCode getSubAccessCode() {
		return subAccessCode;
	}

	/**
	 * Sets the value of the subAccessCode property.
	 * 
	 * @param value
	 *            allowed object is {@link SubAccessCode }
	 * 
	 */
	public void setSubAccessCode(SubAccessCode value) {
		this.subAccessCode = value;
	}

	/**
	 * Gets the value of the additionalProperty property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the additionalProperty property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getAdditionalProperty().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link SimpleProperty }
	 * 
	 * 
	 */
	public List<SimpleProperty> getAdditionalProperty() {
		if (additionalProperty == null) {
			additionalProperty = new ArrayList<SimpleProperty>();
		}
		return this.additionalProperty;
	}
}
