package com.huawei.bss.soaif._interface.mnpservice;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the com.huawei.bss.soaif._interface.mnpservice
 * package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {
	/**
	 * Create a new ObjectFactory that can be used to create new instances of
	 * schema derived classes for package:
	 * com.huawei.bss.soaif._interface.mnpservice
	 * 
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link SubscriptionInfoGetResponse }
	 * 
	 */
	public SubscriptionInfoGetResponse createSubscriptionInfoGetResponse() {
		return new SubscriptionInfoGetResponse();
	}

	/**
	 * Create an instance of {@link SubscriptionInfoGetOut }
	 * 
	 */
	public SubscriptionInfoGetOut createSubscriptionInfoGetOut() {
		return new SubscriptionInfoGetOut();
	}

	/**
	 * Create an instance of {@link SubscriptionInfoGetRequest }
	 * 
	 */
	public SubscriptionInfoGetRequest createSubscriptionInfoGetRequest() {
		return new SubscriptionInfoGetRequest();
	}

	/**
	 * Create an instance of {@link SubscriptionInfoGetIn }
	 * 
	 */
	public SubscriptionInfoGetIn createSubscriptionInfoGetIn() {
		return new SubscriptionInfoGetIn();
	}
}
