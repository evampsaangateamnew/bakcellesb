package com.evampsaanga.bakcell.getappmenu;

import com.evampsaanga.bakcell.responseheaders.BaseResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

public class GetAppMenuResponseUpdated extends BaseResponse {
	
	@JsonProperty("data")
	private Datum data = new Datum();
	@JsonProperty("data")
	public Datum getData() {
		return data;
	}
	@JsonProperty("data")
	public void setData(Datum data) {
		this.data = data;
	}

}
