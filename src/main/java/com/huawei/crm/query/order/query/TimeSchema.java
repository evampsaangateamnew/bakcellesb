
package com.huawei.crm.query.order.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TimeSchema complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TimeSchema">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="TimeSchemaID" type="{http://crm.huawei.com/basetype/}TimeSchemaID" minOccurs="0"/>
 *         &lt;element name="TimeSchemaName" type="{http://crm.huawei.com/basetype/}TimeSchemaName" minOccurs="0"/>
 *         &lt;element name="ExtRule" type="{http://crm.huawei.com/basetype/}ExtRule" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TimeSchema", propOrder = {

})
public class TimeSchema {

    @XmlElement(name = "TimeSchemaID")
    protected String timeSchemaID;
    @XmlElement(name = "TimeSchemaName")
    protected String timeSchemaName;
    @XmlElement(name = "ExtRule")
    protected String extRule;

    /**
     * Gets the value of the timeSchemaID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeSchemaID() {
        return timeSchemaID;
    }

    /**
     * Sets the value of the timeSchemaID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeSchemaID(String value) {
        this.timeSchemaID = value;
    }

    /**
     * Gets the value of the timeSchemaName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeSchemaName() {
        return timeSchemaName;
    }

    /**
     * Sets the value of the timeSchemaName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeSchemaName(String value) {
        this.timeSchemaName = value;
    }

    /**
     * Gets the value of the extRule property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtRule() {
        return extRule;
    }

    /**
     * Sets the value of the extRule property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtRule(String value) {
        this.extRule = value;
    }

}
