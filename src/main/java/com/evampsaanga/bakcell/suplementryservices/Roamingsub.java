package com.evampsaanga.bakcell.suplementryservices;

public class Roamingsub {
	private String descriptionPosition;
	private String description;
	private Country[] country;

	public String getDescriptionPosition() {
		return descriptionPosition;
	}

	public void setDescriptionPosition(String descriptionPosition) {
		this.descriptionPosition = descriptionPosition;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Country[] getCountry() {
		return country;
	}

	public void setCountry(Country[] country) {
		this.country = country;
	}
}
