package com.evampsaanga.bakcell.getcoreservices;

import com.evampsaanga.bakcell.responseheaders.BaseResponse;

public class GetCoreServicesResponse extends BaseResponse {
	private Data data = new Data();

	/**
	 * @return the data
	 */
	public Data getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(Data data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "GetCoreServicesResponse [data=" + data + ", toString()=" + super.toString() + "]";
	}
	
	
}
