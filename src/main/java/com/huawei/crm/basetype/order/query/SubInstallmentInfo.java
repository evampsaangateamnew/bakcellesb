
package com.huawei.crm.basetype.order.query;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SubInstallmentInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubInstallmentInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InstallmentID" type="{http://crm.huawei.com/basetype/}InstallmentID" minOccurs="0"/>
 *         &lt;element name="TotalAmount" type="{http://crm.huawei.com/basetype/}Amount" minOccurs="0"/>
 *         &lt;element name="RemainingAmount" type="{http://crm.huawei.com/basetype/}Amount" minOccurs="0"/>
 *         &lt;element name="TotalPeriods" type="{http://crm.huawei.com/basetype/}int" minOccurs="0"/>
 *         &lt;element name="RemainingPeriods" type="{http://crm.huawei.com/basetype/}int" minOccurs="0"/>
 *         &lt;element name="BeginDate" type="{http://crm.huawei.com/basetype/}Time" minOccurs="0"/>
 *         &lt;element name="EndDate" type="{http://crm.huawei.com/basetype/}Time" minOccurs="0"/>
 *         &lt;element name="InstallmentStatus" type="{http://crm.huawei.com/basetype/}InstallmentStatus" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubInstallmentInfo", propOrder = {
    "installmentID",
    "totalAmount",
    "remainingAmount",
    "totalPeriods",
    "remainingPeriods",
    "beginDate",
    "endDate",
    "installmentStatus"
})
public class SubInstallmentInfo {

    @XmlElement(name = "InstallmentID")
    protected String installmentID;
    @XmlElement(name = "TotalAmount")
    protected Long totalAmount;
    @XmlElement(name = "RemainingAmount")
    protected Long remainingAmount;
    @XmlElement(name = "TotalPeriods")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger totalPeriods;
    @XmlElement(name = "RemainingPeriods")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger remainingPeriods;
    @XmlElement(name = "BeginDate")
    protected String beginDate;
    @XmlElement(name = "EndDate")
    protected String endDate;
    @XmlElement(name = "InstallmentStatus")
    protected String installmentStatus;

    /**
     * Gets the value of the installmentID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstallmentID() {
        return installmentID;
    }

    /**
     * Sets the value of the installmentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstallmentID(String value) {
        this.installmentID = value;
    }

    /**
     * Gets the value of the totalAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getTotalAmount() {
        return totalAmount;
    }

    /**
     * Sets the value of the totalAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setTotalAmount(Long value) {
        this.totalAmount = value;
    }

    /**
     * Gets the value of the remainingAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getRemainingAmount() {
        return remainingAmount;
    }

    /**
     * Sets the value of the remainingAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setRemainingAmount(Long value) {
        this.remainingAmount = value;
    }

    /**
     * Gets the value of the totalPeriods property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTotalPeriods() {
        return totalPeriods;
    }

    /**
     * Sets the value of the totalPeriods property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTotalPeriods(BigInteger value) {
        this.totalPeriods = value;
    }

    /**
     * Gets the value of the remainingPeriods property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getRemainingPeriods() {
        return remainingPeriods;
    }

    /**
     * Sets the value of the remainingPeriods property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setRemainingPeriods(BigInteger value) {
        this.remainingPeriods = value;
    }

    /**
     * Gets the value of the beginDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBeginDate() {
        return beginDate;
    }

    /**
     * Sets the value of the beginDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBeginDate(String value) {
        this.beginDate = value;
    }

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndDate(String value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the installmentStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstallmentStatus() {
        return installmentStatus;
    }

    /**
     * Sets the value of the installmentStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstallmentStatus(String value) {
        this.installmentStatus = value;
    }

}
