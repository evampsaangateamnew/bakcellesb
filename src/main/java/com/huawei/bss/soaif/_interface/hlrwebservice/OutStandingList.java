package com.huawei.bss.soaif._interface.hlrwebservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for OutStandingList complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="OutStandingList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BillCycleID" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="BillCycleBeginTime" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime" minOccurs="0"/>
 *         &lt;element name="BillCycleEndTime" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime" minOccurs="0"/>
 *         &lt;element name="DueDate" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime" minOccurs="0"/>
 *         &lt;element name="OutStandingDetail" type="{http://www.huawei.com/bss/soaif/interface/HLRWEBService/}OutStandingDetail" minOccurs="0"/>
 *         &lt;element name="BillCycleType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OutStandingList", propOrder = { "billCycleID", "billCycleBeginTime", "billCycleEndTime", "dueDate",
		"outStandingDetail", "billCycleType" })
public class OutStandingList {
	@XmlElement(name = "BillCycleID")
	protected String billCycleID;
	@XmlElement(name = "BillCycleBeginTime")
	protected String billCycleBeginTime;
	@XmlElement(name = "BillCycleEndTime")
	protected String billCycleEndTime;
	@XmlElement(name = "DueDate")
	protected String dueDate;
	@XmlElement(name = "OutStandingDetail")
	protected OutStandingDetail outStandingDetail;
	@XmlElement(name = "BillCycleType", required = true)
	protected String billCycleType;

	/**
	 * Gets the value of the billCycleID property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBillCycleID() {
		return billCycleID;
	}

	/**
	 * Sets the value of the billCycleID property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBillCycleID(String value) {
		this.billCycleID = value;
	}

	/**
	 * Gets the value of the billCycleBeginTime property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBillCycleBeginTime() {
		return billCycleBeginTime;
	}

	/**
	 * Sets the value of the billCycleBeginTime property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBillCycleBeginTime(String value) {
		this.billCycleBeginTime = value;
	}

	/**
	 * Gets the value of the billCycleEndTime property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBillCycleEndTime() {
		return billCycleEndTime;
	}

	/**
	 * Sets the value of the billCycleEndTime property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBillCycleEndTime(String value) {
		this.billCycleEndTime = value;
	}

	/**
	 * Gets the value of the dueDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDueDate() {
		return dueDate;
	}

	/**
	 * Sets the value of the dueDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDueDate(String value) {
		this.dueDate = value;
	}

	/**
	 * Gets the value of the outStandingDetail property.
	 * 
	 * @return possible object is {@link OutStandingDetail }
	 * 
	 */
	public OutStandingDetail getOutStandingDetail() {
		return outStandingDetail;
	}

	/**
	 * Sets the value of the outStandingDetail property.
	 * 
	 * @param value
	 *            allowed object is {@link OutStandingDetail }
	 * 
	 */
	public void setOutStandingDetail(OutStandingDetail value) {
		this.outStandingDetail = value;
	}

	/**
	 * Gets the value of the billCycleType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBillCycleType() {
		return billCycleType;
	}

	/**
	 * Sets the value of the billCycleType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBillCycleType(String value) {
		this.billCycleType = value;
	}
}
