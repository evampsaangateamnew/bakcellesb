package com.evampsaanga.magentoservices;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.amqimplementationsesb.LogsType;
import com.evampsaanga.appserver.refreshappservercache.BuildCacheRequestLand;
import com.evampsaanga.appserver.refreshappservercache.CustomerModelCache;
import com.evampsaanga.authorization.AuthorizationAndAuthentication;
import com.evampsaanga.bakcell.changepassword.ChangePasswordRequest;
import com.evampsaanga.bakcell.changepassword.ChangePasswordRequestV2;
import com.evampsaanga.bakcell.changepassword.ChangePasswordResponse;
import com.evampsaanga.bakcell.changesuplimentry.ChangeSupplimentryOfferingSignupRequestLand;
import com.evampsaanga.bakcell.changesuplimentry.Response;
import com.evampsaanga.bakcell.db.DBFactory;
import com.evampsaanga.bakcell.forgotpassword.ForgotPasswordRequest;
import com.evampsaanga.bakcell.forgotpassword.ForgotPasswordRequestV2;
import com.evampsaanga.bakcell.forgotpassword.ForgotPasswordResponse;
import com.evampsaanga.bakcell.getappfaq.GetAppFaqRequest;
import com.evampsaanga.bakcell.getappfaq.GetAppFaqResponse;
import com.evampsaanga.bakcell.getappfaq.MagentoResponse;
import com.evampsaanga.bakcell.getappfaq.MagentoResponseV2;
import com.evampsaanga.bakcell.getappmenu.GetAppMenuRequest;
import com.evampsaanga.bakcell.getappmenu.GetAppMenuResponse;
import com.evampsaanga.bakcell.getappmenu.GetAppMenuResponseUpdated;
import com.evampsaanga.bakcell.getappmenu.GetAppMenuResponseV2;
import com.evampsaanga.bakcell.getcustomerrequest.GetCustomerDataLand;
import com.evampsaanga.bakcell.getcustomerrequest.GetCustomerRequestClient;
import com.evampsaanga.bakcell.getcustomerrequest.GetCustomerRequestResponse;
import com.evampsaanga.bakcell.getstorelocator.GetStoreLocatorRequestClient;
import com.evampsaanga.bakcell.getstorelocator.GetStoreLocatorResponseClient;
import com.evampsaanga.bakcell.otp.MessageTemplateResponse;
import com.evampsaanga.bakcell.rateusV2.RateUsRequest;
import com.evampsaanga.bakcell.rateusV2.RateUsResponse;
import com.evampsaanga.bakcell.rateusV2.RateUsResponseData;
import com.evampsaanga.bakcell.roamingcountriesdata.GetRoamingCountriesRequest;
import com.evampsaanga.bakcell.roamingcountriesdata.GetRoamingCountriesResponse;
import com.evampsaanga.bakcell.roamingcountriesdata.RoamingCountriesMagentoResponse;
import com.evampsaanga.bakcell.savecustomer.SaveCustomerRequest;
import com.evampsaanga.bakcell.savecustomer.SaveCustomerResponse;
import com.evampsaanga.bakcell.sendotp.ResendOTPRequest;
import com.evampsaanga.bakcell.sendotp.SendOTPRequest;
import com.evampsaanga.bakcell.sendotp.SendOTPResponse;
import com.evampsaanga.bakcell.signupresendotp.MagentoResponseValidateMsisdn;
import com.evampsaanga.bakcell.signupresendotp.MagentoResponseValidateMsisdnV2;
import com.evampsaanga.bakcell.signupresendotp.SignUpRequest;
import com.evampsaanga.bakcell.signupresendotp.SignUpResponse;
import com.evampsaanga.bakcell.sms.SendSMSRequest;
import com.evampsaanga.bakcell.sms.SendSMSResponse;
import com.evampsaanga.bakcell.suplementryservices.Offers;
import com.evampsaanga.bakcell.suplementryservices.SupplementryServicesRequest;
import com.evampsaanga.bakcell.suplementryservices.SupplementryServicesRequestV2;
import com.evampsaanga.bakcell.suplementryservices.SupplementryServicesResponse;
import com.evampsaanga.bakcell.suplementryservices.SupplimentryMagentoResponse;
import com.evampsaanga.bakcell.termsandconditionsV2.TnCRequest;
import com.evampsaanga.bakcell.termsandconditionsV2.TnCResponse;
import com.evampsaanga.bakcell.verifycdrsotp.VerifyCDRsOTPRequest;
import com.evampsaanga.bakcell.verifycdrsotp.VerifyCDRsOTPRequestV2;
import com.evampsaanga.bakcell.verifycdrsotp.VerifyCDRsOTPResponse;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.ThirdPartyNames;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.SendSMCService;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.developer.utils.SOAPLoggingHandler;
import com.evampsaanga.magento.getpredefineddata.GetpreDefineDataRequestClient;
import com.evampsaanga.magento.getpredefineddata.GetpreDefineDataResponseClient;
import com.evampsaanga.magento.getpredefineddataV2.DocumentTypesData;
import com.evampsaanga.magento.getpredefineddataV2.GetPredefinedDataRequest;
import com.evampsaanga.magento.getpredefineddataV2.GetPredefinedDataResponse;
import com.evampsaanga.magento.getpredefineddataV2.GetPredefinedDataResponseData;
import com.evampsaanga.magento.getpredefineddataV2.GetPredefinedDataResponseMagento;
import com.evampsaanga.magento.tariffdetails.support.updatedv2b2c.TariffDetailsMagentoResponse;
import com.evampsaanga.magento.tariffdetails.support.updatedv2b2c.TariffDetailsRequest;
import com.evampsaanga.magento.tariffdetails.support.updatedv2b2c.TariffDetailsResponse;
import com.evampsaanga.magento.tariffdetailsv2.TariffDetailsResponseVer2;
import com.evampsaanga.magentoservices.getcdrsbydateotp.GetCDRsByDateOTPMagentoResponse;
import com.evampsaanga.magentoservices.getcdrsbydateotp.GetCDRsByDateOTPRequest;
import com.evampsaanga.magentoservices.getcdrsbydateotp.GetCDRsByDateOTPResponse;
import com.evampsaanga.magentoservices.updateuseremail.UpdateUserEmailRequest;
import com.evampsaanga.magentoservices.updateuseremail.UpdateUserEmailResponse;
import com.evampsaanga.magentoservices.updateuseremail.UpdateUserEmailResponseClient;
import com.evampsaanga.magentoservices.validatecdrbydatepin.ValidateCDRByDateResponse;
import com.evampsaanga.magentoswervices.mysubscriptionofferingids.MySubscriptionsRequestClient;
import com.evampsaanga.magentoswervices.mysubscriptionofferingids.MySubscriptionsResponseClient;
import com.evampsaanga.models.LoginData;
import com.evampsaanga.models.LoginRequest;
import com.evampsaanga.models.LoginResponse;
import com.evampsaanga.validator.rules.ChannelNotEmpty;
import com.evampsaanga.validator.rules.IPNotEmpty;
import com.evampsaanga.validator.rules.LangNotEmpty;
import com.evampsaanga.validator.rules.MSISDNNotEmpty;
import com.evampsaanga.validator.rules.ValidationResult;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.saanga.magento.apiclient.RestClient;

/**
 * 
 * @author EvampSaanga modified by Aqeel Abbas Added new functions to support
 *         B2B
 * 
 */

@Path("/bakcell")
public class MagentoServices {
	public static final Logger logger = Logger.getLogger("bakcellLogs-V2");
	public static final Logger loggerV2 = Logger.getLogger("bakcellLogs-V2");
	public static String modeulName = "MagentoServices";

	@POST
	@Path("/appfaq")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetAppFaqResponse GetAppFaq(@Body String requestBody, @Header("credentials") String credential) {
		logger.info(modeulName + "Request: appFAQ" + requestBody);
		logger.info("credential: appFAQ" + credential);
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.APP_FAQ_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.APPFAQ);
		logs.setTableType(LogsType.AppFaq);
		GetAppFaqRequest cclient = null;
		GetAppFaqResponse resp = new GetAppFaqResponse();
		try {
			cclient = Helper.JsonToObject(requestBody, GetAppFaqRequest.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
				logs.setIsB2B(cclient.getIsB2B());
			}
		} catch (Exception ex) {
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_401);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("lang", cclient.getLang());
					// getConfigurationFromCache
					String response = RestClient.SendCallToMagento(
							ConfigurationManager.getConfigurationFromCache("magento.app.faq"), jsonObject.toString());
					logger.info(response);
					try {
						MagentoResponse data = Helper.JsonToObject(response, MagentoResponse.class);
						if (data.getResultCode() == 200) {
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							resp.setExecTime(data.getExecTime());
							resp.setData(data.getData());
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception ex) {
						resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} catch (JSONException ex) {
					resp.setReturnCode(ResponseCodes.MAGENTO_SERVER_ERROR_CODE);
					resp.setReturnMsg(ResponseCodes.MAGENTO_SERVER_ERROR_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				} catch (Exception ex) {
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		logs.updateLog(logs);
		return resp;
	}

	/**
	 * 
	 * @param requestBody
	 * @param credential
	 * @return App Faq Response
	 */
	// for phase 2

	@POST
	@Path("/appfaqs")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetAppFaqResponse GetAppFaqV2(@Body String requestBody, @Header("credentials") String credential) {
		Helper.logInfoMessageV2("Request Data: appFAQ V2" + requestBody);
		Helper.logInfoMessageV2("credential: appFAQ V2" + credential);
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.APP_FAQ_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.APPFAQ);
		logs.setTableType(LogsType.AppFaq);
		GetAppFaqRequest cclient = null;
		GetAppFaqResponse resp = new GetAppFaqResponse();
		try {
			cclient = Helper.JsonToObject(requestBody, GetAppFaqRequest.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
				logs.setIsB2B(cclient.getIsB2B());
			}
		} catch (Exception ex) {
			loggerV2.error("Error:", ex);
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_401);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
				Helper.logInfoMessageV2(cclient.getmsisdn() + " - Credentials " + credential);
			} catch (Exception ex) {
				loggerV2.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials == null) {
				Helper.logInfoMessageV2(cclient.getmsisdn() + " - Credentials are null");
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					Helper.logInfoMessageV2(cclient.getmsisdn() + " - Verification failed");
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				Helper.logInfoMessageV2(cclient.getmsisdn() + " - Incorrect MSISDN");
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("lang", cclient.getLang());
					// getConfigurationFromCache
					String response = RestClient.SendCallToMagento(
							ConfigurationManager.getConfigurationFromCache("magento.app.faqV2"), jsonObject.toString());
					Helper.logInfoMessageV2(cclient.getmsisdn() + " - Response from Magento: " + response);
					try {
						MagentoResponseV2 data = Helper.JsonToObject(response, MagentoResponseV2.class);
						if (data.getResultCode() == 310) {
							Helper.logInfoMessageV2(cclient.getmsisdn() + " - Success Code Recieved from magento "
									+ data.getResultCode());
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							resp.setExecTime(data.getExecTime());
							resp.setData(data.getData());
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						} else {
							Helper.logInfoMessageV2(cclient.getmsisdn() + " - Success Code Not Recieved From Magento "
									+ data.getResultCode());
							resp.setReturnCode(data.getResultCode().toString());
							resp.setReturnMsg(data.getMsg());
							resp.setExecTime(data.getExecTime());
							resp.setData(data.getData());
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
						}
					} catch (Exception ex) {
						loggerV2.error("ERROR:", ex);
						resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} catch (JSONException ex) {
					loggerV2.error("ERROR: ", ex);
					resp.setReturnCode(ResponseCodes.MAGENTO_SERVER_ERROR_CODE);
					resp.setReturnMsg(ResponseCodes.MAGENTO_SERVER_ERROR_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				} catch (Exception ex) {
					loggerV2.error("ERROR: ", ex);
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				Helper.logInfoMessageV2(cclient.getmsisdn() + " - 401 Access Not Authorized ");
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		logs.updateLog(logs);
		return resp;
	}

	@POST
	@Path("/appmenu")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetAppMenuResponse GetAppMenu(@Header("credentials") String credential, @Body() String requestBody) {
		Logs logs = new Logs();
		GetAppMenuResponse resp = new GetAppMenuResponse();
		logs.setTransactionName(Transactions.APP_MENU_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.APPMENU);
		logs.setTableType(LogsType.AppMenu);
		try {
			String transactionID = System.currentTimeMillis() + "GetAppMenu";
			logger.info("TransactionID :{" + transactionID + " }Getappmenu request LAnded:" + requestBody);
			GetAppMenuRequest cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, GetAppMenuRequest.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setLang(cclient.getLang());
					logs.setIsB2B(cclient.getIsB2B());
				}
				logger.info(transactionID + ":" + requestBody);
			} catch (Exception ex) {
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logger.info(transactionID + ":" + Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.info(transactionID + ":" + Helper.GetException(ex));
				}
				if (credentials == null) {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
						resp.setReturnMsg(verification);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					try {
						JSONObject jsonObject = new JSONObject();
						jsonObject.put("lang", cclient.getLang());
						// String response =
						// "{\"resultCode\":200,\"execTime\":0.46583795547485,\"msg\":\"from
						// cache\",\"data\":[{\"type\":\"app_ver\",\"title\":\"Notifications\",\"sortOrder\":\"1\",\"identifier\":\"notifications\"},{\"type\":\"app_hor\",\"title\":\"Dashboard\",\"sortOrder\":\"1\",\"identifier\":\"dashboard\"},{\"type\":\"app_hor\",\"title\":\"Tarrifs\",\"sortOrder\":\"2\",\"identifier\":\"tariffs\"},{\"type\":\"app_ver\",\"title\":\"Service
						// points\",\"sortOrder\":\"2\",\"identifier\":\"store_locator\"},{\"type\":\"app_hor\",\"title\":\"Services\",\"sortOrder\":\"3\",\"identifier\":\"services\"},{\"type\":\"app_ver\",\"title\":\"FAQ\",\"sortOrder\":\"3\",\"identifier\":\"faq\"},{\"type\":\"app_ver\",\"title\":\"Tutorials\",\"sortOrder\":\"4\",\"identifier\":\"tutorial_and_faqs\"},{\"type\":\"app_hor\",\"title\":\"Topup\",\"sortOrder\":\"4\",\"identifier\":\"topup\"},{\"type\":\"app_hor\",\"title\":\"My
						// Account\",\"sortOrder\":\"5\",\"identifier\":\"myaccount\"},{\"type\":\"app_ver\",\"title\":\"Ulduzum\",\"sortOrder\":\"6\",\"identifier\":\"ulduzum\"},{\"type\":\"app_ver\",\"title\":\"Roaming\",\"sortOrder\":\"6\",\"identifier\":\"roaming\"},{\"type\":\"app_ver\",\"title\":\"Contact
						// Us\",\"sortOrder\":\"7\",\"identifier\":\"contact_us\"},{\"type\":\"app_ver\",\"title\":\"Terms
						// and
						// Conditions\",\"sortOrder\":\"7\",\"identifier\":\"terms_and_conditions\"},{\"type\":\"app_ver\",\"title\":\"Live
						// Chat\",\"sortOrder\":\"7\",\"identifier\":\"live_chat\"},{\"type\":\"app_ver\",\"title\":\"Settings\",\"sortOrder\":\"8\",\"identifier\":\"settings\"},{\"type\":\"app_ver\",\"title\":\"Log
						// Out\",\"sortOrder\":\"9\",\"identifier\":\"logout\"}]}";
						String response = RestClient.SendCallToMagento(
								ConfigurationManager.getConfigurationFromCache("magento.app.menu"),
								jsonObject.toString());
						logger.info(transactionID + ":responseFromMagento" + response);
						try {

							com.evampsaanga.bakcell.getappmenu.MagentoResponse data = Helper.JsonToObject(response,
									com.evampsaanga.bakcell.getappmenu.MagentoResponse.class);
							if (data.getResultCode() == 200) {
								resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
								resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
								resp.setData(data.getData());
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							} else {
								resp.setReturnCode(data.getResultCode() + "");
								resp.setReturnMsg("");
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								resp.setData(data.getData());
							}
						} catch (Exception ex) {
							logger.info(transactionID + ":" + Helper.GetException(ex));
							resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
							resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (JSONException ex) {
						logger.info(transactionID + ":" + Helper.GetException(ex));
						resp.setReturnCode(ResponseCodes.MAGENTO_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.MAGENTO_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					} catch (Exception ex) {
						resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
						resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						logger.info(transactionID + ":" + Helper.GetException(ex));
						return resp;
					}
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	@POST
	@Path("/appmenuupdated")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetAppMenuResponseUpdated GetAppMenuUpdated(@Header("credentials") String credential,
			@Body() String requestBody) {
		Logs logs = new Logs();
		GetAppMenuResponseUpdated resp = new GetAppMenuResponseUpdated();
		logs.setTransactionName(Transactions.APP_MENU_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.APPMENU);
		logs.setTableType(LogsType.AppMenu);
		try {
			String transactionID = System.currentTimeMillis() + "GetAppMenu";
			logger.info("TransactionID :{" + transactionID + " }Getappmenu request LAnded:" + requestBody);
			GetAppMenuRequest cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, GetAppMenuRequest.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setLang(cclient.getLang());
					logs.setIsB2B(cclient.getIsB2B());
				}
				logger.info(transactionID + ":" + requestBody);
			} catch (Exception ex) {
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logger.info(transactionID + ":" + Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.info(transactionID + ":" + Helper.GetException(ex));
				}
				if (credentials == null) {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
						resp.setReturnMsg(verification);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					try {
						JSONObject jsonObject = new JSONObject();
						jsonObject.put("lang", cclient.getLang());
						// String response =
						// "{\"resultCode\":200,\"execTime\":0.46583795547485,\"msg\":\"from
						// cache\",\"data\":[{\"type\":\"app_ver\",\"title\":\"Notifications\",\"sortOrder\":\"1\",\"identifier\":\"notifications\"},{\"type\":\"app_hor\",\"title\":\"Dashboard\",\"sortOrder\":\"1\",\"identifier\":\"dashboard\"},{\"type\":\"app_hor\",\"title\":\"Tarrifs\",\"sortOrder\":\"2\",\"identifier\":\"tariffs\"},{\"type\":\"app_ver\",\"title\":\"Service
						// points\",\"sortOrder\":\"2\",\"identifier\":\"store_locator\"},{\"type\":\"app_hor\",\"title\":\"Services\",\"sortOrder\":\"3\",\"identifier\":\"services\"},{\"type\":\"app_ver\",\"title\":\"FAQ\",\"sortOrder\":\"3\",\"identifier\":\"faq\"},{\"type\":\"app_ver\",\"title\":\"Tutorials\",\"sortOrder\":\"4\",\"identifier\":\"tutorial_and_faqs\"},{\"type\":\"app_hor\",\"title\":\"Topup\",\"sortOrder\":\"4\",\"identifier\":\"topup\"},{\"type\":\"app_hor\",\"title\":\"My
						// Account\",\"sortOrder\":\"5\",\"identifier\":\"myaccount\"},{\"type\":\"app_ver\",\"title\":\"Ulduzum\",\"sortOrder\":\"6\",\"identifier\":\"ulduzum\"},{\"type\":\"app_ver\",\"title\":\"Roaming\",\"sortOrder\":\"6\",\"identifier\":\"roaming\"},{\"type\":\"app_ver\",\"title\":\"Contact
						// Us\",\"sortOrder\":\"7\",\"identifier\":\"contact_us\"},{\"type\":\"app_ver\",\"title\":\"Terms
						// and
						// Conditions\",\"sortOrder\":\"7\",\"identifier\":\"terms_and_conditions\"},{\"type\":\"app_ver\",\"title\":\"Live
						// Chat\",\"sortOrder\":\"7\",\"identifier\":\"live_chat\"},{\"type\":\"app_ver\",\"title\":\"Settings\",\"sortOrder\":\"8\",\"identifier\":\"settings\"},{\"type\":\"app_ver\",\"title\":\"Log
						// Out\",\"sortOrder\":\"9\",\"identifier\":\"logout\"}]}";
						String response = RestClient.SendCallToMagento(
								ConfigurationManager.getConfigurationFromCache("magento.app.menuupdated"),
								jsonObject.toString());
						logger.info(transactionID + ":responseFromMagento" + response);
						try {

							com.evampsaanga.bakcell.getappmenu.MagentoResponseUpdated data = Helper.JsonToObject(
									response, com.evampsaanga.bakcell.getappmenu.MagentoResponseUpdated.class);
							if (data.getResultCode() == 200) {
								resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
								resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
								resp.setData(data.getData());
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							} else {
								resp.setReturnCode(data.getResultCode() + "");
								resp.setReturnMsg("");
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								resp.setData(data.getData());
							}
						} catch (Exception ex) {
							logger.info(transactionID + ":" + Helper.GetException(ex));
							resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
							resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (JSONException ex) {
						logger.info(transactionID + ":" + Helper.GetException(ex));
						resp.setReturnCode(ResponseCodes.MAGENTO_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.MAGENTO_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					} catch (Exception ex) {
						resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
						resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						logger.info(transactionID + ":" + Helper.GetException(ex));
						return resp;
					}
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	/**
	 * 
	 * @param credential
	 * @param requestBody
	 * @return app menu
	 */
	// phase 2
	@POST
	@Path("/appmenuV2")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetAppMenuResponseV2 GetAppMenuV2(@Header("credentials") String credential, @Body() String requestBody) {
		Logs logs = new Logs();
		GetAppMenuResponseV2 resp = new GetAppMenuResponseV2();
		logs.setTransactionName(Transactions.APP_MENU_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.APPMENU);
		logs.setTableType(LogsType.AppMenu);
		try {
			String transactionID = System.currentTimeMillis() + "GetAppMenu";
			Helper.logInfoMessageV2(
					"TransactionID :{" + transactionID + " } GetappmenuV2 request LAnded:" + requestBody);
			GetAppMenuRequest cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, GetAppMenuRequest.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setLang(cclient.getLang());
					logs.setIsB2B(cclient.getIsB2B());
				}
				loggerV2.info(transactionID + ":" + requestBody);
			} catch (Exception ex) {
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				Helper.logInfoMessageV2(cclient.getmsisdn() + " - " + transactionID + ":" + Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					loggerV2.info(transactionID + ":" + Helper.GetException(ex));
				}
				if (credentials == null) {
					Helper.logInfoMessageV2(cclient.getmsisdn() + " - Credentials are null");
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						Helper.logInfoMessageV2(cclient.getmsisdn() + " - Verification Error");
						resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
						resp.setReturnMsg(verification);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} else {
					Helper.logInfoMessageV2(cclient.getmsisdn() + " - MSISDN Error");
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					try {
						JSONObject jsonObject = new JSONObject();
						jsonObject.put("lang", cclient.getLang());
						Helper.logInfoMessageV2(
								cclient.getmsisdn() + " - " + transactionID + ":request: " + jsonObject.toString());
						String response = RestClient.SendCallToMagento(
								ConfigurationManager.getConfigurationFromCache("magento.app.menuV2"),
								jsonObject.toString());
						Helper.logInfoMessageV2(
								cclient.getmsisdn() + " - " + transactionID + ":responseFromMagento" + response);
						try {
							com.evampsaanga.bakcell.getappmenu.MagentoResponseV2 data = Helper.JsonToObject(response,
									com.evampsaanga.bakcell.getappmenu.MagentoResponseV2.class);
							if (data.getResultCode() == 300) {
								resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
								resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
								resp.setData(data.getData());
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							} else {
								resp.setReturnCode(data.getResultCode() + "");
								resp.setReturnMsg("");
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								resp.setData(data.getData());
							}
						} catch (Exception ex) {
							JSONObject jobect = new JSONObject(response);

							Helper.logInfoMessageV2(
									cclient.getmsisdn() + " - " + transactionID + ":" + Helper.GetException(ex));
							resp.setReturnCode(jobect.getString("resultCode"));
							resp.setReturnMsg(jobect.getString("msg"));
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (JSONException ex) {
						Helper.logInfoMessageV2(
								cclient.getmsisdn() + " - " + transactionID + ":" + Helper.GetException(ex));
						resp.setReturnCode(ResponseCodes.MAGENTO_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.MAGENTO_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					} catch (Exception ex) {
						resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
						resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						Helper.logInfoMessageV2(
								cclient.getmsisdn() + " - " + transactionID + ":" + Helper.GetException(ex));
						return resp;
					}
				} else {
					Helper.logInfoMessageV2(cclient.getmsisdn() + " - Error 401");
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			loggerV2.error(Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	/**
	 * 
	 * @param credential
	 * @param requestBody
	 * @return
	 */
	@SuppressWarnings("null")
	// supplementary offerings for phase 2
	@POST
	@Path("/supplementaryservicesV2")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SupplementryServicesResponse SupplementryServicesV2(@Header("credentials") String credential,
			@Body() String requestBody) {
		Helper.logInfoMessageV2("Request body for SupplementryServices =" + requestBody);
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.SUPPLEMENTARY_SERVICES_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.SUPPLEMENTARY_SERVICES);
		logs.setTableType(LogsType.SupplementaryServices);
		SupplementryServicesRequestV2 cclient = null;
		SupplementryServicesResponse resp = new SupplementryServicesResponse();
		try {
			cclient = Helper.JsonToObject(requestBody, SupplementryServicesRequestV2.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
				logs.setIsB2B(cclient.getIsB2B());
			}
		} catch (Exception ex) {
			loggerV2.error("Error:", ex);
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));
			}
			if (credentials == null) {
				Helper.logInfoMessageV2(cclient.getmsisdn() + " - Credentials validations error NULL");
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					Helper.logInfoMessageV2(cclient.getmsisdn() + " - Verification Validations Error");
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				Helper.logInfoMessageV2(cclient.getmsisdn() + " - Invalid MSISDN");
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {
					JSONObject jsonObject = new JSONObject();
					// List<String> offeringIds =
					// Arrays.asList(cclient.getOfferingIds().split(","));
					jsonObject.put("lang", cclient.getLang());
					jsonObject.put("ip", cclient.getiP());
					jsonObject.put("channel", cclient.getChannel());
					jsonObject.put("msisdn", cclient.getmsisdn());
					jsonObject.put("offeringIds", cclient.getOfferingIds());
					Helper.logInfoMessageV2(
							cclient.getmsisdn() + " - Request from suplementryservices: " + jsonObject.toString());
					String response = RestClient.SendCallToMagento(
							ConfigurationManager.getConfigurationFromCache("magento.app.mySubscriptionsOfferingIdsV2"),
							jsonObject.toString());
					Helper.logInfoMessageV2(
							cclient.getmsisdn() + " - Response from mySubscriptionsOfferingIdsV2 : " + response);
					JSONObject rspMag = new JSONObject(response);
					if (rspMag.has("resultCode"))
						Helper.logInfoMessageV2(
								cclient.getmsisdn() + " - Result code-----" + rspMag.getString("resultCode"));
					if (rspMag.has("data"))
						Helper.logInfoMessageV2(
								cclient.getmsisdn() + " - Data oject---" + rspMag.getJSONObject("data"));
					try {
						// JSONObject rspMag = new JSONObject(response);
						SupplimentryMagentoResponse data = Helper.JsonToObject(response,
								SupplimentryMagentoResponse.class);
						Helper.logInfoMessageV2(
								cclient.getmsisdn() + " - Response code from MAgento=---" + data.getResultCode());
						Helper.logInfoMessageV2(
								cclient.getmsisdn() + " - Response Message from MAgento=---" + data.getMsg());
						if (data.getResultCode().equals("132")) {
							resp.setData(data.getData());
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
							logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						} else {
							resp.setReturnCode(data.getResultCode());
							resp.setReturnMsg(data.getMsg());
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception ex) {
						loggerV2.error(Helper.GetException(ex));
						resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} catch (JSONException ex) {
					loggerV2.error(Helper.GetException(ex));
					resp.setReturnCode(ResponseCodes.MAGENTO_SERVER_ERROR_CODE);
					resp.setReturnMsg(ResponseCodes.MAGENTO_SERVER_ERROR_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				} catch (Exception ex) {
					loggerV2.error(Helper.GetException(ex));
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				Helper.logInfoMessageV2(cclient.getmsisdn() + " - 401 Access Not Authorized");
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		Helper.logInfoMessageV2(cclient.getmsisdn()
				+ " - Sorry, Due To Connectivity Problems, The Last Command Could Not Be Completed");
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	/**
	 * login method for phase 2
	 * 
	 * @param requestBody
	 * @param credential
	 * @param isFromB2B
	 * @return
	 */
	// Login For Phase 2
	@POST
	@Path("/loginB2B")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoginResponse login(@Body String requestBody, @Header("credentials") String credential, String isFromB2B) {
		// Logging incoming request to log file
		Helper.logInfoMessageV2("Request lanaded on GetCustomerData with data as :" + requestBody);
		// Below is declaration block for variables to be used
		// Logs object to store values which are to be inserted in database for
		// reporting
		Logs logs = new Logs();
		// response object which is to be returned to user
		LoginResponse loginResponse = new LoginResponse();
		// Request object to store parsed data from requested string
		LoginRequest loginRequest = null;
		try {
			// Generic information about logs is being set to logs object
			logs.setRequestDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.setTransactionName(Transactions.LOGIN_TRANSACTION_NAME);
			logs.setThirdPartyName(ThirdPartyNames.LOGIN);
			logs.setTableType(LogsType.Login);
			// Authenticating the request using credentials string received in
			// header
			boolean authenticationresult = AuthorizationAndAuthentication
					.authenticateAndAuthorizeCredentials(credential);
			// if request is not authenticated adding unauthorized response
			// codes to response and logs object
			if (!authenticationresult)
				prepareErrorResponse(loginResponse, logs, ResponseCodes.ERROR_401_CODE, ResponseCodes.ERROR_401);
			// Converting JSON request to Java class using Mapper class
			try {
				loginRequest = Helper.JsonToObject(requestBody, LoginRequest.class);
			} catch (Exception ex) {
				// block catches the exception from mapper and sets the 400 bad
				// request response code
				loggerV2.error("Exception:", ex);
				// logger.error(Helper.GetException(ex));
				prepareErrorResponse(loginResponse, logs, ResponseCodes.ERROR_400_CODE, ResponseCodes.ERROR_400);
			}
			// if requested string is converted to Java class then processing of
			// request goes below
			if (loginRequest != null) {
				// block sets the mandatory parameters to logs object which are
				// taken from request body
				logs.setIp(loginRequest.getiP());
				logs.setChannel(loginRequest.getChannel());
				logs.setMsisdn(loginRequest.getmsisdn());
				logs.setIsB2B(loginRequest.getIsB2B());
				// if authentication is successful request forwarded for
				// validation of parameters
				if (authenticationresult) {
					ValidationResult validationResult = validateRequest(loginRequest);
					// if validation is performed successfully request is sent
					// to NGBSS to get customer data
					if (validationResult.isValidationResult()) {

						loginResponse = getUserAuthenticated(loginRequest, isFromB2B);

						logs.setResponseCode(loginResponse.getReturnCode());
						logs.setResponseDescription(loginResponse.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
					} else
						// if request parameters fails validation response is
						// prepared with validation error
						prepareErrorResponse(loginResponse, logs, validationResult.getValidationCode(),
								validationResult.getValidationMessage());
				}
				// Helper.logInfoMessageV2(loginRequest.getmsisdn() + " -
				// Returning response from GetCustomerData",
				// Helper.ObjectToJson(loginResponse));
			}
		} catch (Exception ex) {
			// block catches any unhandled exception and returns generic error
			// message to client
			loggerV2.error(loginRequest.getmsisdn() + " " + Helper.GetException(ex));
			prepareErrorResponse(loginResponse, logs, ResponseCodes.GENERIC_ERROR_CODE,
					ResponseCodes.GENERIC_ERROR_DES);
		}
		// insert the log object into database for reporting purpose
		// logs.insertLog(logs);
		// all processing done returning response to client
		// Helper.logInfoMessage(loginRequest.getmsisdn(), "Returning response
		// from GetCustomerData", Helper.ObjectToJson(loginResponse));
		logs.updateLog(logs);
		return loginResponse;
	}

	/**
	 * getting user authencated from magento
	 * 
	 * @param loginRequest
	 * @param isFromB2B
	 * @return
	 * @throws Exception
	 */
	public LoginResponse getUserAuthenticated(LoginRequest loginRequest, String isFromB2B) throws Exception {
		com.evampsaanga.models.LoginResponse loginResponse = new com.evampsaanga.models.LoginResponse();
		JSONObject jsonObject = new JSONObject();
		JSONObject predefinedDataRequest = new JSONObject();

		predefinedDataRequest.put("channel", loginRequest.getChannel());
		predefinedDataRequest.put("lang", loginRequest.getLang());
		predefinedDataRequest.put("ip", loginRequest.getiP());
		predefinedDataRequest.put("msisdn", loginRequest.getmsisdn());

		jsonObject.put("username", loginRequest.getmsisdn());
		jsonObject.put("password", loginRequest.getPassword());
		String response = RestClient.SendCallToMagento(
				ConfigurationManager.getConfigurationFromCache("magento.app.loginb2b"), jsonObject.toString());
		JSONObject jsonResponse = new JSONObject(response);
		Helper.logInfoMessageV2(loginRequest.getmsisdn() + " - Login Response From Magento: " + jsonResponse);
		if (jsonResponse.get("resultCode").toString().equalsIgnoreCase("242")) {
			LoginData loginData = Helper.JsonToObject(jsonResponse.getJSONObject("data").toString(), LoginData.class);
			// String imageUrl =
			/*
			 * if (loginData != null && (loginData.getCustom_profile_image() ==
			 * null || loginData.getCustom_profile_image().equals(""))) {
			 * loginData.setCustom_profile_image(
			 * ConfigurationManager.getConfigurationFromCache(
			 * "profileImageDownloadLink") + loginRequest.getmsisdn() + ".jpg");
			 * }
			 */
			loginData.setCustom_profile_image(loginRequest.getmsisdn() + ".jpg");
			response = RestClient.SendCallToMagento(
					ConfigurationManager.getConfigurationFromCache("magento.app.getpredefinedV2"),
					predefinedDataRequest.toString());
			Helper.logInfoMessageV2(
					loginRequest.getmsisdn() + " - Predefined Data Response From Maagento: " + response);
			JSONObject predeinfedData = new JSONObject(response);
			GetPredefinedDataResponseData predeinfedresp = Helper
					.JsonToObject(predeinfedData.getJSONObject("data").toString(), GetPredefinedDataResponseData.class);
			predeinfedresp.setPopupTitle(
					ConfigurationManager.getConfigurationFromCache("predefined.popuptitle." + loginRequest.getLang()));
			predeinfedresp.setPopupContent(ConfigurationManager
					.getConfigurationFromCache("predefined.popupcontent." + loginRequest.getLang()));
			predeinfedresp.setFirstPopup(ConfigurationManager.getConfigurationFromCache("predefined.firstpopup"));
			predeinfedresp.setLateOnPopup(ConfigurationManager.getConfigurationFromCache("predefined.lateronpopup"));
			predeinfedresp.setDocumentTypes(getDocumentTypesList(loginRequest));
			loginResponse.setReturnCode(ResponseCodes.SUCESS_CODE_200);
			loginResponse.setReturnMsg(jsonResponse.getString("msg"));
			loginResponse.setLoginData(loginData);
			loginResponse.setPredefinedData(predeinfedresp);
			return loginResponse;
		}
		loginResponse.setReturnCode(jsonResponse.getString("resultCode"));
		loginResponse.setReturnMsg(jsonResponse.getString("msg"));

		return loginResponse;
	}

	@POST
	@Path("/login")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public com.evampsaanga.bakcell.login.LoginResponse GetLogIn(@Header("credentials") String credential,
			@Body() String requestBody) {
		logger.info("Login  Customer Request" + requestBody);
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.LOGIN_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.LOGIN);
		logs.setTableType(LogsType.Login);
		com.evampsaanga.bakcell.login.LoginRequest cclient = null;
		com.evampsaanga.bakcell.login.LoginResponse resp = new com.evampsaanga.bakcell.login.LoginResponse();
		try {
			cclient = Helper.JsonToObject(requestBody, com.evampsaanga.bakcell.login.LoginRequest.class);
			logs.setIp(cclient.getiP());
			logs.setChannel(cclient.getChannel());
			logs.setMsisdn(cclient.getmsisdn());
			logs.setLang(cclient.getLang());
			logs.setIsB2B(cclient.getIsB2B());
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		{
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {
					GetCustomerDataLand getCustomer = new GetCustomerDataLand();
					GetCustomerRequestClient getCustomerRequestClient = new GetCustomerRequestClient();
					getCustomerRequestClient.setChannel(cclient.getChannel());
					getCustomerRequestClient.setiP(cclient.getiP());
					getCustomerRequestClient.setLang(cclient.getLang());
					getCustomerRequestClient.setMsisdn(cclient.getmsisdn());
					ObjectMapper mapper = new ObjectMapper();
					String jsonInString = mapper.writeValueAsString(getCustomerRequestClient);
					GetCustomerRequestResponse responseGetCustomer = getCustomer.Get(Constants.CREDENTIALSUNCODED, "",
							jsonInString);
					if (responseGetCustomer.getReturnCode().equals(ResponseCodes.SUCESS_CODE_200)) {
						if (responseGetCustomer.getCustomerData().getStatus().equals("Active")
								|| responseGetCustomer.getCustomerData().getStatus().equals("Block 1 Way")
								|| responseGetCustomer.getCustomerData().getStatus().equals("Block 2 Way")) {
						} else {
							resp.setReturnCode("551");
							resp.setReturnMsg("The number is in " + responseGetCustomer.getCustomerData().getStatus()
									+ " state, you are not allowed to login");
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} else {
						resp.setReturnCode(responseGetCustomer.getReturnCode());
						resp.setReturnMsg(responseGetCustomer.getReturnMsg());
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
					if (responseGetCustomer.getReturnCode().equals(ResponseCodes.SUCESS_CODE_200)) {
						JSONObject jsonObject = new JSONObject();
						jsonObject.put("msisdn", cclient.getmsisdn());
						jsonObject.put("password", cclient.getPassword());
						jsonObject.put("accountId", responseGetCustomer.getCustomerData().getAccountId());
						jsonObject.put("customerId", responseGetCustomer.getCustomerData().getCustomerId());
						logger.info("accountId" + responseGetCustomer.getCustomerData().getAccountId());
						logger.info("Request to magento for log in" + jsonObject);
						// String response = RestClient.SendCallToMagento(
						// ConfigurationManager.getConfigurationFromCache("magento.app.signupflow.login"),
						// jsonObject.toString()); //old one please dont delete
						// String response =
						// RestClient.SendCallToMagento("http://ecare-webvip-prodint.bakcell.com/rest/api/customer.php",jsonObject.toString());
						// logger.info("Response from magento for Login: " +
						// response);
						try {
							// com.evampsaanga.bakcell.login.MagentoResponse
							// data = Helper.JsonToObject(response,
							// com.evampsaanga.bakcell.login.MagentoResponse.class);
							// //dont delete

							//
							boolean checkDb = false;

							if (BuildCacheRequestLand.customerCache == null)
								BuildCacheRequestLand.initHazelcast();
							if (!BuildCacheRequestLand.customerCache.containsKey(cclient.getmsisdn())) {
								checkDb = Helper.checkMsisdnExistInDB(cclient.getmsisdn());
							} else {
								checkDb = true;
							}

							if (checkDb) {

								CustomerModelCache customerModelCache = BuildCacheRequestLand.customerCache
										.get(cclient.getmsisdn());

								int result = Long.compare(
										Long.valueOf(BuildCacheRequestLand.customerCache.get(cclient.getmsisdn())
												.getCustomerId()),
										Long.valueOf(responseGetCustomer.getCustomerData().getCustomerId()));
								if (result == 0) {
									logger.info("LOGIN:" + cclient.getmsisdn() + "Customer id is matched");
									// ------------- checkLoginAttempts checks
									// the login attempts from custom php API
									// ------------//
									if (checkLoginAttempts(cclient.getmsisdn())) {
										loggerV2.info("LOGIN:" + cclient.getmsisdn()
												+ " check login attempt in if check true ");
										if (Helper.checkSHApass(cclient.getPassword(), cclient.getmsisdn())
												.equals(customerModelCache.getPassword_hash())) {

											loggerV2.info("LOGIN:" + cclient.getmsisdn()
													+ " checkSSHPASS compare passwords ");
											logger.info("LOGIN:" + cclient.getmsisdn() + "Email coming"
													+ customerModelCache.getEmail());

											logger.info("LOGIN:" + cclient.getmsisdn() + "EmailFrom Cache :"
													+ customerModelCache.getEmail());

											resetLoginAttempt(cclient.getmsisdn());
											resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
											resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
											resp.setCustomerData(responseGetCustomer.getCustomerData());
											// resp.getCustomerData().setEmail(data.getData().getEmail());
											// resp.getCustomerData().setEntityId(data.getData().getEntityId());
											// //dont delete

											resp.getCustomerData().setEmail(customerModelCache.getEmail());
											resp.getCustomerData().setEntityId(customerModelCache.getEntity_id());

											resp.getCustomerData()
													.setPopupTitle(ConfigurationManager.getConfigurationFromCache(
															"predefined.popuptitle." + cclient.getLang()));
											resp.getCustomerData()
													.setPopupContent(ConfigurationManager.getConfigurationFromCache(
															"predefined.popupcontent." + cclient.getLang()));
											resp.getCustomerData().setFirstPopup(ConfigurationManager
													.getConfigurationFromCache("predefined.firstpopup"));
											resp.getCustomerData().setLateOnPopup(ConfigurationManager
													.getConfigurationFromCache("predefined.lateronpopup"));

											logger.info("LOGIN:" + cclient.getmsisdn() + "Data Sent To App "
													+ resp.toString());
											logs.setResponseCode(resp.getReturnCode());
											logs.setResponseDescription(resp.getReturnMsg());
											logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
											logs.updateLog(logs);
											return resp;
										} else {
											loggerV2.info("LOGIN:" + cclient.getmsisdn() + " "
													+ ResponseCodes.PASSWORD_MATCH_FAILED_DESCRIPTION);
											resp.setReturnCode(ResponseCodes.PASSWORD_MATCH_FAILED_CODE);
											resp.setReturnMsg(ResponseCodes.PASSWORD_MATCH_FAILED_DESCRIPTION);
											logs.setResponseCode(resp.getReturnCode());
											logs.setResponseDescription(resp.getReturnMsg());
											logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
											logs.updateLog(logs);
											return resp;
										}
									} else {
										loggerV2.info("LOGIN:" + cclient.getmsisdn() + " login attempt failed");
										resp.setReturnCode(ResponseCodes.LOGIN_ATTEMPT_FAILED);
										resp.setReturnMsg(ResponseCodes.LOGIN_ATTEMPT_FAILED_DESCRIPTION);
										logs.setResponseCode(resp.getReturnCode());
										logs.setResponseDescription(resp.getReturnMsg());
										logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
										logs.updateLog(logs);
										return resp;
									}
								} else {
									// user not exisit
									loggerV2.info("LOGIN:" + cclient.getmsisdn()
											+ " Deleting Customer due to ownership change");

									deleteCustomer(cclient.getmsisdn());
									BuildCacheRequestLand.customerCache.remove(cclient.getmsisdn());

									resp.setReturnCode(ResponseCodes.USER_DOESNOT_EXIST_CODE);
									resp.setReturnMsg(ResponseCodes.USER_DOESNOT_EXIST_DESC);
									logs.setResponseCode(resp.getReturnCode());
									logs.setResponseDescription(resp.getReturnMsg());
									logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
									logs.updateLog(logs);
									return resp;
								}

							}

							else {
								loggerV2.info("LOGIN:" + cclient.getmsisdn() + "-"
										+ ResponseCodes.USER_NOT_FOUND_DESCRIPTION);
								resp.setReturnCode(ResponseCodes.USER_NOT_FOUND_CODE);
								resp.setReturnMsg(ResponseCodes.USER_NOT_FOUND_DESCRIPTION);
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							}

							// ArrayList<String> data =
							// checkLogin(cclient.getPassword(),
							// cclient.getmsisdn());
							// if(data.size()>0)
							// {
							//
							//// logger.info("Email coming" +
							// jsObject.getJSONObject("data").getJSONObject("data").getString("email"));
							// resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							// resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							// resp.setCustomerData(responseGetCustomer.getCustomerData());
							//// resp.getCustomerData().setEmail(data.getData().getEmail());
							//// resp.getCustomerData().setEntityId(data.getData().getEntityId());
							// //dont delete
							//
							// resp.getCustomerData().setEmail(data.get(1));
							// resp.getCustomerData().setEntityId(data.get(0));
							//
							// resp.getCustomerData().setPopupTitle(ConfigurationManager
							// .getConfigurationFromCache("predefined.popuptitle."
							// + cclient.getLang()));
							// resp.getCustomerData().setPopupContent(ConfigurationManager
							// .getConfigurationFromCache("predefined.popupcontent."
							// + cclient.getLang()));
							// resp.getCustomerData().setFirstPopup(
							// ConfigurationManager.getConfigurationFromCache("predefined.firstpopup"));
							// resp.getCustomerData().setLateOnPopup(
							// ConfigurationManager.getConfigurationFromCache("predefined.lateronpopup"));
							//
							// logger.info("Data Sent To App " +
							// resp.toString());
							// logs.setResponseCode(resp.getReturnCode());
							// logs.setResponseDescription(resp.getReturnMsg());
							// logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							// logs.updateLog(logs);
							// return resp;
							// }
							// else {
							// resp.setReturnCode("100");
							// resp.setReturnMsg("Authentication failed");
							// logs.setResponseCode(resp.getReturnCode());
							// logs.setResponseDescription(resp.getReturnMsg());
							// logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							// logs.updateLog(logs);
							// return resp;
							// }

							// JSONObject jsObject = new JSONObject(response);
							// logger.info("Result code " + response);
							//// if (data.getResultCode().equals("42")) {
							// if
							// (jsObject.getString("resultCode").equals("42")) {
							// logger.info("Email coming" +
							// jsObject.getJSONObject("data").getJSONObject("data").getString("email"));
							// resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							// resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							// resp.setCustomerData(responseGetCustomer.getCustomerData());
							//// resp.getCustomerData().setEmail(data.getData().getEmail());
							//// resp.getCustomerData().setEntityId(data.getData().getEntityId());
							// //dont delete
							//
							// resp.getCustomerData().setEmail(jsObject.getJSONObject("data").getJSONObject("data").getString("email"));
							// resp.getCustomerData().setEntityId(jsObject.getJSONObject("data").getJSONObject("data").getString("entity_id"));
							//
							// resp.getCustomerData().setPopupTitle(ConfigurationManager
							// .getConfigurationFromCache("predefined.popuptitle."
							// + cclient.getLang()));
							// resp.getCustomerData().setPopupContent(ConfigurationManager
							// .getConfigurationFromCache("predefined.popupcontent."
							// + cclient.getLang()));
							// resp.getCustomerData().setFirstPopup(
							// ConfigurationManager.getConfigurationFromCache("predefined.firstpopup"));
							// resp.getCustomerData().setLateOnPopup(
							// ConfigurationManager.getConfigurationFromCache("predefined.lateronpopup"));
							//
							// logger.info("Data Sent To App " +
							// resp.toString());
							// logs.setResponseCode(resp.getReturnCode());
							// logs.setResponseDescription(resp.getReturnMsg());
							// logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							// logs.updateLog(logs);
							// return resp;
							// } else {
							// resp.setReturnCode(jsObject.getString("resultCode"));
							// resp.setReturnMsg(jsObject.getString("msg"));
							// logs.setResponseCode(resp.getReturnCode());
							// logs.setResponseDescription(resp.getReturnMsg());
							// logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							// logs.updateLog(logs);
							// return resp;
							// }
						} catch (Exception ex) {
							logger.error(Helper.GetException(ex));
							resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
							resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} else {
						resp.setReturnCode(responseGetCustomer.getReturnCode());
						resp.setReturnMsg(responseGetCustomer.getReturnMsg());
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} catch (JSONException ex) {
					logger.error(Helper.GetException(ex));
					resp.setReturnCode(ResponseCodes.MAGENTO_SERVER_ERROR_CODE);
					resp.setReturnMsg(ResponseCodes.MAGENTO_SERVER_ERROR_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				} catch (Exception ex) {
					logger.error(Helper.GetException(ex));
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.updateLog(logs);
				return resp;
			}
		}
	}

	private boolean resetLoginAttempt(String getmsisdn) throws Exception {
		// TODO Auto-generated method stub
		loggerV2.info("LOGIN:" + getmsisdn + " reset login attempt");

		JSONObject loginAttemptRequest = new JSONObject();
		loginAttemptRequest.put("msisdn", getmsisdn);
		loginAttemptRequest.put("type", "resetLoginAttempts");
		loggerV2.info("LOGIN:" + getmsisdn + " check login attempt request " + loginAttemptRequest.toString());
		String loginAttemp = RestClient.SendCallToMagento(
				ConfigurationManager.getConfigurationFromCache("magento.app.login.validateLoginAttempts"),
				loginAttemptRequest.toString());
		loggerV2.info("LOGIN:" + getmsisdn + " check login  reset attempt response" + loginAttemp);
		JSONObject loginAttemmptResponse = new JSONObject(loginAttemp);
		if (loginAttemmptResponse.getString("resultCode").equalsIgnoreCase("200")) {
			return true;
		} else {
			return false;
		}

	}

	private boolean checkLoginAttempts(String getmsisdn) throws Exception {
		// check login attempts

		loggerV2.info("LOGIN:" + getmsisdn + " check login attempt");

		JSONObject loginAttemptRequest = new JSONObject();
		loginAttemptRequest.put("msisdn", getmsisdn);
		loginAttemptRequest.put("type", "validateLoginAttempts");
		loggerV2.info("LOGIN:" + getmsisdn + " check login attempt request " + loginAttemptRequest.toString());
		String loginAttemp = RestClient.SendCallToMagento(
				ConfigurationManager.getConfigurationFromCache("magento.app.login.validateLoginAttempts"),
				loginAttemptRequest.toString());
		loggerV2.info("LOGIN:" + getmsisdn + " check login attempt response" + loginAttemp);
		JSONObject loginAttemmptResponse = new JSONObject(loginAttemp);
		if (loginAttemmptResponse.getString("resultCode").equalsIgnoreCase("200")) {
			loggerV2.info("LOGIN:" + getmsisdn + " returning true" + loginAttemmptResponse.getString("resultCode"));
			return true;
		} else {
			loggerV2.info("LOGIN:" + getmsisdn + " returning false");
			return false;
		}
	}

	/**
	 * 
	 * @param response
	 * @return
	 * @throws JSONException
	 */
	public static String stringToJSONString(String response) throws JSONException {
		JSONObject obj = new JSONObject(response.substring(1, response.length() - 1).replace("\\", ""));
		return response = obj.toString();
	}

	@POST
	@Path("/savecustomer")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SaveCustomerResponse SaveCustomer(@Header("credentials") String credential, @Body() String requestBody) {
		SaveCustomerResponse resp = new SaveCustomerResponse();
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.SAVE_CUSTOMER_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.SAVE_CUSTOMER);
		logs.setTableType(LogsType.SaveCustomer);
		try {
			logger.info("Request Save Customer" + requestBody);
			SaveCustomerRequest cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, SaveCustomerRequest.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setLang(cclient.getLang());
					logs.setUserType(cclient.getCustomerData().getSubscriberType());
					logs.setIsB2B(cclient.getIsB2B());
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
						resp.setReturnMsg(verification);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					try {
						JSONObject jsonObjectCustomer = new JSONObject();

						jsonObjectCustomer.put("password", cclient.getPassword());// done
						jsonObjectCustomer.put("confirm_password", cclient.getConfirmPassword());// done
						jsonObjectCustomer.put("terms_and_conditions", cclient.getTermsAndConditions());// done
						jsonObjectCustomer.put("temp", cclient.getTemp());// done

						jsonObjectCustomer.put("channel", cclient.getChannel());// done
						jsonObjectCustomer.put("title", cclient.getCustomerData().getTitle());// done
						jsonObjectCustomer.put("firstName", cclient.getCustomerData().getFirstName());// done
						jsonObjectCustomer.put("middleName", cclient.getCustomerData().getMiddleName());// done
						jsonObjectCustomer.put("lastName", cclient.getCustomerData().getLastName());// done
						jsonObjectCustomer.put("customer_type", cclient.getCustomerData().getCustomerType());// done
						jsonObjectCustomer.put("customer_gender", cclient.getCustomerData().getGender());// done
						jsonObjectCustomer.put("dob", cclient.getCustomerData().getDob());// done
						jsonObjectCustomer.put("account_id", cclient.getCustomerData().getAccountId());// done
						jsonObjectCustomer.put("billingLanguage", cclient.getCustomerData().getBillingLanguage());// done
						jsonObjectCustomer.put("language", ConfigurationManager.getConfigurationFromCache(
								ConfigurationManager.ULDUZUM_LANGUAGE_MAPPING + cclient.getLang()));//// done
																									//// cclient.getCustomerData().getLanguage());
						jsonObjectCustomer.put("effective_date", cclient.getCustomerData().getEffectiveDate());// done
						jsonObjectCustomer.put("expiry_date", cclient.getCustomerData().getExpiryDate());// done
						jsonObjectCustomer.put("subscriber_type", cclient.getCustomerData().getSubscriberType());// done
						jsonObjectCustomer.put("ngbss_status", cclient.getCustomerData().getStatus());// done
						jsonObjectCustomer.put("statusDetails", cclient.getCustomerData().getStatusDetails());
						jsonObjectCustomer.put("brand_id", cclient.getCustomerData().getBrandId());// done
						jsonObjectCustomer.put("loyaltySegment", cclient.getCustomerData().getLoyaltySegment());// done
						jsonObjectCustomer.put("offering_id", cclient.getCustomerData().getOfferingId());// done
						jsonObjectCustomer.put("msisdn", cclient.getCustomerData().getMsisdn());// done
						jsonObjectCustomer.put("offering_name", cclient.getCustomerData().getOfferingName());// done
						jsonObjectCustomer.put("brand_name", cclient.getCustomerData().getBrandName());// done
						jsonObjectCustomer.put("customer_id", cclient.getCustomerData().getCustomerId());// done
						jsonObjectCustomer.put("rateus_android", "0");
						jsonObjectCustomer.put("rateus_ios", "0");

						JSONObject jsonObject = new JSONObject();
						jsonObject.put("type", "saveCustomer");
						jsonObject.put("customerData", jsonObjectCustomer);

						logger.info("Request Save Customer magento:" + jsonObject.toString());
						logs.setRequestDateTime(Helper.GenerateDateTimeToMsAccuracy());
						String saveCustomerMagentoResponse = RestClient.SendCallToMagento(
								ConfigurationManager.getConfigurationFromCache("magento.app.signupflow.savecustomer"),
								jsonObject.toString());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logger.info("Response from Save Customer magento: " + saveCustomerMagentoResponse);
						SOAPLoggingHandler.logger
								.debug("Response from Save Customer magento: " + saveCustomerMagentoResponse);
						try {
							com.evampsaanga.bakcell.savecustomer.MagentoResponse dataFromVerifyOTP = Helper
									.JsonToObject(saveCustomerMagentoResponse,
											com.evampsaanga.bakcell.savecustomer.MagentoResponse.class);
							logger.info("Result code " + dataFromVerifyOTP.getResultCode());
							logs.setResponseDateTime(dataFromVerifyOTP.getResultCode());
							if (dataFromVerifyOTP.getResultCode().equals("32")) {

								CustomerModelCache customerModelCache = new CustomerModelCache();
								customerModelCache.setEntity_id(dataFromVerifyOTP.getEntityId());
								customerModelCache.setMsisdn(cclient.getmsisdn());
								customerModelCache.setEmail(cclient.getmsisdn() + "@bakcell.com");
								customerModelCache.setPassword_hash(
										Helper.checkSHApass(cclient.getPassword(), cclient.getmsisdn()));
								customerModelCache.setIsFromDB("false");
								customerModelCache.setCustomerId(cclient.getCustomerData().getCustomerId());
								BuildCacheRequestLand.customerCache.put(cclient.getmsisdn(), customerModelCache);
								MessageTemplateResponse messageTemplateResponse = ConfigurationManager
										.getMessageTemplateFromCache(Constants.GET_MESSAGE_TEMPLATE_KEY);

								// sending promo message
								logger.info("SAVE_CUSTOMER:Sending promo message");
								if (messageTemplateResponse.getData().getIsPromoEnabled().equals("1")) {
									logger.info("SAVE_CUSTOMER:Promo message is enabled");
									JSONObject requestPacket = new JSONObject();
									requestPacket.put("msisdn", cclient.getmsisdn());
									requestPacket.put("iP", cclient.getiP());
									requestPacket.put("channel", cclient.getChannel());
									requestPacket.put("lang", cclient.getLang());
									requestPacket.put("actionType", "1");
									requestPacket.put("offeringId", messageTemplateResponse.getData().getOfferingIds());
									String credentialsChangeSupp = "RQ/Rjcf8Gn2+7srAiXNO0ZGuRnJORSir";
									ChangeSupplimentryOfferingSignupRequestLand changeSupplimentryOfferingSignupRequestLand = new ChangeSupplimentryOfferingSignupRequestLand();
									Response response = changeSupplimentryOfferingSignupRequestLand
											.Get(credentialsChangeSupp, "", requestPacket.toString());
									// SubmitOrderResponse response =
									// ChangeSupplimentryOfferingSignupRequestLand.changeSupplementaryOffers("1",
									// messageTemplateResponse.getData().getOfferingIds(),
									// cclient.getmsisdn());
									if (response.getReturnCode().equals("200")) {

										if (cclient.getLang().equals("2")) {
											// ru
											logger.info("SAVE_CUSTOMER:Sending Promo messge in RU "
													+ messageTemplateResponse.getData().getPromoMsgRU());
											resp.setPromoMessage(messageTemplateResponse.getData().getPromoMsgRU());
										} else if (cclient.getLang().equals("4")) {
											// az
											logger.info("SAVE_CUSTOMER:Sending Promo messge in AZ "
													+ messageTemplateResponse.getData().getPromoMsgAZ());
											resp.setPromoMessage(messageTemplateResponse.getData().getPromoMsgAZ());
										} else {
											logger.info("SAVE_CUSTOMER:Sending Promo messge in EN "
													+ messageTemplateResponse.getData().getPromoMsgEN());
											resp.setPromoMessage(messageTemplateResponse.getData().getPromoMsgEN());
										}
									} else {
										logger.info("SAVE_CUSTOMER:Submit Order failed");
										resp.setPromoMessage("");
									}
								} else {
									logger.info("SAVE_CUSTOMER:Promo message is diabled");
									resp.setPromoMessage("");
								}

								resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
								resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
								resp.setEntityId(dataFromVerifyOTP.getEntityId());
								// resp.setPromoMessage(dataFromVerifyOTP.getData());
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							} else {
								resp.setReturnCode(dataFromVerifyOTP.getResultCode());
								resp.setReturnMsg(dataFromVerifyOTP.getMsg());
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							}
						} catch (Exception ex) {
							resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
							resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (JSONException ex) {
						resp.setReturnCode(ResponseCodes.MAGENTO_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.MAGENTO_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					} catch (Exception ex) {
						resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
						resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
		}
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	@POST
	@Path("/sendsms")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SendSMSResponse SendSMS(@Header("credentials") String credential, @Body() String requestBody)
			throws Exception {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.GET_SENDSMS_MAGENTO_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.SEND_SMS_MAGENTO);
		logs.setTableType(LogsType.SendSMSMagento);
		SendSMSRequest cclient = null;
		SendSMSResponse resp = new SendSMSResponse();
		try {
			cclient = Helper.JsonToObject(requestBody, SendSMSRequest.class);
			if (cclient.getIsB2B() != null && cclient.getIsB2B().equals("true")) {
				logs.setTransactionName(Transactions.GET_SENDSMS_MAGENTO_TRANSACTION_NAME_B2B);
			}
			logs.setIp(cclient.getiP());
			logs.setChannel(cclient.getChannel());
			logs.setMsisdn(cclient.getSender());
			logs.setLang(cclient.getLang());
			logs.setReceiverMsisdn(cclient.getmsisdn());
			logs.setIsB2B(cclient.getIsB2B());
		} catch (Exception ex) {
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {
					SendSMCService service = new SendSMCService();
					if (service.sendSMS(cclient.getmsisdn(), cclient.getTextmsg(), cclient.getChannel(),
							cclient.getLang(), cclient.getiP(), null)) {
						resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					} else {
						resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
						resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					}
				} catch (Exception ex) {
					Helper.GetException(ex);
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		logs.updateLog(logs);
		return resp;
	}

	/**
	 * returns otp response
	 * 
	 * @param credential
	 * @param requestBody
	 * @return
	 */
	// for phase 2
	@POST
	@Path("/sendotp")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SendOTPResponse Sendotp(@Header("credentials") String credential, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.RESEND_OTP_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.RESEND_OTP);
		logs.setTableType(LogsType.ResendOtp);
		SendOTPRequest cclient = null;
		SendOTPResponse resp = new SendOTPResponse();
		try {
			Helper.logInfoMessageV2("Request body SendOtp V2" + requestBody);
			try {
				cclient = Helper.JsonToObject(requestBody, SendOTPRequest.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setLang(cclient.getLang());
					logs.setIsB2B(cclient.getIsB2B());
				}
			} catch (Exception ex) {
				loggerV2.error("ERROR in converting jsonToObj", ex);
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					loggerV2.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					Helper.logInfoMessageV2(cclient.getmsisdn() + " - 401 Credentials Are Null");
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					Helper.logInfoMessageV2(cclient.getmsisdn() + " - Credentails are :" + credentials);
					/*
					 * String verification = Helper.validateRequest(cclient); if
					 * (!verification.equals("")) {
					 * resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					 * resp.setReturnMsg(verification); return resp; }
					 */
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					// return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					// return RequestSoap( cclient);
					try {
						JSONObject jsonObject = new JSONObject();
						jsonObject.put("storeId", cclient.getLang());
						jsonObject.put("username", cclient.getUserName());
						jsonObject.put("requestPlatform", "usagehistory-details-view-" + cclient.getChannel());
						Helper.logInfoMessageV2(
								cclient.getmsisdn() + "Request Packet from send pin otp: " + jsonObject.toString());
						String response = RestClient.SendCallToMagento(
								ConfigurationManager.getConfigurationFromCache("magento.app.sendotpflow.sendOtp"),
								jsonObject.toString());
						Helper.logInfoMessageV2(cclient.getmsisdn() + "Response from send pin otp: " + response);
						try {
							MagentoResponseValidateMsisdnV2 data = Helper.JsonToObject(response,
									MagentoResponseValidateMsisdnV2.class);
							logger.info("Result code " + data.getResultCode());
							if (data.getResultCode().equals("232")) {

								resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
								resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
								// TODO
								// need to remove the msg from the responses
								resp.setResponseMsg(data.getMsg());
								resp.setChannel(data.getChannel());
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							} else {
								resp.setReturnCode(data.getResultCode());
								resp.setReturnMsg(data.getMsg());
								resp.setResponseMsg(data.getMsg());
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							}
						} catch (Exception ex) {
							loggerV2.error("Error:", ex);
							resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
							resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (JSONException ex) {
						loggerV2.error("Error:", ex);
						resp.setReturnCode(ResponseCodes.MAGENTO_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.MAGENTO_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					} catch (Exception ex) {
						loggerV2.error("Error:", ex);
						resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
						resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} else {
					Helper.logInfoMessageV2(cclient.getmsisdn() + " - Access Not Authorized");
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			loggerV2.error("Error:", ex);
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		logs.updateLog(logs);
		return resp;
	}

	/**
	 * resends otp
	 * 
	 * @param credential
	 * @param requestBody
	 * @return
	 */
	// Resend otp for phase 2
	@POST
	@Path("/resendOTPV2")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SendOTPResponse ReSendotpV2(@Header("credentials") String credential, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.RESEND_OTP_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.RESEND_OTP);
		logs.setTableType(LogsType.ResendOtp);
		ResendOTPRequest cclient = null;
		SendOTPResponse resp = new SendOTPResponse();
		try {
			Helper.logInfoMessageV2("Request body resendotp V2" + requestBody);
			try {
				cclient = Helper.JsonToObject(requestBody, ResendOTPRequest.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setLang(cclient.getLang());
					logs.setIsB2B(cclient.getIsB2B());
				}
			} catch (Exception ex) {
				loggerV2.error("Error: ", ex);
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					loggerV2.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					Helper.logInfoMessageV2(cclient.getmsisdn() + " - Credentials Are Null");
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						Helper.logInfoMessageV2(cclient.getmsisdn() + " - Verification Error");
						resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
						resp.setReturnMsg(verification);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} else {
					Helper.logInfoMessageV2(cclient.getmsisdn() + " - Not a Valid Msisdn Number");
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					// return RequestSoap( cclient);
					try {
						JSONObject jsonObject = new JSONObject();
						jsonObject.put("storeId", cclient.getLang());
						jsonObject.put("username", cclient.getUserName());
						jsonObject.put("requestPlatform", cclient.getCause() + "-" + cclient.getChannel());
						// jsonObject.put("customerId",
						// respns.getGetSubscriberBody().getCustomerId() + "");
						String response = RestClient.SendCallToMagento(ConfigurationManager.getConfigurationFromCache(
								"magento.app.resendOTPflowV2.resendOtpV2"), jsonObject.toString());
						Helper.logInfoMessageV2(cclient.getmsisdn() + " - Response from resend otp pin: " + response);
						try {
							MagentoResponseValidateMsisdn data = Helper.JsonToObject(response,
									MagentoResponseValidateMsisdn.class);
							Helper.logInfoMessageV2(cclient.getmsisdn() + " - Result code " + data.getResultCode());
							if (data.getResultCode().equals("264")) {
								resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
								resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
								resp.setResponseMsg(data.getMsg());
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							} else {
								resp.setReturnCode(data.getResultCode());
								resp.setReturnMsg(data.getMsg());
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							}
						} catch (Exception ex) {
							loggerV2.error("Error: ", ex);
							resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
							resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (JSONException ex) {
						loggerV2.error("Error: ", ex);
						resp.setReturnCode(ResponseCodes.MAGENTO_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.MAGENTO_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					} catch (Exception ex) {
						loggerV2.error("Error: ", ex);
						resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
						resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} else {
					Helper.logInfoMessageV2(cclient.getmsisdn() + " - Access Not Authorized");
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			loggerV2.error("Error: ", ex);
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		logs.updateLog(logs);
		return resp;
	}

	@POST
	@Path("/signupresendotp")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SignUpResponse SignUpReSendotp(@Header("credentials") String credential, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.RESEND_OTP_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.RESEND_OTP);
		logs.setTableType(LogsType.ResendOtp);
		SignUpRequest cclient = null;
		SignUpResponse resp = new SignUpResponse();
		try {
			logger.info("Request body signupresendotp" + requestBody);
			try {
				cclient = Helper.JsonToObject(requestBody, SignUpRequest.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setLang(cclient.getLang());
					logs.setIsB2B(cclient.getIsB2B());
				}
			} catch (Exception ex) {
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
						resp.setReturnMsg(verification);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					// return RequestSoap( cclient);
					try {
						/////////// starting get Customer////////////////
						/// get customer start
						/*
						 * GetCustomerDataLand getCustomer = new
						 * GetCustomerDataLand(); GetCustomerRequestClient
						 * getCustomerRequestClient = new
						 * GetCustomerRequestClient();
						 * getCustomerRequestClient.setChannel(cclient.
						 * getChannel());
						 * getCustomerRequestClient.setiP(cclient.getiP());
						 * getCustomerRequestClient.setLang(cclient.getLang());
						 * getCustomerRequestClient.setMsisdn(cclient.getmsisdn(
						 * )); ObjectMapper mapper = new ObjectMapper(); String
						 * jsonInString =
						 * mapper.writeValueAsString(getCustomerRequestClient);
						 * GetCustomerRequestResponse responseGetCustomer =
						 * getCustomer.Get(Constants.CREDENTIALSUNCODED, "",
						 * jsonInString);
						 */
						com.huawei.crm.query.GetSubscriberResponse respns = new com.evampsaanga.bakcell.getsubscriber.CRMSubscriberService()
								.GetSubscriberRequest(cclient.getmsisdn());
						if (!respns.getResponseHeader().getRetCode().equals("0")) {
							resp.setReturnCode(respns.getResponseHeader().getRetCode());
							resp.setReturnMsg(respns.getResponseHeader().getRetMsg());
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
						// Check for customer status
						String statusCode = respns.getGetSubscriberBody().getStatus();
						if (statusCode.equalsIgnoreCase("B01") && statusCode.equalsIgnoreCase("B03")) {
							resp.setReturnCode(ResponseCodes.CUSTOMER_STATUS_ERROR_CODE);
							resp.setReturnMsg(ResponseCodes.CUSTOMER_STATUS_ERROR_DES);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
						/// get customer end
						/////////// end Customer ////////////////
						JSONObject jsonObject = new JSONObject();
						jsonObject.put("storeId", cclient.getLang());
						jsonObject.put("msisdn", cclient.getmsisdn());
						jsonObject.put("requestPlatform", cclient.getCause() + "-" + cclient.getChannel());
						jsonObject.put("customerId", respns.getGetSubscriberBody().getCustomerId() + "");
						jsonObject.put("type", "sendOtp");
						String response = RestClient.SendCallToMagento(
								ConfigurationManager.getConfigurationFromCache("magento.app.signupflow.resendOtp"),
								jsonObject.toString());
						logger.info("Response from resent pin otp: " + response);
						JSONObject jsonObject2 = new JSONObject(response);
						try {
							// MagentoResponseValidateMsisdn data =
							// Helper.JsonToObject(response,
							// MagentoResponseValidateMsisdn.class);
							logger.info("Result code " + jsonObject2.getString("status_code"));
							if (jsonObject2.getString("status_code").equals("200")) {
								try {
									String message = "";
									if (cclient.getCause().equalsIgnoreCase("usagehistory-details-view"))
										message = Helper.getMessage("usagehistory", cclient.getLang(),
												jsonObject2.getString("pin"));
									else if (cclient.getCause().equalsIgnoreCase("forgotpassword"))
										message = Helper.getMessage("forgot", cclient.getLang(),
												jsonObject2.getString("pin"));
									else if (cclient.getCause().equalsIgnoreCase("signup"))
										message = Helper.getMessage("signup", cclient.getLang(),
												jsonObject2.getString("pin"));
									else
										message = Helper.getMessage("moneytransfer", cclient.getLang(),
												jsonObject2.getString("pin"));
									DBFactory.insertMessageIntoEsmg(cclient.getmsisdn(), message, null, null, null);
									// SendSMCService service = new
									// SendSMCService();
									// if (service.sendSMS(cclient.getmsisdn(),
									// data.getMsg(), cclient.getChannel(),
									// cclient.getLang(), cclient.getiP())) {
									// resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
									// resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
									// logs.setResponseCode(resp.getReturnCode());
									// logs.setResponseDescription(resp.getReturnMsg());
									// logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
									/*
									 * } else {
									 * 
									 * resp.setReturnCode(ResponseCodes.
									 * CONNECTIVITY_PROBLEM_CODE);
									 * resp.setReturnMsg(ResponseCodes.
									 * CONNECTIVITY_PROBLEM_DES);
									 * logs.setResponseCode(resp.getReturnCode()
									 * ); logs.setResponseDescription(resp.
									 * getReturnMsg());
									 * logs.setResponseDateTime(Helper.
									 * GenerateDateTimeToMsAccuracy()); }
									 */
								} catch (Exception ex) {
									Helper.GetException(ex);
									resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
									resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
									logs.setResponseCode(resp.getReturnCode());
									logs.setResponseDescription(resp.getReturnMsg());
									logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								}
								resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
								resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
								resp.setPinMsg(jsonObject2.getString("pin"));
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							} else {
								resp.setReturnCode(jsonObject2.getString("status_code"));
								resp.setReturnMsg(jsonObject2.getString("msg"));
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							}
						} catch (Exception ex) {
							resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
							resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (JSONException ex) {
						resp.setReturnCode(ResponseCodes.MAGENTO_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.MAGENTO_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					} catch (Exception ex) {
						resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
						resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		logs.updateLog(logs);
		return resp;
	}

	@POST
	@Path("/signupsendotp")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public com.evampsaanga.bakcell.signupsendotp.SignUpResponse SignUpSendotp(@Header("credentials") String credential,
			@Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.SEND_OTP_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.SEND_OTP);
		logs.setTableType(LogsType.SendOtp);
		logger.info("Request body signupsendotp" + requestBody);
		com.evampsaanga.bakcell.signupsendotp.SignUpRequest cclient = null;
		com.evampsaanga.bakcell.signupsendotp.SignUpResponse resp = new com.evampsaanga.bakcell.signupsendotp.SignUpResponse();
		try {
			cclient = Helper.JsonToObject(requestBody, com.evampsaanga.bakcell.signupsendotp.SignUpRequest.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
				logs.setIsB2B(cclient.getIsB2B());
			}
		} catch (Exception ex) {
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				// return RequestSoap( cclient);
				try {

					/// get customer start
					/*
					 * GetCustomerDataLand getCustomer = new
					 * GetCustomerDataLand(); GetCustomerRequestClient
					 * getCustomerRequestClient = new
					 * GetCustomerRequestClient();
					 * getCustomerRequestClient.setChannel(cclient.getChannel())
					 * ; getCustomerRequestClient.setiP(cclient.getiP());
					 * getCustomerRequestClient.setLang(cclient.getLang());
					 * getCustomerRequestClient.setMsisdn(cclient.getmsisdn());
					 * ObjectMapper mapper = new ObjectMapper(); String
					 * jsonInString =
					 * mapper.writeValueAsString(getCustomerRequestClient);
					 * GetCustomerRequestResponse responseGetCustomer =
					 * getCustomer.Get(Constants.CREDENTIALSUNCODED, "",
					 * jsonInString);
					 */
					com.huawei.crm.query.GetSubscriberResponse respns = new com.evampsaanga.bakcell.getsubscriber.CRMSubscriberService()
							.GetSubscriberRequest(cclient.getmsisdn());
					if (!respns.getResponseHeader().getRetCode().equals("0")) {
						resp.setReturnCode(respns.getResponseHeader().getRetCode());
						resp.setReturnMsg(respns.getResponseHeader().getRetMsg());
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
					if (!respns.getGetSubscriberBody().getStatus().equalsIgnoreCase("B01")
							&& !respns.getGetSubscriberBody().getStatus().equalsIgnoreCase("B04")) {
						resp.setReturnCode("551");
						resp.setReturnMsg("The number is in " + respns.getGetSubscriberBody().getStatus()
								+ " state, you are not allowed to signup");
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
					/// get customer end
					// com.cloudcontrolled.api.client.security.DumbX509TrustManager
					// se=null;
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("storeId", cclient.getLang());
					jsonObject.put("msisdn", cclient.getmsisdn());
					jsonObject.put("customerId", respns.getGetSubscriberBody().getCustomerId() + "");
					jsonObject.put("requestPlatform", cclient.getCause() + "-" + cclient.getChannel());
					jsonObject.put("type", "sendOtp");
					String response = null;

					// if(BuildCacheRequestLand.getCustomerModel(cclient.getmsisdn())!=null)
					// {
					loggerV2.info("SIGNUPSENDOTP-" + cclient.getmsisdn() + ": Customer exist in cache");
					if (cclient.getCause().equalsIgnoreCase("forgotpassword")) {
						loggerV2.info("SIGNUPSENDOTP-" + cclient.getmsisdn() + ": FORGOTPASSWORD FLOW");
						response = forgotPassword(cclient, respns.getGetSubscriberBody().getCustomerId(), jsonObject);
						if (response.equalsIgnoreCase(Constants.UNABLE_TO_DELETE_DESC)) {
							// unable to delete
							resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
							resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
							return resp;
						} else if (response.equalsIgnoreCase(Constants.USER_NOT_EXIST_IN_DB_OR_CACHE_DESC)) {
							// user does not exisist
							resp.setReturnCode("06");
							resp.setReturnMsg("User does not exist");
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} else if (cclient.getCause().equalsIgnoreCase("signup")) {

						loggerV2.info("SIGNUPSENDOTP-" + cclient.getmsisdn() + ": SIGNUP FLOW");
						response = signup(cclient, respns.getGetSubscriberBody().getCustomerId(), jsonObject);
						if (response.equalsIgnoreCase(Constants.UNABLE_TO_DELETE_DESC)) {
							// unable to delete
							resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
							resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
							return resp;
						} else if (response.equalsIgnoreCase(Constants.USER_NOT_EXIST_IN_DB_OR_CACHE_DESC)) {
							// user does not exisist
							resp.setReturnCode(ResponseCodes.USER_DOESNOT_EXIST_CODE); // 06
							resp.setReturnMsg(ResponseCodes.USER_DOESNOT_EXIST_DESC);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;

						} else if (response.equalsIgnoreCase(Constants.USER_ALREADY_EXIST)) {
							// something went wrong
							resp.setReturnCode(ResponseCodes.USER_ALREADY_EXIST_CODE);
							resp.setReturnMsg(ResponseCodes.USER_ALREADY_EXIST_DESC);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					}
					// }
					// else
					// {
					// loggerV2.info("SIGNUPSENDOTP-"+cclient.getmsisdn()+":
					// Customer not cache");
					// resp.setReturnCode(ResponseCodes.USER_DOESNOT_EXIST_CODE);
					// //06
					// resp.setReturnMsg(ResponseCodes.USER_DOESNOT_EXIST_DESC);
					// logs.setResponseCode(resp.getReturnCode());
					// logs.setResponseDescription(resp.getReturnMsg());
					// logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					// logs.updateLog(logs);
					// return resp;
					// }

					// if (cclient.getCause().equalsIgnoreCase("forgotpassword")
					// &&
					// !BuildCacheRequestLand.customerCache.containsKey(cclient.getmsisdn()))
					// {
					// resp.setReturnCode("06");
					// resp.setReturnMsg("User does not exist");
					// logs.setResponseCode(resp.getReturnCode());
					// logs.setResponseDescription(resp.getReturnMsg());
					// logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					// logs.updateLog(logs);
					// return resp;
					//
					// } else if
					// (!cclient.getCause().equalsIgnoreCase("forgotpassword")
					// &&
					// BuildCacheRequestLand.customerCache.containsKey(cclient.getmsisdn()))
					// {
					// resp.setReturnCode("03");
					// resp.setReturnMsg("User already exist");
					// logs.setResponseCode(resp.getReturnCode());
					// logs.setResponseDescription(resp.getReturnMsg());
					// logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					// logs.updateLog(logs);
					// return resp;
					//
					// } else {
					// response = RestClient.SendCallToMagento(
					// ConfigurationManager.getConfigurationFromCache("magento.app.signupflow.validatemsisdn"),
					// jsonObject.toString());
					//
					logger.info("url:"
							+ ConfigurationManager.getConfigurationFromCache("magento.app.signupflow.validatemsisdn")
							+ " REQUEST:" + jsonObject.toString() + "Response from validatemsisdn: " + response);
					try {
						// MagentoResponseValidateMsisdn data =
						// Helper.JsonToObject(response,
						// MagentoResponseValidateMsisdn.class);
						JSONObject responseJson = new JSONObject(response);
						logger.info("Result code " + responseJson.getString("status_code"));
						if (responseJson.getString("status_code").equals("200")) {
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							resp.setPinMsg(responseJson.getString("pin"));
							try {

								/*
								 * SendSMCService service = new
								 * SendSMCService(); if
								 * (service.sendSMS(cclient.getmsisdn(),
								 * data.getMsg(), cclient.getChannel(),
								 * cclient.getLang(), cclient.getiP())) {
								 */
								// resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
								// resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
								/*
								 * } else {
								 * 
								 * resp.setReturnCode(ResponseCodes. ` *
								 * CONNECTIVITY_PROBLEM_CODE);
								 * resp.setReturnMsg(ResponseCodes.
								 * CONNECTIVITY_PROBLEM_DES);
								 * resp.setPinMsg(""); }
								 */
							} catch (Exception ex) {
								Helper.GetException(ex);
								resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
								resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
							}
						} else {
							resp.setReturnCode(responseJson.getString("status_code"));
							resp.setReturnMsg(responseJson.getString("msg"));
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception ex) {
						resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}

				} catch (JSONException ex) {
					logger.error(Helper.GetException(ex));
					resp.setReturnCode(ResponseCodes.MAGENTO_SERVER_ERROR_CODE);
					resp.setReturnMsg(ResponseCodes.MAGENTO_SERVER_ERROR_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				} catch (Exception ex) {
					logger.error(Helper.GetException(ex));
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		logs.updateLog(logs);
		return resp;
	}

	private String signup(com.evampsaanga.bakcell.signupsendotp.SignUpRequest cclient, Long customerId,
			JSONObject jsonObject) throws Exception {
		// TODO Auto-generated method stub
		// function check user in cache if not there fetch from DB and update
		// cache

		loggerV2.info("SIGNUPSENDOTP-" + cclient.getmsisdn() + ":Signup flow executed");
		if (Helper.checkAndUpdateCache(cclient.getmsisdn())) {
			int result = Long.compare(
					Long.valueOf(BuildCacheRequestLand.customerCache.get(cclient.getmsisdn()).getCustomerId()),
					customerId);
			if (result == 0) {
				loggerV2.info("SIGNUPSENDOTP-" + cclient.getmsisdn() + ": Customer id is same");
				// return error customer already exisit
				return Constants.USER_ALREADY_EXIST;
			} else {
				loggerV2.info("SIGNUPSENDOTP-" + cclient.getmsisdn() + ": Customer id is not same");
				if (deleteCustomer(cclient.getmsisdn())) {
					String response = RestClient.SendCallToMagento(
							ConfigurationManager.getConfigurationFromCache("magento.app.signupflow.validatemsisdn"),
							jsonObject.toString());

					JSONObject jsonObject2 = new JSONObject(response);
					// if(jsonObject2.getString("status_code").equalsIgnoreCase("200"))
					// {
					// String message = Helper.getMessage("forgot",
					// cclient.getLang(), jsonObject2.getString("pin"));
					// DBFactory.insertMessageIntoEsmg(cclient.getmsisdn(),
					// message, cclient.getLang(), null, null);
					// }

					if (jsonObject2.getString("status_code").equalsIgnoreCase("200")) {
						String message = Helper.getMessage("signup", cclient.getLang(), jsonObject2.getString("pin"));
						DBFactory.insertMessageIntoEsmg(cclient.getmsisdn(), message, cclient.getLang(), null, null);
					}

					return response;
				} else {
					// something went wrong successfully
					return Constants.UNABLE_TO_DELETE_DESC;
				}
			}
		} else {
			// user not exisist in DB and Cache
			String response = RestClient.SendCallToMagento(
					ConfigurationManager.getConfigurationFromCache("magento.app.signupflow.validatemsisdn"),
					jsonObject.toString());

			JSONObject jsonObject2 = new JSONObject(response);
			if (jsonObject2.getString("status_code").equalsIgnoreCase("200")) {
				String message = Helper.getMessage("forgot", cclient.getLang(), jsonObject2.getString("pin"));
				DBFactory.insertMessageIntoEsmg(cclient.getmsisdn(), message, cclient.getLang(), null, null);
			}

			return response;
		}

	}

	private String forgotPassword(com.evampsaanga.bakcell.signupsendotp.SignUpRequest cclient, Long customerId,
			JSONObject jsonObject) throws Exception {
		// TODO Auto-generated method stub
		loggerV2.info("SIGNUPSENDOTP-" + cclient.getmsisdn() + ":Forgot flow executed");
		if (Helper.checkAndUpdateCache(cclient.getmsisdn())) {
			loggerV2.info("SIGNUPSENDOTP-" + cclient.getmsisdn() + ": Customer exist in Cache and DB");
			int result = Long.compare(
					Long.valueOf(BuildCacheRequestLand.customerCache.get(cclient.getmsisdn()).getCustomerId()),
					customerId);
			if (result == 0) {
				loggerV2.info("SIGNUPSENDOTP-" + cclient.getmsisdn() + ": Customer id is same");
				// validate msisdn API magento

				loggerV2.info(
						"SIGNUPSENDOTP-" + cclient.getmsisdn() + ": Signup Send OTP request:" + jsonObject.toString());
				SendOTPResponse sendOTPResponse = new SendOTPResponse();

				String response = RestClient.SendCallToMagento(
						ConfigurationManager.getConfigurationFromCache("magento.app.signupflow.validatemsisdn"),
						jsonObject.toString());
				loggerV2.info(
						"SIGNUPSENDOTP-" + cclient.getmsisdn() + ": Signup Send OTP response:" + jsonObject.toString());
				JSONObject jsonObject2 = new JSONObject(response);
				if (jsonObject2.getString("status_code").equalsIgnoreCase("200")) {
					String message = Helper.getMessage("forgot", cclient.getLang(), jsonObject2.getString("pin"));
					DBFactory.insertMessageIntoEsmg(cclient.getmsisdn(), message, cclient.getLang(), null, null);
				}
				return response;
			} else {
				loggerV2.info("SIGNUPSENDOTP-" + cclient.getmsisdn() + ":Customer id is not same");
				// deleting customer
				if (deleteCustomer(cclient.getmsisdn())) {

					String response = RestClient.SendCallToMagento(
							ConfigurationManager.getConfigurationFromCache("magento.app.signupflow.validatemsisdn"),
							jsonObject.toString());

					JSONObject jsonObject2 = new JSONObject(response);
					if (jsonObject2.getString("status_code").equalsIgnoreCase("200")) {
						String message = Helper.getMessage("forgot", cclient.getLang(), jsonObject2.getString("pin"));
						DBFactory.insertMessageIntoEsmg(cclient.getmsisdn(), message, cclient.getLang(), null, null);
					}

					return response;
				} else {
					// something went wrong successfully
					return Constants.UNABLE_TO_DELETE_DESC;
				}
			}
		} else {
			// user not exisist in DB and Cache
			return Constants.USER_NOT_EXIST_IN_DB_OR_CACHE_DESC;
		}

	}

	private boolean deleteCustomer(String getmsisdn) throws Exception {
		// TODO Auto-generated method stub
		try {
			loggerV2.info("SIGNUPSENDOTP-" + getmsisdn + ":Deleting customer " + getmsisdn);
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("msisdn", getmsisdn);
			loggerV2.info("SIGNUPSENDOTP-" + getmsisdn + ":Deleting customer request " + jsonObject.toString());
			String response = RestClient.SendCallToMagento(
					ConfigurationManager.getConfigurationFromCache("magento.app.signupflow.deletecustomer"), // delete
					jsonObject.toString());
			loggerV2.info("SIGNUPSENDOTP-" + getmsisdn + ":Deleting customer response " + response);
			JSONObject responseJSON = new JSONObject(response);
			if (responseJSON.getString("resultCode").equals(Constants.MAGENTO_SUCESS_CODE_DELETE_API)) {
				return true;
			} else
				return false;
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(Helper.GetException(e));
			return false;
		}

	}

	@POST
	@Path("/signupverifyotp")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public com.evampsaanga.bakcell.signupverifyotp.SignUpResponse SignUpVerifyotp(
			@Header("credentials") String credential, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.SIGNUP_VERIFY_OTP_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.SIGNUP_VERIFY);
		logs.setTableType(LogsType.SignUpVerifyOTP);
		logger.info("request body for signup verify =" + requestBody);
		com.evampsaanga.bakcell.signupverifyotp.SignUpRequest cclient = null;
		com.evampsaanga.bakcell.signupverifyotp.SignUpResponse resp = new com.evampsaanga.bakcell.signupverifyotp.SignUpResponse();
		try {
			cclient = Helper.JsonToObject(requestBody, com.evampsaanga.bakcell.signupverifyotp.SignUpRequest.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setLang(cclient.getLang());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setIsB2B(cclient.getIsB2B());
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				// return RequestSoap( cclient);
				try {
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("msisdn", cclient.getmsisdn());
					jsonObject.put("requestPlatform", cclient.getCause() + "-" + cclient.getChannel());
					jsonObject.put("otp", cclient.getPin());
					jsonObject.put("type", "verifyOTP");

					String responseFromOTP = RestClient.SendCallToMagento(
							ConfigurationManager.getConfigurationFromCache("magento.app.signupflow.verifyOtp"),
							jsonObject.toString());
					logger.info("Response from otp: " + responseFromOTP);
					try {
						// MagentoResponseVerifyOTP dataFromVerifyOTP =
						// Helper.JsonToObject(responseFromOTP,
						// MagentoResponseVerifyOTP.class);
						JSONObject dataFromVerifyOTP = new JSONObject(responseFromOTP);
						logger.info("Result code " + dataFromVerifyOTP.getString("status_code"));
						if (dataFromVerifyOTP.getString("status_code").equals("15")) {
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						} else {
							resp.setReturnCode(dataFromVerifyOTP.getString("status_code"));
							resp.setReturnMsg(dataFromVerifyOTP.getString("msg"));
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception ex) {
						logger.error(Helper.GetException(ex));
						resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} catch (JSONException ex) {
					logger.error(Helper.GetException(ex));
					resp.setReturnCode(ResponseCodes.MAGENTO_SERVER_ERROR_CODE);
					resp.setReturnMsg(ResponseCodes.MAGENTO_SERVER_ERROR_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				} catch (Exception ex) {
					logger.error(Helper.GetException(ex));
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	@POST
	@Path("/forgotpassword")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ForgotPasswordResponse ForgotPassword(@Header("credentials") String credential, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.FORGOT_PASSWORD_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.FORGOT_PASS);
		logs.setTableType(LogsType.ForgotPassword);
		logger.info("request body for forgotpassword =" + requestBody);
		ForgotPasswordRequest cclient = null;
		ForgotPasswordResponse resp = new ForgotPasswordResponse();
		try {
			cclient = Helper.JsonToObject(requestBody, ForgotPasswordRequest.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
				logs.setIsB2B(cclient.getIsB2B());
			}
		} catch (Exception ex) {
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);

			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {
					// if(!getCustomerStatus(cclient))
					// {
					// resp.setReturnCode(ResponseCodes.CUSTOMER_STATUS_ERROR_CODE);
					// resp.setReturnMsg(ResponseCodes.CUSTOMER_STATUS_ERROR_DES);
					// logs.setResponseCode(resp.getReturnCode());
					// logs.setResponseDescription(resp.getReturnMsg());
					// logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					// logs.updateLog(logs);
					// return resp;
					// }
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("storeId", cclient.getLang());
					jsonObject.put("msisdn", cclient.getmsisdn());
					jsonObject.put("password", cclient.getPassword());
					jsonObject.put("confirmPassword", cclient.getConfirmPassword());
					jsonObject.put("temp", cclient.getTemp());

					JSONObject jsonObject2 = new JSONObject();
					jsonObject2.put("type", "forgotPassword");
					jsonObject2.put("customerData", jsonObject);
					CustomerModelCache customerModelCache = BuildCacheRequestLand.customerCache
							.get(cclient.getmsisdn());
					jsonObject.put("entity_id", customerModelCache.getEntity_id());
					String response;
					if (BuildCacheRequestLand.getCustomerModel(cclient.getmsisdn()) != null) {
						response = RestClient.SendCallToMagento(
								ConfigurationManager.getConfigurationFromCache("magento.app.signupflow.forgotpassword"),
								jsonObject2.toString());
						logger.info("Response from forgotpassword: " + response);
						try {
							com.evampsaanga.bakcell.forgotpassword.MagentoResponse data = Helper.JsonToObject(response,
									com.evampsaanga.bakcell.forgotpassword.MagentoResponse.class);
							logger.info("Result code " + data.getResultCode());
							if (data.getResultCode().equals("56")) {

								String passworHasd = Helper.checkSHApass(cclient.getConfirmPassword(),
										cclient.getmsisdn());

								CustomerModelCache model = BuildCacheRequestLand.customerCache.get(cclient.getmsisdn());
								model.setPassword_hash(passworHasd);
								BuildCacheRequestLand.customerCache.put(cclient.getmsisdn(), model);
								resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
								resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
								resp.setEntityId(data.getEntityId());
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							} else {
								resp.setReturnCode(data.getResultCode());
								resp.setReturnMsg(data.getMsg());
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							}
						} catch (Exception ex) {
							resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
							resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} else {
						resp.setReturnCode("06");
						resp.setReturnMsg("User does not exist");
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} catch (JSONException ex) {
					resp.setReturnCode(ResponseCodes.MAGENTO_SERVER_ERROR_CODE);
					resp.setReturnMsg(ResponseCodes.MAGENTO_SERVER_ERROR_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				} catch (Exception ex) {
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	/**
	 * forgot password api
	 * 
	 * @param credential
	 * @param requestBody
	 * @return
	 */
	// Forgot password phase 2
	@POST
	@Path("/forgotpasswordV2")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ForgotPasswordResponse ForgotPasswordV2(@Header("credentials") String credential,
			@Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.FORGOT_PASSWORD_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.FORGOT_PASS);
		logs.setTableType(LogsType.ForgotPassword);
		Helper.logInfoMessageV2("request body for forgotpassword =" + requestBody);
		ForgotPasswordRequestV2 cclient = null;
		ForgotPasswordResponse resp = new ForgotPasswordResponse();
		try {
			cclient = Helper.JsonToObject(requestBody, ForgotPasswordRequestV2.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
				logs.setIsB2B(cclient.getIsB2B());
			}
		} catch (Exception ex) {
			loggerV2.error("Error: ", ex);
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				loggerV2.error(Helper.GetException(ex));
			}
			if (credentials == null) {
				Helper.logInfoMessageV2(cclient.getmsisdn() + " - Access Not Authorized");
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					Helper.logInfoMessageV2(cclient.getmsisdn() + " - Verification Failed");
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				Helper.logInfoMessageV2(cclient.getmsisdn() + " - Invalid Msisdn");
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("username", cclient.getUserName());
					jsonObject.put("password", cclient.getPassword());
					jsonObject.put("confirmPassword", cclient.getConfirmPassword());
					jsonObject.put("temp", cclient.getTemp());
					String response = RestClient.SendCallToMagento(
							ConfigurationManager.getConfigurationFromCache("magento.app.signupflow.forgotpasswordV2"),
							jsonObject.toString());
					Helper.logInfoMessageV2(
							cclient.getmsisdn() + " - Response from Magento forgotpassword V2: " + response);
					try {
						com.evampsaanga.bakcell.forgotpassword.MagentoResponse data = Helper.JsonToObject(response,
								com.evampsaanga.bakcell.forgotpassword.MagentoResponse.class);
						Helper.logInfoMessageV2(cclient.getmsisdn() + " - Result code " + data.getResultCode());
						if (data.getResultCode().equals("254")) {
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						} else {
							resp.setReturnCode(data.getResultCode());
							resp.setReturnMsg(data.getMsg());
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception ex) {
						loggerV2.error("Error: ", ex);
						resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} catch (JSONException ex) {
					loggerV2.error("Error: ", ex);
					resp.setReturnCode(ResponseCodes.MAGENTO_SERVER_ERROR_CODE);
					resp.setReturnMsg(ResponseCodes.MAGENTO_SERVER_ERROR_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				} catch (Exception ex) {
					loggerV2.error("Error: ", ex);
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				Helper.logInfoMessageV2(cclient.getmsisdn() + " - Access Not Authorized");
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	@POST
	@Path("/changepassword")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ChangePasswordResponse ChangePassword(@Header("credentials") String credential, @Body() String requestBody) {
		logger.info("request body for ChangePassword =" + requestBody);
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.CHANGE_PASSWORD_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.CHANGE_PASS);
		logs.setTableType(LogsType.ChangePassword);
		ChangePasswordRequest cclient = null;
		ChangePasswordResponse resp = new ChangePasswordResponse();
		try {
			cclient = Helper.JsonToObject(requestBody, ChangePasswordRequest.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
				logs.setIsB2B(cclient.getIsB2B());
			}
		} catch (Exception ex) {
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("msisdn", cclient.getmsisdn());
					jsonObject.put("oldPassword", cclient.getOldPassword());
					jsonObject.put("newPassword", cclient.getNewPassword());
					jsonObject.put("confirmNewPassword", cclient.getConfirmNewPassword());
					jsonObject.put("entity_id",
							BuildCacheRequestLand.customerCache.get(cclient.getmsisdn()).getEntity_id());

					if (BuildCacheRequestLand.getCustomerModel(cclient.getmsisdn()) != null) {
						if (BuildCacheRequestLand.customerCache.get(cclient.getmsisdn()).getPassword_hash()
								.equals(Helper.checkSHApass(cclient.getOldPassword(), cclient.getmsisdn()))) {
							String response = RestClient.SendCallToMagento(ConfigurationManager
									.getConfigurationFromCache("magento.app.signupflow.changepassword"),
									jsonObject.toString());

							logger.info("Response from ChangePassword: " + response);
							try {
								com.evampsaanga.bakcell.changepassword.MagentoResponse data = Helper.JsonToObject(
										response, com.evampsaanga.bakcell.changepassword.MagentoResponse.class);
								logger.info("Result code " + data.getResultCode());
								if (data.getResultCode().equals("105")) {
									String passworHasd = Helper.checkSHApass(cclient.getConfirmNewPassword(),
											cclient.getmsisdn());

									CustomerModelCache model = BuildCacheRequestLand.customerCache
											.get(cclient.getmsisdn());
									model.setPassword_hash(passworHasd);

									BuildCacheRequestLand.customerCache.put(cclient.getmsisdn(), model);

									resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
									resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
									logs.setResponseCode(resp.getReturnCode());
									logs.setResponseDescription(resp.getReturnMsg());
									logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
									logs.updateLog(logs);
									return resp;
								} else {
									resp.setReturnCode(data.getResultCode());
									resp.setReturnMsg(data.getMsg());
									logs.setResponseCode(resp.getReturnCode());
									logs.setResponseDescription(resp.getReturnMsg());
									logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
									logs.updateLog(logs);
									return resp;
								}
							} catch (Exception ex) {
								resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
								resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							}
						} else {
							resp.setReturnCode(ResponseCodes.PASSWORD_AUTHENTICATION_FAILED);
							resp.setReturnMsg(ResponseCodes.PASSWORD_AUTHENTICATION_FAILED_MSG);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} else {
						resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
						resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} catch (JSONException ex) {
					resp.setReturnCode(ResponseCodes.MAGENTO_SERVER_ERROR_CODE);
					resp.setReturnMsg(ResponseCodes.MAGENTO_SERVER_ERROR_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				} catch (Exception ex) {
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	/**
	 * change password api for phase2
	 * 
	 * @param credential
	 * @param requestBody
	 * @return
	 */
	// Change password Phase 2
	@POST
	@Path("/changepasswordV2")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ChangePasswordResponse ChangePasswordV2(@Header("credentials") String credential,
			@Body() String requestBody) {
		Helper.logInfoMessageV2("Request body for ChangePasswordV2 =" + requestBody);
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.CHANGE_PASSWORD_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.CHANGE_PASS);
		logs.setTableType(LogsType.ChangePassword);
		ChangePasswordRequestV2 cclient = null;
		ChangePasswordResponse resp = new ChangePasswordResponse();
		try {
			cclient = Helper.JsonToObject(requestBody, ChangePasswordRequestV2.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
				logs.setIsB2B(cclient.getIsB2B());
			} else
				loggerV2.error("CCLIENT NULL");
		} catch (Exception ex) {
			loggerV2.error("ERROR: ", ex);
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));
			}
			if (credentials == null) {
				Helper.logInfoMessageV2(cclient.getmsisdn() + " - ERROR: credentials == null");
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				/*
				 * String verification = Helper.validateRequest(cclient);
				 * logger.info("<<<<<<<< Verificatio is:" + verification); if
				 * (!verification.equals("")) {
				 * logger.error("<<<<< Verification failed >>>>>");
				 * resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				 * resp.setReturnMsg(verification);
				 * logs.setResponseCode(resp.getReturnCode());
				 * logs.setResponseDescription(resp.getReturnMsg());
				 * logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy(
				 * )); logs.updateLog(logs); return resp; }
				 */
			} else {
				Helper.logInfoMessageV2(cclient.getmsisdn() + " - Invalid MSISDN");
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("username", cclient.getUserName());
					jsonObject.put("oldPassword", cclient.getOldPassword());
					jsonObject.put("newPassword", cclient.getNewPassword());
					jsonObject.put("confirmNewPassword", cclient.getConfirmNewPassword());

					String response = RestClient.SendCallToMagento(
							ConfigurationManager.getConfigurationFromCache("magento.app.signupflow.changepasswordV2"),
							jsonObject.toString());
					Helper.logInfoMessageV2(
							cclient.getmsisdn() + " - Response from Magento ChangePassword: " + response);
					try {
						com.evampsaanga.bakcell.changepassword.MagentoResponse data = Helper.JsonToObject(response,
								com.evampsaanga.bakcell.changepassword.MagentoResponse.class);
						Helper.logInfoMessageV2(cclient.getmsisdn() + " - Result code " + data.getResultCode());
						if (data.getResultCode().equals("105")) {
							// BuildCacheRequestLand.customerCache.put(arg0,
							// arg1)

							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						} else {
							resp.setReturnCode(data.getResultCode());
							resp.setReturnMsg(data.getMsg());
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception ex) {
						loggerV2.error("Error:", ex);
						resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} catch (JSONException ex) {
					loggerV2.error("Error:", ex);
					resp.setReturnCode(ResponseCodes.MAGENTO_SERVER_ERROR_CODE);
					resp.setReturnMsg(ResponseCodes.MAGENTO_SERVER_ERROR_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				} catch (Exception ex) {
					loggerV2.error("Error:", ex);
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				Helper.logInfoMessageV2(cclient.getmsisdn() + " - Access Not Authorized");
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	@POST
	@Path("/contactus")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ContactUsResponse ContactUs(@Header("credentials") String credential, @Body() String requestBody) {
		logger.info("request body for ContactUs =" + requestBody);
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.CONTACTUS__TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.CONTACTUS);
		logs.setTableType(LogsType.ContactUS);
		ContactUsRequest cclient = null;
		ContactUsResponse resp = new ContactUsResponse();
		try {
			cclient = Helper.JsonToObject(requestBody, ContactUsRequest.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
				logs.setIsB2B(cclient.getIsB2B());
			}
		} catch (Exception ex) {
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("storeId", cclient.getStoreId());
					String response = RestClient.SendCallToMagento(
							ConfigurationManager.getConfigurationFromCache("magento.app.contactus"),
							jsonObject.toString());
					logger.info("Response from ContactUs: " + response);
					try {
						ContactUsMagentoResponse data = Helper.JsonToObject(response, ContactUsMagentoResponse.class);
						logger.info("Result code " + data.getResultCode());
						if (data.getResultCode().equals("93") || data.getResultCode().equals("92")) {
							resp.setData(data.getData());
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						} else {
							resp.setReturnCode(data.getResultCode());
							resp.setReturnMsg(data.getMsg());
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception ex) {
						resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} catch (JSONException ex) {
					resp.setReturnCode(ResponseCodes.MAGENTO_SERVER_ERROR_CODE);
					resp.setReturnMsg(ResponseCodes.MAGENTO_SERVER_ERROR_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				} catch (Exception ex) {
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		logs.updateLog(logs);
		return resp;
	}

	/**
	 * Get tariff Details for phase 2
	 * 
	 * @param credential
	 * @param requestBody
	 * @return
	 */
	// Tariff Details for Phase 2 B2B
	@SuppressWarnings("unused")
	@POST
	@Path("/tariffdetailsV2")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TariffDetailsResponseVer2 TariffDetailsV2(@Header("credentials") String credential,
			@Body() String requestBody) {
		Helper.logInfoMessageV2("Request body for TariffDetailsV2 =" + requestBody);
		com.evampsaanga.magento.tariffdetailsv2.TariffDetailsRequestV2 cclient = null;
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.TARRIF_DETAILS_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.TARRIF_DETAILS);
		logs.setTableType(LogsType.TarrifDetails);
		TariffDetailsResponseVer2 resp = new TariffDetailsResponseVer2();
		try {
			cclient = Helper.JsonToObject(requestBody,
					com.evampsaanga.magento.tariffdetailsv2.TariffDetailsRequestV2.class);
			Helper.logInfoMessageV2(cclient.getmsisdn() + " - Request Object For Tariff Details" + cclient.toString());
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
				logs.setIsB2B(cclient.getIsB2B());
			}
		} catch (Exception ex) {
			loggerV2.error("Error", ex);
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				loggerV2.error(Helper.GetException(ex));
			}
			if (credentials == null) {
				Helper.logInfoMessageV2(cclient.getmsisdn() + " - Credentials null ");
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}

			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("lang", cclient.getLang());
					jsonObject.put("offeringId", cclient.getOfferingId());
					Helper.logInfoMessageV2(cclient.getmsisdn() + " -Request body from TariffDetails: " + requestBody);
					String response = RestClient.SendCallToMagento(
							ConfigurationManager.getConfigurationFromCache("magento.app.tariffV2"), requestBody);
					Helper.logInfoMessageV2(cclient.getmsisdn() + " - Response From Magento " + response);
					try {
						com.evampsaanga.magento.tariffdetailsv2.TariffDetailsMagentoResponse data = Helper.JsonToObject(
								response, com.evampsaanga.magento.tariffdetailsv2.TariffDetailsMagentoResponse.class);
						try {
							// logger.info("Response from TariffDetails in try:
							// " +
							// jsonObject2);
							Helper.logInfoMessageV2(
									cclient.getmsisdn() + " - Result code tarrif magento " + data.getResultCode());
							if (data.getResultCode().equals(ResponseCodes.TARIFF_DETAILS_VERSION_2_SUCCESSFUL)) {
								resp.setData(data.getData());
								resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
								resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
								// logger.info("Response FROM ESB--" + resp);
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							} else {
								resp.setReturnCode(data.getResultCode());
								resp.setReturnMsg(data.getMsg());
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							}
						} catch (Exception ex) {
							loggerV2.error(Helper.GetException(ex));
							resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
							resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception e) {
						loggerV2.error(Helper.GetException(e));
						resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
						resp.setReturnMsg("No Data Found");
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}

				} catch (Exception ex) {
					loggerV2.error("ERROR:", ex);
					resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
					resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				Helper.logInfoMessageV2(cclient.getmsisdn() + " - Access Not Authorized");
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		logs.updateLog(logs);
		return resp;
	}

	@POST
	@Path("/tariffdetailsb2cupdated")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TariffDetailsResponse TariffDetailsUpdatedB2C(@Header("credentials") String credential,
			@Body() String requestBody) {
		logger.info("request body for TariffDetails =" + requestBody);
		TariffDetailsRequest cclient = null;
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.TARRIF_DETAILS_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.TARRIF_DETAILS);
		logs.setTableType(LogsType.TarrifDetails);
		TariffDetailsResponse resp = new TariffDetailsResponse();
		try {
			cclient = Helper.JsonToObject(requestBody, TariffDetailsRequest.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
				logs.setIsB2B(cclient.getIsB2B());
			}
		} catch (Exception ex) {
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("storeId", cclient.getStoreId());
					jsonObject.put("subscribedOfferingName", cclient.getSubscribedOfferingName());
					jsonObject.put("customerType", cclient.getCustomerType());
					jsonObject.put("subscriberType", cclient.getSubscriberType());
					jsonObject.put("offeringId", cclient.getOfferingId());
					logger.info("Request from TariffDetails: " + jsonObject.toString());
					String response = RestClient.SendCallToMagento(
							ConfigurationManager.getConfigurationFromCache("magento.app.tariff.new"),
							jsonObject.toString());
					logger.info("Response from TariffDetails: " + response);
					// logger.info("respnse magento-----" +response);
					// JSONObject jsonMagentoResponse = new
					// JSONObject(response);
					// if(jsonMagentoResponse.get("prepaid")
					// if its prepaid
					/*
					 * if(jsonMagentoResponse.has("data")){ String
					 * prepaid=jsonMagentoResponse.getJSONObject("data").
					 * getJSONObject("Prepaid").toString(); Prepaid
					 * prepaidObject=Helper.JsonToObject(response,
					 * com.evampsaanga.magento.tariffdetails.Prepaid.class); }
					 */
					try {
						/*
						 * if (jsonMagentoResponse.has("resultCode")) { if
						 * (jsonMagentoResponse.getString("resultCode").equals(
						 * "62")) { if (jsonMagentoResponse.has("Prepaid")) {
						 * String prepaidobject =
						 * jsonMagentoResponse.getJSONObject("Prepaid").toString
						 * (); Prepaid data = Helper.JsonToObject(prepaidobject,
						 * Prepaid.class);
						 * 
						 * com.evampsaanga.magento.tariffdetails.Data dataaaaa=
						 * new Data();
						 * 
						 * 
						 * } } }
						 */
						TariffDetailsMagentoResponse data = Helper.JsonToObject(response,
								TariffDetailsMagentoResponse.class);
						logger.info("Result code tarrif magento " + data.getResultCode());
						if (data.getResultCode().equals("63") || data.getResultCode().equals("62")) {
							resp.setData(data.getData());
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							logger.info("Response FROM ESB--" + resp);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						} else {
							resp.setReturnCode(data.getResultCode());
							resp.setReturnMsg(data.getMsg());
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception ex) {
						logger.error("ERROR:", ex);
						resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} catch (Exception ex) {
					resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
					resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		logs.updateLog(logs);
		return resp;
	}

	@POST
	@Path("/tariffdetails")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public com.evampsaanga.magento.tariffdetailsold.TariffDetailsResponse TariffDetails(
			@Header("credentials") String credential, @Body() String requestBody) {
		logger.info("request body for TariffDetails =" + requestBody);
		com.evampsaanga.magento.tariffdetailsold.TariffDetailsRequest cclient = null;
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.TARRIF_DETAILS_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.TARRIF_DETAILS);
		logs.setTableType(LogsType.TarrifDetails);
		com.evampsaanga.magento.tariffdetailsold.TariffDetailsResponse resp = new com.evampsaanga.magento.tariffdetailsold.TariffDetailsResponse();
		try {
			cclient = Helper.JsonToObject(requestBody,
					com.evampsaanga.magento.tariffdetailsold.TariffDetailsRequest.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
				logs.setIsB2B(cclient.getIsB2B());
			}
		} catch (Exception ex) {
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("storeId", cclient.getStoreId());
					jsonObject.put("subscribedOfferingName", cclient.getSubscribedOfferingName());
					jsonObject.put("customerType", cclient.getCustomerType());
					jsonObject.put("subscriberType", cclient.getSubscriberType());
					jsonObject.put("offeringId", cclient.getOfferingId());
					logger.info("Request from TariffDetails: " + jsonObject.toString());
					String response = RestClient.SendCallToMagento(
							ConfigurationManager.getConfigurationFromCache("magento.app.tariff"),
							jsonObject.toString());
					logger.info("Response from TariffDetails: " + response);
					// logger.info("respnse magento-----" +response);
					// JSONObject jsonMagentoResponse = new
					// JSONObject(response);
					// if(jsonMagentoResponse.get("prepaid")
					// if its prepaid
					/*
					 * if(jsonMagentoResponse.has("data")){ String
					 * prepaid=jsonMagentoResponse.getJSONObject("data").
					 * getJSONObject("Prepaid").toString(); Prepaid
					 * prepaidObject=Helper.JsonToObject(response,
					 * com.evampsaanga.magento.tariffdetails.Prepaid.class); }
					 */
					try {
						/*
						 * if (jsonMagentoResponse.has("resultCode")) { if
						 * (jsonMagentoResponse.getString("resultCode").equals(
						 * "62")) { if (jsonMagentoResponse.has("Prepaid")) {
						 * String prepaidobject =
						 * jsonMagentoResponse.getJSONObject("Prepaid").toString
						 * (); Prepaid data = Helper.JsonToObject(prepaidobject,
						 * Prepaid.class);
						 * 
						 * com.evampsaanga.magento.tariffdetails.Data dataaaaa=
						 * new Data();
						 * 
						 * 
						 * } } }
						 */
						com.evampsaanga.magento.tariffdetailsold.TariffDetailsMagentoResponse data = Helper
								.JsonToObject(response,
										com.evampsaanga.magento.tariffdetailsold.TariffDetailsMagentoResponse.class);
						logger.info("Result code tarrif magento " + data.getResultCode());
						if (data.getResultCode().equals("63") || data.getResultCode().equals("62")) {
							resp.setData(data.getData());
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							logger.info("Response FROM ESB--" + resp);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						} else {
							resp.setReturnCode(data.getResultCode());
							resp.setReturnMsg(data.getMsg());
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception ex) {
						logger.error("ERROR:", ex);
						resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} catch (Exception ex) {
					resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
					resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		logs.updateLog(logs);
		return resp;
	}

	@POST
	@Path("/supplementaryservices")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SupplementryServicesResponse SupplementryServices(@Header("credentials") String credential,
			@Body() String requestBody) {
		logger.info("request body for SupplementryServices =" + requestBody);
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.SUPPLEMENTARY_SERVICES_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.SUPPLEMENTARY_SERVICES);
		logs.setTableType(LogsType.SupplementaryServices);
		SupplementryServicesRequest cclient = null;
		SupplementryServicesResponse resp = new SupplementryServicesResponse();
		try {
			cclient = Helper.JsonToObject(requestBody, SupplementryServicesRequest.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
				logs.setIsB2B(cclient.getIsB2B());
			}
		} catch (Exception ex) {
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("lang", cclient.getLang());
					jsonObject.put("offeringName", cclient.getOfferingName());
					jsonObject.put("brandName", cclient.getBrandName());

					logger.info("Request from suplementryservices: " + jsonObject.toString());
					String response = RestClient.SendCallToMagento(
							ConfigurationManager.getConfigurationFromCache("magento.app.suplementryservices"),
							jsonObject.toString());
					logger.info("Response from suplementryservices : " + response);
					JSONObject rspMag = new JSONObject(response);
					if (rspMag.has("resultCode"))
						logger.info("Result code-----" + rspMag.getString("resultCode"));
					if (rspMag.has("data"))
						logger.info("Data oject---" + rspMag.getJSONObject("data"));
					try {
						// JSONObject rspMag = new JSONObject(response);
						SupplimentryMagentoResponse data = Helper.JsonToObject(response,
								SupplimentryMagentoResponse.class);

						/*
						 * if (!cclient.getChannel().equalsIgnoreCase("web")) {
						 * ArrayList<Offers> offers = removingTM(data, cclient);
						 * 
						 * data.getData().getTM().setOffers(offers); }
						 */
						logger.info("Response code from MAgento=---" + data.getResultCode());
						logger.info("Response Message from MAgento=---" + data.getMsg());
						if (data.getResultCode().equals("80")) {
							resp.setData(data.getData());
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
							logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						} else {
							resp.setReturnCode(data.getResultCode());
							resp.setReturnMsg(data.getMsg());
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception ex) {
						logger.error(Helper.GetException(ex));
						resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} catch (JSONException ex) {
					logger.error(Helper.GetException(ex));
					resp.setReturnCode(ResponseCodes.MAGENTO_SERVER_ERROR_CODE);
					resp.setReturnMsg(ResponseCodes.MAGENTO_SERVER_ERROR_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				} catch (Exception ex) {
					logger.error(Helper.GetException(ex));
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	@POST
	@Path("/getcdrsbydateotp")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetCDRsByDateOTPResponse GetCDRsByDateOTP(@Header("credentials") String credential,
			@Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.GET_CDRSBY_DATE_OTP_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.GET_CDRS_BYDATE_OTP);
		logs.setTableType(LogsType.GetScdrsByDateOtp);
		logger.info("request body for GetCDRsByDateOTP =" + requestBody);
		GetCDRsByDateOTPRequest cclient = null;
		GetCDRsByDateOTPResponse resp = new GetCDRsByDateOTPResponse();
		try {
			cclient = Helper.JsonToObject(requestBody, GetCDRsByDateOTPRequest.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
				logs.setIsB2B(cclient.getIsB2B());
			}
		} catch (Exception ex) {
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("storeId", cclient.getLang());
					jsonObject.put("msisdn", cclient.getmsisdn());
					jsonObject.put("accountId", cclient.getAccountId());
					jsonObject.put("requestPlatform", "usagehistory-details-view-" + cclient.getChannel());
					String response = RestClient.SendCallToMagento(
							ConfigurationManager.getConfigurationFromCache("magento.app.cdrsbydategetotp"),
							jsonObject.toString());
					logger.info("Response from GetCDRsByDateOTP: " + response);
					try {
						GetCDRsByDateOTPMagentoResponse data = Helper.JsonToObject(response,
								GetCDRsByDateOTPMagentoResponse.class);
						logger.info("Result code " + data.getResultCode());
						if (data.getResultCode().equals("112")) {
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							resp.setPin(data.getPin() + "");
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						} else {
							resp.setReturnCode(data.getResultCode());
							resp.setReturnMsg(data.getMsg());
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception ex) {
						resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} catch (JSONException ex) {
					resp.setReturnCode(ResponseCodes.MAGENTO_SERVER_ERROR_CODE);
					resp.setReturnMsg(ResponseCodes.MAGENTO_SERVER_ERROR_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				} catch (Exception ex) {
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		logs.updateLog(logs);
		return resp;
	}

	@POST
	@Path("/updatecustomeremail")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UpdateUserEmailResponseClient UpdateCustomerEmail(@Header("credentials") String credential,
			@Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.UPDATE_CUSTOMER_EMAIL_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.UPDATE_CUSTOMER_EMAIL);
		logs.setTableType(LogsType.UpdateCustomerEmail);
		logger.info("request body for updatecustomeremail =" + requestBody);
		UpdateUserEmailRequest cclient = null;
		UpdateUserEmailResponseClient resp = new UpdateUserEmailResponseClient();
		try {
			cclient = Helper.JsonToObject(requestBody, UpdateUserEmailRequest.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
				logs.setIsB2B(cclient.getIsB2B());
			}
		} catch (Exception ex) {
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("msisdn", cclient.getmsisdn());
					jsonObject.put("emailId", cclient.getEmail());
					jsonObject.put("storeId", cclient.getLang());
					String response = RestClient.SendCallToMagento(
							ConfigurationManager.getConfigurationFromCache("magento.app.updateuseremail"),
							jsonObject.toString());
					logger.info("Response from updatecustomeremail: " + response);
					try {
						UpdateUserEmailResponse data = Helper.JsonToObject(response, UpdateUserEmailResponse.class);
						logger.info("Result code " + data.getResultCode());
						if (data.getResultCode().equals("124")) {

							// if(BuildCacheRequestLand.customerCache==null)
							// BuildCacheRequestLand.initHazelcast();
							// CustomerModelCache customerModelCache =
							// BuildCacheRequestLand.customerCache.get(cclient.getmsisdn());
							// customerModelCache.setEmail(cclient.getEmail());
							// BuildCacheRequestLand.customerCache.replace(cclient.getmsisdn(),
							// customerModelCache);
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						} else {
							resp.setReturnCode(data.getResultCode());
							resp.setReturnMsg(data.getMsg());
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception ex) {
						resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						logger.info(Helper.GetException(ex));
						return resp;
					}
				} catch (JSONException ex) {
					resp.setReturnCode(ResponseCodes.MAGENTO_SERVER_ERROR_CODE);
					resp.setReturnMsg(ResponseCodes.MAGENTO_SERVER_ERROR_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				} catch (Exception ex) {
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		logs.updateLog(logs);
		return resp;
	}

	/**
	 * pin verification api phase 2
	 * 
	 * @param credential
	 * @param requestBody
	 * @return
	 */
	// Verify Pin For Phase 2
	@POST
	@Path("/verifypin")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public VerifyCDRsOTPResponse VerifyCDRsOTPV2(@Header("credentials") String credential, @Body() String requestBody) {
		Helper.logInfoMessageV2("Request body for VerifyCDRsByDateOTP V2" + requestBody);
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.VERIFY_CDRSBY_DATE_OTP_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.VERIFY_CDRS_BYDATE_OTP);
		logs.setTableType(LogsType.VerifyCdrsOtp);
		VerifyCDRsOTPRequestV2 cclient = null;
		VerifyCDRsOTPResponse resp = new VerifyCDRsOTPResponse();
		try {
			cclient = Helper.JsonToObject(requestBody, VerifyCDRsOTPRequestV2.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
				logs.setIsB2B(cclient.getIsB2B());
			}
		} catch (Exception ex) {
			loggerV2.error("Error: ", ex);
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));
			}
			if (credentials == null) {
				Helper.logInfoMessageV2(cclient.getmsisdn() + " - Credentails Are Null");
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					Helper.logInfoMessageV2(cclient.getmsisdn() + " - Verification of Request Failed");
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				Helper.logInfoMessageV2(cclient.getmsisdn() + " - Msisdn Empty");
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				if (!cclient.getChannel().equals("web")) {
					// VerifyCDRsOTPResponse resp = new VerifyCDRsOTPResponse();
					try {
						JSONObject jsonObject = new JSONObject();
						jsonObject.put("username", cclient.getUserName());
						jsonObject.put("otp", cclient.getOtp());
						jsonObject.put("requestPlatform", cclient.getRequestPlatform() + "-" + cclient.getChannel());
						String response = RestClient.SendCallToMagento(
								ConfigurationManager.getConfigurationFromCache("magento.app.cdrbydateverifyotpV2"),
								jsonObject.toString());
						Helper.logInfoMessageV2(cclient.getmsisdn() + " - Response From Magento: " + response);
						try {
							ValidateCDRByDateResponse data = Helper.JsonToObject(response,
									ValidateCDRByDateResponse.class);
							Helper.logInfoMessageV2(
									cclient.getmsisdn() + " - Result code Magento" + data.getResultCode());
							if (data.getResultCode().equals("215")) {
								resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
								resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
								// logger.info("Result code Esb " +
								// ResponseCodes.SUCESS_CODE_200);
								// logger.info("Result Description Esb " +
								// ResponseCodes.SUCESS_DES_200);
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							} else {
								resp.setReturnCode(data.getResultCode());
								resp.setReturnMsg(data.getMsg());
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							}
						} catch (Exception ex) {
							loggerV2.error("Exception:", ex);
							resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
							resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						} // end of (Exception ex)
					} catch (JSONException ex) {
						loggerV2.error("Exception:", ex);
						resp.setReturnCode(ResponseCodes.MAGENTO_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.MAGENTO_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					} catch (Exception ex) {
						loggerV2.error("Exception:", ex);
						resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
						resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				}
			} else {
				Helper.logInfoMessageV2(cclient.getmsisdn() + " - Access Not Authorized");
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		return resp;
	}

	@POST
	@Path("/verifycdrsbydateotp")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public VerifyCDRsOTPResponse VerifyCDRsOTP(@Header("credentials") String credential, @Body() String requestBody) {
		logger.info("request body for VerifyCDRsByDateOTP =" + requestBody);
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.VERIFY_CDRSBY_DATE_OTP_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.VERIFY_CDRS_BYDATE_OTP);
		logs.setTableType(LogsType.VerifyCdrsOtp);
		VerifyCDRsOTPRequest cclient = null;
		VerifyCDRsOTPResponse resp = new VerifyCDRsOTPResponse();
		try {
			cclient = Helper.JsonToObject(requestBody, VerifyCDRsOTPRequest.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
				logs.setIsB2B(cclient.getIsB2B());
			}
		} catch (Exception ex) {
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				if (!cclient.getChannel().equals("web")) {
					// VerifyCDRsOTPResponse resp = new VerifyCDRsOTPResponse();
					try {
						JSONObject jsonObject = new JSONObject();
						jsonObject.put("accountId", cclient.getAccountId());
						jsonObject.put("msisdn", cclient.getmsisdn());
						jsonObject.put("otp", cclient.getPin());
						jsonObject.put("requestPlatform", "usagehistory-details-view-" + cclient.getChannel());
						jsonObject.put("type", "verifyOTP");
						String response = RestClient.SendCallToMagento(
								ConfigurationManager.getConfigurationFromCache("magento.app.signupflow.verifyOtp"),
								jsonObject.toString());
						try {
							// ValidateCDRByDateResponse data =
							// Helper.JsonToObject(response,
							// ValidateCDRByDateResponse.class);
							JSONObject jsonObject2 = new JSONObject(response);
							logger.info("Result code Magento " + jsonObject2.getString("status_code"));
							if (jsonObject2.getString("status_code").equals("15")) {
								resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
								resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
								logger.info("Result code Esb " + ResponseCodes.SUCESS_CODE_200);
								logger.info("Result Description Esb " + ResponseCodes.SUCESS_DES_200);
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							} else {
								resp.setReturnCode(jsonObject2.getString("status_code"));
								resp.setReturnMsg(jsonObject2.getString("msg"));
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							}
						} catch (Exception ex) {
							resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
							resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						} // end of (Exception ex)
					} catch (JSONException ex) {
						resp.setReturnCode(ResponseCodes.MAGENTO_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.MAGENTO_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					} catch (Exception ex) {
						resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
						resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		return resp;
	}

	@POST
	@Path("/getstorelocator")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetStoreLocatorResponseClient GetStoreLocator(@Header("credentials") String credential,
			@Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.STORE_LOCATOR_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.STORE_LOCATORL);
		logs.setTableType(LogsType.StoreLocator);
		GetStoreLocatorRequestClient cclient = null;
		GetStoreLocatorResponseClient resp = new GetStoreLocatorResponseClient();
		try {
			cclient = Helper.JsonToObject(requestBody, GetStoreLocatorRequestClient.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
				logs.setIsB2B(cclient.getIsB2B());
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				if (!cclient.getChannel().equals("web")) {
					try {
						JSONObject jsonObject = new JSONObject();
						jsonObject.put("lang", cclient.getLang());
						String response = RestClient.SendCallToMagento(
								ConfigurationManager.getConfigurationFromCache("magento.app.storelocator"),
								jsonObject.toString());
						JSONObject bjectjson = new JSONObject(response);
						String magentocode = bjectjson.getString("resultCode");
						String magentomessage = bjectjson.getString("msg");
						if (magentocode.equals("711")) {
							resp.setReturnCode(magentocode);
							resp.setReturnMsg(magentomessage);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
						try {
							com.evampsaanga.bakcell.getstorelocator.MagentoResponse data = Helper.JsonToObject(response,
									com.evampsaanga.bakcell.getstorelocator.MagentoResponse.class);
							if (data.getResultCode().equals("70")) {
								resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
								resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
								resp.setData(data.getData());
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							} else {
								resp.setReturnCode(data.getResultCode());
								resp.setReturnMsg(data.getMsg());
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							}
						} catch (Exception ex) {
							logger.error(Helper.GetException(ex));
							resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
							resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						} // end of (Exception ex)
					} catch (JSONException ex) {
						logger.error(Helper.GetException(ex));
						resp.setReturnCode(ResponseCodes.MAGENTO_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.MAGENTO_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					} catch (Exception ex) {
						logger.error(Helper.GetException(ex));
						resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
						resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		logs.updateLog(logs);
		return resp;
	}

	/**
	 * store locator api for phase 2
	 * 
	 * @param credential
	 * @param requestBody
	 * @return
	 */
	// Store Locator for Phase 2
	@POST
	@Path("/getstorelocatorV2")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetStoreLocatorResponseClient GetStoreLocatorV2(@Header("credentials") String credential,
			@Body() String requestBody) {
		Logs logs = new Logs();
		Helper.logInfoMessageV2(" Request body for Store Locator V2: " + requestBody);
		logs.setTransactionName(Transactions.STORE_LOCATOR_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.STORE_LOCATORL);
		logs.setTableType(LogsType.StoreLocator);
		GetStoreLocatorRequestClient cclient = null;
		GetStoreLocatorResponseClient resp = new GetStoreLocatorResponseClient();
		try {
			cclient = Helper.JsonToObject(requestBody, GetStoreLocatorRequestClient.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
				logs.setIsB2B(cclient.getIsB2B());
			}
		} catch (Exception ex) {
			loggerV2.error(Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				loggerV2.error(Helper.GetException(ex));
			}
			if (credentials == null) {
				Helper.logInfoMessageV2(cclient.getmsisdn() + " - Credentials Verification Failed");
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					Helper.logInfoMessageV2(cclient.getmsisdn() + " - Verification Of Request Failed");
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				Helper.logInfoMessageV2(cclient.getmsisdn() + " - Invalid Msisdn");
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				if (!cclient.getChannel().equals("web")) {
					try {
						JSONObject jsonObject = new JSONObject();
						jsonObject.put("lang", cclient.getLang());
						String response = RestClient.SendCallToMagento(
								ConfigurationManager.getConfigurationFromCache("magento.app.storelocatorV2"),
								jsonObject.toString());
						Helper.logInfoMessageV2(cclient.getmsisdn() + " - Response from Magento:" + response);
						JSONObject bjectjson = new JSONObject(response);
						String magentocode = bjectjson.getString("resultCode");
						String magentomessage = bjectjson.getString("msg");
						if (magentocode.equals("711")) {
							resp.setReturnCode(magentocode);
							resp.setReturnMsg(magentomessage);
							return resp;
						}
						try {
							com.evampsaanga.bakcell.getstorelocator.MagentoResponse data = Helper.JsonToObject(response,
									com.evampsaanga.bakcell.getstorelocator.MagentoResponse.class);
							if (data.getResultCode().equals("280")) {
								resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
								resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
								resp.setData(data.getData());
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							} else {
								resp.setReturnCode(data.getResultCode());
								resp.setReturnMsg(data.getMsg());
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							}
						} catch (Exception ex) {
							loggerV2.error(Helper.GetException(ex));
							resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
							resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						} // end of (Exception ex)
					} catch (JSONException ex) {
						loggerV2.error(Helper.GetException(ex));
						resp.setReturnCode(ResponseCodes.MAGENTO_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.MAGENTO_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					} catch (Exception ex) {
						loggerV2.error(Helper.GetException(ex));
						resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
						resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				}
			} else {
				Helper.logInfoMessageV2(cclient.getmsisdn() + " - Access Not Authorized");
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		logs.updateLog(logs);
		return resp;
	}

	@POST
	@Path("/predefineddata")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetpreDefineDataResponseClient getPredefinedData(@Header("credentials") String credential,
			@Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.GET_PREDEFINED_DATA_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.GET_PREDEFINED_DATA);
		logs.setTableType(LogsType.PreDefineData);
		logger.info("request body for getPredefinedData  =" + requestBody);
		GetpreDefineDataRequestClient cclient = null;
		GetpreDefineDataResponseClient resp = new GetpreDefineDataResponseClient();
		try {
			cclient = Helper.JsonToObject(requestBody, GetpreDefineDataRequestClient.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setLang(cclient.getLang());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setIsB2B(cclient.getIsB2B());
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials.equals(Constants.CREDENTIALS)) {
				try {
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("lang", cclient.getLang());
					String response = RestClient.SendCallToMagento(
							ConfigurationManager.getConfigurationFromCache("magento.app.getpredefined"),
							jsonObject.toString());
					logger.info("<<<<<<<<<<<<< Response From Cache >>>>>>>>>>>>>" + response);
					try {
						com.evampsaanga.magento.getpredefineddata.MagentoResponse data = Helper.JsonToObject(response,
								com.evampsaanga.magento.getpredefineddata.MagentoResponse.class);
						if (data.getResultCode().equals("140")) {

							logger.debug("getFnFAllowed(cclient.getOfferingId())--->>>>  "
									+ getFnFAllowed(cclient.getOfferingId()));
							data.getData().getFnf().setFnFAllowed(getFnFAllowed(cclient.getOfferingId()));
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							resp.setData(data.getData());
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						} else {
							resp.setReturnCode(data.getResultCode());
							resp.setReturnMsg(data.getMsg());
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception ex) {
						logger.error(Helper.GetException(ex));
						resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					} // end of (Exception ex)
				} catch (JSONException ex) {
					logger.error(Helper.GetException(ex));
					resp.setReturnCode(ResponseCodes.MAGENTO_SERVER_ERROR_CODE);
					resp.setReturnMsg(ResponseCodes.MAGENTO_SERVER_ERROR_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				} catch (Exception ex) {
					logger.error(Helper.GetException(ex));
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		logs.updateLog(logs);
		return resp;
	}

	/**
	 * Predefined Data for phase 2
	 * 
	 * @param credential
	 * @param requestBody
	 * @return
	 */
	// Predefined Data For Phase 2
	@POST
	@Path("/predefineddataV2")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetPredefinedDataResponse getPredefinedDataV2(@Header("credentials") String credential,
			@Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.GET_PREDEFINED_DATA_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.GET_PREDEFINED_DATA);
		logs.setTableType(LogsType.PreDefineData);
		Helper.logInfoMessageV2("Request body for getPredefinedData V2  =" + requestBody);
		GetPredefinedDataRequest cclient = null;
		GetPredefinedDataResponseMagento respMag = new GetPredefinedDataResponseMagento();
		GetPredefinedDataResponse resp = new GetPredefinedDataResponse();
		try {
			cclient = Helper.JsonToObject(requestBody, GetPredefinedDataRequest.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setLang(cclient.getLang());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setIsB2B(cclient.getIsB2B());
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
					if (credentials == null) {
						Helper.logInfoMessageV2(cclient.getmsisdn() + " - Credentials Verification Failed");
						resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
						resp.setReturnMsg(ResponseCodes.ERROR_401);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
					if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
						String verification = Helper.validateRequest(cclient);
						if (!verification.equals("")) {
							Helper.logInfoMessageV2(cclient.getmsisdn() + " - Verification Of Request Failed");
							resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
							resp.setReturnMsg(verification);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						} else if (credentials.equals(Constants.CREDENTIALS)) {
							try {
								JSONObject jsonObject = new JSONObject();
								jsonObject.put("lang", cclient.getLang());
								String response = RestClient.SendCallToMagento(
										ConfigurationManager.getConfigurationFromCache("magento.app.getpredefinedV2"),
										jsonObject.toString());
								Helper.logInfoMessageV2(cclient.getmsisdn() + " - Response From Magento" + response);
								try {
									respMag = Helper.JsonToObject(response, GetPredefinedDataResponseMagento.class);
									if (respMag.getResultCode().equals("290") && respMag.getMsg().equals("Success")) {
										resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
										resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
										resp.setData(respMag.getData());
										logs.setResponseCode(resp.getReturnCode());
										logs.setResponseDescription(resp.getReturnMsg());
										logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
										logs.updateLog(logs);
										return resp;
									} else {
										resp.setReturnCode(respMag.getResultCode());
										resp.setReturnMsg(respMag.getMsg());
										resp.setData(respMag.getData());
										logs.setResponseCode(resp.getReturnCode());
										logs.setResponseDescription(resp.getReturnMsg());
										logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
										logs.updateLog(logs);
										return resp;
									}
								} catch (Exception e) {
									loggerV2.error(Helper.GetException(e));
									resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
									resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
									logs.setResponseCode(resp.getReturnCode());
									logs.setResponseDescription(resp.getReturnMsg());
									logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
									logs.updateLog(logs);
									return resp;
								}

							} catch (Exception e) {
								loggerV2.error(Helper.GetException(e));
								resp.setReturnCode(ResponseCodes.MAGENTO_SERVER_ERROR_CODE);
								resp.setReturnMsg(ResponseCodes.MAGENTO_SERVER_ERROR_DES);
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							}
						} else {
							Helper.logInfoMessageV2(cclient.getmsisdn() + " - Invalid Msisdn");
							resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
							resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} else {
						Helper.logInfoMessageV2(cclient.getmsisdn() + " - Invalid Msisdn");
						resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
						resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} catch (Exception ex) {
					loggerV2.error(Helper.GetException(ex));
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_400);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}

			}
		} catch (Exception ex) {
			loggerV2.error(Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		return resp;
	}

	@POST
	@Path("/mysubscriptionsofferingids")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public MySubscriptionsResponseClient GetMySubscriptionsOfferingIds(@Header("credentials") String credential,
			@Body() String requestBody) {
		logger.info("request body for GetMySubscriptionsOfferingIds =" + requestBody);
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.SUPPLEMENTARY_SERVICES_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.SUPPLEMENTARY_SERVICES);
		logs.setTableType(LogsType.SupplementaryServices);
		MySubscriptionsRequestClient cclient = null;
		MySubscriptionsResponseClient resp = new MySubscriptionsResponseClient();
		try {
			cclient = Helper.JsonToObject(requestBody, MySubscriptionsRequestClient.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
				logs.setIsB2B(cclient.getIsB2B());
			}
		} catch (Exception ex) {
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("lang", cclient.getLang());
					jsonObject.put("offeringIds", cclient.getOfferingIds());
					logger.info("Request from mysubscriptionsofferingids: " + jsonObject.toString());
					String response = RestClient.SendCallToMagento(
							ConfigurationManager.getConfigurationFromCache("magento.app.mySubscriptionsOfferingIds"),
							jsonObject.toString());
					logger.info("Response from mysubscriptionsofferingids Magento: " + response);
					try {
						SupplimentryMagentoResponse data = Helper.JsonToObject(response,
								SupplimentryMagentoResponse.class);
						if (data.getResultCode().equals("132")) {
							resp.setData(data.getData());
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
							logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						} else {
							resp.setReturnCode(data.getResultCode());
							resp.setReturnMsg(data.getMsg());
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception ex) {
						logger.error(Helper.GetException(ex));
						resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} catch (JSONException ex) {
					logger.error(Helper.GetException(ex));
					resp.setReturnCode(ResponseCodes.MAGENTO_SERVER_ERROR_CODE);
					resp.setReturnMsg(ResponseCodes.MAGENTO_SERVER_ERROR_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				} catch (Exception ex) {
					logger.error(Helper.GetException(ex));
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		logs.updateLog(logs);
		return resp;
	}

	/**
	 * Accept terms and conds for phase 2
	 * 
	 * @param credential
	 * @param requestBody
	 * @return
	 */
	// phase 2 terms and conditions accept
	@POST
	@Path("/acceptterms&conditions")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TnCResponse acceptTermsnConditions(@Header("credentials") String credential, @Body() String requestBody) {
		Helper.logInfoMessageV2("Request body for accept terms and conditions =" + requestBody);
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.SUPPLEMENTARY_SERVICES_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.SUPPLEMENTARY_SERVICES);
		logs.setTableType(LogsType.SupplementaryServices);
		TnCRequest tnCRequest = new TnCRequest();
		TnCResponse tnCResponse = new TnCResponse();
		try {
			String credentials = Decrypter.getInstance().decrypt(credential);
			if (credentials == null) {
				Helper.logInfoMessageV2("Credentails Verification Failed");
				tnCResponse.setReturnCode(ResponseCodes.ERROR_401_CODE);
				tnCResponse.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(tnCResponse.getReturnCode());
				logs.setResponseDescription(tnCResponse.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return tnCResponse;
			} else {
				tnCRequest = Helper.JsonToObject(requestBody, TnCRequest.class);
				if (tnCRequest != null && tnCRequest.getmsisdn() != null && !tnCRequest.getmsisdn().isEmpty()) {
					logs.setIp(tnCRequest.getiP());
					logs.setChannel(tnCRequest.getChannel());
					logs.setMsisdn(tnCRequest.getmsisdn());
					logs.setLang(tnCRequest.getLang());
					logs.setIsB2B(tnCRequest.getIsB2B());
					String verification = Helper.validateRequest(tnCResponse);
					if (!verification.equals("")) {
						Helper.logInfoMessageV2(tnCRequest.getmsisdn() + " - Verification of Request Failed");
						tnCResponse.setReturnCode(ResponseCodes.ERROR_400_CODE);
						tnCResponse.setReturnMsg(verification);
						logs.setResponseCode(tnCResponse.getReturnCode());
						logs.setResponseDescription(tnCResponse.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return tnCResponse;
					} else if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
						JSONObject jsonObject = new JSONObject();
						jsonObject.put("username", tnCRequest.getUserName());
						try {
							String response = RestClient.SendCallToMagento(
									ConfigurationManager.getConfigurationFromCache("magento.app.acceptTnCV2"),
									jsonObject.toString());
							Helper.logInfoMessageV2(tnCRequest.getmsisdn() + " - Response from Magento: " + response);
							JSONObject object = new JSONObject(response);
							if (object.getString("resultCode").equals("222")) {
								tnCResponse.setReturnCode(ResponseCodes.SUCESS_CODE_200);
								tnCResponse.setReturnMsg(ResponseCodes.SUCESS_DES_200);
								logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
								logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return tnCResponse;
							} else {
								tnCResponse.setReturnCode(object.getString("resultCode"));
								tnCResponse.setReturnMsg(object.getString("msg"));
								logs.setResponseCode(tnCResponse.getReturnCode());
								logs.setResponseDescription(tnCResponse.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return tnCResponse;
							}
						} catch (Exception e) {
							loggerV2.error(Helper.GetException(e));
							tnCResponse.setReturnCode(ResponseCodes.MAGENTO_SERVER_ERROR_CODE);
							tnCResponse.setReturnMsg(ResponseCodes.MAGENTO_SERVER_ERROR_DES);
							logs.setResponseCode(tnCResponse.getReturnCode());
							logs.setResponseDescription(tnCResponse.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return tnCResponse;
						}
					} else {
						tnCResponse.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
						tnCResponse.setReturnMsg(ResponseCodes.ERROR_MSISDN);
						logs.setResponseCode(tnCResponse.getReturnCode());
						logs.setResponseDescription(tnCResponse.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return tnCResponse;
					}

				}

			}
		} catch (Exception e) {
			loggerV2.error("Exception: ", e);
			tnCResponse.setReturnCode(ResponseCodes.ERROR_400_CODE);
			tnCResponse.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(tnCResponse.getReturnCode());
			logs.setResponseDescription(tnCResponse.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return tnCResponse;
		}
		return tnCResponse;

	}

	/**
	 * Get MySubscriptions Offering Ids of Pic for phase 2
	 * 
	 * @param credential
	 * @param requestBody
	 * @return
	 */
	// mysubscriptionsofferingids Phase 2
	@POST
	@Path("/mysubscriptionsofferingidsv2")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public MySubscriptionsResponseClient GetMySubscriptionsOfferingIdsV2(@Header("credentials") String credential,
			@Body() String requestBody) {
		Helper.logInfoMessageV2("Request body for GetMySubscriptionsOfferingIds =" + requestBody);
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.SUPPLEMENTARY_SERVICES_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.SUPPLEMENTARY_SERVICES);
		logs.setTableType(LogsType.SupplementaryServices);
		MySubscriptionsRequestClient cclient = null;
		MySubscriptionsResponseClient resp = new MySubscriptionsResponseClient();
		try {
			cclient = Helper.JsonToObject(requestBody, MySubscriptionsRequestClient.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
				logs.setIsB2B(cclient.getIsB2B());
			}
		} catch (Exception ex) {
			loggerV2.error("Exception: ", ex);
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				loggerV2.error(Helper.GetException(ex));
			}
			if (credentials == null) {
				Helper.logInfoMessageV2(cclient.getmsisdn() + " - Credentials Verification Failed");
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					Helper.logInfoMessageV2(cclient.getmsisdn() + " - Verification of Request Failed");
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				Helper.logInfoMessageV2(cclient.getmsisdn() + " - Invalid MSISDN");
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("lang", cclient.getLang());
					jsonObject.put("offeringIds", cclient.getOfferingIds());
					Helper.logInfoMessageV2(cclient.getmsisdn() + " - Request from mysubscriptionsofferingids: "
							+ jsonObject.toString());
					String response = RestClient.SendCallToMagento(
							ConfigurationManager.getConfigurationFromCache("magento.app.mySubscriptionsOfferingIds"),
							jsonObject.toString());
					Helper.logInfoMessageV2(
							cclient.getmsisdn() + " - Response from mysubscriptionsofferingids Magento: " + response);
					try {
						SupplimentryMagentoResponse data = Helper.JsonToObject(response,
								SupplimentryMagentoResponse.class);
						if (data.getResultCode().equals("132")) {
							resp.setData(data.getData());
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
							logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						} else {
							resp.setReturnCode(data.getResultCode());
							resp.setReturnMsg(data.getMsg());
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception ex) {
						loggerV2.error(Helper.GetException(ex));
						resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} catch (JSONException ex) {
					loggerV2.error(Helper.GetException(ex));
					resp.setReturnCode(ResponseCodes.MAGENTO_SERVER_ERROR_CODE);
					resp.setReturnMsg(ResponseCodes.MAGENTO_SERVER_ERROR_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				} catch (Exception ex) {
					loggerV2.error(Helper.GetException(ex));
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				Helper.logInfoMessageV2(cclient.getmsisdn() + " - Access Not Authorized");
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		logs.updateLog(logs);
		return resp;
	}
	// end mysubscriptionsofferingids Version2

	@POST
	@Path("/getroaming")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetRoamingCountriesResponse GetRoaming(@Header("credentials") String credential,
			@Body() String requestBody) {
		Helper.logInfoMessageV2("Request body for GetRoaming =" + requestBody);
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.GET_ROAMING_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.GET_ROAMING);
		logs.setTableType(LogsType.GetRoaming);
		GetRoamingCountriesRequest cclient = null;
		GetRoamingCountriesResponse resp = new GetRoamingCountriesResponse();
		try {
			cclient = Helper.JsonToObject(requestBody, GetRoamingCountriesRequest.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
				logs.setIsB2B(cclient.getIsB2B());
			}
		} catch (Exception ex) {
			loggerV2.error("Exception: ", ex);
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				loggerV2.error(Helper.GetException(ex));
			}
			if (credentials == null) {
				Helper.logInfoMessageV2(cclient.getmsisdn() + " - Credentials Verification Failed");
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					Helper.logInfoMessageV2(cclient.getmsisdn() + " - Verification of Request Failed");
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				Helper.logInfoMessageV2(cclient.getmsisdn() + " - Invalid MSISDN");
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("lang", cclient.getLang());
					jsonObject.put("brandName", cclient.getBrandName());

					Helper.logInfoMessageV2(
							cclient.getmsisdn() + " - Request from GetRoaming: " + jsonObject.toString());
					String response = RestClient.SendCallToMagento(
							ConfigurationManager.getConfigurationFromCache("magento.app.roamingcountries.url"),
							jsonObject.toString());
					// String response="{ \"resultCode\": 150, \"execTime\":
					// 0.45110082626343, \"msg\": \"from cache\", \"data\": {
					// \"countryList\": [ { \"country\": \"PakistanEn\",
					// \"country_id\": \"1\" }, { \"country\": \"AmericaEn\",
					// \"country_id\": \"2\" }, { \"country\": \"EnglandEn\",
					// \"country_id\": \"5\" } ], \"detailList\": [ {
					// \"country\": \"PakistanEn\", \"country_id\": \"1\",
					// \"operator_list\": [ { \"operator\": \"waridEN\",
					// \"service_list\": [ { \"service\": \"SMSEn\",
					// \"service_unit\": \"1 SMSEn\", \"service_type_list\": [ {
					// \"service_type\": \"InComingEn\", \"value\": \"0.25\",
					// \"unit\": \"manat\" }, { \"service_type\":
					// \"OutgoingEn\", \"value\": \"FREE\", \"unit\": null } ]
					// }, { \"service\": \"CallEn\", \"service_unit\": \"1
					// minuteEN\", \"service_type_list\": [ { \"service_type\":
					// \"OutgoingEn\", \"value\": \"1.99\", \"unit\": \"manat\"
					// }, { \"service_type\": \"InComingEn\", \"value\":
					// \"0.32\", \"unit\": \"manat\" } ] }, { \"service\":
					// \"InternetEn\", \"service_unit\": \"1 MBEn\",
					// \"service_type_list\": [ { \"service_type\": \"Download
					// and upload En\", \"value\": \"19.99\", \"unit\":
					// \"manat\" }, { \"service_type\": \"Network Mode EN\",
					// \"value\": \"2G, 3G\", \"unit\": null } ] } ] }, {
					// \"operator\": \"TelenorEn\", \"service_list\": [ {
					// \"service\": \"SMSEn\", \"service_unit\": \"1 SMSEn\",
					// \"service_type_list\": [ { \"service_type\":
					// \"InComingEn\", \"value\": \"3.0\", \"unit\": \"manat\"
					// }, { \"service_type\": \"OutgoingEn\", \"value\":
					// \"FREE\", \"unit\": null } ] }, { \"service\":
					// \"CallEn\", \"service_unit\": \"1 minuteEN\",
					// \"service_type_list\": [ { \"service_type\":
					// \"OutgoingEn\", \"value\": \"5.27\", \"unit\": \"manat\"
					// } ] }, { \"service\": \"InternetEn\", \"service_unit\":
					// \"1 MBEn\", \"service_type_list\": [ { \"service_type\":
					// \"Download and upload En\", \"value\": \"30.20\",
					// \"unit\": \"manat\" }, { \"service_type\": \"Network Mode
					// EN\", \"value\": \"2G, 3G\", \"unit\": null } ] } ] } ]
					// }, { \"country\": \"AmericaEn\", \"country_id\": \"2\",
					// \"operator_list\": [ { \"operator\": \"waridEN\",
					// \"service_list\": [ { \"service\": \"SMSEn\",
					// \"service_unit\": \"1 SMSEn\", \"service_type_list\": [ {
					// \"service_type\": \"InComingEn\", \"value\": \"0.25\",
					// \"unit\": \"manat\" }, { \"service_type\":
					// \"OutgoingEn\", \"value\": \"FREE\", \"unit\": null } ]
					// }, { \"service\": \"CallEn\", \"service_unit\": \"1
					// minuteEN\", \"service_type_list\": [ { \"service_type\":
					// \"OutgoingEn\", \"value\": \"1.99\", \"unit\": \"manat\"
					// }, { \"service_type\": \"InComingEn\", \"value\":
					// \"0.32\", \"unit\": \"manat\" } ] }, { \"service\":
					// \"InternetEn\", \"service_unit\": \"1 MBEn\",
					// \"service_type_list\": [ { \"service_type\": \"Download
					// and upload En\", \"value\": \"19.99\", \"unit\":
					// \"manat\" }, { \"service_type\": \"Network Mode EN\",
					// \"value\": \"2G, 3G\", \"unit\": null } ] } ] }, {
					// \"operator\": \"TelenorEn\", \"service_list\": [ {
					// \"service\": \"SMSEn\", \"service_unit\": \"1 SMSEn\",
					// \"service_type_list\": [ { \"service_type\":
					// \"InComingEn\", \"value\": \"0.25\", \"unit\": \"manat\"
					// }, { \"service_type\": \"OutgoingEn\", \"value\":
					// \"FREE\", \"unit\": null } ] }, { \"service\":
					// \"CallEn\", \"service_unit\": \"1 minuteEN\",
					// \"service_type_list\": [ { \"service_type\":
					// \"OutgoingEn\", \"value\": \"1.99\", \"unit\": \"manat\"
					// }, { \"service_type\": \"InComingEn\", \"value\":
					// \"0.32\", \"unit\": \"manat\" } ] }, { \"service\":
					// \"InternetEn\", \"service_unit\": \"1 MBEn\",
					// \"service_type_list\": [ { \"service_type\": \"Download
					// and upload En\", \"value\": \"19.99\", \"unit\":
					// \"manat\" }, { \"service_type\": \"Network Mode EN\",
					// \"value\": \"2G, 3G\", \"unit\": null } ] } ] } ] }, {
					// \"country\": \"EnglandEn\", \"country_id\": \"5\",
					// \"operator_list\": [ { \"operator\": \"waridEN\",
					// \"service_list\": [ { \"service\": \"SMSEn\",
					// \"service_unit\": \"1 SMSEn\", \"service_type_list\": [ {
					// \"service_type\": \"InComingEn\", \"value\": \"0.25\",
					// \"unit\": \"manat\" }, { \"service_type\":
					// \"OutgoingEn\", \"value\": \"FREE\", \"unit\": null } ]
					// }, { \"service\": \"CallEn\", \"service_unit\": \"1
					// minuteEN\", \"service_type_list\": [ { \"service_type\":
					// \"OutgoingEn\", \"value\": \"1.99\", \"unit\": \"manat\"
					// }, { \"service_type\": \"InComingEn\", \"value\":
					// \"0.32\", \"unit\": \"manat\" } ] }, { \"service\":
					// \"InternetEn\", \"service_unit\": \"1 MBEn\",
					// \"service_type_list\": [ { \"service_type\": \"Download
					// and upload En\", \"value\": \"19.99\", \"unit\":
					// \"manat\" }, { \"service_type\": \"Network Mode EN\",
					// \"value\": \"2G, 3G\", \"unit\": null } ] } ] }, {
					// \"operator\": \"TelenorEn\", \"service_list\": [ {
					// \"service\": \"SMSEn\", \"service_unit\": \"1 SMSEn\",
					// \"service_type_list\": [ { \"service_type\":
					// \"InComingEn\", \"value\": \"0.25\", \"unit\": \"manat\"
					// }, { \"service_type\": \"OutgoingEn\", \"value\":
					// \"FREE\", \"unit\": null } ] }, { \"service\":
					// \"CallEn\", \"service_unit\": \"1 minuteEN\",
					// \"service_type_list\": [ { \"service_type\":
					// \"OutgoingEn\", \"value\": \"0.32\", \"unit\": \"manat\"
					// } ] }, { \"service\": \"InternetEn\", \"service_unit\":
					// \"1 MBEn\", \"service_type_list\": [ { \"service_type\":
					// \"Download and upload En\", \"value\": \"19.99\",
					// \"unit\": \"manat\" }, { \"service_type\": \"Network Mode
					// EN\", \"value\": \"2G, 3G\", \"unit\": null } ] } ] } ] }
					// ] } }";

					Helper.logInfoMessageV2(cclient.getmsisdn() + " - Response from GetRoaming Magento: " + response);
					try {

						JSONObject responseobj = new JSONObject(response);
						Object dataObject = responseobj.get("data");

						if (dataObject instanceof JSONObject) {
							loggerV2.info("Yes Data is Object");
						}

						else {
							loggerV2.info("Data is String");

							JSONObject object = new JSONObject();
							object.put("data", responseobj.get("data"));
							responseobj.remove("data");
							responseobj.put("data", object);
						}

						RoamingCountriesMagentoResponse data = Helper.JsonToObject(responseobj.toString(),
								RoamingCountriesMagentoResponse.class);
						if (data.getResultCode().toString().equals("150")) {
							resp.setData(data.getData());
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
							logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						} else {
							resp.setReturnCode(data.getResultCode().toString());
							resp.setReturnMsg(data.getMsg());
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception ex) {
						loggerV2.error(Helper.GetException(ex));
						resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} catch (JSONException ex) {
					loggerV2.error(Helper.GetException(ex));
					resp.setReturnCode(ResponseCodes.MAGENTO_SERVER_ERROR_CODE);
					resp.setReturnMsg(ResponseCodes.MAGENTO_SERVER_ERROR_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				} catch (Exception ex) {
					loggerV2.error(Helper.GetException(ex));
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				Helper.logInfoMessageV2(cclient.getmsisdn() + " - Access Not Authorized");
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		logs.updateLog(logs);
		return resp;
	}

	// start of RoamingServices

	// ond of Roaming Services

	public String getFnFAllowed(String key) {
		return Boolean.toString(ConfigurationManager.getContainsValueByKey("fnf.offering.id.limit." + key));
	}

	/**
	 * Method accepts parameters and set them to response and logs object,
	 * defined to reuse same code for initialization of error response.
	 * 
	 * @param response,
	 *            GetCustomerDataResponse object which is to be returned to
	 *            client
	 * @param logs,
	 *            Logs object declared in controller method.
	 * @param returnCode,
	 *            Return code which is to be set as API response code.
	 * @param returnMessage,
	 *            Message to be set as API response description.
	 */
	public void prepareErrorResponse(LoginResponse response, Logs logs, String returnCode, String returnMessage) {
		response.setReturnCode(returnCode);
		response.setReturnMsg(returnMessage);
		logs.setResponseCode(response.getReturnCode());
		logs.setResponseDescription(response.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
	}

	/**
	 * Method accepts the GetCustomerRequestClient object constructed from
	 * request and performs validation. Validation is performed using
	 * ValidationRules {@link com.evampsaanga.validator.rules.ValidationRules}
	 * 
	 * @param Request
	 *            object which is to be validated
	 * @return Return the validation type object
	 *         {@link com.evampsaanga.validator.rules.ValidationResult}
	 */
	private ValidationResult validateRequest(LoginRequest client) {
		ValidationResult validationResult = new ValidationResult();
		validationResult.setValidationResult(true);
		// Validating all the fields against non empty rule
		validationResult = new MSISDNNotEmpty().validateObject(client.getmsisdn());
		validationResult = new ChannelNotEmpty().validateObject(client.getChannel());
		validationResult = new IPNotEmpty().validateObject(client.getiP());
		validationResult = new LangNotEmpty().validateObject(client.getLang());
		return validationResult;
	}

	/**
	 * api used to rate the app
	 * 
	 * @param credential
	 * @param requestBody
	 * @return
	 */
	// Rate Us api for phase 2
	@POST
	@Path("/rateus")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public RateUsResponse rateus(@Header("credentials") String credential, @Body() String requestBody) {
		Helper.logInfoMessageV2("Request body for RateUs =" + requestBody);
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.RATE_US);
		logs.setThirdPartyName(ThirdPartyNames.RATEUS);
		logs.setTableType(LogsType.RateUsService);
		RateUsRequest cclient = null;
		RateUsResponse resp = new RateUsResponse();
		try {
			cclient = Helper.JsonToObject(requestBody, RateUsRequest.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
				logs.setIsB2B(cclient.getIsB2B());
			}
		} catch (Exception ex) {
			loggerV2.error(Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				loggerV2.error(Helper.GetException(ex));
			}
			if (credentials == null) {
				Helper.logInfoMessageV2(cclient.getmsisdn() + " - Credentails Verification Failed");
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					Helper.logInfoMessageV2(cclient.getmsisdn() + " - Verification of Request Failed");
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				Helper.logInfoMessageV2(cclient.getmsisdn() + " - Invalid Msisdn");
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("msisdn", cclient.getmsisdn());
					jsonObject.put("channel", cclient.getChannel());
					Helper.logInfoMessageV2(cclient.getmsisdn() + " - Request from Rate Us: " + jsonObject.toString());
					String response = RestClient.SendCallToMagento(
							ConfigurationManager.getConfigurationFromCache("magento.app.rateus"),
							jsonObject.toString());
					Helper.logInfoMessageV2(cclient.getmsisdn() + " - Response from rateus Magento: " + response);
					try {
						RateUsResponseData data = Helper.JsonToObject(response, RateUsResponseData.class);
						if (data.getResultCode().equals("303")) {
							resp.setData(data);
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
							logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						} else {
							resp.setReturnCode(data.getResultCode());
							resp.setReturnMsg(data.getMsg());
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception ex) {
						loggerV2.error(Helper.GetException(ex));
						resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} catch (JSONException ex) {
					loggerV2.error(Helper.GetException(ex));
					resp.setReturnCode(ResponseCodes.MAGENTO_SERVER_ERROR_CODE);
					resp.setReturnMsg(ResponseCodes.MAGENTO_SERVER_ERROR_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				} catch (Exception ex) {
					loggerV2.error(Helper.GetException(ex));
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				Helper.logInfoMessageV2(cclient.getmsisdn() + " - Access Not Authorized");
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		logs.updateLog(logs);
		return resp;

	}

	public ArrayList<Offers> removingTM(SupplimentryMagentoResponse responsedata, SupplementryServicesRequest cclient) {
		ArrayList<Offers> offers = new ArrayList<Offers>();
		com.huawei.crm.query.GetSubscriberResponse respns = new com.evampsaanga.bakcell.getsubscriber.CRMSubscriberService()
				.GetSubscriberRequest(cclient.getmsisdn());

		logger.info("<<<<<<<<<<<<<<<< GetSubscriberResponse >>>>>>>>>>>>>>>>" + respns.toString());
		if (respns.getResponseHeader().getRetCode().equals("0")) { // if
																	// GetSubscriberResponse
																	// is OK

			ArrayList<String> offeringId = new ArrayList<String>();

			// if1, checking whether we have any Supplimentry Offerings for this
			// customer from bakcell or not.
			if (respns.getGetSubscriberBody().getSupplementaryOfferingList().getGetSubOfferingInfo() != null && !respns
					.getGetSubscriberBody().getSupplementaryOfferingList().getGetSubOfferingInfo().isEmpty()) {
				for (int i = 0; i < respns.getGetSubscriberBody().getSupplementaryOfferingList().getGetSubOfferingInfo()
						.size(); i++) {
					offeringId.add(respns.getGetSubscriberBody().getSupplementaryOfferingList().getGetSubOfferingInfo()
							.get(i).getOfferingId().getOfferingId());
				}
			} else // else of if1,
			{
				// returning empty Offers to the TM array because user doesn't
				// have any Supplimentry Offering activated from bakcell
				return offers;
			}

			logger.info("Activated,  offeringIDs FROM BAKCELL: " + offeringId);
			if (responsedata.getData() != null && responsedata.getData().getTM() != null
					&& responsedata.getData().getTM().getOffers() != null)
				for (int j = 0; j < responsedata.getData().getTM().getOffers().size(); j++) {
					logger.info("---PREREQ_ID IN LOOP FROM MAGENTO: "
							+ responsedata.getData().getTM().getOffers().get(j).getHeader().getPreReqOfferId());
					if (offeringId.contains(
							responsedata.getData().getTM().getOffers().get(j).getHeader().getPreReqOfferId())) {

						offers.add(responsedata.getData().getTM().getOffers().get(j));
						logger.info("---IN IF  OFFER ACTIVATED---");

					} else {
						logger.info("---ELSE OFFER NOT ACTIVATED---");

					}
				}

		}

		return offers;
	}

	public ArrayList<DocumentTypesData> getDocumentTypesList(LoginRequest request) {
		String names = ConfigurationManager.getConfigurationFromCache("sim.swap.document.ids");
		String[] values = names.split(",");
		ArrayList<DocumentTypesData> list = new ArrayList<DocumentTypesData>();
		for (int i = 0; i < values.length; i++) {
			DocumentTypesData documentType = new DocumentTypesData();
			documentType.setId(values[i]);
			documentType.setValue(ConfigurationManager
					.getConfigurationFromCache("sim.swap.documenttype." + values[i] + "." + request.getLang()));
			list.add(documentType);

		}
		return list;
	}

	// public static void main(String[] args) throws Exception {
	//
	// String roaminjson = "{ \"resultCode\": 150, \"execTime\":
	// 0.37665510177612, \"msg\": \"From Database\", \"data\": {
	// \"countryList\": [ { \"countryName\": \"PakistanEn\", \"countryCode\":
	// \"1\" }, { \"countryName\": \"AmericaEn\", \"countryCode\": \"2\" }, {
	// \"countryName\": \"EnglandEn\", \"countryCode\": \"5\" } ],
	// \"detailList\": [ { \"operator\": \"waridEN\", \"countryCode\": \"1\",
	// \"service\": { \"priceValue\": \"0.32\", \"priceUnit\": \"manat\",
	// \"serviceType\": \"InComingEn\", \"service\": \"CallEn\",
	// \"serviceUnit\": \"1 minuteEN\", \"countryCode\": \"1\" } }, {
	// \"operator\": \"waridEN\", \"countryCode\": \"1\", \"service\": {
	// \"priceValue\": \"0.25\", \"priceUnit\": \"manat\", \"serviceType\":
	// \"InComingEn\", \"service\": \"SMSEn\", \"serviceUnit\": \"1 SMSEn\",
	// \"countryCode\": \"1\" } }, { \"operator\": \"TelenorEn\",
	// \"countryCode\": \"1\", \"service\": { \"priceValue\": \"1.99\",
	// \"priceUnit\": \"manat\", \"serviceType\": \"OutgoingEn\", \"service\":
	// \"CallEn\", \"serviceUnit\": \"1 minuteEN\", \"countryCode\": \"1\" } },
	// { \"operator\": \"TelenorEn\", \"countryCode\": \"1\", \"service\": {
	// \"priceValue\": \"0.32\", \"priceUnit\": \"manat\", \"serviceType\":
	// \"OutgoingEn\", \"service\": \"CallEn\", \"serviceUnit\": \"1 minuteEN\",
	// \"countryCode\": \"1\" } }, { \"operator\": \"waridEN\", \"countryCode\":
	// \"2\", \"service\": { \"priceValue\": \"0.32\", \"priceUnit\": \"manat\",
	// \"serviceType\": \"InComingEn\", \"service\": \"CallEn\",
	// \"serviceUnit\": \"1 minuteEN\", \"countryCode\": \"2\" } }, {
	// \"operator\": \"waridEN\", \"countryCode\": \"2\", \"service\": {
	// \"priceValue\": \"0.25\", \"priceUnit\": \"manat\", \"serviceType\":
	// \"InComingEn\", \"service\": \"SMSEn\", \"serviceUnit\": \"1 SMSEn\",
	// \"countryCode\": \"2\" } }, { \"operator\": \"TelenorEn\",
	// \"countryCode\": \"2\", \"service\": { \"priceValue\": \"0.32\",
	// \"priceUnit\": \"manat\", \"serviceType\": \"InComingEn\", \"service\":
	// \"CallEn\", \"serviceUnit\": \"1 minuteEN\", \"countryCode\": \"2\" } },
	// { \"operator\": \"TelenorEn\", \"countryCode\": \"2\", \"service\": {
	// \"priceValue\": \"0.25\", \"priceUnit\": \"manat\", \"serviceType\":
	// \"InComingEn\", \"service\": \"SMSEn\", \"serviceUnit\": \"1 SMSEn\",
	// \"countryCode\": \"2\" } }, { \"operator\": \"waridEN\", \"countryCode\":
	// \"5\", \"service\": { \"priceValue\": \"0.32\", \"priceUnit\": \"manat\",
	// \"serviceType\": \"InComingEn\", \"service\": \"CallEn\",
	// \"serviceUnit\": \"1 minuteEN\", \"countryCode\": \"5\" } }, {
	// \"operator\": \"waridEN\", \"countryCode\": \"5\", \"service\": {
	// \"priceValue\": \"0.25\", \"priceUnit\": \"manat\", \"serviceType\":
	// \"InComingEn\", \"service\": \"SMSEn\", \"serviceUnit\": \"1 SMSEn\",
	// \"countryCode\": \"5\" } }, { \"operator\": \"TelenorEn\",
	// \"countryCode\": \"5\", \"service\": { \"priceValue\": \"19.99\",
	// \"priceUnit\": \"manat\", \"serviceType\": \"Download and upload En\",
	// \"service\": \"InternetEn\", \"serviceUnit\": \"1 MBEn\",
	// \"countryCode\": \"5\" } }, { \"operator\": \"TelenorEn\",
	// \"countryCode\": \"5\", \"service\": { \"priceValue\": \"2G, 3G\",
	// \"priceUnit\": null, \"serviceType\": \"Network Mode EN\", \"service\":
	// \"InternetEn\", \"serviceUnit\": \"1 MBEn\", \"countryCode\": \"5\" } } ]
	// } }";
	//
	// // ArrayList<Service> services=new ArrayList<Service>();
	// /*
	// * ArrayList<DetailsList> detailsList=new ArrayList<DetailsList>();
	// * RoamingCountriesMagentoResponse test=Helper.JsonToObject(roaminjson,
	// * RoamingCountriesMagentoResponse.class);
	// *
	// *
	// * System.out.println("FROM MAGENTOP:" +test.getMsg());
	// *
	// *
	// * for(CountryList countrylist: test.getData().getCountryList()) {
	// * DetailsList detailsObj=new DetailsList(); String
	// * countryListCode=countrylist.getCountryCode();
	// * detailsObj.setCountrycode(countrylist.getCountryCode());
	// *
	// * ArrayList<Operators> operators=new ArrayList<Operators>();
	// * for(DetailList iterateDetailsList: test.getData().getDetailList()) {
	// * if(countryListCode.equalsIgnoreCase(iterateDetailsList.getCountryCode
	// * ())) {
	// *
	// * ArrayList<Service> services=new ArrayList<Service>(); Operators
	// * operator=new Operators(); Service serv=new Service();
	// *
	// * services.add(iterateDetailsList.getService());
	// *
	// * operator.setOperator(iterateDetailsList.getOperator());
	// * operator.setServices(services);
	// *
	// *
	// *
	// * operators.add(operator);
	// *
	// *
	// * }
	// *
	// *
	// * } detailsObj.setOperators(operators); detailsList.add(detailsObj); }
	// *
	// * System.out.println("OperatoreList : "+
	// * Helper.ObjectToJson(detailsList));
	// *
	// * for(int i=0;i<detailsList.size();i++) {
	// *
	// * ArrayList<Operators> opr=new ArrayList<Operators>(); for(int
	// * j=0;j<detailsList.get(i).getOperators().size();j++) { Operators
	// * operat=new Operators();
	// * System.out.println("OperatorName :"+detailsList.get(i).getOperators()
	// * .get(j).getOperator()); if(opr!=null && !opr.isEmpty()) {
	// * if(opr.contains(detailsList.get(i).getOperators().get(j).getOperator(
	// * )) ) { operat.setOperator(detailsList.get(i).getOperators().get(j).
	// * getOperator()); //operat.setServices(); }
	// *
	// * } else { operat.setOperator(detailsList.get(i).getOperators().get(j).
	// * getOperator());
	// * operat.setServices(detailsList.get(i).getOperators().get(j).
	// * getServices()); }
	// *
	// * } }
	// */
	//
	// }
	public static void main(String[] args) {
		new MagentoServices().GetAppMenu("RQ/Rjcf8Gn2+7srAiXNO0ZGuRnJORSir",
				"{\"lang\":\"3\",\"iP\":\"10.220.217.70\",\"channel\":\"android\",\"msisdn\":\"554972937\",\"isB2B\":\"\"}");

	}

}