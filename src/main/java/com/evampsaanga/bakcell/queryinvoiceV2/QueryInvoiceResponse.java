package com.evampsaanga.bakcell.queryinvoiceV2;

import java.util.List;
import com.evampsaanga.bakcell.responseheaders.BaseResponse;

/*@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "InvoiceAmount", "InvoiceDate", "DueDate" })
@XmlRootElement(name = "QueryInvoiceResultMsg")*/
public class QueryInvoiceResponse extends BaseResponse {

	private List<QueryInvoiceResponseData> queryInvoiceResponseData;

	public List<QueryInvoiceResponseData> getQueryInvoiceResponseData() {
		return queryInvoiceResponseData;
	}

	public void setQueryInvoiceResponseData(List<QueryInvoiceResponseData> queryInvoiceResponseData) {
		this.queryInvoiceResponseData = queryInvoiceResponseData;
	}

}
