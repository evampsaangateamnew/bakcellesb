package com.evampsaanga.bakcell.roamingcountriesdata;

import com.evampsaanga.bakcell.requestheaders.BaseRequest;

public class GetRoamingCountriesRequest extends BaseRequest {
	
	private String brandName="";

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

}
