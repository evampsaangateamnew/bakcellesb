package com.evampsaanga.bakcell.queryloanrequesthistory;

import com.evampsaanga.bakcell.requestheaders.BaseRequest;

public class LoanRequestHistoryClientRequest extends BaseRequest {
	private String startDate = "";
	private String endDate = "";

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
}
