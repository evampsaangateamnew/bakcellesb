
package com.evampsaanga.companyinvoice;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "BillSubItemCodes"
})
public class CompanyDetail {

    @JsonProperty("BillSubItemCodes")
    private List<BillSubItemCode> billSubItemCodes = null;

    @JsonProperty("BillSubItemCodes")
    public List<BillSubItemCode> getBillSubItemCodes() {
        return billSubItemCodes;
    }

    @JsonProperty("BillSubItemCodes")
    public void setBillSubItemCodes(List<BillSubItemCode> billSubItemCodes) {
        this.billSubItemCodes = billSubItemCodes;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("billSubItemCodes", billSubItemCodes).toString();
    }

}
