package com.evampsaanga.bakcell.plasticcard.posting.payment.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "body")
@XmlAccessorType(XmlAccessType.FIELD)
public class PaymentResponseBody {
	public PaymentResponse paymentresponse;

	public PaymentResponse getPaymentresponse() {
		return paymentresponse;
	}

	public void setPaymentresponse(PaymentResponse paymentresponse) {
		this.paymentresponse = paymentresponse;
	}

	@Override
	public String toString() {
		return "PaymentResponseBody [paymentresponse=" + paymentresponse + "]";
	}
	
}
