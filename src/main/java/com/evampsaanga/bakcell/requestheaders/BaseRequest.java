package com.evampsaanga.bakcell.requestheaders;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonInclude;

public class BaseRequest 
{
	// msisdn
	@XmlElement(name = "channel", required = true)
	private String channel = "";
	@XmlElement(name = "lang", required = true)
	private String lang = "";
	@XmlElement(name = "msisdn", required = true)
	private String msisdn = "";
	@XmlElement(name = "iP", required = true)
	private String iP = "";
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@XmlElement(name = "isB2B", required = false)
	private String isB2B;

	public BaseRequest(String channel, String lang, String msisdn, String iP) {
		super();
		this.channel = channel;
		this.lang = lang;
		this.msisdn = msisdn;
		this.iP = iP;
	}
	public BaseRequest(String channel, String lang, String msisdn, String iP,String isB2B)
	{
		super();
		this.channel = channel;
		this.lang = lang;
		this.msisdn = msisdn;
		this.iP = iP;
		this.isB2B = isB2B;
	}
	

	
	
	public String getIsB2B() {
		return isB2B;
	}
	public void setIsB2B(String isB2B) {
		this.isB2B = isB2B;
	}
	/**
	 * @return the lang
	 */
	public String getLang() {
		return lang;
	}

	/**
	 * @param lang
	 *            the lang to set
	 */
	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getmsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	/**
	 * @return the channel
	 */
	public String getChannel() {
		return channel;
	}

	/**
	 * @param channel
	 *            the channel to set
	 */
	public void setChannel(String channel) {
		this.channel = channel;
	}

	public BaseRequest() {
		super();
	}

	/**
	 * Gets the value of the iP property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getiP() {
		return iP;
	}

	/**
	 * Sets the value of the iP property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setiP(String iP) {
		this.iP = iP;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BaseRequest [channel=" + channel + ", lang=" + lang + ", msisdn=" + msisdn + ", iP=" + iP + "]";
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
}
