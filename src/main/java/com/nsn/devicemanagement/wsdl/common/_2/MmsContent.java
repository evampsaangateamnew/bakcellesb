package com.nsn.devicemanagement.wsdl.common._2;

import javax.activation.DataHandler;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlMimeType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for MmsContent complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="MmsContent">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MultiMediaPartId" type="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}ContentId" minOccurs="0"/>
 *         &lt;element name="mmPartData" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="messageText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mmPartMeta" type="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}ContentMeta" minOccurs="0"/>
 *         &lt;element name="subject" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MmsContent", propOrder = { "multiMediaPartId", "mmPartData", "messageText", "mmPartMeta", "subject" })
public class MmsContent {
	@XmlElement(name = "MultiMediaPartId")
	protected ContentId multiMediaPartId;
	@XmlMimeType("application/octet-stream")
	protected DataHandler mmPartData;
	protected String messageText;
	protected ContentMeta mmPartMeta;
	protected String subject;

	/**
	 * Gets the value of the multiMediaPartId property.
	 * 
	 * @return possible object is {@link ContentId }
	 * 
	 */
	public ContentId getMultiMediaPartId() {
		return multiMediaPartId;
	}

	/**
	 * Sets the value of the multiMediaPartId property.
	 * 
	 * @param value
	 *            allowed object is {@link ContentId }
	 * 
	 */
	public void setMultiMediaPartId(ContentId value) {
		this.multiMediaPartId = value;
	}

	/**
	 * Gets the value of the mmPartData property.
	 * 
	 * @return possible object is {@link DataHandler }
	 * 
	 */
	public DataHandler getMmPartData() {
		return mmPartData;
	}

	/**
	 * Sets the value of the mmPartData property.
	 * 
	 * @param value
	 *            allowed object is {@link DataHandler }
	 * 
	 */
	public void setMmPartData(DataHandler value) {
		this.mmPartData = value;
	}

	/**
	 * Gets the value of the messageText property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMessageText() {
		return messageText;
	}

	/**
	 * Sets the value of the messageText property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMessageText(String value) {
		this.messageText = value;
	}

	/**
	 * Gets the value of the mmPartMeta property.
	 * 
	 * @return possible object is {@link ContentMeta }
	 * 
	 */
	public ContentMeta getMmPartMeta() {
		return mmPartMeta;
	}

	/**
	 * Sets the value of the mmPartMeta property.
	 * 
	 * @param value
	 *            allowed object is {@link ContentMeta }
	 * 
	 */
	public void setMmPartMeta(ContentMeta value) {
		this.mmPartMeta = value;
	}

	/**
	 * Gets the value of the subject property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * Sets the value of the subject property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSubject(String value) {
		this.subject = value;
	}
}
