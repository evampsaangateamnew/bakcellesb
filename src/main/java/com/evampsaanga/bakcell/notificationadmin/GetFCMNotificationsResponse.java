package com.evampsaanga.bakcell.notificationadmin;

import java.util.List;

import com.evampsaanga.bakcell.responseheaders.BaseResponse;

public class GetFCMNotificationsResponse extends BaseResponse {
	private List<FCMNotification> fcmNotifications;

	public List<FCMNotification> getFcmNotifications() {
		return fcmNotifications;
	}

	public void setFcmNotifications(List<FCMNotification> fcmNotifications) {
		this.fcmNotifications = fcmNotifications;
	}

	@Override
	public String toString() {
		return "GetFCMNotificationsResponse [fcmNotifications=" + fcmNotifications + ", toString()=" + super.toString()
				+ "]";
	}

}
