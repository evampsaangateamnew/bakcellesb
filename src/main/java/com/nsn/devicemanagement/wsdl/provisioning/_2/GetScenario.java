package com.nsn.devicemanagement.wsdl.provisioning._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import com.nsn.devicemanagement.wsdl.common._2.Device;
import com.nsn.devicemanagement.wsdl.common._2.Subscription;

/**
 * <p>
 * Java class for getScenario complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="getScenario">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="scenarioName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="subscription" type="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}Subscription" minOccurs="0"/>
 *         &lt;element name="device" type="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}Device" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getScenario", propOrder = { "scenarioName", "subscription", "device" })
public class GetScenario {
	protected String scenarioName;
	protected Subscription subscription;
	protected Device device;

	/**
	 * Gets the value of the scenarioName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getScenarioName() {
		return scenarioName;
	}

	/**
	 * Sets the value of the scenarioName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setScenarioName(String value) {
		this.scenarioName = value;
	}

	/**
	 * Gets the value of the subscription property.
	 * 
	 * @return possible object is {@link Subscription }
	 * 
	 */
	public Subscription getSubscription() {
		return subscription;
	}

	/**
	 * Sets the value of the subscription property.
	 * 
	 * @param value
	 *            allowed object is {@link Subscription }
	 * 
	 */
	public void setSubscription(Subscription value) {
		this.subscription = value;
	}

	/**
	 * Gets the value of the device property.
	 * 
	 * @return possible object is {@link Device }
	 * 
	 */
	public Device getDevice() {
		return device;
	}

	/**
	 * Sets the value of the device property.
	 * 
	 * @param value
	 *            allowed object is {@link Device }
	 * 
	 */
	public void setDevice(Device value) {
		this.device = value;
	}
}
