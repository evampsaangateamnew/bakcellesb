package com.evampsaanga.companyinvoice;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.DoubleToIntFunction;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.jca.cci.connection.CciLocalTransactionManager;

import com.cloudcontrolled.api.response.Response;
import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.amqimplementationsesb.LogsType;
import com.evampsaanga.bakcell.db.DBBakcellFactory;
import com.evampsaanga.bakcell.getappfaq.GetAppFaqRequest;
import com.evampsaanga.bakcell.getappfaq.GetAppFaqResponse;
import com.evampsaanga.bakcell.getappfaq.MagentoResponse;
import com.evampsaanga.bakcell.getappfaq.MagentoResponseV2;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.ThirdPartyNames;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.developer.utils.SOAPLoggingHandler;
import com.saanga.magento.apiclient.RestClient;


@Path("/bakcell")
public class CompanyInvoice {
	

	public static final Logger logger = Logger.getLogger("bakcellLogs-V2");
	public static final Logger loggerV2 = Logger.getLogger("bakcellLogs-V2");
    public static String modeulName="COMPANY_INVOICE";
	@POST
	@Path("/getsummary")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public CompanyInvoiceResponse getSummary(@Body String requestBody, @Header("credentials") String credential) {
		logMessage("***************************** Company Invoice Summary **********************************");
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.COMPANY_INVOICE);
		logs.setThirdPartyName(ThirdPartyNames.ODS);
		logs.setTableType(LogsType.CompanyInvoice);
		CompanyInvoiceRequest cclient = null;
		CompanyInvoiceResponse resp = new CompanyInvoiceResponse();
		try {
			cclient = Helper.JsonToObject(requestBody, CompanyInvoiceRequest.class);
			logMessage(cclient.getmsisdn()+"-REQUEST: "+Helper.ObjectToJson(cclient));
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
				logs.setIsB2B(cclient.getIsB2B());
			}
		} catch (Exception ex) {
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_401);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {
					logMessage(cclient.getmsisdn()+"=============Fetching Invoice Id and Bill Month =================");
					////////////////////////////////// HardCored value for testing //////////////////////
					
//					cclient.setBillMonth("-4");
					///////////////////////////////////////////////////////////////////////////
					FetchInvoiceAndBillMonth fetchInvoiceAndBillMonth = new FetchInvoiceAndBillMonth();
					fetchInvoiceAndBillMonth.setInvoceidAndBillMonth(cclient.getAcctCode(), cclient.getBillMonth(), cclient.getLang());
					
					if(fetchInvoiceAndBillMonth.getBillMonth()!=null && fetchInvoiceAndBillMonth.getInvoiceId()!=null)
					{
						logMessage(cclient.getmsisdn()+"============= Fetching Summary from ODS =================");
						resp = getSummaryFromODS(cclient.getAcctCode(),cclient.getBillMonth(),cclient.getmsisdn());
					}
					else
					{
						resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
						resp.setReturnMsg("INVOICE_ID or Bill Month is missing");
					
					}
					
//					CompanySummary companySummary = new CompanySummary();
//					companySummary.setChargeClose("10");
//					companySummary.setChargeOpen("21");
//					companySummary.setNewCharge("34");
//					companySummary.setNewCredit("32");
//					companySummary.setTotalDiscount("100");
//					resp.setCompanySummary(companySummary);
//					resp.setReturnCode("200");
//					resp.setReturnMsg("success");
//					resp = Helper.JsonToObject(responseSummary, CompanyInvoiceResponse.class);
					logMessage(cclient.getmsisdn()+"============= Response to App Server and WEB =================");
					logMessage(cclient.getmsisdn()+"RESPONSE: "+Helper.ObjectToJson(resp));
					
				
				} catch (Exception ex) {
					loggerV2.info(Helper.GetException(ex));
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
//		logs.updateLog(logs);
		
		return resp;
	}

	private CompanyInvoiceResponse getSummaryFromODS(String acctCode, String billMonth,String msisdn) throws ParseException {
		CompanyInvoiceResponse companyInvoiceResponse = new CompanyInvoiceResponse();
		CompanySummary companySummary = new CompanySummary();
		String query = "select CHARGE_OPEN,NEW_CREDIT,NEW_CHARGE,CHARGE_CLOSE,TOTAL_DISCOUNT,TOTAL_TAX from V_E_CARE_BILL_INVOICE t where ACCT_CODE = '"+acctCode+"' and BILL_MONTH = to_date('"+Helper.getDateOfLastMonth(billMonth)+"','yyyymmdd')";
		try {
			logMessage(msisdn+"Query: " + query);
			ResultSet rs = DBBakcellFactory.getConnection().prepareStatement(query).executeQuery();
			while (rs.next()) 
			{
				companySummary.setChargeClose(String.valueOf(Helper.roundUptoDecimal(Double.parseDouble(rs.getString("CHARGE_CLOSE")), 2)));
				companySummary.setChargeOpen(String.valueOf(Helper.roundUptoDecimal(Double.parseDouble(rs.getString("CHARGE_OPEN")), 2)));
				companySummary.setNewCredit(String.valueOf(Helper.roundUptoDecimal(Double.parseDouble(rs.getString("NEW_CREDIT")), 2)));
				companySummary.setNewCharge(String.valueOf(Helper.roundUptoDecimal(Double.parseDouble(rs.getString("NEW_CHARGE")), 2)));
				companySummary.setTotalDiscount(String.valueOf(Helper.roundUptoDecimal(Double.parseDouble(rs.getString("CHARGE_CLOSE")), 2)));
				companySummary.setTotalTax(String.valueOf(Helper.roundUptoDecimal(Double.parseDouble(rs.getString("TOTAL_TAX")), 2)));
			}
		} catch (SQLException e) {
			logger.info("ERROR: ", e);
			companyInvoiceResponse.setCompanySummary(companySummary);
			companyInvoiceResponse.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
			companyInvoiceResponse.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
		}
		logMessage(msisdn+"");
		companyInvoiceResponse.setCompanySummary(companySummary);
		companyInvoiceResponse.setReturnCode(ResponseCodes.SUCESS_CODE_200);
		companyInvoiceResponse.setReturnMsg(ResponseCodes.SUCESS_DES_200);
		return companyInvoiceResponse;
	}


	@POST
	@Path("/getdetails")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public CompanyInvoiceResponse getDetails(@Body String requestBody, @Header("credentials") String credential) {
		logMessage("***************************** Company Invoice Detail **********************************");
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.COMPANY_INVOICE);
		logs.setThirdPartyName(ThirdPartyNames.ODS);
		logs.setTableType(LogsType.CompanyInvoice);
		CompanyInvoiceRequest cclient = null;
		CompanyInvoiceResponse resp = new CompanyInvoiceResponse();
		try {
			cclient = Helper.JsonToObject(requestBody, CompanyInvoiceRequest.class);
			logMessage(cclient.getmsisdn()+"REQUEST: "+Helper.ObjectToJson(cclient));
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
				logs.setIsB2B(cclient.getIsB2B());
			}
		} catch (Exception ex) {
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_401);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
//			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
//				String verification = Helper.validateRequest(cclient);
//				if (!verification.equals("")) {
//					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
//					resp.setReturnMsg(verification);
//					logs.setResponseCode(resp.getReturnCode());
//					logs.setResponseDescription(resp.getReturnMsg());
//					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
//					logs.updateLog(logs);
//					return resp;
//				}
//			} else {
//				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
//				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
//				logs.setResponseCode(resp.getReturnCode());
//				logs.setResponseDescription(resp.getReturnMsg());
//				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
//				logs.updateLog(logs);
//				return resp;
//			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {
					
					if(cclient.getAcctCode()!=null && !cclient.getAcctCode().isEmpty())
					{
						logMessage(cclient.getmsisdn()+"============= Fetching Invoice Id and Bill Month =================");
//						////////////////////////////////HardCored value for testing //////////////////////
						
//						cclient.setBillMonth("-4");
						///////////////////////////////////////////////////////////////////////////
						FetchInvoiceAndBillMonth fetchInvoiceAndBillMonth = new FetchInvoiceAndBillMonth();
						fetchInvoiceAndBillMonth.setInvoceidAndBillMonth(cclient.getAcctCode(), cclient.getBillMonth(), cclient.getLang());
						if(fetchInvoiceAndBillMonth.getBillMonth()!=null && fetchInvoiceAndBillMonth.getInvoiceId()!=null)
						{
							logMessage(cclient.getmsisdn()+"============= Fetching Details from ODS =================");
							resp = getDetailsFromODS(fetchInvoiceAndBillMonth.getInvoiceId(),cclient.getBillMonth(),cclient.getLang(),cclient.getmsisdn());
						}
						else
						{
							resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
							resp.setReturnMsg("INVOICE_ID or Bill Month is missing");
						}
							
					}
					else
					{
						resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
						resp.setReturnMsg("Account Code is missing");
					}
//					resp.setReturnCode("200");
//					resp.setReturnMsg("success");
//					CompanyDetail companyDetail = new CompanyDetail();
//					ArrayList<BillSubItemCode> billSubItemCodes = new ArrayList<BillSubItemCode>();
//					BillSubItemCode billSubItemCode = new BillSubItemCode();
//					Details details = new Details();
//					details.setDetailDesc("15");
//					details.setDetailInvoice("23");
//					details.setDetailTotal("43");
//					details.setDetailUnit("KB");
//					details.setDetailUsage("34");
//					details.setSubitemTitle("Internet");
//					
//					billSubItemCode.setDetails(details);
//					billSubItemCodes.add(billSubItemCode);
//					companyDetail.setBillSubItemCodes(billSubItemCodes);
//					resp.setCompanyDetail(companyDetail);
						
					logMessage(cclient.getmsisdn()+"============= Response to App Server and WEB =================");
					logMessage(cclient.getmsisdn()+"RESPONSE: "+Helper.ObjectToJson(resp));
					
				
				} catch (Exception ex) {
					loggerV2.info(Helper.GetException(ex));
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
//		logs.updateLog(logs);
		
		logMessage(cclient.getmsisdn()+"***************************************************************");
		return resp;
	}
	private CompanyInvoiceResponse getDetailsFromODS(String invoiceId, String billMonth,String lang,String msisdn) throws ParseException {

		CompanyInvoiceResponse companyInvoiceResponse = new CompanyInvoiceResponse();
		CompanyDetail companyDetail = new CompanyDetail();
		
		ArrayList<BillSubItemCode> billSubItemCodes = new ArrayList<BillSubItemCode>();
		String query = "select INVOICE,DISC,TOTAL,USAGE,BILL_SUBITEM_CODE from V_E_CARE_BILL_CHARGE t where INVOICE_ID = '"+invoiceId+"' and BILL_MONTH = to_date('"+Helper.getDateOfLastMonth(billMonth)+"','yyyymmdd')";
		try {
			logMessage(msisdn+"Query: " + query);
			ResultSet rs = DBBakcellFactory.getConnection().prepareStatement(query).executeQuery();
			while (rs.next()) 
			{
				BillSubItemCode billSubItemCode = new BillSubItemCode();
				Details details = new Details();
				logMessage("INVOICE "+rs.getFloat("INVOICE"));
				details.setDetailInvoice(String.valueOf(Helper.roundUptoDecimal(Double.parseDouble(rs.getString("INVOICE")), 2)));
//				details.setDetailInvoice(String.valueOf(Helper.roundUptoDecimal((rs.getFloat("INVOICE")+rs.getFloat("TAX")),2)));
				logMessage(details.getDetailInvoice());
				details.setDetailDesc(String.valueOf(Helper.roundUptoDecimal(Double.parseDouble(rs.getString("DISC")), 2)));
				logMessage(details.getDetailDesc());
				details.setDetailTotal(String.valueOf(Helper.roundUptoDecimal(Double.parseDouble(rs.getString("TOTAL")), 2)));
				logMessage(details.getDetailTotal());
				details.setDetailUsage(rs.getString("USAGE"));
				logMessage(rs.getString("USAGE"));
				details.setSubitemTitle(rs.getString("BILL_SUBITEM_CODE"),lang);
				logMessage(rs.getString("BILL_SUBITEM_CODE"));
				details.setDetailUnit(rs.getString("BILL_SUBITEM_CODE"),lang);
				logMessage(rs.getString("BILL_SUBITEM_CODE"));
				billSubItemCode.setDetails(details);
				billSubItemCodes.add(billSubItemCode);
				
				
			}
			companyDetail.setBillSubItemCodes(billSubItemCodes);
			companyInvoiceResponse.setCompanyDetail(companyDetail);
		} catch (SQLException e) {
			logger.info("ERROR: ", e);
			companyInvoiceResponse.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
			companyInvoiceResponse.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
		}
		
		
		companyInvoiceResponse.setReturnCode(ResponseCodes.SUCESS_CODE_200);
		companyInvoiceResponse.setReturnMsg(ResponseCodes.SUCESS_DES_200);
		return companyInvoiceResponse;
	
	}

	private void logMessage(String message)
	{
		logger.info(modeulName+": "+message);
	}


}
