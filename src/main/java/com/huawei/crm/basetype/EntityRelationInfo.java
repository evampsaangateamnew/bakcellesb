package com.huawei.crm.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for EntityRelationInfo complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="EntityRelationInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="EntityRelationSeq" type="{http://crm.huawei.com/basetype/}EntityLong" minOccurs="0"/>
 *         &lt;element name="ActionType" type="{http://crm.huawei.com/basetype/}FeeActionType"/>
 *         &lt;element name="EntityId" type="{http://crm.huawei.com/basetype/}EntityLong" minOccurs="0"/>
 *         &lt;element name="EntityIdEn" type="{http://crm.huawei.com/basetype/}EntityIdEn" minOccurs="0"/>
 *         &lt;element name="RelaEntityId" type="{http://crm.huawei.com/basetype/}EntityLong" minOccurs="0"/>
 *         &lt;element name="RelaEntityIdEn" type="{http://crm.huawei.com/basetype/}EntityIdEn" minOccurs="0"/>
 *         &lt;element name="EntityType" type="{http://crm.huawei.com/basetype/}SubEntityType"/>
 *         &lt;element name="RelationType" type="{http://crm.huawei.com/basetype/}SubEntityType"/>
 *         &lt;element name="EffectiveTime" type="{http://crm.huawei.com/basetype/}Time" minOccurs="0"/>
 *         &lt;element name="ExpiredTime" type="{http://crm.huawei.com/basetype/}Time" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://crm.huawei.com/basetype/}DocumentRemark" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EntityRelationInfo", propOrder = {})
public class EntityRelationInfo {
	@XmlElement(name = "EntityRelationSeq")
	protected Long entityRelationSeq;
	@XmlElement(name = "ActionType", required = true)
	protected String actionType;
	@XmlElement(name = "EntityId")
	protected Long entityId;
	@XmlElement(name = "EntityIdEn")
	protected String entityIdEn;
	@XmlElement(name = "RelaEntityId")
	protected Long relaEntityId;
	@XmlElement(name = "RelaEntityIdEn")
	protected String relaEntityIdEn;
	@XmlElement(name = "EntityType", required = true)
	protected String entityType;
	@XmlElement(name = "RelationType", required = true)
	protected String relationType;
	@XmlElement(name = "EffectiveTime")
	protected String effectiveTime;
	@XmlElement(name = "ExpiredTime")
	protected String expiredTime;
	@XmlElement(name = "Remark")
	protected String remark;
	@XmlElement(name = "ExtParamList")
	protected ExtParameterList extParamList;

	/**
	 * Gets the value of the entityRelationSeq property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getEntityRelationSeq() {
		return entityRelationSeq;
	}

	/**
	 * Sets the value of the entityRelationSeq property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setEntityRelationSeq(Long value) {
		this.entityRelationSeq = value;
	}

	/**
	 * Gets the value of the actionType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getActionType() {
		return actionType;
	}

	/**
	 * Sets the value of the actionType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setActionType(String value) {
		this.actionType = value;
	}

	/**
	 * Gets the value of the entityId property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getEntityId() {
		return entityId;
	}

	/**
	 * Sets the value of the entityId property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setEntityId(Long value) {
		this.entityId = value;
	}

	/**
	 * Gets the value of the entityIdEn property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEntityIdEn() {
		return entityIdEn;
	}

	/**
	 * Sets the value of the entityIdEn property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEntityIdEn(String value) {
		this.entityIdEn = value;
	}

	/**
	 * Gets the value of the relaEntityId property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getRelaEntityId() {
		return relaEntityId;
	}

	/**
	 * Sets the value of the relaEntityId property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setRelaEntityId(Long value) {
		this.relaEntityId = value;
	}

	/**
	 * Gets the value of the relaEntityIdEn property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRelaEntityIdEn() {
		return relaEntityIdEn;
	}

	/**
	 * Sets the value of the relaEntityIdEn property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRelaEntityIdEn(String value) {
		this.relaEntityIdEn = value;
	}

	/**
	 * Gets the value of the entityType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEntityType() {
		return entityType;
	}

	/**
	 * Sets the value of the entityType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEntityType(String value) {
		this.entityType = value;
	}

	/**
	 * Gets the value of the relationType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRelationType() {
		return relationType;
	}

	/**
	 * Sets the value of the relationType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRelationType(String value) {
		this.relationType = value;
	}

	/**
	 * Gets the value of the effectiveTime property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEffectiveTime() {
		return effectiveTime;
	}

	/**
	 * Sets the value of the effectiveTime property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEffectiveTime(String value) {
		this.effectiveTime = value;
	}

	/**
	 * Gets the value of the expiredTime property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getExpiredTime() {
		return expiredTime;
	}

	/**
	 * Sets the value of the expiredTime property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setExpiredTime(String value) {
		this.expiredTime = value;
	}

	/**
	 * Gets the value of the remark property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * Sets the value of the remark property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark(String value) {
		this.remark = value;
	}

	/**
	 * Gets the value of the extParamList property.
	 * 
	 * @return possible object is {@link ExtParameterList }
	 * 
	 */
	public ExtParameterList getExtParamList() {
		return extParamList;
	}

	/**
	 * Sets the value of the extParamList property.
	 * 
	 * @param value
	 *            allowed object is {@link ExtParameterList }
	 * 
	 */
	public void setExtParamList(ExtParameterList value) {
		this.extParamList = value;
	}
}
