package com.evampsaanga.bakcell.changesuplimentry;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.amqimplementationsesb.LogsType;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.ThirdPartyNames;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.developer.utils.SOAPLoggingHandler;
import com.evampsaanga.services.USSDService;
import com.huawei.bss.soaif._interface.common.ReqHeader;
import com.huawei.bss.soaif._interface.ussdgateway.USSDGWChngSOReqMsg;
import com.huawei.bss.soaif._interface.ussdgateway.USSDGWChngSORspMsg;

@Path("/bakcell")
public class ChangeSupplimentryOfferingRequestLand {
	public static final Logger logger = Logger.getLogger("bakcellLogs-V2");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response Get(@Header("credentials") String credential, @Header("Content-Type") String contentType,
			@Body() String requestBody) {
		Response resp = new Response();
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.CHANGE_SUPPLIMENTRY_OFFERING_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.CHANGE_SUPPLIMENTRY_OFFERING);
		logs.setTableType(LogsType.ChangeSupplimentryOffering);
		try {
			logger.info("Request Landed on ChangeSupplimentryOfferingRequestLand Land:" + requestBody);
			Request cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, Request.class);
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						resp.setReturnCode(ResponseCodes.ERROR_400);
						resp.setReturnMsg(verification);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					int actionType = -1;
					try {
						actionType = Integer.parseInt(cclient.getActionType());
						if (actionType != 1 && actionType != 3) {
							resp.setReturnCode(ResponseCodes.MISSING_PARAMETER_CODE);
							resp.setReturnMsg(ResponseCodes.MISSING_PARAMETER_DES);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception ex) {
						resp.setReturnCode(ResponseCodes.MISSING_PARAMETER_CODE);
						resp.setReturnMsg(ResponseCodes.MISSING_PARAMETER_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
					USSDGWChngSORspMsg respone = ussdChangeSuplimentryOfferings(actionType, cclient.getmsisdn(),
							cclient.getOfferingId());
					if (respone.getRspHeader().getReturnCode().equals("0000")) {
						resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					} else {
						resp.setReturnCode(respone.getRspHeader().getReturnCode());
						resp.setReturnMsg(respone.getRspHeader().getReturnMsg());
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	public USSDGWChngSORspMsg ussdChangeSuplimentryOfferings(int action, String msisdn, String offeringUnqiqueId) {
		USSDGWChngSOReqMsg req = new USSDGWChngSOReqMsg();
		req.setReqHeader(getRequestHeader());
		req.setServiceNumber(msisdn);
		req.setIsConfirmed(Constants.USSD_IS_CONFIRMED);
		USSDGWChngSOReqMsg.SupplementaryOfferingList list = new USSDGWChngSOReqMsg.SupplementaryOfferingList();
		list.setActionType(Integer.toString(action));
		USSDGWChngSOReqMsg.SupplementaryOfferingList.OfferingId offeringId = new USSDGWChngSOReqMsg.SupplementaryOfferingList.OfferingId();
		offeringId.setOfferingId(offeringUnqiqueId);
		list.setOfferingId(offeringId);
		req.getSupplementaryOfferingList().add(list);
		USSDGWChngSORspMsg respone = USSDService.getInstance().ussdGatewayChangeSupplementaryOffering(req);
		return respone;
	}

	public ReqHeader getRequestHeader() {
		ReqHeader reqh = new ReqHeader();
		try {
			reqh.setAccessUser(ConfigurationManager.getUSSDProperties("ussd.reqh.AccessUser").trim());
			reqh.setChannelId(ConfigurationManager.getUSSDProperties("ussd.reqh.ChannelId").trim());
			reqh.setAccessPwd(ConfigurationManager.getUSSDProperties("ussd.reqh.AccessPwd").trim());
			reqh.setTransactionId(Helper.generateTransactionID());
			return reqh;
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		}
		return reqh;
	}
}
