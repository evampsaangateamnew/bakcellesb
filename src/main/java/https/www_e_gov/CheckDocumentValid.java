
package https.www_e_gov;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import net.x_rd.az.voen9900037691.producer.CheckDocumentValidRequest;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CheckDocumentValidRequest" type="{http://voen9900037691.az.x-rd.net/producer}CheckDocumentValidRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "checkDocumentValidRequest"
})
@XmlRootElement(name = "CheckDocumentValid")
public class CheckDocumentValid {

    @XmlElement(name = "CheckDocumentValidRequest")
    protected CheckDocumentValidRequest checkDocumentValidRequest;

    /**
     * Gets the value of the checkDocumentValidRequest property.
     * 
     * @return
     *     possible object is
     *     {@link CheckDocumentValidRequest }
     *     
     */
    public CheckDocumentValidRequest getCheckDocumentValidRequest() {
        return checkDocumentValidRequest;
    }

    /**
     * Sets the value of the checkDocumentValidRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link CheckDocumentValidRequest }
     *     
     */
    public void setCheckDocumentValidRequest(CheckDocumentValidRequest value) {
        this.checkDocumentValidRequest = value;
    }

}
