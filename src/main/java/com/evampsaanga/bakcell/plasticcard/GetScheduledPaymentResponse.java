package com.evampsaanga.bakcell.plasticcard;

import java.util.List;

import com.evampsaanga.bakcell.responseheaders.BaseResponse;

public class GetScheduledPaymentResponse extends BaseResponse {

	List<ScheduledPaymentDetails> scheduledPaymentDetailsList;

	public List<ScheduledPaymentDetails> getScheduledPaymentDetailsList() {
		return scheduledPaymentDetailsList;
	}

	public void setScheduledPaymentDetailsList(List<ScheduledPaymentDetails> scheduledPaymentDetailsList) {
		this.scheduledPaymentDetailsList = scheduledPaymentDetailsList;
	}

	@Override
	public String toString() {
		return "GetScheduledPaymentResponse [scheduledPaymentDetailsList=" + scheduledPaymentDetailsList
				+ ", toString()=" + super.toString() + "]";
	}
}
