package com.nsn.devicemanagement.wsdl.applicationmanagement._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for application complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="application">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="deliveryState" type="{http://nsn.com/DeviceManagement/wsdl/ApplicationManagement/2.0}deliveryStates" minOccurs="0"/>
 *         &lt;element name="envType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ext" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="external" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="runningState" type="{http://nsn.com/DeviceManagement/wsdl/ApplicationManagement/2.0}runningStates" minOccurs="0"/>
 *         &lt;element name="version" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "application", propOrder = { "deliveryState", "envType", "ext", "external", "id", "name",
		"runningState", "version" })
public class Application {
	protected DeliveryStates deliveryState;
	protected String envType;
	protected String ext;
	protected String external;
	protected String id;
	protected String name;
	protected RunningStates runningState;
	protected String version;

	/**
	 * Gets the value of the deliveryState property.
	 * 
	 * @return possible object is {@link DeliveryStates }
	 * 
	 */
	public DeliveryStates getDeliveryState() {
		return deliveryState;
	}

	/**
	 * Sets the value of the deliveryState property.
	 * 
	 * @param value
	 *            allowed object is {@link DeliveryStates }
	 * 
	 */
	public void setDeliveryState(DeliveryStates value) {
		this.deliveryState = value;
	}

	/**
	 * Gets the value of the envType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEnvType() {
		return envType;
	}

	/**
	 * Sets the value of the envType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEnvType(String value) {
		this.envType = value;
	}

	/**
	 * Gets the value of the ext property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getExt() {
		return ext;
	}

	/**
	 * Sets the value of the ext property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setExt(String value) {
		this.ext = value;
	}

	/**
	 * Gets the value of the external property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getExternal() {
		return external;
	}

	/**
	 * Sets the value of the external property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setExternal(String value) {
		this.external = value;
	}

	/**
	 * Gets the value of the id property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the value of the id property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setId(String value) {
		this.id = value;
	}

	/**
	 * Gets the value of the name property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the value of the name property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setName(String value) {
		this.name = value;
	}

	/**
	 * Gets the value of the runningState property.
	 * 
	 * @return possible object is {@link RunningStates }
	 * 
	 */
	public RunningStates getRunningState() {
		return runningState;
	}

	/**
	 * Sets the value of the runningState property.
	 * 
	 * @param value
	 *            allowed object is {@link RunningStates }
	 * 
	 */
	public void setRunningState(RunningStates value) {
		this.runningState = value;
	}

	/**
	 * Gets the value of the version property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Sets the value of the version property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setVersion(String value) {
		this.version = value;
	}
}
