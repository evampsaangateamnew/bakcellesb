
package https.wwww_e_gov;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RequestSellerInformation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RequestSellerInformation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PlaceType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Regions" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PlaceCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PlaceName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PlaceAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SellerUsername" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SellerDocumentNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SellerDocumentPin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SellerFullname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SellerContact" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestSellerInformation", propOrder = {
    "placeType",
    "regions",
    "placeCode",
    "placeName",
    "placeAddress",
    "sellerUsername",
    "sellerDocumentNumber",
    "sellerDocumentPin",
    "sellerFullname",
    "sellerContact"
})
public class RequestSellerInformation {

    @XmlElement(name = "PlaceType")
    protected String placeType;
    @XmlElement(name = "Regions")
    protected String regions;
    @XmlElement(name = "PlaceCode")
    protected String placeCode;
    @XmlElement(name = "PlaceName")
    protected String placeName;
    @XmlElement(name = "PlaceAddress")
    protected String placeAddress;
    @XmlElement(name = "SellerUsername")
    protected String sellerUsername;
    @XmlElement(name = "SellerDocumentNumber")
    protected String sellerDocumentNumber;
    @XmlElement(name = "SellerDocumentPin")
    protected String sellerDocumentPin;
    @XmlElement(name = "SellerFullname")
    protected String sellerFullname;
    @XmlElement(name = "SellerContact")
    protected String sellerContact;

    /**
     * Gets the value of the placeType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlaceType() {
        return placeType;
    }

    /**
     * Sets the value of the placeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlaceType(String value) {
        this.placeType = value;
    }

    /**
     * Gets the value of the regions property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegions() {
        return regions;
    }

    /**
     * Sets the value of the regions property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegions(String value) {
        this.regions = value;
    }

    /**
     * Gets the value of the placeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlaceCode() {
        return placeCode;
    }

    /**
     * Sets the value of the placeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlaceCode(String value) {
        this.placeCode = value;
    }

    /**
     * Gets the value of the placeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlaceName() {
        return placeName;
    }

    /**
     * Sets the value of the placeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlaceName(String value) {
        this.placeName = value;
    }

    /**
     * Gets the value of the placeAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlaceAddress() {
        return placeAddress;
    }

    /**
     * Sets the value of the placeAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlaceAddress(String value) {
        this.placeAddress = value;
    }

    /**
     * Gets the value of the sellerUsername property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSellerUsername() {
        return sellerUsername;
    }

    /**
     * Sets the value of the sellerUsername property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSellerUsername(String value) {
        this.sellerUsername = value;
    }

    /**
     * Gets the value of the sellerDocumentNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSellerDocumentNumber() {
        return sellerDocumentNumber;
    }

    /**
     * Sets the value of the sellerDocumentNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSellerDocumentNumber(String value) {
        this.sellerDocumentNumber = value;
    }

    /**
     * Gets the value of the sellerDocumentPin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSellerDocumentPin() {
        return sellerDocumentPin;
    }

    /**
     * Sets the value of the sellerDocumentPin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSellerDocumentPin(String value) {
        this.sellerDocumentPin = value;
    }

    /**
     * Gets the value of the sellerFullname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSellerFullname() {
        return sellerFullname;
    }

    /**
     * Sets the value of the sellerFullname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSellerFullname(String value) {
        this.sellerFullname = value;
    }

    /**
     * Gets the value of the sellerContact property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSellerContact() {
        return sellerContact;
    }

    /**
     * Sets the value of the sellerContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSellerContact(String value) {
        this.sellerContact = value;
    }

}
