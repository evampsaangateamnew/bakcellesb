package com.evampsaanga.ulduzumupdate;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.amqimplementationsesb.LogsType;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.ThirdPartyNames;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.developer.utils.SOAPLoggingHandler;
import com.evampsaanga.ulduzumupdate.getmerchantdetails.GetMerchantDetailsRequestClient;
import com.evampsaanga.ulduzumupdate.getmerchantdetails.GetMerchantDetailsResponse;
import com.evampsaanga.ulduzumupdate.getmerchantdetails.GetMerchantDetailsResponseClient;
import com.evampsaanga.ulduzumupdate.getmerchants.GetMerchantUlduzumResponse;
import com.evampsaanga.ulduzumupdate.getmerchants.GetMerchantsRequestClient;
import com.evampsaanga.ulduzumupdate.getmerchants.GetMerchantsResponseClient;
import com.evampsaanga.ulduzumupdate.getnewcode.GetNewCode;
import com.evampsaanga.ulduzumupdate.getnewcode.GetNewCodeRequestClient;
import com.evampsaanga.ulduzumupdate.getnewcode.GetNewCodeResponseClient;
import com.evampsaanga.ulduzumupdate.getunusedcodes.GetUnusedCodeRequestClient;
import com.evampsaanga.ulduzumupdate.getunusedcodes.GetUnusedCodeResponse;
import com.evampsaanga.ulduzumupdate.getunusedcodes.GetUnusedCodeResponseClient;
import com.evampsaanga.ulduzumupdate.getusagehistory.GetUsageHistoryRequestClient;
import com.evampsaanga.ulduzumupdate.getusagehistory.GetUsageHistoryResponse;
import com.evampsaanga.ulduzumupdate.getusagehistory.GetUsageHistoryResponseClient;
import com.evampsaanga.ulduzumupdate.getusagetotal.GetUsageTotalRequestClient;
import com.evampsaanga.ulduzumupdate.getusagetotal.GetUsageTotalResponse;
import com.evampsaanga.ulduzumupdate.getusagetotal.GetUsageTotalResponseClient;
import com.saanga.magento.apiclient.RestClient;

@Path("/bakcell")
public class UlduzumRequestLand {

	public static final Logger logger = Logger.getLogger("bakcellLogs-V2");
	public static final Logger loggerV2 = Logger.getLogger("bakcellLogs-V2");

	@POST
	@Path("/getmerchants")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetMerchantsResponseClient GetMerchants(@Body String requestBody, @Header("credentials") String credential)
			throws JSONException {
		String identifier = Transactions.ULDUZUM_GET_MERCHANTS_TRANSACTION_NAME;
		logger.info("Request Landed on " + Transactions.ULDUZUM_GET_MERCHANTS_TRANSACTION_NAME + "- with request : "
				+ requestBody);

		Logs logs = new Logs();
		logs.setTransactionName(Transactions.ULDUZUM_GET_MERCHANTS_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.ULDUZUM);
		logs.setTableType(LogsType.UlduzumGetMerchants);
		GetMerchantsRequestClient cclient = new GetMerchantsRequestClient();
		GetMerchantsResponseClient resp = new GetMerchantsResponseClient();
		try {
			cclient = Helper.JsonToObject(requestBody, GetMerchantsRequestClient.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
				logs.setIsB2B(cclient.getIsB2B());
			}
		} catch (Exception ex) {
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_401);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			logger.info(Helper.GetException(ex));
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				logger.info(Helper.GetException(ex));
				return resp;
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);

				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {
					identifier = cclient.getmsisdn() + "-" + Transactions.ULDUZUM_GET_MERCHANTS_TRANSACTION_NAME;

					Map<String, String> obj = new HashMap<String, String>();

					obj.put("msisdn", "994" + cclient.getmsisdn());
					obj.put("lang", mapLang(cclient.getLang()));

					JSONObject json = new JSONObject(obj);

					String secretKey = ConfigurationManager.getConfigurationFromCache("ulduzum.subscriber.secretkey");
					logger.debug(identifier + "-Secret Key is: " + secretKey);
					String HASHCODE = md5ApacheCommonsCodec(json.toString() + secretKey);
					logger.debug(identifier + "-HASHCODE : " + HASHCODE);
					String endPoint = (getEndPoint("ulduzum.subscriber.getmerchants") + HASHCODE).trim();
					logger.debug(identifier + "-Thirdparty Request URL " + endPoint);
					logger.debug(identifier + "-Thirdparty Request Body " + json);
					String response = RestClient.SendCallToUlduzum(endPoint, json.toString());
					logger.debug(identifier + "-Response from thirdparty: " + response);

					try {
						if (isSuccess(response)) {
							GetMerchantUlduzumResponse data = Helper.JsonToObject(
									parseMerchantListResponseJSON(response), GetMerchantUlduzumResponse.class);
							if (data.getResult().equalsIgnoreCase("success")) {
								resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
								resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);

								resp.setData(data.getData());
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							} else {
								resp.setReturnCode(ResponseCodes.CODE_NO_DATA_FOUND);
								resp.setReturnMsg(ResponseCodes.ERROR_NO_DATA_FOUND);

								logs.setResponseCode(ResponseCodes.CODE_NO_DATA_FOUND);
								logs.setResponseDescription(ResponseCodes.ERROR_NO_DATA_FOUND);
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
							}
						} else {
							resp.setReturnCode(ResponseCodes.CODE_NO_DATA_FOUND);
							resp.setReturnMsg(ResponseCodes.ERROR_NO_DATA_FOUND);

							logs.setResponseCode(ResponseCodes.CODE_NO_DATA_FOUND);
							logs.setResponseDescription(ResponseCodes.ERROR_NO_DATA_FOUND);
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception ex) {
						resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						logger.info(Helper.GetException(ex));
						return resp;
					}
				} catch (Exception ex) {
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					logger.info(Helper.GetException(ex));
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		logs.updateLog(logs);
		logger.debug(identifier + "-Response return from ESB: " + resp);
		return resp;
	}

	@POST
	@Path("/getmerchantdetails")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetMerchantDetailsResponseClient GetMerchantDetails(@Body String requestBody,
			@Header("credentials") String credential) throws JSONException {
		String identifier = Transactions.ULDUZUM_GET_MERCHANT_DETAILS_TRANSACTION_NAME;
		logger.info("Request Landed on " + Transactions.ULDUZUM_GET_MERCHANT_DETAILS_TRANSACTION_NAME
				+ "- with request : " + requestBody);

		Logs logs = new Logs();
		logs.setTransactionName(Transactions.ULDUZUM_GET_MERCHANT_DETAILS_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.ULDUZUM);
		logs.setTableType(LogsType.UlduzumGetMerchants);
		GetMerchantDetailsRequestClient cclient = new GetMerchantDetailsRequestClient();
		GetMerchantDetailsResponseClient resp = new GetMerchantDetailsResponseClient();
		try {
			cclient = Helper.JsonToObject(requestBody, GetMerchantDetailsRequestClient.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
				logs.setIsB2B(cclient.getIsB2B());
			}
		} catch (Exception ex) {
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_401);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			logger.info(Helper.GetException(ex));
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				logger.info(Helper.GetException(ex));
				return resp;
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);

				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {
					identifier = cclient.getmsisdn() + "-" + Transactions.ULDUZUM_GET_MERCHANTS_TRANSACTION_NAME;

					Map<String, String> obj = new HashMap<String, String>();

					obj.put("msisdn", "994" + cclient.getmsisdn());
					obj.put("lang", mapLang(cclient.getLang()));
					obj.put("merchantid", cclient.getMerchantid());
					JSONObject json = new JSONObject(obj);

					String secretKey = ConfigurationManager.getConfigurationFromCache("ulduzum.subscriber.secretkey");
					logger.debug(identifier + "-Secret Key is: " + secretKey);
					String HASHCODE = md5ApacheCommonsCodec(json.toString() + secretKey);
					logger.debug(identifier + "-HASHCODE : " + HASHCODE);
					String endPoint = (getEndPoint("ulduzum.subscriber.getmerchantdetails") + HASHCODE).trim();
					logger.debug(identifier + "-Thirdparty Request URL " + endPoint);
					logger.debug(identifier + "-Thirdparty Request Body " + json);
					String response = RestClient.SendCallToUlduzum(endPoint, json.toString());
					logger.debug(identifier + "-Response from thirdparty: " + response);

					try {
						if (isSuccess(response)) {
							GetMerchantDetailsResponse data = Helper.JsonToObject(
									parseSingleObjectResponseJSON(response), GetMerchantDetailsResponse.class);
							if (data.getResult().equalsIgnoreCase("success")) {
								resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
								resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);

								resp.setData(data.getData());
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							} else {
								resp.setReturnCode(ResponseCodes.CODE_NO_DATA_FOUND);
								resp.setReturnMsg(ResponseCodes.ERROR_NO_DATA_FOUND);

								logs.setResponseCode(ResponseCodes.CODE_NO_DATA_FOUND);
								logs.setResponseDescription(ResponseCodes.ERROR_NO_DATA_FOUND);
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
							}
						} else {
							resp.setReturnCode(ResponseCodes.CODE_NO_DATA_FOUND);
							resp.setReturnMsg(ResponseCodes.ERROR_NO_DATA_FOUND);

							logs.setResponseCode(ResponseCodes.CODE_NO_DATA_FOUND);
							logs.setResponseDescription(ResponseCodes.ERROR_NO_DATA_FOUND);
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception ex) {
						resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						logger.info(Helper.GetException(ex));
						return resp;
					}
				} catch (Exception ex) {
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					logger.info(Helper.GetException(ex));
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		logs.updateLog(logs);
		logger.debug(identifier + "-Response return from ESB: " + resp);
		return resp;
	}

	@POST
	@Path("/getusagetotals")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetUsageTotalResponseClient GetUsageTotals(@Body String requestBody,
			@Header("credentials") String credential) throws JSONException {
		String identifier = Transactions.ULDUZUM_GET_USAGE_TOTAL_TRANSACTION_NAME;
		logger.info("Request Landed on " + Transactions.ULDUZUM_GET_USAGE_TOTAL_TRANSACTION_NAME + "- with request : "
				+ requestBody);

		Logs logs = new Logs();
		logs.setTransactionName(Transactions.ULDUZUM_GET_USAGE_TOTAL_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.ULDUZUM);
		logs.setTableType(LogsType.UlduzumGetMerchants);
		GetUsageTotalRequestClient cclient = new GetUsageTotalRequestClient();
		GetUsageTotalResponseClient resp = new GetUsageTotalResponseClient();
		try {
			cclient = Helper.JsonToObject(requestBody, GetUsageTotalRequestClient.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
				logs.setIsB2B(cclient.getIsB2B());
			}
		} catch (Exception ex) {
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_401);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			logger.info(Helper.GetException(ex));
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				logger.info(Helper.GetException(ex));
				return resp;
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);

				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {
					identifier = cclient.getmsisdn() + "-" + Transactions.ULDUZUM_GET_USAGE_TOTAL_TRANSACTION_NAME;

					Map<String, String> obj = new HashMap<String, String>();

					obj.put("msisdn", "994" + cclient.getmsisdn());
					JSONObject json = new JSONObject(obj);

					String secretKey = ConfigurationManager.getConfigurationFromCache("ulduzum.subscriber.secretkey");
					logger.debug(identifier + "-Secret Key is: " + secretKey);
					String HASHCODE = md5ApacheCommonsCodec(json.toString() + secretKey);
					logger.debug(identifier + "-HASHCODE : " + HASHCODE);
					String endPoint = (getEndPoint("ulduzum.subscriber.getusagetotals") + HASHCODE).trim();
					logger.debug(identifier + "-Thirdparty Request URL " + endPoint);
					logger.debug(identifier + "-Thirdparty Request Body " + json);
					String response = RestClient.SendCallToUlduzum(endPoint, json.toString());
					logger.debug(identifier + "-Response from thirdparty: " + response);

					try {
						if (isSuccess(response)) {
							GetUsageTotalResponse data = Helper.JsonToObject(response, GetUsageTotalResponse.class);
							if (data.getResult().equalsIgnoreCase("success")) {
								resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
								resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
								data.getData().setDaily_limit_total(Integer.valueOf(ConfigurationManager
										.getConfigurationFromCache("ulduzum.subscriber.getusage.totallimit")));
								resp.setData(data.getData());
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							} else {
								resp.setReturnCode(ResponseCodes.CODE_NO_DATA_FOUND);
								resp.setReturnMsg(ResponseCodes.ERROR_NO_DATA_FOUND);

								logs.setResponseCode(ResponseCodes.CODE_NO_DATA_FOUND);
								logs.setResponseDescription(ResponseCodes.ERROR_NO_DATA_FOUND);
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
							}
						} else {
							resp.setReturnCode(ResponseCodes.CODE_NO_DATA_FOUND);
							resp.setReturnMsg(ResponseCodes.ERROR_NO_DATA_FOUND);

							logs.setResponseCode(ResponseCodes.CODE_NO_DATA_FOUND);
							logs.setResponseDescription(ResponseCodes.ERROR_NO_DATA_FOUND);
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception ex) {
						resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						logger.info(Helper.GetException(ex));
						return resp;
					}
				} catch (Exception ex) {
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					logger.info(Helper.GetException(ex));
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		logs.updateLog(logs);
		logger.debug(identifier + "-Response return from ESB: " + resp);
		return resp;
	}

	@POST
	@Path("/codegenerate")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetNewCodeResponseClient CodeGenerate(@Body String requestBody, @Header("credentials") String credential)
			throws JSONException {
		String identifier = Transactions.ULDUZUM_GET_CODE_GENERATE_TRANSACTION_NAME;
		logger.info("Request Landed on " + Transactions.ULDUZUM_GET_CODE_GENERATE_TRANSACTION_NAME + "- with request : "
				+ requestBody);

		Logs logs = new Logs();
		logs.setTransactionName(Transactions.ULDUZUM_GET_CODE_GENERATE_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.ULDUZUM);
		logs.setTableType(LogsType.UlduzumGetMerchants);
		GetNewCodeRequestClient cclient = new GetNewCodeRequestClient();
		GetNewCodeResponseClient resp = new GetNewCodeResponseClient();
		try {
			cclient = Helper.JsonToObject(requestBody, GetNewCodeRequestClient.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
				logs.setIsB2B(cclient.getIsB2B());
			}
		} catch (Exception ex) {
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_401);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			logger.info(Helper.GetException(ex));
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				logger.info(Helper.GetException(ex));
				return resp;
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);

				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {
					identifier = cclient.getmsisdn() + "-" + Transactions.ULDUZUM_GET_CODE_GENERATE_TRANSACTION_NAME;

					Map<String, String> obj = new HashMap<String, String>();

					obj.put("msisdn", "994" + cclient.getmsisdn());
					JSONObject json = new JSONObject(obj);

					String secretKey = ConfigurationManager.getConfigurationFromCache("ulduzum.subscriber.secretkey");
					logger.debug(identifier + "-Secret Key is: " + secretKey);
					String HASHCODE = md5ApacheCommonsCodec(json.toString() + secretKey);
					logger.debug(identifier + "-HASHCODE : " + HASHCODE);
					String endPoint = (getEndPoint("ulduzum.subscriber.codegenerate") + HASHCODE).trim();
					logger.debug(identifier + "-Thirdparty Request URL " + endPoint);
					logger.debug(identifier + "-Thirdparty Request Body " + json);
					String response = RestClient.SendCallToUlduzum(endPoint, json.toString());
					logger.debug(identifier + "-Response from thirdparty: " + response);

					try {
						JSONObject jObj = new JSONObject(response);

						if (jObj.getString("result").equalsIgnoreCase("success")) {
							GetNewCode data = new GetNewCode();
							data.setCode(jObj.getJSONObject("data").getString("code"));
							resp.setData(data);
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						} else if (jObj.getString("result")
								.equalsIgnoreCase(ResponseCodes.ULDUZUM_DAILY_LIMIT_REACHED_DESC)) {
							resp.setReturnCode(ResponseCodes.ULDUZUM_DAILY_LIMIT_REACHED);
							resp.setReturnMsg(ResponseCodes.ULDUZUM_DAILY_LIMIT_REACHED_DESC);

							logs.setResponseCode(ResponseCodes.ULDUZUM_DAILY_LIMIT_REACHED);
							logs.setResponseDescription(ResponseCodes.ULDUZUM_DAILY_LIMIT_REACHED_DESC);
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						} else {
							resp.setReturnCode(ResponseCodes.CODE_NO_DATA_FOUND);
							resp.setReturnMsg(ResponseCodes.ERROR_NO_DATA_FOUND);

							logs.setResponseCode(ResponseCodes.CODE_NO_DATA_FOUND);
							logs.setResponseDescription(ResponseCodes.ERROR_NO_DATA_FOUND);
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
						}

					} catch (Exception ex) {
						resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						logger.info(Helper.GetException(ex));
						return resp;
					}
				} catch (Exception ex) {
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					logger.info(Helper.GetException(ex));
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		logs.updateLog(logs);
		logger.debug(identifier + "-Response return from ESB: " + resp);
		return resp;
	}

	@POST
	@Path("/getunusedcodes")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetUnusedCodeResponseClient GetUnusedCodes(@Body String requestBody,
			@Header("credentials") String credential) throws JSONException {
		String identifier = Transactions.ULDUZUM_GET_UNUSED_CODES_TRANSACTION_NAME;
		logger.info("Request Landed on " + Transactions.ULDUZUM_GET_UNUSED_CODES_TRANSACTION_NAME + "- with request : "
				+ requestBody);

		Logs logs = new Logs();
		logs.setTransactionName(Transactions.ULDUZUM_GET_UNUSED_CODES_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.ULDUZUM);
		logs.setTableType(LogsType.UlduzumGetMerchants);
		GetUnusedCodeRequestClient cclient = new GetUnusedCodeRequestClient();
		GetUnusedCodeResponseClient resp = new GetUnusedCodeResponseClient();
		try {
			cclient = Helper.JsonToObject(requestBody, GetUnusedCodeRequestClient.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
				logs.setIsB2B(cclient.getIsB2B());
			}
		} catch (Exception ex) {
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_401);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			logger.info(Helper.GetException(ex));
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				logger.info(Helper.GetException(ex));
				return resp;
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);

				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {
					identifier = cclient.getmsisdn() + "-" + Transactions.ULDUZUM_GET_UNUSED_CODES_TRANSACTION_NAME;

					Map<String, String> obj = new HashMap<String, String>();

					obj.put("msisdn", "994" + cclient.getmsisdn());
					JSONObject json = new JSONObject(obj);

					String secretKey = ConfigurationManager.getConfigurationFromCache("ulduzum.subscriber.secretkey");
					logger.debug(identifier + "-Secret Key is: " + secretKey);
					String HASHCODE = md5ApacheCommonsCodec(json.toString() + secretKey);
					logger.debug(identifier + "-HASHCODE : " + HASHCODE);
					String endPoint = (getEndPoint("ulduzum.subscriber.getunusedcodes") + HASHCODE).trim();
					logger.debug(identifier + "-Thirdparty Request URL " + endPoint);
					logger.debug(identifier + "-Thirdparty Request Body " + json);
					String response = RestClient.SendCallToUlduzum(endPoint, json.toString());
					logger.debug(identifier + "-Response from thirdparty: " + response);

					try {
						if (isSuccess(response)) {
							GetUnusedCodeResponse data = Helper.JsonToObject(parseResponseDataObjJSON(response),
									GetUnusedCodeResponse.class);
							if (data.getResult().equalsIgnoreCase("success")) {
								resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
								resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
								resp.setData(data.getData());
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							} else {
								resp.setReturnCode(ResponseCodes.CODE_NO_DATA_FOUND);
								resp.setReturnMsg(ResponseCodes.ERROR_NO_DATA_FOUND);

								logs.setResponseCode(ResponseCodes.CODE_NO_DATA_FOUND);
								logs.setResponseDescription(ResponseCodes.ERROR_NO_DATA_FOUND);
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							}
						} else {
							resp.setReturnCode(ResponseCodes.CODE_NO_DATA_FOUND);
							resp.setReturnMsg(ResponseCodes.ERROR_NO_DATA_FOUND);

							logs.setResponseCode(ResponseCodes.CODE_NO_DATA_FOUND);
							logs.setResponseDescription(ResponseCodes.ERROR_NO_DATA_FOUND);
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
						}
					} catch (Exception ex) {
						resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						logger.info(Helper.GetException(ex));
						return resp;
					}
				} catch (Exception ex) {
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					logger.info(Helper.GetException(ex));
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		logs.updateLog(logs);
		logger.debug(identifier + "-Response return from ESB: " + resp);
		return resp;

	}

	@POST
	@Path("/getusagehistory")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetUsageHistoryResponseClient GetUsageHistory(@Body String requestBody,
			@Header("credentials") String credential) throws JSONException {
		String identifier = Transactions.ULDUZUM_GET_USAGE_HISTORY_TRANSACTION_NAME;
		logger.info("Request Landed on " + Transactions.ULDUZUM_GET_USAGE_HISTORY_TRANSACTION_NAME + "- with request : "
				+ requestBody);

		Logs logs = new Logs();
		logs.setTransactionName(Transactions.ULDUZUM_GET_USAGE_HISTORY_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.ULDUZUM);
		logs.setTableType(LogsType.UlduzumGetMerchants);
		GetUsageHistoryRequestClient cclient = new GetUsageHistoryRequestClient();
		GetUsageHistoryResponseClient resp = new GetUsageHistoryResponseClient();
		try {
			cclient = Helper.JsonToObject(requestBody, GetUsageHistoryRequestClient.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
				logs.setIsB2B(cclient.getIsB2B());
			}
		} catch (Exception ex) {
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_401);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			logger.info(Helper.GetException(ex));
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				logger.info(Helper.GetException(ex));
				return resp;
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);

				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {
					identifier = cclient.getmsisdn() + "-" + Transactions.ULDUZUM_GET_USAGE_HISTORY_TRANSACTION_NAME;

					Map<String, String> obj = new HashMap<String, String>();

					obj.put("msisdn", "994" + cclient.getmsisdn());
					obj.put("lang", mapLang(cclient.getLang()));
					obj.put("date_beg", cclient.getStartDate());
					obj.put("date_end", cclient.getEndDate());
					JSONObject json = new JSONObject(obj);

					String secretKey = ConfigurationManager.getConfigurationFromCache("ulduzum.subscriber.secretkey");
					logger.debug(identifier + "-Secret Key is: " + secretKey);
					String HASHCODE = md5ApacheCommonsCodec(json.toString() + secretKey);
					logger.debug(identifier + "-HASHCODE : " + HASHCODE);
					String endPoint = (getEndPoint("ulduzum.subscriber.getusagehistory") + HASHCODE).trim();
					logger.debug(identifier + "-Thirdparty Request URL " + endPoint);
					logger.debug(identifier + "-Thirdparty Request Body " + json);
					String response = RestClient.SendCallToUlduzum(endPoint, json.toString());
					logger.debug(identifier + "-Response from thirdparty: " + response);

					try {
						if (isSuccess(response)) {
							GetUsageHistoryResponse data = Helper.JsonToObject(parseResponseDataObjJSON(response),
									GetUsageHistoryResponse.class);
							if (data.getResult().equalsIgnoreCase("success")) {
								resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
								resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
								resp.setData(data.getData());
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							} else {
								resp.setReturnCode(ResponseCodes.CODE_NO_DATA_FOUND);
								resp.setReturnMsg(ResponseCodes.ERROR_NO_DATA_FOUND);

								logs.setResponseCode(ResponseCodes.CODE_NO_DATA_FOUND);
								logs.setResponseDescription(ResponseCodes.ERROR_NO_DATA_FOUND);
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
							}
						} else {
							resp.setReturnCode(ResponseCodes.CODE_NO_DATA_FOUND);
							resp.setReturnMsg(ResponseCodes.ERROR_NO_DATA_FOUND);

							logs.setResponseCode(ResponseCodes.CODE_NO_DATA_FOUND);
							logs.setResponseDescription(ResponseCodes.ERROR_NO_DATA_FOUND);
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception ex) {
						resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						logger.info(Helper.GetException(ex));
						return resp;
					}
				} catch (Exception ex) {
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					logger.info(Helper.GetException(ex));
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		logs.updateLog(logs);
		logger.debug(identifier + "-Response return from ESB: " + resp);
		return resp;
	}

	private boolean isSuccess(String response) throws JSONException {
		JSONObject obj = new JSONObject(response);
		if (obj.getString("result").equalsIgnoreCase("success"))
			return true;
		return false;
	}

	private String md5ApacheCommonsCodec(String content) {
		return DigestUtils.md5Hex(content);
	}

	private String mapLang(String lang) {
		return ConfigurationManager.getConfigurationFromCache("ulduzum.subscriber.language." + lang);
	}

	private String parseMerchantListResponseJSON(String resJson) throws JSONException {

		JSONObject jobj = new JSONObject(resJson);

		JSONArray jarr = new JSONArray();
		JSONObject subObjects = jobj.getJSONObject("data");

		for (int i = 0; i < subObjects.length(); i++) {
			JSONObject singleObject = subObjects.getJSONObject(String.valueOf(i));

			JSONObject brObj = singleObject.getJSONObject("branches");

			JSONArray branches = new JSONArray();
			for (int j = 0; j < brObj.length(); j++) {
				JSONObject brSignleObj = brObj.getJSONObject(String.valueOf(j));
				branches.put(brSignleObj);
			}
			singleObject.put("branches", branches);
			jarr.put(singleObject);
		}

		jobj.put("data", jarr);
		System.out.println(jobj);

		return jobj.toString();

	}

	private String parseSingleObjectResponseJSON(String resJson) throws JSONException {

		JSONObject jobj = new JSONObject(resJson);

		JSONObject singleObject = jobj.getJSONObject("data");

		JSONObject brObj = singleObject.getJSONObject("branches");
		JSONArray branches = new JSONArray();
		if (brObj.length() > 0) {

			for (int j = 0; j < brObj.length(); j++) {
				JSONObject brSingleObj = brObj.getJSONObject(String.valueOf(j));
				branches.put(brSingleObj);
			}

		} else {
			if (singleObject.get("address") != null) {
				JSONObject jObj = new JSONObject();
				jObj.put("name", singleObject.get("name"));
				jObj.put("coord_lat", singleObject.get("coord_lat"));
				jObj.put("coord_lng", singleObject.get("coord_lng"));
				jObj.put("address", singleObject.get("address"));
				branches.put(jObj);
			}
		}
		singleObject.put("branches", branches);
		jobj.put("data", singleObject);

		return jobj.toString();

	}

	/**
	 * Below parser is used for get Usage History and get unused codes as both
	 * have same parsing of data object to data list.
	 * 
	 * @param resJson
	 * @return
	 * @throws JSONException
	 */
	private String parseResponseDataObjJSON(String resJson) throws JSONException {

		JSONObject jobj = new JSONObject(resJson);

		JSONObject ObjList = jobj.getJSONObject("data");

		JSONArray usageList = new JSONArray();
		for (int j = 0; j < ObjList.length(); j++) {
			JSONObject brSignleObj = ObjList.getJSONObject(String.valueOf(j));
			usageList.put(brSignleObj);
		}

		jobj.put("data", usageList);

		return jobj.toString();

	}

	private String getEndPoint(String key) {
		return ConfigurationManager.getConfigurationFromCache("ulduzum.subscriber.baseurl")
				+ ConfigurationManager.getConfigurationFromCache(key);
	}
}
