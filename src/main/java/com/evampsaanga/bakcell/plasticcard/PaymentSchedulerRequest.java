package com.evampsaanga.bakcell.plasticcard;

import com.evampsaanga.bakcell.requestheaders.BaseRequest;

public class PaymentSchedulerRequest extends BaseRequest {

	private String amount;
	private String billingCycle; // 1. daily, 2. weekly, 3. monthly
	private String startDate;
	private String recurrenceNumber;
	private String savedCardId;
	private String recurrenceFrequency;

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getBillingCycle() {
		return billingCycle;
	}

	public void setBillingCycle(String billingCycle) {
		this.billingCycle = billingCycle;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getRecurrenceNumber() {
		return recurrenceNumber;
	}

	public void setRecurrenceNumber(String recurrenceNumber) {
		this.recurrenceNumber = recurrenceNumber;
	}

	public String getSavedCardId() {
		return savedCardId;
	}

	public void setSavedCardId(String savedCardId) {
		this.savedCardId = savedCardId;
	}

	@Override
	public String toString() {
		return "PaymentSchedulerRequest [amount=" + amount + ", billingCycle=" + billingCycle + ", startDate=" + startDate + ", recurrenceNumber=" + recurrenceNumber + ", savedCardId=" + savedCardId + ", toString()=" + super.toString() + "]";
	}

	public String getRecurrenceFrequency() {
		return recurrenceFrequency;
	}

	public void setRecurrenceFrequency(String recurrenceFrequency) {
		this.recurrenceFrequency = recurrenceFrequency;
	}

}