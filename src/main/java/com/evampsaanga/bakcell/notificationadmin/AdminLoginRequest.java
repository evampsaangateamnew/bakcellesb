package com.evampsaanga.bakcell.notificationadmin;

import com.evampsaanga.bakcell.requestheaders.BaseRequest;

public class AdminLoginRequest extends BaseRequest {
	private String password;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "AdminLoginRequest [password=" + password + ", toString()=" + super.toString() + "]";
	}
}
