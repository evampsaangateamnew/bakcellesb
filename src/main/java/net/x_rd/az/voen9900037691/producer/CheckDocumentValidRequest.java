
package net.x_rd.az.voen9900037691.producer;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import https.wwww_e_gov.RequestAuthentication;
import https.wwww_e_gov.RequestDocumentInformation;
import https.wwww_e_gov.RequestSellerInformation;


/**
 * <p>Java class for CheckDocumentValidRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CheckDocumentValidRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Authentication" type="{https://wwww.e-gov.az}RequestAuthentication" minOccurs="0"/>
 *         &lt;element name="DocumentInformation" type="{https://wwww.e-gov.az}RequestDocumentInformation" minOccurs="0"/>
 *         &lt;element name="SellerInformation" type="{https://wwww.e-gov.az}RequestSellerInformation" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CheckDocumentValidRequest", propOrder = {
    "authentication",
    "documentInformation",
    "sellerInformation"
})
public class CheckDocumentValidRequest {

    @XmlElement(name = "Authentication")
    protected RequestAuthentication authentication;
    @XmlElement(name = "DocumentInformation")
    protected RequestDocumentInformation documentInformation;
    @XmlElement(name = "SellerInformation")
    protected RequestSellerInformation sellerInformation;

    /**
     * Gets the value of the authentication property.
     * 
     * @return
     *     possible object is
     *     {@link RequestAuthentication }
     *     
     */
    public RequestAuthentication getAuthentication() {
        return authentication;
    }

    /**
     * Sets the value of the authentication property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestAuthentication }
     *     
     */
    public void setAuthentication(RequestAuthentication value) {
        this.authentication = value;
    }

    /**
     * Gets the value of the documentInformation property.
     * 
     * @return
     *     possible object is
     *     {@link RequestDocumentInformation }
     *     
     */
    public RequestDocumentInformation getDocumentInformation() {
        return documentInformation;
    }

    /**
     * Sets the value of the documentInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestDocumentInformation }
     *     
     */
    public void setDocumentInformation(RequestDocumentInformation value) {
        this.documentInformation = value;
    }

    /**
     * Gets the value of the sellerInformation property.
     * 
     * @return
     *     possible object is
     *     {@link RequestSellerInformation }
     *     
     */
    public RequestSellerInformation getSellerInformation() {
        return sellerInformation;
    }

    /**
     * Sets the value of the sellerInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestSellerInformation }
     *     
     */
    public void setSellerInformation(RequestSellerInformation value) {
        this.sellerInformation = value;
    }

}
