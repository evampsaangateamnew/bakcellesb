package com.nsn.devicemanagement.wsdl.common._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for SoftKeyAssignmentAction complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="SoftKeyAssignmentAction">
 *   &lt;complexContent>
 *     &lt;extension base="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}Action">
 *       &lt;sequence>
 *         &lt;element name="appPackID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="softKeyID" type="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}SoftKeyID" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SoftKeyAssignmentAction", propOrder = { "appPackID", "softKeyID" })
public class SoftKeyAssignmentAction extends Action {
	protected long appPackID;
	@XmlSchemaType(name = "string")
	protected SoftKeyID softKeyID;

	/**
	 * Gets the value of the appPackID property.
	 * 
	 */
	public long getAppPackID() {
		return appPackID;
	}

	/**
	 * Sets the value of the appPackID property.
	 * 
	 */
	public void setAppPackID(long value) {
		this.appPackID = value;
	}

	/**
	 * Gets the value of the softKeyID property.
	 * 
	 * @return possible object is {@link SoftKeyID }
	 * 
	 */
	public SoftKeyID getSoftKeyID() {
		return softKeyID;
	}

	/**
	 * Sets the value of the softKeyID property.
	 * 
	 * @param value
	 *            allowed object is {@link SoftKeyID }
	 * 
	 */
	public void setSoftKeyID(SoftKeyID value) {
		this.softKeyID = value;
	}
}
