package com.evampsaanga.ulduzum;

import java.util.ArrayList;
import java.util.List;

import com.evampsaanga.bakcell.responseheaders.BaseResponse;


public class GetCategoriesResponseclient extends BaseResponse {

	List<com.evampsaanga.ulduzum.getcategories.Datum> data = new ArrayList<>();

	public List<com.evampsaanga.ulduzum.getcategories.Datum> getData() {
		return data;
	}

	public void setData(List<com.evampsaanga.ulduzum.getcategories.Datum> data) {
		this.data = data;
	}

}
