package com.evampsaanga.ulduzumupdate.getunusedcodes;

import java.util.ArrayList;
import java.util.List;

import com.evampsaanga.bakcell.responseheaders.BaseResponse;

public class GetUnusedCodeResponseClient extends BaseResponse {

	List<UnusedCode> data = new ArrayList<>();

	public List<UnusedCode> getData() {
		return data;
	}

	public void setData(List<UnusedCode> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "GetUnusedCodeResponseClient [data=" + data + "]";
	}

}
