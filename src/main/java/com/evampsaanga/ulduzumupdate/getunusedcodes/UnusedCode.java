package com.evampsaanga.ulduzumupdate.getunusedcodes;

public class UnusedCode {
	private String identical_code;
	private String created;

	public String getIdentical_code() {
		return identical_code;
	}

	public void setIdentical_code(String identical_code) {
		this.identical_code = identical_code;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	@Override
	public String toString() {
		return "UnusedCode [identical_code=" + identical_code + ", created=" + created + "]";
	}

}
