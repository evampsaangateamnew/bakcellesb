package com.nsn.devicemanagement.wsdl.common._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for connectivityBootstrapScenario complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="connectivityBootstrapScenario">
 *   &lt;complexContent>
 *     &lt;extension base="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}Scenario">
 *       &lt;sequence>
 *         &lt;element name="locale" type="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}localeWrapper" minOccurs="0"/>
 *         &lt;element name="shouldConfirm" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="connectivity" type="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}setting" minOccurs="0"/>
 *         &lt;element name="message" type="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}message" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "connectivityBootstrapScenario", propOrder = { "locale", "shouldConfirm", "connectivity", "message" })
public class ConnectivityBootstrapScenario extends Scenario {
	protected LocaleWrapper locale;
	protected boolean shouldConfirm;
	protected Setting connectivity;
	protected Message message;

	/**
	 * Gets the value of the locale property.
	 * 
	 * @return possible object is {@link LocaleWrapper }
	 * 
	 */
	public LocaleWrapper getLocale() {
		return locale;
	}

	/**
	 * Sets the value of the locale property.
	 * 
	 * @param value
	 *            allowed object is {@link LocaleWrapper }
	 * 
	 */
	public void setLocale(LocaleWrapper value) {
		this.locale = value;
	}

	/**
	 * Gets the value of the shouldConfirm property.
	 * 
	 */
	public boolean isShouldConfirm() {
		return shouldConfirm;
	}

	/**
	 * Sets the value of the shouldConfirm property.
	 * 
	 */
	public void setShouldConfirm(boolean value) {
		this.shouldConfirm = value;
	}

	/**
	 * Gets the value of the connectivity property.
	 * 
	 * @return possible object is {@link Setting }
	 * 
	 */
	public Setting getConnectivity() {
		return connectivity;
	}

	/**
	 * Sets the value of the connectivity property.
	 * 
	 * @param value
	 *            allowed object is {@link Setting }
	 * 
	 */
	public void setConnectivity(Setting value) {
		this.connectivity = value;
	}

	/**
	 * Gets the value of the message property.
	 * 
	 * @return possible object is {@link Message }
	 * 
	 */
	public Message getMessage() {
		return message;
	}

	/**
	 * Sets the value of the message property.
	 * 
	 * @param value
	 *            allowed object is {@link Message }
	 * 
	 */
	public void setMessage(Message value) {
		this.message = value;
	}
}
