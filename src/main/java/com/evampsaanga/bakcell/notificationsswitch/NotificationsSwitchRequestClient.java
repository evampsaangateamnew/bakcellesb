package com.evampsaanga.bakcell.notificationsswitch;

import com.evampsaanga.bakcell.requestheaders.BaseRequest;

public class NotificationsSwitchRequestClient extends BaseRequest {
	String enable = "";

	public String getEnable() {
		return enable;
	}

	public void setEnable(String enable) {
		this.enable = enable;
	}
}
