package com.evampsaanga.bakcell.notificationadmin;

import com.evampsaanga.bakcell.requestheaders.BaseRequest;

public class GetHistoryNotificationsRequest extends BaseRequest {

	private String userMsisdn;

	public String getUserMsisdn() {
		return userMsisdn;
	}

	public void setUserMsisdn(String userMsisdn) {
		this.userMsisdn = userMsisdn;
	}

	@Override
	public String toString() {
		return "GetHistoryNotificationsRequest [userMsisdn=" + userMsisdn + ", toString()=" + super.toString() + "]";
	}

}
