package com.evampsaanga.bakcell.verifypassport;

import com.evampsaanga.bakcell.requestheaders.BaseRequest;

public class VerifyPassportRequest extends BaseRequest {
	String passPortNumber = "";

	/**
	 * @return the passPortNumber
	 */
	public String getPassPortNumber() {
		return passPortNumber;
	}

	/**
	 * @param passPortNumber
	 *            the passPortNumber to set
	 */
	public void setPassPortNumber(String passPortNumber) {
		this.passPortNumber = passPortNumber;
	}
}
