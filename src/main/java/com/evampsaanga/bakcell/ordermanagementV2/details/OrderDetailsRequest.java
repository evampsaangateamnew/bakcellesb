package com.evampsaanga.bakcell.ordermanagementV2.details;

import com.evampsaanga.bakcell.requestheaders.BaseRequest;

public class OrderDetailsRequest extends BaseRequest{

	private String orderId;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
}
