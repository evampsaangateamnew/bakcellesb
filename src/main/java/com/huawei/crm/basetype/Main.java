package com.huawei.crm.basetype;

import java.text.SimpleDateFormat;
import java.util.Date;
import com.huawei.crm.basetype.ens.OfferingExtParameterInfo;
import com.huawei.crm.basetype.ens.OfferingInfo;
import com.huawei.crm.basetype.ens.OfferingKey;
import com.huawei.crm.basetype.ens.OrderInfo;
import com.huawei.crm.basetype.ens.OrderItemInfo;
import com.huawei.crm.basetype.ens.OrderItemValue;
import com.huawei.crm.basetype.ens.OrderItems;
import com.huawei.crm.basetype.ens.ProductInfo;
import com.huawei.crm.basetype.ens.RequestHeader;
import com.huawei.crm.basetype.ens.SubscriberInfo;
import com.huawei.crm.service.ens.HuaweiCRMPortType;
import com.huawei.crm.service.ens.OrderHandle;
import com.huawei.crm.service.ens.SubmitOrderRequest;
import com.huawei.crm.service.ens.SubmitOrderResponse;
import com.huawei.crm.service.ens.SubmitRequestBody;

public class Main {
	public static String generateTransactionID() {
		String tID = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()).toString() + 0.2 * 1000;
		return tID.substring(0, tID.indexOf('.'));
	}

	public static void main(String[] args) {
		/*
		 * Action type of an manage family member. 1 Add 2 Modify 3 Detele
		 */
		RequestHeader reqH = new RequestHeader();
		reqH.setVersion("1");
		reqH.setChannelId("1");
		reqH.setTechnicalChannelId("53");
		reqH.setAccessUser("ussd" + "");
		reqH.setAccessPwd("r8q0a5WwGNboj9I35XzNcQ==");
		reqH.setTransactionId(generateTransactionID());
		HuaweiCRMPortType port = new OrderHandle().getHuaweiCRMPort();
		SubmitOrderRequest submitOrderRequestMsgReq = new SubmitOrderRequest();
		submitOrderRequestMsgReq.setRequestHeader(reqH);
		OfferingInfo offeringInfo = new OfferingInfo();
		offeringInfo.setActionType(args[0]);
		SubmitRequestBody submitRequestBody = new SubmitRequestBody();
		OrderInfo oInfo = new OrderInfo();
		oInfo.setOrderType("CO075");
		OrderItemInfo oitemInfo = new OrderItemInfo();
		oitemInfo.setOrderItemType("CO075");
		submitRequestBody.setOrder(oInfo);//
		OrderItems OrderItems = new OrderItems();
		OrderItemValue OrderItemValue = new OrderItemValue();
		OrderItemValue.setOrderItemInfo(oitemInfo);
		// List<OrderItemValue> orderItem = new ArrayList<OrderItemValue>();
		SubscriberInfo subscriber = new SubscriberInfo();
		subscriber.setServiceNumber("556061015");
		OfferingKey offeringId = new OfferingKey();
		offeringId.setOfferingId("1473278558");
		offeringInfo.setOfferingId(offeringId);
		ProductInfo productInfo = new ProductInfo();
		productInfo.setProductId("1023");
		productInfo.setSelectFlag("1");
		OfferingExtParameterInfo offeringExtPaInfo = new OfferingExtParameterInfo();
		offeringExtPaInfo.setParamName("C_FNINFO");
		offeringExtPaInfo.setParamValue("2");
		OfferingExtParameterInfo offeringExt = new OfferingExtParameterInfo();
		offeringExt.setParamName("C_FN_SERIAL_NO");
		offeringExt.setParamValue(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
		OfferingExtParameterInfo offeringExt2 = new OfferingExtParameterInfo();
		offeringExt2.setParamName("C_FN_NUMBER");
		offeringExt2.setParamValue("994555957013");
		// OfferingExtParameterList extParamList=new OfferingExtParameterList();
		// offeringExtPaInfo.setExtParamList(extParamList);
		// offeringExtPaInfo.getExtParamList().getParameterInfo().add(offeringExt);
		// offeringExtPaInfo.getExtParamList().getParameterInfo().add(offeringExt2);
		// offeringExtPaInfo.setParamValue("2");
		// extParaList.getParameterInfo().add(offeringExtPaInfo);
		productInfo.getExtParamList().getParameterInfo().add(offeringExtPaInfo);
		productInfo.getExtParamList().getParameterInfo().add(offeringExt);
		productInfo.getExtParamList().getParameterInfo().add(offeringExt2);
		offeringInfo.getProductList().getProductInfo().add(productInfo);
		// offeringInfo.setProductList(productInfoList);
		subscriber.getSupplementaryOfferingList().getOfferingInfo().add(offeringInfo);
		OrderItemValue.setSubscriber(subscriber);
		OrderItems.getOrderItem().add(OrderItemValue);
		submitRequestBody.setOrderItems(OrderItems);//
		submitOrderRequestMsgReq.setSubmitRequestBody(submitRequestBody);
		SubmitOrderResponse response = port.submitOrder(submitOrderRequestMsgReq);
		response.getResponseHeader().getRequestHeader();
		// port.
	}

	public void manipulateFNF() {
	}
}
