
package com.huawei.bss.soaif._interface.common.createorder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BillMedium complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BillMedium">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BillMediumId" type="{http://www.huawei.com/bss/soaif/interface/common/}BillMediumId" minOccurs="0"/>
 *         &lt;element name="BillMediumCode" type="{http://www.huawei.com/bss/soaif/interface/common/}BillMediumCode" minOccurs="0"/>
 *         &lt;element name="BillContentType" type="{http://www.huawei.com/bss/soaif/interface/common/}BillContentType" minOccurs="0"/>
 *         &lt;element name="BillMediumInfo" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="512"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BillMedium", propOrder = {
    "billMediumId",
    "billMediumCode",
    "billContentType",
    "billMediumInfo"
})
public class BillMedium {

    @XmlElement(name = "BillMediumId")
    protected String billMediumId;
    @XmlElement(name = "BillMediumCode")
    protected String billMediumCode;
    @XmlElement(name = "BillContentType")
    protected String billContentType;
    @XmlElement(name = "BillMediumInfo")
    protected String billMediumInfo;

    /**
     * Gets the value of the billMediumId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillMediumId() {
        return billMediumId;
    }

    /**
     * Sets the value of the billMediumId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillMediumId(String value) {
        this.billMediumId = value;
    }

    /**
     * Gets the value of the billMediumCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillMediumCode() {
        return billMediumCode;
    }

    /**
     * Sets the value of the billMediumCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillMediumCode(String value) {
        this.billMediumCode = value;
    }

    /**
     * Gets the value of the billContentType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillContentType() {
        return billContentType;
    }

    /**
     * Sets the value of the billContentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillContentType(String value) {
        this.billContentType = value;
    }

    /**
     * Gets the value of the billMediumInfo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillMediumInfo() {
        return billMediumInfo;
    }

    /**
     * Sets the value of the billMediumInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillMediumInfo(String value) {
        this.billMediumInfo = value;
    }

}
