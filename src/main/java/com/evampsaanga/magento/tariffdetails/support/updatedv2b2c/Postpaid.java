package com.evampsaanga.magento.tariffdetails.support.updatedv2b2c;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Postpaid extends TarrifDataV2{
	@JsonProperty("BusinessCorporate")
	private BusinessCorporate[] BusinessCorporate;
	@JsonProperty("KlassPostpaid")
	private KlassPostpaid[] KlassPostpaid;
	@JsonProperty("BusinessIndividual")
	private BusinessCorporate[] BusinessIndividual;//BusinessIndividual[] BusinessIndividual;
	public BusinessCorporate[] getBusinessCorporate() {
		return BusinessCorporate;
	}
	public void setBusinessCorporate(BusinessCorporate[] businessCorporate) {
		BusinessCorporate = businessCorporate;
	}
	public KlassPostpaid[] getKlassPostpaid() {
		return KlassPostpaid;
	}
	public void setKlassPostpaid(KlassPostpaid[] klassPostpaid) {
		KlassPostpaid = klassPostpaid;
	}
//	public BusinessIndividual[] getBusinessIndividual() {
//		return BusinessIndividual;
//	}
//	public void setBusinessIndividual(BusinessIndividual[] businessIndividual) {
//		BusinessIndividual = businessIndividual;
//	}
	
	public BusinessCorporate[] getBusinessIndividual() {
		return BusinessIndividual;
	}
	public void setBusinessIndividual(BusinessCorporate[] businessIndividual) {
		BusinessIndividual = businessIndividual;
	}
	
	
}
