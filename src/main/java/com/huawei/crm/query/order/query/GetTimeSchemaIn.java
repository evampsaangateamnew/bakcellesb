
package com.huawei.crm.query.order.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetTimeSchemaIn complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetTimeSchemaIn">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="TimeSchemaID" type="{http://crm.huawei.com/basetype/}TimeSchemaID" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetTimeSchemaIn", propOrder = {

})
public class GetTimeSchemaIn {

    @XmlElement(name = "TimeSchemaID")
    protected String timeSchemaID;

    /**
     * Gets the value of the timeSchemaID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeSchemaID() {
        return timeSchemaID;
    }

    /**
     * Sets the value of the timeSchemaID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeSchemaID(String value) {
        this.timeSchemaID = value;
    }

}
