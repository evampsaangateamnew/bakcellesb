package com.evampsaanga.magento.tariffdetails.support.updatedv2b2c;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BusinessIndividual {
	private String subscribable = "";
	@JsonProperty("details")
	private Details details;
	@JsonProperty("description")
	private Description description;
	@JsonProperty
	private Prices prices;
	@JsonProperty
	private KlassHeader header;

	
	public String getSubscribable() {
		return subscribable;
	}

	public void setSubscribable(String subscribable) {
		this.subscribable = subscribable;
	}

	public Details getDetails() {
		return details;
	}

	public void setDetails(Details details) {
		this.details = details;
	}

	public Description getDescription() {
		return description;
	}

	public void setDescription(Description description) {
		this.description = description;
	}
}