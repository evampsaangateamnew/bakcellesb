package com.evampsaanga.bakcell.getcustomerrequest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "GetCustomerRequestClient")
public class GetCustomerRequestClient extends com.evampsaanga.bakcell.requestheaders.BaseRequest {
	
}
