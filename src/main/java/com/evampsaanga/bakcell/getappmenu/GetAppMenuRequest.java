package com.evampsaanga.bakcell.getappmenu;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import com.evampsaanga.bakcell.requestheaders.BaseRequest;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "GetAppMenuRequest")
public class GetAppMenuRequest extends BaseRequest {
}
