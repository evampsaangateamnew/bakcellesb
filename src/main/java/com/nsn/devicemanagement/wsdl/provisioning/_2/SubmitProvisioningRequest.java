package com.nsn.devicemanagement.wsdl.provisioning._2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import com.nsn.devicemanagement.wsdl.common._2.DmMap;

/**
 * <p>
 * Java class for submitProvisioningRequest complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="submitProvisioningRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="msisdn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="imeiOrPhoneType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="properties" type="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}dmMap" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="specialProperties" type="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}dmMap" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "submitProvisioningRequest", propOrder = { "msisdn", "imeiOrPhoneType", "properties",
		"specialProperties" })
public class SubmitProvisioningRequest {
	protected String msisdn;
	protected String imeiOrPhoneType;
	protected List<DmMap> properties;
	protected DmMap specialProperties;

	/**
	 * Gets the value of the msisdn property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMsisdn() {
		return msisdn;
	}

	/**
	 * Sets the value of the msisdn property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMsisdn(String value) {
		this.msisdn = value;
	}

	/**
	 * Gets the value of the imeiOrPhoneType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getImeiOrPhoneType() {
		return imeiOrPhoneType;
	}

	/**
	 * Sets the value of the imeiOrPhoneType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setImeiOrPhoneType(String value) {
		this.imeiOrPhoneType = value;
	}

	/**
	 * Gets the value of the properties property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the properties property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getProperties().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list {@link DmMap }
	 * 
	 * 
	 */
	public List<DmMap> getProperties() {
		if (properties == null) {
			properties = new ArrayList<DmMap>();
		}
		return this.properties;
	}

	/**
	 * Gets the value of the specialProperties property.
	 * 
	 * @return possible object is {@link DmMap }
	 * 
	 */
	public DmMap getSpecialProperties() {
		return specialProperties;
	}

	/**
	 * Sets the value of the specialProperties property.
	 * 
	 * @param value
	 *            allowed object is {@link DmMap }
	 * 
	 */
	public void setSpecialProperties(DmMap value) {
		this.specialProperties = value;
	}
}
