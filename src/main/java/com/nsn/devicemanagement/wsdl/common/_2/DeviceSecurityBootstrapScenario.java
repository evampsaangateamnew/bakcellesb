package com.nsn.devicemanagement.wsdl.common._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for deviceSecurityBootstrapScenario complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="deviceSecurityBootstrapScenario">
 *   &lt;complexContent>
 *     &lt;extension base="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}Scenario">
 *       &lt;sequence>
 *         &lt;element name="deviceSecurityMethod" type="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}deviceSecurityMethod" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deviceSecurityBootstrapScenario", propOrder = { "deviceSecurityMethod" })
public class DeviceSecurityBootstrapScenario extends Scenario {
	protected DeviceSecurityMethod deviceSecurityMethod;

	/**
	 * Gets the value of the deviceSecurityMethod property.
	 * 
	 * @return possible object is {@link DeviceSecurityMethod }
	 * 
	 */
	public DeviceSecurityMethod getDeviceSecurityMethod() {
		return deviceSecurityMethod;
	}

	/**
	 * Sets the value of the deviceSecurityMethod property.
	 * 
	 * @param value
	 *            allowed object is {@link DeviceSecurityMethod }
	 * 
	 */
	public void setDeviceSecurityMethod(DeviceSecurityMethod value) {
		this.deviceSecurityMethod = value;
	}
}
