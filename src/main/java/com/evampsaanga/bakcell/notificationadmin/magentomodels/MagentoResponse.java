package com.evampsaanga.bakcell.notificationadmin.magentomodels;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "resultCode", "msg", "data", "execTime" })
public class MagentoResponse {
	@JsonProperty("resultCode")
	private String resultCode;
	@JsonProperty("msg")
	private String msg;
	@JsonProperty("execTime")
	private Double execTime;
	@JsonProperty("data")
	private Data data;

	@JsonProperty("resultCode")
	public String getResultCode() {
		return resultCode;
	}

	@JsonProperty("resultCode")
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	@JsonProperty("execTime")
	public Double getExecTime() {
		return execTime;
	}

	@JsonProperty("execTime")
	public void setExecTime(Double execTime) {
		this.execTime = execTime;
	}

	@JsonProperty("msg")
	public String getMsg() {
		return msg;
	}

	@JsonProperty("msg")
	public void setMsg(String msg) {
		this.msg = msg;
	}

	@JsonProperty("data")
	public Data getData() {
		return data;
	}

	@JsonProperty("data")
	public void setData(Data data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "MagentoResponse [resultCode=" + resultCode + ", execTime=" + execTime + ", msg=" + msg + ", data="
				+ data + "]";
	}

}
