package com.evampsaanga.validator.rules;

public interface ValidationRules {

	public ValidationResult validateObject(Object object);
	
}
