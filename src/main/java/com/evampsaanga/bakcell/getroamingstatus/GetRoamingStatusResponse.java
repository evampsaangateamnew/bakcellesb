package com.evampsaanga.bakcell.getroamingstatus;

import com.evampsaanga.bakcell.responseheaders.BaseResponse;

public class GetRoamingStatusResponse extends BaseResponse {

	private boolean isActive = false;

	/**
	 * @return the isActive
	 */
	public boolean isActive() {
		return isActive;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "GetRoamingStatusResponse [isActive=" + isActive + "]";
	}

}
