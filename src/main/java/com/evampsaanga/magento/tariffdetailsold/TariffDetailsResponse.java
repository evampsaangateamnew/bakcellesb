package com.evampsaanga.magento.tariffdetailsold;

import com.evampsaanga.bakcell.responseheaders.BaseResponse;

public class TariffDetailsResponse extends BaseResponse {
	private com.evampsaanga.magento.tariffdetailsold.Data data = new Data();

	public com.evampsaanga.magento.tariffdetailsold.Data getData() {
		return data;
	}

	public void setData(com.evampsaanga.magento.tariffdetailsold.Data data) {
		this.data = data;
	}
}
