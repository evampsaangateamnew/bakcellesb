package com.evampsaanga.services;

import com.evampsaanga.developer.utils.SoapHandlerService;
import com.nsn.devicemanagement.wsdl.provisioning._2.ProvisioningIf;
import com.nsn.devicemanagement.wsdl.provisioning._2.ProvisioningService;

public class SADM {
	private static ProvisioningIf portType = null;

	private SADM() {
	}

	public static synchronized ProvisioningIf getInstance() {
		if (portType == null) {
			portType = new ProvisioningService().getProvisioningPort();
			SoapHandlerService.configureBinding(portType);
		}
		return portType;
	}
}
