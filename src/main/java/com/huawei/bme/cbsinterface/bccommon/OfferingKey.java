package com.huawei.bme.cbsinterface.bccommon;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for OfferingKey complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="OfferingKey">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OfferingID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="PurchaseSeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OfferingKey", propOrder = { "offeringID", "purchaseSeq" })
public class OfferingKey {
	@XmlElement(name = "OfferingID", required = true, nillable = true)
	protected BigInteger offeringID;
	@XmlElement(name = "PurchaseSeq")
	protected String purchaseSeq;

	/**
	 * Gets the value of the offeringID property.
	 * 
	 * @return possible object is {@link BigInteger }
	 * 
	 */
	public BigInteger getOfferingID() {
		return offeringID;
	}

	/**
	 * Sets the value of the offeringID property.
	 * 
	 * @param value
	 *            allowed object is {@link BigInteger }
	 * 
	 */
	public void setOfferingID(BigInteger value) {
		this.offeringID = value;
	}

	/**
	 * Gets the value of the purchaseSeq property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPurchaseSeq() {
		return purchaseSeq;
	}

	/**
	 * Sets the value of the purchaseSeq property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPurchaseSeq(String value) {
		this.purchaseSeq = value;
	}
}
