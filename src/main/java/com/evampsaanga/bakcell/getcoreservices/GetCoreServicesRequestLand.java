package com.evampsaanga.bakcell.getcoreservices;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.amqimplementationsesb.LogsType;
import com.evampsaanga.bakcell.getnetworksettings.GetNetworkSettingsRequestClient;
import com.evampsaanga.bakcell.getsubscriber.CRMSubscriberService;
import com.evampsaanga.bakcell.getsubscriber.CoreServicesCategoryItem;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.ThirdPartyNames;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;
import com.huawei.crm.basetype.GetSubProductInfo;
import com.huawei.crm.query.GetNetworkSettingDataResponse;
import com.huawei.crm.query.GetSubscriberResponse;

@Path("/bakcell")
public class GetCoreServicesRequestLand {
	public static final Logger logger = Logger.getLogger("bakcellLogs-V2");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetCoreServicesResponse Get(@Header("credentials") String credential, @Body() String requestBody) {
		GetCoreServicesResponse resp = new GetCoreServicesResponse();
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.GET_CORE_SERVICES_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.GET_CORE_SERVICES);
		logs.setTableType(LogsType.GetCoreServices);
		try {
			logger.info("Request Landed on GetCoreServicesRequestLand " + requestBody);
			String credentials = null;
			GetCoreServicesRequest cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, GetCoreServicesRequest.class);
			} catch (Exception ex1) {
				logger.error(Helper.GetException(ex1));
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				if (cclient.getIsB2B() != null && cclient.getIsB2B().equals("true")) {
					logs.setTransactionName(Transactions.GET_CORE_SERVICES_TRANSACTION_NAME_B2B);
				}
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setIsB2B(cclient.getIsB2B());
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					logger.info("credentials validated.....");
					CRMSubscriberService crmSub = new CRMSubscriberService();
					GetSubscriberResponse subsResponse = crmSub.GetSubscriberRequest(cclient.getmsisdn());

					GetNetworkSettingsRequestClient networkclient = new GetNetworkSettingsRequestClient();
					networkclient.setMsisdn(cclient.getmsisdn());
					GetNetworkSettingDataResponse responseFromBakcell = com.evampsaanga.bakcell.getnetworksettings.GetNetworkSettingsLand
							.RequestSoap(com.evampsaanga.bakcell.getnetworksettings.GetNetworkSettingsLand
									.getRequestHeader(), networkclient);

					List<GetSubProductInfo> getsubproductinfo = responseFromBakcell.getGetNetworkSettingDataBody()
							.getGetNetworkSettingDataList();

					if (Constants.CRMSUBACCESSCODE.equals(subsResponse.getResponseHeader().getRetCode())) {
						if (cclient.getGroupType() == null)
							cclient.setGroupType("");
						if (cclient.getBrand() != null && cclient.getBrand().equalsIgnoreCase("business"))
							cclient.setBrand("Business");
						resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						String freeFor = getFreeForValue(cclient.getUserType(), cclient.getBrand(),
								cclient.getAccountType(), cclient.getGroupType());
						String visibleFor = getVisibleForValue(cclient.getUserType(), cclient.getBrand(),
								cclient.getAccountType(), cclient.getGroupType());
						resp.getData()
								.setCoreServices(crmSub.getCoreServices(subsResponse.getGetSubscriberBody(),
										cclient.getLang(), cclient.getIsFrom(), freeFor, visibleFor, getsubproductinfo,
										cclient.getAccountType()));
						
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						
						for (CoreServices coreServices : resp.getData().getCoreServices()) {
								for (CoreServicesCategoryItem categoryItem : coreServices.getCoreServicesList()) {
									if (categoryItem.getOfferingId().equalsIgnoreCase(Constants.PAYG_OFFERING_ID)) {
										logger.info(cclient.getmsisdn() + "-PayG Status swaping");
										categoryItem.setStatus(
												categoryItem.getStatus().equalsIgnoreCase("active") ? "Inactive" : "Active");
									}
								}
						}
						logger.info(cclient.getmsisdn() + "-Response returned from " + Transactions.GET_CORE_SERVICES_TRANSACTION_NAME + resp.toString());
						
					} else {
						resp.setReturnCode(subsResponse.getResponseHeader().getRetCode());
						resp.setReturnMsg(subsResponse.getResponseHeader().getRetMsg());
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					}
					logs.updateLog(logs);

					return resp;
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else
				resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
	}

	private String getVisibleForValue(String userType, String brand, String accountType, String groupType) {
		StringBuffer visibleFor = new StringBuffer();
		visibleFor.append(userType);
		visibleFor.append("-" + brand);

		if (accountType != null && !accountType.trim().equals("")) {
			if (!userType.equalsIgnoreCase("prepaid")) {
				visibleFor.append("-" + accountType);
			}
		}

		if (groupType != null && !groupType.trim().equals("")) {
			visibleFor.append("-" + groupType);
		}
		return visibleFor.toString();
	}

	private String getFreeForValue(String userType, String brand, String accountType, String groupType) {
		StringBuffer freeFor = new StringBuffer();
		freeFor.append(userType);
		freeFor.append("-" + brand);

		if (accountType != null && !accountType.trim().equals("")) {
			if (!userType.equalsIgnoreCase("prepaid")) {
				freeFor.append("-" + accountType);
			}
		}

		if (groupType != null && !groupType.trim().equals("")) {
			freeFor.append("-" + groupType);
		}
		return freeFor.toString();
	}
}
