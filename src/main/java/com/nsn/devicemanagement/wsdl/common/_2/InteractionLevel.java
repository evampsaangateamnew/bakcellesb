package com.nsn.devicemanagement.wsdl.common._2;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for InteractionLevel.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * <p>
 * 
 * <pre>
 * &lt;simpleType name="InteractionLevel">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="LOW"/>
 *     &lt;enumeration value="MEDIUM"/>
 *     &lt;enumeration value="HIGH"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "InteractionLevel")
@XmlEnum
public enum InteractionLevel {
	LOW, MEDIUM, HIGH;
	public String value() {
		return name();
	}

	public static InteractionLevel fromValue(String v) {
		return valueOf(v);
	}
}
