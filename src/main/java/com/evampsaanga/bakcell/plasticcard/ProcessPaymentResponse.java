package com.evampsaanga.bakcell.plasticcard;

import com.evampsaanga.bakcell.responseheaders.BaseResponse;

public class ProcessPaymentResponse extends BaseResponse {

	@Override
	public String toString() {
		return "GetCardTokenResponse [toString()=" + super.toString() + "]";
	}

}
