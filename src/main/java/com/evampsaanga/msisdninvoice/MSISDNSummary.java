
package com.evampsaanga.msisdninvoice;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "msisdn",
    "limit",
    "total"
})
public class MSISDNSummary {

    @JsonProperty("msisdn")
    private String msisdn;
    @JsonProperty("limit")
    private String limit;
    @JsonProperty("total")
    private String total;
   
    
	@JsonProperty("msisdn")
	public String getMsisdn() {
		return msisdn;
	}
    @JsonProperty("msisdn")
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
    @JsonProperty("limit")
	public String getLimit() {
		return limit;
	}
    @JsonProperty("limit")
	public void setLimit(String limit) {
		this.limit = limit;
	}
    @JsonProperty("total")
	public String getTotal() {
		return total;
	}
    @JsonProperty("total")
	public void setTotal(String total) {
		this.total = total;
	}
   
}
