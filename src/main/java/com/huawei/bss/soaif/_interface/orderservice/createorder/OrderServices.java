
package com.huawei.bss.soaif._interface.orderservice.createorder;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "OrderServices", targetNamespace = "http://www.huawei.com/bss/soaif/interface/OrderService/", wsdlLocation = "file:/C:/Users/EvampSaanga/Desktop/apache-cxf-3.2.7/bin/HWBSS_Order_V10.wsdl")
public class OrderServices
    extends Service
{

    private final static URL ORDERSERVICES_WSDL_LOCATION;
    private final static WebServiceException ORDERSERVICES_EXCEPTION;
    private final static QName ORDERSERVICES_QNAME = new QName("http://www.huawei.com/bss/soaif/interface/OrderService/", "OrderServices");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
//            url = new URL("file:/C:/Users/EvampSaanga/Desktop/apache-cxf-3.2.7/bin/HWBSS_Order_V10.wsdl");
            url = new URL("file:/opt/createorder/HWBSS_Order_V10.wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        ORDERSERVICES_WSDL_LOCATION = url;
        ORDERSERVICES_EXCEPTION = e;
    }

    public OrderServices() {
        super(__getWsdlLocation(), ORDERSERVICES_QNAME);
    }

    public OrderServices(WebServiceFeature... features) {
        super(__getWsdlLocation(), ORDERSERVICES_QNAME, features);
    }

    public OrderServices(URL wsdlLocation) {
        super(wsdlLocation, ORDERSERVICES_QNAME);
    }

    public OrderServices(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, ORDERSERVICES_QNAME, features);
    }

    public OrderServices(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public OrderServices(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns OrderInterfaces
     */
    @WebEndpoint(name = "OrderServicePort")
    public OrderInterfaces getOrderServicePort() {
        return super.getPort(new QName("http://www.huawei.com/bss/soaif/interface/OrderService/", "OrderServicePort"), OrderInterfaces.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns OrderInterfaces
     */
    @WebEndpoint(name = "OrderServicePort")
    public OrderInterfaces getOrderServicePort(WebServiceFeature... features) {
        return super.getPort(new QName("http://www.huawei.com/bss/soaif/interface/OrderService/", "OrderServicePort"), OrderInterfaces.class, features);
    }

    private static URL __getWsdlLocation() {
        if (ORDERSERVICES_EXCEPTION!= null) {
            throw ORDERSERVICES_EXCEPTION;
        }
        return ORDERSERVICES_WSDL_LOCATION;
    }

}
