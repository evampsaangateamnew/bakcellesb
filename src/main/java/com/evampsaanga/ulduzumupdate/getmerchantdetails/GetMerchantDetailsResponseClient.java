package com.evampsaanga.ulduzumupdate.getmerchantdetails;

import com.evampsaanga.bakcell.responseheaders.BaseResponse;

public class GetMerchantDetailsResponseClient extends BaseResponse {

	Merchant data;

	/**
	 * @return the data
	 */
	public Merchant getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(Merchant data) {
		this.data = data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "GetMerchantsResponseClient [data=" + data + ", getReturnMsg()=" + getReturnMsg() + ", getReturnCode()="
				+ getReturnCode() + "]";
	}

}
