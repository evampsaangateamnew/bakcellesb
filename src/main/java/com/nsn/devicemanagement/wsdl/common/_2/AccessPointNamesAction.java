package com.nsn.devicemanagement.wsdl.common._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for AccessPointNamesAction complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="AccessPointNamesAction">
 *   &lt;complexContent>
 *     &lt;extension base="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}ProvisioningAction">
 *       &lt;sequence>
 *         &lt;element name="services" type="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}dmMap" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccessPointNamesAction", propOrder = { "services" })
public class AccessPointNamesAction extends ProvisioningAction {
	protected DmMap services;

	/**
	 * Gets the value of the services property.
	 * 
	 * @return possible object is {@link DmMap }
	 * 
	 */
	public DmMap getServices() {
		return services;
	}

	/**
	 * Sets the value of the services property.
	 * 
	 * @param value
	 *            allowed object is {@link DmMap }
	 * 
	 */
	public void setServices(DmMap value) {
		this.services = value;
	}
}
