package com.huawei.bme.cbsinterface.arservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.cbs.ar.wsservice.arcommon.BankInfo;
import com.huawei.cbs.ar.wsservice.arcommon.SimpleProperty;
import com.huawei.cbs.ar.wsservice.arcommon.SubAccessCode;

/**
 * <p>
 * Java class for RechargeRequest complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="RechargeRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RechargeSerialNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RechargeType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RechargeChannelID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RechargeObj">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;choice>
 *                   &lt;element name="SubAccessCode" type="{http://cbs.huawei.com/ar/wsservice/arcommon}SubAccessCode"/>
 *                   &lt;element name="AcctAccessCode">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;extension base="{http://cbs.huawei.com/ar/wsservice/arcommon}AcctAccessCode">
 *                           &lt;sequence>
 *                             &lt;element name="PayType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/extension>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/choice>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="RechargeInfo">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;choice>
 *                   &lt;element name="CardPayment">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;choice>
 *                             &lt;element name="CardPinNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CardSequence" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/choice>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="CashPayment" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="PaymentMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="BankInfo" type="{http://cbs.huawei.com/ar/wsservice/arcommon}BankInfo" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/choice>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="AdditionalProperty" type="{http://cbs.huawei.com/ar/wsservice/arcommon}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RechargeRequest", propOrder = { "rechargeSerialNo", "rechargeType", "rechargeChannelID", "rechargeObj",
		"rechargeInfo", "currencyID", "additionalProperty" })
public class RechargeRequest {
	@XmlElement(name = "RechargeSerialNo")
	protected String rechargeSerialNo;
	@XmlElement(name = "RechargeType")
	protected String rechargeType;
	@XmlElement(name = "RechargeChannelID")
	protected String rechargeChannelID;
	@XmlElement(name = "RechargeObj", required = true)
	protected RechargeRequest.RechargeObj rechargeObj;
	@XmlElement(name = "RechargeInfo", required = true)
	protected RechargeRequest.RechargeInfo rechargeInfo;
	@XmlElement(name = "CurrencyID")
	protected BigInteger currencyID;
	@XmlElement(name = "AdditionalProperty")
	protected List<SimpleProperty> additionalProperty;

	/**
	 * Gets the value of the rechargeSerialNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRechargeSerialNo() {
		return rechargeSerialNo;
	}

	/**
	 * Sets the value of the rechargeSerialNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRechargeSerialNo(String value) {
		this.rechargeSerialNo = value;
	}

	/**
	 * Gets the value of the rechargeType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRechargeType() {
		return rechargeType;
	}

	/**
	 * Sets the value of the rechargeType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRechargeType(String value) {
		this.rechargeType = value;
	}

	/**
	 * Gets the value of the rechargeChannelID property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRechargeChannelID() {
		return rechargeChannelID;
	}

	/**
	 * Sets the value of the rechargeChannelID property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRechargeChannelID(String value) {
		this.rechargeChannelID = value;
	}

	/**
	 * Gets the value of the rechargeObj property.
	 * 
	 * @return possible object is {@link RechargeRequest.RechargeObj }
	 * 
	 */
	public RechargeRequest.RechargeObj getRechargeObj() {
		return rechargeObj;
	}

	/**
	 * Sets the value of the rechargeObj property.
	 * 
	 * @param value
	 *            allowed object is {@link RechargeRequest.RechargeObj }
	 * 
	 */
	public void setRechargeObj(RechargeRequest.RechargeObj value) {
		this.rechargeObj = value;
	}

	/**
	 * Gets the value of the rechargeInfo property.
	 * 
	 * @return possible object is {@link RechargeRequest.RechargeInfo }
	 * 
	 */
	public RechargeRequest.RechargeInfo getRechargeInfo() {
		return rechargeInfo;
	}

	/**
	 * Sets the value of the rechargeInfo property.
	 * 
	 * @param value
	 *            allowed object is {@link RechargeRequest.RechargeInfo }
	 * 
	 */
	public void setRechargeInfo(RechargeRequest.RechargeInfo value) {
		this.rechargeInfo = value;
	}

	/**
	 * Gets the value of the currencyID property.
	 * 
	 * @return possible object is {@link BigInteger }
	 * 
	 */
	public BigInteger getCurrencyID() {
		return currencyID;
	}

	/**
	 * Sets the value of the currencyID property.
	 * 
	 * @param value
	 *            allowed object is {@link BigInteger }
	 * 
	 */
	public void setCurrencyID(BigInteger value) {
		this.currencyID = value;
	}

	/**
	 * Gets the value of the additionalProperty property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the additionalProperty property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getAdditionalProperty().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link SimpleProperty }
	 * 
	 * 
	 */
	public List<SimpleProperty> getAdditionalProperty() {
		if (additionalProperty == null) {
			additionalProperty = new ArrayList<SimpleProperty>();
		}
		return this.additionalProperty;
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;choice>
	 *         &lt;element name="CardPayment">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;choice>
	 *                   &lt;element name="CardPinNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                   &lt;element name="CardSequence" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                 &lt;/choice>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="CashPayment" maxOccurs="unbounded">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="PaymentMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *                   &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}long"/>
	 *                   &lt;element name="BankInfo" type="{http://cbs.huawei.com/ar/wsservice/arcommon}BankInfo" minOccurs="0"/>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *       &lt;/choice>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "cardPayment", "cashPayment" })
	public static class RechargeInfo {
		@XmlElement(name = "CardPayment")
		protected RechargeRequest.RechargeInfo.CardPayment cardPayment;
		@XmlElement(name = "CashPayment")
		protected List<RechargeRequest.RechargeInfo.CashPayment> cashPayment;

		/**
		 * Gets the value of the cardPayment property.
		 * 
		 * @return possible object is
		 *         {@link RechargeRequest.RechargeInfo.CardPayment }
		 * 
		 */
		public RechargeRequest.RechargeInfo.CardPayment getCardPayment() {
			return cardPayment;
		}

		/**
		 * Sets the value of the cardPayment property.
		 * 
		 * @param value
		 *            allowed object is
		 *            {@link RechargeRequest.RechargeInfo.CardPayment }
		 * 
		 */
		public void setCardPayment(RechargeRequest.RechargeInfo.CardPayment value) {
			this.cardPayment = value;
		}

		/**
		 * Gets the value of the cashPayment property.
		 * 
		 * <p>
		 * This accessor method returns a reference to the live list, not a
		 * snapshot. Therefore any modification you make to the returned list
		 * will be present inside the JAXB object. This is why there is not a
		 * <CODE>set</CODE> method for the cashPayment property.
		 * 
		 * <p>
		 * For example, to add a new item, do as follows:
		 * 
		 * <pre>
		 * getCashPayment().add(newItem);
		 * </pre>
		 * 
		 * 
		 * <p>
		 * Objects of the following type(s) are allowed in the list
		 * {@link RechargeRequest.RechargeInfo.CashPayment }
		 * 
		 * 
		 */
		public List<RechargeRequest.RechargeInfo.CashPayment> getCashPayment() {
			if (cashPayment == null) {
				cashPayment = new ArrayList<RechargeRequest.RechargeInfo.CashPayment>();
			}
			return this.cashPayment;
		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * 
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;choice>
		 *         &lt;element name="CardPinNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *         &lt;element name="CardSequence" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *       &lt;/choice>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "cardPinNumber", "cardSequence" })
		public static class CardPayment {
			@XmlElement(name = "CardPinNumber")
			protected String cardPinNumber;
			@XmlElement(name = "CardSequence")
			protected String cardSequence;

			/**
			 * Gets the value of the cardPinNumber property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getCardPinNumber() {
				return cardPinNumber;
			}

			/**
			 * Sets the value of the cardPinNumber property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setCardPinNumber(String value) {
				this.cardPinNumber = value;
			}

			/**
			 * Gets the value of the cardSequence property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getCardSequence() {
				return cardSequence;
			}

			/**
			 * Sets the value of the cardSequence property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setCardSequence(String value) {
				this.cardSequence = value;
			}
		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * 
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="PaymentMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
		 *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}long"/>
		 *         &lt;element name="BankInfo" type="{http://cbs.huawei.com/ar/wsservice/arcommon}BankInfo" minOccurs="0"/>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "paymentMethod", "amount", "bankInfo" })
		public static class CashPayment {
			@XmlElement(name = "PaymentMethod")
			protected String paymentMethod;
			@XmlElement(name = "Amount")
			protected long amount;
			@XmlElement(name = "BankInfo")
			protected BankInfo bankInfo;

			/**
			 * Gets the value of the paymentMethod property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getPaymentMethod() {
				return paymentMethod;
			}

			/**
			 * Sets the value of the paymentMethod property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setPaymentMethod(String value) {
				this.paymentMethod = value;
			}

			/**
			 * Gets the value of the amount property.
			 * 
			 */
			public long getAmount() {
				return amount;
			}

			/**
			 * Sets the value of the amount property.
			 * 
			 */
			public void setAmount(long value) {
				this.amount = value;
			}

			/**
			 * Gets the value of the bankInfo property.
			 * 
			 * @return possible object is {@link BankInfo }
			 * 
			 */
			public BankInfo getBankInfo() {
				return bankInfo;
			}

			/**
			 * Sets the value of the bankInfo property.
			 * 
			 * @param value
			 *            allowed object is {@link BankInfo }
			 * 
			 */
			public void setBankInfo(BankInfo value) {
				this.bankInfo = value;
			}
		}
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;choice>
	 *         &lt;element name="SubAccessCode" type="{http://cbs.huawei.com/ar/wsservice/arcommon}SubAccessCode"/>
	 *         &lt;element name="AcctAccessCode">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;extension base="{http://cbs.huawei.com/ar/wsservice/arcommon}AcctAccessCode">
	 *                 &lt;sequence>
	 *                   &lt;element name="PayType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *                 &lt;/sequence>
	 *               &lt;/extension>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *       &lt;/choice>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "subAccessCode", "acctAccessCode" })
	public static class RechargeObj {
		@XmlElement(name = "SubAccessCode")
		protected SubAccessCode subAccessCode;
		@XmlElement(name = "AcctAccessCode")
		protected RechargeRequest.RechargeObj.AcctAccessCode acctAccessCode;

		/**
		 * Gets the value of the subAccessCode property.
		 * 
		 * @return possible object is {@link SubAccessCode }
		 * 
		 */
		public SubAccessCode getSubAccessCode() {
			return subAccessCode;
		}

		/**
		 * Sets the value of the subAccessCode property.
		 * 
		 * @param value
		 *            allowed object is {@link SubAccessCode }
		 * 
		 */
		public void setSubAccessCode(SubAccessCode value) {
			this.subAccessCode = value;
		}

		/**
		 * Gets the value of the acctAccessCode property.
		 * 
		 * @return possible object is
		 *         {@link RechargeRequest.RechargeObj.AcctAccessCode }
		 * 
		 */
		public RechargeRequest.RechargeObj.AcctAccessCode getAcctAccessCode() {
			return acctAccessCode;
		}

		/**
		 * Sets the value of the acctAccessCode property.
		 * 
		 * @param value
		 *            allowed object is
		 *            {@link RechargeRequest.RechargeObj.AcctAccessCode }
		 * 
		 */
		public void setAcctAccessCode(RechargeRequest.RechargeObj.AcctAccessCode value) {
			this.acctAccessCode = value;
		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * 
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;extension base="{http://cbs.huawei.com/ar/wsservice/arcommon}AcctAccessCode">
		 *       &lt;sequence>
		 *         &lt;element name="PayType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
		 *       &lt;/sequence>
		 *     &lt;/extension>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "payType" })
		public static class AcctAccessCode extends com.huawei.cbs.ar.wsservice.arcommon.AcctAccessCode {
			@XmlElement(name = "PayType")
			protected String payType;

			/**
			 * Gets the value of the payType property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getPayType() {
				return payType;
			}

			/**
			 * Sets the value of the payType property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setPayType(String value) {
				this.payType = value;
			}
		}
	}
}
