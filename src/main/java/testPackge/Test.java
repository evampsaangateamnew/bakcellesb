package testPackge;

import java.security.NoSuchAlgorithmException;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class Test {

	public static void main(String[] args) throws JSONException, NoSuchAlgorithmException {
		// JSONObject jsonObj = new JSONObject();
		// jsonObj.put("msisdn", "994551234567");
		// String JSON = jsonObj.toString();
		// String SECRETKEY = "x8f8ea1a1da962c552a0b0abf5a34376";
		//
		// MessageDigest md = MessageDigest.getInstance("MD5");
		// md.update((JSON + "+" + SECRETKEY).getBytes());
		// byte[] digest = md.digest();
		//
		// StringBuilder sb = new StringBuilder();
		// for (byte b : digest) {
		// sb.append(String.format("%02x", b));
		// }
		//
		// System.out.println(sb.toString());
		// JSONObject branchObject = jobj.getJSONObject("branches");
		String json = "{\"result\":\"success\",\"data\":{\"id\":181,\"name\":\"181\",\"coord_lat\":\"181\",\"coord_lng\":\"181\",\"address\":\"H\u0259s\u0259n Abdullayev 5A, AZ1040\",\"branches\":{}}}";

		JSONObject jobj = new JSONObject(json);

		JSONObject singleObject = jobj.getJSONObject("data");

		JSONObject brObj = singleObject.getJSONObject("branches");
		JSONArray branches = new JSONArray();
		if (brObj.length() > 0) {

			for (int j = 0; j < brObj.length(); j++) {
				JSONObject brSignleObj = brObj.getJSONObject(String.valueOf(j));
				branches.put(brSignleObj);
			}
			System.out.println("Branches SIZE: " + branches.length());
		} else {

			if (singleObject.get("address") != null) {
				JSONObject jObj = new JSONObject();

				jObj.put("name", singleObject.get("name"));
				jObj.put("coord_lat", singleObject.get("coord_lat"));
				jObj.put("coord_lng", singleObject.get("coord_lng"));
				jObj.put("address", singleObject.get("address"));
				branches.put(jObj);

				singleObject.put("branches", branches);
			}
		}

		System.out.println("Objects SIZE: " + singleObject.length());

		jobj.put("data", singleObject);
		System.out.println(jobj);
	}

}
