package com.evampsaanga.bakcell.suplementryservices;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Header {
	private String validityLabel;
	private String callLable;
	private String appOfferFilter;
	private String whatsappDestinationLabel1;
	private String tag;
	private String whatsappDestinationLabel2;
	private String freeResourceFDestinationValue2;
	private String renew;
	private String freeResourceFDestinationValue1;
	private String offerGroupNameValue;
	private String freeResourceFValue;
	private String type;
	private String freeResourceGDestinationValue1;
	private String freeResourceGDestinationValue2;
	private String freeResourceGLable;
	private String internetDestinationLabel2;
	private String internetDestinationLabel1;
	private String whatsappUnit;
	private String freeResourceHUnit;
	private String freeResourceGUnit;
	private String whatsappValue;
	private String callUnit;
	private String freeResourceGDestinationLabel1;
	private String freeResourceHIcon;
	private String freeResourceGDestinationLabel2;
	private String tagValidToDate;
	private String freeResourceGIcon;
	private String internetLabel;
	private String deactivate;
	private String whatsappIcon;
	private String validityInformation;
	private String freeResourceGDesc;
	private String internetDesc;
	private String freeResourceFDestinationLabel1;
	private String freeResourceEDestinationLabel2;
	private String freeResourceEDestinationLabel1;
	private String freeResourceFDestinationLabel2;
	private String smsIcon;
	private String freeResourceHDestinationLabel1;
	private String callDestinationLabel1;
	private String freeResourceHDestinationLabel2;
	private String callDestinationLabel2;
	private String smsDesc;
	private String freeResourceFDesc;
	private String freeResourceEIcon;
	private String freeResourceFUnit;
	private String callDesc;
	private String price;
	private String whatsappDesc;
	private String freeResourceFLable;
	private String freeResourceGValue;
	private String freeResourceHLable;
	private String internetUnit;
	private String freeResourceHDesc;
	private String internetValue;
	private String smsLable;
	private String internetIcon;
	private String id;
	private String callValue;
	private String internetDestinationValue1;
	private String internetDestinationValue2;
	private String smsUnit;
	private String name;
	private String whatsappLable;
	private String freeResourceHDestinationValue1;
	private String freeResourceHDestinationValue2;
	private String freeResourceEUnit;
	private String tagValidFromDate;
	private String smsValue;
	private String smsDestinationLabel1;
	private String freeResourceEDestinationValue1;
	private String smsDestinationValue2;
	private String smsDestinationLabel2;
	private String smsDestinationValue1;
	private String freeResourceHValue;
	private String freeResourceELable;
	private String callIcon;
	private String freeResourceEDestinationValue2;
	private String offerGroupNameLabel;
	private String whatsappDestinationValue2;
	private String whatsappDestinationValue1;
	private String freeResourceEValue;
	private String validityDate;
	private String offeringId;
	private String freeResourceFIcon;
	private String callDestinationValue2;
	private String callDestinationValue1;
	private String freeResourceEDesc;
	private String offerLevel;
	private Integer sortOrder;
	private Integer sortOrderMS;
	private String preReqOfferId;
	private String isTopUp;
	private String enabledForLoyalty;
	private String loyaltyPoints;
	private String loyaltyPriceOnPurchase;

	public String getPreReqOfferId() {
		return preReqOfferId;
	}

	public void setPreReqOfferId(String preReqOfferId) {
		this.preReqOfferId = preReqOfferId;
	}

	public String getIsTopUp() {
		return isTopUp;
	}

	public void setIsTopUp(String isTopUp) {
		this.isTopUp = isTopUp;
	}

	public String getValidityLabel() {
		return validityLabel;
	}

	public void setValidityLabel(String validityLabel) {
		this.validityLabel = validityLabel;
	}

	public String getCallLable() {
		return callLable;
	}

	public void setCallLable(String callLable) {
		this.callLable = callLable;
	}

	public String getAppOfferFilter() {
		return appOfferFilter;
	}

	public void setAppOfferFilter(String appOfferFilter) {
		this.appOfferFilter = appOfferFilter;
	}

	public String getWhatsappDestinationLabel1() {
		return whatsappDestinationLabel1;
	}

	public void setWhatsappDestinationLabel1(String whatsappDestinationLabel1) {
		this.whatsappDestinationLabel1 = whatsappDestinationLabel1;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getWhatsappDestinationLabel2() {
		return whatsappDestinationLabel2;
	}

	public void setWhatsappDestinationLabel2(String whatsappDestinationLabel2) {
		this.whatsappDestinationLabel2 = whatsappDestinationLabel2;
	}

	public String getFreeResourceFDestinationValue2() {
		return freeResourceFDestinationValue2;
	}

	public void setFreeResourceFDestinationValue2(String freeResourceFDestinationValue2) {
		this.freeResourceFDestinationValue2 = freeResourceFDestinationValue2;
	}

	public String getRenew() {
		return renew;
	}

	public void setRenew(String renew) {
		this.renew = renew;
	}

	public String getFreeResourceFDestinationValue1() {
		return freeResourceFDestinationValue1;
	}

	public void setFreeResourceFDestinationValue1(String freeResourceFDestinationValue1) {
		this.freeResourceFDestinationValue1 = freeResourceFDestinationValue1;
	}

	public String getOfferGroupNameValue() {
		return offerGroupNameValue;
	}

	public void setOfferGroupNameValue(String offerGroupNameValue) {
		this.offerGroupNameValue = offerGroupNameValue;
	}

	public String getFreeResourceFValue() {
		return freeResourceFValue;
	}

	public void setFreeResourceFValue(String freeResourceFValue) {
		this.freeResourceFValue = freeResourceFValue;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFreeResourceGDestinationValue1() {
		return freeResourceGDestinationValue1;
	}

	public void setFreeResourceGDestinationValue1(String freeResourceGDestinationValue1) {
		this.freeResourceGDestinationValue1 = freeResourceGDestinationValue1;
	}

	public String getFreeResourceGDestinationValue2() {
		return freeResourceGDestinationValue2;
	}

	public void setFreeResourceGDestinationValue2(String freeResourceGDestinationValue2) {
		this.freeResourceGDestinationValue2 = freeResourceGDestinationValue2;
	}

	public String getFreeResourceGLable() {
		return freeResourceGLable;
	}

	public void setFreeResourceGLable(String freeResourceGLable) {
		this.freeResourceGLable = freeResourceGLable;
	}

	public String getInternetDestinationLabel2() {
		return internetDestinationLabel2;
	}

	public void setInternetDestinationLabel2(String internetDestinationLabel2) {
		this.internetDestinationLabel2 = internetDestinationLabel2;
	}

	public String getInternetDestinationLabel1() {
		return internetDestinationLabel1;
	}

	public void setInternetDestinationLabel1(String internetDestinationLabel1) {
		this.internetDestinationLabel1 = internetDestinationLabel1;
	}

	public String getWhatsappUnit() {
		return whatsappUnit;
	}

	public void setWhatsappUnit(String whatsappUnit) {
		this.whatsappUnit = whatsappUnit;
	}

	public String getFreeResourceHUnit() {
		return freeResourceHUnit;
	}

	public void setFreeResourceHUnit(String freeResourceHUnit) {
		this.freeResourceHUnit = freeResourceHUnit;
	}

	public String getFreeResourceGUnit() {
		return freeResourceGUnit;
	}

	public void setFreeResourceGUnit(String freeResourceGUnit) {
		this.freeResourceGUnit = freeResourceGUnit;
	}

	public String getWhatsappValue() {
		return whatsappValue;
	}

	public void setWhatsappValue(String whatsappValue) {
		this.whatsappValue = whatsappValue;
	}

	public String getCallUnit() {
		return callUnit;
	}

	public void setCallUnit(String callUnit) {
		this.callUnit = callUnit;
	}

	public String getFreeResourceGDestinationLabel1() {
		return freeResourceGDestinationLabel1;
	}

	public void setFreeResourceGDestinationLabel1(String freeResourceGDestinationLabel1) {
		this.freeResourceGDestinationLabel1 = freeResourceGDestinationLabel1;
	}

	public String getFreeResourceHIcon() {
		return freeResourceHIcon;
	}

	public void setFreeResourceHIcon(String freeResourceHIcon) {
		this.freeResourceHIcon = freeResourceHIcon;
	}

	public String getFreeResourceGDestinationLabel2() {
		return freeResourceGDestinationLabel2;
	}

	public void setFreeResourceGDestinationLabel2(String freeResourceGDestinationLabel2) {
		this.freeResourceGDestinationLabel2 = freeResourceGDestinationLabel2;
	}

	public String getTagValidToDate() {
		return tagValidToDate;
	}

	public void setTagValidToDate(String tagValidToDate) {
		this.tagValidToDate = tagValidToDate;
	}

	public String getFreeResourceGIcon() {
		return freeResourceGIcon;
	}

	public void setFreeResourceGIcon(String freeResourceGIcon) {
		this.freeResourceGIcon = freeResourceGIcon;
	}

	public String getInternetLabel() {
		return internetLabel;
	}

	public void setInternetLabel(String internetLabel) {
		this.internetLabel = internetLabel;
	}

	public String getDeactivate() {
		return deactivate;
	}

	public void setDeactivate(String deactivate) {
		this.deactivate = deactivate;
	}

	public String getWhatsappIcon() {
		return whatsappIcon;
	}

	public void setWhatsappIcon(String whatsappIcon) {
		this.whatsappIcon = whatsappIcon;
	}

	public String getValidityInformation() {
		return validityInformation;
	}

	public void setValidityInformation(String validityInformation) {
		this.validityInformation = validityInformation;
	}

	public String getFreeResourceGDesc() {
		return freeResourceGDesc;
	}

	public void setFreeResourceGDesc(String freeResourceGDesc) {
		this.freeResourceGDesc = freeResourceGDesc;
	}

	public String getInternetDesc() {
		return internetDesc;
	}

	public void setInternetDesc(String internetDesc) {
		this.internetDesc = internetDesc;
	}

	public String getFreeResourceFDestinationLabel1() {
		return freeResourceFDestinationLabel1;
	}

	public void setFreeResourceFDestinationLabel1(String freeResourceFDestinationLabel1) {
		this.freeResourceFDestinationLabel1 = freeResourceFDestinationLabel1;
	}

	public String getFreeResourceEDestinationLabel2() {
		return freeResourceEDestinationLabel2;
	}

	public void setFreeResourceEDestinationLabel2(String freeResourceEDestinationLabel2) {
		this.freeResourceEDestinationLabel2 = freeResourceEDestinationLabel2;
	}

	public String getFreeResourceEDestinationLabel1() {
		return freeResourceEDestinationLabel1;
	}

	public void setFreeResourceEDestinationLabel1(String freeResourceEDestinationLabel1) {
		this.freeResourceEDestinationLabel1 = freeResourceEDestinationLabel1;
	}

	public String getFreeResourceFDestinationLabel2() {
		return freeResourceFDestinationLabel2;
	}

	public void setFreeResourceFDestinationLabel2(String freeResourceFDestinationLabel2) {
		this.freeResourceFDestinationLabel2 = freeResourceFDestinationLabel2;
	}

	public String getSmsIcon() {
		return smsIcon;
	}

	public void setSmsIcon(String smsIcon) {
		this.smsIcon = smsIcon;
	}

	public String getFreeResourceHDestinationLabel1() {
		return freeResourceHDestinationLabel1;
	}

	public void setFreeResourceHDestinationLabel1(String freeResourceHDestinationLabel1) {
		this.freeResourceHDestinationLabel1 = freeResourceHDestinationLabel1;
	}

	public String getCallDestinationLabel1() {
		return callDestinationLabel1;
	}

	public void setCallDestinationLabel1(String callDestinationLabel1) {
		this.callDestinationLabel1 = callDestinationLabel1;
	}

	public String getFreeResourceHDestinationLabel2() {
		return freeResourceHDestinationLabel2;
	}

	public void setFreeResourceHDestinationLabel2(String freeResourceHDestinationLabel2) {
		this.freeResourceHDestinationLabel2 = freeResourceHDestinationLabel2;
	}

	public String getCallDestinationLabel2() {
		return callDestinationLabel2;
	}

	public void setCallDestinationLabel2(String callDestinationLabel2) {
		this.callDestinationLabel2 = callDestinationLabel2;
	}

	public String getSmsDesc() {
		return smsDesc;
	}

	public void setSmsDesc(String smsDesc) {
		this.smsDesc = smsDesc;
	}

	public String getFreeResourceFDesc() {
		return freeResourceFDesc;
	}

	public void setFreeResourceFDesc(String freeResourceFDesc) {
		this.freeResourceFDesc = freeResourceFDesc;
	}

	public String getFreeResourceEIcon() {
		return freeResourceEIcon;
	}

	public void setFreeResourceEIcon(String freeResourceEIcon) {
		this.freeResourceEIcon = freeResourceEIcon;
	}

	public String getFreeResourceFUnit() {
		return freeResourceFUnit;
	}

	public void setFreeResourceFUnit(String freeResourceFUnit) {
		this.freeResourceFUnit = freeResourceFUnit;
	}

	public String getCallDesc() {
		return callDesc;
	}

	public void setCallDesc(String callDesc) {
		this.callDesc = callDesc;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getWhatsappDesc() {
		return whatsappDesc;
	}

	public void setWhatsappDesc(String whatsappDesc) {
		this.whatsappDesc = whatsappDesc;
	}

	public String getFreeResourceFLable() {
		return freeResourceFLable;
	}

	public void setFreeResourceFLable(String freeResourceFLable) {
		this.freeResourceFLable = freeResourceFLable;
	}

	public String getFreeResourceGValue() {
		return freeResourceGValue;
	}

	public void setFreeResourceGValue(String freeResourceGValue) {
		this.freeResourceGValue = freeResourceGValue;
	}

	public String getFreeResourceHLable() {
		return freeResourceHLable;
	}

	public void setFreeResourceHLable(String freeResourceHLable) {
		this.freeResourceHLable = freeResourceHLable;
	}

	public String getInternetUnit() {
		return internetUnit;
	}

	public void setInternetUnit(String internetUnit) {
		this.internetUnit = internetUnit;
	}

	public String getFreeResourceHDesc() {
		return freeResourceHDesc;
	}

	public void setFreeResourceHDesc(String freeResourceHDesc) {
		this.freeResourceHDesc = freeResourceHDesc;
	}

	public String getInternetValue() {
		return internetValue;
	}

	public void setInternetValue(String internetValue) {
		this.internetValue = internetValue;
	}

	public String getSmsLable() {
		return smsLable;
	}

	public void setSmsLable(String smsLable) {
		this.smsLable = smsLable;
	}

	public String getInternetIcon() {
		return internetIcon;
	}

	public void setInternetIcon(String internetIcon) {
		this.internetIcon = internetIcon;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCallValue() {
		return callValue;
	}

	public void setCallValue(String callValue) {
		this.callValue = callValue;
	}

	public String getInternetDestinationValue1() {
		return internetDestinationValue1;
	}

	public void setInternetDestinationValue1(String internetDestinationValue1) {
		this.internetDestinationValue1 = internetDestinationValue1;
	}

	public String getInternetDestinationValue2() {
		return internetDestinationValue2;
	}

	public void setInternetDestinationValue2(String internetDestinationValue2) {
		this.internetDestinationValue2 = internetDestinationValue2;
	}

	public String getSmsUnit() {
		return smsUnit;
	}

	public void setSmsUnit(String smsUnit) {
		this.smsUnit = smsUnit;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getWhatsappLable() {
		return whatsappLable;
	}

	public void setWhatsappLable(String whatsappLable) {
		this.whatsappLable = whatsappLable;
	}

	public String getFreeResourceHDestinationValue1() {
		return freeResourceHDestinationValue1;
	}

	public void setFreeResourceHDestinationValue1(String freeResourceHDestinationValue1) {
		this.freeResourceHDestinationValue1 = freeResourceHDestinationValue1;
	}

	public String getFreeResourceHDestinationValue2() {
		return freeResourceHDestinationValue2;
	}

	public void setFreeResourceHDestinationValue2(String freeResourceHDestinationValue2) {
		this.freeResourceHDestinationValue2 = freeResourceHDestinationValue2;
	}

	public String getFreeResourceEUnit() {
		return freeResourceEUnit;
	}

	public void setFreeResourceEUnit(String freeResourceEUnit) {
		this.freeResourceEUnit = freeResourceEUnit;
	}

	public String getTagValidFromDate() {
		return tagValidFromDate;
	}

	public void setTagValidFromDate(String tagValidFromDate) {
		this.tagValidFromDate = tagValidFromDate;
	}

	public String getSmsValue() {
		return smsValue;
	}

	public void setSmsValue(String smsValue) {
		this.smsValue = smsValue;
	}

	public String getSmsDestinationLabel1() {
		return smsDestinationLabel1;
	}

	public void setSmsDestinationLabel1(String smsDestinationLabel1) {
		this.smsDestinationLabel1 = smsDestinationLabel1;
	}

	public String getFreeResourceEDestinationValue1() {
		return freeResourceEDestinationValue1;
	}

	public void setFreeResourceEDestinationValue1(String freeResourceEDestinationValue1) {
		this.freeResourceEDestinationValue1 = freeResourceEDestinationValue1;
	}

	public String getSmsDestinationValue2() {
		return smsDestinationValue2;
	}

	public void setSmsDestinationValue2(String smsDestinationValue2) {
		this.smsDestinationValue2 = smsDestinationValue2;
	}

	public String getSmsDestinationLabel2() {
		return smsDestinationLabel2;
	}

	public void setSmsDestinationLabel2(String smsDestinationLabel2) {
		this.smsDestinationLabel2 = smsDestinationLabel2;
	}

	public String getSmsDestinationValue1() {
		return smsDestinationValue1;
	}

	public void setSmsDestinationValue1(String smsDestinationValue1) {
		this.smsDestinationValue1 = smsDestinationValue1;
	}

	public String getFreeResourceHValue() {
		return freeResourceHValue;
	}

	public void setFreeResourceHValue(String freeResourceHValue) {
		this.freeResourceHValue = freeResourceHValue;
	}

	public String getFreeResourceELable() {
		return freeResourceELable;
	}

	public void setFreeResourceELable(String freeResourceELable) {
		this.freeResourceELable = freeResourceELable;
	}

	public String getCallIcon() {
		return callIcon;
	}

	public void setCallIcon(String callIcon) {
		this.callIcon = callIcon;
	}

	public String getFreeResourceEDestinationValue2() {
		return freeResourceEDestinationValue2;
	}

	public void setFreeResourceEDestinationValue2(String freeResourceEDestinationValue2) {
		this.freeResourceEDestinationValue2 = freeResourceEDestinationValue2;
	}

	public String getOfferGroupNameLabel() {
		return offerGroupNameLabel;
	}

	public void setOfferGroupNameLabel(String offerGroupNameLabel) {
		this.offerGroupNameLabel = offerGroupNameLabel;
	}

	public String getWhatsappDestinationValue2() {
		return whatsappDestinationValue2;
	}

	public void setWhatsappDestinationValue2(String whatsappDestinationValue2) {
		this.whatsappDestinationValue2 = whatsappDestinationValue2;
	}

	public String getWhatsappDestinationValue1() {
		return whatsappDestinationValue1;
	}

	public void setWhatsappDestinationValue1(String whatsappDestinationValue1) {
		this.whatsappDestinationValue1 = whatsappDestinationValue1;
	}

	public String getFreeResourceEValue() {
		return freeResourceEValue;
	}

	public void setFreeResourceEValue(String freeResourceEValue) {
		this.freeResourceEValue = freeResourceEValue;
	}

	public String getValidityDate() {
		return validityDate;
	}

	public void setValidityDate(String validityDate) {
		this.validityDate = validityDate;
	}

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	public String getFreeResourceFIcon() {
		return freeResourceFIcon;
	}

	public void setFreeResourceFIcon(String freeResourceFIcon) {
		this.freeResourceFIcon = freeResourceFIcon;
	}

	public String getCallDestinationValue2() {
		return callDestinationValue2;
	}

	public void setCallDestinationValue2(String callDestinationValue2) {
		this.callDestinationValue2 = callDestinationValue2;
	}

	public String getCallDestinationValue1() {
		return callDestinationValue1;
	}

	public void setCallDestinationValue1(String callDestinationValue1) {
		this.callDestinationValue1 = callDestinationValue1;
	}

	public String getFreeResourceEDesc() {
		return freeResourceEDesc;
	}

	public void setFreeResourceEDesc(String freeResourceEDesc) {
		this.freeResourceEDesc = freeResourceEDesc;
	}

	public String getOfferLevel() {
		return offerLevel;
	}

	public void setOfferLevel(String offerLevel) {
		this.offerLevel = offerLevel;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public Integer getSortOrderMS() {
		return sortOrderMS;
	}

	public void setSortOrderMS(Integer sortOrderMS) {
		this.sortOrderMS = sortOrderMS;
	}

	public String getEnabledForLoyalty() {
		return enabledForLoyalty;
	}

	public void setEnabledForLoyalty(String enabledForLoyalty) {
		this.enabledForLoyalty = enabledForLoyalty;
	}

	public String getLoyaltyPoints() {
		return loyaltyPoints;
	}

	public void setLoyaltyPoints(String loyaltyPoints) {
		this.loyaltyPoints = loyaltyPoints;
	}

	public String getLoyaltyPriceOnPurchase() {
		return loyaltyPriceOnPurchase;
	}

	public void setLoyaltyPriceOnPurchase(String loyaltyPriceOnPurchase) {
		this.loyaltyPriceOnPurchase = loyaltyPriceOnPurchase;
	}

	@Override
	public String toString() {
		return "Header [validityLabel=" + validityLabel + ", callLable=" + callLable + ", appOfferFilter="
				+ appOfferFilter + ", whatsappDestinationLabel1=" + whatsappDestinationLabel1 + ", tag=" + tag
				+ ", whatsappDestinationLabel2=" + whatsappDestinationLabel2 + ", freeResourceFDestinationValue2="
				+ freeResourceFDestinationValue2 + ", renew=" + renew + ", freeResourceFDestinationValue1="
				+ freeResourceFDestinationValue1 + ", offerGroupNameValue=" + offerGroupNameValue
				+ ", freeResourceFValue=" + freeResourceFValue + ", type=" + type + ", freeResourceGDestinationValue1="
				+ freeResourceGDestinationValue1 + ", freeResourceGDestinationValue2=" + freeResourceGDestinationValue2
				+ ", freeResourceGLable=" + freeResourceGLable + ", internetDestinationLabel2="
				+ internetDestinationLabel2 + ", internetDestinationLabel1=" + internetDestinationLabel1
				+ ", whatsappUnit=" + whatsappUnit + ", freeResourceHUnit=" + freeResourceHUnit + ", freeResourceGUnit="
				+ freeResourceGUnit + ", whatsappValue=" + whatsappValue + ", callUnit=" + callUnit
				+ ", freeResourceGDestinationLabel1=" + freeResourceGDestinationLabel1 + ", freeResourceHIcon="
				+ freeResourceHIcon + ", freeResourceGDestinationLabel2=" + freeResourceGDestinationLabel2
				+ ", tagValidToDate=" + tagValidToDate + ", freeResourceGIcon=" + freeResourceGIcon + ", internetLabel="
				+ internetLabel + ", deactivate=" + deactivate + ", whatsappIcon=" + whatsappIcon
				+ ", validityInformation=" + validityInformation + ", freeResourceGDesc=" + freeResourceGDesc
				+ ", internetDesc=" + internetDesc + ", freeResourceFDestinationLabel1="
				+ freeResourceFDestinationLabel1 + ", freeResourceEDestinationLabel2=" + freeResourceEDestinationLabel2
				+ ", freeResourceEDestinationLabel1=" + freeResourceEDestinationLabel1
				+ ", freeResourceFDestinationLabel2=" + freeResourceFDestinationLabel2 + ", smsIcon=" + smsIcon
				+ ", freeResourceHDestinationLabel1=" + freeResourceHDestinationLabel1 + ", callDestinationLabel1="
				+ callDestinationLabel1 + ", freeResourceHDestinationLabel2=" + freeResourceHDestinationLabel2
				+ ", callDestinationLabel2=" + callDestinationLabel2 + ", smsDesc=" + smsDesc + ", freeResourceFDesc="
				+ freeResourceFDesc + ", freeResourceEIcon=" + freeResourceEIcon + ", freeResourceFUnit="
				+ freeResourceFUnit + ", callDesc=" + callDesc + ", price=" + price + ", whatsappDesc=" + whatsappDesc
				+ ", freeResourceFLable=" + freeResourceFLable + ", freeResourceGValue=" + freeResourceGValue
				+ ", freeResourceHLable=" + freeResourceHLable + ", internetUnit=" + internetUnit
				+ ", freeResourceHDesc=" + freeResourceHDesc + ", internetValue=" + internetValue + ", smsLable="
				+ smsLable + ", internetIcon=" + internetIcon + ", id=" + id + ", callValue=" + callValue
				+ ", internetDestinationValue1=" + internetDestinationValue1 + ", internetDestinationValue2="
				+ internetDestinationValue2 + ", smsUnit=" + smsUnit + ", name=" + name + ", whatsappLable="
				+ whatsappLable + ", freeResourceHDestinationValue1=" + freeResourceHDestinationValue1
				+ ", freeResourceHDestinationValue2=" + freeResourceHDestinationValue2 + ", freeResourceEUnit="
				+ freeResourceEUnit + ", tagValidFromDate=" + tagValidFromDate + ", smsValue=" + smsValue
				+ ", smsDestinationLabel1=" + smsDestinationLabel1 + ", freeResourceEDestinationValue1="
				+ freeResourceEDestinationValue1 + ", smsDestinationValue2=" + smsDestinationValue2
				+ ", smsDestinationLabel2=" + smsDestinationLabel2 + ", smsDestinationValue1=" + smsDestinationValue1
				+ ", freeResourceHValue=" + freeResourceHValue + ", freeResourceELable=" + freeResourceELable
				+ ", callIcon=" + callIcon + ", freeResourceEDestinationValue2=" + freeResourceEDestinationValue2
				+ ", offerGroupNameLabel=" + offerGroupNameLabel + ", whatsappDestinationValue2="
				+ whatsappDestinationValue2 + ", whatsappDestinationValue1=" + whatsappDestinationValue1
				+ ", freeResourceEValue=" + freeResourceEValue + ", validityDate=" + validityDate + ", offeringId="
				+ offeringId + ", freeResourceFIcon=" + freeResourceFIcon + ", callDestinationValue2="
				+ callDestinationValue2 + ", callDestinationValue1=" + callDestinationValue1 + ", freeResourceEDesc="
				+ freeResourceEDesc + ", offerLevel=" + offerLevel + ", sortOrder=" + sortOrder + ", sortOrderMS="
				+ sortOrderMS + ", preReqOfferId=" + preReqOfferId + ", isTopUp=" + isTopUp + ", enabledForLoyalty="
				+ enabledForLoyalty + ", loyaltyPoints=" + loyaltyPoints + ", loyaltyPriceOnPurchase="
				+ loyaltyPriceOnPurchase + "]";
	}
}