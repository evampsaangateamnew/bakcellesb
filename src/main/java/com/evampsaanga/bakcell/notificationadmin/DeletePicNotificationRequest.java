package com.evampsaanga.bakcell.notificationadmin;

import com.evampsaanga.bakcell.requestheaders.BaseRequest;

public class DeletePicNotificationRequest extends BaseRequest {
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "DeletePicNotificationRequest [id=" + id + ", toString()=" + super.toString() + "]";
	}

}
