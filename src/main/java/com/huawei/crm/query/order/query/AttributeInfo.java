
package com.huawei.crm.query.order.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AttributeInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AttributeInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="AttrId" type="{http://crm.huawei.com/basetype/}AttrId"/>
 *         &lt;element name="AttrCode" type="{http://crm.huawei.com/basetype/}AttrCode"/>
 *         &lt;element name="AttrName" type="{http://crm.huawei.com/basetype/}AttrName" minOccurs="0"/>
 *         &lt;element name="AttrValue" type="{http://crm.huawei.com/basetype/}AttrValue" minOccurs="0"/>
 *         &lt;element name="AttrValueName" type="{http://crm.huawei.com/basetype/}AttrValueName" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AttributeInfo", propOrder = {

})
public class AttributeInfo {

    @XmlElement(name = "AttrId", required = true)
    protected String attrId;
    @XmlElement(name = "AttrCode", required = true)
    protected String attrCode;
    @XmlElement(name = "AttrName")
    protected String attrName;
    @XmlElement(name = "AttrValue")
    protected String attrValue;
    @XmlElement(name = "AttrValueName")
    protected String attrValueName;

    /**
     * Gets the value of the attrId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttrId() {
        return attrId;
    }

    /**
     * Sets the value of the attrId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttrId(String value) {
        this.attrId = value;
    }

    /**
     * Gets the value of the attrCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttrCode() {
        return attrCode;
    }

    /**
     * Sets the value of the attrCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttrCode(String value) {
        this.attrCode = value;
    }

    /**
     * Gets the value of the attrName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttrName() {
        return attrName;
    }

    /**
     * Sets the value of the attrName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttrName(String value) {
        this.attrName = value;
    }

    /**
     * Gets the value of the attrValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttrValue() {
        return attrValue;
    }

    /**
     * Sets the value of the attrValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttrValue(String value) {
        this.attrValue = value;
    }

    /**
     * Gets the value of the attrValueName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttrValueName() {
        return attrValueName;
    }

    /**
     * Sets the value of the attrValueName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttrValueName(String value) {
        this.attrValueName = value;
    }

}
