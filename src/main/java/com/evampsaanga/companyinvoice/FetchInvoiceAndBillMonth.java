package com.evampsaanga.companyinvoice;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.apache.camel.language.Constant;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.bakcell.db.DBBakcellFactory;
import com.evampsaanga.bakcell.getUsersV2.UsersGroupData;
import com.evampsaanga.cache.UserGroupDataCache;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.developer.utils.Helper;

public class FetchInvoiceAndBillMonth {
	
	public static final Logger logger = Logger.getLogger("bakcellLogs-V2");
	public static final Logger loggerV2 = Logger.getLogger("bakcellLogs-V2");
    public static String modeulName="FETCHING INVOICE ID AND BILL MONTH";
	private String invoiceId;
	private String billMonth;
	private String groupType;
	
	public String getGroupType() {
		return groupType;
	}
	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}
	public String getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}
	public String getBillMonth() {
		return billMonth;
	}
	public void setBillMonth(String billMonth) {
		this.billMonth = billMonth;
	}
	
	
	public void setInvoceidAndBillMonth(String accountCode, String billMonth,String lang,String msisdn,String selectedMsisdn,String customerId) throws IOException, Exception 
	{
		String accountId = getAccountIDFromCache(msisdn, selectedMsisdn, customerId, accountCode);
		String query;
		if(this.groupType!=null && this.groupType.equals("PaybySubs")  )
			query = "select BILL_MONTH,INVOICE_ID from V_E_CARE_BILL_INVOICE  where CRM_ACCT_ID = '"+accountId+"' and BILL_MONTH = to_date('"+Helper.getDateOfLastMonth(billMonth)+"','yyyymmdd')";
		else
			query = "select BILL_MONTH,INVOICE_ID from V_E_CARE_BILL_INVOICE  where ACCT_CODE = '"+accountId+"' and BILL_MONTH = to_date('"+Helper.getDateOfLastMonth(billMonth)+"','yyyymmdd')";

		try 
		{
			logMessage("Query: " + query);
			ResultSet rs = DBBakcellFactory.getConnection().prepareStatement(query).executeQuery();
			while (rs.next()) 
			{
				
				this.setBillMonth(rs.getDate("BILL_MONTH").toString());
				logMessage(rs.getDate("BILL_MONTH").toString());
				
				this.setInvoiceId(rs.getString("INVOICE_ID"));
				logMessage(rs.getString("INVOICE_ID"));
				
				
				
			}
			
		} catch (SQLException e) {
			logger.info(Helper.GetException(e));
			
		}
	
	}
	
	
	
	
	public void setInvoceidAndBillMonth(String accountCode, String billMonth,String lang) throws IOException, Exception 
	{
//		String accountId = getAccountIDFromCache(msisdn, selectedMsisdn, customerId, accountCode);
		
//		if(this.groupType.equals("PayBySubs"))
//			query = "select BILL_MONTH,INVOICE_ID from V_E_CARE_BILL_INVOICE  where CRM_ACCT_CODE = '"+accountId+"' and BILL_MONTH = to_date('"+Helper.getDateOfLastMonth(billMonth)+"','yyyymmdd')";
//		else
		String query = "select BILL_MONTH,INVOICE_ID from V_E_CARE_BILL_INVOICE  where ACCT_CODE = '"+accountCode+"' and BILL_MONTH = to_date('"+Helper.getDateOfLastMonth(billMonth)+"','yyyymmdd')";

		try 
		{
			logMessage("Query: " + query);
			ResultSet rs = DBBakcellFactory.getConnection().prepareStatement(query).executeQuery();
			while (rs.next()) 
			{
				
				this.setBillMonth(rs.getDate("BILL_MONTH").toString());
				logMessage(rs.getDate("BILL_MONTH").toString());
				
				this.setInvoiceId(rs.getString("INVOICE_ID"));
				logMessage(rs.getString("INVOICE_ID"));
				
				
				
			}
			
		} catch (SQLException e) {
			logger.info(Helper.GetException(e));
			
		}
	
	}
	
	
	
	private String getAccountIDFromCache(String msisdn,String selectedMsisdn,String customerId,String accountID) throws IOException, Exception 
	{
		// TODO Auto-generated method stub
		logMessage("*************************FETCH ACCOUNT ID*******************************");
		String result=null;
		ArrayList<UsersGroupData> usersGroupData = new ArrayList<UsersGroupData>();
		JSONObject getGroupRequest = new JSONObject();
		getGroupRequest.put("lang", "3");
		getGroupRequest.put("iP", "9.9.9.9");
		getGroupRequest.put("channel", "web");
		getGroupRequest.put("msisdn", selectedMsisdn);
		getGroupRequest.put("isB2B", "true");
		getGroupRequest.put("customerID", customerId);
		UserGroupDataCache userCache = new UserGroupDataCache();
		usersGroupData = (ArrayList<UsersGroupData>) userCache.checkUsersInCache(getGroupRequest.toString());
		
		
		String customerCrmAccountId=null;
		for(int k=0;k<usersGroupData.size();k++)
		{
			logMessage("Group Name : "+usersGroupData.get(k).getGroup_name());
			logMessage("MSISDN : "+usersGroupData.get(k).getMsisdn());
			if(usersGroupData.get(k).getMsisdn().equals(selectedMsisdn) && (usersGroupData.get(k).getGroup_name().equals("PaybySubs")))
			{
				String value=null;
				String query = "select CRM_ACCT_ID from V_E_CARE_BILL_CHARGE_OBJ t where PRI_IDENTITY='"+selectedMsisdn+"'";
				try {
					logMessage("Query: " + query);
					ResultSet rs = DBBakcellFactory.getConnection().prepareStatement(query).executeQuery();
					while (rs.next()) {
						value = rs.getString("CRM_ACCT_ID");
					}
				}
				catch (SQLException e) {
					logger.info("ERROR: "+ Helper.GetException(e));
					this.groupType=null;
					return null;
				}
				
					this.groupType=usersGroupData.get(k).getGroup_name();
					logMessage("group type "+this.groupType);
					logMessage("account id"+value);
					logMessage("*************************FETCH ACCOUNT ID*******************************"+result);
					return value;
				
//				customerCrmAccountId = usersGroupData.get(k).getCorp_crm_acct_id();
			}
			else
			{
				this.groupType=null;
				result = accountID;
			}
		}
		logMessage("*************************FETCH ACCOUNT ID*******************************"+result);
		return result;
		
	}
	private void logMessage(String message)
	{
		logger.info(modeulName+": "+message);
	}
	public static void main(String[] args) {
		Date firstDateOfPreviousMonth = null;
		try {
			firstDateOfPreviousMonth = new SimpleDateFormat("yyyy-MM-dd").parse("2018-01-01");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		SimpleDateFormat f = new SimpleDateFormat("yyyyMMdd");
		f.format(firstDateOfPreviousMonth);
		System.out.println(f.format(firstDateOfPreviousMonth).toString());
	}

}
