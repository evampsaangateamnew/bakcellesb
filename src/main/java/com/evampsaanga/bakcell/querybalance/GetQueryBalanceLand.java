package com.evampsaanga.bakcell.querybalance;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.amqimplementationsesb.LogsType;
import com.evampsaanga.bakcell.hlrweb.getquerybalanceRequest.GetQueryBalanceResponseClient;
import com.evampsaanga.bakcell.hlrweb.getquerybalanceRequest.HLRBalanceServices;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.ThirdPartyNames;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.developer.utils.SOAPLoggingHandler;
import com.evampsaanga.gethomepage.Balance;
import com.evampsaanga.services.CBSARService;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceRequest;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceRequestMsg;
import com.huawei.bss.soaif._interface.common.ReqHeader;
import com.huawei.cbs.ar.wsservice.arcommon.SubAccessCode;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceRequest.QueryObj;

/**
 * A bean which we use in the route
 */
@Path("/bakcell")
public class GetQueryBalanceLand {
	private static final Logger logger = Logger.getLogger("bakcellLogs-V2");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetQueryBalanceResponseClient Get(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.HLR_QUERY_BALANCE_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.HLR_QUERY_BALANCE);
		logs.setTableType(LogsType.HlrQueryBalance);
		try {
			logger.info("Request Landed on GetQueryBalanceLand:" + requestBody);
			GetQueryBalanceRequestClient cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, GetQueryBalanceRequestClient.class);
				logs.setUserType(cclient.getCustomerType());
				logs.setIsB2B(cclient.getIsB2B());
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				GetQueryBalanceResponseClient resp = new GetQueryBalanceResponseClient();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(ResponseCodes.ERROR_400_CODE);
				logs.setResponseDescription(ResponseCodes.ERROR_400);
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
					GetQueryBalanceResponseClient resp = new GetQueryBalanceResponseClient();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
				}
				if (credentials == null) {
					GetQueryBalanceResponseClient resp = new GetQueryBalanceResponseClient();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						GetQueryBalanceResponseClient res = new GetQueryBalanceResponseClient();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						logs.setResponseCode(ResponseCodes.ERROR_400);
						logs.setResponseDescription(verification);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);	return res;
					}
				} else {
					GetQueryBalanceResponseClient resp = new GetQueryBalanceResponseClient();
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode((ResponseCodes.ERROR_MSISDN_CODE));
					logs.setResponseDescription(ResponseCodes.ERROR_MSISDN);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					GetQueryBalanceResponseClient balanceResponse = new GetQueryBalanceResponseClient();
					try {
						HLRBalanceServices service = new HLRBalanceServices();
						Balance responseFromBakcell = service.getBalance(cclient.getmsisdn(),
								cclient.getCustomerType(),null);
						if (responseFromBakcell.getReturnCode().equals("0")) {
							balanceResponse.setBalance(responseFromBakcell);
							balanceResponse.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							balanceResponse.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
							logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						
							
							logs.updateLog(logs);					
							
							return balanceResponse;
						} else {
							balanceResponse.setReturnCode(responseFromBakcell.getReturnCode());
							balanceResponse.setReturnMsg(responseFromBakcell.getReturnMsg());
							logs.setResponseDescription(responseFromBakcell.getReturnMsg());
							logs.setResponseCode(responseFromBakcell.getReturnCode());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						
							
							logs.updateLog(logs);					
												return balanceResponse;
						}
					} catch (Exception ex) {
						GetQueryBalanceResponseClient resp = new GetQueryBalanceResponseClient();
						logger.error(Helper.GetException(ex));
						resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
						resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
						return resp;
					}
				} else {
					GetQueryBalanceResponseClient resp = new GetQueryBalanceResponseClient();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		GetQueryBalanceResponseClient resp = new GetQueryBalanceResponseClient();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.updateLog(logs);			
		return resp;
	}

	public ReqHeader getReqHeaderForQueryBalanceHLR() {
		ReqHeader reqHForQBalance = new ReqHeader();
		reqHForQBalance.setChannelId(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.ChannelId").trim());
		reqHForQBalance.setAccessUser(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.AccessUser").trim());
		reqHForQBalance.setAccessPwd(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.AccessPwd").trim());
		reqHForQBalance
				.setMIDWAREChannelID(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.MIDWAREChannelID").trim());
		reqHForQBalance
				.setMIDWAREAccessPwd(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.MIDWAREAccessPwd").trim());
		reqHForQBalance
				.setMIDWAREAccessUser(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.MIDWAREAccessUser").trim());
		reqHForQBalance.setTransactionId(Helper.generateTransactionID());
		return reqHForQBalance;
	}

}
