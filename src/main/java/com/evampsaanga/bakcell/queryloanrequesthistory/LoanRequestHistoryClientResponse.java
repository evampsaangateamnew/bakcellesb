package com.evampsaanga.bakcell.queryloanrequesthistory;

import java.util.ArrayList;
import com.evampsaanga.bakcell.responseheaders.BaseResponse;

public class LoanRequestHistoryClientResponse extends BaseResponse {
	ArrayList<LoanRequestHistory> loanRequest = new ArrayList<LoanRequestHistory>();

	public ArrayList<LoanRequestHistory> getLoanRequest() {
		return loanRequest;
	}

	/**
	 * @param loanRequest
	 *            the loanRequest to set
	 */
	public void setLoanRequest(ArrayList<LoanRequestHistory> loanRequest) {
		this.loanRequest = loanRequest;
	}

	public LoanRequestHistoryClientResponse() {
		super();
	}
}
