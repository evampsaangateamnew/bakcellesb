package com.nsn.devicemanagement.wsdl.common._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for Action complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="Action">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Action")
@XmlSeeAlso({ ApplicationStartAction.class, SoftKeyAssignmentAction.class, SendAutoLockSettingsAction.class,
		ApplicationUninstallAction.class, ApplicationStopAction.class, SendAdminPasswordAction.class,
		SyncRestoreAction.class, SettingVerificationAction.class, ApplicationInstallAction.class,
		DeviceEnableSecurityAction.class, FileDownloadAction.class, SettingDeleteAction.class,
		ScenarioInvocationAction.class, FirmwareUpgradeAction.class, DeviceLockAction.class,
		SecuritySettingsScanAction.class, FirmwareScanAction.class, EnablePolicyControlAction.class,
		DevDetailScanAction.class, SendMmsAction.class, ProvisioningAction.class, SettingScanAction.class,
		SendPasswordPolicySettingsAction.class, InteractionMessageAction.class, SyncBackupActionAction.class,
		DeviceEnableOTAAction.class, ApplicationScanAction.class, SettingDeleteScannedAction.class,
		DeviceDataWipeAction.class })
public abstract class Action {
}
