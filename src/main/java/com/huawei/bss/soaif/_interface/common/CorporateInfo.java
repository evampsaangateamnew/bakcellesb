package com.huawei.bss.soaif._interface.common;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for CorporateInfo complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="CorporateInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustType" type="{http://www.huawei.com/bss/soaif/interface/common/}OrganizationType" minOccurs="0"/>
 *         &lt;element name="CustName" type="{http://www.huawei.com/bss/soaif/interface/common/}CustName" minOccurs="0"/>
 *         &lt;element name="CustShortName" type="{http://www.huawei.com/bss/soaif/interface/common/}CustShortName" minOccurs="0"/>
 *         &lt;element name="CustSize" type="{http://www.huawei.com/bss/soaif/interface/common/}CustSize" minOccurs="0"/>
 *         &lt;element name="CustomerGrade" type="{http://www.huawei.com/bss/soaif/interface/common/}CustomerGrade" minOccurs="0"/>
 *         &lt;element name="Certificate" type="{http://www.huawei.com/bss/soaif/interface/common/}Certificate"/>
 *         &lt;element name="TINNumber" type="{http://www.huawei.com/bss/soaif/interface/common/}TINNumber" minOccurs="0"/>
 *         &lt;element name="Taxation" type="{http://www.huawei.com/bss/soaif/interface/common/}Taxation" minOccurs="0"/>
 *         &lt;element name="Industry" type="{http://www.huawei.com/bss/soaif/interface/common/}Industry" minOccurs="0"/>
 *         &lt;element name="SubIndustry" type="{http://www.huawei.com/bss/soaif/interface/common/}Industry" minOccurs="0"/>
 *         &lt;element name="CustPhoneNumber" type="{http://www.huawei.com/bss/soaif/interface/common/}Phone" minOccurs="0"/>
 *         &lt;element name="CustFaxNumber" type="{http://www.huawei.com/bss/soaif/interface/common/}Phone" minOccurs="0"/>
 *         &lt;element name="CustEmail" type="{http://www.huawei.com/bss/soaif/interface/common/}Email" minOccurs="0"/>
 *         &lt;element name="RegisterDate" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime" minOccurs="0"/>
 *         &lt;element name="RegisterCapital" type="{http://www.huawei.com/bss/soaif/interface/common/}RegisterCapital" minOccurs="0"/>
 *         &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}SimpleProperty" maxOccurs="100" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://www.huawei.com/bss/soaif/interface/common/}Remark" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CorporateInfo", propOrder = { "custType", "custName", "custShortName", "custSize", "customerGrade",
		"certificate", "tinNumber", "taxation", "industry", "subIndustry", "custPhoneNumber", "custFaxNumber",
		"custEmail", "registerDate", "registerCapital", "additionalProperty", "remark" })
public class CorporateInfo {
	@XmlElement(name = "CustType")
	protected String custType;
	@XmlElement(name = "CustName")
	protected String custName;
	@XmlElement(name = "CustShortName")
	protected String custShortName;
	@XmlElement(name = "CustSize")
	protected String custSize;
	@XmlElement(name = "CustomerGrade")
	protected String customerGrade;
	@XmlElement(name = "Certificate", required = true)
	protected Certificate certificate;
	@XmlElement(name = "TINNumber")
	protected String tinNumber;
	@XmlElement(name = "Taxation")
	protected String taxation;
	@XmlElement(name = "Industry")
	protected String industry;
	@XmlElement(name = "SubIndustry")
	protected String subIndustry;
	@XmlElement(name = "CustPhoneNumber")
	protected String custPhoneNumber;
	@XmlElement(name = "CustFaxNumber")
	protected String custFaxNumber;
	@XmlElement(name = "CustEmail")
	protected String custEmail;
	@XmlElement(name = "RegisterDate")
	protected String registerDate;
	@XmlElement(name = "RegisterCapital")
	protected String registerCapital;
	@XmlElement(name = "AdditionalProperty")
	protected List<SimpleProperty> additionalProperty;
	@XmlElement(name = "Remark")
	protected String remark;

	/**
	 * Gets the value of the custType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustType() {
		return custType;
	}

	/**
	 * Sets the value of the custType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustType(String value) {
		this.custType = value;
	}

	/**
	 * Gets the value of the custName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustName() {
		return custName;
	}

	/**
	 * Sets the value of the custName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustName(String value) {
		this.custName = value;
	}

	/**
	 * Gets the value of the custShortName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustShortName() {
		return custShortName;
	}

	/**
	 * Sets the value of the custShortName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustShortName(String value) {
		this.custShortName = value;
	}

	/**
	 * Gets the value of the custSize property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustSize() {
		return custSize;
	}

	/**
	 * Sets the value of the custSize property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustSize(String value) {
		this.custSize = value;
	}

	/**
	 * Gets the value of the customerGrade property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustomerGrade() {
		return customerGrade;
	}

	/**
	 * Sets the value of the customerGrade property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustomerGrade(String value) {
		this.customerGrade = value;
	}

	/**
	 * Gets the value of the certificate property.
	 * 
	 * @return possible object is {@link Certificate }
	 * 
	 */
	public Certificate getCertificate() {
		return certificate;
	}

	/**
	 * Sets the value of the certificate property.
	 * 
	 * @param value
	 *            allowed object is {@link Certificate }
	 * 
	 */
	public void setCertificate(Certificate value) {
		this.certificate = value;
	}

	/**
	 * Gets the value of the tinNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTINNumber() {
		return tinNumber;
	}

	/**
	 * Sets the value of the tinNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTINNumber(String value) {
		this.tinNumber = value;
	}

	/**
	 * Gets the value of the taxation property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTaxation() {
		return taxation;
	}

	/**
	 * Sets the value of the taxation property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTaxation(String value) {
		this.taxation = value;
	}

	/**
	 * Gets the value of the industry property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIndustry() {
		return industry;
	}

	/**
	 * Sets the value of the industry property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIndustry(String value) {
		this.industry = value;
	}

	/**
	 * Gets the value of the subIndustry property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSubIndustry() {
		return subIndustry;
	}

	/**
	 * Sets the value of the subIndustry property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSubIndustry(String value) {
		this.subIndustry = value;
	}

	/**
	 * Gets the value of the custPhoneNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustPhoneNumber() {
		return custPhoneNumber;
	}

	/**
	 * Sets the value of the custPhoneNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustPhoneNumber(String value) {
		this.custPhoneNumber = value;
	}

	/**
	 * Gets the value of the custFaxNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustFaxNumber() {
		return custFaxNumber;
	}

	/**
	 * Sets the value of the custFaxNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustFaxNumber(String value) {
		this.custFaxNumber = value;
	}

	/**
	 * Gets the value of the custEmail property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustEmail() {
		return custEmail;
	}

	/**
	 * Sets the value of the custEmail property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustEmail(String value) {
		this.custEmail = value;
	}

	/**
	 * Gets the value of the registerDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRegisterDate() {
		return registerDate;
	}

	/**
	 * Sets the value of the registerDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRegisterDate(String value) {
		this.registerDate = value;
	}

	/**
	 * Gets the value of the registerCapital property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRegisterCapital() {
		return registerCapital;
	}

	/**
	 * Sets the value of the registerCapital property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRegisterCapital(String value) {
		this.registerCapital = value;
	}

	/**
	 * Gets the value of the additionalProperty property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the additionalProperty property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getAdditionalProperty().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link SimpleProperty }
	 * 
	 * 
	 */
	public List<SimpleProperty> getAdditionalProperty() {
		if (additionalProperty == null) {
			additionalProperty = new ArrayList<SimpleProperty>();
		}
		return this.additionalProperty;
	}

	/**
	 * Gets the value of the remark property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * Sets the value of the remark property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark(String value) {
		this.remark = value;
	}
}
