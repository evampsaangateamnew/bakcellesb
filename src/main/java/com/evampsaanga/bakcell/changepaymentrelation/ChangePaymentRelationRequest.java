package com.evampsaanga.bakcell.changepaymentrelation;

import java.util.ArrayList;

import com.evampsaanga.bakcell.requestheaders.BaseRequest;

public class ChangePaymentRelationRequest extends BaseRequest {
	private String groupType="";
	private ArrayList<Users> users=null;
	public String getGroupType() {
		return groupType;
	}
	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}
	public ArrayList<Users> getUsers() {
		return users;
	}
	public void setUsers(ArrayList<Users> users) {
		this.users = users;
	}

}
