package com.huawei.bss.soaif._interface.ussdgateway;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for AcctList complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="AcctList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AcctKey" type="{http://www.huawei.com/bss/soaif/interface/common/}string"/>
 *         &lt;element name="BalanceResult" type="{http://www.huawei.com/bss/soaif/interface/USSDGateWay/}AcctBalance" maxOccurs="unbounded"/>
 *         &lt;element name="OutStandingList" type="{http://www.huawei.com/bss/soaif/interface/USSDGateWay/}OutStandingList" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="AccountCredit" type="{http://www.huawei.com/bss/soaif/interface/USSDGateWay/}AccountCredit" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="CorPayRelaList" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="CorPayRelaAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="CorPayRelaInstanID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="PayRelaType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="CorpUnbilledAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="IndvReserAmountList" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="IndvReserAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="CorpReserAmountList" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="CorpReserAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="StatusDetail" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PreMonIndvUnbilledAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="CurMonIndvUnbilledAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="PayByIndvPayOffAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="PayByCorpPayOffAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="OtherFeeAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AcctList", propOrder = { "acctKey", "balanceResult", "outStandingList", "accountCredit",
		"corPayRelaList", "corpUnbilledAmount", "indvReserAmountList", "corpReserAmountList", "statusDetail",
		"preMonIndvUnbilledAmount", "curMonIndvUnbilledAmount", "payByIndvPayOffAmount", "payByCorpPayOffAmount",
		"otherFeeAmount" })
public class AcctList {
	@XmlElement(name = "AcctKey", required = true)
	protected String acctKey;
	@XmlElement(name = "BalanceResult", required = true)
	protected List<AcctBalance> balanceResult;
	@XmlElement(name = "OutStandingList")
	protected List<OutStandingList> outStandingList;
	@XmlElement(name = "AccountCredit")
	protected List<AccountCredit> accountCredit;
	@XmlElement(name = "CorPayRelaList")
	protected List<AcctList.CorPayRelaList> corPayRelaList;
	@XmlElement(name = "CorpUnbilledAmount")
	protected long corpUnbilledAmount;
	@XmlElement(name = "IndvReserAmountList", required = true)
	protected List<AcctList.IndvReserAmountList> indvReserAmountList;
	@XmlElement(name = "CorpReserAmountList", required = true)
	protected List<AcctList.CorpReserAmountList> corpReserAmountList;
	@XmlElement(name = "StatusDetail", required = true)
	protected String statusDetail;
	@XmlElement(name = "PreMonIndvUnbilledAmount")
	protected Long preMonIndvUnbilledAmount;
	@XmlElement(name = "CurMonIndvUnbilledAmount")
	protected Long curMonIndvUnbilledAmount;
	@XmlElement(name = "PayByIndvPayOffAmount")
	protected Long payByIndvPayOffAmount;
	@XmlElement(name = "PayByCorpPayOffAmount")
	protected Long payByCorpPayOffAmount;
	@XmlElement(name = "OtherFeeAmount")
	protected Long otherFeeAmount;

	/**
	 * Gets the value of the acctKey property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctKey() {
		return acctKey;
	}

	/**
	 * Sets the value of the acctKey property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctKey(String value) {
		this.acctKey = value;
	}

	/**
	 * Gets the value of the balanceResult property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the balanceResult property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getBalanceResult().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link AcctBalance }
	 * 
	 * 
	 */
	public List<AcctBalance> getBalanceResult() {
		if (balanceResult == null) {
			balanceResult = new ArrayList<AcctBalance>();
		}
		return this.balanceResult;
	}

	/**
	 * Gets the value of the outStandingList property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the outStandingList property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getOutStandingList().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link OutStandingList }
	 * 
	 * 
	 */
	public List<OutStandingList> getOutStandingList() {
		if (outStandingList == null) {
			outStandingList = new ArrayList<OutStandingList>();
		}
		return this.outStandingList;
	}

	/**
	 * Gets the value of the accountCredit property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the accountCredit property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getAccountCredit().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link AccountCredit }
	 * 
	 * 
	 */
	public List<AccountCredit> getAccountCredit() {
		if (accountCredit == null) {
			accountCredit = new ArrayList<AccountCredit>();
		}
		return this.accountCredit;
	}

	/**
	 * Gets the value of the corPayRelaList property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the corPayRelaList property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getCorPayRelaList().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link AcctList.CorPayRelaList }
	 * 
	 * 
	 */
	public List<AcctList.CorPayRelaList> getCorPayRelaList() {
		if (corPayRelaList == null) {
			corPayRelaList = new ArrayList<AcctList.CorPayRelaList>();
		}
		return this.corPayRelaList;
	}

	/**
	 * Gets the value of the corpUnbilledAmount property.
	 * 
	 */
	public long getCorpUnbilledAmount() {
		return corpUnbilledAmount;
	}

	/**
	 * Sets the value of the corpUnbilledAmount property.
	 * 
	 */
	public void setCorpUnbilledAmount(long value) {
		this.corpUnbilledAmount = value;
	}

	/**
	 * Gets the value of the indvReserAmountList property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the indvReserAmountList property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getIndvReserAmountList().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link AcctList.IndvReserAmountList }
	 * 
	 * 
	 */
	public List<AcctList.IndvReserAmountList> getIndvReserAmountList() {
		if (indvReserAmountList == null) {
			indvReserAmountList = new ArrayList<AcctList.IndvReserAmountList>();
		}
		return this.indvReserAmountList;
	}

	/**
	 * Gets the value of the corpReserAmountList property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the corpReserAmountList property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getCorpReserAmountList().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link AcctList.CorpReserAmountList }
	 * 
	 * 
	 */
	public List<AcctList.CorpReserAmountList> getCorpReserAmountList() {
		if (corpReserAmountList == null) {
			corpReserAmountList = new ArrayList<AcctList.CorpReserAmountList>();
		}
		return this.corpReserAmountList;
	}

	/**
	 * Gets the value of the statusDetail property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStatusDetail() {
		return statusDetail;
	}

	/**
	 * Sets the value of the statusDetail property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStatusDetail(String value) {
		this.statusDetail = value;
	}

	/**
	 * Gets the value of the preMonIndvUnbilledAmount property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getPreMonIndvUnbilledAmount() {
		return preMonIndvUnbilledAmount;
	}

	/**
	 * Sets the value of the preMonIndvUnbilledAmount property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setPreMonIndvUnbilledAmount(Long value) {
		this.preMonIndvUnbilledAmount = value;
	}

	/**
	 * Gets the value of the curMonIndvUnbilledAmount property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getCurMonIndvUnbilledAmount() {
		return curMonIndvUnbilledAmount;
	}

	/**
	 * Sets the value of the curMonIndvUnbilledAmount property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setCurMonIndvUnbilledAmount(Long value) {
		this.curMonIndvUnbilledAmount = value;
	}

	/**
	 * Gets the value of the payByIndvPayOffAmount property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getPayByIndvPayOffAmount() {
		return payByIndvPayOffAmount;
	}

	/**
	 * Sets the value of the payByIndvPayOffAmount property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setPayByIndvPayOffAmount(Long value) {
		this.payByIndvPayOffAmount = value;
	}

	/**
	 * Gets the value of the payByCorpPayOffAmount property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getPayByCorpPayOffAmount() {
		return payByCorpPayOffAmount;
	}

	/**
	 * Sets the value of the payByCorpPayOffAmount property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setPayByCorpPayOffAmount(Long value) {
		this.payByCorpPayOffAmount = value;
	}

	/**
	 * Gets the value of the otherFeeAmount property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getOtherFeeAmount() {
		return otherFeeAmount;
	}

	/**
	 * Sets the value of the otherFeeAmount property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setOtherFeeAmount(Long value) {
		this.otherFeeAmount = value;
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="CorPayRelaAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
	 *         &lt;element name="CorPayRelaInstanID" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *         &lt;element name="PayRelaType" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "corPayRelaAmount", "corPayRelaInstanID", "payRelaType" })
	public static class CorPayRelaList {
		@XmlElement(name = "CorPayRelaAmount")
		protected long corPayRelaAmount;
		@XmlElement(name = "CorPayRelaInstanID", required = true)
		protected String corPayRelaInstanID;
		@XmlElement(name = "PayRelaType", required = true)
		protected String payRelaType;

		/**
		 * Gets the value of the corPayRelaAmount property.
		 * 
		 */
		public long getCorPayRelaAmount() {
			return corPayRelaAmount;
		}

		/**
		 * Sets the value of the corPayRelaAmount property.
		 * 
		 */
		public void setCorPayRelaAmount(long value) {
			this.corPayRelaAmount = value;
		}

		/**
		 * Gets the value of the corPayRelaInstanID property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getCorPayRelaInstanID() {
			return corPayRelaInstanID;
		}

		/**
		 * Sets the value of the corPayRelaInstanID property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setCorPayRelaInstanID(String value) {
			this.corPayRelaInstanID = value;
		}

		/**
		 * Gets the value of the payRelaType property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getPayRelaType() {
			return payRelaType;
		}

		/**
		 * Sets the value of the payRelaType property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setPayRelaType(String value) {
			this.payRelaType = value;
		}
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="CorpReserAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "corpReserAmount" })
	public static class CorpReserAmountList {
		@XmlElement(name = "CorpReserAmount")
		protected long corpReserAmount;

		/**
		 * Gets the value of the corpReserAmount property.
		 * 
		 */
		public long getCorpReserAmount() {
			return corpReserAmount;
		}

		/**
		 * Sets the value of the corpReserAmount property.
		 * 
		 */
		public void setCorpReserAmount(long value) {
			this.corpReserAmount = value;
		}
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="IndvReserAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "indvReserAmount" })
	public static class IndvReserAmountList {
		@XmlElement(name = "IndvReserAmount")
		protected long indvReserAmount;

		/**
		 * Gets the value of the indvReserAmount property.
		 * 
		 */
		public long getIndvReserAmount() {
			return indvReserAmount;
		}

		/**
		 * Sets the value of the indvReserAmount property.
		 * 
		 */
		public void setIndvReserAmount(long value) {
			this.indvReserAmount = value;
		}
	}
}
