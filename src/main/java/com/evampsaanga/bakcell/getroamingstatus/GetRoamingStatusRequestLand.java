package com.evampsaanga.bakcell.getroamingstatus;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.amqimplementationsesb.LogsType;
import com.evampsaanga.bakcell.getnetworksettings.GetNetworkSettingsRequestClient;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.ThirdPartyNames;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;
import com.huawei.crm.basetype.GetSubProductInfo;
import com.huawei.crm.query.GetNetworkSettingDataResponse;

@Path("/bakcell")
public class GetRoamingStatusRequestLand {
	public static final Logger logger = Logger.getLogger("bakcellLogs-V2");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetRoamingStatusResponse Get(@Header("credentials") String credential, @Body() String requestBody) {
		GetRoamingStatusResponse resp = new GetRoamingStatusResponse();
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.GET_CORE_SERVICES_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.GET_CORE_SERVICES);
		logs.setTableType(LogsType.GetCoreServices);
		try {
			logger.info("Request Landed on GetRoamingServiceRequestLand " + requestBody);
			String credentials = null;
			GetRoamingStatusRequest cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, GetRoamingStatusRequest.class);
				logs.setIsB2B(cclient.getIsB2B());
			} catch (Exception ex1) {
				logger.error(Helper.GetException(ex1));
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String verification = Helper.validateRequest(cclient);
				if (cclient.getIsB2B() != null && cclient.getIsB2B().equals("true")) {
					resp.setReturnCode(ResponseCodes.ERROR_400);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setIsB2B(cclient.getIsB2B());
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}

				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					logger.info("credentials validated.....");

					GetNetworkSettingsRequestClient networkclient = new GetNetworkSettingsRequestClient();
					networkclient.setMsisdn(cclient.getmsisdn());
					GetNetworkSettingDataResponse responseFromBakcell = com.evampsaanga.bakcell.getnetworksettings.GetNetworkSettingsLand
							.RequestSoap(com.evampsaanga.bakcell.getnetworksettings.GetNetworkSettingsLand
									.getRequestHeader(), networkclient);

					List<GetSubProductInfo> getsubproductinfo = responseFromBakcell.getGetNetworkSettingDataBody()
							.getGetNetworkSettingDataList();

					if (responseFromBakcell.getResponseHeader().getRetCode().equalsIgnoreCase("0")) {
						for (GetSubProductInfo getSubProductInfo2 : getsubproductinfo) {
							if (getSubProductInfo2.getProductId().equalsIgnoreCase("177943157")) {
								if (getSubProductInfo2.getStatus()
										.equalsIgnoreCase(Constants.CORE_SERVICE_ACTIVE_STATUS))
									resp.setActive(true);
							}
						}
						resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					} else {
						resp.setReturnCode(responseFromBakcell.getResponseHeader().getRetCode());
						resp.setReturnMsg(responseFromBakcell.getResponseHeader().getRetMsg());
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					}
					logs.updateLog(logs);

					return resp;
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else
				resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
	}
}
