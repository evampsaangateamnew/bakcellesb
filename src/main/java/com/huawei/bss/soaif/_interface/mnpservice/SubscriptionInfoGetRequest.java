package com.huawei.bss.soaif._interface.mnpservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bss.soaif._interface.common.ReqHeader;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.huawei.com/bss/soaif/interface/common/}ReqHeader"/>
 *         &lt;element name="Subscription_Info_GetBody" type="{http://www.huawei.com/bss/soaif/interface/MNPService/}Subscription_Info_GetIn"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "reqHeader", "subscriptionInfoGetBody" })
@XmlRootElement(name = "Subscription_Info_GetRequest")
public class SubscriptionInfoGetRequest {
	@XmlElement(name = "ReqHeader", namespace = "http://www.huawei.com/bss/soaif/interface/common/", required = true)
	protected ReqHeader reqHeader;
	@XmlElement(name = "Subscription_Info_GetBody", required = true)
	protected SubscriptionInfoGetIn subscriptionInfoGetBody;

	/**
	 * Gets the value of the reqHeader property.
	 * 
	 * @return possible object is {@link ReqHeader }
	 * 
	 */
	public ReqHeader getReqHeader() {
		return reqHeader;
	}

	/**
	 * Sets the value of the reqHeader property.
	 * 
	 * @param value
	 *            allowed object is {@link ReqHeader }
	 * 
	 */
	public void setReqHeader(ReqHeader value) {
		this.reqHeader = value;
	}

	/**
	 * Gets the value of the subscriptionInfoGetBody property.
	 * 
	 * @return possible object is {@link SubscriptionInfoGetIn }
	 * 
	 */
	public SubscriptionInfoGetIn getSubscriptionInfoGetBody() {
		return subscriptionInfoGetBody;
	}

	/**
	 * Sets the value of the subscriptionInfoGetBody property.
	 * 
	 * @param value
	 *            allowed object is {@link SubscriptionInfoGetIn }
	 * 
	 */
	public void setSubscriptionInfoGetBody(SubscriptionInfoGetIn value) {
		this.subscriptionInfoGetBody = value;
	}
}
