package com.huawei.crm.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for RetrivedScreenNumInfo complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="RetrivedScreenNumInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TotalRowNum" type="{http://crm.huawei.com/basetype/}TotalRowNum"/>
 *         &lt;element name="CallScreenList" type="{http://crm.huawei.com/query/}CallScreenNumList" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RetrivedScreenNumInfo", propOrder = { "totalRowNum", "callScreenList" })
public class RetrivedScreenNumInfo {
	@XmlElement(name = "TotalRowNum")
	protected long totalRowNum;
	@XmlElement(name = "CallScreenList")
	protected CallScreenNumList callScreenList;

	/**
	 * Gets the value of the totalRowNum property.
	 * 
	 */
	public long getTotalRowNum() {
		return totalRowNum;
	}

	/**
	 * Sets the value of the totalRowNum property.
	 * 
	 */
	public void setTotalRowNum(long value) {
		this.totalRowNum = value;
	}

	/**
	 * Gets the value of the callScreenList property.
	 * 
	 * @return possible object is {@link CallScreenNumList }
	 * 
	 */
	public CallScreenNumList getCallScreenList() {
		return callScreenList;
	}

	/**
	 * Sets the value of the callScreenList property.
	 * 
	 * @param value
	 *            allowed object is {@link CallScreenNumList }
	 * 
	 */
	public void setCallScreenList(CallScreenNumList value) {
		this.callScreenList = value;
	}
}
