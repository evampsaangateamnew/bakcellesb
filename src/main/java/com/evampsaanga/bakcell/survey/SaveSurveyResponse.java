package com.evampsaanga.bakcell.survey;

import com.evampsaanga.bakcell.responseheaders.BaseResponse;

public class SaveSurveyResponse extends BaseResponse {

	@Override
	public String toString() {
		return "SaveSurveyResponse [toString()=" + super.toString() + "]";
	}

}
