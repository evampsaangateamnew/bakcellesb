package com.huawei.bss.soaif._interface.ussdgateway;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import com.huawei.bss.soaif._interface.common.ReqHeader;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.huawei.com/bss/soaif/interface/common/}ReqHeader"/>
 *         &lt;sequence>
 *           &lt;element name="SubAccessCode">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="PrimaryIdentity" type="{http://www.huawei.com/bss/soaif/interface/common/}string"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="LoanAmount" type="{http://www.huawei.com/bss/soaif/interface/common/}long"/>
 *           &lt;element name="Remark" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "reqHeader", "subAccessCode", "loanAmount", "remark" })
@XmlRootElement(name = "USSDGWLoanReqMsg")
public class USSDGWLoanReqMsg {
	@XmlElement(name = "ReqHeader", namespace = "http://www.huawei.com/bss/soaif/interface/common/", required = true)
	protected ReqHeader reqHeader;
	@XmlElement(name = "SubAccessCode", required = true)
	protected USSDGWLoanReqMsg.SubAccessCode subAccessCode;
	@XmlElement(name = "LoanAmount")
	@XmlJavaTypeAdapter(type = long.class, value = com.huawei.crm.service.LongXMLAdapter.class)
	protected long loanAmount;
	@XmlElement(name = "Remark")
	protected String remark;

	/**
	 * Gets the value of the reqHeader property.
	 * 
	 * @return possible object is {@link ReqHeader }
	 * 
	 */
	public ReqHeader getReqHeader() {
		return reqHeader;
	}

	/**
	 * Sets the value of the reqHeader property.
	 * 
	 * @param value
	 *            allowed object is {@link ReqHeader }
	 * 
	 */
	public void setReqHeader(ReqHeader value) {
		this.reqHeader = value;
	}

	/**
	 * Gets the value of the subAccessCode property.
	 * 
	 * @return possible object is {@link USSDGWLoanReqMsg.SubAccessCode }
	 * 
	 */
	public USSDGWLoanReqMsg.SubAccessCode getSubAccessCode() {
		return subAccessCode;
	}

	/**
	 * Sets the value of the subAccessCode property.
	 * 
	 * @param value
	 *            allowed object is {@link USSDGWLoanReqMsg.SubAccessCode }
	 * 
	 */
	public void setSubAccessCode(USSDGWLoanReqMsg.SubAccessCode value) {
		this.subAccessCode = value;
	}

	/**
	 * Gets the value of the loanAmount property.
	 * 
	 */
	public long getLoanAmount() {
		return loanAmount;
	}

	/**
	 * Sets the value of the loanAmount property.
	 * 
	 */
	public void setLoanAmount(long value) {
		this.loanAmount = value;
	}

	/**
	 * Gets the value of the remark property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * Sets the value of the remark property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark(String value) {
		this.remark = value;
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="PrimaryIdentity" type="{http://www.huawei.com/bss/soaif/interface/common/}string"/>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "primaryIdentity" })
	public static class SubAccessCode {
		@XmlElement(name = "PrimaryIdentity", required = true)
		protected String primaryIdentity;

		/**
		 * Gets the value of the primaryIdentity property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getPrimaryIdentity() {
			return primaryIdentity;
		}

		/**
		 * Sets the value of the primaryIdentity property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setPrimaryIdentity(String value) {
			this.primaryIdentity = value;
		}
	}
}
