
package net.x_rd.az.voen9900037691.producer;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the net.x_rd.az.voen9900037691.producer package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: net.x_rd.az.voen9900037691.producer
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CheckVoenValidRequest }
     * 
     */
    public CheckVoenValidRequest createCheckVoenValidRequest() {
        return new CheckVoenValidRequest();
    }

    /**
     * Create an instance of {@link GetSunlistByVoenRequest }
     * 
     */
    public GetSunlistByVoenRequest createGetSunlistByVoenRequest() {
        return new GetSunlistByVoenRequest();
    }

    /**
     * Create an instance of {@link CheckDocumentValidRequest }
     * 
     */
    public CheckDocumentValidRequest createCheckDocumentValidRequest() {
        return new CheckDocumentValidRequest();
    }

    /**
     * Create an instance of {@link AddApplicationRequest }
     * 
     */
    public AddApplicationRequest createAddApplicationRequest() {
        return new AddApplicationRequest();
    }

    /**
     * Create an instance of {@link ArrayOfRequestApplication }
     * 
     */
    public ArrayOfRequestApplication createArrayOfRequestApplication() {
        return new ArrayOfRequestApplication();
    }

}
