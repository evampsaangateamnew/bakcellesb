package com.evampsaanga.bakcell.getappmenu;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MenuHorizontal {
	private String identifier = "";
	private String title = "";
	private String sortOrder = "";	
	@JsonProperty("items")
	private DataV2[] items ;
	
	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}
	public DataV2[] getItems() {
		return items;
	}
	public void setItems(DataV2[] items) {
		this.items = items;
	}
	
	
	
}
