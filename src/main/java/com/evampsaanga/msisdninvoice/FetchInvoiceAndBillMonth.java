package com.evampsaanga.msisdninvoice;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.camel.language.Constant;
import org.apache.log4j.Logger;

import com.evampsaanga.bakcell.db.DBBakcellFactory;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.developer.utils.Helper;

public class FetchInvoiceAndBillMonth {
	
	public static final Logger logger = Logger.getLogger("bakcellLogs-V2");
	public static final Logger loggerV2 = Logger.getLogger("bakcellLogs-V2");
    public static String modeulName="FETCHING INVOICE ID AND BILL MONTH";
	private String invoiceId;
	private String billMonth;
	public String getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}
	public String getBillMonth() {
		return billMonth;
	}
	public void setBillMonth(String billMonth) {
		this.billMonth = billMonth;
	}
	
	
	public void setInvoceidAndBillMonth(String accountCode, String billMonth,String lang) {

		
		String query = "select INVOICE_ID,BILL_MONTH from V_E_CARE_BILL_INVOICE t where ACCT_CODE = '"+accountCode+"' and BILL_MONTH = to_date('"+billMonth+"','yyyymmdd');";
		try {
			logMessage("Query: " + query);
			ResultSet rs = DBBakcellFactory.getConnection().prepareStatement(query).executeQuery();
			while (rs.next()) 
			{
				
				this.setBillMonth(rs.getString("BILL_MONT"));
				logMessage(rs.getString("BILL_MONT"));
				
				this.setInvoiceId(rs.getString("INVOICE_ID"));
				logMessage(rs.getString("INVOICE_ID"));
				
				
				
			}
			
		} catch (SQLException e) {
			logger.info(Helper.GetException(e));
			
		}
		
		
		
		
	
	}
	
	
	private void logMessage(String message)
	{
		logger.info(modeulName+": "+message);
	}

}
