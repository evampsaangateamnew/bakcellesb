
package com.huawei.crm.service.order.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.order.query.ResponseHeader;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResponseHeader" type="{http://crm.huawei.com/basetype/}ResponseHeader"/>
 *         &lt;element name="RetrievePendingOrderResponseBody" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="TotalRowNum" type="{http://crm.huawei.com/basetype/}TotalRowNum"/>
 *                   &lt;element name="PendingOrderList" type="{http://crm.huawei.com/service/}RetrievePendingOrderList"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "responseHeader",
    "retrievePendingOrderResponseBody"
})
@XmlRootElement(name = "RetrievePendingOrderResponse")
public class RetrievePendingOrderResponse {

    @XmlElement(name = "ResponseHeader", required = true)
    protected ResponseHeader responseHeader;
    @XmlElement(name = "RetrievePendingOrderResponseBody")
    protected RetrievePendingOrderResponse.RetrievePendingOrderResponseBody retrievePendingOrderResponseBody;

    /**
     * Gets the value of the responseHeader property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseHeader }
     *     
     */
    public ResponseHeader getResponseHeader() {
        return responseHeader;
    }

    /**
     * Sets the value of the responseHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseHeader }
     *     
     */
    public void setResponseHeader(ResponseHeader value) {
        this.responseHeader = value;
    }

    /**
     * Gets the value of the retrievePendingOrderResponseBody property.
     * 
     * @return
     *     possible object is
     *     {@link RetrievePendingOrderResponse.RetrievePendingOrderResponseBody }
     *     
     */
    public RetrievePendingOrderResponse.RetrievePendingOrderResponseBody getRetrievePendingOrderResponseBody() {
        return retrievePendingOrderResponseBody;
    }

    /**
     * Sets the value of the retrievePendingOrderResponseBody property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrievePendingOrderResponse.RetrievePendingOrderResponseBody }
     *     
     */
    public void setRetrievePendingOrderResponseBody(RetrievePendingOrderResponse.RetrievePendingOrderResponseBody value) {
        this.retrievePendingOrderResponseBody = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="TotalRowNum" type="{http://crm.huawei.com/basetype/}TotalRowNum"/>
     *         &lt;element name="PendingOrderList" type="{http://crm.huawei.com/service/}RetrievePendingOrderList"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "totalRowNum",
        "pendingOrderList"
    })
    public static class RetrievePendingOrderResponseBody {

        @XmlElement(name = "TotalRowNum")
        protected long totalRowNum;
        @XmlElement(name = "PendingOrderList", required = true)
        protected RetrievePendingOrderList pendingOrderList;

        /**
         * Gets the value of the totalRowNum property.
         * 
         */
        public long getTotalRowNum() {
            return totalRowNum;
        }

        /**
         * Sets the value of the totalRowNum property.
         * 
         */
        public void setTotalRowNum(long value) {
            this.totalRowNum = value;
        }

        /**
         * Gets the value of the pendingOrderList property.
         * 
         * @return
         *     possible object is
         *     {@link RetrievePendingOrderList }
         *     
         */
        public RetrievePendingOrderList getPendingOrderList() {
            return pendingOrderList;
        }

        /**
         * Sets the value of the pendingOrderList property.
         * 
         * @param value
         *     allowed object is
         *     {@link RetrievePendingOrderList }
         *     
         */
        public void setPendingOrderList(RetrievePendingOrderList value) {
            this.pendingOrderList = value;
        }

    }

}
