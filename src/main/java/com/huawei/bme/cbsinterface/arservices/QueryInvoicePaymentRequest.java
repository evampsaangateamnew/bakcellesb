package com.huawei.bme.cbsinterface.arservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for QueryInvoicePaymentRequest complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="QueryInvoicePaymentRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InvoiceNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryInvoicePaymentRequest", propOrder = { "invoiceNo" })
public class QueryInvoicePaymentRequest {
	@XmlElement(name = "InvoiceNo", required = true)
	protected String invoiceNo;

	/**
	 * Gets the value of the invoiceNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getInvoiceNo() {
		return invoiceNo;
	}

	/**
	 * Sets the value of the invoiceNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setInvoiceNo(String value) {
		this.invoiceNo = value;
	}
}
