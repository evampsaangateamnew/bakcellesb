package com.evampsaanga.bakcell.ordermanagementV2;

import com.evampsaanga.bakcell.responseheaders.BaseResponse;

public class OrderManagementResponse extends BaseResponse {
	private String responseMsg;

	public String getResponseMsg() {
		return responseMsg;
	}

	public void setResponseMsg(String responseMsg) {
		this.responseMsg = responseMsg;
	}

	@Override
	public String toString() {
		return "OrderManagementResponse [responseMsg=" + responseMsg + "]";
	}
	
}
