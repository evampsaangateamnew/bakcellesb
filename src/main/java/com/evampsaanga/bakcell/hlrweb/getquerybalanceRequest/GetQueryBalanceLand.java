package com.evampsaanga.bakcell.hlrweb.getquerybalanceRequest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.ws.WebServiceException;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.amqimplementationsesb.LogsType;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.ThirdPartyNames;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.gethomepage.Balance;
import com.huawei.bss.soaif._interface.common.ReqHeader;

@Path("/bakcell")
public class GetQueryBalanceLand {

	public static final Logger logger = Logger.getLogger("bakcellLogs-V2");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetQueryBalanceResponseClient Get(@Header("credentials") String credential, @Body() String requestBody) {
		logger.info("Requset on getBalance HLR:" + requestBody);
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.HLR_QUERY_BALANCE_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.HLR_QUERY_BALANCE);
		logs.setTableType(LogsType.HlrQueryBalance);
		GetQueryBalanceResponseClient response = new GetQueryBalanceResponseClient();
		try {
			GetQueryBalanceRequestClient cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, GetQueryBalanceRequestClient.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setIsB2B(cclient.getIsB2B());
				}
			} catch (Exception ex) {
				response.setReturnCode(ResponseCodes.ERROR_400_CODE);
				response.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(response.getReturnCode());
				logs.setResponseDescription(response.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return response;
			}
			if (cclient != null) {
				String credentials = Decrypter.getInstance().decrypt(credential);
				if (credentials == null) {
					response = new GetQueryBalanceResponseClient();
					response.setReturnCode(ResponseCodes.ERROR_401_CODE);
					response.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(response.getReturnCode());
					logs.setResponseDescription(response.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return response;
				}
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					response = new GetQueryBalanceResponseClient();
					response.setReturnCode(ResponseCodes.ERROR_400);
					response.setReturnMsg(verification);
					logs.setResponseCode(response.getReturnCode());
					logs.setResponseDescription(response.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return response;
				}
				try {
					HLRBalanceServices service = new HLRBalanceServices();
					Balance balance = service.getBalance(cclient.getmsisdn(), cclient.getCustomerType(),null);
					if (balance.getReturnCode().equals("0")) {
						response.setBalance(balance);
						response.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						response.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						logs.setResponseCode(response.getReturnCode());
						logs.setResponseDescription(response.getReturnMsg());
						logs.setUserType(cclient.getCustomerType());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					} else {
						response.setReturnCode(balance.getReturnCode());
						response.setReturnMsg(balance.getReturnMsg());
						logs.setResponseCode(response.getReturnCode());
						logs.setResponseDescription(response.getReturnMsg());
						logs.setUserType(cclient.getCustomerType());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					}
					logs.updateLog(logs);
					return response;
				} catch (Exception ee) {
					response = new GetQueryBalanceResponseClient();
					if (ee instanceof WebServiceException) {
						response.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
						response.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
						logs.setResponseCode(response.getReturnCode());
						logs.setResponseDescription(response.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					}
				}
			} 
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		logs.updateLog(logs);
		return response;
	}

	public ReqHeader getReqHeaderForQueryBalanceHLR() {
		ReqHeader reqHForQBalance = new ReqHeader();
			reqHForQBalance.setChannelId(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.ChannelId").trim());
			reqHForQBalance.setAccessUser(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.AccessUser").trim());
			reqHForQBalance.setAccessPwd(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.AccessPwd").trim());
			reqHForQBalance.setMIDWAREChannelID(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.MIDWAREChannelID").trim());
			reqHForQBalance.setMIDWAREAccessPwd(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.MIDWAREAccessPwd").trim());
			reqHForQBalance.setMIDWAREAccessUser(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.MIDWAREAccessUser").trim());
			reqHForQBalance.setTransactionId(Helper.generateTransactionID());
		return reqHForQBalance;
	}
}
