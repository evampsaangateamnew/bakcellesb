package com.huawei.bme.cbsinterface.arservices;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the com.huawei.bme.cbsinterface.arservices
 * package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {
	/**
	 * Create a new ObjectFactory that can be used to create new instances of
	 * schema derived classes for package:
	 * com.huawei.bme.cbsinterface.arservices
	 * 
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link RechargeRollBackResult }
	 * 
	 */
	public RechargeRollBackResult createRechargeRollBackResult() {
		return new RechargeRollBackResult();
	}

	/**
	 * Create an instance of {@link RechargeRollBackResult.LifeCycleRollBack }
	 * 
	 */
	public RechargeRollBackResult.LifeCycleRollBack createRechargeRollBackResultLifeCycleRollBack() {
		return new RechargeRollBackResult.LifeCycleRollBack();
	}

	/**
	 * Create an instance of {@link RechargeRollBackResult.BonusRollBack }
	 * 
	 */
	public RechargeRollBackResult.BonusRollBack createRechargeRollBackResultBonusRollBack() {
		return new RechargeRollBackResult.BonusRollBack();
	}

	/**
	 * Create an instance of {@link QueryInvoicePaymentResult }
	 * 
	 */
	public QueryInvoicePaymentResult createQueryInvoicePaymentResult() {
		return new QueryInvoicePaymentResult();
	}

	/**
	 * Create an instance of
	 * {@link QueryInvoicePaymentResult.InvoicePaymentInfo }
	 * 
	 */
	public QueryInvoicePaymentResult.InvoicePaymentInfo createQueryInvoicePaymentResultInvoicePaymentInfo() {
		return new QueryInvoicePaymentResult.InvoicePaymentInfo();
	}

	/**
	 * Create an instance of {@link RechargeRollBackRequest }
	 * 
	 */
	public RechargeRollBackRequest createRechargeRollBackRequest() {
		return new RechargeRollBackRequest();
	}

	/**
	 * Create an instance of {@link RechargeRollBackRequest.RechargeObj }
	 * 
	 */
	public RechargeRollBackRequest.RechargeObj createRechargeRollBackRequestRechargeObj() {
		return new RechargeRollBackRequest.RechargeObj();
	}

	/**
	 * Create an instance of {@link PaymentRollBackRequest }
	 * 
	 */
	public PaymentRollBackRequest createPaymentRollBackRequest() {
		return new PaymentRollBackRequest();
	}

	/**
	 * Create an instance of {@link PaymentRollBackRequest.PaymentObj }
	 * 
	 */
	public PaymentRollBackRequest.PaymentObj createPaymentRollBackRequestPaymentObj() {
		return new PaymentRollBackRequest.PaymentObj();
	}

	/**
	 * Create an instance of {@link QueryInvoiceResult }
	 * 
	 */
	public QueryInvoiceResult createQueryInvoiceResult() {
		return new QueryInvoiceResult();
	}

	/**
	 * Create an instance of {@link QueryInvoiceResult.InvoiceInfo }
	 * 
	 */
	public QueryInvoiceResult.InvoiceInfo createQueryInvoiceResultInvoiceInfo() {
		return new QueryInvoiceResult.InvoiceInfo();
	}

	/**
	 * Create an instance of {@link TransferBalanceRequest }
	 * 
	 */
	public TransferBalanceRequest createTransferBalanceRequest() {
		return new TransferBalanceRequest();
	}

	/**
	 * Create an instance of {@link RechargeResult }
	 * 
	 */
	public RechargeResult createRechargeResult() {
		return new RechargeResult();
	}

	/**
	 * Create an instance of {@link RechargeResult.LifeCycleChgInfo }
	 * 
	 */
	public RechargeResult.LifeCycleChgInfo createRechargeResultLifeCycleChgInfo() {
		return new RechargeResult.LifeCycleChgInfo();
	}

	/**
	 * Create an instance of {@link RechargeResult.RechargeBonus }
	 * 
	 */
	public RechargeResult.RechargeBonus createRechargeResultRechargeBonus() {
		return new RechargeResult.RechargeBonus();
	}

	/**
	 * Create an instance of {@link QueryPaymentLogResult }
	 * 
	 */
	public QueryPaymentLogResult createQueryPaymentLogResult() {
		return new QueryPaymentLogResult();
	}

	/**
	 * Create an instance of {@link QueryPaymentLogResult.PaymentInfo }
	 * 
	 */
	public QueryPaymentLogResult.PaymentInfo createQueryPaymentLogResultPaymentInfo() {
		return new QueryPaymentLogResult.PaymentInfo();
	}

	/**
	 * Create an instance of {@link QueryTransferLogRequest }
	 * 
	 */
	public QueryTransferLogRequest createQueryTransferLogRequest() {
		return new QueryTransferLogRequest();
	}

	/**
	 * Create an instance of {@link QueryTransferLogRequest.QueryObj }
	 * 
	 */
	public QueryTransferLogRequest.QueryObj createQueryTransferLogRequestQueryObj() {
		return new QueryTransferLogRequest.QueryObj();
	}

	/**
	 * Create an instance of {@link QueryRechargeLogRequest }
	 * 
	 */
	public QueryRechargeLogRequest createQueryRechargeLogRequest() {
		return new QueryRechargeLogRequest();
	}

	/**
	 * Create an instance of {@link QueryRechargeLogRequest.QueryObj }
	 * 
	 */
	public QueryRechargeLogRequest.QueryObj createQueryRechargeLogRequestQueryObj() {
		return new QueryRechargeLogRequest.QueryObj();
	}

	/**
	 * Create an instance of {@link TransferBalanceResult }
	 * 
	 */
	public TransferBalanceResult createTransferBalanceResult() {
		return new TransferBalanceResult();
	}

	/**
	 * Create an instance of {@link TransferBalanceResult.Transferee }
	 * 
	 */
	public TransferBalanceResult.Transferee createTransferBalanceResultTransferee() {
		return new TransferBalanceResult.Transferee();
	}

	/**
	 * Create an instance of
	 * {@link TransferBalanceResult.Transferee.LifeCycleChgInfo }
	 * 
	 */
	public TransferBalanceResult.Transferee.LifeCycleChgInfo createTransferBalanceResultTransfereeLifeCycleChgInfo() {
		return new TransferBalanceResult.Transferee.LifeCycleChgInfo();
	}

	/**
	 * Create an instance of {@link TransferBalanceResult.Transferor }
	 * 
	 */
	public TransferBalanceResult.Transferor createTransferBalanceResultTransferor() {
		return new TransferBalanceResult.Transferor();
	}

	/**
	 * Create an instance of
	 * {@link TransferBalanceResult.Transferor.LifeCycleChgInfo }
	 * 
	 */
	public TransferBalanceResult.Transferor.LifeCycleChgInfo createTransferBalanceResultTransferorLifeCycleChgInfo() {
		return new TransferBalanceResult.Transferor.LifeCycleChgInfo();
	}

	/**
	 * Create an instance of {@link QueryRechargeLogResult }
	 * 
	 */
	public QueryRechargeLogResult createQueryRechargeLogResult() {
		return new QueryRechargeLogResult();
	}

	/**
	 * Create an instance of {@link QueryRechargeLogResult.RechargeInfo }
	 * 
	 */
	public QueryRechargeLogResult.RechargeInfo createQueryRechargeLogResultRechargeInfo() {
		return new QueryRechargeLogResult.RechargeInfo();
	}

	/**
	 * Create an instance of
	 * {@link QueryRechargeLogResult.RechargeInfo.RechargeBonus }
	 * 
	 */
	public QueryRechargeLogResult.RechargeInfo.RechargeBonus createQueryRechargeLogResultRechargeInfoRechargeBonus() {
		return new QueryRechargeLogResult.RechargeInfo.RechargeBonus();
	}

	/**
	 * Create an instance of
	 * {@link QueryRechargeLogResult.RechargeInfo.LifeCycleChgInfo }
	 * 
	 */
	public QueryRechargeLogResult.RechargeInfo.LifeCycleChgInfo createQueryRechargeLogResultRechargeInfoLifeCycleChgInfo() {
		return new QueryRechargeLogResult.RechargeInfo.LifeCycleChgInfo();
	}

	/**
	 * Create an instance of {@link QueryTransferLogResult }
	 * 
	 */
	public QueryTransferLogResult createQueryTransferLogResult() {
		return new QueryTransferLogResult();
	}

	/**
	 * Create an instance of {@link QueryBalanceResult }
	 * 
	 */
	public QueryBalanceResult createQueryBalanceResult() {
		return new QueryBalanceResult();
	}

	/**
	 * Create an instance of {@link QueryBalanceResult.AcctList }
	 * 
	 */
	public QueryBalanceResult.AcctList createQueryBalanceResultAcctList() {
		return new QueryBalanceResult.AcctList();
	}

	/**
	 * Create an instance of {@link QueryBalanceResult.AcctList.AccountCredit }
	 * 
	 */
	public QueryBalanceResult.AcctList.AccountCredit createQueryBalanceResultAcctListAccountCredit() {
		return new QueryBalanceResult.AcctList.AccountCredit();
	}

	/**
	 * Create an instance of
	 * {@link QueryBalanceResult.AcctList.OutStandingList }
	 * 
	 */
	public QueryBalanceResult.AcctList.OutStandingList createQueryBalanceResultAcctListOutStandingList() {
		return new QueryBalanceResult.AcctList.OutStandingList();
	}

	/**
	 * Create an instance of {@link QueryLoanLogResult }
	 * 
	 */
	public QueryLoanLogResult createQueryLoanLogResult() {
		return new QueryLoanLogResult();
	}

	/**
	 * Create an instance of {@link QueryLoanLogResult.LoanLogDetail }
	 * 
	 */
	public QueryLoanLogResult.LoanLogDetail createQueryLoanLogResultLoanLogDetail() {
		return new QueryLoanLogResult.LoanLogDetail();
	}

	/**
	 * Create an instance of {@link QueryLoanLogResult.LoanLogSummary }
	 * 
	 */
	public QueryLoanLogResult.LoanLogSummary createQueryLoanLogResultLoanLogSummary() {
		return new QueryLoanLogResult.LoanLogSummary();
	}

	/**
	 * Create an instance of {@link PaymentResult }
	 * 
	 */
	public PaymentResult createPaymentResult() {
		return new PaymentResult();
	}

	/**
	 * Create an instance of {@link PaymentResult.OutStandingList }
	 * 
	 */
	public PaymentResult.OutStandingList createPaymentResultOutStandingList() {
		return new PaymentResult.OutStandingList();
	}

	/**
	 * Create an instance of {@link PaymentResult.PaymentBonus }
	 * 
	 */
	public PaymentResult.PaymentBonus createPaymentResultPaymentBonus() {
		return new PaymentResult.PaymentBonus();
	}

	/**
	 * Create an instance of {@link QueryBalanceRequest }
	 * 
	 */
	public QueryBalanceRequest createQueryBalanceRequest() {
		return new QueryBalanceRequest();
	}

	/**
	 * Create an instance of {@link QueryBalanceRequest.QueryObj }
	 * 
	 */
	public QueryBalanceRequest.QueryObj createQueryBalanceRequestQueryObj() {
		return new QueryBalanceRequest.QueryObj();
	}

	/**
	 * Create an instance of {@link QueryPaymentLogRequest }
	 * 
	 */
	public QueryPaymentLogRequest createQueryPaymentLogRequest() {
		return new QueryPaymentLogRequest();
	}

	/**
	 * Create an instance of {@link RechargeRequest }
	 * 
	 */
	public RechargeRequest createRechargeRequest() {
		return new RechargeRequest();
	}

	/**
	 * Create an instance of {@link RechargeRequest.RechargeInfo }
	 * 
	 */
	public RechargeRequest.RechargeInfo createRechargeRequestRechargeInfo() {
		return new RechargeRequest.RechargeInfo();
	}

	/**
	 * Create an instance of {@link RechargeRequest.RechargeObj }
	 * 
	 */
	public RechargeRequest.RechargeObj createRechargeRequestRechargeObj() {
		return new RechargeRequest.RechargeObj();
	}

	/**
	 * Create an instance of {@link PaymentRequest }
	 * 
	 */
	public PaymentRequest createPaymentRequest() {
		return new PaymentRequest();
	}

	/**
	 * Create an instance of {@link PaymentRequest.PaymentInfo }
	 * 
	 */
	public PaymentRequest.PaymentInfo createPaymentRequestPaymentInfo() {
		return new PaymentRequest.PaymentInfo();
	}

	/**
	 * Create an instance of {@link PaymentRequest.PaymentInfo.CashPayment }
	 * 
	 */
	public PaymentRequest.PaymentInfo.CashPayment createPaymentRequestPaymentInfoCashPayment() {
		return new PaymentRequest.PaymentInfo.CashPayment();
	}

	/**
	 * Create an instance of {@link PaymentRequest.PaymentObj }
	 * 
	 */
	public PaymentRequest.PaymentObj createPaymentRequestPaymentObj() {
		return new PaymentRequest.PaymentObj();
	}

	/**
	 * Create an instance of {@link QueryInvoiceRequest }
	 * 
	 */
	public QueryInvoiceRequest createQueryInvoiceRequest() {
		return new QueryInvoiceRequest();
	}

	/**
	 * Create an instance of {@link QueryInvoiceRequestMsg }
	 * 
	 */
	public QueryInvoiceRequestMsg createQueryInvoiceRequestMsg() {
		return new QueryInvoiceRequestMsg();
	}

	/**
	 * Create an instance of {@link PaymentRequestMsg }
	 * 
	 */
	public PaymentRequestMsg createPaymentRequestMsg() {
		return new PaymentRequestMsg();
	}

	/**
	 * Create an instance of {@link RechargeRequestMsg }
	 * 
	 */
	public RechargeRequestMsg createRechargeRequestMsg() {
		return new RechargeRequestMsg();
	}

	/**
	 * Create an instance of {@link QueryPaymentLogRequestMsg }
	 * 
	 */
	public QueryPaymentLogRequestMsg createQueryPaymentLogRequestMsg() {
		return new QueryPaymentLogRequestMsg();
	}

	/**
	 * Create an instance of {@link QueryBalanceRequestMsg }
	 * 
	 */
	public QueryBalanceRequestMsg createQueryBalanceRequestMsg() {
		return new QueryBalanceRequestMsg();
	}

	/**
	 * Create an instance of {@link PaymentResultMsg }
	 * 
	 */
	public PaymentResultMsg createPaymentResultMsg() {
		return new PaymentResultMsg();
	}

	/**
	 * Create an instance of {@link QueryLoanLogResultMsg }
	 * 
	 */
	public QueryLoanLogResultMsg createQueryLoanLogResultMsg() {
		return new QueryLoanLogResultMsg();
	}

	/**
	 * Create an instance of {@link QueryBalanceResultMsg }
	 * 
	 */
	public QueryBalanceResultMsg createQueryBalanceResultMsg() {
		return new QueryBalanceResultMsg();
	}

	/**
	 * Create an instance of {@link QueryTransferLogResultMsg }
	 * 
	 */
	public QueryTransferLogResultMsg createQueryTransferLogResultMsg() {
		return new QueryTransferLogResultMsg();
	}

	/**
	 * Create an instance of {@link QueryRechargeLogResultMsg }
	 * 
	 */
	public QueryRechargeLogResultMsg createQueryRechargeLogResultMsg() {
		return new QueryRechargeLogResultMsg();
	}

	/**
	 * Create an instance of {@link TransferBalanceResultMsg }
	 * 
	 */
	public TransferBalanceResultMsg createTransferBalanceResultMsg() {
		return new TransferBalanceResultMsg();
	}

	/**
	 * Create an instance of {@link QueryRechargeLogRequestMsg }
	 * 
	 */
	public QueryRechargeLogRequestMsg createQueryRechargeLogRequestMsg() {
		return new QueryRechargeLogRequestMsg();
	}

	/**
	 * Create an instance of {@link QueryLoanLogRequestMsg }
	 * 
	 */
	public QueryLoanLogRequestMsg createQueryLoanLogRequestMsg() {
		return new QueryLoanLogRequestMsg();
	}

	/**
	 * Create an instance of {@link QueryLoanLogRequest }
	 * 
	 */
	public QueryLoanLogRequest createQueryLoanLogRequest() {
		return new QueryLoanLogRequest();
	}

	/**
	 * Create an instance of {@link QueryTransferLogRequestMsg }
	 * 
	 */
	public QueryTransferLogRequestMsg createQueryTransferLogRequestMsg() {
		return new QueryTransferLogRequestMsg();
	}

	/**
	 * Create an instance of {@link QueryPaymentLogResultMsg }
	 * 
	 */
	public QueryPaymentLogResultMsg createQueryPaymentLogResultMsg() {
		return new QueryPaymentLogResultMsg();
	}

	/**
	 * Create an instance of {@link RechargeResultMsg }
	 * 
	 */
	public RechargeResultMsg createRechargeResultMsg() {
		return new RechargeResultMsg();
	}

	/**
	 * Create an instance of {@link TransferBalanceRequestMsg }
	 * 
	 */
	public TransferBalanceRequestMsg createTransferBalanceRequestMsg() {
		return new TransferBalanceRequestMsg();
	}

	/**
	 * Create an instance of {@link QueryInvoiceResultMsg }
	 * 
	 */
	public QueryInvoiceResultMsg createQueryInvoiceResultMsg() {
		return new QueryInvoiceResultMsg();
	}

	/**
	 * Create an instance of {@link QueryInvoicePaymentRequest }
	 * 
	 */
	public QueryInvoicePaymentRequest createQueryInvoicePaymentRequest() {
		return new QueryInvoicePaymentRequest();
	}

	/**
	 * Create an instance of
	 * {@link RechargeRollBackResult.LifeCycleRollBack.OldLifeCycleStatus }
	 * 
	 */
	public RechargeRollBackResult.LifeCycleRollBack.OldLifeCycleStatus createRechargeRollBackResultLifeCycleRollBackOldLifeCycleStatus() {
		return new RechargeRollBackResult.LifeCycleRollBack.OldLifeCycleStatus();
	}

	/**
	 * Create an instance of
	 * {@link RechargeRollBackResult.LifeCycleRollBack.NewLifeCycleStatus }
	 * 
	 */
	public RechargeRollBackResult.LifeCycleRollBack.NewLifeCycleStatus createRechargeRollBackResultLifeCycleRollBackNewLifeCycleStatus() {
		return new RechargeRollBackResult.LifeCycleRollBack.NewLifeCycleStatus();
	}

	/**
	 * Create an instance of
	 * {@link RechargeRollBackResult.BonusRollBack.FreeUnitItemList }
	 * 
	 */
	public RechargeRollBackResult.BonusRollBack.FreeUnitItemList createRechargeRollBackResultBonusRollBackFreeUnitItemList() {
		return new RechargeRollBackResult.BonusRollBack.FreeUnitItemList();
	}

	/**
	 * Create an instance of
	 * {@link RechargeRollBackResult.BonusRollBack.BalanceList }
	 * 
	 */
	public RechargeRollBackResult.BonusRollBack.BalanceList createRechargeRollBackResultBonusRollBackBalanceList() {
		return new RechargeRollBackResult.BonusRollBack.BalanceList();
	}

	/**
	 * Create an instance of
	 * {@link QueryInvoicePaymentResult.InvoicePaymentInfo.CardInfo }
	 * 
	 */
	public QueryInvoicePaymentResult.InvoicePaymentInfo.CardInfo createQueryInvoicePaymentResultInvoicePaymentInfoCardInfo() {
		return new QueryInvoicePaymentResult.InvoicePaymentInfo.CardInfo();
	}

	/**
	 * Create an instance of
	 * {@link RechargeRollBackRequest.RechargeObj.AcctAccessCode }
	 * 
	 */
	public RechargeRollBackRequest.RechargeObj.AcctAccessCode createRechargeRollBackRequestRechargeObjAcctAccessCode() {
		return new RechargeRollBackRequest.RechargeObj.AcctAccessCode();
	}

	/**
	 * Create an instance of
	 * {@link PaymentRollBackRequest.PaymentObj.AcctAccessCode }
	 * 
	 */
	public PaymentRollBackRequest.PaymentObj.AcctAccessCode createPaymentRollBackRequestPaymentObjAcctAccessCode() {
		return new PaymentRollBackRequest.PaymentObj.AcctAccessCode();
	}

	/**
	 * Create an instance of
	 * {@link QueryInvoiceResult.InvoiceInfo.InvoiceDetail }
	 * 
	 */
	public QueryInvoiceResult.InvoiceInfo.InvoiceDetail createQueryInvoiceResultInvoiceInfoInvoiceDetail() {
		return new QueryInvoiceResult.InvoiceInfo.InvoiceDetail();
	}

	/**
	 * Create an instance of {@link TransferBalanceRequest.TransferorAcct }
	 * 
	 */
	public TransferBalanceRequest.TransferorAcct createTransferBalanceRequestTransferorAcct() {
		return new TransferBalanceRequest.TransferorAcct();
	}

	/**
	 * Create an instance of {@link TransferBalanceRequest.TransfereeAcct }
	 * 
	 */
	public TransferBalanceRequest.TransfereeAcct createTransferBalanceRequestTransfereeAcct() {
		return new TransferBalanceRequest.TransfereeAcct();
	}

	/**
	 * Create an instance of
	 * {@link RechargeResult.LifeCycleChgInfo.OldLifeCycleStatus }
	 * 
	 */
	public RechargeResult.LifeCycleChgInfo.OldLifeCycleStatus createRechargeResultLifeCycleChgInfoOldLifeCycleStatus() {
		return new RechargeResult.LifeCycleChgInfo.OldLifeCycleStatus();
	}

	/**
	 * Create an instance of
	 * {@link RechargeResult.LifeCycleChgInfo.NewLifeCycleStatus }
	 * 
	 */
	public RechargeResult.LifeCycleChgInfo.NewLifeCycleStatus createRechargeResultLifeCycleChgInfoNewLifeCycleStatus() {
		return new RechargeResult.LifeCycleChgInfo.NewLifeCycleStatus();
	}

	/**
	 * Create an instance of
	 * {@link RechargeResult.RechargeBonus.FreeUnitItemList }
	 * 
	 */
	public RechargeResult.RechargeBonus.FreeUnitItemList createRechargeResultRechargeBonusFreeUnitItemList() {
		return new RechargeResult.RechargeBonus.FreeUnitItemList();
	}

	/**
	 * Create an instance of {@link RechargeResult.RechargeBonus.BalanceList }
	 * 
	 */
	public RechargeResult.RechargeBonus.BalanceList createRechargeResultRechargeBonusBalanceList() {
		return new RechargeResult.RechargeBonus.BalanceList();
	}

	/**
	 * Create an instance of
	 * {@link QueryPaymentLogResult.PaymentInfo.PaymentDetail }
	 * 
	 */
	public QueryPaymentLogResult.PaymentInfo.PaymentDetail createQueryPaymentLogResultPaymentInfoPaymentDetail() {
		return new QueryPaymentLogResult.PaymentInfo.PaymentDetail();
	}

	/**
	 * Create an instance of {@link QueryPaymentLogResult.PaymentInfo.CardInfo }
	 * 
	 */
	public QueryPaymentLogResult.PaymentInfo.CardInfo createQueryPaymentLogResultPaymentInfoCardInfo() {
		return new QueryPaymentLogResult.PaymentInfo.CardInfo();
	}

	/**
	 * Create an instance of
	 * {@link QueryTransferLogRequest.QueryObj.AcctAccessCode }
	 * 
	 */
	public QueryTransferLogRequest.QueryObj.AcctAccessCode createQueryTransferLogRequestQueryObjAcctAccessCode() {
		return new QueryTransferLogRequest.QueryObj.AcctAccessCode();
	}

	/**
	 * Create an instance of
	 * {@link QueryRechargeLogRequest.QueryObj.AcctAccessCode }
	 * 
	 */
	public QueryRechargeLogRequest.QueryObj.AcctAccessCode createQueryRechargeLogRequestQueryObjAcctAccessCode() {
		return new QueryRechargeLogRequest.QueryObj.AcctAccessCode();
	}

	/**
	 * Create an instance of
	 * {@link TransferBalanceResult.Transferee.LifeCycleChgInfo.OldLifeCycleStatus }
	 * 
	 */
	public TransferBalanceResult.Transferee.LifeCycleChgInfo.OldLifeCycleStatus createTransferBalanceResultTransfereeLifeCycleChgInfoOldLifeCycleStatus() {
		return new TransferBalanceResult.Transferee.LifeCycleChgInfo.OldLifeCycleStatus();
	}

	/**
	 * Create an instance of
	 * {@link TransferBalanceResult.Transferee.LifeCycleChgInfo.NewLifeCycleStatus }
	 * 
	 */
	public TransferBalanceResult.Transferee.LifeCycleChgInfo.NewLifeCycleStatus createTransferBalanceResultTransfereeLifeCycleChgInfoNewLifeCycleStatus() {
		return new TransferBalanceResult.Transferee.LifeCycleChgInfo.NewLifeCycleStatus();
	}

	/**
	 * Create an instance of
	 * {@link TransferBalanceResult.Transferor.LifeCycleChgInfo.OldLifeCycleStatus }
	 * 
	 */
	public TransferBalanceResult.Transferor.LifeCycleChgInfo.OldLifeCycleStatus createTransferBalanceResultTransferorLifeCycleChgInfoOldLifeCycleStatus() {
		return new TransferBalanceResult.Transferor.LifeCycleChgInfo.OldLifeCycleStatus();
	}

	/**
	 * Create an instance of
	 * {@link TransferBalanceResult.Transferor.LifeCycleChgInfo.NewLifeCycleStatus }
	 * 
	 */
	public TransferBalanceResult.Transferor.LifeCycleChgInfo.NewLifeCycleStatus createTransferBalanceResultTransferorLifeCycleChgInfoNewLifeCycleStatus() {
		return new TransferBalanceResult.Transferor.LifeCycleChgInfo.NewLifeCycleStatus();
	}

	/**
	 * Create an instance of
	 * {@link QueryRechargeLogResult.RechargeInfo.CardInfo }
	 * 
	 */
	public QueryRechargeLogResult.RechargeInfo.CardInfo createQueryRechargeLogResultRechargeInfoCardInfo() {
		return new QueryRechargeLogResult.RechargeInfo.CardInfo();
	}

	/**
	 * Create an instance of
	 * {@link QueryRechargeLogResult.RechargeInfo.RechargeBonus.FreeUnitItemList }
	 * 
	 */
	public QueryRechargeLogResult.RechargeInfo.RechargeBonus.FreeUnitItemList createQueryRechargeLogResultRechargeInfoRechargeBonusFreeUnitItemList() {
		return new QueryRechargeLogResult.RechargeInfo.RechargeBonus.FreeUnitItemList();
	}

	/**
	 * Create an instance of
	 * {@link QueryRechargeLogResult.RechargeInfo.RechargeBonus.BalanceList }
	 * 
	 */
	public QueryRechargeLogResult.RechargeInfo.RechargeBonus.BalanceList createQueryRechargeLogResultRechargeInfoRechargeBonusBalanceList() {
		return new QueryRechargeLogResult.RechargeInfo.RechargeBonus.BalanceList();
	}

	/**
	 * Create an instance of
	 * {@link QueryRechargeLogResult.RechargeInfo.LifeCycleChgInfo.OldLifeCycleStatus }
	 * 
	 */
	public QueryRechargeLogResult.RechargeInfo.LifeCycleChgInfo.OldLifeCycleStatus createQueryRechargeLogResultRechargeInfoLifeCycleChgInfoOldLifeCycleStatus() {
		return new QueryRechargeLogResult.RechargeInfo.LifeCycleChgInfo.OldLifeCycleStatus();
	}

	/**
	 * Create an instance of
	 * {@link QueryRechargeLogResult.RechargeInfo.LifeCycleChgInfo.NewLifeCycleStatus }
	 * 
	 */
	public QueryRechargeLogResult.RechargeInfo.LifeCycleChgInfo.NewLifeCycleStatus createQueryRechargeLogResultRechargeInfoLifeCycleChgInfoNewLifeCycleStatus() {
		return new QueryRechargeLogResult.RechargeInfo.LifeCycleChgInfo.NewLifeCycleStatus();
	}

	/**
	 * Create an instance of {@link QueryTransferLogResult.TransferInfo }
	 * 
	 */
	public QueryTransferLogResult.TransferInfo createQueryTransferLogResultTransferInfo() {
		return new QueryTransferLogResult.TransferInfo();
	}

	/**
	 * Create an instance of {@link QueryBalanceResult.AcctList.CorPayRelaList }
	 * 
	 */
	public QueryBalanceResult.AcctList.CorPayRelaList createQueryBalanceResultAcctListCorPayRelaList() {
		return new QueryBalanceResult.AcctList.CorPayRelaList();
	}

	/**
	 * Create an instance of
	 * {@link QueryBalanceResult.AcctList.IndvReserAmountList }
	 * 
	 */
	public QueryBalanceResult.AcctList.IndvReserAmountList createQueryBalanceResultAcctListIndvReserAmountList() {
		return new QueryBalanceResult.AcctList.IndvReserAmountList();
	}

	/**
	 * Create an instance of
	 * {@link QueryBalanceResult.AcctList.CorpReserAmountList }
	 * 
	 */
	public QueryBalanceResult.AcctList.CorpReserAmountList createQueryBalanceResultAcctListCorpReserAmountList() {
		return new QueryBalanceResult.AcctList.CorpReserAmountList();
	}

	/**
	 * Create an instance of
	 * {@link QueryBalanceResult.AcctList.AccountCredit.CreditAmountInfo }
	 * 
	 */
	public QueryBalanceResult.AcctList.AccountCredit.CreditAmountInfo createQueryBalanceResultAcctListAccountCreditCreditAmountInfo() {
		return new QueryBalanceResult.AcctList.AccountCredit.CreditAmountInfo();
	}

	/**
	 * Create an instance of
	 * {@link QueryBalanceResult.AcctList.OutStandingList.OutStandingDetail }
	 * 
	 */
	public QueryBalanceResult.AcctList.OutStandingList.OutStandingDetail createQueryBalanceResultAcctListOutStandingListOutStandingDetail() {
		return new QueryBalanceResult.AcctList.OutStandingList.OutStandingDetail();
	}

	/**
	 * Create an instance of {@link QueryLoanLogResult.RepaymentLogDetail }
	 * 
	 */
	public QueryLoanLogResult.RepaymentLogDetail createQueryLoanLogResultRepaymentLogDetail() {
		return new QueryLoanLogResult.RepaymentLogDetail();
	}

	/**
	 * Create an instance of
	 * {@link QueryLoanLogResult.LoanLogDetail.AdditionalProperty }
	 * 
	 */
	public QueryLoanLogResult.LoanLogDetail.AdditionalProperty createQueryLoanLogResultLoanLogDetailAdditionalProperty() {
		return new QueryLoanLogResult.LoanLogDetail.AdditionalProperty();
	}

	/**
	 * Create an instance of
	 * {@link QueryLoanLogResult.LoanLogSummary.AdditionalProperty }
	 * 
	 */
	public QueryLoanLogResult.LoanLogSummary.AdditionalProperty createQueryLoanLogResultLoanLogSummaryAdditionalProperty() {
		return new QueryLoanLogResult.LoanLogSummary.AdditionalProperty();
	}

	/**
	 * Create an instance of
	 * {@link PaymentResult.OutStandingList.OutStandingDetail }
	 * 
	 */
	public PaymentResult.OutStandingList.OutStandingDetail createPaymentResultOutStandingListOutStandingDetail() {
		return new PaymentResult.OutStandingList.OutStandingDetail();
	}

	/**
	 * Create an instance of
	 * {@link PaymentResult.PaymentBonus.FreeUnitItemList }
	 * 
	 */
	public PaymentResult.PaymentBonus.FreeUnitItemList createPaymentResultPaymentBonusFreeUnitItemList() {
		return new PaymentResult.PaymentBonus.FreeUnitItemList();
	}

	/**
	 * Create an instance of {@link PaymentResult.PaymentBonus.BalanceList }
	 * 
	 */
	public PaymentResult.PaymentBonus.BalanceList createPaymentResultPaymentBonusBalanceList() {
		return new PaymentResult.PaymentBonus.BalanceList();
	}

	/**
	 * Create an instance of
	 * {@link QueryBalanceRequest.QueryObj.AcctAccessCode }
	 * 
	 */
	public QueryBalanceRequest.QueryObj.AcctAccessCode createQueryBalanceRequestQueryObjAcctAccessCode() {
		return new QueryBalanceRequest.QueryObj.AcctAccessCode();
	}

	/**
	 * Create an instance of {@link QueryPaymentLogRequest.AcctAccessCode }
	 * 
	 */
	public QueryPaymentLogRequest.AcctAccessCode createQueryPaymentLogRequestAcctAccessCode() {
		return new QueryPaymentLogRequest.AcctAccessCode();
	}

	/**
	 * Create an instance of {@link RechargeRequest.RechargeInfo.CardPayment }
	 * 
	 */
	public RechargeRequest.RechargeInfo.CardPayment createRechargeRequestRechargeInfoCardPayment() {
		return new RechargeRequest.RechargeInfo.CardPayment();
	}

	/**
	 * Create an instance of {@link RechargeRequest.RechargeInfo.CashPayment }
	 * 
	 */
	public RechargeRequest.RechargeInfo.CashPayment createRechargeRequestRechargeInfoCashPayment() {
		return new RechargeRequest.RechargeInfo.CashPayment();
	}

	/**
	 * Create an instance of {@link RechargeRequest.RechargeObj.AcctAccessCode }
	 * 
	 */
	public RechargeRequest.RechargeObj.AcctAccessCode createRechargeRequestRechargeObjAcctAccessCode() {
		return new RechargeRequest.RechargeObj.AcctAccessCode();
	}

	/**
	 * Create an instance of {@link PaymentRequest.PaymentInfo.CardPayment }
	 * 
	 */
	public PaymentRequest.PaymentInfo.CardPayment createPaymentRequestPaymentInfoCardPayment() {
		return new PaymentRequest.PaymentInfo.CardPayment();
	}

	/**
	 * Create an instance of
	 * {@link PaymentRequest.PaymentInfo.CashPayment.ApplyList }
	 * 
	 */
	public PaymentRequest.PaymentInfo.CashPayment.ApplyList createPaymentRequestPaymentInfoCashPaymentApplyList() {
		return new PaymentRequest.PaymentInfo.CashPayment.ApplyList();
	}

	/**
	 * Create an instance of {@link PaymentRequest.PaymentObj.AcctAccessCode }
	 * 
	 */
	public PaymentRequest.PaymentObj.AcctAccessCode createPaymentRequestPaymentObjAcctAccessCode() {
		return new PaymentRequest.PaymentObj.AcctAccessCode();
	}

	/**
	 * Create an instance of {@link QueryInvoiceRequest.AcctAccessCode }
	 * 
	 */
	public QueryInvoiceRequest.AcctAccessCode createQueryInvoiceRequestAcctAccessCode() {
		return new QueryInvoiceRequest.AcctAccessCode();
	}

	/**
	 * Create an instance of {@link QueryInvoiceRequest.TimePeriod }
	 * 
	 */
	public QueryInvoiceRequest.TimePeriod createQueryInvoiceRequestTimePeriod() {
		return new QueryInvoiceRequest.TimePeriod();
	}
}
