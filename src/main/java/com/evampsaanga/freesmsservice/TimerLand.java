package com.evampsaanga.freesmsservice;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import com.evampsaanga.bakcell.db.DBFactory;
import com.evampsaanga.cache.UserGroupDataCache;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.developer.utils.Helper;

public class TimerLand {
	public static final Logger logger = Logger.getLogger("bakcellLogs-V2");

	public void clean() {
		// make it true for server 5. and false for server 6.
		if (true) {
			// if("10.220.48.198".equals(Helper.currentMachineIPAddress()) ||
			// "10.220.48.5".equals(Helper.currentMachineIPAddress()))
			logger.info("TIMERLAND:Create Table is executed from this server");
			String newTableName = new SimpleDateFormat("yyyyMMdd").format(new Date());

			for (int i = 0; i < 10; i++) {
				logger.info("hitting route" + i);
			}
			try {

				RenameTable(newTableName);
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				clean();
				return;
			}
			try {

				createSameTable(newTableName);
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				try {
					createSameTable(newTableName);
				} catch (Exception ex2) {
					logger.error(Helper.GetException(ex2));
				}
			}
		}
		// else {
		// logger.info("TIMERLAND:Create Table is not executed from this
		// server");
		// }
	}

	public void refreshPICUsers() {
		UserGroupDataCache.users.clear();
		UserGroupDataCache.usersCount.clear();
	}

	private static String tableName = "sendfreesms";

	static {
		tableName = ConfigurationManager.getDBProperties("sendfreesmstable.name");
	}

	private void createSameTable(String newTableName) {
		// TODO Auto-generated method stub
		String query = "CREATE TABLE " + tableName + "  LIKE " + tableName + newTableName + "";
		try (Connection connection = DBFactory.getDbConnectionFromPool();
				PreparedStatement stmt = connection.prepareStatement(query);) {
			logger.info("Executed for running RunHistoryReports " + stmt.executeUpdate());
		} catch (SQLException ex) {
			logger.error(Helper.GetException(ex));
		}
	}

	private void RenameTable(String newTableName) {
		String query = "RENAME TABLE `" + tableName + "` TO `" + tableName + newTableName + "`;";
		try (Connection con = DBFactory.getDbConnectionFromPool();
				PreparedStatement stmt = con.prepareStatement(query);) {
			logger.info("Executed for running RunHistoryReports " + stmt.executeUpdate());
			stmt.close();
		} catch (SQLException ex) {
			logger.error(Helper.GetException(ex));
		}
	}
}
