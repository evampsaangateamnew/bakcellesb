package com.nsn.devicemanagement.wsdl.common._2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for SettingDeleteAction complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="SettingDeleteAction">
 *   &lt;complexContent>
 *     &lt;extension base="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}Action">
 *       &lt;sequence>
 *         &lt;element name="confirmation" type="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}message" minOccurs="0"/>
 *         &lt;element name="notification" type="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}message" minOccurs="0"/>
 *         &lt;element name="settings" type="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}setting" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SettingDeleteAction", propOrder = { "confirmation", "notification", "settings" })
public class SettingDeleteAction extends Action {
	protected Message confirmation;
	protected Message notification;
	@XmlElement(nillable = true)
	protected List<Setting> settings;

	/**
	 * Gets the value of the confirmation property.
	 * 
	 * @return possible object is {@link Message }
	 * 
	 */
	public Message getConfirmation() {
		return confirmation;
	}

	/**
	 * Sets the value of the confirmation property.
	 * 
	 * @param value
	 *            allowed object is {@link Message }
	 * 
	 */
	public void setConfirmation(Message value) {
		this.confirmation = value;
	}

	/**
	 * Gets the value of the notification property.
	 * 
	 * @return possible object is {@link Message }
	 * 
	 */
	public Message getNotification() {
		return notification;
	}

	/**
	 * Sets the value of the notification property.
	 * 
	 * @param value
	 *            allowed object is {@link Message }
	 * 
	 */
	public void setNotification(Message value) {
		this.notification = value;
	}

	/**
	 * Gets the value of the settings property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the settings property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getSettings().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list {@link Setting }
	 * 
	 * 
	 */
	public List<Setting> getSettings() {
		if (settings == null) {
			settings = new ArrayList<Setting>();
		}
		return this.settings;
	}
}
