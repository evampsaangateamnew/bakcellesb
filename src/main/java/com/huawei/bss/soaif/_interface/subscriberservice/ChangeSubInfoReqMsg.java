package com.huawei.bss.soaif._interface.subscriberservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bss.soaif._interface.common.ObjectAccessInfo;
import com.huawei.bss.soaif._interface.common.ReqHeader;
import com.huawei.bss.soaif._interface.common.SimpleProperty;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.huawei.com/bss/soaif/interface/common/}ReqHeader"/>
 *         &lt;sequence>
 *           &lt;element name="AccessInfo" type="{http://www.huawei.com/bss/soaif/interface/common/}ObjectAccessInfo"/>
 *           &lt;element name="Subscriber">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="Language" type="{http://www.huawei.com/bss/soaif/interface/common/}Language" minOccurs="0"/>
 *                     &lt;element name="WrittenLanguage" type="{http://www.huawei.com/bss/soaif/interface/common/}Language" minOccurs="0"/>
 *                     &lt;element name="IMEI" type="{http://www.huawei.com/bss/soaif/interface/common/}IMEI" minOccurs="0"/>
 *                     &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "reqHeader", "accessInfo", "subscriber" })
@XmlRootElement(name = "ChangeSubInfoReqMsg")
public class ChangeSubInfoReqMsg {
	@XmlElement(name = "ReqHeader", namespace = "http://www.huawei.com/bss/soaif/interface/common/", required = true)
	protected ReqHeader reqHeader;
	@XmlElement(name = "AccessInfo", required = true)
	protected ObjectAccessInfo accessInfo;
	@XmlElement(name = "Subscriber", required = true)
	protected ChangeSubInfoReqMsg.Subscriber subscriber;

	/**
	 * Gets the value of the reqHeader property.
	 * 
	 * @return possible object is {@link ReqHeader }
	 * 
	 */
	public ReqHeader getReqHeader() {
		return reqHeader;
	}

	/**
	 * Sets the value of the reqHeader property.
	 * 
	 * @param value
	 *            allowed object is {@link ReqHeader }
	 * 
	 */
	public void setReqHeader(ReqHeader value) {
		this.reqHeader = value;
	}

	/**
	 * Gets the value of the accessInfo property.
	 * 
	 * @return possible object is {@link ObjectAccessInfo }
	 * 
	 */
	public ObjectAccessInfo getAccessInfo() {
		return accessInfo;
	}

	/**
	 * Sets the value of the accessInfo property.
	 * 
	 * @param value
	 *            allowed object is {@link ObjectAccessInfo }
	 * 
	 */
	public void setAccessInfo(ObjectAccessInfo value) {
		this.accessInfo = value;
	}

	/**
	 * Gets the value of the subscriber property.
	 * 
	 * @return possible object is {@link ChangeSubInfoReqMsg.Subscriber }
	 * 
	 */
	public ChangeSubInfoReqMsg.Subscriber getSubscriber() {
		return subscriber;
	}

	/**
	 * Sets the value of the subscriber property.
	 * 
	 * @param value
	 *            allowed object is {@link ChangeSubInfoReqMsg.Subscriber }
	 * 
	 */
	public void setSubscriber(ChangeSubInfoReqMsg.Subscriber value) {
		this.subscriber = value;
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="Language" type="{http://www.huawei.com/bss/soaif/interface/common/}Language" minOccurs="0"/>
	 *         &lt;element name="WrittenLanguage" type="{http://www.huawei.com/bss/soaif/interface/common/}Language" minOccurs="0"/>
	 *         &lt;element name="IMEI" type="{http://www.huawei.com/bss/soaif/interface/common/}IMEI" minOccurs="0"/>
	 *         &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "language", "writtenLanguage", "imei", "additionalProperty" })
	public static class Subscriber {
		@XmlElement(name = "Language")
		protected String language;
		@XmlElement(name = "WrittenLanguage")
		protected String writtenLanguage;
		@XmlElement(name = "IMEI")
		protected String imei;
		@XmlElement(name = "AdditionalProperty")
		protected List<SimpleProperty> additionalProperty;

		/**
		 * Gets the value of the language property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getLanguage() {
			return language;
		}

		/**
		 * Sets the value of the language property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setLanguage(String value) {
			this.language = value;
		}

		/**
		 * Gets the value of the writtenLanguage property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getWrittenLanguage() {
			return writtenLanguage;
		}

		/**
		 * Sets the value of the writtenLanguage property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setWrittenLanguage(String value) {
			this.writtenLanguage = value;
		}

		/**
		 * Gets the value of the imei property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getIMEI() {
			return imei;
		}

		/**
		 * Sets the value of the imei property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setIMEI(String value) {
			this.imei = value;
		}

		/**
		 * Gets the value of the additionalProperty property.
		 * 
		 * <p>
		 * This accessor method returns a reference to the live list, not a
		 * snapshot. Therefore any modification you make to the returned list
		 * will be present inside the JAXB object. This is why there is not a
		 * <CODE>set</CODE> method for the additionalProperty property.
		 * 
		 * <p>
		 * For example, to add a new item, do as follows:
		 * 
		 * <pre>
		 * getAdditionalProperty().add(newItem);
		 * </pre>
		 * 
		 * 
		 * <p>
		 * Objects of the following type(s) are allowed in the list
		 * {@link SimpleProperty }
		 * 
		 * 
		 */
		public List<SimpleProperty> getAdditionalProperty() {
			if (additionalProperty == null) {
				additionalProperty = new ArrayList<SimpleProperty>();
			}
			return this.additionalProperty;
		}
	}
}
