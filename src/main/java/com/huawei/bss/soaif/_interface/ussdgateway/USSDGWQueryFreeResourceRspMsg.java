package com.huawei.bss.soaif._interface.ussdgateway;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bss.soaif._interface.common.RspHeader;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.huawei.com/bss/soaif/interface/common/}RspHeader"/>
 *         &lt;sequence>
 *           &lt;element name="FreeUnitItem" maxOccurs="unbounded" minOccurs="0">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="FreeUnitType" type="{http://www.huawei.com/bss/soaif/interface/common/}string"/>
 *                     &lt;element name="FreeUnitTypeName" type="{http://www.huawei.com/bss/soaif/interface/common/}string"/>
 *                     &lt;element name="MeasureUnit" type="{http://www.huawei.com/bss/soaif/interface/common/}int"/>
 *                     &lt;element name="MeasureUnitName" type="{http://www.huawei.com/bss/soaif/interface/common/}string"/>
 *                     &lt;element name="TotalInitialAmount" type="{http://www.huawei.com/bss/soaif/interface/common/}long"/>
 *                     &lt;element name="TotalUnusedAmount" type="{http://www.huawei.com/bss/soaif/interface/common/}long"/>
 *                     &lt;element name="FreeUnitItemDetail" maxOccurs="unbounded">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="FreeUnitInstanceID" type="{http://www.huawei.com/bss/soaif/interface/common/}long"/>
 *                               &lt;element name="InitialAmount" type="{http://www.huawei.com/bss/soaif/interface/common/}long"/>
 *                               &lt;element name="CurrentAmount" type="{http://www.huawei.com/bss/soaif/interface/common/}string"/>
 *                               &lt;element name="EffectiveTime" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime"/>
 *                               &lt;element name="ExpireTime" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime"/>
 *                               &lt;element name="RollOverFlag" type="{http://www.huawei.com/bss/soaif/interface/common/}string"/>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "rspHeader", "freeUnitItem" })
@XmlRootElement(name = "USSDGWQueryFreeResourceRspMsg")
public class USSDGWQueryFreeResourceRspMsg {
	@XmlElement(name = "RspHeader", namespace = "http://www.huawei.com/bss/soaif/interface/common/", required = true)
	protected RspHeader rspHeader;
	@XmlElement(name = "FreeUnitItem")
	protected List<USSDGWQueryFreeResourceRspMsg.FreeUnitItem> freeUnitItem;

	/**
	 * Gets the value of the rspHeader property.
	 * 
	 * @return possible object is {@link RspHeader }
	 * 
	 */
	public RspHeader getRspHeader() {
		return rspHeader;
	}

	/**
	 * Sets the value of the rspHeader property.
	 * 
	 * @param value
	 *            allowed object is {@link RspHeader }
	 * 
	 */
	public void setRspHeader(RspHeader value) {
		this.rspHeader = value;
	}

	/**
	 * Gets the value of the freeUnitItem property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the freeUnitItem property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getFreeUnitItem().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link USSDGWQueryFreeResourceRspMsg.FreeUnitItem }
	 * 
	 * 
	 */
	public List<USSDGWQueryFreeResourceRspMsg.FreeUnitItem> getFreeUnitItem() {
		if (freeUnitItem == null) {
			freeUnitItem = new ArrayList<USSDGWQueryFreeResourceRspMsg.FreeUnitItem>();
		}
		return this.freeUnitItem;
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="FreeUnitType" type="{http://www.huawei.com/bss/soaif/interface/common/}string"/>
	 *         &lt;element name="FreeUnitTypeName" type="{http://www.huawei.com/bss/soaif/interface/common/}string"/>
	 *         &lt;element name="MeasureUnit" type="{http://www.huawei.com/bss/soaif/interface/common/}int"/>
	 *         &lt;element name="MeasureUnitName" type="{http://www.huawei.com/bss/soaif/interface/common/}string"/>
	 *         &lt;element name="TotalInitialAmount" type="{http://www.huawei.com/bss/soaif/interface/common/}long"/>
	 *         &lt;element name="TotalUnusedAmount" type="{http://www.huawei.com/bss/soaif/interface/common/}long"/>
	 *         &lt;element name="FreeUnitItemDetail" maxOccurs="unbounded">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="FreeUnitInstanceID" type="{http://www.huawei.com/bss/soaif/interface/common/}long"/>
	 *                   &lt;element name="InitialAmount" type="{http://www.huawei.com/bss/soaif/interface/common/}long"/>
	 *                   &lt;element name="CurrentAmount" type="{http://www.huawei.com/bss/soaif/interface/common/}string"/>
	 *                   &lt;element name="EffectiveTime" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime"/>
	 *                   &lt;element name="ExpireTime" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime"/>
	 *                   &lt;element name="RollOverFlag" type="{http://www.huawei.com/bss/soaif/interface/common/}string"/>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "freeUnitType", "freeUnitTypeName", "measureUnit", "measureUnitName",
			"totalInitialAmount", "totalUnusedAmount", "freeUnitItemDetail" })
	public static class FreeUnitItem {
		@XmlElement(name = "FreeUnitType", required = true)
		protected String freeUnitType;
		@XmlElement(name = "FreeUnitTypeName", required = true)
		protected String freeUnitTypeName;
		@XmlElement(name = "MeasureUnit")
		protected String measureUnit;
		@XmlElement(name = "MeasureUnitName", required = true)
		protected String measureUnitName;
		@XmlElement(name = "TotalInitialAmount")
		protected String totalInitialAmount;
		@XmlElement(name = "TotalUnusedAmount")
		protected String totalUnusedAmount;
		@XmlElement(name = "FreeUnitItemDetail", required = true)
		protected List<USSDGWQueryFreeResourceRspMsg.FreeUnitItem.FreeUnitItemDetail> freeUnitItemDetail;

		/**
		 * Gets the value of the freeUnitType property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getFreeUnitType() {
			return freeUnitType;
		}

		/**
		 * Sets the value of the freeUnitType property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setFreeUnitType(String value) {
			this.freeUnitType = value;
		}

		/**
		 * Gets the value of the freeUnitTypeName property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getFreeUnitTypeName() {
			return freeUnitTypeName;
		}

		/**
		 * Sets the value of the freeUnitTypeName property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setFreeUnitTypeName(String value) {
			this.freeUnitTypeName = value;
		}

		/**
		 * Gets the value of the measureUnit property.
		 * 
		 */
		public String getMeasureUnit() {
			return measureUnit;
		}

		/**
		 * Sets the value of the measureUnit property.
		 * 
		 */
		public void setMeasureUnit(String value) {
			this.measureUnit = value;
		}

		/**
		 * Gets the value of the measureUnitName property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getMeasureUnitName() {
			return measureUnitName;
		}

		/**
		 * Sets the value of the measureUnitName property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setMeasureUnitName(String value) {
			this.measureUnitName = value;
		}

		/**
		 * Gets the value of the totalInitialAmount property.
		 * 
		 */
		public String getTotalInitialAmount() {
			return totalInitialAmount;
		}

		/**
		 * Sets the value of the totalInitialAmount property.
		 * 
		 */
		public void setTotalInitialAmount(String value) {
			this.totalInitialAmount = value;
		}

		/**
		 * Gets the value of the totalUnusedAmount property.
		 * 
		 */
		public String getTotalUnusedAmount() {
			return totalUnusedAmount;
		}

		/**
		 * Sets the value of the totalUnusedAmount property.
		 * 
		 */
		public void setTotalUnusedAmount(String value) {
			this.totalUnusedAmount = value;
		}

		/**
		 * Gets the value of the freeUnitItemDetail property.
		 * 
		 * <p>
		 * This accessor method returns a reference to the live list, not a
		 * snapshot. Therefore any modification you make to the returned list
		 * will be present inside the JAXB object. This is why there is not a
		 * <CODE>set</CODE> method for the freeUnitItemDetail property.
		 * 
		 * <p>
		 * For example, to add a new item, do as follows:
		 * 
		 * <pre>
		 * getFreeUnitItemDetail().add(newItem);
		 * </pre>
		 * 
		 * 
		 * <p>
		 * Objects of the following type(s) are allowed in the list
		 * {@link USSDGWQueryFreeResourceRspMsg.FreeUnitItem.FreeUnitItemDetail }
		 * 
		 * 
		 */
		public List<USSDGWQueryFreeResourceRspMsg.FreeUnitItem.FreeUnitItemDetail> getFreeUnitItemDetail() {
			if (freeUnitItemDetail == null) {
				freeUnitItemDetail = new ArrayList<USSDGWQueryFreeResourceRspMsg.FreeUnitItem.FreeUnitItemDetail>();
			}
			return this.freeUnitItemDetail;
		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * 
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="FreeUnitInstanceID" type="{http://www.huawei.com/bss/soaif/interface/common/}long"/>
		 *         &lt;element name="InitialAmount" type="{http://www.huawei.com/bss/soaif/interface/common/}long"/>
		 *         &lt;element name="CurrentAmount" type="{http://www.huawei.com/bss/soaif/interface/common/}string"/>
		 *         &lt;element name="EffectiveTime" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime"/>
		 *         &lt;element name="ExpireTime" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime"/>
		 *         &lt;element name="RollOverFlag" type="{http://www.huawei.com/bss/soaif/interface/common/}string"/>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "freeUnitInstanceID", "initialAmount", "currentAmount", "effectiveTime",
				"expireTime", "rollOverFlag" })
		public static class FreeUnitItemDetail {
			@XmlElement(name = "FreeUnitInstanceID")
			protected String freeUnitInstanceID;
			@XmlElement(name = "InitialAmount")
			protected String initialAmount;
			@XmlElement(name = "CurrentAmount", required = true)
			protected String currentAmount;
			@XmlElement(name = "EffectiveTime", required = true)
			protected String effectiveTime;
			@XmlElement(name = "ExpireTime", required = true)
			protected String expireTime;
			@XmlElement(name = "RollOverFlag", required = true)
			protected String rollOverFlag;

			/**
			 * Gets the value of the freeUnitInstanceID property.
			 * 
			 */
			public String getFreeUnitInstanceID() {
				return freeUnitInstanceID;
			}

			/**
			 * Sets the value of the freeUnitInstanceID property.
			 * 
			 */
			public void setFreeUnitInstanceID(String value) {
				this.freeUnitInstanceID = value;
			}

			/**
			 * Gets the value of the initialAmount property.
			 * 
			 */
			public String getInitialAmount() {
				return initialAmount;
			}

			/**
			 * Sets the value of the initialAmount property.
			 * 
			 */
			public void setInitialAmount(String value) {
				this.initialAmount = value;
			}

			/**
			 * Gets the value of the currentAmount property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getCurrentAmount() {
				return currentAmount;
			}

			/**
			 * Sets the value of the currentAmount property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setCurrentAmount(String value) {
				this.currentAmount = value;
			}

			/**
			 * Gets the value of the effectiveTime property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getEffectiveTime() {
				return effectiveTime;
			}

			/**
			 * Sets the value of the effectiveTime property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setEffectiveTime(String value) {
				this.effectiveTime = value;
			}

			/**
			 * Gets the value of the expireTime property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getExpireTime() {
				return expireTime;
			}

			/**
			 * Sets the value of the expireTime property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setExpireTime(String value) {
				this.expireTime = value;
			}

			/**
			 * Gets the value of the rollOverFlag property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getRollOverFlag() {
				return rollOverFlag;
			}

			/**
			 * Sets the value of the rollOverFlag property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setRollOverFlag(String value) {
				this.rollOverFlag = value;
			}
		}
	}
}
