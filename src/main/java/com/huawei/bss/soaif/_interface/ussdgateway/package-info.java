@javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters({
		@javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter(value = com.huawei.crm.service.LongXMLAdapter.class, type = Long.class) })
@javax.xml.bind.annotation.XmlSchema(namespace = "http://www.huawei.com/bss/soaif/interface/USSDGateWay/", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package com.huawei.bss.soaif._interface.ussdgateway;
