package com.evampsaanga.mhmclient;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.ThirdPartyNames;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.developer.utils.SOAPLoggingHandler;
import com.evampsaanga.services.OrderHandleService;
import com.huawei.crm.basetype.ens.ChargeFeeInfo;
import com.huawei.crm.basetype.ens.ChargeFeeList;
import com.huawei.crm.basetype.ens.OrderInfo;
import com.huawei.crm.basetype.ens.OrderItemInfo;
import com.huawei.crm.basetype.ens.OrderItemValue;
import com.huawei.crm.basetype.ens.OrderItems;
import com.huawei.crm.basetype.ens.SubscriberInfo;
import com.huawei.crm.service.ens.SubmitOrderResponse;

import https.www_e_gov.AddApplicationResponse;
import https.www_e_gov.CheckVoenValidResponse;
import https.wwww_e_gov.RequestApplication;
import https.wwww_e_gov.RequestApplicationTypes;
import https.wwww_e_gov.RequestAuthentication;
import https.wwww_e_gov.RequestDocumentInformation;
import https.wwww_e_gov.RequestSellerInformation;
import https.wwww_e_gov.RequestVoenInformation;
import https.wwww_e_gov.ResponseApplicationInfo;
import https.wwww_e_gov.ResponseInformationByPin;
import https.wwww_e_gov.ResponseInformationByVoen;
import net.x_rd.az.voen9900037691.producer.AddApplicationRequest;
import net.x_rd.az.voen9900037691.producer.ArrayOfRequestApplication;
import net.x_rd.az.voen9900037691.producer.CheckDocumentValidRequest;
import net.x_rd.az.voen9900037691.producer.CheckVoenValidRequest;

@Path("/bakcell")
public class SimSwapService {
	public static final Logger logger = Logger.getLogger("bakcellLogs-V2");
	public static final Logger loggerV2 = Logger.getLogger("bakcellLogs-V2");
	public static String modeulName = "SimSWAP";

	@POST
	@Path("/verify")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SimSwapResponse verify(@Body String requestBody, @Header("credentials") String credential) {
		logger.info(modeulName + "Request: Verify" + requestBody);
		logger.info(modeulName + "credential: verify" + credential);
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.SIM_SWAP_VERIFY);
		logs.setThirdPartyName(ThirdPartyNames.THIRD_PARTY_VERIFY);
		// logs.setTableType(LogsType.verify);
		SimSwapRequest cclient = null;
		SimSwapResponse resp = new SimSwapResponse();
		try {
			cclient = Helper.JsonToObject(requestBody, SimSwapRequest.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
				logs.setIsB2B(cclient.getIsB2B());
			}
		} catch (Exception ex) {
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_401);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {

				try {
					logger.info(modeulName + ":" + Helper.ObjectToJson(cclient));
					CheckVoenValidResponse checkVoenValidResponse = checkVoienValid(cclient);
					logger.info(modeulName + ":" + Helper.ObjectToJson(checkVoenValidResponse));

					if (checkVoenValidResponse.getCheckVoenValidResult().getStatus().getCode() == 200) {
						resp.setTransactionId(checkVoenValidResponse.getCheckVoenValidResult().getTransactionId());
						resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					} else {
						resp.setReturnCode(
								String.valueOf(checkVoenValidResponse.getCheckVoenValidResult().getStatus().getCode()));
						resp.setReturnMsg(checkVoenValidResponse.getCheckVoenValidResult().getStatus().getText());
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());

					}
				} catch (Exception ex) {
					loggerV2.info(Helper.GetException(ex));
					resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
					resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;

				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		logs.updateLog(logs);
		return resp;
	}

	//
	//
	// @POST
	// @Path("/documentValid")
	// @Produces(MediaType.APPLICATION_JSON)
	// @Consumes(MediaType.APPLICATION_JSON)
	// public SimSwapResponse documentValid(@Body String requestBody,
	// @Header("credentials") String credential) {
	// Helper.logInfoMessageV2("Request Data: upload Document" + requestBody);
	// Helper.logInfoMessageV2("credential: upload Document" + credential);
	// Logs logs = new Logs();
	// logs.setTransactionName(Transactions.SIM_SWAP_UPLOAD_FILE);
	// logs.setThirdPartyName(ThirdPartyNames.THIRD_PARTY_UPLOAD_FILE);
	// // logs.setTableType(LogsType.UploadFile);
	// SimSwapRequest cclient = null;
	// SimSwapResponse resp = new SimSwapResponse();
	// try {
	// cclient = Helper.JsonToObject(requestBody, SimSwapRequest.class);
	// if (cclient != null) {
	// logs.setIp(cclient.getiP());
	// logs.setChannel(cclient.getChannel());
	// logs.setMsisdn(cclient.getmsisdn());
	// logs.setLang(cclient.getLang());
	// logs.setIsB2B(cclient.getIsB2B());
	// }
	// } catch (Exception ex) {
	// loggerV2.error("Error:", ex);
	// resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
	// resp.setReturnMsg(ResponseCodes.ERROR_401);
	// logs.setResponseCode(resp.getReturnCode());
	// logs.setResponseDescription(resp.getReturnMsg());
	// logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
	// logs.updateLog(logs);
	// return resp;
	// }
	// if (cclient != null) {
	// String credentials = null;
	// try {
	// credentials = Decrypter.getInstance().decrypt(credential);
	// Helper.logInfoMessageV2(cclient.getmsisdn() + " - Credentials " +
	// credential);
	// } catch (Exception ex) {
	// loggerV2.error(Helper.GetException(ex));
	// resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
	// resp.setReturnMsg(ResponseCodes.ERROR_401);
	// logs.setResponseCode(resp.getReturnCode());
	// logs.setResponseDescription(resp.getReturnMsg());
	// logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
	// logs.updateLog(logs);
	// return resp;
	// }
	// if (credentials == null) {
	// Helper.logInfoMessageV2(cclient.getmsisdn() + " - Credentials are null");
	// resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
	// resp.setReturnMsg(ResponseCodes.ERROR_401);
	// logs.setResponseCode(resp.getReturnCode());
	// logs.setResponseDescription(resp.getReturnMsg());
	// logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
	// logs.updateLog(logs);
	// return resp;
	// }
	// if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
	// String verification = Helper.validateRequest(cclient);
	// if (!verification.equals("")) {
	// Helper.logInfoMessageV2(cclient.getmsisdn() + " - Verification failed");
	// resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
	// resp.setReturnMsg(verification);
	// logs.setResponseCode(resp.getReturnCode());
	// logs.setResponseDescription(resp.getReturnMsg());
	// logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
	// logs.updateLog(logs);
	// return resp;
	// }
	// } else {
	// Helper.logInfoMessageV2(cclient.getmsisdn() + " - Incorrect MSISDN");
	// resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
	// resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
	// logs.setResponseCode(resp.getReturnCode());
	// logs.setResponseDescription(resp.getReturnMsg());
	// logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
	// logs.updateLog(logs);
	// return resp;
	// }
	// if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
	//
	// try {
	// logger.info(modeulName + ":" + Helper.ObjectToJson(cclient));
	// ResponseInformationByPin responseInformationByVoen =
	// checkDocumentValid(cclient);
	// logger.info(modeulName + ":" +
	// Helper.ObjectToJson(responseInformationByVoen));
	// if (responseInformationByVoen.getStatus().getCode() == 200) {
	// resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
	// resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
	// logs.setResponseCode(resp.getReturnCode());
	// logs.setResponseDescription(resp.getReturnMsg());
	// logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
	// logs.updateLog(logs);
	// return resp;
	// } else {
	// resp.setReturnCode(String.valueOf(responseInformationByVoen.getStatus().getCode()));
	// resp.setReturnMsg(responseInformationByVoen.getStatus().getText());
	// logs.setResponseCode(resp.getReturnCode());
	// logs.setResponseDescription(resp.getReturnMsg());
	// logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
	// logs.updateLog(logs);
	// }
	// } catch (Exception ex) {
	// loggerV2.info(Helper.GetException(ex));
	// resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
	// resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
	// logs.setResponseCode(resp.getReturnCode());
	// logs.setResponseDescription(resp.getReturnMsg());
	// logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
	// logs.updateLog(logs);
	// return resp;
	//
	// }
	// } else {
	// Helper.logInfoMessageV2(cclient.getmsisdn() + " - 401 Access Not
	// Authorized ");
	// resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
	// resp.setReturnMsg(ResponseCodes.ERROR_401);
	// logs.setResponseCode(resp.getReturnCode());
	// logs.setResponseDescription(resp.getReturnMsg());
	// logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
	// logs.updateLog(logs);
	// return resp;
	// }
	// }
	// logs.updateLog(logs);
	// return resp;
	// }
	//
	// @POST
	// @Path("/addApplication")
	// @Produces(MediaType.APPLICATION_JSON)
	// @Consumes(MediaType.APPLICATION_JSON)
	// public SimSwapResponse addApplication(@Body String requestBody,
	// @Header("credentials") String credential) {
	// Helper.logInfoMessageV2("Request Data: add Application" + requestBody);
	// Helper.logInfoMessageV2("credential: add Application" + credential);
	// Logs logs = new Logs();
	// logs.setTransactionName(Transactions.SIM_SWAP_ADD_APPLICATION);
	// logs.setThirdPartyName(ThirdPartyNames.THIRD_PARTY_ADD_APPLICATION);
	// // logs.setTableType(LogsType.AddApplication);
	// SimSwapRequest cclient = null;
	// SimSwapResponse resp = new SimSwapResponse();
	// try {
	// cclient = Helper.JsonToObject(requestBody, SimSwapRequest.class);
	// if (cclient != null) {
	// logs.setIp(cclient.getiP());
	// logs.setChannel(cclient.getChannel());
	// logs.setMsisdn(cclient.getmsisdn());
	// logs.setLang(cclient.getLang());
	// logs.setIsB2B(cclient.getIsB2B());
	// }
	// } catch (Exception ex) {
	// loggerV2.error("Error:", ex);
	// resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
	// resp.setReturnMsg(ResponseCodes.ERROR_401);
	// logs.setResponseCode(resp.getReturnCode());
	// logs.setResponseDescription(resp.getReturnMsg());
	// logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
	// logs.updateLog(logs);
	// return resp;
	// }
	// if (cclient != null) {
	// String credentials = null;
	// try {
	// credentials = Decrypter.getInstance().decrypt(credential);
	// Helper.logInfoMessageV2(cclient.getmsisdn() + " - Credentials " +
	// credential);
	// } catch (Exception ex) {
	// loggerV2.error(Helper.GetException(ex));
	// resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
	// resp.setReturnMsg(ResponseCodes.ERROR_401);
	// logs.setResponseCode(resp.getReturnCode());
	// logs.setResponseDescription(resp.getReturnMsg());
	// logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
	// logs.updateLog(logs);
	// return resp;
	// }
	// if (credentials == null) {
	// Helper.logInfoMessageV2(cclient.getmsisdn() + " - Credentials are null");
	// resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
	// resp.setReturnMsg(ResponseCodes.ERROR_401);
	// logs.setResponseCode(resp.getReturnCode());
	// logs.setResponseDescription(resp.getReturnMsg());
	// logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
	// logs.updateLog(logs);
	// return resp;
	// }
	// if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
	// String verification = Helper.validateRequest(cclient);
	// if (!verification.equals("")) {
	// Helper.logInfoMessageV2(cclient.getmsisdn() + " - Verification failed");
	// resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
	// resp.setReturnMsg(verification);
	// logs.setResponseCode(resp.getReturnCode());
	// logs.setResponseDescription(resp.getReturnMsg());
	// logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
	// logs.updateLog(logs);
	// return resp;
	// }
	// } else {
	// Helper.logInfoMessageV2(cclient.getmsisdn() + " - Incorrect MSISDN");
	// resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
	// resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
	// logs.setResponseCode(resp.getReturnCode());
	// logs.setResponseDescription(resp.getReturnMsg());
	// logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
	// logs.updateLog(logs);
	// return resp;
	// }
	// if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
	//
	// try {
	// logger.info(modeulName + ":" + Helper.ObjectToJson(cclient));
	// AddApplicationResponse addApplicationResponse = addApplication(cclient);
	// logger.info(modeulName + ":" +
	// Helper.ObjectToJson(addApplicationResponse));
	// if
	// (addApplicationResponse.getAddApplicationResult().getStatus().getCode()
	// == 0) {
	//
	// logger.info(modeulName + ":" + "Add Application passed");
	// SubmitOrderResponse submitOrderResponse =
	// ngbssForSimSWAP(cclient.getmsisdn(),
	// cclient.getGsmNumber(), cclient.getIccid(), cclient.getCustomerId());
	// if (submitOrderResponse.getResponseHeader().getRetCode().equals("0")) {
	// resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
	// resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
	// logs.setResponseCode(resp.getReturnCode());
	// logs.setResponseDescription(resp.getReturnMsg());
	// logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
	// logs.updateLog(logs);
	// } else {
	// resp.setReturnCode(submitOrderResponse.getResponseHeader().getRetCode());
	// resp.setReturnMsg(submitOrderResponse.getResponseHeader().getRetMsg());
	// logs.setResponseCode(resp.getReturnCode());
	// logs.setResponseDescription(resp.getReturnMsg());
	// logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
	// logs.updateLog(logs);
	// }
	//
	// return resp;
	// } else {
	// resp.setReturnCode(
	// String.valueOf(addApplicationResponse.getAddApplicationResult().getStatus().getCode()));
	// resp.setReturnMsg(addApplicationResponse.getAddApplicationResult().getStatus().getText());
	// logs.setResponseCode(resp.getReturnCode());
	// logs.setResponseDescription(resp.getReturnMsg());
	// logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
	// logs.updateLog(logs);
	// }
	// } catch (Exception ex) {
	// loggerV2.info(Helper.GetException(ex));
	// resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
	// resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
	// logs.setResponseCode(resp.getReturnCode());
	// logs.setResponseDescription(resp.getReturnMsg());
	// logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
	// logs.updateLog(logs);
	// return resp;
	//
	// }
	// } else {
	// Helper.logInfoMessageV2(cclient.getmsisdn() + " - 401 Access Not
	// Authorized ");
	// resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
	// resp.setReturnMsg(ResponseCodes.ERROR_401);
	// logs.setResponseCode(resp.getReturnCode());
	// logs.setResponseDescription(resp.getReturnMsg());
	// logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
	// logs.updateLog(logs);
	// return resp;
	// }
	// }
	// // logs.updateLog(logs);
	// return resp;
	// }

	public SubmitOrderResponse ngbssForSimSWAP(String corporateMsisdn, String simMsisdn, String iccid,
			String customerId) throws IOException, Exception {

		// String customerCrmAccountId =
		// Helper.getCustomerCRMAccountId(customerId,corporateMsisdn,simMsisdn);

		com.huawei.crm.service.ens.SubmitOrderRequest submitOrderRequestMsgReq = new com.huawei.crm.service.ens.SubmitOrderRequest();
		submitOrderRequestMsgReq.setRequestHeader(getReqHeaderForGETNetworkSettingsimswap());
		com.huawei.crm.service.ens.SubmitRequestBody submitRequestBody = new com.huawei.crm.service.ens.SubmitRequestBody();
		OrderInfo oInfo = new OrderInfo();
		oInfo.setOrderType(Constants.SIM_SWAP_ORDERTYPE);// CO016
		OrderItemInfo oitemInfo = new OrderItemInfo();
		oitemInfo.setOrderItemType(Constants.SIM_SWAP_ORDERTYPE);
		oitemInfo.setIsCustomerNotification(Constants.SIM_SWAP_NOTIFICATION);
		oitemInfo.setIsPartnerNotification(Constants.SIM_SWAP_ISPARTNER_NOTIFICATION);
		submitRequestBody.setOrder(oInfo);
		OrderItems OrderItems = new OrderItems();
		OrderItemValue OrderItemValue = new OrderItemValue();
		OrderItemValue.setOrderItemInfo(oitemInfo);
		SubscriberInfo subscriber = new SubscriberInfo();
		subscriber.setServiceNumber(simMsisdn);
		subscriber.setICCID(iccid);
		com.huawei.crm.basetype.ens.BusinessFee businessFee = new com.huawei.crm.basetype.ens.BusinessFee();
		com.huawei.crm.basetype.ens.ChargeInfo chargeInfo = new com.huawei.crm.basetype.ens.ChargeInfo();
		chargeInfo.setFeeActionType("1");
		// chargeInfo.setAccountId(Long.parseLong(customerCrmAccountId));
		ChargeFeeList chargeFeeList = new ChargeFeeList();
		ChargeFeeInfo chargeFeeInfo = new ChargeFeeInfo();
		chargeFeeInfo.setChargeType("C_FEE_DEDUCTION_CHARGE_CODE");
		chargeFeeInfo.setPayType("2");
		chargeFeeInfo.setCurrency("1009");

		chargeFeeInfo.setUnitPrice(0L);
		chargeFeeInfo.setAmount(0L);
		chargeFeeInfo.setTaxAmount(0L);

		chargeFeeInfo.setQuantity("1");
		chargeFeeInfo.setChargeRemark("test");
		chargeFeeList.getChargeFeeInfo().add(chargeFeeInfo);
		chargeInfo.setChargeFeeList(chargeFeeList);
		// chargeInfo.setFeeActionType("2");
		com.huawei.crm.basetype.ens.ChargeInfo value;
		businessFee.setCharge(chargeInfo);

		OrderItemValue.setSubscriber(subscriber);

		OrderItemValue.setBusinessFee(businessFee);
		OrderItems.getOrderItem().add(OrderItemValue);

		submitRequestBody.setOrderItems(OrderItems);
		submitOrderRequestMsgReq.setSubmitRequestBody(submitRequestBody);

		com.huawei.crm.service.ens.SubmitOrderResponse response = OrderHandleService.getInstance()
				.submitOrder(submitOrderRequestMsgReq);
		logger.info(corporateMsisdn + ":" + " SimSwap NGBSS Request " + Helper.ObjectToJson(submitOrderRequestMsgReq));
		return response;

	}

	private static com.huawei.crm.basetype.ens.RequestHeader getReqHeaderForGETNetworkSettingsimswap() {
		com.huawei.crm.basetype.ens.RequestHeader reqH = new com.huawei.crm.basetype.ens.RequestHeader();
		reqH.setVersion("1");
		reqH.setChannelId("3");
		reqH.setTechnicalChannelId("53");
		reqH.setAccessUser("ecare");
		reqH.setAccessPwd("r8q0a5WwGNboj9I35XzNcQ==");
		reqH.setTransactionId(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
		reqH.setTenantId("101");
		reqH.setTestFlag("0");
		reqH.setLanguage("2002");

		return reqH;
	}

	public AddApplicationResponse addApplication(SimSwapRequest cclient) {
		AddApplicationRequest addApplicationRequest = new AddApplicationRequest();
		RequestAuthentication requestAuthentication = new RequestAuthentication();
		requestAuthentication.setRequestKey("ywEZtV5u0KRhY3MRH8WXNQn00");
		requestAuthentication.setRequestName("bakcell");
		addApplicationRequest.setAuthentication(requestAuthentication);
		RequestApplication requestApplication = new RequestApplication();
		// if(cclient.getAddType()!=null)
		// requestApplication.setAddType(cclient.getAddType());
		if (cclient.getContact() != null)
			requestApplication.setContact(cclient.getContact());
		if (cclient.getEmail() != null)
			requestApplication.setEmail(cclient.getEmail());
		if (cclient.getGsmNumber() != null)
			requestApplication.setGsmNumber(cclient.getGsmNumber());
		if (cclient.getIccid() != null)
			requestApplication.setICCID(cclient.getIccid());
		if (cclient.getImei() != null)
			requestApplication.setIMEI(cclient.getImei());
		if (cclient.getImsi() != null)
			requestApplication.setIMSI(cclient.getImsi());

		///////////////////// Adding Transaction Id in add
		///////////////////// Application///////////////////
		if (cclient.getTransactionId() != null)
			addApplicationRequest.setTransactionId(cclient.getTransactionId());

		RequestApplicationTypes value = new RequestApplicationTypes();
		value.setAddType("3");
		value.setDeviceType("90");
		value.setUsingType("1");
		value.setSignatureType("1");
		addApplicationRequest.setApplicationTypes(value);
		///////////////////// ArrayOfRequestApplication
		///////////////////// //////////////////////////////////
		ArrayOfRequestApplication arrayOfRequestApplication = new ArrayOfRequestApplication();
		arrayOfRequestApplication.getRequestApplication().add(requestApplication);
		addApplicationRequest.setApplicationInformation(arrayOfRequestApplication);

		ResponseApplicationInfo responseApplicationInfo = MHMService.getInstance()
				.addApplication(addApplicationRequest);
		AddApplicationResponse addApplicationResponse = new AddApplicationResponse();
		addApplicationResponse.setAddApplicationResult(responseApplicationInfo);
		return addApplicationResponse;
	}

	private static ResponseInformationByPin checkDocumentValid(SimSwapRequest cclient) {
		// TODO Auto-generated method stub

		CheckDocumentValidRequest checkDocumentValidRequest = new CheckDocumentValidRequest();
		RequestAuthentication requestAuthentication = new RequestAuthentication();
		requestAuthentication.setRequestKey("ywEZtV5u0KRhY3MRH8WXNQn00");
		requestAuthentication.setRequestName("bakcell");
		checkDocumentValidRequest.setAuthentication(requestAuthentication);

		RequestSellerInformation requestSellerInformation = new RequestSellerInformation();
		requestSellerInformation.setPlaceAddress("Neftchiler Ave.Port Baku Business Center");
		requestSellerInformation.setPlaceCode("5555");
		requestSellerInformation.setPlaceName("B2B Sales");
		requestSellerInformation.setPlaceType("1");

		requestSellerInformation.setSellerContact("558452929");
		requestSellerInformation.setSellerDocumentNumber("Test");
		requestSellerInformation.setSellerDocumentPin("Test");
		requestSellerInformation.setSellerFullname("B2B Sales");
		requestSellerInformation.setSellerUsername("Bakcell LLC");
		requestSellerInformation.setRegions("10090");

		RequestDocumentInformation requestDocumentInformation = new RequestDocumentInformation();
		if (cclient.getDocumentNumber() != null) {
			requestDocumentInformation.setDocumentNumber(cclient.getDocumentNumber());
		}
		if (cclient.getDocumentPin() != null)
			requestDocumentInformation.setDocumentPin(cclient.getDocumentPin());
		if (cclient.getDocumentType() != null)
			requestDocumentInformation.setDocumentType(cclient.getDocumentType());

		checkDocumentValidRequest.setSellerInformation(requestSellerInformation);
		checkDocumentValidRequest.setDocumentInformation(requestDocumentInformation);

		ResponseInformationByPin responseInformationByPin = MHMService.getInstance()
				.checkDocumentValid(checkDocumentValidRequest);
		return responseInformationByPin;

	}

	private static CheckVoenValidResponse checkVoienValid(SimSwapRequest cclient) {
		// TODO Auto-generated method stub
		CheckVoenValidRequest checkVoenValidRequest = new CheckVoenValidRequest();
		RequestAuthentication requestAuthentication = new RequestAuthentication();
		requestAuthentication.setRequestKey("ywEZtV5u0KRhY3MRH8WXNQn00");
		requestAuthentication.setRequestName("bakcell");
		checkVoenValidRequest.setAuthentication(requestAuthentication);

		RequestVoenInformation requestVoenInformation = new RequestVoenInformation();
		// requestVoenInformation.setSun("10110058419");
		// requestVoenInformation.setVoenNumber("1004243801");
		requestVoenInformation.setSun(cclient.getSun());
		requestVoenInformation.setVoenNumber(cclient.getVoen());

		checkVoenValidRequest.setVoenInformation(requestVoenInformation);

		RequestDocumentInformation requestDocumentInformation = new RequestDocumentInformation();
		if (cclient.getDocumentNumber() != null)
			requestDocumentInformation.setDocumentNumber(cclient.getDocumentNumber());
		if (cclient.getDocumentPin() != null)
			requestDocumentInformation.setDocumentPin(cclient.getDocumentPin());
		if (cclient.getDocumentType() != null)
			requestDocumentInformation.setDocumentType(cclient.getDocumentType());

		checkVoenValidRequest.setDocumentInformation(requestDocumentInformation);

		RequestSellerInformation requestSellerInformation = new RequestSellerInformation();
		requestSellerInformation.setPlaceAddress("Neftchiler Ave.Port Baku Business Center");
		requestSellerInformation.setPlaceCode("5555");
		requestSellerInformation.setPlaceName("B2B Sales");
		requestSellerInformation.setPlaceType("1");

		requestSellerInformation.setSellerContact("558452929");
		requestSellerInformation.setSellerDocumentNumber("Test");
		requestSellerInformation.setSellerDocumentPin("Test");
		requestSellerInformation.setSellerFullname("B2B Sales");
		requestSellerInformation.setSellerUsername("Bakcell LLC");
		requestSellerInformation.setRegions("10090");

		checkVoenValidRequest.setSellerInformation(requestSellerInformation);
		ResponseInformationByVoen responseInformationByPin = MHMService.getInstance()
				.checkVoenValid(checkVoenValidRequest);
		CheckVoenValidResponse checkVoenValidResponse = new CheckVoenValidResponse();
		checkVoenValidResponse.setCheckVoenValidResult(responseInformationByPin);
		return checkVoenValidResponse;

	}

}
