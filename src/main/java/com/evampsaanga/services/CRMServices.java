package com.evampsaanga.services;

import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.developer.utils.SoapHandlerService;
import com.huawei.crm.basetype.RequestHeader;
import com.huawei.crm.service.HuaweiCRMPortType;
import com.huawei.crm.service.OrderQuery;

public class CRMServices {
	private static HuaweiCRMPortType crmPortType = null;

	private CRMServices() {
	}

	public static synchronized HuaweiCRMPortType getInstance() {
		if (crmPortType == null) {
			crmPortType = new OrderQuery().getHuaweiCRMPort();
			SoapHandlerService.configureBinding(crmPortType);
		}
		return crmPortType;
	}
	

	
	//TODO
		public static com.huawei.crm.basetype.RequestHeader getReqHeaderForGETNetworkSetting() {
			 com.huawei.crm.basetype.RequestHeader reqH = new  com.huawei.crm.basetype.RequestHeader();
			reqH.setVersion(Constants.VERSION);
			reqH.setChannelId(ConfigurationManager.getConfigurationFromCache("order.handle.channelid"));
			reqH.setTechnicalChannelId(Constants.TECHNICAL_CHANEL_ID);
			reqH.setAccessUser(ConfigurationManager.getConfigurationFromCache("order.handle.user"));
			reqH.setAccessPwd(ConfigurationManager.getConfigurationFromCache("order.handle.password"));
			reqH.setTransactionId(Helper.generateTransactionID());
			return reqH;
		}
	
}