package com.evampsaanga.ulduzumupdate.getunusedcodes;

import com.evampsaanga.bakcell.requestheaders.BaseRequest;

public class GetUnusedCodeRequestClient extends BaseRequest {

	private String identicalCode;
	private String insertDate;

	public String getIdenticalCode() {
		return identicalCode;
	}

	public void setIdenticalCode(String identicalCode) {
		this.identicalCode = identicalCode;
	}

	public String getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(String insertDate) {
		this.insertDate = insertDate;
	}

	@Override
	public String toString() {
		return "GetUnusedCodeRequestClient [identicalCode=" + identicalCode + ", insertDate=" + insertDate + "]";
	}

}
