package com.huawei.crm.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.crm.basetype.CalcOneOffFeeInfoForQuery;
import com.huawei.crm.basetype.ResponseHeader;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResponseHeader" type="{http://crm.huawei.com/basetype/}ResponseHeader"/>
 *         &lt;element name="GetOfferSalesFeeBody">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="GetOfferSalesFeeList" type="{http://crm.huawei.com/basetype/}CalcOneOffFeeInfoForQuery" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "responseHeader", "getOfferSalesFeeBody" })
@XmlRootElement(name = "GetOfferSalesFeeResponse")
public class GetOfferSalesFeeResponse {
	@XmlElement(name = "ResponseHeader", required = true)
	protected ResponseHeader responseHeader;
	@XmlElement(name = "GetOfferSalesFeeBody", required = true)
	protected GetOfferSalesFeeResponse.GetOfferSalesFeeBody getOfferSalesFeeBody;

	/**
	 * Gets the value of the responseHeader property.
	 * 
	 * @return possible object is {@link ResponseHeader }
	 * 
	 */
	public ResponseHeader getResponseHeader() {
		return responseHeader;
	}

	/**
	 * Sets the value of the responseHeader property.
	 * 
	 * @param value
	 *            allowed object is {@link ResponseHeader }
	 * 
	 */
	public void setResponseHeader(ResponseHeader value) {
		this.responseHeader = value;
	}

	/**
	 * Gets the value of the getOfferSalesFeeBody property.
	 * 
	 * @return possible object is
	 *         {@link GetOfferSalesFeeResponse.GetOfferSalesFeeBody }
	 * 
	 */
	public GetOfferSalesFeeResponse.GetOfferSalesFeeBody getGetOfferSalesFeeBody() {
		return getOfferSalesFeeBody;
	}

	/**
	 * Sets the value of the getOfferSalesFeeBody property.
	 * 
	 * @param value
	 *            allowed object is
	 *            {@link GetOfferSalesFeeResponse.GetOfferSalesFeeBody }
	 * 
	 */
	public void setGetOfferSalesFeeBody(GetOfferSalesFeeResponse.GetOfferSalesFeeBody value) {
		this.getOfferSalesFeeBody = value;
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="GetOfferSalesFeeList" type="{http://crm.huawei.com/basetype/}CalcOneOffFeeInfoForQuery" maxOccurs="unbounded" minOccurs="0"/>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "getOfferSalesFeeList" })
	public static class GetOfferSalesFeeBody {
		@XmlElement(name = "GetOfferSalesFeeList")
		protected List<CalcOneOffFeeInfoForQuery> getOfferSalesFeeList;

		/**
		 * Gets the value of the getOfferSalesFeeList property.
		 * 
		 * <p>
		 * This accessor method returns a reference to the live list, not a
		 * snapshot. Therefore any modification you make to the returned list
		 * will be present inside the JAXB object. This is why there is not a
		 * <CODE>set</CODE> method for the getOfferSalesFeeList property.
		 * 
		 * <p>
		 * For example, to add a new item, do as follows:
		 * 
		 * <pre>
		 * getGetOfferSalesFeeList().add(newItem);
		 * </pre>
		 * 
		 * 
		 * <p>
		 * Objects of the following type(s) are allowed in the list
		 * {@link CalcOneOffFeeInfoForQuery }
		 * 
		 * 
		 */
		public List<CalcOneOffFeeInfoForQuery> getGetOfferSalesFeeList() {
			if (getOfferSalesFeeList == null) {
				getOfferSalesFeeList = new ArrayList<CalcOneOffFeeInfoForQuery>();
			}
			return this.getOfferSalesFeeList;
		}
	}
}
