
package com.huawei.bss.soaif._interface.common.createorder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LifeCycleStatus complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LifeCycleStatus">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StatusName">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="64"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="StatusIndex" type="{http://www.huawei.com/bss/soaif/interface/common/}SubStatus"/>
 *         &lt;element name="StatusExpireTime" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LifeCycleStatus", propOrder = {
    "statusName",
    "statusIndex",
    "statusExpireTime"
})
public class LifeCycleStatus {

    @XmlElement(name = "StatusName", required = true)
    protected String statusName;
    @XmlElement(name = "StatusIndex", required = true)
    protected String statusIndex;
    @XmlElement(name = "StatusExpireTime", required = true)
    protected String statusExpireTime;

    /**
     * Gets the value of the statusName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusName() {
        return statusName;
    }

    /**
     * Sets the value of the statusName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusName(String value) {
        this.statusName = value;
    }

    /**
     * Gets the value of the statusIndex property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusIndex() {
        return statusIndex;
    }

    /**
     * Sets the value of the statusIndex property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusIndex(String value) {
        this.statusIndex = value;
    }

    /**
     * Gets the value of the statusExpireTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusExpireTime() {
        return statusExpireTime;
    }

    /**
     * Sets the value of the statusExpireTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusExpireTime(String value) {
        this.statusExpireTime = value;
    }

}
