package com.huawei.bss.soaif._interface.subscriberservice;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bss.soaif._interface.common.BillMedium;
import com.huawei.bss.soaif._interface.common.CreditLimit;
import com.huawei.bss.soaif._interface.common.EffectiveMode;
import com.huawei.bss.soaif._interface.common.Name;
import com.huawei.bss.soaif._interface.common.ObjectAccessInfo;
import com.huawei.bss.soaif._interface.common.OfferingId;
import com.huawei.bss.soaif._interface.common.OfferingInstance;
import com.huawei.bss.soaif._interface.common.PaymentChannel;
import com.huawei.bss.soaif._interface.common.ReqHeader;
import com.huawei.bss.soaif._interface.common.SimpleProperty;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.huawei.com/bss/soaif/interface/common/}ReqHeader"/>
 *         &lt;sequence>
 *           &lt;element name="AccessInfo">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}ObjectAccessInfo">
 *                 &lt;/extension>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="PostpaidCust" minOccurs="0">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="CustId" type="{http://www.huawei.com/bss/soaif/interface/common/}CustId"/>
 *                     &lt;element name="Title" type="{http://www.huawei.com/bss/soaif/interface/common/}Title" minOccurs="0"/>
 *                     &lt;element name="Name" type="{http://www.huawei.com/bss/soaif/interface/common/}Name" minOccurs="0"/>
 *                     &lt;element name="Nationality" type="{http://www.huawei.com/bss/soaif/interface/common/}Nationality" minOccurs="0"/>
 *                     &lt;element name="Birthday" type="{http://www.huawei.com/bss/soaif/interface/common/}Date" minOccurs="0"/>
 *                     &lt;element name="Contact" maxOccurs="unbounded" minOccurs="0">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}Contact">
 *                             &lt;sequence>
 *                               &lt;element name="ActionType" type="{http://www.huawei.com/bss/soaif/interface/common/}ActionType"/>
 *                             &lt;/sequence>
 *                           &lt;/extension>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="Address" maxOccurs="unbounded" minOccurs="0">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}Address">
 *                             &lt;sequence>
 *                               &lt;element name="ActionType" type="{http://www.huawei.com/bss/soaif/interface/common/}ActionType"/>
 *                             &lt;/sequence>
 *                           &lt;/extension>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="PostpaidAcct">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;choice>
 *                     &lt;element name="ExistingAcctId" type="{http://www.huawei.com/bss/soaif/interface/common/}AcctId"/>
 *                     &lt;element name="Account">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="CustId" type="{http://www.huawei.com/bss/soaif/interface/common/}CustId"/>
 *                               &lt;element name="AcctName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                               &lt;element name="PaymentType" type="{http://www.huawei.com/bss/soaif/interface/common/}PaymentType" minOccurs="0"/>
 *                               &lt;element name="Title" type="{http://www.huawei.com/bss/soaif/interface/common/}Title" minOccurs="0"/>
 *                               &lt;element name="Name" type="{http://www.huawei.com/bss/soaif/interface/common/}Name" minOccurs="0"/>
 *                               &lt;element name="BillCycleType" type="{http://www.huawei.com/bss/soaif/interface/common/}BillCycleType" minOccurs="0"/>
 *                               &lt;element name="BillLanguage" type="{http://www.huawei.com/bss/soaif/interface/common/}Language" minOccurs="0"/>
 *                               &lt;element name="Contact" type="{http://www.huawei.com/bss/soaif/interface/common/}Contact" minOccurs="0"/>
 *                               &lt;element name="Address" type="{http://www.huawei.com/bss/soaif/interface/common/}Address" maxOccurs="unbounded" minOccurs="0"/>
 *                               &lt;element name="BillMedium" type="{http://www.huawei.com/bss/soaif/interface/common/}BillMedium" maxOccurs="unbounded" minOccurs="0"/>
 *                               &lt;element name="Currency" type="{http://www.huawei.com/bss/soaif/interface/common/}Currency" minOccurs="0"/>
 *                               &lt;element name="InitialBalance" type="{http://www.huawei.com/bss/soaif/interface/common/}Amount" minOccurs="0"/>
 *                               &lt;element name="CreditLimit" type="{http://www.huawei.com/bss/soaif/interface/common/}CreditLimit" maxOccurs="unbounded" minOccurs="0"/>
 *                               &lt;element name="AcctPayMethod" type="{http://www.huawei.com/bss/soaif/interface/common/}AcctPayMethod" minOccurs="0"/>
 *                               &lt;element name="PaymentChannel" type="{http://www.huawei.com/bss/soaif/interface/common/}PaymentChannel" maxOccurs="unbounded" minOccurs="0"/>
 *                               &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                   &lt;/choice>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="Offering">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="OldPrimaryOffering" type="{http://www.huawei.com/bss/soaif/interface/common/}OfferingId" minOccurs="0"/>
 *                     &lt;element name="NewPrimaryOffering">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}OfferingInstance">
 *                             &lt;sequence>
 *                               &lt;element name="EffectiveMode" type="{http://www.huawei.com/bss/soaif/interface/common/}EffectiveMode"/>
 *                             &lt;/sequence>
 *                           &lt;/extension>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "reqHeader", "accessInfo", "postpaidCust", "postpaidAcct", "offering" })
@XmlRootElement(name = "ChangePrepaidToPostpaidReqMsg")
public class ChangePrepaidToPostpaidReqMsg {
	@XmlElement(name = "ReqHeader", namespace = "http://www.huawei.com/bss/soaif/interface/common/", required = true)
	protected ReqHeader reqHeader;
	@XmlElement(name = "AccessInfo", required = true)
	protected ChangePrepaidToPostpaidReqMsg.AccessInfo accessInfo;
	@XmlElement(name = "PostpaidCust")
	protected ChangePrepaidToPostpaidReqMsg.PostpaidCust postpaidCust;
	@XmlElement(name = "PostpaidAcct", required = true)
	protected ChangePrepaidToPostpaidReqMsg.PostpaidAcct postpaidAcct;
	@XmlElement(name = "Offering", required = true)
	protected ChangePrepaidToPostpaidReqMsg.Offering offering;

	/**
	 * Gets the value of the reqHeader property.
	 * 
	 * @return possible object is {@link ReqHeader }
	 * 
	 */
	public ReqHeader getReqHeader() {
		return reqHeader;
	}

	/**
	 * Sets the value of the reqHeader property.
	 * 
	 * @param value
	 *            allowed object is {@link ReqHeader }
	 * 
	 */
	public void setReqHeader(ReqHeader value) {
		this.reqHeader = value;
	}

	/**
	 * Gets the value of the accessInfo property.
	 * 
	 * @return possible object is
	 *         {@link ChangePrepaidToPostpaidReqMsg.AccessInfo }
	 * 
	 */
	public ChangePrepaidToPostpaidReqMsg.AccessInfo getAccessInfo() {
		return accessInfo;
	}

	/**
	 * Sets the value of the accessInfo property.
	 * 
	 * @param value
	 *            allowed object is
	 *            {@link ChangePrepaidToPostpaidReqMsg.AccessInfo }
	 * 
	 */
	public void setAccessInfo(ChangePrepaidToPostpaidReqMsg.AccessInfo value) {
		this.accessInfo = value;
	}

	/**
	 * Gets the value of the postpaidCust property.
	 * 
	 * @return possible object is
	 *         {@link ChangePrepaidToPostpaidReqMsg.PostpaidCust }
	 * 
	 */
	public ChangePrepaidToPostpaidReqMsg.PostpaidCust getPostpaidCust() {
		return postpaidCust;
	}

	/**
	 * Sets the value of the postpaidCust property.
	 * 
	 * @param value
	 *            allowed object is
	 *            {@link ChangePrepaidToPostpaidReqMsg.PostpaidCust }
	 * 
	 */
	public void setPostpaidCust(ChangePrepaidToPostpaidReqMsg.PostpaidCust value) {
		this.postpaidCust = value;
	}

	/**
	 * Gets the value of the postpaidAcct property.
	 * 
	 * @return possible object is
	 *         {@link ChangePrepaidToPostpaidReqMsg.PostpaidAcct }
	 * 
	 */
	public ChangePrepaidToPostpaidReqMsg.PostpaidAcct getPostpaidAcct() {
		return postpaidAcct;
	}

	/**
	 * Sets the value of the postpaidAcct property.
	 * 
	 * @param value
	 *            allowed object is
	 *            {@link ChangePrepaidToPostpaidReqMsg.PostpaidAcct }
	 * 
	 */
	public void setPostpaidAcct(ChangePrepaidToPostpaidReqMsg.PostpaidAcct value) {
		this.postpaidAcct = value;
	}

	/**
	 * Gets the value of the offering property.
	 * 
	 * @return possible object is
	 *         {@link ChangePrepaidToPostpaidReqMsg.Offering }
	 * 
	 */
	public ChangePrepaidToPostpaidReqMsg.Offering getOffering() {
		return offering;
	}

	/**
	 * Sets the value of the offering property.
	 * 
	 * @param value
	 *            allowed object is
	 *            {@link ChangePrepaidToPostpaidReqMsg.Offering }
	 * 
	 */
	public void setOffering(ChangePrepaidToPostpaidReqMsg.Offering value) {
		this.offering = value;
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}ObjectAccessInfo">
	 *     &lt;/extension>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "")
	public static class AccessInfo extends ObjectAccessInfo {
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="OldPrimaryOffering" type="{http://www.huawei.com/bss/soaif/interface/common/}OfferingId" minOccurs="0"/>
	 *         &lt;element name="NewPrimaryOffering">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}OfferingInstance">
	 *                 &lt;sequence>
	 *                   &lt;element name="EffectiveMode" type="{http://www.huawei.com/bss/soaif/interface/common/}EffectiveMode"/>
	 *                 &lt;/sequence>
	 *               &lt;/extension>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "oldPrimaryOffering", "newPrimaryOffering" })
	public static class Offering {
		@XmlElement(name = "OldPrimaryOffering")
		protected OfferingId oldPrimaryOffering;
		@XmlElement(name = "NewPrimaryOffering", required = true)
		protected ChangePrepaidToPostpaidReqMsg.Offering.NewPrimaryOffering newPrimaryOffering;

		/**
		 * Gets the value of the oldPrimaryOffering property.
		 * 
		 * @return possible object is {@link OfferingId }
		 * 
		 */
		public OfferingId getOldPrimaryOffering() {
			return oldPrimaryOffering;
		}

		/**
		 * Sets the value of the oldPrimaryOffering property.
		 * 
		 * @param value
		 *            allowed object is {@link OfferingId }
		 * 
		 */
		public void setOldPrimaryOffering(OfferingId value) {
			this.oldPrimaryOffering = value;
		}

		/**
		 * Gets the value of the newPrimaryOffering property.
		 * 
		 * @return possible object is
		 *         {@link ChangePrepaidToPostpaidReqMsg.Offering.NewPrimaryOffering }
		 * 
		 */
		public ChangePrepaidToPostpaidReqMsg.Offering.NewPrimaryOffering getNewPrimaryOffering() {
			return newPrimaryOffering;
		}

		/**
		 * Sets the value of the newPrimaryOffering property.
		 * 
		 * @param value
		 *            allowed object is
		 *            {@link ChangePrepaidToPostpaidReqMsg.Offering.NewPrimaryOffering }
		 * 
		 */
		public void setNewPrimaryOffering(ChangePrepaidToPostpaidReqMsg.Offering.NewPrimaryOffering value) {
			this.newPrimaryOffering = value;
		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * 
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}OfferingInstance">
		 *       &lt;sequence>
		 *         &lt;element name="EffectiveMode" type="{http://www.huawei.com/bss/soaif/interface/common/}EffectiveMode"/>
		 *       &lt;/sequence>
		 *     &lt;/extension>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "effectiveMode" })
		public static class NewPrimaryOffering extends OfferingInstance {
			@XmlElement(name = "EffectiveMode", required = true)
			protected EffectiveMode effectiveMode;

			/**
			 * Gets the value of the effectiveMode property.
			 * 
			 * @return possible object is {@link EffectiveMode }
			 * 
			 */
			public EffectiveMode getEffectiveMode() {
				return effectiveMode;
			}

			/**
			 * Sets the value of the effectiveMode property.
			 * 
			 * @param value
			 *            allowed object is {@link EffectiveMode }
			 * 
			 */
			public void setEffectiveMode(EffectiveMode value) {
				this.effectiveMode = value;
			}
		}
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;choice>
	 *         &lt;element name="ExistingAcctId" type="{http://www.huawei.com/bss/soaif/interface/common/}AcctId"/>
	 *         &lt;element name="Account">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="CustId" type="{http://www.huawei.com/bss/soaif/interface/common/}CustId"/>
	 *                   &lt;element name="AcctName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *                   &lt;element name="PaymentType" type="{http://www.huawei.com/bss/soaif/interface/common/}PaymentType" minOccurs="0"/>
	 *                   &lt;element name="Title" type="{http://www.huawei.com/bss/soaif/interface/common/}Title" minOccurs="0"/>
	 *                   &lt;element name="Name" type="{http://www.huawei.com/bss/soaif/interface/common/}Name" minOccurs="0"/>
	 *                   &lt;element name="BillCycleType" type="{http://www.huawei.com/bss/soaif/interface/common/}BillCycleType" minOccurs="0"/>
	 *                   &lt;element name="BillLanguage" type="{http://www.huawei.com/bss/soaif/interface/common/}Language" minOccurs="0"/>
	 *                   &lt;element name="Contact" type="{http://www.huawei.com/bss/soaif/interface/common/}Contact" minOccurs="0"/>
	 *                   &lt;element name="Address" type="{http://www.huawei.com/bss/soaif/interface/common/}Address" maxOccurs="unbounded" minOccurs="0"/>
	 *                   &lt;element name="BillMedium" type="{http://www.huawei.com/bss/soaif/interface/common/}BillMedium" maxOccurs="unbounded" minOccurs="0"/>
	 *                   &lt;element name="Currency" type="{http://www.huawei.com/bss/soaif/interface/common/}Currency" minOccurs="0"/>
	 *                   &lt;element name="InitialBalance" type="{http://www.huawei.com/bss/soaif/interface/common/}Amount" minOccurs="0"/>
	 *                   &lt;element name="CreditLimit" type="{http://www.huawei.com/bss/soaif/interface/common/}CreditLimit" maxOccurs="unbounded" minOccurs="0"/>
	 *                   &lt;element name="AcctPayMethod" type="{http://www.huawei.com/bss/soaif/interface/common/}AcctPayMethod" minOccurs="0"/>
	 *                   &lt;element name="PaymentChannel" type="{http://www.huawei.com/bss/soaif/interface/common/}PaymentChannel" maxOccurs="unbounded" minOccurs="0"/>
	 *                   &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *       &lt;/choice>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "existingAcctId", "account" })
	public static class PostpaidAcct {
		@XmlElement(name = "ExistingAcctId")
		protected String existingAcctId;
		@XmlElement(name = "Account")
		protected ChangePrepaidToPostpaidReqMsg.PostpaidAcct.Account account;

		/**
		 * Gets the value of the existingAcctId property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getExistingAcctId() {
			return existingAcctId;
		}

		/**
		 * Sets the value of the existingAcctId property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setExistingAcctId(String value) {
			this.existingAcctId = value;
		}

		/**
		 * Gets the value of the account property.
		 * 
		 * @return possible object is
		 *         {@link ChangePrepaidToPostpaidReqMsg.PostpaidAcct.Account }
		 * 
		 */
		public ChangePrepaidToPostpaidReqMsg.PostpaidAcct.Account getAccount() {
			return account;
		}

		/**
		 * Sets the value of the account property.
		 * 
		 * @param value
		 *            allowed object is
		 *            {@link ChangePrepaidToPostpaidReqMsg.PostpaidAcct.Account }
		 * 
		 */
		public void setAccount(ChangePrepaidToPostpaidReqMsg.PostpaidAcct.Account value) {
			this.account = value;
		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * 
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="CustId" type="{http://www.huawei.com/bss/soaif/interface/common/}CustId"/>
		 *         &lt;element name="AcctName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
		 *         &lt;element name="PaymentType" type="{http://www.huawei.com/bss/soaif/interface/common/}PaymentType" minOccurs="0"/>
		 *         &lt;element name="Title" type="{http://www.huawei.com/bss/soaif/interface/common/}Title" minOccurs="0"/>
		 *         &lt;element name="Name" type="{http://www.huawei.com/bss/soaif/interface/common/}Name" minOccurs="0"/>
		 *         &lt;element name="BillCycleType" type="{http://www.huawei.com/bss/soaif/interface/common/}BillCycleType" minOccurs="0"/>
		 *         &lt;element name="BillLanguage" type="{http://www.huawei.com/bss/soaif/interface/common/}Language" minOccurs="0"/>
		 *         &lt;element name="Contact" type="{http://www.huawei.com/bss/soaif/interface/common/}Contact" minOccurs="0"/>
		 *         &lt;element name="Address" type="{http://www.huawei.com/bss/soaif/interface/common/}Address" maxOccurs="unbounded" minOccurs="0"/>
		 *         &lt;element name="BillMedium" type="{http://www.huawei.com/bss/soaif/interface/common/}BillMedium" maxOccurs="unbounded" minOccurs="0"/>
		 *         &lt;element name="Currency" type="{http://www.huawei.com/bss/soaif/interface/common/}Currency" minOccurs="0"/>
		 *         &lt;element name="InitialBalance" type="{http://www.huawei.com/bss/soaif/interface/common/}Amount" minOccurs="0"/>
		 *         &lt;element name="CreditLimit" type="{http://www.huawei.com/bss/soaif/interface/common/}CreditLimit" maxOccurs="unbounded" minOccurs="0"/>
		 *         &lt;element name="AcctPayMethod" type="{http://www.huawei.com/bss/soaif/interface/common/}AcctPayMethod" minOccurs="0"/>
		 *         &lt;element name="PaymentChannel" type="{http://www.huawei.com/bss/soaif/interface/common/}PaymentChannel" maxOccurs="unbounded" minOccurs="0"/>
		 *         &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "custId", "acctName", "paymentType", "title", "name", "billCycleType",
				"billLanguage", "contact", "address", "billMedium", "currency", "initialBalance", "creditLimit",
				"acctPayMethod", "paymentChannel", "additionalProperty" })
		public static class Account {
			@XmlElement(name = "CustId", required = true)
			protected String custId;
			@XmlElement(name = "AcctName")
			protected String acctName;
			@XmlElement(name = "PaymentType")
			protected String paymentType;
			@XmlElementRef(name = "Title", namespace = "http://www.huawei.com/bss/soaif/interface/SubscriberService/", type = JAXBElement.class, required = false)
			protected JAXBElement<String> title;
			@XmlElement(name = "Name")
			protected Name name;
			@XmlElement(name = "BillCycleType")
			protected String billCycleType;
			@XmlElement(name = "BillLanguage")
			protected String billLanguage;
			@XmlElement(name = "Contact")
			protected com.huawei.bss.soaif._interface.common.Contact contact;
			@XmlElement(name = "Address")
			protected List<com.huawei.bss.soaif._interface.common.Address> address;
			@XmlElement(name = "BillMedium")
			protected List<BillMedium> billMedium;
			@XmlElement(name = "Currency")
			protected BigInteger currency;
			@XmlElement(name = "InitialBalance")
			protected String initialBalance;
			@XmlElement(name = "CreditLimit")
			protected List<CreditLimit> creditLimit;
			@XmlElement(name = "AcctPayMethod")
			protected String acctPayMethod;
			@XmlElement(name = "PaymentChannel")
			protected List<PaymentChannel> paymentChannel;
			@XmlElement(name = "AdditionalProperty")
			protected List<SimpleProperty> additionalProperty;

			/**
			 * Gets the value of the custId property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getCustId() {
				return custId;
			}

			/**
			 * Sets the value of the custId property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setCustId(String value) {
				this.custId = value;
			}

			/**
			 * Gets the value of the acctName property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getAcctName() {
				return acctName;
			}

			/**
			 * Sets the value of the acctName property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setAcctName(String value) {
				this.acctName = value;
			}

			/**
			 * Gets the value of the paymentType property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getPaymentType() {
				return paymentType;
			}

			/**
			 * Sets the value of the paymentType property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setPaymentType(String value) {
				this.paymentType = value;
			}

			/**
			 * Gets the value of the title property.
			 * 
			 * @return possible object is {@link JAXBElement
			 *         }{@code <}{@link String }{@code >}
			 * 
			 */
			public JAXBElement<String> getTitle() {
				return title;
			}

			/**
			 * Sets the value of the title property.
			 * 
			 * @param value
			 *            allowed object is {@link JAXBElement
			 *            }{@code <}{@link String }{@code >}
			 * 
			 */
			public void setTitle(JAXBElement<String> value) {
				this.title = value;
			}

			/**
			 * Gets the value of the name property.
			 * 
			 * @return possible object is {@link Name }
			 * 
			 */
			public Name getName() {
				return name;
			}

			/**
			 * Sets the value of the name property.
			 * 
			 * @param value
			 *            allowed object is {@link Name }
			 * 
			 */
			public void setName(Name value) {
				this.name = value;
			}

			/**
			 * Gets the value of the billCycleType property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getBillCycleType() {
				return billCycleType;
			}

			/**
			 * Sets the value of the billCycleType property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setBillCycleType(String value) {
				this.billCycleType = value;
			}

			/**
			 * Gets the value of the billLanguage property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getBillLanguage() {
				return billLanguage;
			}

			/**
			 * Sets the value of the billLanguage property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setBillLanguage(String value) {
				this.billLanguage = value;
			}

			/**
			 * Gets the value of the contact property.
			 * 
			 * @return possible object is
			 *         {@link com.huawei.bss.soaif._interface.common.Contact }
			 * 
			 */
			public com.huawei.bss.soaif._interface.common.Contact getContact() {
				return contact;
			}

			/**
			 * Sets the value of the contact property.
			 * 
			 * @param value
			 *            allowed object is
			 *            {@link com.huawei.bss.soaif._interface.common.Contact }
			 * 
			 */
			public void setContact(com.huawei.bss.soaif._interface.common.Contact value) {
				this.contact = value;
			}

			/**
			 * Gets the value of the address property.
			 * 
			 * <p>
			 * This accessor method returns a reference to the live list, not a
			 * snapshot. Therefore any modification you make to the returned
			 * list will be present inside the JAXB object. This is why there is
			 * not a <CODE>set</CODE> method for the address property.
			 * 
			 * <p>
			 * For example, to add a new item, do as follows:
			 * 
			 * <pre>
			 * getAddress().add(newItem);
			 * </pre>
			 * 
			 * 
			 * <p>
			 * Objects of the following type(s) are allowed in the list
			 * {@link com.huawei.bss.soaif._interface.common.Address }
			 * 
			 * 
			 */
			public List<com.huawei.bss.soaif._interface.common.Address> getAddress() {
				if (address == null) {
					address = new ArrayList<com.huawei.bss.soaif._interface.common.Address>();
				}
				return this.address;
			}

			/**
			 * Gets the value of the billMedium property.
			 * 
			 * <p>
			 * This accessor method returns a reference to the live list, not a
			 * snapshot. Therefore any modification you make to the returned
			 * list will be present inside the JAXB object. This is why there is
			 * not a <CODE>set</CODE> method for the billMedium property.
			 * 
			 * <p>
			 * For example, to add a new item, do as follows:
			 * 
			 * <pre>
			 * getBillMedium().add(newItem);
			 * </pre>
			 * 
			 * 
			 * <p>
			 * Objects of the following type(s) are allowed in the list
			 * {@link BillMedium }
			 * 
			 * 
			 */
			public List<BillMedium> getBillMedium() {
				if (billMedium == null) {
					billMedium = new ArrayList<BillMedium>();
				}
				return this.billMedium;
			}

			/**
			 * Gets the value of the currency property.
			 * 
			 * @return possible object is {@link BigInteger }
			 * 
			 */
			public BigInteger getCurrency() {
				return currency;
			}

			/**
			 * Sets the value of the currency property.
			 * 
			 * @param value
			 *            allowed object is {@link BigInteger }
			 * 
			 */
			public void setCurrency(BigInteger value) {
				this.currency = value;
			}

			/**
			 * Gets the value of the initialBalance property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getInitialBalance() {
				return initialBalance;
			}

			/**
			 * Sets the value of the initialBalance property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setInitialBalance(String value) {
				this.initialBalance = value;
			}

			/**
			 * Gets the value of the creditLimit property.
			 * 
			 * <p>
			 * This accessor method returns a reference to the live list, not a
			 * snapshot. Therefore any modification you make to the returned
			 * list will be present inside the JAXB object. This is why there is
			 * not a <CODE>set</CODE> method for the creditLimit property.
			 * 
			 * <p>
			 * For example, to add a new item, do as follows:
			 * 
			 * <pre>
			 * getCreditLimit().add(newItem);
			 * </pre>
			 * 
			 * 
			 * <p>
			 * Objects of the following type(s) are allowed in the list
			 * {@link CreditLimit }
			 * 
			 * 
			 */
			public List<CreditLimit> getCreditLimit() {
				if (creditLimit == null) {
					creditLimit = new ArrayList<CreditLimit>();
				}
				return this.creditLimit;
			}

			/**
			 * Gets the value of the acctPayMethod property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getAcctPayMethod() {
				return acctPayMethod;
			}

			/**
			 * Sets the value of the acctPayMethod property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setAcctPayMethod(String value) {
				this.acctPayMethod = value;
			}

			/**
			 * Gets the value of the paymentChannel property.
			 * 
			 * <p>
			 * This accessor method returns a reference to the live list, not a
			 * snapshot. Therefore any modification you make to the returned
			 * list will be present inside the JAXB object. This is why there is
			 * not a <CODE>set</CODE> method for the paymentChannel property.
			 * 
			 * <p>
			 * For example, to add a new item, do as follows:
			 * 
			 * <pre>
			 * getPaymentChannel().add(newItem);
			 * </pre>
			 * 
			 * 
			 * <p>
			 * Objects of the following type(s) are allowed in the list
			 * {@link PaymentChannel }
			 * 
			 * 
			 */
			public List<PaymentChannel> getPaymentChannel() {
				if (paymentChannel == null) {
					paymentChannel = new ArrayList<PaymentChannel>();
				}
				return this.paymentChannel;
			}

			/**
			 * Gets the value of the additionalProperty property.
			 * 
			 * <p>
			 * This accessor method returns a reference to the live list, not a
			 * snapshot. Therefore any modification you make to the returned
			 * list will be present inside the JAXB object. This is why there is
			 * not a <CODE>set</CODE> method for the additionalProperty
			 * property.
			 * 
			 * <p>
			 * For example, to add a new item, do as follows:
			 * 
			 * <pre>
			 * getAdditionalProperty().add(newItem);
			 * </pre>
			 * 
			 * 
			 * <p>
			 * Objects of the following type(s) are allowed in the list
			 * {@link SimpleProperty }
			 * 
			 * 
			 */
			public List<SimpleProperty> getAdditionalProperty() {
				if (additionalProperty == null) {
					additionalProperty = new ArrayList<SimpleProperty>();
				}
				return this.additionalProperty;
			}
		}
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="CustId" type="{http://www.huawei.com/bss/soaif/interface/common/}CustId"/>
	 *         &lt;element name="Title" type="{http://www.huawei.com/bss/soaif/interface/common/}Title" minOccurs="0"/>
	 *         &lt;element name="Name" type="{http://www.huawei.com/bss/soaif/interface/common/}Name" minOccurs="0"/>
	 *         &lt;element name="Nationality" type="{http://www.huawei.com/bss/soaif/interface/common/}Nationality" minOccurs="0"/>
	 *         &lt;element name="Birthday" type="{http://www.huawei.com/bss/soaif/interface/common/}Date" minOccurs="0"/>
	 *         &lt;element name="Contact" maxOccurs="unbounded" minOccurs="0">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}Contact">
	 *                 &lt;sequence>
	 *                   &lt;element name="ActionType" type="{http://www.huawei.com/bss/soaif/interface/common/}ActionType"/>
	 *                 &lt;/sequence>
	 *               &lt;/extension>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="Address" maxOccurs="unbounded" minOccurs="0">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}Address">
	 *                 &lt;sequence>
	 *                   &lt;element name="ActionType" type="{http://www.huawei.com/bss/soaif/interface/common/}ActionType"/>
	 *                 &lt;/sequence>
	 *               &lt;/extension>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "custId", "title", "name", "nationality", "birthday", "contact", "address",
			"additionalProperty" })
	public static class PostpaidCust {
		@XmlElement(name = "CustId", required = true)
		protected String custId;
		@XmlElementRef(name = "Title", namespace = "http://www.huawei.com/bss/soaif/interface/SubscriberService/", type = JAXBElement.class, required = false)
		protected JAXBElement<String> title;
		@XmlElement(name = "Name")
		protected Name name;
		@XmlElementRef(name = "Nationality", namespace = "http://www.huawei.com/bss/soaif/interface/SubscriberService/", type = JAXBElement.class, required = false)
		protected JAXBElement<String> nationality;
		@XmlElement(name = "Birthday")
		protected String birthday;
		@XmlElement(name = "Contact")
		protected List<ChangePrepaidToPostpaidReqMsg.PostpaidCust.Contact> contact;
		@XmlElement(name = "Address")
		protected List<ChangePrepaidToPostpaidReqMsg.PostpaidCust.Address> address;
		@XmlElement(name = "AdditionalProperty")
		protected List<SimpleProperty> additionalProperty;

		/**
		 * Gets the value of the custId property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getCustId() {
			return custId;
		}

		/**
		 * Sets the value of the custId property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setCustId(String value) {
			this.custId = value;
		}

		/**
		 * Gets the value of the title property.
		 * 
		 * @return possible object is {@link JAXBElement }{@code <}{@link String
		 *         }{@code >}
		 * 
		 */
		public JAXBElement<String> getTitle() {
			return title;
		}

		/**
		 * Sets the value of the title property.
		 * 
		 * @param value
		 *            allowed object is {@link JAXBElement
		 *            }{@code <}{@link String }{@code >}
		 * 
		 */
		public void setTitle(JAXBElement<String> value) {
			this.title = value;
		}

		/**
		 * Gets the value of the name property.
		 * 
		 * @return possible object is {@link Name }
		 * 
		 */
		public Name getName() {
			return name;
		}

		/**
		 * Sets the value of the name property.
		 * 
		 * @param value
		 *            allowed object is {@link Name }
		 * 
		 */
		public void setName(Name value) {
			this.name = value;
		}

		/**
		 * Gets the value of the nationality property.
		 * 
		 * @return possible object is {@link JAXBElement }{@code <}{@link String
		 *         }{@code >}
		 * 
		 */
		public JAXBElement<String> getNationality() {
			return nationality;
		}

		/**
		 * Sets the value of the nationality property.
		 * 
		 * @param value
		 *            allowed object is {@link JAXBElement
		 *            }{@code <}{@link String }{@code >}
		 * 
		 */
		public void setNationality(JAXBElement<String> value) {
			this.nationality = value;
		}

		/**
		 * Gets the value of the birthday property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getBirthday() {
			return birthday;
		}

		/**
		 * Sets the value of the birthday property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setBirthday(String value) {
			this.birthday = value;
		}

		/**
		 * Gets the value of the contact property.
		 * 
		 * <p>
		 * This accessor method returns a reference to the live list, not a
		 * snapshot. Therefore any modification you make to the returned list
		 * will be present inside the JAXB object. This is why there is not a
		 * <CODE>set</CODE> method for the contact property.
		 * 
		 * <p>
		 * For example, to add a new item, do as follows:
		 * 
		 * <pre>
		 * getContact().add(newItem);
		 * </pre>
		 * 
		 * 
		 * <p>
		 * Objects of the following type(s) are allowed in the list
		 * {@link ChangePrepaidToPostpaidReqMsg.PostpaidCust.Contact }
		 * 
		 * 
		 */
		public List<ChangePrepaidToPostpaidReqMsg.PostpaidCust.Contact> getContact() {
			if (contact == null) {
				contact = new ArrayList<ChangePrepaidToPostpaidReqMsg.PostpaidCust.Contact>();
			}
			return this.contact;
		}

		/**
		 * Gets the value of the address property.
		 * 
		 * <p>
		 * This accessor method returns a reference to the live list, not a
		 * snapshot. Therefore any modification you make to the returned list
		 * will be present inside the JAXB object. This is why there is not a
		 * <CODE>set</CODE> method for the address property.
		 * 
		 * <p>
		 * For example, to add a new item, do as follows:
		 * 
		 * <pre>
		 * getAddress().add(newItem);
		 * </pre>
		 * 
		 * 
		 * <p>
		 * Objects of the following type(s) are allowed in the list
		 * {@link ChangePrepaidToPostpaidReqMsg.PostpaidCust.Address }
		 * 
		 * 
		 */
		public List<ChangePrepaidToPostpaidReqMsg.PostpaidCust.Address> getAddress() {
			if (address == null) {
				address = new ArrayList<ChangePrepaidToPostpaidReqMsg.PostpaidCust.Address>();
			}
			return this.address;
		}

		/**
		 * Gets the value of the additionalProperty property.
		 * 
		 * <p>
		 * This accessor method returns a reference to the live list, not a
		 * snapshot. Therefore any modification you make to the returned list
		 * will be present inside the JAXB object. This is why there is not a
		 * <CODE>set</CODE> method for the additionalProperty property.
		 * 
		 * <p>
		 * For example, to add a new item, do as follows:
		 * 
		 * <pre>
		 * getAdditionalProperty().add(newItem);
		 * </pre>
		 * 
		 * 
		 * <p>
		 * Objects of the following type(s) are allowed in the list
		 * {@link SimpleProperty }
		 * 
		 * 
		 */
		public List<SimpleProperty> getAdditionalProperty() {
			if (additionalProperty == null) {
				additionalProperty = new ArrayList<SimpleProperty>();
			}
			return this.additionalProperty;
		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * 
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}Address">
		 *       &lt;sequence>
		 *         &lt;element name="ActionType" type="{http://www.huawei.com/bss/soaif/interface/common/}ActionType"/>
		 *       &lt;/sequence>
		 *     &lt;/extension>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "actionType" })
		public static class Address extends com.huawei.bss.soaif._interface.common.Address {
			@XmlElement(name = "ActionType", required = true)
			protected String actionType;

			/**
			 * Gets the value of the actionType property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getActionType() {
				return actionType;
			}

			/**
			 * Sets the value of the actionType property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setActionType(String value) {
				this.actionType = value;
			}
		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * 
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}Contact">
		 *       &lt;sequence>
		 *         &lt;element name="ActionType" type="{http://www.huawei.com/bss/soaif/interface/common/}ActionType"/>
		 *       &lt;/sequence>
		 *     &lt;/extension>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "actionType" })
		public static class Contact extends com.huawei.bss.soaif._interface.common.Contact {
			@XmlElement(name = "ActionType", required = true)
			protected String actionType;

			/**
			 * Gets the value of the actionType property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getActionType() {
				return actionType;
			}

			/**
			 * Sets the value of the actionType property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setActionType(String value) {
				this.actionType = value;
			}
		}
	}
}
