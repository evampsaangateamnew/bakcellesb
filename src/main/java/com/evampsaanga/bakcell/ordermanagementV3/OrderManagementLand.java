package com.evampsaanga.bakcell.ordermanagementV3;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.transaction.annotation.Transactional;

import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.amqimplementationsesb.LogsType;
import com.evampsaanga.bakcell.actionhistory.ActionHistoryRequest;
import com.evampsaanga.bakcell.actionhistory.ActionHistoryResponse;
import com.evampsaanga.bakcell.actionhistory.ActionHistoryResponseData;
import com.evampsaanga.bakcell.callforwarding.CallForwardRequestV2;
import com.evampsaanga.bakcell.callforwarding.CallForwardResponse;
import com.evampsaanga.bakcell.callforwarding.CallForwardingRequestLand;
import com.evampsaanga.bakcell.callforwarding.ProcessCoreServicesRequestV2;
import com.evampsaanga.bakcell.changesuplimentry.ChangeSupplimentryOfferingRequestLand;
import com.evampsaanga.bakcell.changesuplimentry.RequestBulk;
import com.evampsaanga.bakcell.changesuplimentry.RequestBulkData;
import com.evampsaanga.bakcell.changesuplimentry.RequestLandBulk;
import com.evampsaanga.bakcell.changesuplimentry.Response;
import com.evampsaanga.bakcell.db.DBFactory;
import com.evampsaanga.bakcell.freesms.SendSMSRequest;
import com.evampsaanga.bakcell.freesms.SendSMSRequestLand;
import com.evampsaanga.bakcell.responseheaders.BaseResponse;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.ThirdPartyNames;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @author Aqeel Abbas Creates and handles bulk order operations
 */

@Path("/bakcell/")
public class OrderManagementLand {
	public static final Logger loggerV2 = Logger.getLogger("bakcellLogs-order");

	/**
	 * Insert the orders according to the type specified in the parameters
	 * 
	 * @param credential
	 * @param contentType
	 * @param requestBody
	 * @return Returns a message indicating if the insertion of the order is
	 *         successful or not
	 * @throws SQLException
	 * @throws Exception
	 */
	@POST
	@Path("/insertorders")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Transactional(rollbackFor = Exception.class)
	public OrderManagementResponse insertOrder(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) throws SQLException, Exception {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.MANAGE_ORDER);
		logs.setThirdPartyName(ThirdPartyNames.MANAGE_ORDER);
		logs.setTableType(LogsType.ManageOrder);
		loggerV2.info("Request Landed on insertOrder: " + requestBody);
		OrderManagementResponse resp = new OrderManagementResponse();

		JSONObject jsonObject = new JSONObject(requestBody);
		try {
			if (jsonObject.getString("orderKey").toString().equals(Constants.BROADCAST_SMS)) {
				resp = insertBroadcastSms(jsonObject, credential, logs);
				loggerV2.info(jsonObject.getString("msisdn").toString() + " - Response is: " + resp.toString());
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			} else if (jsonObject.getString("orderKey").toString().equals(Constants.CORE_SERVICES)) {
				resp = insertCoreServices(jsonObject, credential, logs);
				loggerV2.info(jsonObject.getString("msisdn").toString() + " - Response is: " + resp.toString());
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			} else if (jsonObject.getString("orderKey").toString().equals(Constants.CHANGE_SUPPLEMENTARY)) {
				resp = insertChangeSupplementary(jsonObject, credential, logs);
				loggerV2.info(jsonObject.getString("msisdn").toString() + " - Response is: " + resp.toString());
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			} else if (jsonObject.getString("orderKey").toString().equals(Constants.RETRY_FAILED)) {
				resp = RetryFailed(jsonObject, credential, logs);
				loggerV2.info(jsonObject.getString("msisdn").toString() + " - Response is: " + resp.toString());
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			} else if (jsonObject.getString("orderKey").toString().equals(Constants.CANCEL_PENDING)) {
				resp = cancelPending(jsonObject, credential, logs);
				loggerV2.info(jsonObject.getString("msisdn").toString() + " - Response is: " + resp.toString());
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		} catch (Exception e) {
			loggerV2.error(Helper.GetException(e));
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
		}
		return resp;
	}

	/**
	 * Cancels all pending or new orders
	 * 
	 * @param jsonObject
	 * @param credential
	 * @param logs
	 * @return Returns a string indicating if the cancellation is successful or not
	 */
	private OrderManagementResponse cancelPending(JSONObject jsonObject, String credential, Logs logs) {
		String credentials = null;
		OrderManagementResponse resp = new OrderManagementResponse();
//		java.sql.Connection conn = DBFactory.getDbConnection();
		try (java.sql.Connection conn = DBFactory.getDbConnectionFromPool();){
			credentials = Decrypter.getInstance().decrypt(credential);
			if (credentials == null) {
				loggerV2.info(jsonObject.getString("msisdn").toString() + " - Credentials are Null");
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				resp.setResponseMsg("Credentials are not correct");
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			} else if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {
					loggerV2.info(jsonObject.getString("msisdn").toString() + " - Updating Order Details to Cancel");
					boolean falg = updateOrderDetailsToCancel(jsonObject.getString("orderId"), "U", conn);
					if (falg) {
						loggerV2.info(jsonObject.getString("msisdn").toString() + " - Updating Order Status");
						falg = updateOrderStatus(jsonObject.getString("orderId"), "C", conn);
						// updateRetryCount(jsonObject.getString("orderId"),
						// conn);
						if (falg) {
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							resp.setResponseMsg("Your Request is under Process Now");
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						} else {
							resp.setReturnCode(ResponseCodes.UNSUCCESS_CODE_CANCEL);
							resp.setReturnMsg(ResponseCodes.UNSUCCESS_CANCEL_DESC);
							resp.setResponseMsg("Requested Order Is Not In Pending State");
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} else {
						loggerV2.info(jsonObject.getString("msisdn").toString() + " - Order is Not in Pending Status");
						resp.setReturnCode(ResponseCodes.UNSUCCESS_CODE_CANCEL);
						resp.setReturnMsg(ResponseCodes.UNSUCCESS_CANCEL_DESC);
						resp.setResponseMsg("Requested Order Is Not In Pending State");
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} catch (Exception e) {
					loggerV2.error(Helper.GetException(e));
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(e.getMessage());
					resp.setResponseMsg(e.getMessage());
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception e) {
			loggerV2.error(Helper.GetException(e));
			resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
			resp.setReturnMsg(e.getMessage());
			resp.setResponseMsg(e.getMessage());
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		return resp;
	}

	/**
	 * Updates order_details table status against msisdn to cancel the order
	 * 
	 * @param orderId
	 * @param Stauts
	 * @param conn
	 * @return Returns a flag. True in case of success, false in case of failure
	 * @throws SQLException
	 */
	private boolean updateOrderDetailsToCancel(String orderId, String Stauts, Connection conn) throws SQLException {
		boolean flag = true;
		// boolean retFlag = false;
		int count = 0;
		while (flag == true) {
			String query = "select msisdn, status from order_details where order_id_fk =? and status in ('P','N') order by created_at limit 1";
			PreparedStatement preparedStatement = conn.prepareStatement(query);
			preparedStatement.setString(1, orderId);
			ResultSet rSet = preparedStatement.executeQuery();
			if (!rSet.isBeforeFirst()) {
				flag = false;
			}
			loggerV2.info("Cancellation flag is:" + flag);

			while (rSet.next()) {
				loggerV2.info("Canceling order against MSISDN: " + rSet.getString(1) + " Status:" + rSet.getString(2));
				query = "update order_details set status =? where order_id_fk =? and msisdn =? and status =?";
				preparedStatement = conn.prepareStatement(query);
				preparedStatement.setString(1, Stauts);
				preparedStatement.setString(2, orderId);
				preparedStatement.setString(3, rSet.getString(1));
				preparedStatement.setString(4, rSet.getString(2));
				loggerV2.info("Query: " + preparedStatement.toString());
				preparedStatement.execute();
				count += preparedStatement.getUpdateCount();
			}
			rSet.close();
			preparedStatement.close();
		}
		if (count > 0)
			return true;
		else
			return false;
	}

	/**
	 * Retries the failed orders
	 * 
	 * @param jsonObject
	 * @param credential
	 * @param logs
	 * @return Returns a string indicating if the operation is successful or not
	 */
	private OrderManagementResponse RetryFailed(JSONObject jsonObject, String credential, Logs logs) {
		String credentials = null;
		OrderManagementResponse resp = new OrderManagementResponse();
//		java.sql.Connection conn = DBFactory.getDbConnection();
		try (java.sql.Connection conn = DBFactory.getDbConnectionFromPool();) {
			credentials = Decrypter.getInstance().decrypt(credential);
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				resp.setResponseMsg("Credentials are not correct");
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			} else if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				// jsonObject.put("type", "Retry Failed Order");
				try {
					boolean flag = checkFailedCount(jsonObject.getString("orderId"), conn);
					loggerV2.info("Failed Order Found: " + flag);
					if (flag) {
						flag = updateOrderStatus(jsonObject.getString("orderId"), "N", conn);
						loggerV2.info("Main Table Updated:" + flag);
						if (flag) {
							updateRetryCount(jsonObject.getString("orderId"), conn);
							updateOrderDetails(jsonObject.getString("orderId"), "", "P", "", "", conn);
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							resp.setResponseMsg("Your Request is under Process Now");
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						} else {
							loggerV2.info("No Orders Found");
							resp.setReturnCode(ResponseCodes.UNSUCCESS_CODE_CANCEL);
							resp.setReturnMsg(ResponseCodes.UNSUCCESS_CANCEL_DESC);
							resp.setResponseMsg("Reuqest Order Doesn't Exist!");
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} else {
						resp.setReturnCode(ResponseCodes.NO_FAILED_CODE);
						resp.setReturnMsg(ResponseCodes.NO_FAILED_DES);
						resp.setResponseMsg("No Failed Orders Found!");
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} catch (Exception e) {
					loggerV2.error(Helper.GetException(e));
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(e.getMessage());
					resp.setResponseMsg(e.getMessage());
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception e) {
			loggerV2.error(Helper.GetException(e));
			resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
			resp.setReturnMsg(e.getMessage());
			resp.setResponseMsg(e.getMessage());
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		return resp;
	}

	/**
	 * Checks if there are any failed orders against order id
	 * 
	 * @param order_id
	 * @param conn
	 * @return Returns true if any failed orders found else false
	 * @throws SQLException
	 */
	private boolean checkFailedCount(String order_id, Connection conn) throws SQLException {
		String query = "select count(*) from order_details where order_id_fk =? and status ='F'";
		PreparedStatement preparedStmt = conn.prepareStatement(query);
		preparedStmt = conn.prepareStatement(query);
		preparedStmt.setString(1, order_id);
		loggerV2.info("QUERY for update Order_purchase " + preparedStmt);
		ResultSet rSet = preparedStmt.executeQuery();
		int count = 0;
		while (rSet.next()) {
			count = rSet.getInt(1);
		}
		if (count > 0) {
			rSet.close();
			preparedStmt.close();
			return true;
		} else {
			rSet.close();
			preparedStmt.close();
			return false;
		}
	}

	/**
	 * Updates retry count in the purchase_order table
	 * 
	 * @param order_id
	 * @param conn
	 * @throws SQLException
	 */
	private void updateRetryCount(String order_id, Connection conn) throws SQLException {
		String query = "select retry_count from purchase_order where order_id=?";
		PreparedStatement preparedStmt = conn.prepareStatement(query);
		preparedStmt = conn.prepareStatement(query);
		preparedStmt.setString(1, order_id);
		loggerV2.info("QUERY for update Order_purchase " + preparedStmt);
		ResultSet rSet = preparedStmt.executeQuery();
		int retryCount = 0;
		while (rSet.next()) {
			retryCount = rSet.getInt(1);
		}
		retryCount++;
		query = "update purchase_order set retry_count = ? where order_id= ?";
		preparedStmt = conn.prepareStatement(query);
		preparedStmt = conn.prepareStatement(query);
		preparedStmt.setInt(1, retryCount);
		preparedStmt.setString(2, order_id);
		loggerV2.info("QUERY for update Order_purchase " + preparedStmt);
		preparedStmt.execute();
		rSet.close();
		preparedStmt.close();
	}

	/**
	 * Inserts Data into order management related tables to prepare data for the
	 * cron job of change supplementary
	 * 
	 * @param jsonObject
	 * @param credential
	 * @param logs
	 * @return Returns a string indicating if the operation is successful or not
	 */
	private OrderManagementResponse insertChangeSupplementary(JSONObject jsonObject, String credential, Logs logs) {
		String credentials = null;
		OrderManagementResponse resp = new OrderManagementResponse();
//		java.sql.Connection conn = DBFactory.getDbConnection();
		try (java.sql.Connection conn = DBFactory.getDbConnectionFromPool();) {
			credentials = Decrypter.getInstance().decrypt(credential);
			if (credentials == null) {
				loggerV2.info("Credentials are Null");
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				resp.setResponseMsg("Credentials are not correct");
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			} else if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				jsonObject.put("type", "Change Supplementary Offerings");
				try {
					// inserting data into purchase order table
					String uuid = insertPurchaseOrder(jsonObject, conn);
					// inserting data into order attributes
					try {
						addOrderDetails(jsonObject, uuid, conn, jsonObject.get("type").toString());
						loggerV2.info("Addition of details is done");
						try {
							addOrderttributes(jsonObject, uuid, conn);
							loggerV2.info(" Addition of attributes is done");
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							resp.setResponseMsg("Your Request is under Process Now");
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						} catch (Exception e) {
							loggerV2.error(Helper.GetException(e));
							resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
							resp.setReturnMsg(e.getMessage());
							resp.setResponseMsg(e.getMessage());
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception e) {
						loggerV2.error(Helper.GetException(e));
						resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
						resp.setReturnMsg(e.getMessage());
						resp.setResponseMsg(e.getMessage());
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} catch (Exception e) {
					loggerV2.error(Helper.GetException(e));
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(e.getMessage());
					resp.setResponseMsg(e.getMessage());
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception e) {
			loggerV2.error(Helper.GetException(e));
			resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
			resp.setReturnMsg(e.getMessage());
			resp.setResponseMsg(e.getMessage());
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		return resp;
	}

	/**
	 * Inserts Data into order management related tables to prepare data for the
	 * cron job of core services
	 * 
	 * @param jsonObject
	 * @param credential
	 * @param logs
	 * @return Returns a string indicating if the operation is successful or not
	 */
	private OrderManagementResponse insertCoreServices(JSONObject jsonObject, String credential, Logs logs) {
		String credentials = null;
		OrderManagementResponse resp = new OrderManagementResponse();
//		java.sql.Connection conn = DBFactory.getDbConnection();
		try(java.sql.Connection conn = DBFactory.getDbConnectionFromPool();) {
			credentials = Decrypter.getInstance().decrypt(credential);
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				resp.setResponseMsg("Credentials are not correct");
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			} else if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				jsonObject.put("type", "Process Core Services");
				try {
					// inserting data into purchase order table
					String uuid = insertPurchaseOrder(jsonObject, conn);
					// inserting data into order attributes
					try {
						addOrderDetails(jsonObject, uuid, conn, jsonObject.get("type").toString());
						loggerV2.info("Addition of details is done");
						try {
							addOrderttributes(jsonObject, uuid, conn);
							loggerV2.info("Addition of attributes is done");
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							resp.setResponseMsg("Your Request is under Process Now");
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						} catch (Exception e) {
							loggerV2.error(Helper.GetException(e));
							resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
							resp.setReturnMsg(e.getMessage());
							resp.setResponseMsg(e.getMessage());
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception e) {
						loggerV2.error(Helper.GetException(e));
						resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
						resp.setReturnMsg(e.getMessage());
						resp.setResponseMsg(e.getMessage());
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} catch (Exception e) {
					loggerV2.error(Helper.GetException(e));
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(e.getMessage());
					resp.setResponseMsg(e.getMessage());
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception e) {
			loggerV2.error(Helper.GetException(e));
			resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
			resp.setReturnMsg(e.getMessage());
			resp.setResponseMsg(e.getMessage());
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		return resp;
	}

	/**
	 * Inserts Data into order management related tables to prepare data for the
	 * cron job of broadcast sms
	 * 
	 * @param jsonObject
	 * @param credential
	 * @param logs
	 * @return Returns a string indicating if the operation is successful or not
	 */
	private OrderManagementResponse insertBroadcastSms(JSONObject jsonObject, String credential, Logs logs) {
		String credentials = null;
		OrderManagementResponse resp = new OrderManagementResponse();
//		java.sql.Connection conn = DBFactory.getDbConnection();
		try (java.sql.Connection conn = DBFactory.getDbConnectionFromPool();) {
			credentials = Decrypter.getInstance().decrypt(credential);
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				resp.setResponseMsg("Credentials are not correct");
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			} else if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				jsonObject.put("type", "BroadcastSMS");
				try {
					// inserting data into purchase order table
					if (jsonObject.getString("msisdn").toString().equals("")) {
						resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
						resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
					String uuid = insertPurchaseOrder(jsonObject, conn);
					// inserting data into order attributes
					try {
						addOrderDetails(jsonObject, uuid, conn, jsonObject.get("type").toString());

						try {
							addOrderttributes(jsonObject, uuid, conn);
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							resp.setResponseMsg("Your Request is under Process Now");
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						} catch (Exception e) {
							loggerV2.error(Helper.GetException(e));
							resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
							resp.setReturnMsg(e.getMessage());
							resp.setResponseMsg(e.getMessage());
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception e) {
						loggerV2.error(Helper.GetException(e));
						resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
						resp.setReturnMsg(e.getMessage());
						resp.setResponseMsg(e.getMessage());
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} catch (Exception e) {
					loggerV2.error(Helper.GetException(e));
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(e.getMessage());
					resp.setResponseMsg(e.getMessage());
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception e) {
			loggerV2.error(Helper.GetException(e));
			resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
			resp.setReturnMsg(e.getMessage());
			resp.setResponseMsg(e.getMessage());
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		return resp;
	}

	/**
	 * Helper Method to insert order details
	 * 
	 * @param jsonObject
	 * @param uuid
	 * @param conn
	 * @param type
	 * @throws IOException
	 * @throws Exception
	 */
	@Transactional(rollbackFor = Exception.class)
	void addOrderDetails(JSONObject jsonObject, String uuid, Connection conn, String type)
			throws IOException, Exception {
		loggerV2.info("Type is : " + type);
		loggerV2.info("JSON is " + jsonObject);
		int length = 0;
		// list to be used for broadcast sms
		List<String> idStrings = new ArrayList<>();
		// list to be used for core services
		List<String> userList = new ArrayList<>();
		// list to be used for change supplementary
		ArrayList<RequestBulkData> usersList = new ArrayList<>();

		if (type.equals("BroadcastSMS")) {
			idStrings = Arrays.asList(jsonObject.getString("recieverMsisdn").split(","));
			length = idStrings.size();
		}
		if (type.equals("Process Core Services")) {
			CallForwardRequestV2 users = Helper.JsonToObject(jsonObject.toString(), CallForwardRequestV2.class);
			userList = Arrays.asList(users.getUsers().split(","));
			length = userList.size();
		}
		if (type.equals("Change Supplementary Offerings")) {
			RequestBulk users = Helper.JsonToObject(jsonObject.toString(), RequestBulk.class);
			usersList = users.getUsers();
			loggerV2.info("User List :" + new ObjectMapper().writeValueAsString(usersList));
			length = usersList.size();
			// logger.info(users.toString() + "\n************* Loop length is:
			// *************" + userList.size());
		}
		if (type.equals("Retry Failed Order")) {
			length = 1;
		}

		for (int i = 0; i < length; i++) {

			String queryorderdetails = "insert into order_details (order_id_fk, msisdn,status,created_at,group_type)"
					+ " values (?, ?, ?,?,?)";
			PreparedStatement preparedStmtOrderDetails = conn.prepareStatement(queryorderdetails);
			preparedStmtOrderDetails.setString(1, uuid);
			if (type.equals("BroadcastSMS")) {
				preparedStmtOrderDetails.setString(2, idStrings.get(i));
				preparedStmtOrderDetails.setString(5, "");
			} else if (type.equals("Process Core Services")) {
				preparedStmtOrderDetails.setString(2, userList.get(i));
				preparedStmtOrderDetails.setString(5, "");
			} else if (type.equals("Change Supplementary Offerings")) {
				preparedStmtOrderDetails.setString(2, usersList.get(i).getMsisdn());
				preparedStmtOrderDetails.setString(5, usersList.get(i).getGroupType());
			} else if (type.equals("Retry Failed Order")) {
				preparedStmtOrderDetails.setString(2, jsonObject.getString("msisdn").toString());
				preparedStmtOrderDetails.setString(5, "");
			}
			// TODO check the status
			preparedStmtOrderDetails.setString(3, "P");
			preparedStmtOrderDetails.setString(4, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			loggerV2.info("QUERY Order Details" + preparedStmtOrderDetails);

			preparedStmtOrderDetails.execute();
		}

	}

	/**
	 * helper method to insert data into main order table of order management
	 * 
	 * @param jsonObject
	 * @param conn
	 * @return
	 * @throws SQLException
	 * @throws JSONException
	 */
	@Transactional(rollbackFor = Exception.class)
	String insertPurchaseOrder(JSONObject jsonObject, Connection conn) throws SQLException, JSONException {
		String uuid = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
		String orderKey = GeneratedKey(jsonObject.getString("type"));

		String query = "insert into purchase_order (type, order_key,status,host,username,order_id)"
				+ " values (?, ?, ?, ?, ?, ?)";

		// create the mysql insert preparedstatement
		PreparedStatement preparedStmt = conn.prepareStatement(query);
		preparedStmt.setString(1, jsonObject.getString("type"));
		preparedStmt.setString(2, orderKey);
		preparedStmt.setString(3, "N");
		preparedStmt.setString(4, jsonObject.getString("iP"));
		preparedStmt.setString(5, jsonObject.getString("msisdn"));
		preparedStmt.setString(6, uuid);
		loggerV2.info("Insert Purchase Order Query" + preparedStmt);
		preparedStmt.execute();
		return uuid;
	}

	/**
	 * key generation
	 * 
	 * @param type
	 * @return Key Against the order type
	 */
	private String GeneratedKey(String type) {
		if (type.equals("BroadcastSMS"))
			return Constants.BROADCAST_SMS;
		else if (type.equals("Process Core Services"))
			return Constants.CORE_SERVICES;
		else if (type.equals("Change Supplementary Offerings"))
			return Constants.CHANGE_SUPPLEMENTARY;
		else if (type.equals("Retry Failed Order"))
			return Constants.RETRY_FAILED;
		return "";

	}

	/**
	 * helper function to insert order request attributes
	 * 
	 * @param jsonObject
	 * @param uuid
	 * @param conn
	 * @throws SQLException
	 * @throws JSONException
	 */
	@SuppressWarnings("unchecked")
	@Transactional(rollbackFor = Exception.class)
	void addOrderttributes(JSONObject jsonObject, String uuid, Connection conn) throws SQLException, JSONException {
		// int lenght = jsonObject.length();
		if (jsonObject.has("recieverMsisdn"))
			jsonObject.remove("recieverMsisdn");
		if (jsonObject.has("users"))
			jsonObject.remove("users");
		Iterator<String> iterator = jsonObject.keys();
		loggerV2.info("Adding attribute");
		String query = "insert into order_attributes (order_id_fk,key_value,value,created_at) values(?,?,?,?)";
		PreparedStatement preparedStmt = conn.prepareStatement(query);
		String key = "";
		while (iterator.hasNext()) {
			key = iterator.next();
			preparedStmt.setString(1, uuid);
			preparedStmt.setString(2, key);
			preparedStmt.setString(3, jsonObject.getString(key).toString());
			preparedStmt.setString(4, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			loggerV2.info("Adding Order Attributes Query" + preparedStmt.toString());
			preparedStmt.execute();
		}

	}

	/**
	 * mian function to process orders. this function is executed via cron
	 * 
	 * @throws SQLException
	 * @throws Exception
	 */
	@Transactional(rollbackFor = Exception.class)
	public void processOrder() throws SQLException, Exception {
		Logs logs = new Logs();
//		java.sql.Connection conn = DBFactory.getDbConnection();
		boolean resFlag = false;
		try (java.sql.Connection conn = DBFactory.getDbConnectionFromPool();) {
			Map<String, ArrayList<String>> map = getOrderType(conn);
			ArrayList<String> type = map.get("Type");
			ArrayList<String> order_id = map.get("Order_Id");
			for (int index = 0; index < type.size(); index++) {
				if (type.get(index).equals("BroadcastSMS")) {
					SendSMSRequest request = new SendSMSRequest();
					SendSMSRequestLand sendSMS = new SendSMSRequestLand();
					try {
						// updating order status from N to P
						updateOrderStatus(order_id.get(index), "P", conn);
						// selecting order attributes
						JSONObject jsonObject = getOrderAttributes(order_id.get(index), conn);
						loggerV2.info("Order Attributes: " + jsonObject);
						String[] idString = jsonObject.get("recieverMsisdn").toString().split(",");
						jsonObject.remove("recieverMsisdn");
						jsonObject.remove("type");
						jsonObject.remove("orderKey");
						for (int j = 0; j < idString.length; j++) {
							jsonObject.put("recieverMsisdn", idString[j]);
							request = Helper.JsonToObject(jsonObject.toString(), SendSMSRequest.class);
							resFlag = sendSMS.sendSMSBulk(request, logs);
							if (resFlag) {
								// updating order details
								updateOrderDetails(order_id.get(index), idString[j], "C", ResponseCodes.SUCESS_CODE_200,
										ResponseCodes.SUCESS_DES_200, conn);
							} else if (!resFlag) {
								// updating order details
								updateOrderDetails(order_id.get(index), idString[j], "F", ResponseCodes.UNSUCCESS_CODE,
										ResponseCodes.UNSUCCESS_DESC + idString[j], conn);
							}
						}
						// updating purchase order
						updateOrderStatus(order_id.get(index), "C", conn);
					} catch (Exception e) {
						loggerV2.error(Helper.GetException(e));
						logs.setResponseCode(ResponseCodes.ERROR_400_CODE);
						logs.setResponseDescription(e.getMessage());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return;
					}
				} else if (type.get(index).equals("Process Core Services")) {
					ProcessCoreServicesRequestV2 requestV2 = new ProcessCoreServicesRequestV2();
					CallForwardingRequestLand coreServices = new CallForwardingRequestLand();
					CallForwardResponse response = new CallForwardResponse();
					// update the main table status
					updateOrderStatus(order_id.get(index), "P", conn);
					// get the order attributes so that further processing can
					// be done
					try {
						JSONObject jsonObject = getOrderAttributes(order_id.get(index), conn);
						loggerV2.info("Order Attributes: " + jsonObject);
						// request packet to process
						JSONArray jArray = jsonObject.getJSONArray("users");
						jsonObject.remove("users");
						jsonObject.remove("type");
						jsonObject.remove("orderKey");
						for (int i = 0; i < jArray.length(); i++) {
							jsonObject.put("msisdn", jArray.getJSONObject(i).get("msisdn").toString());
							// jsonObject.put("groupType",
							// jArray.getJSONObject(i).get("groupType").toString());
							loggerV2.info("msisdn is: " + jsonObject.getString("msisdn"));
							requestV2 = Helper.JsonToObject(jsonObject.toString(), ProcessCoreServicesRequestV2.class);

							response = coreServices.processCoreServices(requestV2);
							if (response.getReturnCode().equals(ResponseCodes.SUCESS_CODE_200)) {
								// updating order details
								updateOrderDetails(order_id.get(index), jsonObject.getString("msisdn"), "C",
										ResponseCodes.SUCESS_CODE_200, ResponseCodes.SUCESS_DES_200, conn);
								// updating purchase order
								updateOrderStatus(order_id.get(index), "C", conn);
							} else {
								// updating order details
								updateOrderDetails(order_id.get(index), jsonObject.getString("msisdn"), "F",
										response.getReturnCode(),
										response.getReturnMsg() + jsonObject.getString("msisdn"), conn);
								// update to be put here in case of partial
								// success
								// TODOs
							}

						}
						// update the main table status
						updateOrderStatus(order_id.get(index), "C", conn);
					} catch (Exception e) {
						// updating purchase order
						updateOrderStatus(order_id.get(index), "F", conn);
						loggerV2.error(Helper.GetException(e));
					}

				} else if (type.get(index).equals("Change Supplementary Offerings")) {

					RequestLandBulk requestLandBulk = new RequestLandBulk();

					// update the main table status
					updateOrderStatus(order_id.get(index), "P", conn);
					// get the order attributes so that further processing can
					// be done
					try {
						JSONObject jsonObject = getOrderAttributes(order_id.get(index), conn);
						loggerV2.info("Order Attributes: " + jsonObject);
						ChangeSupplimentryOfferingRequestLand CSOLand = new ChangeSupplimentryOfferingRequestLand();
						// request packet to process
						JSONArray jArray = jsonObject.getJSONArray("users");
						jsonObject.remove("users");
						jsonObject.remove("type");
						jsonObject.remove("orderKey");
						for (int i = 0; i < jArray.length(); i++) {
							jsonObject.put("msisdn", jArray.getJSONObject(i).get("msisdn").toString());
							jsonObject.put("groupType", jArray.getJSONObject(i).get("groupType").toString());
							loggerV2.info("msisdn is: " + jsonObject.getString("msisdn"));
							requestLandBulk = Helper.JsonToObject(jsonObject.toString(), RequestLandBulk.class);
							Response response = CSOLand.changeSupplementaryV2(requestLandBulk);

							if (response.getReturnCode().equals(ResponseCodes.SUCESS_CODE_200)) {
								// updating order details
								updateOrderDetails(order_id.get(index), jsonObject.getString("msisdn"), "C",
										ResponseCodes.SUCESS_CODE_200, ResponseCodes.SUCESS_DES_200, conn);
								// updating purchase order
								updateOrderStatus(order_id.get(index), "C", conn);
							} else {
								// updating order details
								updateOrderDetails(order_id.get(index), jsonObject.getString("msisdn"), "F",
										response.getReturnCode(),
										response.getReturnMsg() + jsonObject.getString("msisdn"), conn);
								// update to be put here in case of partial
								// success
								// TODOs
							}
						}
						// update the main table status
						updateOrderStatus(order_id.get(index), "C", conn);
					} catch (Exception e) {
						// updating purchase order
						updateOrderStatus(order_id.get(index), "F", conn);
						loggerV2.error(Helper.GetException(e));
					}
				}
			}
		} catch (Exception e) {
			loggerV2.error(Helper.GetException(e));
			logs.setResponseCode(ResponseCodes.ERROR_400_CODE);
			logs.setResponseDescription(e.getMessage());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return;
		}
	}

	/**
	 * helper function to update order details
	 * 
	 * @param order_id
	 * @param msisdn
	 * @param status
	 * @param responseCode
	 * @param responseDesc
	 * @param conn
	 * @return Returns true if the update is successful else false
	 * @throws SQLException
	 */
	@Transactional(rollbackFor = Exception.class)
	private boolean updateOrderDetails(String order_id, String msisdn, String status, String responseCode,
			String responseDesc, Connection conn) throws SQLException {
		String query = "";
		if (msisdn.equals("")) {
			query = "update order_details set status = ?, responsecode=?, responsedescription=? where order_id_fk= ?";
		} else
			query = "update order_details set status = ?, responsecode=?, responsedescription=? where order_id_fk= ? and msisdn=?";
		// String
		PreparedStatement preparedStmt = conn.prepareStatement(query);
		preparedStmt = conn.prepareStatement(query);
		preparedStmt.setString(1, status);
		preparedStmt.setString(2, responseCode);
		preparedStmt.setString(3, responseDesc);
		preparedStmt.setString(4, order_id);
		if (!msisdn.equals(""))
			preparedStmt.setString(5, msisdn);
		loggerV2.info("QUERY for update Order_purchase" + preparedStmt.toString());
		preparedStmt.execute();
		int count = preparedStmt.getUpdateCount();
		if (count > 0)
			return true;
		else
			return false;

	}

	/**
	 * Fetches order details from DB
	 * 
	 * @param order_id
	 * @param conn
	 * @return Json object containing order details
	 * @throws IOException
	 * @throws Exception
	 */
	@SuppressWarnings("resource")
	private JSONObject getOrderAttributes(String order_id, Connection conn) throws IOException, Exception {
		String query = "select key_value, value from order_attributes where order_id_fk =?";
		PreparedStatement preparedStmt = conn.prepareStatement(query);
		preparedStmt.setString(1, order_id);
		ResultSet resultSet = preparedStmt.executeQuery();
		JSONObject jsonObject = new JSONObject();
		while (resultSet.next()) {
			// logger.info("<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>" +
			// (resultSet.getString(2)));
			jsonObject.put(resultSet.getString(1), (resultSet.getString(2)));
		}
		if (jsonObject.getString("type").toString().equals("BroadcastSMS")) {
			query = "select msisdn from order_details where order_id_fk =?";
			preparedStmt = conn.prepareStatement(query);
			preparedStmt.setString(1, order_id);
			resultSet = preparedStmt.executeQuery();
			StringBuffer stringBuffer = new StringBuffer();
			while (resultSet.next()) {
				stringBuffer.append(resultSet.getString(1)).append(",");
			}
			stringBuffer.replace(stringBuffer.length() - 1, stringBuffer.length(), "");
			jsonObject.put("recieverMsisdn", stringBuffer.toString());
		} else if (jsonObject.getString("type").toString().equals("Process Core Services")) {
			// TODO change the resp according to request packet
			query = "select msisdn from order_details where order_id_fk =?";
			preparedStmt = conn.prepareStatement(query);
			preparedStmt.setString(1, order_id);
			resultSet = preparedStmt.executeQuery();
			JSONArray jArray = new JSONArray();
			while (resultSet.next()) {
				JSONObject jobj = new JSONObject();
				jobj.put("msisdn", resultSet.getString(1));
				jArray.put(jobj);
			}
			jsonObject.put("users", jArray);
		} else if (jsonObject.getString("type").toString().equals("Change Supplementary Offerings")) {
			query = "select msisdn,group_type from order_details where order_id_fk =?";
			preparedStmt = conn.prepareStatement(query);
			preparedStmt.setString(1, order_id);
			resultSet = preparedStmt.executeQuery();
			JSONArray jArray = new JSONArray();
			while (resultSet.next()) {
				JSONObject jobj = new JSONObject();
				jobj.put("msisdn", resultSet.getString(1));
				jobj.put("groupType", resultSet.getString(2));
				jArray.put(jobj);
			}
			jsonObject.put("users", jArray);
		}
		loggerV2.info("attributes are: " + jsonObject);
		resultSet.close();
		preparedStmt.close();
		return jsonObject;

	}

	/**
	 * Fetches order type from DB
	 * 
	 * @param conn
	 * @return Returns a map containing array list of orders with types
	 * @throws SQLException
	 * @throws JSONException
	 */
	private Map<String, ArrayList<String>> getOrderType(Connection conn) throws SQLException, JSONException {
		String query = "select type,order_id from purchase_order where status ='N'";
		PreparedStatement preparedStmt = conn.prepareStatement(query);
		ResultSet rSet = preparedStmt.executeQuery();
		Map<String, ArrayList<String>> map = new HashMap<>();
		ArrayList<String> type = new ArrayList<>();
		ArrayList<String> order_id = new ArrayList<>();
		while (rSet.next()) {
			type.add(rSet.getString("TYPE"));
			order_id.add(rSet.getString("ORDER_ID"));
			loggerV2.info("Pending Order Type is: " + rSet.getString("TYPE") + "  >>>>> <<<<< Order Id is: "
					+ rSet.getString("ORDER_ID"));
		}
		map.put("Type", type);
		map.put("Order_Id", order_id);
		rSet.close();
		preparedStmt.close();
		return map;
	}

	/**
	 * helper function to update order status upon processing
	 * 
	 * @param order_id
	 * @param status
	 * @param conn
	 * @return Returns true if any failed orders found else false
	 * @throws SQLException
	 */
	@Transactional(rollbackFor = Exception.class)
	private boolean updateOrderStatus(String order_id, String status, Connection conn) throws SQLException {
		String query = "update purchase_order set status = ? where order_id= ?";
		if(conn.isClosed())
			conn = DBFactory.getDbConnectionFromPool();
		PreparedStatement preparedStmt = conn.prepareStatement(query);
		preparedStmt = conn.prepareStatement(query);
		preparedStmt.setString(1, status);
		preparedStmt.setString(2, order_id);
		loggerV2.info("QUERY for update Order_purchase" + preparedStmt);
		preparedStmt.execute();
		int count = preparedStmt.getUpdateCount();
		if (count > 0)
		{
			conn.close();
			return true;
		}
		else
		{
			conn.close();
			return false;
		}
	}

	/**
	 * Generates order history against pic
	 * 
	 * @param credential
	 * @param contentType
	 * @param requestBody
	 * @return Returns action history for bulk operations against Pic
	 */
	@POST
	@Path("/actionhistory")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Transactional(rollbackFor = Exception.class)
	public BaseResponse actionHistory(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		Logs logs = new Logs();
//		java.sql.Connection conn = DBFactory.getDbConnection();
		logs.setTransactionName(Transactions.MANAGE_ORDER);
		logs.setThirdPartyName(ThirdPartyNames.MANAGE_ORDER);
		logs.setTableType(LogsType.ManageOrder);
		ActionHistoryRequest cclient = new ActionHistoryRequest();
		ActionHistoryResponse resp = new ActionHistoryResponse();

		loggerV2.info("Request Landed on Action History: " + requestBody);
		try (java.sql.Connection conn = DBFactory.getDbConnectionFromPool();) {
			cclient = Helper.JsonToObject(requestBody, ActionHistoryRequest.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
					if (credentials == null) {
						loggerV2.info(cclient.getmsisdn() + " - Credentials Not Verified");
						resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
						resp.setReturnMsg(ResponseCodes.ERROR_401);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					} else if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
						String verification = Helper.validateRequest(cclient);
						if (!verification.equals("")) {
							loggerV2.info(cclient.getmsisdn() + " - Verification of Request Failed");
							resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
							resp.setReturnMsg(verification);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
						if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
							// getting the order id
							String query = "";
							if (cclient.getOrderType().equals("") || cclient.getOrderType() == null)
								query = "select order_id, type, status,created_at, order_key from purchase_order where username =? and created_at>=? and created_at<=?";
							else
								query = "select order_id, type, status,created_at, order_key from purchase_order where username =? and created_at>=? and created_at<=? and type =?";
							PreparedStatement preparedStmt = conn.prepareStatement(query);
							try {
								// ArrayList<ActionHistoryResponse>
								// historyResponse = new ArrayList<>();
								preparedStmt = conn.prepareStatement(query);
								preparedStmt.setString(1, cclient.getmsisdn());
								preparedStmt.setString(2, cclient.getStartDate());
								preparedStmt.setString(3, cclient.getEndDate() + " 23:59:59");
								if (!cclient.getOrderType().equals(""))
									preparedStmt.setString(4, cclient.getOrderType());
								ResultSet rSet = preparedStmt.executeQuery();
								ArrayList<ActionHistoryResponseData> historylist = new ArrayList<ActionHistoryResponseData>();
								while (rSet.next()) {
									ActionHistoryResponseData resObj = new ActionHistoryResponseData();

									String orderId = rSet.getString(1);
									String orderType = rSet.getString(2);
									String orderStatus = rSet.getString(3);
									String createdAt = rSet.getString(4);
									String orderKey = rSet.getString(5);
									resObj.setOrderId(orderId);
									resObj.setOrderStatus(orderStatus);
									resObj.setOrderType(orderType);
									resObj.setOrderKey(orderKey);

									resObj.setDate(createdAt.replace("-", "/"));
									loggerV2.info(cclient.getmsisdn() + " - Parent order ID:" + orderId + " order Type:"
											+ orderType + " order stauts:" + orderStatus);
									// calculating count for completed orders
									query = "select count(*) as COUNT from order_details where order_id_fk =? and status =? ";
									preparedStmt = conn.prepareStatement(query);
									preparedStmt.setString(1, orderId);
									preparedStmt.setString(2, "C");
									int totalCount = 0;
									loggerV2.info(
											cclient.getmsisdn() + " - Query for completed Orders: " + preparedStmt);
									ResultSet resultSet = preparedStmt.executeQuery();
									while (resultSet.next()) {
										String count = resultSet.getString(1);
										totalCount += Integer.parseInt(count);
										loggerV2.info(
												cclient.getmsisdn() + " - Count For Completed Orders is:" + count);
										resObj.setSuccess(count);
									}
									// calculating count for pending orders
									preparedStmt.setString(2, "P");
									loggerV2.info(cclient.getmsisdn() + " - Query for Pending Orders " + preparedStmt);
									resultSet = preparedStmt.executeQuery();
									while (resultSet.next()) {
										String count = resultSet.getString(1);
										totalCount += Integer.parseInt(count);
										loggerV2.info(cclient.getmsisdn() + " - Count For Pending Orders is: " + count);
										resObj.setPending(count);
									}

									// calculating count for Cancelled orders
									preparedStmt.setString(2, "U");
									loggerV2.info(cclient.getmsisdn() + " - Query For Cancelled Orders" + preparedStmt);
									resultSet = preparedStmt.executeQuery();
									while (resultSet.next()) {
										String count = resultSet.getString(1);
										totalCount += Integer.parseInt(count);
										loggerV2.info(
												cclient.getmsisdn() + " - Count For Cancelled Orders is:" + count);
										resObj.setCancelled(count);
									}

									// calculating count for Failed orders
									preparedStmt.setString(2, "F");
									loggerV2.info(cclient.getmsisdn() + " - Query for Failed Orders: " + preparedStmt);
									resultSet = preparedStmt.executeQuery();
									while (resultSet.next()) {
										String count = resultSet.getString(1);
										totalCount += Integer.parseInt(count);
										loggerV2.info(cclient.getmsisdn() + " - Count For Failed Orders is:" + count);
										resObj.setFailed(count);
									}
									resObj.setTotalCount(String.valueOf(totalCount));
									historylist.add(resObj);

								}

								resp.setOrderList(historylist);
								resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
								resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								rSet.close();
								preparedStmt.close();
								return resp;

							} catch (Exception e) {
								loggerV2.error(Helper.GetException(e));
								resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
								resp.setReturnMsg(e.getMessage());
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							}

						}
					} else {
						loggerV2.error("Incorrect Msisdn");
						resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
						resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} catch (Exception ex) {
					loggerV2.error(Helper.GetException(ex));
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}

		} catch (Exception e) {
			loggerV2.error(Helper.GetException(e));
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_401);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		return resp;

	}

}
