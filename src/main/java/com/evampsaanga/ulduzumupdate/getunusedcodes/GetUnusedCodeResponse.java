package com.evampsaanga.ulduzumupdate.getunusedcodes;

import java.util.List;

public class GetUnusedCodeResponse {
	String result;
	List<UnusedCode> data;

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public List<UnusedCode> getData() {
		return data;
	}

	public void setData(List<UnusedCode> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "GetUnusedCodeResponse [result=" + result + ", data=" + data + "]";
	}

}
