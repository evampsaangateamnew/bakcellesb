package com.huawei.crm.service.ens;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.crm.basetype.ens.OfferingKey;

/**
 * <p>
 * Java class for OfferingRspInfo complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="OfferingRspInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ActionType" type="{http://crm.huawei.com/basetype/}ActionType"/>
 *         &lt;element name="OfferingId" type="{http://crm.huawei.com/basetype/}OfferingKey"/>
 *         &lt;element name="OldOfferingId" type="{http://crm.huawei.com/basetype/}OfferingKey" minOccurs="0"/>
 *         &lt;element name="OwnerType" type="{http://crm.huawei.com/basetype/}OwnerType" minOccurs="0"/>
 *         &lt;element name="ParentOfferingId" type="{http://crm.huawei.com/basetype/}OfferingId" minOccurs="0"/>
 *         &lt;element name="EffectiveTime" type="{http://crm.huawei.com/basetype/}Time" minOccurs="0"/>
 *         &lt;element name="ExpiredTime" type="{http://crm.huawei.com/basetype/}Time" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OfferingRspInfo", propOrder = {})
public class OfferingRspInfo {
	@XmlElement(name = "ActionType", required = true)
	protected String actionType;
	@XmlElement(name = "OfferingId", required = true)
	protected OfferingKey offeringId;
	@XmlElement(name = "OldOfferingId")
	protected OfferingKey oldOfferingId;
	@XmlElement(name = "OwnerType")
	protected String ownerType;
	@XmlElement(name = "ParentOfferingId")
	protected String parentOfferingId;
	@XmlElement(name = "EffectiveTime")
	protected String effectiveTime;
	@XmlElement(name = "ExpiredTime")
	protected String expiredTime;

	/**
	 * Gets the value of the actionType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getActionType() {
		return actionType;
	}

	/**
	 * Sets the value of the actionType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setActionType(String value) {
		this.actionType = value;
	}

	/**
	 * Gets the value of the offeringId property.
	 * 
	 * @return possible object is {@link OfferingKey }
	 * 
	 */
	public OfferingKey getOfferingId() {
		return offeringId;
	}

	/**
	 * Sets the value of the offeringId property.
	 * 
	 * @param value
	 *            allowed object is {@link OfferingKey }
	 * 
	 */
	public void setOfferingId(OfferingKey value) {
		this.offeringId = value;
	}

	/**
	 * Gets the value of the oldOfferingId property.
	 * 
	 * @return possible object is {@link OfferingKey }
	 * 
	 */
	public OfferingKey getOldOfferingId() {
		return oldOfferingId;
	}

	/**
	 * Sets the value of the oldOfferingId property.
	 * 
	 * @param value
	 *            allowed object is {@link OfferingKey }
	 * 
	 */
	public void setOldOfferingId(OfferingKey value) {
		this.oldOfferingId = value;
	}

	/**
	 * Gets the value of the ownerType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOwnerType() {
		return ownerType;
	}

	/**
	 * Sets the value of the ownerType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOwnerType(String value) {
		this.ownerType = value;
	}

	/**
	 * Gets the value of the parentOfferingId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getParentOfferingId() {
		return parentOfferingId;
	}

	/**
	 * Sets the value of the parentOfferingId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setParentOfferingId(String value) {
		this.parentOfferingId = value;
	}

	/**
	 * Gets the value of the effectiveTime property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEffectiveTime() {
		return effectiveTime;
	}

	/**
	 * Sets the value of the effectiveTime property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEffectiveTime(String value) {
		this.effectiveTime = value;
	}

	/**
	 * Gets the value of the expiredTime property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getExpiredTime() {
		return expiredTime;
	}

	/**
	 * Sets the value of the expiredTime property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setExpiredTime(String value) {
		this.expiredTime = value;
	}
}
