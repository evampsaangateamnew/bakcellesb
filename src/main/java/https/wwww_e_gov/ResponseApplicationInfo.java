
package https.wwww_e_gov;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ResponseApplicationInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResponseApplicationInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Status" type="{https://wwww.e-gov.az}ResponseStatus" minOccurs="0"/>
 *         &lt;element name="AddSuccessCount" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseApplicationInfo", propOrder = {
    "status",
    "addSuccessCount"
})
public class ResponseApplicationInfo {

    @XmlElement(name = "Status")
    protected ResponseStatus status;
    @XmlElement(name = "AddSuccessCount")
    protected int addSuccessCount;

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseStatus }
     *     
     */
    public ResponseStatus getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseStatus }
     *     
     */
    public void setStatus(ResponseStatus value) {
        this.status = value;
    }

    /**
     * Gets the value of the addSuccessCount property.
     * 
     */
    public int getAddSuccessCount() {
        return addSuccessCount;
    }

    /**
     * Sets the value of the addSuccessCount property.
     * 
     */
    public void setAddSuccessCount(int value) {
        this.addSuccessCount = value;
    }

}
