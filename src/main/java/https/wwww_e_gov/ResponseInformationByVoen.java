
package https.wwww_e_gov;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ResponseInformationByVoen complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResponseInformationByVoen">
 *   &lt;complexContent>
 *     &lt;extension base="{https://wwww.e-gov.az}ResponseInformationByDocuments">
 *       &lt;sequence>
 *         &lt;element name="VoenInformation" type="{https://wwww.e-gov.az}ResponseVoenInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseInformationByVoen", propOrder = {
    "voenInformation"
})
public class ResponseInformationByVoen
    extends ResponseInformationByDocuments
{

    @XmlElement(name = "VoenInformation")
    protected ResponseVoenInfo voenInformation;

    /**
     * Gets the value of the voenInformation property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseVoenInfo }
     *     
     */
    public ResponseVoenInfo getVoenInformation() {
        return voenInformation;
    }

    /**
     * Sets the value of the voenInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseVoenInfo }
     *     
     */
    public void setVoenInformation(ResponseVoenInfo value) {
        this.voenInformation = value;
    }

}
