package com.evampsaanga.bakcell.notificationadmin.magentomodels;

import java.util.List;

public class Data {
	private List<OfferData> offer;
	private List<TariffData> tariff;

	public List<OfferData> getOffer() {
		return offer;
	}

	public void setOffer(List<OfferData> offer) {
		this.offer = offer;
	}

	public List<TariffData> getTariff() {
		return tariff;
	}

	public void setTariff(List<TariffData> tariff) {
		this.tariff = tariff;
	}

	@Override
	public String toString() {
		return "Data [offer=" + offer + ", tariff=" + tariff + "]";
	}
}
