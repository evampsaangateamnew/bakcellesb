package com.huawei.crm.update.ens;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the com.huawei.crm.update package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {
	/**
	 * Create a new ObjectFactory that can be used to create new instances of
	 * schema derived classes for package: com.huawei.crm.update
	 * 
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link NotificationContentInfo }
	 * 
	 */
	public NotificationContentInfo createNotificationContentInfo() {
		return new NotificationContentInfo();
	}

	/**
	 * Create an instance of {@link NotificationContentInfo.ContentInfo }
	 * 
	 */
	public NotificationContentInfo.ContentInfo createNotificationContentInfoContentInfo() {
		return new NotificationContentInfo.ContentInfo();
	}

	/**
	 * Create an instance of {@link SendNotificationOut }
	 * 
	 */
	public SendNotificationOut createSendNotificationOut() {
		return new SendNotificationOut();
	}

	/**
	 * Create an instance of {@link SendSMSMessageRequest }
	 * 
	 */
	public SendSMSMessageRequest createSendSMSMessageRequest() {
		return new SendSMSMessageRequest();
	}

	/**
	 * Create an instance of {@link SendSMSMessageIn }
	 * 
	 */
	public SendSMSMessageIn createSendSMSMessageIn() {
		return new SendSMSMessageIn();
	}

	/**
	 * Create an instance of {@link SendMessageToSubscriberRequest }
	 * 
	 */
	public SendMessageToSubscriberRequest createSendMessageToSubscriberRequest() {
		return new SendMessageToSubscriberRequest();
	}

	/**
	 * Create an instance of {@link SendMessageToSubsIn }
	 * 
	 */
	public SendMessageToSubsIn createSendMessageToSubsIn() {
		return new SendMessageToSubsIn();
	}

	/**
	 * Create an instance of {@link SendMessageToSubscriberResponse }
	 * 
	 */
	public SendMessageToSubscriberResponse createSendMessageToSubscriberResponse() {
		return new SendMessageToSubscriberResponse();
	}

	/**
	 * Create an instance of {@link TriggerNotificationRequest }
	 * 
	 */
	public TriggerNotificationRequest createTriggerNotificationRequest() {
		return new TriggerNotificationRequest();
	}

	/**
	 * Create an instance of {@link TriggerNotificationIn }
	 * 
	 */
	public TriggerNotificationIn createTriggerNotificationIn() {
		return new TriggerNotificationIn();
	}

	/**
	 * Create an instance of {@link SendNotificationResponse }
	 * 
	 */
	public SendNotificationResponse createSendNotificationResponse() {
		return new SendNotificationResponse();
	}

	/**
	 * Create an instance of {@link SendNotificationRequest }
	 * 
	 */
	public SendNotificationRequest createSendNotificationRequest() {
		return new SendNotificationRequest();
	}

	/**
	 * Create an instance of {@link SendNotificationIn }
	 * 
	 */
	public SendNotificationIn createSendNotificationIn() {
		return new SendNotificationIn();
	}

	/**
	 * Create an instance of {@link SendSMSMessageResponse }
	 * 
	 */
	public SendSMSMessageResponse createSendSMSMessageResponse() {
		return new SendSMSMessageResponse();
	}

	/**
	 * Create an instance of {@link TriggerNotificationResponse }
	 * 
	 */
	public TriggerNotificationResponse createTriggerNotificationResponse() {
		return new TriggerNotificationResponse();
	}

	/**
	 * Create an instance of {@link TriggerNotificationOut }
	 * 
	 */
	public TriggerNotificationOut createTriggerNotificationOut() {
		return new TriggerNotificationOut();
	}

	/**
	 * Create an instance of {@link ReceiverEntityInfo }
	 * 
	 */
	public ReceiverEntityInfo createReceiverEntityInfo() {
		return new ReceiverEntityInfo();
	}

	/**
	 * Create an instance of {@link AttachmentInfo }
	 * 
	 */
	public AttachmentInfo createAttachmentInfo() {
		return new AttachmentInfo();
	}

	/**
	 * Create an instance of {@link NotifyEntityInfo }
	 * 
	 */
	public NotifyEntityInfo createNotifyEntityInfo() {
		return new NotifyEntityInfo();
	}

	/**
	 * Create an instance of {@link ReceiverAddressList }
	 * 
	 */
	public ReceiverAddressList createReceiverAddressList() {
		return new ReceiverAddressList();
	}

	/**
	 * Create an instance of {@link NotificationInfo }
	 * 
	 */
	public NotificationInfo createNotificationInfo() {
		return new NotificationInfo();
	}

	/**
	 * Create an instance of {@link NotificationContentInfo.TemplateInfo }
	 * 
	 */
	public NotificationContentInfo.TemplateInfo createNotificationContentInfoTemplateInfo() {
		return new NotificationContentInfo.TemplateInfo();
	}

	/**
	 * Create an instance of
	 * {@link NotificationContentInfo.ContentInfo.AttachmentList }
	 * 
	 */
	public NotificationContentInfo.ContentInfo.AttachmentList createNotificationContentInfoContentInfoAttachmentList() {
		return new NotificationContentInfo.ContentInfo.AttachmentList();
	}

	/**
	 * Create an instance of {@link SendNotificationOut.ResultInfo }
	 * 
	 */
	public SendNotificationOut.ResultInfo createSendNotificationOutResultInfo() {
		return new SendNotificationOut.ResultInfo();
	}
}
