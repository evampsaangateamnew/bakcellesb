
package com.huawei.bss.soaif._interface.common.createorder;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Consumption complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Consumption">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LimitType">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="16"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="Currency" type="{http://www.huawei.com/bss/soaif/interface/common/}Currency" minOccurs="0"/>
 *         &lt;element name="LimitValue" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;pattern value="[0-9]+(.[0-9]{2})?"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="NotifyType" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="0"/>
 *               &lt;enumeration value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="NotifyLimit" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;pattern value="[0-9]{1,4}"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Consumption", propOrder = {
    "limitType",
    "currency",
    "limitValue",
    "notifyType",
    "notifyLimit"
})
public class Consumption {

    @XmlElement(name = "LimitType", required = true)
    protected String limitType;
    @XmlElement(name = "Currency")
    protected BigInteger currency;
    @XmlElement(name = "LimitValue")
    protected String limitValue;
    @XmlElement(name = "NotifyType")
    protected String notifyType;
    @XmlElement(name = "NotifyLimit")
    protected String notifyLimit;

    /**
     * Gets the value of the limitType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLimitType() {
        return limitType;
    }

    /**
     * Sets the value of the limitType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLimitType(String value) {
        this.limitType = value;
    }

    /**
     * Gets the value of the currency property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCurrency() {
        return currency;
    }

    /**
     * Sets the value of the currency property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCurrency(BigInteger value) {
        this.currency = value;
    }

    /**
     * Gets the value of the limitValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLimitValue() {
        return limitValue;
    }

    /**
     * Sets the value of the limitValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLimitValue(String value) {
        this.limitValue = value;
    }

    /**
     * Gets the value of the notifyType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotifyType() {
        return notifyType;
    }

    /**
     * Sets the value of the notifyType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotifyType(String value) {
        this.notifyType = value;
    }

    /**
     * Gets the value of the notifyLimit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotifyLimit() {
        return notifyLimit;
    }

    /**
     * Sets the value of the notifyLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotifyLimit(String value) {
        this.notifyLimit = value;
    }

}
