package com.evampsaanga.ulduzumupdate.getnewcode;

public class GetNewCodeResponse {
	String result;
	GetNewCode data;

	/**
	 * @return the result
	 */
	public String getResult() {
		return result;
	}

	/**
	 * @param result
	 *            the result to set
	 */
	public void setResult(String result) {
		this.result = result;
	}

	public GetNewCode getData() {
		return data;
	}

	public void setData(GetNewCode data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "GetNewCodeResponse [result=" + result + ", data=" + data + "]";
	}

}
