package com.evampsaanga.bakcell.querypaymentlog;

import com.evampsaanga.bakcell.responseheaders.BaseResponse;
import com.huawei.bme.cbsinterface.arservices.QueryPaymentLogResult;

public class QueryPaymentLogResponseClient extends BaseResponse {
	QueryPaymentLogResult querypaymentlogresult = new QueryPaymentLogResult();

	public void setQueryPaymentLog(QueryPaymentLogResult queryPaymentLogResult) {
		querypaymentlogresult = queryPaymentLogResult;
	}
}
