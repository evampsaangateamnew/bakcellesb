package com.huawei.crm.service;

import java.math.BigInteger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class DoMarshall {
	public static void main(String[] args) {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(Item.class);
			Item item = new Item();
			item.setDescription("Test description");
			item.price = 10;
			item.setCatalogNumber(new BigInteger("10"));
			Marshaller marshaller = jaxbContext.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			// USSDGWLoanRspMsg erp= new USSDGWLoanRspMsg();
			// erp.setETUGracePeriod(9);
			marshaller.marshal(item, System.out);
		} catch (JAXBException e) {
		}
	}
}