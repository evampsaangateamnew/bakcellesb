package com.evampsaanga.bakcell.notificationadmin;

public class HistoryNotification {
	private String id;
	private String msisdn;
	private String dateTime;
	private String messageAz;
	private String messageRu;
	private String messageEn;
	private String icon;
	private String actionType;
	private String actionID;
	private String btnTxt;
	private String readStatus;
	private String notificationId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getMessageAz() {
		return messageAz;
	}

	public void setMessageAz(String messageAz) {
		this.messageAz = messageAz;
	}

	public String getMessageRu() {
		return messageRu;
	}

	public void setMessageRu(String messageRu) {
		this.messageRu = messageRu;
	}

	public String getMessageEn() {
		return messageEn;
	}

	public void setMessageEn(String messageEn) {
		this.messageEn = messageEn;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getActionID() {
		return actionID;
	}

	public void setActionID(String actionID) {
		this.actionID = actionID;
	}

	public String getBtnTxt() {
		return btnTxt;
	}

	public void setBtnTxt(String btnTxt) {
		this.btnTxt = btnTxt;
	}

	public String getReadStatus() {
		return readStatus;
	}

	public void setReadStatus(String readStatus) {
		this.readStatus = readStatus;
	}

	public String getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(String notificationId) {
		this.notificationId = notificationId;
	}

	@Override
	public String toString() {
		return "HistoryNotification [id=" + id + ", msisdn=" + msisdn + ", dateTime=" + dateTime + ", messageAz="
				+ messageAz + ", messageRu=" + messageRu + ", messageEn=" + messageEn + ", icon=" + icon
				+ ", actionType=" + actionType + ", actionID=" + actionID + ", btnTxt=" + btnTxt + ", readStatus="
				+ readStatus + ", notificationId=" + notificationId + "]";
	}

}
