package com.evampsaanga.ulduzumupdate.getusagehistory;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UsageHistory {

	@JsonProperty("id")
	private String id;
	@JsonProperty("merchant_id")
	private String merchantId;
	@JsonProperty("merchant_name")
	private String merchantName;
	@JsonProperty("category_id")
	private String categoryId;
	@JsonProperty("category_name")
	private String categoryName;
	@JsonProperty("identical_code")
	private String identicalCode;
	@JsonProperty("redemption_date")
	private String redemptionDate;
	@JsonProperty("redemption_time")
	private String redemptionTime;
	@JsonProperty("amount")
	private String amount;
	@JsonProperty("discount_amount")
	private String discountAmount;
	@JsonProperty("discounted_amount")
	private String discountedAmount;
	@JsonProperty("discount_percent")
	private String discountPercent;
	@JsonProperty("payment_type")
	private String paymentType;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getIdenticalCode() {
		return identicalCode;
	}

	public void setIdenticalCode(String identicalCode) {
		this.identicalCode = identicalCode;
	}

	public String getRedemptionDate() {
		return redemptionDate;
	}

	public void setRedemptionDate(String redemptionDate) {
		this.redemptionDate = redemptionDate;
	}

	public String getRedemptionTime() {
		return redemptionTime;
	}

	public void setRedemptionTime(String redemptionTime) {
		this.redemptionTime = redemptionTime;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(String discountAmount) {
		this.discountAmount = discountAmount;
	}

	public String getDiscountedAmount() {
		return discountedAmount;
	}

	public void setDiscountedAmount(String discountedAmount) {
		this.discountedAmount = discountedAmount;
	}

	public String getDiscountPercent() {
		return discountPercent;
	}

	public void setDiscountPercent(String discountPercent) {
		this.discountPercent = discountPercent;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	@Override
	public String toString() {
		return "UsageHistory [id=" + id + ", merchantId=" + merchantId + ", merchantName=" + merchantName
				+ ", categoryId=" + categoryId + ", categoryName=" + categoryName + ", identicalCode=" + identicalCode
				+ ", redemptionDate=" + redemptionDate + ", redemptionTime=" + redemptionTime + ", amount=" + amount
				+ ", discountAmount=" + discountAmount + ", discountedAmount=" + discountedAmount + ", discountPercent="
				+ discountPercent + ", paymentType=" + paymentType + "]";
	}

}
