package com.evampsaanga.bakcell.getsubscriber;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import org.apache.log4j.Logger;

import com.evampsaanga.bakcell.db.DBFactory;
import com.evampsaanga.bakcell.getcoreservices.CoreServices;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.services.BcService;
import com.evampsaanga.services.CRMServices;
import com.huawei.bme.cbsinterface.bccommon.OfferingKey;
import com.huawei.bme.cbsinterface.bccommon.SubAccessCode;
import com.huawei.bme.cbsinterface.bcservices.QueryOfferingRentCycleRequest;
import com.huawei.bme.cbsinterface.bcservices.QueryOfferingRentCycleRequest.OfferingInst;
import com.huawei.bme.cbsinterface.bcservices.QueryOfferingRentCycleRequest.OfferingInst.OfferingOwner;
import com.huawei.bme.cbsinterface.bcservices.QueryOfferingRentCycleRequestMsg;
import com.huawei.bme.cbsinterface.bcservices.QueryOfferingRentCycleResultMsg;
import com.huawei.crm.basetype.ExtParameterInfo;
import com.huawei.crm.basetype.GetSubOfferingInfo;
import com.huawei.crm.basetype.GetSubProductInfo;
import com.huawei.crm.basetype.RequestHeader;
import com.huawei.crm.query.GetSubscriberIn;
import com.huawei.crm.query.GetSubscriberOut;
import com.huawei.crm.query.GetSubscriberRequest;
import com.huawei.crm.query.GetSubscriberResponse;

public class CRMSubscriberService {
	public static final Logger logger = Logger.getLogger("bakcellLogs-V2");
	static {
		java.util.logging.Logger.getLogger("com.sun.xml.internal.bind").setLevel(Level.FINEST);
	}

	/*
	 * public static HashMap<String, ArrayList<CoreServicesCategoryItem>>
	 * coreServicesCategoryItemCacheHashMap = new HashMap<String,
	 * ArrayList<CoreServicesCategoryItem>>();
	 * 
	 * public static ArrayList<CoreServicesCategoryItem>
	 * getCoreServicesCategoryItemList(String key) {
	 * logger.info("Cache Get configuration for key "+key);
	 * logger.info("Cache Check in HashMap "
	 * +coreServicesCategoryItemCacheHashMap.containsKey(key));
	 * if(coreServicesCategoryItemCacheHashMap.containsKey(key)) {
	 * logger.info("Cache "+coreServicesCategoryItemCacheHashMap.get(key));
	 * return coreServicesCategoryItemCacheHashMap.get(key); } else {
	 * logger.info("Cache Reloading properties"); reloadPropertiesCache();
	 * logger.info("Cache Check in HashMap after reload"
	 * +coreServicesCategoryItemCacheHashMap.containsKey(key));
	 * if(coreServicesCategoryItemCacheHashMap.containsKey(key)) return
	 * coreServicesCategoryItemCacheHashMap.get(key); else return new
	 * ArrayList<CoreServicesCategoryItem>(); } }
	 */

	public static HashMap<String, List<CoreServicesCategory>> coreServicesCategoryCacheHashMap = new HashMap<String, List<CoreServicesCategory>>();

	private List<CoreServicesCategory> getCoreServicesCategoryListFromCacheMap(String lang, String freeFor) {
		List<CoreServicesCategory> categories = new ArrayList<CoreServicesCategory>();
		/*
		 * TODO Below commented code has some bug on apps, while deactivating. I
		 * am back from apps is not reflecting in apps as deactivated unless
		 * refresh from apps. To be checked and fixed.
		 *
		 */
		// logger.info("Cache Get CoreServicesCategory for key " + lang);

		// logger.info("Cache Check in HashMap coreservices:" +
		// coreServicesCategoryCacheHashMap.containsKey(lang + "-" + freeFor));
		// if (coreServicesCategoryCacheHashMap.containsKey(lang + "-@" +
		// freeFor)
		// || coreServicesCategoryCacheHashMap.containsKey(lang + "-" +
		// freeFor)) {
		// logger.info("Cache " + coreServicesCategoryCacheHashMap.get(lang +
		// "-" + freeFor));
		// categories = coreServicesCategoryCacheHashMap.get(lang + "-" +
		// freeFor);
		// logger.info(" <<<<<<<<< hashmap :" +
		// coreServicesCategoryCacheHashMap.get(lang + "-" + freeFor).toString()
		// + ">>>>>>>>>");
		// logger.info("category size from hashmap :" + categories.size());
		// return categories;
		// } else {
		// coreServicesCategoryCacheHashMap.clear();
		logger.info("Cache Reloading CoreServicesCategory");
		reloadCoreServicesCategoryCacheHashMap(lang, freeFor);
		logger.info("Cache Check in HashMap after reload" + coreServicesCategoryCacheHashMap.containsKey(lang + "-" + freeFor));
		if (coreServicesCategoryCacheHashMap.containsKey(lang + "-" + freeFor)) {
			categories = coreServicesCategoryCacheHashMap.get(lang + "-" + freeFor);
			logger.info("category size from hashmap :" + categories.size());
			return categories;
		}
		else {
			logger.info("category size from hashmap :" + categories.size());
			return categories;
		}

		// }

	}

	private List<CoreServicesCategory> getCoreServicesCategoryListFromCacheMapV2(String lang) {
		List<CoreServicesCategory> categories = new ArrayList<CoreServicesCategory>();
		logger.info("Cache Get CoreServicesCategory for key " + lang);
		logger.info("Cache Check in HashMap " + coreServicesCategoryCacheHashMap.containsKey(lang + "-V2"));
		if (coreServicesCategoryCacheHashMap.containsKey(lang + "-V2")) {
			logger.info("Cache " + coreServicesCategoryCacheHashMap.get(lang + "-V2"));
			categories = coreServicesCategoryCacheHashMap.get(lang + "-V2");
			logger.info(" <<<<<<<<< hashmap :" + coreServicesCategoryCacheHashMap.get(lang + "-V2").toString() + ">>>>>>>>>");
			logger.info("category size from hashmap :" + categories.size());
			return categories;
		}
		else {
			coreServicesCategoryCacheHashMap.clear();
			logger.info("Cache Reloading CoreServicesCategory");
			reloadCoreServicesCategoryCacheHashMap(lang, "V2");
			logger.info("Cache Check in HashMap after reload" + coreServicesCategoryCacheHashMap.containsKey(lang + "-V2"));
			if (coreServicesCategoryCacheHashMap.containsKey(lang + "-V2")) {
				categories = coreServicesCategoryCacheHashMap.get(lang + "-V2");
				logger.info("category size from hashmap :" + categories.size());
				return categories;
			}
			else {
				logger.info("category size from hashmap :" + categories.size());
				return categories;
			}

		}
	}

	private void reloadCoreServicesCategoryCacheHashMap(String lang, String freeFor) {
		List<CoreServicesCategory> listOfCoreServicesCategory = getListOfCoreServicesCategory(lang + "," + freeFor);
		if (listOfCoreServicesCategory != null && listOfCoreServicesCategory.size() > 0) {
			coreServicesCategoryCacheHashMap.put(lang + "-" + freeFor, listOfCoreServicesCategory);
		}
	}

	public ArrayList<CoreServices> getCoreServices(GetSubscriberOut data, String lang, String isFrom, String freeFor, String visibleFor, List<GetSubProductInfo> getsubproductinfo, String accountType) {

		// As We need Status from GetNetWorkSettingsAPI so we will call
		// GetNetWork Settings API

		ArrayList<CoreServices> coreServicesList = new ArrayList<CoreServices>();
		String effctive = "", expire = "";
		logger.info("visible for--->>>  " + visibleFor);
		logger.info("free for--->>>  " + freeFor);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat desiredDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

		String offeringIDs = ConfigurationManager.getConfigurationFromCache("network.settings.offeringIds");
		ArrayList<String> offeringIdsList = new ArrayList<String>(Arrays.asList(offeringIDs.split(",")));
		List<CoreServicesCategory> listCoreServicesCategory = getCoreServicesCategoryListFromCacheMap(lang, freeFor);
		if (listCoreServicesCategory != null && listCoreServicesCategory.size() > 0) {
			for (CoreServicesCategory coreServicesCategory : listCoreServicesCategory) {

				List<CoreServicesCategoryItem> listOfCoreServicesCategoryItems = coreServicesCategory.getListOfCoreServicesCategoryItem();
				logger.info("<<<<<<<<<<<< listOfCoreServicesCategoryItems size >>>>>>>>>>>>" + listOfCoreServicesCategoryItems.size());
				List<CoreServicesCategoryItem> resultList = new ArrayList<CoreServicesCategoryItem>();
				if (listOfCoreServicesCategoryItems != null && listOfCoreServicesCategoryItems.size() > 0) {
					for (int i = 0; i < listOfCoreServicesCategoryItems.size(); i++) {
						logger.info("<<<<<<< getSupplementaryOfferingList size:" + data.getSupplementaryOfferingList().getGetSubOfferingInfo().size());

						for (GetSubOfferingInfo oneOffer : data.getSupplementaryOfferingList().getGetSubOfferingInfo()) {
							// CoreServicesCategoryItem coreServicesCategoryItem
							// = listOfCoreServicesCategoryItems.get(i);
							if (listOfCoreServicesCategoryItems.get(i).getOfferingId().equals(oneOffer.getOfferingId().getOfferingId())) {
								listOfCoreServicesCategoryItems.get(i).setStatus(ConfigurationManager.getConfigurationFromCache(ConfigurationManager.MAPPING_CRM_STATUS + oneOffer.getStatus()));

								QueryOfferingRentCycleResultMsg queryOfferingRentCycleResultMsg = null;
								QueryOfferingRentCycleRequestMsg qORCR = new QueryOfferingRentCycleRequestMsg();
								qORCR.setRequestHeader(BcService.getRequestHeader());
								QueryOfferingRentCycleRequest qORC = new QueryOfferingRentCycleRequest();
								OfferingInst of = new OfferingInst();
								OfferingKey offeringKey = new OfferingKey();
								offeringKey.setOfferingID(new BigInteger(oneOffer.getOfferingId().getOfferingId()));
								of.setOfferingKey(offeringKey);
								OfferingOwner oO = new OfferingOwner();
								SubAccessCode subACode = new SubAccessCode();
								subACode.setPrimaryIdentity(data.getServiceNumber());
								oO.setSubAccessCode(subACode);
								of.setOfferingOwner(oO);
								qORC.getOfferingInst().add(of);
								qORCR.setQueryOfferingRentCycleRequest(qORC);
								queryOfferingRentCycleResultMsg = BcService.getInstance().queryOfferingRentCycle(qORCR);

								if (queryOfferingRentCycleResultMsg.getResultHeader().getResultCode().equalsIgnoreCase("0")) {
									if (queryOfferingRentCycleResultMsg.getQueryOfferingRentCycleResult().getOfferingRentCycle().size() > 0) {
										effctive = queryOfferingRentCycleResultMsg.getQueryOfferingRentCycleResult().getOfferingRentCycle().get(0).getOpenDay();
										expire = queryOfferingRentCycleResultMsg.getQueryOfferingRentCycleResult().getOfferingRentCycle().get(0).getEndDay();
									}
								}

								try {
									String effectiveDate = dateFormat.format(desiredDateFormat.parse(effctive)) + " 00:00:00";
									listOfCoreServicesCategoryItems.get(i).setEffectiveDate(effectiveDate);

									String expireDate = dateFormat.format(desiredDateFormat.parse(expire)) + " 00:00:00";
									listOfCoreServicesCategoryItems.get(i).setExpireDate(expireDate);

									listOfCoreServicesCategoryItems.get(i).setExpireDate(getNewEffectiveDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(effectiveDate), listOfCoreServicesCategoryItems.get(i).getValidity(), listOfCoreServicesCategoryItems.get(i).getOfferingId()));

									ArrayList<String> arrayList = new ArrayList<>();
									arrayList.add("560890134");
									arrayList.add("1360897046");
									arrayList.add("1660897633");
									arrayList.add("1460968964");
									arrayList.add("1360968579");
									arrayList.add("1660970156");
									if (accountType.equalsIgnoreCase("Corporate"))
										for (int z = 0; z < arrayList.size(); z++)
											if (listOfCoreServicesCategoryItems.get(i).getOfferingId().equalsIgnoreCase(arrayList.get(z))) {
												listOfCoreServicesCategoryItems.get(i).setEffectiveDate(null);
												listOfCoreServicesCategoryItems.get(i).setExpireDate(null);
											}

								}
								catch (Exception e) {
									e.printStackTrace();
								}

								String forwardNumber = "";
								for (ExtParameterInfo exParm : oneOffer.getExtParamList().getParameterInfo()) {
									if (exParm.getParamName().equals(Constants.EXTERNAL_PARAMETER_INFO_PARAM_NAME)) {
										forwardNumber = exParm.getParamValue();
										listOfCoreServicesCategoryItems.get(i).setForwardNumber(forwardNumber);
									}
								}
							}
						}

						// START OF Adding logic to check NetworkSettings Core
						// Services
						if (isFrom != null && !isFrom.isEmpty() && isFrom.equalsIgnoreCase("B2B")) {

							for (GetSubProductInfo oneOffer1 : getsubproductinfo) {
								// CoreServicesCategoryItem
								// coreServicesCategoryItem
								// = listOfCoreServicesCategoryItems.get(i);
								if (listOfCoreServicesCategoryItems.get(i).getOfferingId().equals(oneOffer1.getProductId())) {
									listOfCoreServicesCategoryItems.get(i).setStatus(ConfigurationManager.getConfigurationFromCache(ConfigurationManager.MAPPING_CRM_STATUS + oneOffer1.getStatus()));

									logger.info("*********************   GET NETWORK SETTINGS OFFERING IN IF *************** " + listOfCoreServicesCategoryItems.get(i).getOfferingId());
									logger.info("*********************   GET NETWORK SETTINGS STATUS IN IF *************** " + ConfigurationManager.getConfigurationFromCache(ConfigurationManager.MAPPING_CRM_STATUS + oneOffer1.getStatus()));
									logger.info("*********************   GET NETWORK SETTINGS PRODUCT NAME *************** " + oneOffer1.getProductName());
									logger.info("*********************   GET NETWORK SETTINGS PRODUCT ID *************** " + oneOffer1.getProductId());

									// START of Commented Lines by saboor, to
									// skip flow of QueryLifecycle as we doon,y
									// need effectiveDate, ExpiryDate
									/*
									 * QueryOfferingRentCycleResultMsg
									 * queryOfferingRentCycleResultMsg = null;
									 * QueryOfferingRentCycleRequestMsg qORCR =
									 * new QueryOfferingRentCycleRequestMsg();
									 * qORCR.setRequestHeader(BcService.
									 * getRequestHeader());
									 * QueryOfferingRentCycleRequest qORC = new
									 * QueryOfferingRentCycleRequest();
									 * OfferingInst of = new OfferingInst();
									 * OfferingKey offeringKey = new
									 * OfferingKey();
									 * offeringKey.setOfferingID(new
									 * BigInteger(oneOffer.getProductId()));
									 * of.setOfferingKey(offeringKey);
									 * OfferingOwner oO = new OfferingOwner();
									 * SubAccessCode subACode = new
									 * SubAccessCode();
									 * subACode.setPrimaryIdentity(data.
									 * getServiceNumber());
									 * oO.setSubAccessCode(subACode);
									 * of.setOfferingOwner(oO);
									 * qORC.getOfferingInst().add(of);
									 * qORCR.setQueryOfferingRentCycleRequest(
									 * qORC); queryOfferingRentCycleResultMsg =
									 * BcService.getInstance().
									 * queryOfferingRentCycle(qORCR);
									 * 
									 * if (queryOfferingRentCycleResultMsg.
									 * getResultHeader().getResultCode()
									 * .equalsIgnoreCase("0")) { if
									 * (queryOfferingRentCycleResultMsg.
									 * getQueryOfferingRentCycleResult()
									 * .getOfferingRentCycle().size() > 0) {
									 * effctive =
									 * queryOfferingRentCycleResultMsg.
									 * getQueryOfferingRentCycleResult()
									 * .getOfferingRentCycle().get(0).getOpenDay
									 * (); expire =
									 * queryOfferingRentCycleResultMsg.
									 * getQueryOfferingRentCycleResult()
									 * .getOfferingRentCycle().get(0).getEndDay(
									 * ); } }
									 * 
									 * try { String effectiveDate =
									 * dateFormat.format(desiredDateFormat.parse
									 * (effctive)) + " 00:00:00";
									 * listOfCoreServicesCategoryItems.get(i).
									 * setEffectiveDate(effectiveDate);
									 * 
									 * String expireDate =
									 * dateFormat.format(desiredDateFormat.parse
									 * (expire)) + " 00:00:00";
									 * listOfCoreServicesCategoryItems.get(i).
									 * setExpireDate(expireDate);
									 */
									// end of Commented Lines by saboor, to skip
									// flow of QueryLifecycle as we doon,y need
									// effectiveDate, ExpiryDate

									// listOfCoreServicesCategoryItems.get(i)
									// .setExpireDate(getNewEffectiveDate(
									// new SimpleDateFormat("yyyy-MM-dd
									// HH:mm:ss").parse(effectiveDate),
									// listOfCoreServicesCategoryItems.get(i).getValidity(),
									// listOfCoreServicesCategoryItems.get(i).getOfferingId()));

									/*
									 * } catch (Exception e) {
									 * e.printStackTrace(); }
									 */

									/*
									 * String forwardNumber = ""; for
									 * (ExtParameterInfo exParm :
									 * oneOffer.getExtParamList().
									 * getParameterInfo()) { if
									 * (exParm.getParamName().equals(Constants.
									 * EXTERNAL_PARAMETER_INFO_PARAM_NAME)) {
									 * forwardNumber = exParm.getParamValue();
									 * listOfCoreServicesCategoryItems.get(i).
									 * setForwardNumber(forwardNumber); } }
									 */
								}
							}
						}

						// END OF Adding logic to check NetWork Settings Core
						// Services

						logger.info("<<<<<<< FREE FOR " + freeFor + "-------" + listOfCoreServicesCategoryItems.get(i).getFreeFor() + " >>>>>>>" + coreServicesCategory.getName() + "   " + listOfCoreServicesCategoryItems.get(i).getName());
						if (freeFor != null && listOfCoreServicesCategoryItems.get(i).getFreeFor() != null) {
							String[] freeForTokens = listOfCoreServicesCategoryItems.get(i).getFreeFor().split(",");
							if (freeForTokens != null && freeForTokens.length > 0) {
								boolean free = false;
								for (int j = 0; j < freeForTokens.length; j++) {
									String ff = freeForTokens[j];
									logger.info("freeFor--->>> " + freeFor);
									logger.info("ff--->>> " + ff);
									if (freeFor.equalsIgnoreCase(ff)) {
										logger.info("Matched.....");
										logger.info("             ");
										free = true;
										break;
									}
									else {
										free = false;
									}
								}
								if (free) {
									listOfCoreServicesCategoryItems.get(i).setPrice("0.0");
								}
							}
						}
						logger.info("<<<<<< Visible for: " + visibleFor + "-------- " + listOfCoreServicesCategoryItems.get(i).getVisibleFor() + " >>>>>>");
						if (visibleFor != null && listOfCoreServicesCategoryItems.get(i).getVisibleFor() != null) {
							String[] visibleForTokens = listOfCoreServicesCategoryItems.get(i).getVisibleFor().split(",");
							logger.info("visible token size is:" + visibleForTokens.length);
							if (visibleForTokens != null && visibleForTokens.length > 0) {
								boolean visible = false;
								for (int j = 0; j < visibleForTokens.length; j++) {
									String vf = visibleForTokens[j];
									logger.info("<<<<<<< VF from token is: " + vf + ">>>>>>>");
									if (vf.equalsIgnoreCase(visibleFor)) {
										visible = true;
										break;
									}
									else {
										visible = false;
									}
								}
								if (visible) {
									if ((isFrom == null || isFrom.isEmpty() || !isFrom.equalsIgnoreCase("B2B")) && (offeringIdsList.contains(listOfCoreServicesCategoryItems.get(i).getOfferingId()))) {

									}
									else {
										resultList.add(listOfCoreServicesCategoryItems.get(i));
									}
								}
							}
						}
					}
				}
				if (resultList != null && resultList.size() > 0) {

					coreServicesList.add(new CoreServices(coreServicesCategory.getName(), (ArrayList<CoreServicesCategoryItem>) resultList));
				}

			} // her end for loop
		}

		return coreServicesList;
	}

	public ArrayList<CoreServices> getCoreServicesV2(String lang) {
		ArrayList<CoreServices> coreServicesList = new ArrayList<CoreServices>();
		new SimpleDateFormat("yyyy-MM-dd");
		new SimpleDateFormat("yyyyMMddHHmmss");

		List<CoreServicesCategory> listCoreServicesCategory = getCoreServicesCategoryListFromCacheMapV2(lang);
		if (listCoreServicesCategory != null && listCoreServicesCategory.size() > 0) {
			for (CoreServicesCategory coreServicesCategory : listCoreServicesCategory) {
				List<CoreServicesCategoryItem> listOfCoreServicesCategoryItems = coreServicesCategory.getListOfCoreServicesCategoryItem();
				logger.info("<<<<<<<<<<<< listOfCoreServicesCategoryItems size >>>>>>>>>>>>" + listOfCoreServicesCategoryItems.size());
				List<CoreServicesCategoryItem> resultList = new ArrayList<CoreServicesCategoryItem>();
				if (listOfCoreServicesCategoryItems != null && listOfCoreServicesCategoryItems.size() > 0) {

					for (int i = 0; i < listOfCoreServicesCategoryItems.size(); i++) {
						resultList.add(listOfCoreServicesCategoryItems.get(i));
					}
					/*
					 * logger.info("<<<<<<< getSupplementaryOfferingList size:"
					 * +
					 * data.getSupplementaryOfferingList().getGetSubOfferingInfo
					 * ().size()); for (GetSubOfferingInfo oneOffer :
					 * data.getSupplementaryOfferingList()
					 * .getGetSubOfferingInfo()) { // CoreServicesCategoryItem
					 * coreServicesCategoryItem // =
					 * listOfCoreServicesCategoryItems.get(i); if
					 * (listOfCoreServicesCategoryItems.get(i).getOfferingId()
					 * .equals(oneOffer.getOfferingId().getOfferingId())) {
					 * listOfCoreServicesCategoryItems.get(i)
					 * .setStatus(ConfigurationManager.
					 * getConfigurationFromCache(
					 * ConfigurationManager.MAPPING_CRM_STATUS +
					 * oneOffer.getStatus()));
					 * 
					 * QueryOfferingRentCycleResultMsg
					 * queryOfferingRentCycleResultMsg = null;
					 * QueryOfferingRentCycleRequestMsg qORCR = new
					 * QueryOfferingRentCycleRequestMsg();
					 * qORCR.setRequestHeader(BcService.getRequestHeader());
					 * QueryOfferingRentCycleRequest qORC = new
					 * QueryOfferingRentCycleRequest(); OfferingInst of = new
					 * OfferingInst(); OfferingKey offeringKey = new
					 * OfferingKey(); offeringKey.setOfferingID(new
					 * BigInteger(oneOffer.getOfferingId().getOfferingId()));
					 * of.setOfferingKey(offeringKey); OfferingOwner oO = new
					 * OfferingOwner(); SubAccessCode subACode = new
					 * SubAccessCode();
					 * subACode.setPrimaryIdentity(data.getServiceNumber());
					 * oO.setSubAccessCode(subACode); of.setOfferingOwner(oO);
					 * qORC.getOfferingInst().add(of);
					 * qORCR.setQueryOfferingRentCycleRequest(qORC);
					 * queryOfferingRentCycleResultMsg =
					 * BcService.getInstance().queryOfferingRentCycle(qORCR);
					 * 
					 * if (queryOfferingRentCycleResultMsg.getResultHeader().
					 * getResultCode() .equalsIgnoreCase("0")) { if
					 * (queryOfferingRentCycleResultMsg.
					 * getQueryOfferingRentCycleResult()
					 * .getOfferingRentCycle().size() > 0) { effctive =
					 * queryOfferingRentCycleResultMsg.
					 * getQueryOfferingRentCycleResult()
					 * .getOfferingRentCycle().get(0).getOpenDay(); expire =
					 * queryOfferingRentCycleResultMsg.
					 * getQueryOfferingRentCycleResult()
					 * .getOfferingRentCycle().get(0).getEndDay(); } }
					 * 
					 * try { String effectiveDate =
					 * dateFormat.format(desiredDateFormat.parse(effctive)) +
					 * " 00:00:00";
					 * listOfCoreServicesCategoryItems.get(i).setEffectiveDate(
					 * effectiveDate);
					 * 
					 * String expireDate =
					 * dateFormat.format(desiredDateFormat.parse(expire)) +
					 * " 00:00:00";
					 * listOfCoreServicesCategoryItems.get(i).setExpireDate(
					 * expireDate);
					 * 
					 * // listOfCoreServicesCategoryItems.get(i) //
					 * .setExpireDate(getNewEffectiveDate( // new
					 * SimpleDateFormat("yyyy-MM-dd //
					 * HH:mm:ss").parse(effectiveDate), //
					 * listOfCoreServicesCategoryItems.get(i).getValidity(), //
					 * listOfCoreServicesCategoryItems.get(i).getOfferingId()));
					 * 
					 * } catch (Exception e) { e.printStackTrace(); }
					 * 
					 * String forwardNumber = ""; for (ExtParameterInfo exParm :
					 * oneOffer.getExtParamList().getParameterInfo()) { if
					 * (exParm.getParamName().equals(Constants.
					 * EXTERNAL_PARAMETER_INFO_PARAM_NAME)) { forwardNumber =
					 * exParm.getParamValue();
					 * listOfCoreServicesCategoryItems.get(i).setForwardNumber(
					 * forwardNumber); } } } }
					 * 
					 * logger.info("<<<<<<< FREE FOR " + freeFor + "-------" +
					 * listOfCoreServicesCategoryItems.get(i).getFreeFor() +
					 * " >>>>>>>" + coreServicesCategory.getName() + "   " +
					 * listOfCoreServicesCategoryItems.get(i).getName()); if (
					 * freeFor != null &&
					 * listOfCoreServicesCategoryItems.get(i).getFreeFor() !=
					 * null) { String[] freeForTokens =
					 * listOfCoreServicesCategoryItems.get(i).getFreeFor().split
					 * (","); if (freeForTokens != null && freeForTokens.length
					 * > 0) { boolean free = true;
					 * 
					 * if (free) {
					 * listOfCoreServicesCategoryItems.get(i).setPrice("0.0"); }
					 * } } if ( visibleFor != null &&
					 * listOfCoreServicesCategoryItems.get(i) .getVisibleFor()
					 * != null) { String[] visibleForTokens =
					 * listOfCoreServicesCategoryItems.get(i).getVisibleFor()
					 * .split(","); logger.info("visible token size is:" +
					 * visibleForTokens.length); if (visibleForTokens != null &&
					 * visibleForTokens.length > 0) { boolean visible = true;
					 * 
					 * if (visible) {
					 * resultList.add(listOfCoreServicesCategoryItems.get(i)); }
					 * } }
					 * 
					 * }
					 */
				}
				if (resultList != null && resultList.size() > 0) {
					coreServicesList.add(new CoreServices(coreServicesCategory.getName(), (ArrayList<CoreServicesCategoryItem>) resultList));
				}
			}
		}
		return coreServicesList;
	}

	private String getNewEffectiveDate(Date date, String ValidityType, String OfferingID) {
		logger.debug("OfferingID: " + OfferingID + "-Old Effective Date" + date + "- Validity Type:" + ValidityType);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();

		cal.setTime(date);
		if (ValidityType.trim().toLowerCase().equalsIgnoreCase("daily") || ValidityType.trim().equalsIgnoreCase("Ежедневно") || ValidityType.trim().equalsIgnoreCase("Gündəlik"))
			cal.add(Calendar.DATE, 1);
		if (ValidityType.trim().toLowerCase().equalsIgnoreCase("weekly") || ValidityType.trim().equalsIgnoreCase("Еженедельно") || ValidityType.trim().equalsIgnoreCase("Həftəlik"))
			cal.add(Calendar.DATE, 7);
		if (ValidityType.trim().toLowerCase().equalsIgnoreCase("15 days") || ValidityType.trim().equalsIgnoreCase("15 дней") || ValidityType.trim().equalsIgnoreCase("15 gün"))
			cal.add(Calendar.DATE, 15);
		if (ValidityType.trim().toLowerCase().equalsIgnoreCase("monthly") || ValidityType.trim().equalsIgnoreCase("Ежемесячно") || ValidityType.trim().equalsIgnoreCase("Aylıq"))
			cal.add(Calendar.DATE, 30);

		Date newDate = cal.getTime();
		logger.debug("New Effective Date" + sdf.format(newDate) + " 00:00:00");
		return sdf.format(newDate) + " 00:00:00";

	}

	private List<CoreServicesCategory> getListOfCoreServicesCategory(String lang) {
		String[] checker = lang.split(",");
		List<CoreServicesCategory> listOfCoreServicesCategory = getCoreServicesCategoryList(checker[0]);
		try {
			if (listOfCoreServicesCategory != null && listOfCoreServicesCategory.size() > 0) {
				for (CoreServicesCategory coreServicesCategory : listOfCoreServicesCategory) {
					List<CoreServicesCategoryItem> listOfCoreServicesCategoryItem = getCoreServicesCategoryItemList(coreServicesCategory.getId(), lang);
					if (listOfCoreServicesCategoryItem != null && listOfCoreServicesCategoryItem.size() > 0) {
						coreServicesCategory.setListOfCoreServicesCategoryItem(listOfCoreServicesCategoryItem);
					}
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return listOfCoreServicesCategory;
	}

	private static List<CoreServicesCategory> getCoreServicesCategoryList(String stId) {
		logger.info("<<<<<<<<<<<<<<<<<<<<< Generating cache with STID: " + stId);
		List<CoreServicesCategory> listOfCoreServicesCategory = new ArrayList<CoreServicesCategory>();
		try (Connection con = DBFactory.getMagentoDBConnection();) {
			// Connection con = DBFactory.getMagentoDBConnection();
			String query = "";
			PreparedStatement pstmt = null;
			/*
			 * if (stId.equals("bulk")) { query =
			 * "select * from coreservice_category where store_id=?  order by sort_order "
			 * ; pstmt = con.prepareStatement(query); // create a statement
			 * pstmt.setString(1, stId); } else {
			 */
			query = "select * from coreservice_category where store_id=? and status = 1  order by sort_order ";
			pstmt = con.prepareStatement(query); // create a statement
			pstmt.setString(1, stId);
			// /}

			logger.info("<<<<<<<< Query >>>>>>>>" + pstmt.toString());
			ResultSet rs = pstmt.executeQuery();
			// extract data from the ResultSet
			while (rs.next()) {
				Long id = rs.getLong(1);
				String name = rs.getString(2);
				Integer status = rs.getInt(3);
				String storeId = rs.getString(4);
				Integer sortOrder = rs.getInt(5);
				CoreServicesCategory csc = new CoreServicesCategory(id, name, status, storeId, sortOrder);
				logger.info("<<<<<<<<<< CSC >>>>>>>>>>" + csc.toString());
				listOfCoreServicesCategory.add(csc);
			}
			pstmt.close();
			rs.close();
		}
		catch (Exception e) {
			logger.error("EXCEPTION:", e);
		}
		logger.info("number of core services" + listOfCoreServicesCategory.size());
		return listOfCoreServicesCategory;
	}

	private static List<CoreServicesCategoryItem> getCoreServicesCategoryItemList(Long cid, String stId) {

		List<CoreServicesCategoryItem> listOfCoreServicesCategoryItem = new ArrayList<CoreServicesCategoryItem>();
		try (Connection con = DBFactory.getMagentoDBConnection();) {
			String[] checker = stId.split(",");
			// Connection con = DBFactory.getMagentoDBConnection();
			String query = "";
			logger.info("<<<< STID is: >>>>" + stId);
			PreparedStatement pstmt = null;
			if (checker[1].equals("V2")) {
				query = "select * from coreservice_item where category_id=? and status=? AND NAME NOT LIKE (?) AND NAME NOT LIKE (?) AND NAME NOT LIKE (?) order by sort_order ";
				pstmt = con.prepareStatement(query); // create a
				// statement
				pstmt.setLong(1, cid);
				pstmt.setInt(2, 1);
				pstmt.setString(3, "%7%");
				pstmt.setString(4, "%15%");
				pstmt.setString(5, "%30%");
			}
			else {
				query = "select * from coreservice_item where category_id=? and store_id=? and status=?  order by sort_order ";
				pstmt = con.prepareStatement(query); // create a
														// statement
				pstmt.setLong(1, cid);
				pstmt.setString(2, checker[0]);
				pstmt.setInt(3, 1);
			}
			logger.info("<<<<<<<< Query for categorty item list >>>>>>>>" + pstmt.toString());
			ResultSet rs = pstmt.executeQuery();
			// extract data from the ResultSet
			while (rs.next()) {
				Long id = rs.getLong(1);
				String name = rs.getString(2);
				String description = rs.getString(3);
				Long categoryId = rs.getLong(4);
				String price = rs.getString(5);
				String validity = rs.getString(6);
				String offeringId = rs.getString(7);
				String storeId = rs.getString(9);
				Integer sortOrder = rs.getInt(10);
				Integer renewable = rs.getInt(11);
				String freeFor = rs.getString(12);
				String visibleFor = rs.getString(13);
				CoreServicesCategoryItem categoryItem = new CoreServicesCategoryItem(id, name, description, categoryId, price, validity, offeringId, storeId, sortOrder, "", renewable, freeFor, visibleFor);
				listOfCoreServicesCategoryItem.add(categoryItem);
				logger.info("<<<<<<<<< CATEGORY ITEM: >>>>>>>>>" + categoryItem.toString());
			}
			pstmt.close();
			rs.close();
			// con.close();

		}
		catch (Exception e) {
			logger.error(e);
		}

		return listOfCoreServicesCategoryItem;
	}

	public GetSubscriberResponse GetSubscriberRequest(String msisdn) {
		GetSubscriberRequest getSubReq = new GetSubscriberRequest();
		GetSubscriberIn getsubIn = new GetSubscriberIn();
		getsubIn.setServiceNumber(msisdn);
		getsubIn.setIncludeOfferFlag(ConfigurationManager.getNGBSSAPIProperties("crm.sub.IncludeOfferFlag").trim());

		/*
		 * getsubIn.setIncludeOfferFlag(ConfigurationManager.
		 * getConfigurationFromCache("crm.sub.IncludeOfferFlag").trim());
		 */
		getSubReq.setGetSubscriberBody(getsubIn);
		getSubReq.setRequestHeader(getRequestHeader());
		com.huawei.crm.query.GetSubscriberResponse response = CRMServices.getInstance().getSubscriberData(getSubReq);
		return response;
	}

	private RequestHeader getRequestHeader() {
		RequestHeader reqh = new RequestHeader();
		reqh.setChannelId(ConfigurationManager.getConfigurationFromCache("crm.sub.ChannelId").trim());
		reqh.setTechnicalChannelId(ConfigurationManager.getConfigurationFromCache("crm.sub.TechnicalChannelId").trim());
		reqh.setAccessUser(ConfigurationManager.getConfigurationFromCache("crm.sub.AccessUser").trim());
		reqh.setTenantId(ConfigurationManager.getConfigurationFromCache("crm.sub.TenantId").trim());
		reqh.setAccessPwd(ConfigurationManager.getConfigurationFromCache("crm.sub.AccessPwd").trim());
		reqh.setTestFlag(ConfigurationManager.getConfigurationFromCache("crm.sub.TestFlag").trim());
		reqh.setLanguage(ConfigurationManager.getConfigurationFromCache("crm.sub.Language").trim());
		reqh.setTransactionId(Helper.generateTransactionID());
		return reqh;
	}
}
