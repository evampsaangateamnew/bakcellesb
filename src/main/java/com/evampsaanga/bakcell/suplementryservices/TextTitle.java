package com.evampsaanga.bakcell.suplementryservices;

public class TextTitle {
	private String textWithTitleLabel;
	private String textWithTitleDescription;

	public String getTextWithTitleLabel() {
		return textWithTitleLabel;
	}

	public void setTextWithTitleLabel(String textWithTitleLabel) {
		this.textWithTitleLabel = textWithTitleLabel;
	}

	public String getTextWithTitleDescription() {
		return textWithTitleDescription;
	}

	public void setTextWithTitleDescription(String textWithTitleDescription) {
		this.textWithTitleDescription = textWithTitleDescription;
	}

	@Override
	public String toString() {
		return "ClassPojo [textWithTitleLabel = " + textWithTitleLabel + ", textWithTitleDescription = "
				+ textWithTitleDescription + "]";
	}
}