
package https.www_e_gov;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the https.www_e_gov package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: https.www_e_gov
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CheckVoenValidResponse }
     * 
     */
    public CheckVoenValidResponse createCheckVoenValidResponse() {
        return new CheckVoenValidResponse();
    }

    /**
     * Create an instance of {@link CheckVoenValid }
     * 
     */
    public CheckVoenValid createCheckVoenValid() {
        return new CheckVoenValid();
    }

    /**
     * Create an instance of {@link AddApplicationResponse }
     * 
     */
    public AddApplicationResponse createAddApplicationResponse() {
        return new AddApplicationResponse();
    }

    /**
     * Create an instance of {@link GetSunlistByVoen }
     * 
     */
    public GetSunlistByVoen createGetSunlistByVoen() {
        return new GetSunlistByVoen();
    }

    /**
     * Create an instance of {@link GetSunlistByVoenResponse }
     * 
     */
    public GetSunlistByVoenResponse createGetSunlistByVoenResponse() {
        return new GetSunlistByVoenResponse();
    }

    /**
     * Create an instance of {@link CheckDocumentValid }
     * 
     */
    public CheckDocumentValid createCheckDocumentValid() {
        return new CheckDocumentValid();
    }

    /**
     * Create an instance of {@link CheckDocumentValidResponse }
     * 
     */
    public CheckDocumentValidResponse createCheckDocumentValidResponse() {
        return new CheckDocumentValidResponse();
    }

    /**
     * Create an instance of {@link AddApplication }
     * 
     */
    public AddApplication createAddApplication() {
        return new AddApplication();
    }

}
