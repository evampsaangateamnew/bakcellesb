package com.evampsaanga.bakcell.notificationadmin;

import com.evampsaanga.bakcell.requestheaders.BaseRequest;
import com.fasterxml.jackson.annotation.JsonInclude;

public class SavePicNotificationRequest extends BaseRequest {
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String id;
	private String titleAz;
	private String titleEn;
	private String titleRu;
	private String messageAz;
	private String messageEn;
	private String messageRu;
	private String icon;
	private String dateTime;
	private String actionType;
	private String actionId;
	private String buttonText;
	private String language;
	private String fileName;
	private String file;
	private String confirm;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitleAz() {
		return titleAz;
	}

	public void setTitleAz(String titleAz) {
		this.titleAz = titleAz;
	}

	public String getTitleEn() {
		return titleEn;
	}

	public void setTitleEn(String titleEn) {
		this.titleEn = titleEn;
	}

	public String getTitleRu() {
		return titleRu;
	}

	public void setTitleRu(String titleRu) {
		this.titleRu = titleRu;
	}

	public String getMessageAz() {
		return messageAz;
	}

	public void setMessageAz(String messageAz) {
		this.messageAz = messageAz;
	}

	public String getMessageEn() {
		return messageEn;
	}

	public void setMessageEn(String messageEn) {
		this.messageEn = messageEn;
	}

	public String getMessageRu() {
		return messageRu;
	}

	public void setMessageRu(String messageRu) {
		this.messageRu = messageRu;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getActionId() {
		return actionId;
	}

	public void setActionId(String actionId) {
		this.actionId = actionId;
	}

	public String getButtonText() {
		return buttonText;
	}

	public void setButtonText(String buttonText) {
		this.buttonText = buttonText;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@Override
	public String toString() {
		return "SavePicNotificationRequest [id=" + id + ", titleAz=" + titleAz + ", titleEn=" + titleEn + ", titleRu="
				+ titleRu + ", messageAz=" + messageAz + ", messageEn=" + messageEn + ", messageRu=" + messageRu
				+ ", icon=" + icon + ", dateTime=" + dateTime + ", actionType=" + actionType + ", actionId=" + actionId
				+ ", buttonText=" + buttonText + ", language=" + language + ", fileName=" + fileName + ", file=" + file
				+ ", confirm=" + confirm + ", toString()=" + super.toString() + "]";
	}

	public String getConfirm() {
		return confirm;
	}

	public void setConfirm(String confirm) {
		this.confirm = confirm;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}
}
