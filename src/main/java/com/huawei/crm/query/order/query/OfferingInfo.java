
package com.huawei.crm.query.order.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.order.query.OfferingKey;


/**
 * <p>Java class for OfferingInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OfferingInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OfferingID" type="{http://crm.huawei.com/basetype/}OfferingKey" minOccurs="0"/>
 *         &lt;element name="OfferingName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OfferingStatus" type="{http://crm.huawei.com/basetype/}OfferingStatus" minOccurs="0"/>
 *         &lt;element name="ProductList" type="{http://crm.huawei.com/query/}ProductInstList" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OfferingInfo", propOrder = {
    "offeringID",
    "offeringName",
    "offeringStatus",
    "productList"
})
public class OfferingInfo {

    @XmlElement(name = "OfferingID")
    protected OfferingKey offeringID;
    @XmlElement(name = "OfferingName")
    protected String offeringName;
    @XmlElement(name = "OfferingStatus")
    protected String offeringStatus;
    @XmlElement(name = "ProductList")
    protected ProductInstList productList;

    /**
     * Gets the value of the offeringID property.
     * 
     * @return
     *     possible object is
     *     {@link OfferingKey }
     *     
     */
    public OfferingKey getOfferingID() {
        return offeringID;
    }

    /**
     * Sets the value of the offeringID property.
     * 
     * @param value
     *     allowed object is
     *     {@link OfferingKey }
     *     
     */
    public void setOfferingID(OfferingKey value) {
        this.offeringID = value;
    }

    /**
     * Gets the value of the offeringName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfferingName() {
        return offeringName;
    }

    /**
     * Sets the value of the offeringName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfferingName(String value) {
        this.offeringName = value;
    }

    /**
     * Gets the value of the offeringStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfferingStatus() {
        return offeringStatus;
    }

    /**
     * Sets the value of the offeringStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfferingStatus(String value) {
        this.offeringStatus = value;
    }

    /**
     * Gets the value of the productList property.
     * 
     * @return
     *     possible object is
     *     {@link ProductInstList }
     *     
     */
    public ProductInstList getProductList() {
        return productList;
    }

    /**
     * Sets the value of the productList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductInstList }
     *     
     */
    public void setProductList(ProductInstList value) {
        this.productList = value;
    }

}
