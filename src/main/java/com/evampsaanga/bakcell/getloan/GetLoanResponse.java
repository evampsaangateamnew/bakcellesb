package com.evampsaanga.bakcell.getloan;

import com.evampsaanga.bakcell.responseheaders.BaseResponse;

public class GetLoanResponse extends BaseResponse {
	private String newBalance = "";
	private String newCredit = "";

	/**
	 * @return the newBalance
	 */
	public String getNewBalance() {
		return newBalance;
	}

	/**
	 * @param newBalance
	 *            the newBalance to set
	 */
	public void setNewBalance(String newBalance) {
		this.newBalance = newBalance;
	}

	public String getNewCredit() {
		return newCredit;
	}

	public void setNewCredit(String newCredit) {
		this.newCredit = newCredit;
	}
}
