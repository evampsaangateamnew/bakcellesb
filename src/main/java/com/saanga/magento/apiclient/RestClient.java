package com.saanga.magento.apiclient;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.log4j.Logger;

import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.developer.utils.Helper;

/**
 * Class to implement Rest Client for Magento Services and Future integrations
 * will be added here too.
 * 
 * @author Adeel
 * 
 */
public class RestClient {
	public static final Logger logger = Logger.getLogger("bakcellLogs-V2");

	public static String SendCallToMagento(String url, String input) throws Exception {
		logger.info("Rest Client: URL: " + url);
		logger.info("Rest Client: Request: " + input);
		url = ConfigurationManager.getConfigurationFromCache("magento.app.baseurl") + url;
		if (url.startsWith("http") && !url.startsWith("https")) {
			HttpURLConnection connection = null;
			URL urlCon = new URL(url);
			connection = (HttpURLConnection) urlCon.openConnection();
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			// connection.setRequestProperty("credentials","RQ/Rjcf8Gn2+7srAiXNO0ZGuRnJORSir");
			connection.setRequestProperty("User-Agent", "ESB");
			connection.setDoOutput(true);
			DataOutputStream writer = new DataOutputStream(connection.getOutputStream());
			writer.write(input.getBytes(StandardCharsets.UTF_8));
			String responseString = myInputStreamReader(connection.getInputStream());
			logger.info("Rest Client: Response: " + responseString);
			return responseString;
		} else if (url.startsWith("https")) {
			HttpsURLConnection ssl = null;
			// HttpURLConnection connection = null;
			URL urlCon = new URL(url);
			ssl = (HttpsURLConnection) urlCon.openConnection();
			ssl.setConnectTimeout(5000);
			ssl.setRequestMethod("POST");
			ssl.setRequestProperty("Content-Type", "application/json");
			// connection.setRequestProperty("credentials","RQ/Rjcf8Gn2+7srAiXNO0ZGuRnJORSir");
			ssl.setRequestProperty("User-Agent", "ESB");
			ssl.setDoOutput(true);
			DataOutputStream writer = new DataOutputStream(ssl.getOutputStream());
			writer.write(input.getBytes(StandardCharsets.UTF_8));
			String responseString = myInputStreamReader(ssl.getInputStream());
			System.out.println("response : " + responseString);
			return responseString;
		}
		return input;
	}

	public static String SendCallEMobile(String hashCode, String input) throws Exception {

		String url = ConfigurationManager.getConfigurationFromCache("ulduzum.subscriber.codegenerate.url") + hashCode;
		logger.info("Rest Client: URL: " + url);
		logger.info("Rest Client: Request: " + input);
		if (url.startsWith("http") && !url.startsWith("https")) {
			HttpURLConnection connection = null;
			URL urlCon = new URL(url);
			connection = (HttpURLConnection) urlCon.openConnection();
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("POST");
			// connection.setRequestProperty("Content-Type",
			// "application/json");
			// connection.setRequestProperty("credentials","RQ/Rjcf8Gn2+7srAiXNO0ZGuRnJORSir");
			// connection.setRequestProperty("User-Agent", "ESB");
			connection.setDoOutput(true);
			DataOutputStream writer = new DataOutputStream(connection.getOutputStream());
			writer.write(input.getBytes(StandardCharsets.UTF_8));
			String responseString = myInputStreamReader(connection.getInputStream());
			logger.info("Rest Client: Response: " + responseString);
			return responseString;
		} else if (url.startsWith("https")) {
			HttpsURLConnection ssl = null;
			// HttpURLConnection connection = null;
			URL urlCon = new URL(url);
			ssl = (HttpsURLConnection) urlCon.openConnection();
			ssl.setConnectTimeout(5000);
			ssl.setRequestMethod("POST");
			// ssl.setRequestProperty("Content-Type", "application/json");
			// connection.setRequestProperty("credentials","RQ/Rjcf8Gn2+7srAiXNO0ZGuRnJORSir");
			// ssl.setRequestProperty("User-Agent", "ESB");
			ssl.setDoOutput(true);
			DataOutputStream writer = new DataOutputStream(ssl.getOutputStream());
			writer.write(input.getBytes(StandardCharsets.UTF_8));
			String responseString = myInputStreamReader(ssl.getInputStream());
			System.out.println("response : " + responseString);
			return responseString;
		}
		return input;
	}

	public static String SendCallToAppserver(String url, String input) throws Exception {
		if (url.startsWith("http") && !url.startsWith("https")) {
			HttpURLConnection connection = null;
			URL urlCon = new URL(url);
			connection = (HttpURLConnection) urlCon.openConnection();
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			// connection.setRequestProperty("credentials","RQ/Rjcf8Gn2+7srAiXNO0ZGuRnJORSir");
			connection.setRequestProperty("credentials", "RQ/Rjcf8Gn2+7srAiXNO0ZGuRnJORSir");
			// connection.setRequestProperty("User-Agent", "ESB");
			connection.setDoOutput(true);
			DataOutputStream writer = new DataOutputStream(connection.getOutputStream());
			writer.write(input.getBytes(StandardCharsets.UTF_8));
			String responseString = myInputStreamReader(connection.getInputStream());
			System.out.println("response : " + responseString);
			return responseString;
		} else if (url.startsWith("https")) {
			HttpsURLConnection ssl = null;
			// HttpURLConnection connection = null;
			URL urlCon = new URL(url);
			ssl = (HttpsURLConnection) urlCon.openConnection();
			ssl.setConnectTimeout(5000);
			ssl.setRequestMethod("POST");
			ssl.setRequestProperty("Content-Type", "application/json");
			// connection.setRequestProperty("credentials","RQ/Rjcf8Gn2+7srAiXNO0ZGuRnJORSir");
			// ssl.setRequestProperty("User-Agent", "ESB");
			ssl.setRequestProperty("credentials", "RQ/Rjcf8Gn2+7srAiXNO0ZGuRnJORSir");
			ssl.setDoOutput(true);
			DataOutputStream writer = new DataOutputStream(ssl.getOutputStream());
			writer.write(input.getBytes(StandardCharsets.UTF_8));
			String responseString = myInputStreamReader(ssl.getInputStream());
			System.out.println("response : " + responseString);
			return responseString;
		}
		return input;
	}

	public static String SendCallToUlduzum(String url_str,String input) {
		logger.info("Rest Client ulduzum: URL & Response: " + url_str);
		try {
			TrustManager[] trustAllCerts = { new X509TrustManager() {
				@Override
				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				@Override
				public void checkClientTrusted(X509Certificate[] certs, String authType) {
				}

				@Override
				public void checkServerTrusted(X509Certificate[] certs, String authType) {
				}
			} };
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new SecureRandom());

			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			HostnameVerifier allHostsValid = new HostnameVerifier() {
				@Override
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
			URL url = new URL(url_str);

			HttpURLConnection ssl = (HttpURLConnection) url.openConnection();
			ssl.setConnectTimeout(5000);
			ssl.setRequestMethod("POST");

			ssl.setDoOutput(true);
			DataOutputStream writer = new DataOutputStream(ssl.getOutputStream());
//			DataOutputStream writer = new DataOutputStream(connection.getOutputStream());
			writer.write(input.getBytes(StandardCharsets.UTF_8));
			
			String responseString = myInputStreamReader(ssl.getInputStream());
			System.out.println("response : " + responseString);
			return responseString;
		} catch (Exception e) {
			// TODO: handle exception
			logger.info("Exception : " + e.getMessage());
			logger.info(Helper.GetException(e));
		}
		return url_str;

	}

	static public String myInputStreamReader(InputStream in) throws IOException {
		StringBuilder sb = new StringBuilder();
		InputStreamReader reader = null;
		try {
			reader = new InputStreamReader(in);
			int c = reader.read();
			while (c != -1) {
				sb.append((char) c);
				c = reader.read();
			}
			reader.close();
			return sb.toString();
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			if (reader != null)
				reader.close();
		}
		return sb.toString();
	}

	public static String SendCallToGoldenPay(String url, String input) throws Exception {
		
		TrustManager[] trustAllCerts = { new X509TrustManager() {
			@Override
			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			@Override
			public void checkClientTrusted(X509Certificate[] certs, String authType) {
			}

			@Override
			public void checkServerTrusted(X509Certificate[] certs, String authType) {
			}
		} };
		SSLContext sc = SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, new SecureRandom());

		HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

		HostnameVerifier allHostsValid = new HostnameVerifier() {
			@Override
			public boolean verify(String hostname, SSLSession session) {
				return true;
			}
		};
		HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		logger.info("Rest Client: URL: " + url);
		logger.info("Rest Client: Request: " + input);
		if (url.startsWith("http") && !url.startsWith("https")) {
			HttpURLConnection connection = null;
			URL urlCon = new URL(url);
			connection = (HttpURLConnection) urlCon.openConnection();
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			// connection.setRequestProperty("credentials","RQ/Rjcf8Gn2+7srAiXNO0ZGuRnJORSir");
			connection.setDoOutput(true);
			DataOutputStream writer = new DataOutputStream(connection.getOutputStream());
			writer.write(input.getBytes(StandardCharsets.UTF_8));
			String responseString = myInputStreamReader(connection.getInputStream());
			logger.info("Rest Client: Response: " + responseString);
			return responseString;
		} else if (url.startsWith("https")) {
			HttpsURLConnection ssl = null;
			// HttpURLConnection connection = null;
			URL urlCon = new URL(url);
			ssl = (HttpsURLConnection) urlCon.openConnection();
			ssl.setConnectTimeout(5000);
			ssl.setRequestMethod("POST");
			ssl.setRequestProperty("Content-Type", "application/json");
			ssl.setRequestProperty("Accept", "application/json");
			// connection.setRequestProperty("credentials","RQ/Rjcf8Gn2+7srAiXNO0ZGuRnJORSir");
//			ssl.setRequestProperty("User-Agent", "ESB");
			ssl.setDoOutput(true);
			DataOutputStream writer = new DataOutputStream(ssl.getOutputStream());
			writer.write(input.getBytes(StandardCharsets.UTF_8));
			String responseString = myInputStreamReader(ssl.getInputStream());
			System.out.println("response : " + responseString);
			return responseString;
		}
		return input;
	}
	
	public static String SendCallToMagentoNar(String url, String input) throws Exception {
		logger.info("Rest Client: URL: " + url);
		logger.info("Rest Client: Request: " + input);
//		url = ConfigurationManager.getConfigurationFromCache("magento.app.baseurl") + url;
		if (url.startsWith("http") && !url.startsWith("https")) {
			HttpURLConnection connection = null;
			URL urlCon = new URL(url);
			connection = (HttpURLConnection) urlCon.openConnection();
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			// connection.setRequestProperty("credentials","RQ/Rjcf8Gn2+7srAiXNO0ZGuRnJORSir");
			connection.setRequestProperty("User-Agent", "ESB");
			connection.setDoOutput(true);
			DataOutputStream writer = new DataOutputStream(connection.getOutputStream());
			writer.write(input.getBytes(StandardCharsets.UTF_8));
			String responseString = myInputStreamReader(connection.getInputStream());
			logger.info("Rest Client: Response: " + responseString);
			return responseString;
		} else if (url.startsWith("https")) {
			HttpsURLConnection ssl = null;
			// HttpURLConnection connection = null;
			URL urlCon = new URL(url);
			ssl = (HttpsURLConnection) urlCon.openConnection();
			ssl.setConnectTimeout(5000);
			ssl.setRequestMethod("POST");
			ssl.setRequestProperty("Content-Type", "application/json");
			// connection.setRequestProperty("credentials","RQ/Rjcf8Gn2+7srAiXNO0ZGuRnJORSir");
			ssl.setRequestProperty("User-Agent", "ESB");
			ssl.setDoOutput(true);
			DataOutputStream writer = new DataOutputStream(ssl.getOutputStream());
			writer.write(input.getBytes(StandardCharsets.UTF_8));
			String responseString = myInputStreamReader(ssl.getInputStream());
			System.out.println("response : " + responseString);
			return responseString;
		}
		return input;
	}
	
}
