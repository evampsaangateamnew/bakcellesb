package com.evampsaanga.ulduzumupdate.getnewcode;

import com.evampsaanga.bakcell.responseheaders.BaseResponse;

public class GetNewCodeResponseClient extends BaseResponse {

	GetNewCode data;

	public GetNewCode getData() {
		return data;
	}

	public void setData(GetNewCode data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "GetNewCodeResponseClient [data=" + data + "]";
	}

}
