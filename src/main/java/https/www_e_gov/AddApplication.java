
package https.www_e_gov;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import net.x_rd.az.voen9900037691.producer.AddApplicationRequest;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AddApplicationRequest" type="{http://voen9900037691.az.x-rd.net/producer}AddApplicationRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "addApplicationRequest"
})
@XmlRootElement(name = "AddApplication")
public class AddApplication {

    @XmlElement(name = "AddApplicationRequest")
    protected AddApplicationRequest addApplicationRequest;

    /**
     * Gets the value of the addApplicationRequest property.
     * 
     * @return
     *     possible object is
     *     {@link AddApplicationRequest }
     *     
     */
    public AddApplicationRequest getAddApplicationRequest() {
        return addApplicationRequest;
    }

    /**
     * Sets the value of the addApplicationRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddApplicationRequest }
     *     
     */
    public void setAddApplicationRequest(AddApplicationRequest value) {
        this.addApplicationRequest = value;
    }

}
