package com.evampsaanga.bakcell.external.services;

import com.evampsaanga.bakcell.responseheaders.BaseResponse;

public class LoginExternalResponse extends BaseResponse{

    private String entityId;

    public String getEntityId() {
	return entityId;
    }

    public void setEntityId(String entityId) {
	this.entityId = entityId;
    }
    
}
