package com.evampsaanga.bakcell.ussdgwqueryfreeresource;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;
import com.huawei.bss.soaif._interface.common.ReqHeader;

@Path("/bakcell")
public class USSDGWQueryFreeResourcesLand {
	public static final Logger logger = Logger.getLogger("bakcellLogs-V2");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public USSDGWQueryFreeResourcesResponse Get(@Header("credentials") String credential, @Body() String requestBody) {
		USSDGWQueryFreeResourcesResponse response = new USSDGWQueryFreeResourcesResponse();
		USSDGWQueryFreeResourcesRequestClient cclient = null;
		try {
			cclient = Helper.JsonToObject(requestBody, USSDGWQueryFreeResourcesRequestClient.class);
		} catch (Exception ex) {
			response.setReturnCode(ResponseCodes.ERROR_400_CODE);
			response.setReturnMsg(ResponseCodes.ERROR_400);
			return response;
		}
		String credentials = null;
		if (cclient != null) {
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				// TODO: handle exception
			}
			if (credentials == null) {
				response = new USSDGWQueryFreeResourcesResponse();
				response.setReturnCode(ResponseCodes.ERROR_401_CODE);
				response.setReturnMsg(ResponseCodes.ERROR_401);
				return response;
			}
			/// ussd GWQuery getCountryCode
			String verification = Helper.validateRequest(cclient);
			if (!verification.equals("")) {
				response = new USSDGWQueryFreeResourcesResponse();
				response.setReturnCode(ResponseCodes.ERROR_400);
				response.setReturnMsg(verification);
				return response;
			}
			if (credentials.equals(Constants.CREDENTIALS)) {
				// resources.getFreeResources(cclient.getmsisdn() ,cclient.get);
				// return callSoap(getRequestHeaderForFreeResources(), cclient);
			} else {
				USSDGWQueryFreeResourcesResponse resp = new USSDGWQueryFreeResourcesResponse();
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				return resp;
			}
		} else
			System.out.println("Request failed");
		return null;
	}

	public ReqHeader getRequestHeaderForFreeResources() {
		ReqHeader reqh = new ReqHeader();
			reqh.setAccessUser(ConfigurationManager.getConfigurationFromCache("ussd.reqh.AccessUser").trim());
			reqh.setChannelId(ConfigurationManager.getConfigurationFromCache("ussd.reqh.ChannelId").trim());
			reqh.setAccessPwd(ConfigurationManager.getConfigurationFromCache("ussd.reqh.AccessPwd").trim());
			reqh.setTransactionId(Helper.generateTransactionID());
			return reqh;
	}
}
