
package net.x_rd.az.voen9900037691.producer;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import https.wwww_e_gov.RequestAuthentication;


/**
 * <p>Java class for GetSunlistByVoenRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetSunlistByVoenRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Authentication" type="{https://wwww.e-gov.az}RequestAuthentication" minOccurs="0"/>
 *         &lt;element name="VOEN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetSunlistByVoenRequest", propOrder = {
    "authentication",
    "voen"
})
public class GetSunlistByVoenRequest {

    @XmlElement(name = "Authentication")
    protected RequestAuthentication authentication;
    @XmlElement(name = "VOEN")
    protected String voen;

    /**
     * Gets the value of the authentication property.
     * 
     * @return
     *     possible object is
     *     {@link RequestAuthentication }
     *     
     */
    public RequestAuthentication getAuthentication() {
        return authentication;
    }

    /**
     * Sets the value of the authentication property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestAuthentication }
     *     
     */
    public void setAuthentication(RequestAuthentication value) {
        this.authentication = value;
    }

    /**
     * Gets the value of the voen property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVOEN() {
        return voen;
    }

    /**
     * Sets the value of the voen property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVOEN(String value) {
        this.voen = value;
    }

}
